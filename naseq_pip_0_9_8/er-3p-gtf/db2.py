#!/usr/bin/env python

from gtf_to_genes import *
import logging
logger = logging.getLogger("test")



# Index  file
index_file = "/gs/project/wst-164-ab/test2/gtf.index"

# look for GTF or gzipped GTF files
# regex_input = r"(.+\/)(([^.]+)\..+\.(.+)\.gtf(?:\.gz)?)$"
regex_input = r"(.+)\.gtf$"

search_path_root = "/gs/project/wst-164-ab/test2"

# put cache file in same directory as GTF index file
cache_file_pattern = r"/gs/project/wst-164-ab/test2/\1.cache"

#
# uncomment this line to put cache file in same directory index file
#
# cache_file_pattern   = r"{INDEX_FILE_PATH}/\2.cache"

#
# Unique identifier per GTF file
# e.g. "Anolis_carolinensis:77"
#
identifier_pattern = r"\1:aaa"


# index_gtf_files(index_file,
#                 search_path_root,
#                 regex_input,
#                 cache_file_pattern,
#                 identifier_pattern,
#                 False,
#                 logger)

index_gtf_files(index_file,
                search_path_root,
                regex_input,
                cache_file_pattern,
                identifier_pattern,
                False)
