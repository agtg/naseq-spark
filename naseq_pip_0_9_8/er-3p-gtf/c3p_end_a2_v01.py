#!/usr/bin/env python
from __future__ import print_function

import gc
import os
import subprocess

import tables

import itertools
import time


"""
coding_lengths.py
Kamil Slowikowski
February 7, 2014

Count the number of coding base pairs in each Gencode gene.

Gencode coordinates, including all exons with Ensembl identifiers.
(Gencode release 17 corresponds to hg19)

    ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
    ftp://ftp.sanger.ac.uk/pub/gencode/release_17/gencode.v17.annotation.gtf.gz

    chr1  HAVANA  gene        11869  14412  .  +  .  gene_id "ENSG00000223972.4";
    chr1  HAVANA  transcript  11869  14409  .  +  .  gene_id "ENSG00000223972.4";
    chr1  HAVANA  exon        11869  12227  .  +  .  gene_id "ENSG00000223972.4";
    chr1  HAVANA  exon        12613  12721  .  +  .  gene_id "ENSG00000223972.4";
    chr1  HAVANA  exon        13221  14409  .  +  .  gene_id "ENSG00000223972.4";


NCBI mapping from Entrez GeneID to Ensembl identifiers.

    ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2ensembl.gz

    9606  1  ENSG00000121410  NM_130786.3     ENST00000263100  NP_570602.2     ENSP00000263100
    9606  2  ENSG00000175899  NM_000014.4     ENST00000318602  NP_000005.2     ENSP00000323929
    9606  3  ENSG00000256069  NR_040112.1     ENST00000543404  -               -
    9606  9  ENSG00000171428  NM_000662.5     ENST00000307719  NP_000653.3     ENSP00000307218
    9606  9  ENSG00000171428  XM_005273679.1  ENST00000517492  XP_005273736.1  ENSP00000429407

Output:

    Ensembl_gene_identifier  GeneID  length
    ENSG00000000005          64102   1339
    ENSG00000000419          8813    1185
    ENSG00000000457          57147   3755
    ENSG00000000938          2268    3167

USAGE:
    coding_lengths.py -g FILE -n FILE [-o FILE]

OPTIONS:
    -h          Show this help message.
    -g FILE     Gencode annotation.gtf.gz file.
    -n FILE     NCBI gene2ensembl.gz file.
    -o FILE     Output file (gzipped).
"""


# import GTF                      # https://gist.github.com/slowkow/8101481
import GTF_ENCODE                      # https://gist.github.com/slowkow/8101481

from docopt import docopt
import pandas as pd
import numpy as np
import scipy.sparse

import gzip
import time
import sys
from contextlib import contextmanager

import tables

def chop_exons(df):
    """Given a DataFrame with the exon coordinates from Gencode for a single
    gene, return the total number of coding bases in that gene.
    Example:
        >>> import numpy as np
        >>> n = 3
        >>> r = lambda x: np.random.sample(x) * 10
        >>> d = pd.DataFrame([np.sort([a,b]) for a,b in zip(r(n), r(n))], columns=['start','end']).astype(int)
        >>> d
           start  end
        0      6    9
        1      3    4
        2      4    9
        >>> count_bp(d)
        7
    """
    # start = df.start.min()

    dist_to_end = 500

    end = df.end.max()

    tr_columns = ['transcript_id']
    tr_ = pd.DataFrame(columns=tr_columns)



    for i in range(df.shape[0]):

        # s = df.iloc[i]['start']
        e = df.iloc[i]['end']
        if e == end:
            tr_.loc[len(tr_)+1] = [df.iloc[i]['transcript_id']]

    # arbitrarily select first transcript covering the end
    best_tr = tr_.iloc[0]['transcript_id']

    # print("best_tr: ", best_tr)

    ex_columns = ['gene_name', 'transcript_id', 'transcript_name', 'exon_id','exon_number', 'seqname', 'start', 'end', 'source']
    ex_ = pd.DataFrame(columns=ex_columns)

    # parse exons again
    for i in range(df.shape[0]):

        gene_name = df.iloc[i]['gene_name']
        transcript_id = df.iloc[i]['transcript_id']
        transcript_name = df.iloc[i]['transcript_name']

        exon_id = df.iloc[i]['exon_id']
        exon_number = df.iloc[i]['exon_number']
        seqname = df.iloc[i]['seqname']
        s = df.iloc[i]['start']
        e = df.iloc[i]['end']
        source = df.iloc[i]['source']

        # only exons of the best transcript are processed
        if transcript_id == best_tr:
            # exon in window
            if end - e < dist_to_end:
                # correct begining
                if end - s > dist_to_end:
                    s = end - dist_to_end
                # add exon to output
                ex_.loc[len(ex_)+1] = [gene_name, transcript_id, transcript_name, exon_id, exon_number, seqname, s, e, source]


    # print("ex_: \n", ex_)


    # tr_gr = tr_.groupby('transcript_id')

    # print("tr_gr: \n", tr_gr)

    # g0 = tr_gr.first()

    # print("g0: \n", g0)

    # bp = [False] * (end - start + 1)
    # for i in range(df.shape[0]):
    #     s = df.iloc[i]['start'] - start
    #     e = df.iloc[i]['end'] - start + 1
    #     bp[s:e] = [True] * (e - s)

    # df_ = df_.reset_index(drop=True)

    return ex_


def proc3(chrom):

    store = pd.HDFStore('Homo_sapiens.GRCh37.75.h5')
    print(store)
    sel = store.select('all/compr', where="seqname == %r & feature == exon" % chrom)

    print("sel: \n", sel)

    store.close()

    return





def proc2(chrom):


    store = pd.HDFStore('Homo_sapiens.GRCh37.75.h5')

    print(store)
    # store.remove('all/all')

    # compr = store['all/all']
    # store.put('all/compr', compr, format='table', complib='blosc', data_columns=True, chunksize=2000000)

    # print(store)
    # store.close()

    # exit(0)

    # exon = store['/exon']
    # store.remove('exons/exon2')
    # store.put('exons/exon3', exon, format='table', data_columns=['seqname', 'gene_id', 'transcript_id', 'exon_id'])
    # store.put('exons/exon3', exon, format='table', data_columns=True)

    # print("exon: \n", exon)

    # store.put('exons/exon2', exon, format='table')

    # sel = store.select('exons/exon3', where="gene_id == ENSG00000237917  & columns == gene_id" )
    # sel = store.select('all/compr', where="seqname == Y & gene_id == ENSG00000129824")
    # sel = store.select('all/compr', where="seqname == Y & feature == exon")
    # sel = store.select('all/compr', where="seqname == Y & feature == exon")
    sel = store.select('all/compr', where="seqname == %r & feature == exon" % chrom)

    # sel = store.select('all/compr', where="feature == exon")
    # sel = store.select('all/exon', where="seqname == Y")
    # sel = store.select('exons/exon3', where="gene_id == %r" % "ENSG00000127603")
    # sel = store.select('exons/exon3')
    print("sel: \n", sel)


    # Group the rows by the Ensembl gene identifier (with version numbers.)
    groups = sel.groupby('gene_id')
    # print("groups: ", groups.first())

    #
    gr_res = groups.apply(chop_exons)
    #
    # gr_res = gr_res.reset_index(level=0)
    gr_res = gr_res.reset_index()

    print("gr_res: \n", gr_res)


    store.close()


    gr_res_gtf = "gr_res4_%s.gtf" % chrom
    gr_res_gtf_f = open(gr_res_gtf, "w")

    # start = df.start.min()
    # end = df.end.max()
    # bp = [False] * (end - start + 1)
    for i in range(gr_res.shape[0]):
        s = gr_res.iloc[i]['start'].astype('int')
        e = gr_res.iloc[i]['end'].astype('int')


        chrm = gr_res.iloc[i]['seqname']

        # print("chrm, size: ", chrm, size)
        #
        # gene_row = r"""%(chrm)s%(sep)sENCODE%(sep)sgene%(sep)s1%(sep)s%(size)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(chrm)s"; transcript_id "%(chrm)s"; gene_type "spikein"; gene_status "PUTATIVE"; gene_name "%(chrm)s"; transcript_type "spikein"; transcript_status "PUTATIVE"; transcript_name "%(chrm)s"; level 2;""" \
        #     % {"chrm": chrm, "sep": "\t", "size": size}
        # spikes_gtf_f.write(gene_row)
        # spikes_gtf_f.write("\n")

        # transcript_row = r"""%(chrm)s%(sep)sENCODE%(sep)stranscript%(sep)s1%(sep)s%(size)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(chrm)s"; transcript_id "%(chrm)s"; gene_type "spikein"; gene_status "PUTATIVE"; gene_name "%(chrm)s"; transcript_type "spikein"; transcript_status "PUTATIVE"; transcript_name "%(chrm)s"; level 2;""" \
        #     % {"chrm": chrm, "sep": "\t", "size": size}
        # spikes_gtf_f.write(transcript_row)
        # spikes_gtf_f.write("\n")

        exon_row = r"""%(chrm)s%(sep)s%(source)s%(sep)sexon%(sep)s%(start)s%(sep)s%(end)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(gene_id)s"; transcript_id "%(transcript_id)s"; gene_name "%(gene_name)s"; transcript_name "%(transcript_name)s"; exon_id "%(exon_id)s"; exon_number %(exon_number)s;""" \
            % {"chrm": chrm,
               "source": gr_res.iloc[i]['source'],
               "sep": "\t",
               "start": s,
               "end": e,
               "gene_id": gr_res.iloc[i]['gene_id'],
               "gene_name": gr_res.iloc[i]['gene_name'],
               "transcript_id": gr_res.iloc[i]['transcript_id'],
               "transcript_name": gr_res.iloc[i]['transcript_name'],
               "exon_id": gr_res.iloc[i]['exon_id'],
               "exon_number": gr_res.iloc[i]['exon_number']
               }
        gr_res_gtf_f.write(exon_row)
        gr_res_gtf_f.write("\n")

    gr_res_gtf_f.close()

    del sel
    del groups
    del gr_res
    gc.collect()


# https://fgiesen.wordpress.com/2011/10/16/checking-for-interval-overlap/
#  [a0, a1] and [b0, b1]
#  [sx, ex]     [sy, ey]
def is_superpose(xx, yy, sx, sy, ex, ey):

    # return (a0 <= b1) & (b0 <= a1)
    return (sx <= ey) & (sy <= ex)



def proc4_plus(chrom, dist):


    store = pd.HDFStore('Homo_sapiens.GRCh37.75.h5')

    print(store)
    # store.remove('all/all')

    # compr = store['all/all']
    # store.put('all/compr', compr, format='table', complib='blosc', data_columns=True, chunksize=2000000)

    # print(store)
    # store.close()

    # exit(0)

    # exon = store['/exon']
    # store.remove('exons/exon2')
    # store.put('exons/exon3', exon, format='table', data_columns=['seqname', 'gene_id', 'transcript_id', 'exon_id'])
    # store.put('exons/exon3', exon, format='table', data_columns=True)

    # print("exon: \n", exon)

    # store.put('exons/exon2', exon, format='table')

    # sel = store.select('exons/exon3', where="gene_id == ENSG00000237917  & columns == gene_id" )
    # sel = store.select('all/compr', where="seqname == Y & gene_id == ENSG00000129824")
    # sel = store.select('all/compr', where="seqname == Y & feature == exon")
    # sel = store.select('all/compr', where="seqname == Y & feature == exon")

    sel = store.select('all/compr', where="seqname == %r & feature == exon" % chrom,
                       columns=['gene_id', 'transcript_id', 'exon_id', 'start', 'end',
                                'source', 'gene_name', 'transcript_name', 'exon_number', 'strand'])

    # sel = store.select('all/compr', where="feature == exon")
    # sel = store.select('all/exon', where="seqname == Y")
    # sel = store.select('exons/exon3', where="gene_id == %r" % "ENSG00000127603")
    # sel = store.select('exons/exon3')
    # print("sel: \n", sel)

    all_ex_df = sel
    all_ex_df.reset_index(inplace=True, drop=True)
    # sort for output
    all_ex_df.sort(['gene_id', 'transcript_id', 'end'], inplace=True)
    all_ex_df.to_csv('all_ex_df.csv', sep=","
                       ,header=True
                       ,index=True
                       ,index_label="idx"
                       )



    # print("all_ex_df: \n", all_ex_df)

    # exit(0)

    tr_gn_df = all_ex_df.groupby('transcript_id').first()
    tr_gn_df.drop('exon_id', axis=1, inplace=True)
    tr_gn_df.drop('start', axis=1, inplace=True)
    tr_gn_df.drop('end', axis=1, inplace=True)
    # print("tr_gn_df: \n", tr_gn_df)


    tr_fp_df = pd.read_csv('isoforms.fpkm_tracking', sep='\t', header=False,
                                     index_col=[0],
                                     usecols=[0, 11],
                                     names=["transcript_id", "fp"]
                              )
    # print("tr_fp_df: \n", tr_fp_df)

    # join t_g_f

    # tr_gn_fp_df = pd.merge(tr_gn_df, tr_fp_df, how='inner', left_on=['transcript_id'], right_on=['transcript_id'])
    all_tr_gn_fp_df = pd.merge(tr_gn_df, tr_fp_df, how='inner', left_index=True, right_index=True)
    # print("tr_gn_fp_df: \n", tr_gn_fp_df)
    all_tr_gn_fp_df.reset_index(level=0, inplace=True)

    g = all_tr_gn_fp_df.sort('fp', ascending=False).groupby(['gene_id'], as_index=False)
    # print("g: ", g)
    tr_gn_fp_df = g.agg({'transcript_id': lambda x: x.iloc[0], 'fp': lambda x: x.iloc[0]})
    # gb.rename(columns={'score': 'cnt'}, inplace=True)

    # gb_min = g.agg({'rname': lambda x: x.iloc[0], 'score': 'min'})
    # gb['score_min'] = gb_min.score
    # print ("gb_cnt: ", gb_cnt)
    # print ("gb_min: ", gb_min)
    # gb_cnt['bar'] = df.bar.map(str) + " is " + df.foo
    # gb = g.apply(self.raw_stats)


    # print("tr_gn_fp_df: \n", tr_gn_fp_df)
    # tr_gn_fp_df is the list of best ranking fpkm transcript per gene

    # here we delete fpkm ==0
    tr_gn_fp_df=tr_gn_fp_df[tr_gn_fp_df['fp'] != 0]


    # calculate best transcripts boundaries based on their exomes and their strand

    best_tr_ex_df = pd.merge(tr_gn_fp_df, all_ex_df, how='inner',
                             left_on=['transcript_id'], right_on=['transcript_id'])

    print("best_tr_ex_df: \n", best_tr_ex_df)

    # aggregate to extract limits
    g = best_tr_ex_df.groupby(['transcript_id'], as_index=False)
    print("g: \n", g)

    gr = g.agg({'start': 'min', 'end': 'max', 'strand': lambda x: x.iloc[0]
                ,'fp': lambda x: x.iloc[0]}
               )

    # gr.to_csv('gr.csv', sep=","
    #                        ,header=True
    #                        ,index=True
    #                        ,index_label="idx"
    #                        )

    start = gr['start'].values
    end = gr['end'].values
    strand_np = gr['strand'].values
    start_p = (gr['start'] + dist).values
    end_m = (gr['end'] - dist).values

    ts = np.where(strand_np == '+', end_m, start)
    te = np.where(strand_np == '+', end, start_p)

    gr.drop('start', axis=1, inplace=True)
    gr.drop('end', axis=1, inplace=True)
    gr.insert(2, 'ts', ts)
    gr.insert(3, 'te', te)

    gr['clust'] = np.zeros(gr.shape[0])

    best_tr_info_df = gr.copy()
    best_tr_info_df.reset_index(inplace=True, drop=True)
    best_tr_info_df.drop_duplicates()

    # print("best_tr_info_df: \n", best_tr_info_df)

        # print("df.shape before drop: \n", df.shape)
        # df.drop_duplicates(cols=["idx_seq"], inplace=True)
        # print("df.shape after drop: \n", df.shape)


    # clustering

    n = gr.shape[0]
    x = scipy.sparse.lil_matrix( (n,n) )

    ts = best_tr_info_df['ts'].values
    te = best_tr_info_df['te'].values

    for (i,j) in itertools.product(range(n), repeat=2):
        sx = ts[i]
        sy = ts[j]
        ex = te[i]
        ey = te[j]

        if (sx <= ey) & (sy <= ex):
            x[i,j] = 1

    # connected components
    (n_components, labels) = scipy.sparse.csgraph.connected_components(x)
    # print("n_components: ", n_components)
    # print("labels: ", labels, labels.size)

    best_tr_info_df['clust'] = labels
    print("best_tr_info_df: \n", best_tr_info_df)

    best_tr_info_df.to_csv('best_tr_info_df.csv', sep=","
                           ,header=True
                           ,index=True
                           ,index_label="idx"
                           )

    g = best_tr_info_df.groupby(['clust'], as_index=False)
    print("g: \n", g)

    gr = g.agg({'transcript_id': 'count'}
               )

    print("gr: \n", gr)

    gr.to_csv('clust_count.csv', sep=","
              , header=True
              , index=True
              , index_label="idx"
              )



        # print("ij: ", ij)

        # x[ij] = a[ij]

    # exit(0)


    # cx = x.tocoo()
    # for i,j,v in zip(cx.row, cx.col, cx.data):






    # x = np.arange(0, gr.shape[0], 1)
    # y = np.arange(0, gr.shape[0], 1)
    # ts = best_tr_info_df['ts'].values
    # te = best_tr_info_df['te'].values
    #
    # xx, yy = np.meshgrid(x, y, sparse=True, indexing='ij')
    # sx, sy = np.meshgrid(ts, ts, sparse=True, indexing='ij')
    # ex, ey = np.meshgrid(te, te, sparse=True, indexing='ij')
    #
    # f_vec = np.vectorize(is_superpose) # vectorization of function f
    # tp_vec = f_vec(xx, yy, sx, sy, ex, ey)
    #
    # print("tp_vec: \n", tp_vec.shape)

    # print('meshgrid\nx\n', xx,'\ny\n',yy)



    exit(0)


    # g = best_tr_ex_df.sort('end', ascending=False).groupby(['transcript_id'], as_index=False).first()
    # print("g: \n", g)

    # tr_coord_df = g[['transcript_id', 'start', 'end', 'strand']].copy()


    tr_coord_df.insert(1 ,'ts', (g[['end']] - dist))
    tr_coord_df.rename(columns={'end': 'te'}, inplace=True)
    # print("tr_coord_df: \n", tr_coord_df)

    # final transcript coordinates
    best_tr_info_df = pd.merge(tr_coord_df, tr_gn_fp_df, how='inner',
                             left_on=['transcript_id'], right_on=['transcript_id'])
    print("best_tr_info_df: \n", best_tr_info_df)







    exit(0)



    # Group the rows by the Ensembl gene identifier (with version numbers.)
    all_trans = sel.groupby('transcript_id')

    print("all_trans: ", all_trans.first())


    #
    # gr_res = groups.apply(chop_exons)
    #
    # gr_res = gr_res.reset_index(level=0)
    # gr_res = gr_res.reset_index()

    # print("gr_res: \n", gr_res)


    store.close()


    # del sel
    # del groups
    # del gr_res
    gc.collect()




def read_gtf():

    pd.set_option('io.hdf.default_format', 'table')

    store = pd.HDFStore('Homo_sapiens.GRCh37.75.h5')

    print(store)

    # Input files.
    GENCODE      = "Homo_sapiens.GRCh37.75.gtf"
    # NCBI_ENSEMBL = args['-n']

    # Output file prefix.
    # GENE_LENGTHS = "ncbi_ensembl_coding_lengths.txt.gz"

    with log("Reading the Gencode annotation file: {}".format(GENCODE)):
        gc = GTF_ENCODE.dataframe(GENCODE)

    print("gc: \n ", gc)


    # Select just exons of protein coding genes, and columns that we want to use.
    # idx = (gc.feature == 'exon') & (gc.transcript_type == 'protein_coding')
    # idx = (gc.feature == 'exon')
    # exon = gc.ix[idx, ['seqname', 'transcript_type', 'feature', 'start', 'end', 'score', 'strand', 'frame',
    #                    'gene_biotype', 'gene_id', 'gene_name', 'gene_source',
    #                    'transcript_id', 'transcript_name', 'transcript_source', 'exon_id', 'exon_number']]

    # Convert columns to proper types.
    gc.start = gc.start.astype(int)
    gc.end = gc.end.astype(int)

    # Sort in place.
    gc.sort(['seqname', 'start', 'end'], inplace=True)

    # print("exon: \n ", exon)
    #
    # store['exon'] = exon

    # store.put('all/all', gc, format='table', data_columns=True)
    store.put('all/compr', gc, format='table', complib='blosc', chunksize=2000000,
              data_columns=['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame',
                            'gene_id', 'gene_name', 'gene_source', 'gene_biotype',
                            'ccds_id', 'transcript_id', 'transcript_name', 'transcript_source' ,'tss_id',
                            'exon_id', 'exon_number',
                            'p_id', 'protein_id', 'tag'])
    print(store)

    store.close()

    # gc.collect()

def todo():

    # Group the rows by the Ensembl gene identifier (with version numbers.)
    groups = exon.groupby('gene_id')

    with log("Calculating coding region (exonic) length for each gene..."):
        lengths = groups.apply(count_bp)


        # print("ij: ", ij)

        # x[ij] = a[ij]

    # exit(0)


    # cx = x.tocoo()
    # for i,j,v in zip(cx.row, cx.col, cx.data):






    # x = np.arange(0, gr.shape[0], 1)
    # y = np.arange(0, gr.shape[0], 1)
    # ts = best_tr_info_df['ts'].values
    # te = best_tr_info_df['te'].values
    #
    # xx, yy = np.meshgrid(x, y, sparse=True, indexing='ij')
    # sx, sy = np.meshgrid(ts, ts, sparse=True, indexing='ij')
    # ex, ey = np.meshgrid(te, te, sparse=True, indexing='ij')
    #
    # f_vec = np.vectorize(is_superpose) # vectorization of function f
    # tp_vec = f_vec(xx, yy, sx, sy, ex, ey)
    #
    # print("tp_vec: \n", tp_vec.shape)

    # print('meshgrid\nx\n', xx,'\ny\n',yy)




    with log("Reading NCBI mapping of Entrez GeneID "\
             "to Ensembl gene identifier: {}".format(NCBI_ENSEMBL)):
        g2e = pd.read_table(NCBI_ENSEMBL,
                            compression="gzip",
                            header=None,
                            names=['tax_id', 'GeneID',
                                   'Ensembl_gene_identifier',
                                   'RNA_nucleotide_accession.version',
                                   'Ensembl_rna_identifier',
                                   'protein_accession.version',
                                   'Ensembl_protein_identifier'])

    # Create a new DataFrame with gene lengths and EnsemblID.
    ensembl_no_version = lengths.index.map(lambda x: x.split(".")[0])
    ldf = pd.DataFrame({'length': lengths,
                        'Ensembl_gene_identifier': ensembl_no_version},
                       index=lengths.index)

    # Merge so we have EntrezGeneID with length.
    m1 = pd.merge(ldf, g2e, on='Ensembl_gene_identifier')
    m1 = m1[['Ensembl_gene_identifier', 'GeneID', 'length']].drop_duplicates()

    with log("Writing output file: {}".format(GENE_LENGTHS)):
        with gzip.open(GENE_LENGTHS, "wb") as out:
            m1.to_csv(out, sep="\t", index=False)


def main():



    chr = "1"

    proc4_plus(chr, 200)

    exit(0)

    # h5file= tables.open_file('Homo_sapiens.GRCh37.75.h5', mode= "r")

    print("h5file: ", h5file)
    table = h5file.root.all.compr.table

    # print("table: ", table)

    # table.where("""(TDCcount > 3) & (20 <= pressure) & (pressure < 50)""")

    # pr = [x['transcript_id'] for x in table.where('seqname == "1"')]
    pr = [x for x in table.where('transcript_id == "ENST00000530986"') or ('transcript_id == "ENST00000391820"')]



    # pr = [x['transcript_id'] for x in table.iterrows() if x['transcript_id'] >= "ENST0"]

    print("pr: ", pr)

    h5file.close()

    exit(0)


    # read_gtf()

    tot_res_gtf = "c3p_a2_res_01.gtf"

    for i in [tot_res_gtf]:
        if os.path.exists(i):
            os.remove(i)

    # fasta start
    cmd = r"""touch %s""" % tot_res_gtf
    print(cmd)
    p3 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    p3.wait()
    rc3 = p3.returncode
    print("rc3: ", rc3)

    for i in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
          "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y"]:
        proc2(i)

        gr_res_gtf = "gr_res4_%s.gtf" % i

        # add to gtf
        cmd = r"""cat %s >> %s""" % (gr_res_gtf, tot_res_gtf)
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)



    # exit(0)



def count_bp(df):
    """Given a DataFrame with the exon coordinates from Gencode for a single
    gene, return the total number of coding bases in that gene.
    Example:
        >>> import numpy as np
        >>> n = 3
        >>> r = lambda x: np.random.sample(x) * 10
        >>> d = pd.DataFrame([np.sort([a,b]) for a,b in zip(r(n), r(n))], columns=['start','end']).astype(int)
        >>> d
           start  end
        0      6    9
        1      3    4
        2      4    9
        >>> count_bp(d)
        7
    """
    start = df.start.min()
    end = df.end.max()
    bp = [False] * (end - start + 1)
    for i in range(df.shape[0]):
        s = df.iloc[i]['start'] - start
        e = df.iloc[i]['end'] - start + 1
        bp[s:e] = [True] * (e - s)
    return sum(bp)


@contextmanager
def log(message):
    """Log a timestamp, a message, and the elapsed time to stderr."""
    start = time.time()
    sys.stderr.write("{} # {}\n".format(time.asctime(), message))
    yield
    elapsed = int(time.time() - start + 0.5)
    sys.stderr.write("{} # done in {} s\n".format(time.asctime(), elapsed))
    sys.stderr.flush()


if __name__ == '__main__':
    # args = docopt(__doc__)
    main()

