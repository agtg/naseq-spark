#!/usr/bin/env python

import pandas as pd
import io

print "ok"

faidx = "spikes3p92.fa.fai"

chrm_df = pd.read_csv(faidx,
               sep='\t', 
               header = None,
               #index_col=[0]
               usecols=[0,1]
               )
#print chrm_df

spikes_gtf = "spikes.gtf"
spikes_gtf_f = open(spikes_gtf,"w")

for chrm_idx in range(chrm_df.shape[0]):
    print "chrm_idx: ", chrm_idx
    chrm = chrm_df.iloc[chrm_idx,0]
    size = chrm_df.iloc[chrm_idx,1]
    print "chrm, size: ", chrm, size
    #
    gene_row = r"""%(chrm)s%(sep)sENCODE%(sep)sgene%(sep)s1%(sep)s%(size)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(chrm)s"; transcript_id "%(chrm)s"; gene_type "spikein"; gene_status "PUTATIVE"; gene_name "%(chrm)s"; transcript_type "spikein"; transcript_status "PUTATIVE"; transcript_name "%(chrm)s"; level 2;""" \
        % {"chrm": chrm, "sep": "\t", "size": size}
    spikes_gtf_f.write(gene_row)
    spikes_gtf_f.write("\n")
    
    transcript_row = r"""%(chrm)s%(sep)sENCODE%(sep)stranscript%(sep)s1%(sep)s%(size)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(chrm)s"; transcript_id "%(chrm)s"; gene_type "spikein"; gene_status "PUTATIVE"; gene_name "%(chrm)s"; transcript_type "spikein"; transcript_status "PUTATIVE"; transcript_name "%(chrm)s"; level 2;""" \
        % {"chrm": chrm, "sep": "\t", "size": size}
    spikes_gtf_f.write(transcript_row)
    spikes_gtf_f.write("\n")
        
    exon_row = r"""%(chrm)s%(sep)sENCODE%(sep)sexon%(sep)s1%(sep)s%(size)s%(sep)s.%(sep)s+%(sep)s.%(sep)sgene_id "%(chrm)s"; transcript_id "%(chrm)s"; gene_type "spikein"; gene_status "PUTATIVE"; gene_name "%(chrm)s"; transcript_type "spikein"; transcript_status "PUTATIVE"; transcript_name "%(chrm)s"; level 2; exon_number 1;""" \
        % {"chrm": chrm, "sep": "\t", "size": size}
    spikes_gtf_f.write(exon_row)
    spikes_gtf_f.write("\n")

spikes_gtf_f.close()

 
