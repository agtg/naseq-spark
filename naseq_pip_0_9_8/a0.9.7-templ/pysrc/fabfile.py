
import psutil
from fabric.api import *
from fabric.contrib.console import confirm
import time

from fabric.context_managers import settings


def main(): 
    global env
    env.host_string = "sw-2r13-n37"
    code_dir = '/gs/project/wst-164-ab/exome/dac-901/pysrc'
    
    run("/gs/project/wst-164-ab/exome/dac-901/pysrc/check_work.py")
    
