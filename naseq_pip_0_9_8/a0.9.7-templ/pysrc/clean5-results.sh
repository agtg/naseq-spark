#!/bin/sh

# this is data pipeline
rm -fr ../bam
rm -fr ../fastq
 
# deployment
#rm -fr ../raw_reads/*
#rm -fr ../conf/all-sample-info.csv
#rm -fr ../conf/one-sample-info.csv
#rm -fr ../conf/grp-sample-info.csv
#rm -fr ../conf/smp_report.csv

# trimming
rm -fr ../reads2

rm -fr ../alignment2
#rm -fr ../align-scratch/*
rm -fr ../align2
rm -fr ../align3

#ln -s /gs/scratch/dbadescu/share/expr/a011-019-x038/alignment2 /gs/project/wst-164-ab/share/expr/a011-019-x038/align-scratch
#unlink ../align-scratch

# sample level
#rm -fr ../alignment2/*/*.mer*
#rm -fr ../alignment2/*/*.ral*
#rm -fr ../alignment2/*/*.rca*
#rm -fr ../alignment2/*/ral_smp_contigs
#rm -fr ../alignment2/*/rca_smp_rpt


# statistics
rm -fr ../metrics2
rm -fr ../matr
rm -fr ../deploy
rm -fr ../rnaseqc

# variation
rm -fr ../variants2

# redis
rm -fr job_output/*

