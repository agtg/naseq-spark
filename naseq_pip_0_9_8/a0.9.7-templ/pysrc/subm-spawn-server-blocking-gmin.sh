#!/bin/sh


WORK_DIR=`pwd`
JOB_OUTPUT_ROOT=$WORK_DIR/job_output

TARGET_JOB_ID=$(echo "naseq_server.py --spawn-server-blocking 1 1700" | qsub -V -m ae -W umask=0002 -A wst-164-ab -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N a002-srv -l walltime=32:00:0 -q metaq -l nodes=1:ppn=12,pmem=1700M | grep "[0-9]")

#TARGET_JOB_ID=$(echo "run_naseq.sh test --spawn-server-blocking 1 1700" | qsub -V -m ae -W umask=0002 -A wst-164-ab -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N a028t-srv -l \
#walltime=2:00:0 -q metaq -l nodes=1:ppn=12,pmem=1700M | grep "[0-9]")

#TARGET_JOB_ID=$(echo "naseq_server.py --spawn-server-blocking" | qsub -V -m ae -W umask=0002 -A wst-164-ab -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N bplan-srv -l walltime=6:00:0 -q scalemp -l nodes=1:ppn=12 | grep "[0-9]")

echo $TARGET_JOB_ID

