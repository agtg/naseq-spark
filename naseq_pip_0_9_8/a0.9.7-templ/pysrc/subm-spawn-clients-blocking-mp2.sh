#!/bin/sh


WORK_DIR=`pwd`
JOB_OUTPUT_ROOT=$WORK_DIR/job_output

TARGET_JOB_ID=$(echo "naseq_server.py --spawn-clients-blocking 12" | qsub -V -m ae -W umask=0002 -A wst-164-ab -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N a002-cl -l walltime=32:00:0 -q qwork@mp2 -l nodes=1 | grep "[0-9]")

echo $TARGET_JOB_ID

