#!/bin/sh


WORK_DIR=`pwd`
JOB_OUTPUT_ROOT=$WORK_DIR/job_output

TARGET_JOB_ID=$(echo "naseq_server.py --spawn-clients-blocking 12 1700" | qsub -V -m ae -W umask=0002 -A wst-164-ab -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT \
-N a028-cl -l walltime=4:00:0 -q metaq -l nodes=1:ppn=12,pmem=1700M | grep "[0-9]")

echo $TARGET_JOB_ID

