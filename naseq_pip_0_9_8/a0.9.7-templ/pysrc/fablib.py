#!/usr/bin/env python

import psutil
from fabric.api import *
from fabric.contrib.console import confirm
import time

from fabric.context_managers import settings



def showHostnameSerial():
    p = psutil.Process(30110)
    print(p.cpu_times())
    print(psutil.cpu_times())
    time.sleep(10)
    print(p.cpu_times())

    #cmd = "hostname"
    #if run(cmd).failed:
    #    return True
    #return False

def showHostnameParallel():
    cmd = "hostname"
    if sudo(cmd).failed:
        return True
    return False

def main(): 
    global env
    env.host_string = "sw-2r13-n37"
    code_dir = '/gs/project/wst-164-ab/exome/dac-901/pysrc'
    with cd(code_dir):
        run("./check_work.py")
    
    #results = execute(showHostnameSerial)
#    results = execute(showHostnameSerial, hosts=_HOSTS)
#    with settings(host_string='lm-2r01-n75'):
#        results = execute(showHostnameSerial)
    #results = local(showHostnameSerial, capture=True)

#    print("results: ", results)
#    results = execute(showHostnameParallel, hosts=_HOSTS)
#    print results


main()


from fabric.state import connections

for key in connections.keys():
    connections[key].close()
    del connections[key]
