#!/bin/sh


WORK_DIR=`pwd`
JOB_OUTPUT_ROOT=$WORK_DIR/job_output

TARGET_JOB_ID=$(echo "naseq_server.py --spawn-clients-blocking 8 1700" | qsub -V -m ae -W umask=0002 -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N a00ht-cl -l walltime=6:00:0 -q sw -l nodes=1:ppn=8 | grep "[0-9]")

echo $TARGET_JOB_ID

