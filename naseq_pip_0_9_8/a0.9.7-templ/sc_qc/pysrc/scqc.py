#!/usr/bin/env python3

import gc

import os
os.environ['MPLBACKEND'] = 'agg'

import subprocess

# import tables

import itertools

import pandas as pd
import numpy as np
import pylab
import re


import scipy.stats as stats
import scipy

# load statsmodels as alias ``sm``
import statsmodels.api as sm


import matplotlib
# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')


import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib.backends.backend_pdf import PdfPages
# from matplotlib.backends.backend_svg import SVG
from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import ScalarFormatter


def load_one_module(module_name):

    cmd = r"""/home/dbadescu/local/bin/modulecmd python load %s""" % module_name
    cmd_proc = os.popen(cmd)
    exec(cmd_proc)



class ScQc(object):

    def __init__(self):

        # self.project_root = "/gs/project/wst-164-ab/share/rnvar/a012-x040/sc_qc"
        self.project_root = "/gs/project/wst-164-ab/share/expr/a018-x046/sc_qc"

        # use indexes exactly as in file + displacement
        # self.datasets = { "e1-gn-cn-norm": ["vmp-e1-gn-cn.csv", 0, 32663-1],
        #                   "e1-gn-cn-ercc": ["vmp-e1-gn-cn.csv", 32663-1, 32758-1],
        #                   "e2-gn-cn-norm": ["su-e2-gn-cn.csv", 0, 32663-1],
        #                   "e2-gn-cn-ercc": ["su-e2-gn-cn.csv", 32663-1, 32758-1]
        #     ,
        #                   }

        # these limits are gtf dependent
        self.datasets = {
            "a018-gn-cn-norm": ["a018-gn-cn.csv", 0, 63678-1],
            "a018-gn-cn-ercc": ["a018-gn-cn.csv", 63678-1, 63773-1]
                  # "p9-rn-vl-norm": ["p9-rn-vl.csv", 0, 7683],
                  # "p8-gn-cn-norm": ["p8-gn-cn.csv", 0, 27579],
                  # "p8-gn-cn-ercc": ["p8-gn-cn.csv", 27579, 27657],
                  # "p3-gn-cn-norm": ["p3-gn-cn.csv", 0, 27552],
                  # "p2-gn-cn-norm": ["p2-gn-cn.csv", 0, 24703],
                  # "hc-dn-vl-p4-p5": ["bab-HC.csv", 0, 124635],
                  # "hc-dn-vl-p4-p5-cosmic-32": ["hc-dn-vl-p4-p5-cosmic-32.csv", 0, 32]
                  }

        # self.smp_idx = None
        # self.region_idx = None
        #
        # self.spln_idx = 0
        # self.rprt_idx = None
        # self.smp_idx = None
        #
        # self.spln_hplex_qc = {}



    def read_expr(self, file):

        print("file: ", file)

        expr = pd.read_csv(file, sep=',', header=0
                                     # index_col=[0]
                                     # usecols=[0, 1],
                                     # names=["transcript_id", "ex"]
                              )
        # filter zeros
        # expr = expr.query('ex != 0.0')

        # print("expr: \n", expr.iloc[:,0:3])

        return expr

    @staticmethod
    def mylogavg(a):
        """Average first and last element of a 1-D array"""
        # return (a[0] + a[-1]) * 0.5
        return np.log2(np.average(a+1))

    @staticmethod
    def myavg(a):
        """Average first and last element of a 1-D array"""
        # return (a[0] + a[-1]) * 0.5
        return np.average(a)


    def stats_targ_graph(self):

        df_ = pd.read_csv(self.targ_stats_csv, sep=',', header=0,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6],
                          names=['idx', 'sample', 'on_unique', 'on_multiple', 'off_unique', 'off_multiple', 'unmapped'])

        print("df_: ", df_)

        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width)
        pd.options.display.mpl_style = 'default'
        # with PdfPages("../metrics2/qc-targ-%s.pdf" % self.conf.proj_abbrev) as pdf:

        plt.figure(figsize=[6, 6])
        # plt.figure()
        # plt.set_dpi(100)

        ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))
        legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

        xlabels = ax.get_xticklabels()
        for i in range(len(xlabels)):
            label = xlabels[i].get_text()
            print("i: ", i, " label: ", label)
            raw = df_.ix[i, "sample"]
            print("row: ", raw)
            xlabels[i] = raw
            # print(i, xlabels[i])

        # labels = [item.get_text() for item in ax.get_xticklabels()]
        # labels[1] = 'Testing'
        # ax.set_xticklabels(labels)


        # ax.set_xticklabels(["" for x in xlabels])
        # print("xlabels: ", xlabels.inspect)
        ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
        # for label in labels:
        #     label.set_rotation(45)
        #     label.set_fontsize(10)

        # for k in ax.get_xmajorticklabels():
        #     if some-condition:
        #         k.set_color(any_colour_you_like)

        plt.title('Target coverage',  fontsize= 22)
        plt.subplots_adjust(top=0.95, bottom=0.20, left=0.3, right=0.99, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()


        # pdf.savefig()  # saves the current figure into a pdf page
        plt.savefig("../metrics2/qc-targ-%s.svg" % self.conf.proj_abbrev, format="svg")  # saves the current figure into a pdf page

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()

    def stats_targ_graph2(self):

        df_ = pd.read_csv(self.targ_stats_csv, sep=',', header=0,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6],
                          names=['idx', 'sample', 'on_unique', 'on_multiple', 'off_unique', 'off_multiple', 'unmapped'])

        print("df_: ", df_)

        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width)
        pd.options.display.mpl_style = 'default'
        with PdfPages("../metrics2/qc-targ-%s.pdf" % self.conf.proj_abbrev) as pdf:
            plt.figure()
            # plt.set_dpi(100)

            ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))
            legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

            xlabels = ax.get_xticklabels()
            for i in range(len(xlabels)):
                label = xlabels[i].get_text()
                print("i: ", i, " label: ", label)
                raw = df_.ix[i, "sample"]
                print("row: ", raw)
                xlabels[i] = raw
                # print(i, xlabels[i])

            # labels = [item.get_text() for item in ax.get_xticklabels()]
            # labels[1] = 'Testing'
            # ax.set_xticklabels(labels)


            # ax.set_xticklabels(["" for x in xlabels])
            # print("xlabels: ", xlabels.inspect)
            ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
            # for label in labels:
            #     label.set_rotation(45)
            #     label.set_fontsize(10)

            # for k in ax.get_xmajorticklabels():
            #     if some-condition:
            #         k.set_color(any_colour_you_like)

            plt.title('Target coverage',  fontsize= 22)
            plt.subplots_adjust(top=0.95, bottom=0.20, left=0.3, right=0.99, hspace = 0.1, wspace = 0.2)
            # plt.tight_layout()


            pdf.savefig()  # saves the current figure into a pdf page

            # plt.title('Page Two')
            # pdf.savefig()  # saves the current figure into a pdf page

            plt.close()

    def plot_fig_2b(self):

        # pd.options.display.mpl_style = 'default'
        # plt.style.use('ggplot')

        # The grid we'll use for plotting
        x_grid = np.linspace(0, 1.1, 1000)

        # Plot the three kernel density estimates
        # fig, ax = plt.subplots(1, 4, sharey=True,
        #                        figsize=(13, 3))
        # fig.subplots_adjust(wspace=0)


        pdf = self.kde_scipy(self.fig2b_serie, x_grid) #,, bandwidth=0.2



        fig = plt.figure()
        # plt.set_dpi(100)
        # fig.subplots_adjust(bottom=0.1)
        ax = fig.add_subplot(111)
        ax.plot(x_grid, pdf, color='blue', alpha=0.5, lw=1)
        # ax[i].fill(x_grid, pdf_true, ec='gray', fc='gray', alpha=0.4)
        ax.set_title("kde_scipy")
        ax.set_xlim(0, 1.1)

        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        # axis
        ax.get_xaxis().set_tick_params(direction='in')
        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.tick_params(which='both', width=1)
        plt.tick_params(which='major', length=10)
        plt.tick_params(which='minor', length=4, color='gray')
        ax.yaxis.set_tick_params(direction='in')
        ax.yaxis.set_tick_params(which='both', width=1)
        ax.yaxis.set_tick_params(which='major', length=10)
        ax.get_yaxis().set_tick_params(which='minor', length=4, color='gray')





        # bbox={'facecolor':'black', 'alpha':0.5, 'pad':10}
        # ax.text(1, np.amax(self.y_cols_avg) - 1, "Pearson Correlation %s" % round(pearsonr, 2), style='italic', fontsize= 5)
        # ax.text(1, np.amax(self.y_cols_avg) - 2, "Spearman Correlation %s" % round(spearmanr, 2), style='italic', fontsize= 5)

        # colors = np.random.rand(nlen)


        # area = np.pi * (2 * np.random.rand(nlen))**2  # 0 to 15 point radiuses
        # area = 2


        # results = sm.OLS(self.y_cols_avg, sm.add_constant(self.x_cols_avg)).fit()

        # print(results.summary())

        # fit = np.polyfit(self.x_cols_avg, self.y_cols_avg, deg=1)
        # print("fit: ", fit)
        # fity = fit[0] * self.x_cols_avg + fit[1]
        # print("fity: ", fity)

        # ax.plot(self.x_cols_avg, fity, linestyle='-', c='blue', linewidth=0.5, alpha=0.4)
        # ax.plot(datax, fity)


        # ax.scatter(self.x_cols_avg, self.y_cols_avg, marker='o', c='red', s=4, alpha=0.5)


        # plt.axis([0, 1.1*np.amax(self.x_cols_avg), 0, 1.1*np.amax(self.y_cols_avg)])


        # plt.grid(True, color='w', linestyle='-', linewidth=1)
        # plt.gca().patch.set_facecolor('0.8')
        # plt.savefig("../figs/fig2b-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page

        plt.savefig("../figs/fig2b-%s.pdf" % self.anal_name, format="pdf")  # saves the current figure into a pdf page
        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()

    def plot_fig_2d(self):

        # pd.options.display.mpl_style = 'default'
        # plt.style.use('ggplot')

        # The grid we'll use for plotting
        x_grid = np.linspace(0, 1.1, 1000)

        # Plot the three kernel density estimates
        # fig, ax = plt.subplots(1, 4, sharey=True,
        #                        figsize=(13, 3))
        # fig.subplots_adjust(wspace=0)


        self.fig2d_serie = self.fig2d_serie[np.isfinite(self.fig2d_serie)]
        print("self.fig2d_seri: ", self.fig2d_serie)

        # exit(0)

        pdf = self.kde_scipy(self.fig2d_serie, x_grid) #,, bandwidth=0.2



        fig = plt.figure()
        # plt.set_dpi(100)
        # fig.subplots_adjust(bottom=0.1)
        ax = fig.add_subplot(111)
        ax.plot(x_grid, pdf, color='blue', alpha=0.5, lw=1)
        # ax[i].fill(x_grid, pdf_true, ec='gray', fc='gray', alpha=0.4)
        ax.set_title("kde_scipy")
        ax.set_xlim(0, 1.1)

        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)

        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        ax.get_xaxis().set_tick_params(direction='in')

        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))

        plt.tick_params(which='both', width=1)
        plt.tick_params(which='major', length=10)
        plt.tick_params(which='minor', length=4, color='gray')

        ax.yaxis.set_tick_params(direction='in')
        ax.yaxis.set_tick_params(which='both', width=1)
        ax.yaxis.set_tick_params(which='major', length=10)
        ax.get_yaxis().set_tick_params(which='minor', length=4, color='gray')



        # plt.grid(True, color='w', linestyle='-', linewidth=1)
        # plt.gca().patch.set_facecolor('0.8')
        # plt.savefig("../figs/fig2d-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/fig2d-%s.pdf" % self.anal_name, format="pdf")  # saves the current figure into a pdf page

        plt.close()



    def plot_fig_2a(self):

        # figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')

        # fig_width = 8 + round(self.nb_samples / 3, 2)
        # print("fig_width: ", fig_width)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use('ggplot')


        pearsonr = stats.pearsonr(self.x_cols_avg, self.y_cols_avg)[0]
        spearmanr = stats.spearmanr(self.x_cols_avg, self.y_cols_avg)[0]

        print("pearsonr, spearmanr: ", pearsonr, spearmanr)

        # with SVGPages("../figs/fig2a-%s.pdf" % self.anal_name) as pdf:


        fig = plt.figure()
        # plt.set_dpi(100)
        # fig.subplots_adjust(bottom=0.1)
        ax = fig.add_subplot(111)
        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        # bbox={'facecolor':'black', 'alpha':0.5, 'pad':10}
        ax.text(1, np.amax(self.y_cols_avg) - 1, "Pearson Correlation %s" % round(pearsonr, 2), style='italic', fontsize= 10)
        ax.text(1, np.amax(self.y_cols_avg) - 2, "Spearman Correlation %s" % round(spearmanr, 2), style='italic', fontsize= 10)

        # colors = np.random.rand(nlen)


        # area = np.pi * (2 * np.random.rand(nlen))**2  # 0 to 15 point radiuses
        area = 2

        # X = np.random.rand(100)
        # Y = X + np.random.rand(100)*0.1

        results = sm.OLS(self.y_cols_avg, sm.add_constant(self.x_cols_avg)).fit()

        print(results.summary())

        # plt.scatter(datax, datay, s=area, c=colors, alpha=0.5)

        # X_plot = np.linspace(0, 15, 100)
        # plt.plot(X_plot, X_plot*results.params[0] + results.params[1])

        fit = np.polyfit(self.x_cols_avg, self.y_cols_avg, deg=1)
        print("fit: ", fit)
        fity = fit[0] * self.x_cols_avg + fit[1]
        print("fity: ", fity)

        ax.plot(self.x_cols_avg, fity, linestyle='-', c='blue', linewidth=2, alpha=0.4)
        # ax.plot(datax, fity)


        ax.scatter(self.x_cols_avg, self.y_cols_avg, marker='o', c='red', s=4, alpha=0.6)


        plt.axis([0, 1.1*np.amax(self.x_cols_avg), 0, 1.1*np.amax(self.y_cols_avg)])

        # axis
        ax.get_xaxis().set_tick_params(direction='in')
        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.tick_params(which='both', width=1)
        plt.tick_params(which='major', length=10)
        plt.tick_params(which='minor', length=4, color='gray')
        ax.yaxis.set_tick_params(direction='in')
        ax.yaxis.set_tick_params(which='both', width=1)
        ax.yaxis.set_tick_params(which='major', length=10)
        ax.get_yaxis().set_tick_params(which='minor', length=4, color='gray')

        # plt.tick_params(
    # axis='x',          # changes apply to the x-axis
    # which='both',      # both major and minor ticks are affected
    # bottom='off',      # ticks along the bottom edge are off
    # top='off',         # ticks along the top edge are off
    # labelbottom='off') # labels along the bottom edge are off

        # ax.get_xaxis().set_tick_params(top='on', direction='in')
        # ax.get_xaxis().set_tick_params(bottom='on', direction='out')


        # ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))

        # legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

        # xlabels = ax.get_xticklabels()
        # for i in range(len(xlabels)):
        #     label = xlabels[i].get_text()
        #     print("i: ", i, " label: ", label)
        #     raw = df_.ix[i, "sample"]
        #     print("row: ", raw)
        #     xlabels[i] = raw
            # print(i, xlabels[i])

        # labels = [item.get_text() for item in ax.get_xticklabels()]
        # labels[1] = 'Testing'
        # ax.set_xticklabels(labels)


        # ax.set_xticklabels(["" for x in xlabels])
        # print("xlabels: ", xlabels.inspect)
        # ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
        # for label in labels:
        #     label.set_rotation(45)
        #     label.set_fontsize(10)

        # for k in ax.get_xmajorticklabels():
        #     if some-condition:
        #         k.set_color(any_colour_you_like)

        # plt.grid(True, color='w', linestyle='-', linewidth=1)
        # plt.gca().patch.set_facecolor('0.8')
        plt.savefig("../figs/fig2a-%s.png" % self.anal_name, format="png", dpi=400)  # saves the current figure into a pdf page
        # plt.savefig("../figs/fig2a-%s.pdf" % self.anal_name, format="pdf")  # saves the current figure into a pdf page

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()

    def extract_dataset(self):

        # extract dataset information
        self.dtst_file, self.dtst_min_row, self.dtst_max_row = self.datasets[self.dtst_name]

        self.edf = self.read_expr(os.path.join(self.project_root, "files", self.dtst_file))

        print("self.edf.shape: ", self.edf.shape)


        self.edf2 = self.edf.query(r"""index >= %s & index <%s""" % (self.dtst_min_row, self.dtst_max_row))
        self.edf2_len = self.edf2.shape[0]

        # print("edf2: ", self.edf2.iloc[:,0:6])
        # print("edf2: ", self.edf2)
        # exit(0)

    # https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
    def kde_scipy(self, x, x_grid, bandwidth=0.2, **kwargs):
        """Kernel Density Estimation with Scipy"""
        # Note that scipy weights its bandwidth by the covariance of the
        # input data.  To make the results comparable to the other methods,
        # we divide the bandwidth by the sample standard deviation here.
        # kde = stats.gaussian_kde(x, bw_method=bandwidth / x.std(ddof=1), **kwargs)
        kde = stats.gaussian_kde(x, bw_method=bandwidth, **kwargs)
        return kde.evaluate(x_grid)


    def prepare_fig2b(self):

        # self.x_cols_df = self.edf2.iloc[:, self.x_col_range]
        #
        # print("self.x_cols_df: ", self.x_cols_df.shape, self.x_cols_df.iloc[0:5, :200])
        # exit(0)

        self.fig2b_serie = np.empty(shape=0)
        for i in self.x_col_range:
            for j in self.x_col_range:
                if j > i:
                    print("i, j: ", i, j)

                    self.i_cols_arr = self.edf2.iloc[:, i].values
                    self.j_cols_arr = self.edf2.iloc[:, j].values
                    print("self.i_cols_arr: \n", self.i_cols_arr)
                    print("self.j_cols_arr: \n", self.j_cols_arr)
                    pearsonr = stats.pearsonr(self.i_cols_arr, self.j_cols_arr)[0]
                    self.fig2b_serie = np.append(self.fig2b_serie, [pearsonr])

        print("self.fig2b_serie: ", self.fig2b_serie)




                    # self.x_cols_avg = np.apply_along_axis(self.mylogavg, 1, self.x_cols_arr)
                    # print("x_cols_avg: ", self.x_cols_avg)

        # y = pd.DataFrame({'yavg' : self.y_cols_avg})  # response
        # X = pd.DataFrame({'xavg' : self.x_cols_avg})  # predictor
        # X = sm.add_constant(X)  # Adds a constant term to the predictor
        # print(X.head())
        # est = sm.OLS(y, X)
        # est = est.fit()
        # print("est.summary: \n", est.summary())
        # print("est.params: \n", est.params)

    def prepare_fig2d(self):

        self.x_cols_df = self.edf2.iloc[:, self.x_col_range]
        #
        # print("self.x_cols_df: ", self.x_cols_df.shape, self.x_cols_df.iloc[0:5, :200])

        # exit(0)

        self.fig2d_serie = np.empty(shape=0)
        for i in self.x_col_range:
            for j in self.x_col_range:
                if j > i:
                    print("i, j: ", i, j)

                    ia = np.copy(self.edf2.iloc[:, i].values)
                    ja = np.copy(self.edf2.iloc[:, j].values)

                    # self.i_cols_arr = self.edf2.iloc[:, i].values
                    # self.j_cols_arr = self.edf2.iloc[:, j].values

                    print("ia before: \n", ia, ia.shape)
                    print("ja before: \n", ja, ja.shape)

                    # change to na = -1, 0/1/2
                    ia[ia == 2] = -1
                    ia[ia == 3] = 2

                    ca = ia >0
                    print("ca: ", ca)
                    # ib
                    ja[ja == 2] = -1
                    ja[ja == 3] = 2



                    print("ia: \n", ia, ia.shape)
                    print("ja: \n", ja, ja.shape)

                    z = np.zeros(ia.shape)
                    print("z: \n", z, z.shape)

                    ci = ia>0
                    cj = ja>0

                    z2 = np.logical_and(ci,cj)
                    z3 = np.sum(z2)

                    # denominators
                    cond_den_i = ia >= 0
                    cond_den_j = ja >= 0

                    den2 = np.logical_and(cond_den_i, cond_den_j)
                    den3 = np.sum(den2)
                    quot = z3/den3
                    print("z3: \n", z3, den3, quot)




                    # exit(0)
                    # pearsonr = stats.pearsonr(self.i_cols_arr, self.j_cols_arr)[0]
                    self.fig2d_serie = np.append(self.fig2d_serie, [quot])

        print("self.fig2d_serie: ", self.fig2d_serie)


    @staticmethod
    def reg_m2(y, x):
        # x = np.array(x).T
        # x = x.T
        # print("x:, \n", x, x.shape )
        x = sm.add_constant(x)
        # print("x:, \n", x, x.shape )

        # print("y:, \n", y, y.shape )

        results = sm.OLS(endog=y, exog=x).fit()
        return results

    def prepare_fig2c(self):


        columns = ['idx', 'name', 'q1', 'q2', 'q3', 'q4', 'q5']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        dtypes = {'idx': 'int', 'name': 'object',
                  'q1': 'float64', 'q2': 'float64', 'q3': 'float64',
                  'q4': 'float64', 'q5': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)

        # debug min row
        self.y_cols_df = self.edf2.iloc[:, self.y_col_range]

        # we have transpose range names for the multiliner regression,
        # x is on the y axis (controls)
        # y is on x axis (single cells)

        self.y_cols_arr = self.y_cols_df.values
        print("y_cols_arr: \n", self.y_cols_arr)

        # blk_arr = np.zeros(edf2_len, dtype="float")
        # create bulk median column
        # print("blk_arr: ", blk_arr)

        self.y_cols_avg = np.apply_along_axis(self.myavg, 1, self.y_cols_arr)
        self.y_cols_avg = np.expand_dims(self.y_cols_avg, axis=1)
        print("y_cols_avg: \n", self.y_cols_avg, self.y_cols_avg.shape )

        # X_pred = pd.DataFrame({'xavg' : self.y_cols_avg})  # predictor

        # X_pred = sm.add_constant(X_pred)  # Adds a constant term to the predictor
        # print("X_pred: \n", X_pred)


        min_col_range = min(self.x_col_range)
        max_col_range = max(self.x_col_range)
        print("min_col_range, max_col_range: ", min_col_range, max_col_range)

        nb_sim = 300
        cnt = 0
        # for scsize in [5, 10, 15, 20, 25, (max_col_range - min_col_range +1)]:
        for scsize in range(1, max_col_range - min_col_range +2):
            fig2c_serie = np.empty(shape=0)
            print("nb_sim, cnt: ", nb_sim, cnt)
            for i in range(nb_sim):

                # rand_range = np.random.randint(min_col_range, max_col_range + 1, size= scsize)

                rand_range = np.random.choice(self.x_col_range, size= scsize, replace=False)

                self.x_cols_arr = self.edf2.iloc[:, rand_range].values
                # print("x_cols_arr: \n", self.x_cols_arr, self.x_cols_arr.shape)

                results = self.reg_m2(self.y_cols_avg, self.x_cols_arr)
                # print(results.summary())

                # print("rand_range, R2: ", rand_range, results.rsquared)
                fig2c_serie = np.append(fig2c_serie, [results.rsquared])

            # print("fig2c_serie: ", fig2c_serie)

            qmed = np.percentile(fig2c_serie, 50)
            q25 = np.percentile(fig2c_serie, 25)
            q75 = np.percentile(fig2c_serie, 75)

            wisk_hi = np.max(fig2c_serie)
            wisk_lo = np.min(fig2c_serie)

            # whis = 1.5
            # get high extreme
            # iq = q75 - q25
            # hi_val = q75 + whis * iq
            # wisk_hi = np.compress(fig2c_serie <= hi_val, fig2c_serie)
            #
            # if len(wisk_hi) == 0 or np.max(wisk_hi) < q75:
            #     wisk_hi = q75
            # else:
            #     wisk_hi = max(wisk_hi)

            # get low extreme
            # lo_val = q25 - whis * iq
            # wisk_lo = np.compress(fig2c_serie >= lo_val, fig2c_serie)
            # if len(wisk_lo) == 0 or np.min(wisk_lo) > q25:
            #     wisk_lo = q25
            # else:
            #     wisk_lo = min(wisk_lo)


            # print(wisk_lo, q25, qmed, q75, wisk_hi)


            # Setting With Enlargement
            df_.loc[len(df_)+1] = [cnt, "%s" % scsize, wisk_lo, q25, qmed, q75, wisk_hi]
            cnt += 1

        df_.set_index(['idx'], inplace=True)
        df_.sort_index(inplace=True)
        print("df_: \n", df_)
        df_.to_csv("../figs/fig2c-%s.csv" % self.anal_name,
                           sep=',', header=True
                           ,index=True
                           ,index_label="idx"
                           )



    # based on eSNV
    def prepare_fig2e(self):


        columns = ['idx', 'name', 'q1', 'q2', 'q3', 'q4', 'q5']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        dtypes = {'idx': 'int', 'name': 'object',
                  'q1': 'float64', 'q2': 'float64', 'q3': 'float64',
                  'q4': 'float64', 'q5': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)



        # debug min row
        self.y_cols_df = self.edf2.iloc[:, self.y_col_range]
        # print("self.y_cols_df: ", self.y_cols_df)
        # exit(0)

        # only for debugging
        # self.x_cols_df = self.edf2.iloc[:, self.x_col_range]
        # print("self.x_cols_df: ", self.x_cols_df.iloc[:,0:3])
        # exit(0)


        # we have transpose range names for the multiliner regression,
        # x is on the y axis (controls)
        # y is on x axis (single cells)

        self.y_cols_arr = self.y_cols_df.values
        # print("y_cols_arr: \n", self.y_cols_arr)

        ja = np.copy(self.y_cols_arr)

        # print("ja before: \n", ja, ja.shape)
        # change to na = -1, 0/1/2
        ja[ja == 2] = -1
        ja[ja == 3] = 2

        # print("ja after: \n", ja, ja.shape)

        jcond = ja > 0

        # print("jcond after: \n", jcond, jcond.shape)

        jan = np.any(jcond, axis=1)
        jan_len = np.sum(jan)

        # print("jan : \n", jan, jan.shape, np.sum(jan))

        # jden = np.any(ja >= 0, axis=1)
        # jden_len = np.sum(jden)
        # print("jden : \n", jden, jden.shape, jden_len)


        # --- find the notfound

        # jc = np.copy(self.edf2.iloc[:, self.x_col_range].values)

        # print("x_cols_arr: \n", self.x_cols_arr, self.x_cols_arr.shape)

        # --------------------------------------
        # print("jb before: \n", jb, jb.shape)
        # change to na = -1, 0/1/2
        # jc[jc == 2] = -1
        # jc[jc == 3] = 2

        # print("jc after: \n", jc, jc.shape)

        # exit(0)

        # jcond = jb > 0
        # print("jcond after: \n", jcond, jcond.shape)

        # jcn = np.all(jc <= 0, axis=1)
        # print("jcn: \n", jcn, jcn.shape, np.sum(jcn))

        # exit(0)

        # fnd = np.logical_and(jbn, jan)
        # fnd_len = np.sum(fnd)
        # print("fnd: \n", fnd, fnd.shape, fnd_len)




        # ----------------------------


        # exit(0)

        # ca = ia >0
        # print("ca: ", ca)
        # ib
        # ja[ja == 2] = -1
        # ja[ja == 3] = 2

        # print("ia: \n", ia, ia.shape)
        # print("ja: \n", ja, ja.shape)

        # z = np.zeros(ia.shape)
        # print("z: \n", z, z.shape)

        # ci = ia>0
        # cj = ja>0

        # z2 = np.logical_and(ci,cj)
        # z3 = np.sum(z2)

        # denominators
        # cond_den_i = ia >= 0
        # cond_den_j = ja >= 0

        # den2 = np.logical_and(cond_den_i, cond_den_j)
        # den3 = np.sum(den2)
        # quot = z3/den3
        # print("z3: \n", z3, den3, quot)
        # exit(0)

        # blk_arr = np.zeros(edf2_len, dtype="float")
        # create bulk common column
        # print("blk_arr: ", blk_arr)

        # self.y_cols_avg = np.apply_along_axis(self.myavg, 1, self.y_cols_arr)
        # self.y_cols_avg = np.expand_dims(self.y_cols_avg, axis=1)
        # print("y_cols_avg: \n", self.y_cols_avg, self.y_cols_avg.shape )

        # X_pred = pd.DataFrame({'xavg' : self.y_cols_avg})  # predictor

        # X_pred = sm.add_constant(X_pred)  # Adds a constant term to the predictor
        # print("X_pred: \n", X_pred)


        min_col_range = min(self.x_col_range)
        max_col_range = max(self.x_col_range)
        # print("min_col_range, max_col_range: ", min_col_range, max_col_range)

        nb_sim = 300
        cnt = 0
        # for scsize in [5, 10, 15, 20, 25, (max_col_range - min_col_range +1)]:
        for scsize in range(1, max_col_range - min_col_range +2):
        # for scsize in [(max_col_range - min_col_range +1)]:

            fig2e_serie = np.empty(shape=0)
            print("nb_sim, cnt: ", nb_sim, cnt)
            for i in range(nb_sim):

                # rand_range = np.random.randint(min_col_range, max_col_range + 1, size= scsize)

                rand_range = np.random.choice(self.x_col_range, size= scsize, replace=False)

                self.x_cols_arr = self.edf2.iloc[:, rand_range].values
                # print("x_cols_arr: \n", self.x_cols_arr, self.x_cols_arr.shape)

                # print("x_cols_arr: \n", self.x_cols_arr[0:16, 0])
                # print("x_cols_arr: \n", self.x_cols_arr[0:16, 0])
                # print("x_cols_arr: \n", self.x_cols_arr[0:16, 31])
                # exit(0)

                # --------------------------------------
                jb = np.copy(self.x_cols_arr)
                # print("jb before: \n", jb, jb.shape)
                # change to na = -1, 0/1/2
                jb[jb == 2] = -1
                jb[jb == 3] = 2

                # print("jb after: \n", jb, jb.shape)

                # exit(0)

                # jcond = jb > 0
                # print("jcond after: \n", jcond, jcond.shape)

                jbn = np.any(jb > 0, axis=1)
                # print("jbn: \n", jbn, jbn.shape, np.sum(jbn))

                fnd = np.logical_and(jbn, jan)
                fnd_len = np.sum(fnd)
                # print("fnd: \n", fnd, fnd.shape, fnd_len)


                # quot = fnd_len/ jden_len
                quot = fnd_len / jan_len

                # print("quot: \n", quot)



                # -------------------------------------


                # exit(0)

                # results = self.reg_m2(self.y_cols_avg, self.x_cols_arr)
                # print(results.summary())

                # print("rand_range, R2: ", rand_range, results.rsquared)
                fig2e_serie = np.append(fig2e_serie, [quot])

            # print("fig2c_serie: ", fig2c_serie)

            qmed = np.percentile(fig2e_serie, 50)
            q25 = np.percentile(fig2e_serie, 25)
            q75 = np.percentile(fig2e_serie, 75)

            wisk_hi = np.max(fig2e_serie)
            wisk_lo = np.min(fig2e_serie)

            # Setting With Enlargement
            df_.loc[len(df_)+1] = [cnt, "%s" % scsize, wisk_lo, q25, qmed, q75, wisk_hi]
            cnt += 1

        df_.set_index(['idx'], inplace=True)
        df_.sort_index(inplace=True)

        # df_.sort(['sample'], ascending=[1], inplace=True)
        # df_ = df_.reset_index(drop=True)
        # df_= df_.set_index(["sample"])
        print("df_: \n", df_)


        df_.to_csv("../figs/fig2e-%s.csv" % self.anal_name,
                           sep=',', header=True
                           ,index=True
                           ,index_label="idx"
                           )


    @staticmethod
    def roundup(num):
        # x = Math.log10(num).floor
        # num=(num/(10.0**x)).ceil*10**x

        x0 = np.array([num])

        x = np.floor(np.log10(x0))[0]
        y = (np.ceil(x0/(10.0**x))*10**x)[0]


        print("num, x, y : ", num, x, y )


        return y

    @staticmethod
    def roundown(num):
        # x = Math.log10(num).floor
        # num=(num/(10.0**x)).ceil*10**x

        x0 = np.array([num])

        x = np.floor(np.log10(x0))[0]
        y = (np.floor(x0/(10.0**x))*10**x)[0]


        print("num, x, y : ", num, x, y )


        return y




    def customized_box_plot_horiz(self, perc_df, axes, redraw = True, *args, **kwargs):
        """
        Generates a customized boxplot based on the given percentile values
        """
        n_box = perc_df.shape[0]
        print("n_box: ", n_box)

        box_plot = axes.boxplot([[-9, -4, 2, 4, 9],]*n_box, vert=0, *args, **kwargs)
        min_x = perc_df['q1'].min()
        # min_y = 0
        min_x = self.roundown(min_x)

        max_x = perc_df['q5'].max()

        # min_y = roundup(min_y)
        max_x = self.roundup(max_x)

        # The y axis is rescaled to fit the new box plot completely with 10%
        # of the maximum value at both ends
        # axes.set_ylim([min_y*0.95, max_y*1.05])
        axes.set_xlim([min_x, max_x])



        xticks = np.linspace(min_x, max_x, num=11, endpoint=True)
        axes.set_xticks(xticks, minor=True)

        for box_no in range(n_box):

            print("box_no: ", box_no)
            q1_start = perc_df.at[box_no, 'q1']
            q2_start = perc_df.at[box_no, 'q2']
            q3_start = perc_df.at[box_no, 'q3']
            q4_start = perc_df.at[box_no, 'q4']
            q4_end =  perc_df.at[box_no, 'q5']
            fliers_xy = None
            print("q1_start: ", q1_start)

            # Lower cap
            box_plot['caps'][2*box_no].set_xdata([q1_start, q1_start])
            # xdata is determined by the width of the box plot

            # Lower whiskers
            box_plot['whiskers'][2*box_no].set_xdata([q1_start, q2_start])

            # Higher cap
            box_plot['caps'][2*box_no + 1].set_xdata([q4_end, q4_end])

            # Higher whiskers
            box_plot['whiskers'][2*box_no + 1].set_xdata([q4_start, q4_end])

            # Box
            box_plot['boxes'][box_no].set_xdata([q2_start,
                                                 q2_start,
                                                 q4_start,
                                                 q4_start,
                                                 q2_start])


            # Median
            box_plot['medians'][box_no].set_xdata([q3_start, q3_start])

            #plt.setp(box_plot['boxes'], color='blue')
            plt.setp(box_plot['medians'], color='red', linewidth=1.5)

            #plt.setp(bp['whiskers'], color='black')
            #plt.setp(bp['fliers'], color='red', marker='+')



        return box_plot


    def customized_box_plot(self, perc_df, axes, redraw = True, *args, **kwargs):
        """
        Generates a customized boxplot based on the given percentile values
        """
        n_box = perc_df.shape[0]
        print("n_box: ", n_box)

        box_plot = axes.boxplot([[-9, -4, 2, 4, 9],]*n_box, *args, **kwargs)
        # Creates len(percentiles) no of box plots


        # min_y, max_y = float('inf'), -float('inf')

        # all_vals = np.unique(perc_df[['q1', 'q2', 'q3', 'q4', 'q5']].values.ravel())

        # print("all_vals: \n", all_vals)

        min_y = perc_df['q1'].min()
        # min_y = 0
        min_y = self.roundown(min_y)

        max_y = perc_df['q5'].max()

        # min_y = roundup(min_y)
        max_y = self.roundup(max_y)

        # The y axis is rescaled to fit the new box plot completely with 10%
        # of the maximum value at both ends
        # axes.set_ylim([min_y*0.95, max_y*1.05])
        axes.set_ylim([min_y, max_y])



        yticks = np.linspace(min_y, max_y, num=11, endpoint=True)
        axes.set_yticks(yticks, minor=False)


        # box_no=0
        for box_no in range(n_box):
            # ndex,value in enumerate(percentiles):
            #do stuff with array[n]


            print("box_no: ", box_no)
            q1_start = perc_df.at[box_no, 'q1']
            q2_start = perc_df.at[box_no, 'q2']
            q3_start = perc_df.at[box_no, 'q3']
            q4_start = perc_df.at[box_no, 'q4']
            q4_end =  perc_df.at[box_no, 'q5']
            fliers_xy = None
            print("q1_start: ", q1_start)



        # for box_no, (q1_start,
        #              q2_start,
        #              q3_start,
        #              q4_start,
        #              q4_end,
        #              fliers_xy) in enumerate(percentiles):

            # Lower cap
            box_plot['caps'][2*box_no].set_ydata([q1_start, q1_start])
            # xdata is determined by the width of the box plot

            # Lower whiskers
            box_plot['whiskers'][2*box_no].set_ydata([q1_start, q2_start])

            # Higher cap
            box_plot['caps'][2*box_no + 1].set_ydata([q4_end, q4_end])

            # Higher whiskers
            box_plot['whiskers'][2*box_no + 1].set_ydata([q4_start, q4_end])

            # Box
            box_plot['boxes'][box_no].set_ydata([q2_start,
                                                 q2_start,
                                                 q4_start,
                                                 q4_start,
                                                 q2_start])


            # Median
            box_plot['medians'][box_no].set_ydata([q3_start, q3_start])

            # Outliers
            # if fliers_xy is not None and len(fliers_xy[0]) != 0:
            #     If outliers exist
                # box_plot['fliers'][box_no].set(xdata = fliers_xy[0],
                #                                ydata = fliers_xy[1])
                #
                # min_y = min(q1_start, min_y, fliers_xy[1].min())
                # max_y = max(q4_end, max_y, fliers_xy[1].max())
            #
            # else:
            #     min_y = min(q1_start, min_y)
            #     max_y = max(q4_end, max_y)



            # box_no += 1

        # If redraw is set to true, the canvas is updated.
        # if redraw:
        #     ax.figure.canvas.draw()

        return box_plot


# pl.plot(x, y, 'k-')
# pl.fill_between(x, y-error, y+error)
# pl.show()


    def plot_fig_2c(self):

        # load data
        perc_df = pd.read_csv("../figs/fig2c-%s.csv" % self.anal_name,
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        print("perc_df: \n", perc_df)




        # figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')

        # fig_width = 8 + round(self.nb_samples / 3, 2)
        # print("fig_width: ", fig_width)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use(['ggplot'])

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()
        # plt.set_dpi(100)
        # fig.subplots_adjust(bottom=0.1)
        # ax = fig.add_subplot(111)
        # fig, ax = plt.subplots()
        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)

        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        bp = self.customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)

        # rstyle(ax)

        # colors = ['cyan', 'lightblue', 'lightgreen', 'tan', 'pink']
        # for patch, color in zip(b['boxes'], colors):
        #     patch.set_facecolor(color)

        # pylab.setp(bp['boxes'], color='blue', linewidth=2.0)
        # pylab.setp(bp['whiskers'], color='blue')
        # pylab.setp(bp['fliers'], marker='None')
        # pylab.setp(bp['caps'], color='blue', linewidth=1.0)
        # pylab.setp(bp['medians'], color='red', linewidth=1.0)





        # plt.show()

        # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
        xlabsmps = perc_df['name'].values

        print("xlabsmps: \n", xlabsmps)

        # plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
        # ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
        ax.set_xticklabels(xlabsmps, rotation=0, fontsize=10, ha='right')

        ax.get_yaxis().set_tick_params(direction='in')
        ax.get_xaxis().set_tick_params(direction='in')


        # plt.title('Duplication rates',  fontsize= 22)
        # ax.set_ylabel('Library size (M)', color='b')
        # for tl in ax.get_yticklabels():
        #     tl.set_color('b')


        ax.figure.canvas.draw()

        plt.tight_layout()


        # bbox={'facecolor':'black', 'alpha':0.5, 'pad':10}
        # ax.text(1, np.amax(self.y_cols_avg) - 1, "Pearson Correlation %s" % round(pearsonr, 2), style='italic', fontsize= 5)
        # ax.text(1, np.amax(self.y_cols_avg) - 2, "Spearman Correlation %s" % round(spearmanr, 2), style='italic', fontsize= 5)

        # colors = np.random.rand(nlen)


        # area = np.pi * (2 * np.random.rand(nlen))**2  # 0 to 15 point radiuses
        # area = 2

        # X = np.random.rand(100)
        # Y = X + np.random.rand(100)*0.1

        # results = sm.OLS(self.y_cols_avg, sm.add_constant(self.x_cols_avg)).fit()
        #
        # print(results.summary())

        # plt.scatter(datax, datay, s=area, c=colors, alpha=0.5)

        # X_plot = np.linspace(0, 15, 100)
        # plt.plot(X_plot, X_plot*results.params[0] + results.params[1])

        # fit = np.polyfit(self.x_cols_avg, self.y_cols_avg, deg=1)
        # print("fit: ", fit)
        # fity = fit[0] * self.x_cols_avg + fit[1]
        # print("fity: ", fity)

        # ax.plot(self.x_cols_avg, fity, linestyle='-', c='blue', linewidth=0.5, alpha=0.4)
        # ax.plot(datax, fity)


        # ax.scatter(self.x_cols_avg, self.y_cols_avg, marker='o', c='red', s=4, alpha=0.5)


        # plt.axis([0, 1.1*np.amax(self.x_cols_avg), 0, 1.1*np.amax(self.y_cols_avg)])

        # ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))

        # legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

        # xlabels = ax.get_xticklabels()
        # for i in range(len(xlabels)):
        #     label = xlabels[i].get_text()
        #     print("i: ", i, " label: ", label)
        #     raw = df_.ix[i, "sample"]
        #     print("row: ", raw)
        #     xlabels[i] = raw
            # print(i, xlabels[i])

        # labels = [item.get_text() for item in ax.get_xticklabels()]
        # labels[1] = 'Testing'
        # ax.set_xticklabels(labels)


        # ax.set_xticklabels(["" for x in xlabels])
        # print("xlabels: ", xlabels.inspect)
        # ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
        # for label in labels:
        #     label.set_rotation(45)
        #     label.set_fontsize(10)

        # for k in ax.get_xmajorticklabels():
        #     if some-condition:
        #         k.set_color(any_colour_you_like)

        # plt.grid(True, color='w', linestyle='-', linewidth=1)
        # plt.gca().patch.set_facecolor('0.8')
        # plt.savefig("../figs/fig2c-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/fig2c-%s.pdf" % self.anal_name, format="pdf")

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()

    def plot_fig_2c2(self):

        # load data
        perc_df = pd.read_csv("../figs/fig2c-%s.csv" % self.anal_name,
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        print("perc_df: \n", perc_df)

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()



        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        # bp = self.customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)

        plt.plot(perc_df.name.values, perc_df.q5.values, linestyle=':', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q4.values, linestyle='--', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q3.values, linestyle='-', linewidth=2, color='#B22400')
        plt.plot(perc_df.name.values, perc_df.q2.values, linestyle='--', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q1.values, linestyle=':', linewidth=1, color='#006BB2')

        # axis
        ax.get_xaxis().set_tick_params(direction='in')
        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.tick_params(which='both', width=1)
        plt.tick_params(which='major', length=10)
        plt.tick_params(which='minor', length=4, color='gray')
        ax.yaxis.set_tick_params(direction='in')
        ax.yaxis.set_tick_params(which='both', width=1)
        ax.yaxis.set_tick_params(which='major', length=10)
        ax.get_yaxis().set_tick_params(which='minor', length=4, color='gray')


        ax.figure.canvas.draw()

        plt.tight_layout()


        # plt.savefig("../figs/fig2c-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/fig2c-%s.pdf" % self.anal_name, format="pdf", dpi=300)  # saves the current figure into a pdf page


        plt.close()
        # ----------------------------------------------

        # fig = plt.figure()
        # ax = plt.gca()
        # plt.title(self.fig_title,  fontsize= 10)
        # plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # fig.set_size_inches(5, 5)
        # ax.set_xlabel(self.x_title, fontsize= 8)
        # ax.set_ylabel(self.y_title, fontsize= 8)
        # bp = self.customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)
        # xlabsmps = perc_df['name'].values
        # print("xlabsmps: \n", xlabsmps)
        # ax.set_xticklabels(xlabsmps, rotation=0, fontsize=10, ha='right')
        # ax.get_yaxis().set_tick_params(direction='in')
        # ax.get_xaxis().set_tick_params(direction='in')
        # ax.figure.canvas.draw()
        # plt.tight_layout()
        # plt.savefig("../figs/fig2c-%s.pdf" % self.anal_name, format="pdf")
        # plt.close()


    def plot_fig_2e(self):

        # load data
        perc_df = pd.read_csv("../figs/fig2e-%s.csv" % self.anal_name,
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        print("perc_df: \n", perc_df)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use(['ggplot'])

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()



        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        bp = self.customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)



        # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
        xlabsmps = perc_df['name'].values

        print("xlabsmps: \n", xlabsmps)

        # plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
        # ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
        ax.set_xticklabels(xlabsmps, rotation=0, fontsize=10, ha='right')

        ax.get_yaxis().set_tick_params(direction='in')
        ax.get_xaxis().set_tick_params(direction='in')


        # plt.title('Duplication rates',  fontsize= 22)
        # ax.set_ylabel('Library size (M)', color='b')
        # for tl in ax.get_yticklabels():
        #     tl.set_color('b')


        ax.figure.canvas.draw()

        plt.tight_layout()


        # bbox={'facecolor':'black', 'alpha':0.5, 'pad':10}
        # ax.text(1, np.amax(self.y_cols_avg) - 1, "Pearson Correlation %s" % round(pearsonr, 2), style='italic', fontsize= 5)
        # ax.text(1, np.amax(self.y_cols_avg) - 2, "Spearman Correlation %s" % round(spearmanr, 2), style='italic', fontsize= 5)

        # colors = np.random.rand(nlen)


        # area = np.pi * (2 * np.random.rand(nlen))**2  # 0 to 15 point radiuses
        # area = 2

        # X = np.random.rand(100)
        # Y = X + np.random.rand(100)*0.1

        # results = sm.OLS(self.y_cols_avg, sm.add_constant(self.x_cols_avg)).fit()
        #
        # print(results.summary())

        # plt.scatter(datax, datay, s=area, c=colors, alpha=0.5)

        # X_plot = np.linspace(0, 15, 100)
        # plt.plot(X_plot, X_plot*results.params[0] + results.params[1])

        # fit = np.polyfit(self.x_cols_avg, self.y_cols_avg, deg=1)
        # print("fit: ", fit)
        # fity = fit[0] * self.x_cols_avg + fit[1]
        # print("fity: ", fity)

        # ax.plot(self.x_cols_avg, fity, linestyle='-', c='blue', linewidth=0.5, alpha=0.4)
        # ax.plot(datax, fity)


        # ax.scatter(self.x_cols_avg, self.y_cols_avg, marker='o', c='red', s=4, alpha=0.5)


        # plt.axis([0, 1.1*np.amax(self.x_cols_avg), 0, 1.1*np.amax(self.y_cols_avg)])

        # ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))

        # legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

        # xlabels = ax.get_xticklabels()
        # for i in range(len(xlabels)):
        #     label = xlabels[i].get_text()
        #     print("i: ", i, " label: ", label)
        #     raw = df_.ix[i, "sample"]
        #     print("row: ", raw)
        #     xlabels[i] = raw
            # print(i, xlabels[i])

        # labels = [item.get_text() for item in ax.get_xticklabels()]
        # labels[1] = 'Testing'
        # ax.set_xticklabels(labels)


        # ax.set_xticklabels(["" for x in xlabels])
        # print("xlabels: ", xlabels.inspect)
        # ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
        # for label in labels:
        #     label.set_rotation(45)
        #     label.set_fontsize(10)

        # for k in ax.get_xmajorticklabels():
        #     if some-condition:
        #         k.set_color(any_colour_you_like)

        # plt.grid(True, color='w', linestyle='-', linewidth=1)
        # plt.gca().patch.set_facecolor('0.8')
        # plt.savefig("../figs/fig2e-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/fig2e-%s.pdf" % self.anal_name, format="pdf", dpi=300)  # saves the current figure into a pdf page

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()



    def plot_fig_depth(self):

        # load data
        perc_df = pd.read_csv("../files/metrics2/depth_stats/%s-bplot-depth-%s.csv" % (self.anal_name, self.subfig_name),
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        print("perc_df: \n", perc_df)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use(['ggplot'])

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()



        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(self.fig_dim_x, self.fig_dim_y)


        ax.set_xlabel(self.x_title, fontsize= 10)
        ax.set_ylabel(self.y_title, fontsize= 10)




        bp = self.customized_box_plot_horiz(perc_df, ax, redraw=True, notch=0, sym='+', vert=0, whis=1.5)


        #ax.set_xscale('log', basex=2)
        ax.set_yscale('log', basey=2)

        ax.yaxis.set_major_formatter(ScalarFormatter())
        # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
        xlabsmps = perc_df['name'].values

        print("xlabsmps: \n", xlabsmps)

        # plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
        ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
        #ax.set_xticklabels(xlabsmps, rotation=0, fontsize=10, ha='right')

        ax.get_yaxis().set_tick_params(direction='in')
        ax.get_xaxis().set_tick_params(direction='in')

        ax.figure.canvas.draw()

        plt.tight_layout()

        # plt.savefig("../figs/fig2e-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/depth_1%s-%s.pdf" % (self.subfig_name,self.anal_name), format="pdf", dpi=300)  # saves the current figure into a pdf page

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()


    def plot_fig_depth_horiz(self, sort=False):

        # load data
        perc_df = pd.read_csv("../files/metrics2/depth_stats/%s-bplot-depth-%s.csv" % (self.anal_name, self.subfig_name),
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )
        if sort:
            perc_df.sort(['q3'], inplace=True, ascending=False)

        perc_df.reset_index(drop=True, inplace=True)

        print("perc_df: \n", perc_df)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use(['ggplot'])

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()




        # plt.tight_layout()

        fig.set_size_inches(self.fig_dim_x, self.fig_dim_y)


        ax.set_xlabel(self.x_title, fontsize= 10)
        ax.set_ylabel(self.y_title, fontsize= 10)




        bp = self.customized_box_plot_horiz(perc_df, ax, redraw=True, notch=0, sym='+', whis=1.5)



        #ax.set_xscale('log', basey=2)
        ax.set_xscale('log', basex=2)

        ax.xaxis.set_major_formatter(ScalarFormatter())
        # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
        ylabsmps = perc_df['name'].values

        print("ylabsmps: \n", ylabsmps)

        #plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
        ax.set_yticklabels(ylabsmps, rotation=0, fontsize=8, ha='right')
        #ax.set_xticklabels(rotation=0, fontsize=8, ha='right')

        ax.get_yaxis().set_tick_params(direction='in')

        # show labels on top
        ax.get_xaxis().set_tick_params(direction='in', which='both', labeltop = 'on')

        #plt.subplots_adjust(top=0.80, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        plt.title(self.fig_title,  fontsize= 14, y=self.fig_title_topspace)

        ax.figure.canvas.draw()

        #plt.tight_layout(rect=[0.05, 0.03, 1, 0.95])
        plt.tight_layout()

        # plt.savefig("../figs/fig2e-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/depth_1%s-%s.pdf" % (self.subfig_name,self.anal_name), format="pdf")  # saves the current figure into a pdf page

        # plt.title('Page Two')
        # pdf.savefig()  # saves the current figure into a pdf page

        plt.close()





    def plot_fig_2e2(self):

        # load data
        perc_df = pd.read_csv("../figs/fig2e-%s.csv" % self.anal_name,
                      sep=',', header=0,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        # print("perc_df: \n", perc_df)
        # print("perc_df: \n", perc_df.q3.values)

        # exit(0)

        # pd.options.display.mpl_style = 'default'
        # plt.style.use(['ggplot'])

        print("style.available: ", plt.style.available)

        fig = plt.figure()
        ax = plt.gca()



        plt.title(self.fig_title,  fontsize= 10)
        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.12, right=0.95, hspace = 0.1, wspace = 0.2)
        # plt.tight_layout()

        fig.set_size_inches(5, 5)


        ax.set_xlabel(self.x_title, fontsize= 8)
        ax.set_ylabel(self.y_title, fontsize= 8)

        # bp = self.customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)

        plt.plot(perc_df.name.values, perc_df.q5.values, linestyle=':', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q4.values, linestyle='--', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q3.values, linestyle='-', linewidth=2, color='#B22400')
        plt.plot(perc_df.name.values, perc_df.q2.values, linestyle='--', linewidth=1, color='#006BB2')
        plt.plot(perc_df.name.values, perc_df.q1.values, linestyle=':', linewidth=1, color='#006BB2')





        # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
        # xlabsmps = perc_df['name'].values +1
        # print("xlabsmps: \n", xlabsmps)

        # plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
        # ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
        # ax.set_xticklabels(xlabsmps, rotation=0, fontsize=10, ha='right')


        ax.get_xaxis().set_tick_params(direction='in')

        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))

        plt.tick_params(which='both', width=1)
        plt.tick_params(which='major', length=10)
        plt.tick_params(which='minor', length=4, color='gray')

        ax.yaxis.set_tick_params(direction='in')
        ax.yaxis.set_tick_params(which='both', width=1)
        ax.yaxis.set_tick_params(which='major', length=10)
        ax.get_yaxis().set_tick_params(which='minor', length=4, color='gray')




        # plt.title('Duplication rates',  fontsize= 22)
        # ax.set_ylabel('Library size (M)', color='b')
        # for tl in ax.get_yticklabels():
        #     tl.set_color('b')


        ax.figure.canvas.draw()

        plt.tight_layout()


        # bbox={'facecolor':'black', 'alpha':0.5, 'pad':10}
        # ax.text(1, np.amax(self.y_cols_avg) - 1, "Pearson Correlation %s" % round(pearsonr, 2), style='italic', fontsize= 5)
        # ax.text(1, np.amax(self.y_cols_avg) - 2, "Spearman Correlation %s" % round(spearmanr, 2), style='italic', fontsize= 5)


        # plt.savefig("../figs/fig2e-%s.png" % self.anal_name, format="png", dpi=300)  # saves the current figure into a pdf page
        plt.savefig("../figs/fig2e-%s.pdf" % self.anal_name, format="pdf", dpi=300)  # saves the current figure into a pdf page


        plt.close()




    def prepare_fig2a(self):

        self.y_cols_df = self.edf2.iloc[:, self.y_col_range]
        # print("y_cols_df: ", self.y_cols_df)
        # exit(0)


        self.y_cols_arr = self.y_cols_df.values
        # print("self.y_cols_arr: \n", self.y_cols_arr)
        # exit(0)
        # blk_arr = np.zeros(edf2_len, dtype="float")
        # create bulk median column
        # print("blk_arr: ", blk_arr)

        self.y_cols_avg = np.apply_along_axis(self.mylogavg, 1, self.y_cols_arr)
        # print("y_cols_avg: ", self.y_cols_avg)


        self.x_cols_df = self.edf2.iloc[:, self.x_col_range]
        # print("x_cols_df: ", self.x_cols_df)
        # exit(0)
        self.x_cols_arr = self.x_cols_df.values
        # print("edf_scl_arr: \n", edf_scl_arr)
        self.x_cols_avg = np.apply_along_axis(self.mylogavg, 1, self.x_cols_arr)
        # print("x_cols_avg: ", self.x_cols_avg)


        y = pd.DataFrame({'yavg' : self.y_cols_avg})  # response
        X = pd.DataFrame({'xavg' : self.x_cols_avg})  # predictor
        X = sm.add_constant(X)  # Adds a constant term to the predictor
        print(X.head())
        est = sm.OLS(y, X)
        est = est.fit()
        print("est.summary: \n", est.summary())
        print("est.params: \n", est.params)

        # Now we calculate the predicted values
        # y_hat = est.predict(X)



    def main_fig2a(self):
        # load_one_module('gcc/4.8.2')

        #                      ["p3-gn-cn-norm-ss", "p3-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 2",  range(6, 36), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p2-gn-cn-norm-ss", "p2-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 1",  range(5, 35), "Pearson correlation", [1, 2, 4], "Kernel Density"]]
        self.run_datasets = [
            # ["p9-gn-cn-norm-ss-tc200-rneasy", "a011-gn-cn-norm", "Gene expression correlation P2-28 PDX",  range(12-1, 45), "Average of single cells [log2(CNT+1)]", [8-1, 9-1, 10-1, 11-1], "Average of Bulk Cells TC200+RNeasy [log2(CNT+1)]"],
            # ["p9-gn-cn-norm-ss-tc200", "a011-gn-cn-norm", "Gene expression correlation P2-28 PDX",  range(12-1, 45), "Average of single cells [log2(CNT+1)]", [8-1, 9-1], "Average of Bulk Cells TC200 [log2(CNT+1)]"],
            # ["p1876-gn-cn-norm-ss-rneasy", "a012-gn-cn-norm", "Gene expression correlation P1876 PDX",  range(7-1, 50), "Average of single cells [log2(CNT+1)]", [5-1, 6-1], "Average of Bulk Cells RNeasy [log2(CNT+1)]"],

            ["HCI001-gn-cn-norm-ss-tc1000", "a018-gn-cn-norm", "Gene expression correlation HCI001 PDX",  range(22-1, 62), "Average of single cells [log2(CNT+1)]", [8-1, 9-1], "Average of bulk TC1000 cells  [log2(CNT+1)]"],
            ["HCI001-gn-cn-norm-ss-tc200", "a018-gn-cn-norm", "Gene expression correlation HCI001 PDX",  range(22-1, 62), "Average of single cells [log2(CNT+1)]", [10-1, 11-1], "Average of bulk TC200 cells  [log2(CNT+1)]"],
            ["HCI001-gn-cn-norm-ss-tcall", "a018-gn-cn-norm", "Gene expression correlation HCI001 PDX",  range(22-1, 62), "Average of single cells [log2(CNT+1)]", [8-1, 9-1, 10-1, 11-1, 15-1, 16-1, 17-1, 18-1], "Average of bulk TCall cells [log2(CNT+1)]"],

            # ["p9-gn-cn-norm-tc200-rneasy", "a011-gn-cn-norm", "Gene expression correlation P2-28 PDX",  [8-1, 9-1], "Average of single cells [log2(CNT+1)]", [10-1, 11-1], "Average of RNeasy Bulk cells [log2(CNT+1)]"],

            # ["p9-gn-cn-ercc-ss-tc200-rneasy", "a011-gn-cn-ercc", "ERCC expression correlation P2-28 PDX",  range(12-1, 45), "Average of single cells [log2(CNT+1)]", [8-1, 9-1, 10-1, 11-1], "Average of bulk TC_200+TC_RNeasy cells [log2(CNT+1)]"],
            # ["p9-gn-cn-ercc-ss-tc200", "a011-gn-cn-ercc", "ERCC expression correlation P2-28 PDX",  range(12-1, 45), "Average of single cells [log2(CNT+1)]", [8-1, 9-1], "Average of bulk TC_200 cells [log2(CNT+1)]"],
            # ["p1876-gn-cn-ercc-ss-rneasy", "a012-gn-cn-ercc", "ERCC expression correlation P1876 PDX",  range(7-1, 50), "Average of single cells [log2(CNT+1)]", [5-1, 6-1], "Average of bulk TC_RNeasy cells [log2(CNT+1)]"]
            ["HCI001-gn-cn-ercc-ss-tc1000", "a018-gn-cn-ercc", "ERCC expression correlation HCI001 PDX",  range(22-1, 62), "Average of single cells [log2(CNT+1)]", [8-1, 9-1], "Average of bulk TC1000 cells [log2(CNT+1)]"],
            ["HCI001-gn-cn-ercc-ss-tcall", "a018-gn-cn-ercc", "ERCC expression correlation HCI001 PDX",  range(22-1, 62), "Average of single cells [log2(CNT+1)]", [8-1, 9-1, 10-1, 11-1, 15-1, 16-1, 17-1, 18-1], "Average of bulk TCall cells [log2(CNT+1)]"],
            # ["p9-gn-cn-ercc-tc200-rneasy", "a011-gn-cn-ercc", "ERCC expression correlation P2-28 PDX",  [8-1, 9-1], "Average of bulk TC_200 cells [log2(CNT+1)]", [10-1, 11-1], "Average of bulk TC_RNeasy cells [log2(CNT+1)]"]

            # ["p3-gn-cn-norm-ss-rneasy", "p3-gn-cn-norm", "Gene expression correlation P2-6L run 2",  range(6, 36), "Average of single cells [log2(CNT+1)]", [4, 5], "Average of bulk TC_RNeasy [log2(CNT+1)]"],
            #
            # ["p2-gn-cn-norm-ss-tc200-rneasy", "p2-gn-cn-norm", "Gene expression correlation P2-6L run 1",  range(5, 35), "Average of single cells [log2(CNT+1)]", [1, 2, 3, 4], "Average of bulk TC_200+TC_RNeasy cells [log2(CNT+1)]"],
            # ["p2-gn-cn-norm-ss-tc200", "p2-gn-cn-norm", "Gene expression correlation P2-6L run 1",  range(5, 35), "Average of single cells [log2(CNT+1)]", [1, 2], "Average of bulk TC_200 cells [log2(CNT+1)]"],
            # ["p2-gn-cn-norm-ss-rneasy", "p2-gn-cn-norm", "Gene expression correlation P2-6L run 1",  range(5, 35), "Average of single cells [log2(CNT+1)]", [3, 4], "Average of bulk TC_RNeasy cells [log2(CNT+1)]"],
            # ["p2-gn-cn-norm-tc200-rneasy", "p2-gn-cn-norm", "Gene expression correlation P2-6L run 1",  [1, 2], "Average of bulk TC_200 cells [log2(CNT+1)]", [3, 4], "Average of bulk TC_RNeasy cells [log2(CNT+1)]"],
            #
            # ["p8-gn-cn-norm-ss-tc200", "p8-gn-cn-norm", "Gene expression correlation P2-25",  range(5, 46), "Average of single cells [log2(CNT+1)]", [1, 2], "Average of bulk TC_200 cells [log2(CNT+1)]"],
            # ["p8-gn-cn-ercc-ss-tc200", "p8-gn-cn-ercc", "ERCC expression correlation P2-25",  range(5, 46), "Average of single cells [log2(CNT+1)]", [1, 2], "Average of bulk TC_200 cells [log2(CNT+1)]"]

        ]




        # debug
        # self.run_datasets = [["p9-gn-cn-norm-ss-tc200", "p9-gn-cn-norm", "Gene expression correlation P2-28 PDX",  range(6, 39), "Average of Single Cells [log2(CNT+1)]", [1, 2], "Average of Bulk Cells TC200 [log2(CNT+1)]"]]

        for run_dtst in self.run_datasets:
            self.anal_name, \
            self.dtst_name, self.fig_title, self.x_col_range, self.x_title, self.y_col_range, self.y_title = run_dtst

            self.extract_dataset()

            self.prepare_fig2a()
            self.plot_fig_2a()



    def main_fig2b(self):
        # load_one_module('gcc/4.8.2')



        self.run_datasets = [
            # ["p1876-gn-cn-norm-ss", "a012-gn-cn-norm", "Gene inter single-cell correlation p1876 PDX", range(7-1, 50), "Pearson correlation", [5-1, 6-1], "Kernel Density"],
            # ["p1876-gn-cn-ercc-ss", "a012-gn-cn-ercc", "ERCC inter single-cell correlation p1876 PDX", range(7-1, 50),"Pearson correlation", [5-1, 6-1], "Kernel Density"]

            ["HCI001-gn-cn-norm-ss", "a018-gn-cn-norm", "Gene inter single-cell correlation HCI001 PDX", range(22-1, 62), "Pearson correlation", [8-1, 9-1], "Kernel Density"],
            ["HCI001-gn-cn-ercc-ss", "a018-gn-cn-ercc", "ERCC inter single-cell correlation HCI001 PDX", range(22-1, 62), "Pearson correlation", [8-1, 9-1], "Kernel Density"]

            # ["p3-gn-cn-norm-ss", "p3-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 2",  range(6, 36), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
            # ["p2-gn-cn-norm-ss", "p2-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 1",  range(5, 35), "Pearson correlation", [1, 2, 4], "Kernel Density"],
            # ["p8-gn-cn-norm-ss", "p8-gn-cn-norm", "Gene inter single-cell correlation Paul8",  range(5, 46), "Pearson correlation", [1, 2], "Kernel Density"],
            # ["p8-gn-cn-ercc-ss", "p8-gn-cn-ercc", "ERCC inter single-cell correlation Paul8",  range(5, 46), "Pearson correlation", [1, 2], "Kernel Density"]
        ]


        for run_dtst in self.run_datasets:
            self.anal_name, \
            self.dtst_name, self.fig_title, self.x_col_range, self.x_title, self.y_col_range, self.y_title = run_dtst

            self.extract_dataset()

            self.prepare_fig2b()
            self.plot_fig_2b()

    def main_fig2c(self):
        # load_one_module('gcc/4.8.2')



        # self.run_datasets = [["p9-gn-cn-norm-ss", "p9-gn-cn-norm", "Gene inter single-cell correlation P2-28 PDX",  range(6, 39), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p9-gn-cn-ercc-ss", "p9-gn-cn-ercc", "ERCC inter single-cell correlation P2-28 PDX",  range(6, 39),"Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p3-gn-cn-norm-ss", "p3-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 2",  range(6, 36), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p2-gn-cn-norm-ss", "p2-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 1",  range(5, 35), "Pearson correlation", [1, 2, 4], "Kernel Density"]]

        # debugging
        self.run_datasets = [
            # ["p9-gn-cn-norm-ss-tc200-rneasy", "a011-gn-cn-norm", "Gene expression of single cell subsets P2-28 PDX",  range(12-1, 45), "Single cells subset [cardinality]", [8-1, 9-1, 10-1, 11-1], r"Average of bulk TC_200+TC_RNeasy cells [$R^2$]"],
            # ["p9-gn-cn-norm-ss-tc200", "a011-gn-cn-norm", "Gene expression of single cell subsets P2-28 PDX",  range(12-1, 45), "Single cells subset [cardinality]", [8-1, 9-1], r"Average of bulk TC_200 cells [$R^2$]"],


            # ["p1876-gn-cn-norm-ss-rneasy", "a012-gn-cn-norm", "Gene expression of single cell subsets p1876 PDX",  range(7-1, 50), "Single cells subset [cardinality]", [5-1, 6-1], r"Average of bulk TC_RNeasy cells [$R^2$]"],

            # ["p9-gn-cn-ercc-ss-tc200-rneasy", "a011-gn-cn-ercc", "ERCC expression of single cell subsets P2-28 PDX",  range(12-1, 45), "Single cells subset [cardinality]", [8-1, 9-1, 10-1, 11-1], r"Average of bulk TC_200+TC_RNeasy cells [$R^2$]"],
            # ["p9-gn-cn-ercc-ss-tc200", "a011-gn-cn-ercc", "ERCC expression of single cell subsets P2-28 PDX",  range(12-1, 45), "Single cells subset [cardinality]", [8-1, 9-1], r"Average of bulk TC_200 cells [$R^2$]"],
            # ["p1876-gn-cn-ercc-ss-rneasy", "a012-gn-cn-ercc", "ERCC expression of single cell subsets p1876 PDX",  range(7-1, 50), "Single cells subset [cardinality]", [5-1, 6-1], r"Average of bulk TC_RNeasy cells [$R^2$]"]

            ["HCI001-gn-cn-norm-ss-tcall", "a018-gn-cn-norm", "Gene expression of single cell subsets HCI001 PDX",  range(22-1, 62), "Single cells subset [cardinality]", [8-1, 9-1, 10-1, 11-1, 15-1, 16-1, 17-1, 18-1], r"Average of bulk TC_all cells [$R^2$]"],
            ["HCI001-gn-cn-ercc-ss-tcall", "a018-gn-cn-ercc", "ERCC expression of single cell subsets HCI001 PDX",  range(22-1, 62), "Single cells subset [cardinality]", [8-1, 9-1, 10-1, 11-1, 15-1, 16-1, 17-1, 18-1], r"Average of bulk TC_all cells [$R^2$]"]

            # ["p3-gn-cn-norm-ss-rneasy", "p3-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 2",  range(6, 36), "Single cells subset [cardinality]", [4, 5], r"Average of bulk TC_RNeasy cells [$R^2$]"],
            #
            # ["p2-gn-cn-norm-ss-tc200-rneasy", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [cardinality]", [1, 2, 3, 4], r"Average of bulk TC_200+TC_RNeasy cells [$R^2$]"],
            # ["p2-gn-cn-norm-ss-tc200", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [cardinality]", [1, 2], r"Average of bulk TC_200 cells [$R^2$]"],
            # ["p2-gn-cn-norm-ss-rneasy", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [cardinality]", [3, 4], r"Average of bulk TC_RNeasy cells [$R^2$]"],
            #
            # ["p8-gn-cn-norm-ss-tc200", "p8-gn-cn-norm", "Gene expression of single cell subsets P2-25",  range(5, 46), "Single cells subset [cardinality]", [1, 2], r"Average of bulk TC_200 cells [$R^2$]"],
            # ["p8-gn-cn-ercc-ss-tc200", "p8-gn-cn-ercc", "ERCC expression of single cell subsets P2-25",  range(5, 46), "Single cells subset [cardinality]", [1, 2], r"Average of bulk TC_200 cells [$R^2$]"]
        ]


        for run_dtst in self.run_datasets:
            self.anal_name, \
            self.dtst_name, self.fig_title, self.x_col_range, self.x_title, self.y_col_range, self.y_title = run_dtst

            self.extract_dataset()

            self.prepare_fig2c()
            self.plot_fig_2c2()


    # based on expressed SNVs
    def main_fig2e(self):
        # load_one_module('gcc/4.8.2')



        # self.run_datasets = [["p9-gn-cn-norm-ss", "p9-gn-cn-norm", "Gene inter single-cell correlation P2-28 PDX",  range(6, 39), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p9-gn-cn-ercc-ss", "p9-gn-cn-ercc", "ERCC inter single-cell correlation P2-28 PDX",  range(6, 39),"Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p3-gn-cn-norm-ss", "p3-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 2",  range(6, 36), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #                      ["p2-gn-cn-norm-ss", "p2-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 1",  range(5, 35), "Pearson correlation", [1, 2, 4], "Kernel Density"]]

        # debugging
        self.run_datasets = [

            ["p9-rn-vl-norm-ss-tc200", "p9-rn-vl-norm", "eSNV overlap with bulk P2-28 PDX",  range(12, 45), "Single cells subset [cardinality]", [7, 8], r"Overlap with TC_200 [ratio]"],
            ["p9-rn-vl-norm-ss-rneasy", "p9-rn-vl-norm", "eSNV overlap with bulk P2-28 PDX",  range(12, 45), "Single cells subset [cardinality]", [9, 10], r"Overlap with TC_RNeasy [ratio]"],
            ["hc-dn-vl-p4-p5-gd-un", "hc-dn-vl-p4-p5", "SNV overlap with bulk P2-6L cells",  range(20, 103), "Single cells subset [cardinality]", [10, 11], r"Overlap with GD_UN [ratio]"],
            ["hc-dn-vl-p4-p5-tc200", "hc-dn-vl-p4-p5", "SNV overlap with bulk P2-6L cells",  range(20, 103), "Single cells subset [cardinality]", [12, 13], r"Overlap with TC_200 [ratio]"]

        ]


            # ["p9-gn-cn-norm-ss-tc200-rneasy", "p9-gn-cn-norm", "Gene expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [1, 2, 4, 5], r"Average of Bulk Cells TC200+RNeasy [$R^2$]"],
            # ["p9-gn-cn-norm-ss-tc200", "p9-gn-cn-norm", "Gene expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [1, 2], r"Average of Bulk Cells TC200 [$R^2$]"],
            # ["p9-gn-cn-norm-ss-rneasy", "p9-gn-cn-norm", "Gene expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [4, 5], r"Average of Bulk Cells RNeasy [$R^2$]"],
            #
            # ["p9-gn-cn-ercc-ss-tc200-rneasy", "p9-gn-cn-ercc", "ERCC expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [1, 2, 4, 5], r"Average of Bulk Cells TC200+RNeasy [$R^2$]"],
            # ["p9-gn-cn-ercc-ss-tc200", "p9-gn-cn-ercc", "ERCC expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [1, 2], r"Average of Bulk Cells TC200 [$R^2$]"],
            # ["p9-gn-cn-ercc-ss-rneasy", "p9-gn-cn-ercc", "ERCC expression of single cell subsets P2-28 PDX",  range(6, 39), "Single cells subset [Cardinality]", [4, 5], r"Average of Bulk Cells RNeasy [$R^2$]"],
            #
            # ["p3-gn-cn-norm-ss-rneasy", "p3-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 2",  range(6, 36), "Single cells subset [Cardinality]", [4, 5], r"Average of Bulk Cells RNeasy [$R^2$]"],
            #
            # ["p2-gn-cn-norm-ss-tc200-rneasy", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [Cardinality]", [1, 2, 3, 4], r"Average of Bulk Cells TC200+RNeasy [$R^2$]"],
            # ["p2-gn-cn-norm-ss-tc200", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [Cardinality]", [1, 2], r"Average of Bulk Cells TC200 [$R^2$]"],
            # ["p2-gn-cn-norm-ss-rneasy", "p2-gn-cn-norm", "Gene expression of single cell subsets P2-6L run 1",  range(5, 35), "Single cells subset [Cardinality]", [3, 4], r"Average of Bulk Cells RNeasy [$R^2$]"],

            # ["p8-gn-cn-norm-ss-tc200", "p8-gn-cn-norm", "Gene expression of single cell subsets Paul8",  range(5, 46), "Single cells subset [Cardinality]", [1, 2], r"Average of Bulk Cells TC200 [$R^2$]"],
            # ["p8-gn-cn-ercc-ss-tc200", "p8-gn-cn-ercc", "ERCC expression of single cell subsets Paul8",  range(5, 46), "Single cells subset [Cardinality]", [1, 2], r"Average of Bulk Cells TC200 [$R^2$]"],


        for run_dtst in self.run_datasets:
            self.anal_name, \
            self.dtst_name, self.fig_title, self.x_col_range, self.x_title, self.y_col_range, self.y_title = run_dtst

            self.extract_dataset()

            self.prepare_fig2e()
            self.plot_fig_2e2()



    # based on expressed SNVs
    def main_fig_depth_1a(self):
        # load_one_module('gcc/4.8.2')

        self.anal_name = "bab"
        self.fig_title = "Depth of coverage"
        self.y_title = "Average coverage [Log2(copies)]"


        self.subfig_name = "a"
        self.x_title = "Bulk and Control samples"
        self.fig_dim_x = 6
        self.fig_dim_y = 7

        self.plot_fig_depth()

        #self.subfig_name = "b"
        #self.x_title = "Exome E4 samples"
        #self.fig_dim_x = 7
        #self.fig_dim_y = 7

        #self.plot_fig_depth()

        #self.subfig_name = "c"
        #self.x_title = "Exome E5 samples"
        #self.fig_dim_x = 22
        #self.fig_dim_y = 7

        #self.plot_fig_depth()

    # based on expressed SNVs
    def main_fig_depth_horiz(self):
        # load_one_module('gcc/4.8.2')

        self.anal_name = "bab"
        self.fig_title = "Exome bulk and tissue control samples depth of coverage"
        self.x_title = "Base coverage [copies]"


        self.subfig_name = "a"
        self.y_title = "Samples"
        self.fig_dim_x = 10
        self.fig_dim_y = 5
        self.fig_title_topspace = 1.08

        self.plot_fig_depth_horiz(sort=False)

        self.subfig_name = "d"
        self.fig_title = "GCRC1735T-P2-6L single cells depth of coverage"

        self.y_title = "Single cells"
        self.fig_dim_x = 10
        self.fig_dim_y = 30
        self.fig_title_topspace = 1.014

        self.plot_fig_depth_horiz(sort=True)

        #self.subfig_name = "c"
        #self.y_title = "Exome E5 samples"
        #self.fig_dim_x = 10
        #self.fig_dim_y = 22

        #self.plot_fig_depth_horiz()




    # based on expressed SNV
    def main_fig2d(self):
        # load_one_module('gcc/4.8.2')



        self.run_datasets = [
            # ["p9-rn-vl-norm-ss", "p9-rn-vl-norm", "eSNV inter single-cell overlap of P2-28 PDX",  range(12, 45), "Overlap ratio", [8, 9], "Kernel density"],
            # ["hc-dn-vl-p4-p5", "hc-dn-vl-p4-p5", "SNV inter single-cell overlap of P2-6L cells",  range(20, 103), "Overlap ratio", [12, 13], r"Kernel density"]

            ["hc-dn-vl-p4-p5-cosmic-32", "hc-dn-vl-p4-p5-cosmic-32", "COSMIC SNV inter single-cell overlap of P2-6L cells",  range(9, 89), "Overlap ratio", [1, 2], r"Kernel density"]


        ]

        # ["p9-gn-cn-ercc-ss", "p9-gn-cn-ercc", "ERCC inter single-cell correlation P2-28 PDX",  range(6, 39),"Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #     ["p3-gn-cn-norm-ss", "p3-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 2",  range(6, 36), "Pearson correlation", [1, 2, 4, 5], "Kernel Density"],
        #     ["p2-gn-cn-norm-ss", "p2-gn-cn-norm", "Gene inter single-cell correlation P2-6L run 1",  range(5, 35), "Pearson correlation", [1, 2, 4], "Kernel Density"],
        #     ["p8-gn-cn-norm-ss", "p8-gn-cn-norm", "Gene inter single-cell correlation Paul8",  range(5, 46), "Pearson correlation", [1, 2], "Kernel Density"],
        #     ["p8-gn-cn-ercc-ss", "p8-gn-cn-ercc", "ERCC inter single-cell correlation Paul8",  range(5, 46), "Pearson correlation", [1, 2], "Kernel Density"]


        for run_dtst in self.run_datasets:
            self.anal_name, \
            self.dtst_name, self.fig_title, self.x_col_range, self.x_title, self.y_col_range, self.y_title = run_dtst

            self.extract_dataset()

            self.prepare_fig2d()
            self.plot_fig_2d()


    def stats_depth_bplot(self):

        columns = ['smp_idx', 'smp_name', 'q1', 'q2', 'q3', 'q4', 'q5']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'smp_idx': 'object', 'smp_name': 'object',
                  'q1': 'float64', 'q2': 'float64', 'q3': 'float64',
                  'q4': 'float64', 'q5': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)


        for idx in range(self.nb_smpgrps):
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            print("self.smpgrp_idx: ", self.smpgrp_idx)
            print("self.smpgrp_name: ", self.smpgrp_name)


            print("smp_targ: ", self.smp_targ_stats_csv)
            # read sample matrix
            try:
                #positional reindexing
                cur_df = pd.read_csv(self.smp_depth_stats_base, sep='\t', header=0,
                                     # index_col=[0],
                                     usecols=[1, 2],
                                     names=["depth", "nbases"])
            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            # cur_df = cur_df[cur_df['sample'] == self.smp_name]
            # cur_df = cur_df[cur_df.index == self.smp_name]
            # print("cur_df: ", cur_df)

            data = np.repeat(cur_df["depth"].values, cur_df["nbases"].values)
            print ("data: \n", data.shape[0])

            qmed = np.percentile(data, 50)
            q25 = np.percentile(data, 25)
            q75 = np.percentile(data, 75)

            whis = 1.5
            # get high extreme
            iq = q75 - q25
            hi_val = q75 + whis * iq
            wisk_hi = np.compress(data <= hi_val, data)
            if len(wisk_hi) == 0 or np.max(wisk_hi) < q75:
                wisk_hi = q75
            else:
                wisk_hi = max(wisk_hi)

            # get low extreme
            lo_val = q25 - whis * iq
            wisk_lo = np.compress(data >= lo_val, data)
            if len(wisk_lo) == 0 or np.min(wisk_lo) > q25:
                wisk_lo = q25
            else:
                wisk_lo = min(wisk_lo)

            # print(wisk_lo, q25, qmed, q75, wisk_hi)


            # Setting With Enlargement
            df_.loc[len(df_)+1] = [self.smpgrp_idx, self.smpgrp_name, wisk_lo, q25, qmed, q75, wisk_hi]

        df_.set_index(['smp_idx'], inplace=True)
        df_.sort_index(inplace=True)

        # df_.sort(['sample'], ascending=[1], inplace=True)
        # df_ = df_.reset_index(drop=True)
        # df_= df_.set_index(["sample"])
        print("df_: \n", df_)


        df_.to_csv(self.depth_stats_csv,
                           sep=',', header=True
                           ,index=True
                           ,index_label="smp_idx"
                           )


    def gen_stats_depth_graph_files(self):

        stats_df = pd.read_csv(self.depth_stats_csv, sep=',', header=False,
                                     index_col=[1],
                                     # usecols=[1, 2, 3, 4, 5, 6, 7],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"]
                                     )
        print("stats_df: \n", stats_df)

        # create group csvs
        conf_df = pd.read_csv(self.smp_depth_conf_csv, sep=',', header=False,
                                     index_col=[1],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                              )
        merged_df = pd.merge(conf_df, stats_df, how='inner', left_index=True, right_index=True)
        merged_df.drop('other_name', axis=1, inplace=True)
        merged_df.drop('smp_idx_x', axis=1, inplace=True)
        merged_df.drop('smp_idx_y', axis=1, inplace=True)


        print("merged_df: \n", merged_df)

        groups_idx = np.unique(merged_df["grp"].values)
        for grp_id in groups_idx:
            self.graph_grp_idx = grp_id

            print("grp_id: ", grp_id)
            # mer_grp_df = merged_df[merged_df['grp'] == grp]
            mer_grp_df = merged_df.query('grp == @grp_id').copy()
            mer_grp_df.drop('grp', axis=1, inplace=True)
            mer_grp_df.drop('sbgrp', axis=1, inplace=True)
            mer_grp_df.reset_index(level=0, inplace=True)
            # move index to datacolumn
            # merged_df = merged_df.reset_index('rname')

            mer_grp_df.set_index(['order_id'], inplace=True, drop=True)
            mer_grp_df.sort_index(inplace=True)

            # mer_grp_df.sort('order_id', ascending=True, inplace=True)
            print("mer_grp_df: \n", mer_grp_df)
            mer_grp_df.to_csv(self.depth_stats_grp_csv,
                           sep=',', header=True
                           ,index=True
                           # ,index_label="smp_idx"
                           )


    def prepare_hc_mp_ug(self):

        # load data
        gtdf = pd.read_csv("../files/bab-hc-mp-ug.gemini.gttypes.txt",
                      sep='\t', header=0
                           # ,
                      # index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        print("gtdf: \n", gtdf)

        # pd.set_option('io.hdf.default_format', 'table')
        #
        # store = pd.HDFStore("tst.h5")
        # store = pd.HDFStore("/gs/project/wst-164-ab/share/rnvar/a004-019/sc_qc/files/gtdf.h5")

        # print(store)

        # gtdf.start = gtdf.start.astype(int)
        # gtdf.end = gtdf.end.astype(int)

        # Sort in place.
        # gtdf.sort(['chrom', 'start', 'end'], inplace=True)

        # print("exon: \n ", exon)
        #
        # store['exon'] = exon

        # store.put('all/all', gc, format='table', data_columns=True)
        # store.put('all/compr', gc, format='table', complib='blosc', chunksize=2000000
                  # ,
                  # data_columns=['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame',
                  #               'gene_id', 'gene_name', 'gene_source', 'gene_biotype',
                  #               'ccds_id', 'transcript_id', 'transcript_name', 'transcript_source' ,'tss_id',
                  #               'exon_id', 'exon_number',
                  #               'p_id', 'protein_id', 'tag']
        # )
        # print(store)

        # store.close()
        # gc.collect()



        cols = gtdf.columns.tolist()
        print("cols: ", cols)

        filt_cols = ["chrom", "start", "end", "ref", "alt"]

        callers = ["HC", "MP", "UG"]

        for caller in callers:

            # filter columns
            for col in cols:
                m2 = re.match("^gt_types\.(\S+)-ba[a,b]-%s$" % caller, col)
                if m2:
                    filt_cols.append(col)

            print("filt_cols: ", filt_cols)

            df2 = gtdf.loc[:, filt_cols]
            print("df2: \n", df2)


            # filt_cols2 = ["chrom", "start", "end", "ref", "alt"]
            filt_cols_len = len(filt_cols)
            print("filt_cols_len: ", filt_cols_len)

            # exit(0)

            # rename
            for col in filt_cols:
            #     m2 = re.match("^gt_types\.(\S+)\.33\.pair1\.fastq\.gz$", fname)
                m2 = re.match("^gt_types\.(\S+)-ba[a,b]-%s$" % caller, col)
                if m2:
                    df2.rename(columns={col: m2.group(1)}, inplace=True)

            print("df2: \n", df2)
            idx = []
            for ir in df2.itertuples():
                ia= np.array(ir)[6:filt_cols_len]
                # print("ia: ", ia )
                if (not np.all(ia == "2")) and (not np.all(ia == "0")):
                    idx.append(ir[0])

                # print(ir[0],ir[1], ir[10])
                # C.append((ir[1], ir[2]))
            # print(idx[0:10], len(idx))

            df3 = df2.iloc[idx,:]
            # print("df3: ", df3.columns)
            # exit(0)

            rencols = ['chrom', 'start', 'end', 'ref', 'alt', 'Germline', 'NTC',
                       'SCells', 'TC200s', 'UNCTRLs',
                       'P4_GD_UN', 'P5_GD_UN', 'P4_TC_200', 'P5_TC_200',
                       'Pre_Treatment_1735', 'Post_Treatment_1735',
                       'TI_GD_1521', 'TI_GD_1522', 'TI_GD_1523', 'TI_GD_1528',
                       'P4_SC_A02_G_C02', 'P4_SC_A08_G_C05', 'P4_SC_B03_G_C07',
                       'P4_SC_B08_G_C11', 'P4_SC_B10_RBLOB', 'P4_SC_B11_G_C59',
                       'P4_SC_C03_G_C13', 'P4_SC_C04_G_C61', 'P4_SC_C09_G_C16',
                       'P4_SC_D02_G_C20', 'P4_SC_E03_G_C27', 'P4_SC_E07_G_C28',
                       'P4_SC_E10_G_C78', 'P4_SC_F02_G_C32', 'P4_SC_F08_G_C35',
                       'P4_SC_G07_G_C40', 'P4_SC_H02_G_C44', 'P4_SC_H07_G_C46',
                       'P4_SC_H09_G_C48',  'P5_SC_A01_G_C03', 'P5_SC_A02_G_C02',
                       'P5_SC_A03_G_C01', 'P5_SC_A04_G_C49', 'P5_SC_A06_G_C51',
                       'P5_SC_A07_G_C06', 'P5_SC_A08_G_C05',
                       'P5_SC_A11_G_C53', 'P5_SC_B01_G_C09', 'P5_SC_B02_G_C08',
                       'P5_SC_B03_G_C07', 'P5_SC_B05_G_C56', 'P5_SC_B06_G_C57',
                       'P5_SC_B07_G_C12', 'P5_SC_B08_G_C11', 'P5_SC_B09_G_C10',
                       'P5_SC_B11_G_C59', 'P5_SC_B12_G_C60', 'P5_SC_C01_G_C15',
                       'P5_SC_C02_G_C14', 'P5_SC_C04_G_C61', 'P5_SC_C06_G_C63',
                       'P5_SC_C07_G_C18', 'P5_SC_C08_G_C17', 'P5_SC_C10_G_C64',
                       'P5_SC_C12_G_C66', 'P5_SC_D01_G_C21', 'P5_SC_D02_G_C20',
                       'P5_SC_D05_G_C68', 'P5_SC_D07_G_C24', 'P5_SC_D08_G_C23',
                       'P5_SC_D09_G_C22', 'P5_SC_D10_G_C70', 'P5_SC_D11_G_C71',
                       'P5_SC_D12_G_C72', 'P5_SC_E02_G_C26', 'P5_SC_E05_G_C74',
                       'P5_SC_E06_G_C73', 'P5_SC_E07_G_C28', 'P5_SC_E08_G_C29',
                       'P5_SC_E09_G_C30', 'P5_SC_F01_G_C31', 'P5_SC_F02_G_C32',
                       'P5_SC_F03_G_C33', 'P5_SC_F04_G_C81', 'P5_SC_F05_G_C80',
                       'P5_SC_F09_G_C36', 'P5_SC_F10_G_C84', 'P5_SC_F11_G_C83',
                       'P5_SC_G01_G_C37', 'P5_SC_G02_E_C38', 'P5_SC_G04_G_C87',
                       'P5_SC_G07_G_C40', 'P5_SC_G08_G_C41', 'P5_SC_G09_G_C42',
                       'P5_SC_G10_G_C90', 'P5_SC_H01_G_C43', 'P5_SC_H02_G_C44',
                       'P5_SC_H03_G_C45', 'P5_SC_H04_G_C93', 'P5_SC_H05_G_C92',
                       'P5_SC_H07_G_C46', 'P5_SC_H08_G_C47', 'P5_SC_H09_G_C48'
                       ]

            df4 = df3.loc[:, rencols]
            print(df4)

            # df.reindex_axis(sorted(df.columns), axis=1)

            df4.to_csv("../files/bab-%s.csv" % caller,
                   sep=',', header=True
                   , index=False
                   # , index_label="smp_idx"
                   )

    def fastq_reads(self):

        #parallel "echo {} && gunzip -c {} | wc -l | awk '{d=\$1; print d/4;}'" ::: raw_reads/*/*/*.fastq.gz

        #parallel "echo {} && gunzip -c {} | wc -l | awk '{d=\$1; print d/4;}'" ::: raw_reads/*/*/*.fastq.gz

        # load data
        fqdf = pd.read_csv("../files/bab-fastq-reads.txt",
                      sep='\n', header=None
                           # ,
                      # index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )
        columns = ['smp', 'runlane', 'direction', 'nb_reads']
        df_ = pd.DataFrame(columns=columns)
        dtypes = {'smp': 'object', 'runlane': 'object', 'direction': 'int', 'nb_reads': 'int'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)



        print("fqdf: \n", fqdf, fqdf.shape)

        for i in range(0,fqdf.shape[0] -1, 2):
            line = fqdf.iloc[i,0]
            val = fqdf.iloc[i+1,0]

            m2 = re.match("^\/gs\/project\/wst-164-ab\/share\/exome\/bab-013\/raw_reads\/(\S+)\/(\S+)\/(\S+\.pair(\d)\.fastq.gz)$", line)
            if m2:
                sample = m2.group(1)
                m3 = re.match("(\S+)-baa$", sample)
                if m3:
                    sample = m3.group(1)



                runlane = m2.group(2)
                file = m2.group(3)
                direction = m2.group(4)

            print("line: ", line, val, sample, runlane, file, direction)
            # Setting With Enlargement
            df_.loc[len(df_)+1] = [sample, runlane, int(direction), int(val)]
        print("df_: \n", df_)

        g = df_.groupby(['smp'], as_index=False)
        print("g: \n", g)

        gr = g.agg({'nb_reads': 'sum'}
                    )

        print("gr: \n", gr)

        gr.to_csv("../files/bab-fastq-reads-count.csv", sep=","
                  , header=True
                  , index=True
                  , index_label="idx"
                   )


    def e9_bam_mapping(self):

        #parallel "echo {} && gunzip -c {} | wc -l | awk '{d=\$1; print d/4;}'" ::: raw_reads/*/*/*.fastq.gz

        #parallel "echo {} && gunzip -c {} | wc -l | awk '{d=\$1; print d/4;}'" ::: raw_reads/*/*/*.fastq.gz

        # load data
        smpdf = pd.read_csv("../files/p9_rnaseqc_samples.csv",
                      sep='\t', header=None
                           # ,
                      # index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

        columns = ['smp', 'total_reads', 'mapped_reads']
        df_ = pd.DataFrame(columns=columns)
        dtypes = {'smp': 'object', 'total_reads': 'int', 'mapped_reads': 'int'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)



        print("smpdf: \n", smpdf, smpdf.shape)

        for i in range(smpdf.shape[0]):
        #for i in range(3):
            smp_name = smpdf.iloc[i,0]
            bam_name = smpdf.iloc[i,1]
            #val = smpdf.iloc[i+1,0]

            print("smp_name, bam_name: ", smp_name, bam_name)

            cmd = r"""sambamba flagstat %(bam)s
""" % {"bam": bam_name}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            flags_str = p1.communicate()[0].decode("utf-8").rstrip()
            rc1 = p1.returncode
            print("rc1: ", rc1, flags_str)

            lines = flags_str.split("\n")

            total_reads= mapped_reads = 0
            for line in lines:
                print("ln: ", line)


                m2 = re.match("^(\d+)(.+)in\stotal", line)
                if m2:
                    total_reads = m2.group(1)
                    print("total_reads: ", total_reads)

                m2 = re.match("^(\d+)(.+)mapped\s\(", line)
                if m2:
                    mapped_reads = m2.group(1)

            print("total_reads, mapped_reads: ", total_reads, mapped_reads)
            # Setting With Enlargement
            df_.loc[len(df_)+1] = [smp_name, int(total_reads), int(mapped_reads)]


        p9_smps = []
        p9_ctrls = ['E9_TC_RNeasy_1of2', 'E9_TC_RNeasy_2of2','E9_TC_200_1of2', 'E9_TC_200_2of2']

        p9_sc_smps = ['E9_SC_A09_G__C04', 'E9_SC_C04_G__C61', 'E9_SC_F04_G__C81', 'E9_SC_C07_G__C18',
                      'E9_SC_F12_G__C82', 'E9_SC_H11_G__C95', 'E9_SC_F11_G__C83', 'E9_SC_E11_G__C77',
                      'E9_SC_H02_G__C44', 'E9_SC_F05_G__C80', 'E9_SC_E05_G__C74', 'E9_SC_H05_G__C92',
                      'E9_SC_G08_G__C41', 'E9_SC_G06_G__C85', 'E9_SC_G10_G__C90', 'E9_SC_G11_G__C89',
                      'E9_SC_F07_G__C34', 'E9_SC_H10_G__C96', 'E9_SC_A05_G__C50', 'E9_SC_C12_G__C66',
                      'E9_SC_A07_G__C06', 'E9_SC_B12_G__C60', 'E9_SC_B10_G__C58', 'E9_SC_E03_G__C27',
                      'E9_SC_C09_G__C16', 'E9_SC_E10_G__C78', 'E9_SC_C03_G__C13', 'E9_SC_D03_G__C19',
                      'E9_SC_H06_G__C91', 'E9_SC_A10_G__C52', 'E9_SC_C10_G__C64', 'E9_SC_F10_G__C84',
                      'E9_SC_A08_G__C05']

        p9_smps.extend(p9_ctrls)
        p9_smps.extend(p9_sc_smps)

        print("p9_smps: \n", p9_smps)

        #df_ = df_.set_index("smp")
        df_ = df_.take(df_['smp'][df_['smp'].isin(p9_smps)].sort_values().index)
        print("df_: \n", df_)


        #df_ = df_[df_['smp'].isin(p9_smps)]

        #df_.reset_index(level=0, inplace=True)



        #df_= df_.loc[p9_smps, :]

        df_.to_csv("../files/p9_rnaseqc_mapped.csv", sep=","
                  , header=True
                  , index=True
                  , index_label="idx"
                   )


    def pval_fdr(self):

        for i in range(0,83):
            # binom.pmf(k) = choose(n, k) * p**k * (1-p)**(n-k)
            n= 83
            k = i
            fdr = 3.46e-6
            # fdr = 4.61E-06

            prb = scipy.misc.comb(n, k, exact=True) * fdr**k * (1-fdr)**(fdr-k)
            print("i, prb: ", i,prb)









def todo():
    pass


        # for i in range(edf2_len):
        #     x = edf_blk.iloc[i, :].values
        #     print("x: \n", x)






        # blk_pair_arr = np.empty([nbvals, 2], dtype="float")



        # blkcol = edf.iloc[min_row:max_row, blk_col]
        # df1 = edf.iloc[min_row:max_row, min_col:max_col]

        # print("refcol :", blkcol)
        # print("df1: ", df1)

        # collist = df1.columns.tolist()
        # print("collist: ", collist)

        # columns = ['blk', 'scl']
        # df_ = pd.DataFrame(columns=columns)
        # print("df_: \n", df_)
        # Setting With Enlargement
        # df_.loc[len(df_)+1] = [smp_name, unmapped_str, mapped_str, unique_str, on_mapped_str, on_unique_str]

        # nbvals = (max_col - min_col) * 2 * (max_row - min_row)
        # print("nbvals: ", nbvals)
        #
        # columns = ['blk', 'scl']
        # pairdf = pd.DataFrame(index=np.arange(0, nbvals), columns=columns)
        # print("df_: \n", idx_df, idx_df.dtypes)

        # dtypes = {'blk': 'S100', 'idx_seq': 'S20'}
        # dtypes = {'blk': 'float', 'scl': 'float'}

        # for c in pairdf.columns:
        #     pairdf[c] = pairdf[c].astype(dtypes[c])
        #     print(mydata['y'].dtype)   #=> int64
        # print("idx_df: \n", idx_df, idx_df.dtypes)

        # pair_arr = np.empty([nbvals, 2], dtype="float")


        # for i in range(min_row, max_row):
        #     print("i: ", i)
        #     valcol =  edf.iloc[min_row:max_row, i]
        #     print("valcol: ", valcol)
        #     for j in range(min_col, max_col):
            # for j in range(valcol.shape[0]):

                # vblk = blkcol[j]
                # vcol = valcol[j]
                # print("i, j, vblk, vcol: ", i, j, vblk, vcol)


                # pair_arr[i, 0] = edf.iat[i, j] # edf.iloc[i, j]
                # pair_arr[i, 1] = edf.iat[i, blk_col] #edf.iloc[i, blk_col]


            # if i % 100000 == 0:
            #     print("i: ", i)
            #     print("idx_df: \n", idx_df)
            #     time.sleep(10)
        # df = pd.DataFrame({'blk': pair_arr[:, 0], 'scl': pair_arr[:, 1]})
        # df.set_index("rname", inplace=True)
        # pairdf['blk']=pair_arr
        # print("df: \n", df, df.dtypes)







# Runs all the functions
def main():
    scqc = ScQc()
    # scqc.main_fig2a()
    scqc.main_fig2b()
    scqc.main_fig2c()


    scqc.main_fig2d()

    # scqc.main_fig2e()
    # scqc.prepare_hc_mp_ug()
    #
    # scqc.main_fig_depth_1a()
    #
    # scqc.main_fig_depth_horiz()
    #
    # scqc.fastq_reads()

    # scqc.pval_fdr()

    # scqc.e9_bam_mapping()



# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    main()
