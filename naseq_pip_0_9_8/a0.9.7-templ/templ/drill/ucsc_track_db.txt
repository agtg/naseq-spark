track bw_cov
container multiWig
shortLabel Coverage Data
longLabel Coverage Data
type bigWig 0 30000
viewLimits 0:100
visibility full
maxHeightPixels 150:30:11
aggregate transparentOverlay
showSubtrackColorOnUi on
windowingFunction mean
priority 1.4
configurable on
autoScale on

   {% for item in samples %}
    track {{ item.name }}-subtr
    bigDataUrl big_wig/{{ item.name }}.bw
    shortLabel {{ item.name }}.bw
    longLabel {{ item.idx }}_bw
    parent bw_cov
    type bigWig
    color 125,0,0

  {% endfor %}

