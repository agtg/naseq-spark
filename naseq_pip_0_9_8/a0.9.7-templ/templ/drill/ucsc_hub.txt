hub {{ abbrev }}_hub
shortLabel {{ abbrev }} Raglab rnaSeq tracks
longLabel {{ abbrev }} Raglab rnaSeq Pipeline tracks
genomesFile ucsc_genomes.txt
email dunarel@gmail.com

