# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DRILL_MAX_DIRECT_MEMORY="8G"
DRILL_HEAP="4G"

export DRILL_JAVA_OPTS="-Xms$DRILL_HEAP -Xmx$DRILL_HEAP -XX:MaxDirectMemorySize=$DRILL_MAX_DIRECT_MEMORY -XX:MaxPermSize=512M -XX:ReservedCodeCacheSize=1G -Ddrill.exec.enable-epoll=true"

# Class unloading is disabled by default in Java 7
# http://hg.openjdk.java.net/jdk7u/jdk7u60/hotspot/file/tip/src/share/vm/runtime/globals.hpp#l1622
export SERVER_GC_OPTS="-XX:+CMSClassUnloadingEnabled -XX:+UseG1GC "

#   JAVA_HOME                  The java implementation to use.
#   DRILL_CLASSPATH            Extra Java CLASSPATH entries.
#   DRILL_CLASSPATH_PREFIX     Extra Java CLASSPATH entries that should
#   HADOOP_HOME                Hadoop home
#   HBASE_HOME                 HBase home


export PROJ_DIR={{proj_dir}}
export DRILL_LOC_CONF_DIR=$PROJ_DIR/servers/drill/{{local_host}}
export DRILL_CONF_DIR=$DRILL_LOC_CONF_DIR/conf
#  Alternate drill conf dir. Default is ${DRILL_HOME}/conf

export DRILL_LOG_DIR=$DRILL_LOC_CONF_DIR/log

#    Where log files are stored.  PWD by default.

export DRILL_PID_DIR=$DRILL_LOC_CONF_DIR/pid
#    The pid files are stored. /tmp by default.

#   DRILL_IDENT_STRING   A string representing this instance of drillbit. $USER by default
#   DRILL_NICENESS The scheduling priority for daemons. Defaults to 0.
#   DRILL_STOP_TIMEOUT  Time, in seconds, after which we kill -9 the server if it has not stopped. Default 120 sec.

