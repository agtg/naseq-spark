#!/usr/bin/env python



"""
rnaseq4-v.0.10

"""
from calendar import _localized_day

import subprocess
import optparse
import re
import os
import csv
import sys, random, itertools
#import HTSeq
import shutil

#
import numpy as np
import pandas as pd


import redis
import time
import socket
import json
import sys
import gc
import os
import subprocess

import tables

import itertools
import shutil
import time

import jinja2


# this_function_name = sys._getframe().f_code.co_name
    # the frame and code objects also offer other useful information:
# this_line_number = sys._getframe().f_lineno
# this_filename = sys._getframe().f_code.co_filename
# also, by calling sys._getframe(1), you can get this information
# for the *caller* of the current function.  So you can package
# this functionality up into your own handy functions:
# def whoami():
#     import sys
#
#     return sys._getframe(1).f_code.co_name
#
#
# me = whoami()
# him = callersname()

import imp
import os

pypack = os.environ['NASEQ_PYPACK']

scan_config_rnaseq = imp.load_source('scan_config_rnaseq', "%s/naseqpack/scan_config_rnaseq.py" % (pypack) )

from scan_config_rnaseq import ScanConfigRnaSeq

from rnaseq_utils import *

from naseq import NaSeqPip
from scan_config_dnaseq import ScanConfigDnaSeq
from naseq_utils import *
from redis_controller import RedisController
import shutil

import scipy.sparse

GTF_ENCODE = imp.load_source('GTF_ENCODE', "%s/naseqpack/GTF_ENCODE.py" % (pypack) )

# import GTF_ENCODE                      # https://gist.github.com/slowkow/8101481

import glob

class RnaSeqPip(NaSeqPip, object):

    def __init__(self):

        self.smp_idx = None
        self.queue_work = None
        # this is needed to establish a type
        self.__smp_idx = 0

        # readcnt, fpkm, rnaseqc, rpkmsaturation
        self.nb_rprts = 7

        self.conf = None
        # composition
        self.redis_obj = None

        super(RnaSeqPip, self).__init__()

    # @property
    # def active_queue(self):
    # return self.__active_queue

    # @active_queue.setter
    # def active_queue(self, val):
    # self.__active_queue = val

# ---------------------------------------

    def load_modules(self):

        # load_one_module('raglab/naseq_pip/0.9.4')

        load_one_module('raglab/zlib/1.2.8')
        # load_one_module('gcc/4.8.2')
        # load_one_module('raglab/python/2.7.9')


        load_one_module('raglab/jdk/1.7.0_71')
        load_one_module('raglab/R/3.2.2')

        load_one_module('raglab/picard/1.128')
        load_one_module('raglab/samtools/1.2')
        load_one_module('raglab/bcftools/1.2')
        load_one_module('raglab/sambamba/0.5.4')
        load_one_module('raglab/bowtie2/2.2.3')
        load_one_module('raglab/tophat/2.0.13')

        load_one_module('raglab/bedtools/2.24.0')

        load_one_module('raglab/ucscbio/4.0.0')
        load_one_module('raglab/redis/2.8.18')
        #
        load_one_module('raglab/trimmomatic/0.33')

        load_one_module('raglab/bwa/0.7.12')

        load_one_module('raglab/cufflinks/2.2.1')
        load_one_module('raglab/kallisto/0.42.2.1')

        # load_one_module('mugqic/tools/1.9')





        # cmd = os.popen('modulecmd python load mugqic/tophat/2.0.11')
        #exec(cmd)
        #cmd = os.popen('modulecmd python load raglab/jvarkit/14.11.28')
        #exec(cmd)

        #cmd = os.popen('modulecmd python load raglab/snpEff/3.6')
        #exec(cmd)

        #cmd = os.popen('modulecmd python load jdk64/7u60')
        #exec(cmd)
        #cmd = os.popen('modulecmd python load mugqic/GenomeAnalysisTK/3.2-2 ')
        #exec(cmd)


    def init_proj(self):

        self.load_modules()

        cnf = ScanConfigRnaSeq()
        cnf.scan_local()

        cnf.set_proj_dirs()
        cnf.set_proj_files_path_names()

        cnf.load_sample_lanes()
        cnf.load_sample_names()

        self.conf = cnf

        # print self.conf.makeReport() #self.conf.items()

        # redis object
        self.redis_obj = RedisController(cnf)

        # import values from config
        # the readcount gtf is fixed to the bulk for the analysis
        self.c3p_size = self.conf.c3p_size
        self.c3p_smp_idx = self.conf.c3p_bulk_idx

        # self.c3p_bulk_name = self.conf.c3p_bulk_name


    @property
    def align_smp_dir(self):
        this_dir = os.path.join(self.conf.align2_dir, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def rseqc_smp_dir(self):
        this_dir = os.path.join(self.conf.rseqc_dir, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def rseqc_smp_prefix(self):
        return os.path.join(self.rseqc_smp_dir, self.smp_name)

    # alias
    @property
    def loc_dir(self):
        return self.align_smp_dir

    @property
    def spln_tophat_dir(self):
        return os.path.join(self.spln_align_lib_dir, "tophat_out")


    @property
    def spln_tophat_out(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.%s.33.tophat.out""" % (self.spln_smp, self.spln_lib))

    @property
    def spln_acc_am(self):
        return os.path.join(self.spln_tophat_dir, "accepted_hits.bam")

    @property
    def spln_sorted_bam(self):
        return os.path.join(self.spln_tophat_dir, "accepted_hits_sorted.bam")
        

    @property
    def spln_unm_am(self):
        return os.path.join(self.spln_tophat_dir, "unmapped.bam")

    @property
    def spln_unm_fix_am(self):
        return os.path.join(self.spln_tophat_dir, "unmapped_fixup.bam")

    @property
    def spln_merged_bam(self):
        return os.path.join(self.spln_tophat_dir, "merged.bam")

# ---------------------------------------

    # this name is identical to the dnaSeq mer_smp_bam
    # it is used to keep same rnaseq variable names to link to rnaseq alignenment data
    # in rnvar projects
    @property
    def allmerg_bam(self):
        return os.path.join(self.align_smp_dir, "%s.mer.bam" % self.smp_name)

    @property
    def smp_r1_fq_gz(self):
        return os.path.join(self.align_smp_dir, "%s.r1.fastq.gz" % self.smp_name)

    @property
    def smp_r2_fq_gz(self):
        return os.path.join(self.align_smp_dir, "%s.r2.fastq.gz" % self.smp_name)



    @property
    def rmdup_bam(self):
        return os.path.join(self.align_smp_dir, "%s.merged.rmdup.bam" % self.smp_name)

    @property
    def smp_rmdup_ml_metrics(self):
        return os.path.join(self.align_smp_dir, "%s.rmdup_ml.metrics" % self.smp_name)

    @property
    def smp_markdup_mlr_metrics(self):
        return os.path.join(self.align_smp_dir, "%s.markdup_mlr.metrics" % self.smp_name)

    @property
    def smp_accepted_err_ml_csv(self):
        return os.path.join(self.align_smp_dir, "accepted_err_ml.csv")

    @property
    def smp_accepted_err_mlr_csv(self):
        return os.path.join(self.align_smp_dir, "accepted_err_mlr.csv")

    @property
    def smp_unmapped_err_mlr_csv(self):
        return os.path.join(self.align_smp_dir, "unmapped_err_mlr.csv")

# ------------------------------ gtf related genes and transcripts dataframes

    @property
    def genes_tracking_ids_df(self):
        return self.__genes_tracking_ids_df

    @genes_tracking_ids_df.setter
    def genes_tracking_ids_df(self, val):
        self.__genes_tracking_ids_df = val

    @property
    def isofs_tracking_ids_df(self):
        return self.__isofs_tracking_ids_df

    @isofs_tracking_ids_df.setter
    def isofs_tracking_ids_df(self, val):
        self.__isofs_tracking_ids_df = val

    @property
    def all_isofs_fpkm_tracking_ids_df(self):
        return self.__all_isofs_fpkm_tracking_ids_df

    @all_isofs_fpkm_tracking_ids_df.setter
    def all_isofs_fpkm_tracking_ids_df(self, val):
        self.__all_isofs_fpkm_tracking_ids_df = val

    @property
    def all_isofs_fpkm_tracking_ids_f(self):
        return os.path.join(self.conf.matr_dir, "all_isofs_fpkm_tracking_ids.csv")


    @property
    def isofs_matrix_df(self):
        return self.__isofs_matrix_df

    @isofs_matrix_df.setter
    def isofs_matrix_df(self, val):
        self.__isofs_matrix_df = val



# ---------------------------- readcount properties ----------------------------

    @property
    def readcnt_prefix(self):
        return self.__readcnt_prefix

    @readcnt_prefix.setter
    def readcnt_prefix(self, val):
        self.__readcnt_prefix = val

    @property
    def readcnt_gtf(self):
        return self.__readcnt_gtf

    @readcnt_gtf.setter
    def readcnt_gtf(self, val):
        self.__readcnt_gtf = val


    # @property
    # def readcounts_csv(self):
    #     return os.path.join(self.align_smp_dir, "%s.readcounts.csv" % self.smp_name)





    def set_sample_dirs(self):
        pass


    def set_sample_files_path_names(self):

        # self.lcnf.merged_bam = os.path.join(self.conf.align_dir,self.lcnf.s_name,"%s.merged.bam" % self.lcnf.s_name)
        # self.lcnf.karyo_bam = os.path.join(self.conf.align2_dir,self.lcnf.s_name,"%s.merged.karyotypic.bam" % self.lcnf.s_name)
        # print "self.lcnf.karyo_bam: ",self.lcnf.karyo_bam


        # self.lcnf.s_orig_bam_name = "%s.%s.sorted.bam" % (self.s_name,self.s_lib_name)
        #self.s_orig_bam = os.path.join(self.bam_reads_dir,self.s_orig_bam_name).strip('\n')
        #print "self.s_orig_bam: ", self.s_orig_bam 
        #self.s_sorted_bam = os.path.join(self.loc_dir,"%s.sorted.bam" % self.s_name).strip('\n')
        #print "self.s_sorted_bam: ", self.s_sorted_bam 
        #self.sort_and_index_bam_log = os.path.join(self.loc_dir,"sort_and_index_bam.log").strip('\n')
        #print "self.sort_and_index_bam_log: ", self.sort_and_index_bam_log 
        pass


    def set_sample_lane(self, idx):
        # self.s_name = "TC_Nextera_NTC"
        #        self.s_lib_name = "MPS073391"
        #        self.s_run="runX_X"

        #self.s_name = "Paul3_SC_C11_G_C65"
        #self.s_lib_name = "MPS073409"
        #self.s_run="run1704_1"
        lane = self.sample_lanes[int(idx)]
        print(("lane: ", lane))
        self.s_name = lane["Name"]
        #print self.s_name
        self.s_lib_name = lane["Library Barcode"]
        #print self.s_lib_name
        self.s_run = lane["Run"]
        self.s_run_region = "run%s_%s" % (lane["Run"], lane["Region"])


        #self.set_sample_dirs()
        #self.set_files_path_names2()


    def set_lane_dirs(self):


        #
        self.fastqc_dir = os.path.join(self.proj_dir, "fastqc", self.s_name, self.s_run_region)
        if not os.path.exists(self.fastqc_dir):
            os.makedirs(self.fastqc_dir)

        self.s_align_bt2_dir = os.path.join(self.proj_dir, "files-bt2")

        self.tophat_dir = os.path.join(self.proj_dir, "tophat3", self.s_name, self.s_run_region)
        if not os.path.exists(self.tophat_dir):
            os.makedirs(self.tophat_dir)

        self.bwa_dir = os.path.join(self.proj_dir, "bwa3", self.s_name, self.s_run_region)
        if not os.path.exists(self.bwa_dir):
            os.makedirs(self.bwa_dir)


    def set_lane_files_path_names(self):
        # %(raw_reads_dir)/%(s_name)/%(s_run)/%(s_name).%(s_lib_name).33.pair1.fastq.gz
        self.s_fq_name = """
%(raw_reads_dir)s/%(name)s/%(run_region)s/%(name)s.%(lib_name)s.33""" \
                         % {"raw_reads_dir": self.raw_reads_dir, "name": self.s_name, "run_region": self.s_run_region,
                            "lib_name": self.s_lib_name}

        self.s_fq_r1 = os.path.join(self.s_fq_name + ".pair1.fastq.gz").strip('\n')
        self.s_fq_r2 = os.path.join(self.s_fq_name + ".pair2.fastq.gz").strip('\n')

        # uncompressed samples
        self.s_pe_r1 = os.path.join(self.loc_dir, "p1.fastq").strip('\n')
        self.s_pe_r2 = os.path.join(self.loc_dir, "p2.fastq").strip('\n')
        #compressed samples
        self.s_pe_c_r1 = os.path.join(self.loc_dir, "p1.fastq.gz").strip('\n')
        self.s_pe_c_r2 = os.path.join(self.loc_dir, "p2.fastq.gz").strip('\n')
        #saved sample
        self.s_pe_bkp_r1 = os.path.join(self.loc_dir, "p1-bkp.fastq.gz").strip('\n')
        self.s_pe_bkp_r2 = os.path.join(self.loc_dir, "p2-bkp.fastq.gz").strip('\n')

        self.s_adapter_1 = os.path.join(self.proj_dir, "adapters/adapters-nextera.fa").strip('\n')
        self.s_adapter_2 = os.path.join(self.proj_dir, "adapters/david-nextera.fa").strip('\n')
        self.s_adapter_3 = os.path.join(self.proj_dir, "adapters/adapters-nextera-s-n-3.fa").strip('\n')


        #fqc_tst_f = "sa.sb.txt"
        fqc_tst_f = "transp.n.txt"
        #fqc_tst_f = "n.txt"
        self.s_fqc_ada = os.path.join(self.proj_dir, "adapters/%s" % fqc_tst_f).strip('\n')
        self.s_fqc_con = os.path.join(self.proj_dir, "adapters/%s" % fqc_tst_f).strip('\n')

        #pair to align
        self.s_rp1 = os.path.join(self.loc_dir, "trimmed.pair1.fastq.gz").strip('\n')
        #lost
        self.s_rs1 = os.path.join(self.loc_dir, "trimmed.single1.fastq.gz").strip('\n')
        #pair to align
        self.s_rp2 = os.path.join(self.loc_dir, "trimmed.pair2.fastq.gz").strip('\n')
        #lost
        self.s_rs2 = os.path.join(self.loc_dir, "trimmed.single2.fastq.gz").strip('\n')
        #log
        self.s_trim_out = os.path.join(self.loc_dir, "trimmed.out").strip('\n')

        #log
        self.s_tophat_out = os.path.join(self.tophat_dir, "tophat.out").strip('\n')

        self.s_accepted_hits_bam = os.path.join(self.tophat_dir, "accepted_hits.bam")
        self.s_unmapped_bam = os.path.join(self.tophat_dir, "unmapped.bam")
        self.s_unmapped_paired_bam = os.path.join(self.tophat_dir, "unmapped_paired.bam")
        self.s_unmapped_paired_sort_bam = os.path.join(self.tophat_dir, "unmapped_paired_sort.bam")
        self.s_unmapped_paired_sort_bam_bam = os.path.join(self.tophat_dir, "unmapped_paired_sort.bam.bam")
        self.s_unmapped_paired_sort_flt_bam = os.path.join(self.tophat_dir, "unmapped_paired_sort_flt.bam")
        self.s_unmapped_fastx = os.path.join(self.tophat_dir, "unmapped_fastx.fq")
        self.s_unmapped_fastx1 = os.path.join(self.tophat_dir, "unmapped_fastx.1.fq")
        self.s_unmapped_fastx2 = os.path.join(self.tophat_dir, "unmapped_fastx.1.fq")



        #bwa
        self.s_bwa_sorted = os.path.join(self.bwa_dir, "aln-pe-sorted.bam").strip('\n')
        self.s_bwa_out = os.path.join(self.bwa_dir, "bwa.out").strip('\n')


        #export S_FQ_R2=${RAW_READS}/${S_NAME}/${S_RUN}/${S_NAME}.${S_LIB_NAME}.33.pair2.fastq.gz


        #self.db_dir = os.path.join(self.proj_dir,"db")
        #self.db_sqlite = os.path.join(self.db_dir,"vcfdb.sqlite")
        #self.vcf_files_dir = os.path.join(self.proj_dir,"vcf-files")

        print(("proj_dir: ", self.proj_dir))
        #print "db_sqlite: ", self.db_sqlite
        #print "vcf_files_dir: ", self.vcf_files_dir

        self.all_vcf_files_a = [
            "P4_GD_UN", "P4_TC_200",
            "P4_SC_A02_G_C02", "P4_SC_A08_G_C05", "P4_SC_B03_G_C07",
            "P4_SC_B08_G_C11", "P4_SC_B11_G_C59", "P4_SC_C03_G_C13",
            "P4_SC_C04_G_C61", "P4_SC_C09_G_C16", "P4_SC_D02_G_C20",
            "P4_SC_E03_G_C27", "P4_SC_E07_G_C28", "P4_SC_E10_G_C78",
            "P4_SC_F02_G_C32", "P4_SC_F08_G_C35", "P4_SC_G07_G_C40", "P4_SC_H02_G_C44",
            "P4_SC_H07_G_C46", "P4_SC_H09_G_C48",
            "TI_GD_1521", "TI_GD_1522",
            "TI_GD_1523", "TI_GD_1528"
        ]


    def subsample(self, fraction):
        # 10%  0.1
        # fraction = 1
        in1 = iter(HTSeq.FastqReader(self.s_fq_r1))
        in2 = iter(HTSeq.FastqReader(self.s_fq_r2))
        out1 = open(self.s_pe_r1, "w")
        out2 = open(self.s_pe_r2, "w")

        for read1, read2 in zip(in1, in2):
            if random.random() < fraction:
                read1.write_to_fastq_file(out1)
                read2.write_to_fastq_file(out2)
        #
        out1.close()
        out2.close()

    def fastqc_subsample(self):
        cmd = FASTQC % {"bin_dir": self.bin_dir, "fastqc_dir": self.fastqc_dir, \
                        "adapters": self.s_fqc_ada, \
                        "contaminants": self.s_fqc_con, \
                        "p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2}
        print(("cmd: >%s<" % cmd))
        value = runBash(cmd)
        print(value)
        print((report(value, "cmd")))

    def subsample_compress(self):

        # first remove files to not question interactive
        cmd = "rm -fr %s %s" % (self.s_pe_c_r1, self.s_pe_c_r2)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "result: ")
        # compress
        cmd = PIGZ % {"p1": self.s_pe_r1, "p2": self.s_pe_r2}
        print(("cmd: ", cmd))
        value = runBash(cmd)
        print((report(value, "result: ")))


    def trim(self):
        print("inside trimming...")        # thisday = runBash(DAY)
        #report(thisday, "DAY")

        #cmd= TRIM % {"p1": self.s_fq_r1 , "p2": self.s_fq_r2, "adapter": self.s_adapter, "rp1": self.s_rp1, "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        cmd = TRIM % {"p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2, "adapter": self.s_adapter_3, "rp1": self.s_rp1,
                      "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        #cmd= TRIM % {"p1": self.s_fq_r1 , "p2": self.s_fq_r2, "adapter": self.s_adapter, "rp1": self.s_rp1, "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        print(("cmd: %s" % cmd))
        value = runBash(cmd)
        print(value)
        report(value, "cmd")

    def trim2(self):
        print("inside trimming...")
        # thisday = runBash(DAY)
        #report(thisday, "DAY")

        #cmd= TRIM % {"p1": self.s_fq_r1 , "p2": self.s_fq_r2, "adapter": self.s_adapter, "rp1": self.s_rp1, "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        cmd = TRIM % {"p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2, "adapter": self.s_adapter, "rp1": self.s_rp1,
                      "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        #cmd= TRIM % {"p1": self.s_fq_r1 , "p2": self.s_fq_r2, "adapter": self.s_adapter, "rp1": self.s_rp1, "rs1": self.s_rs1, "rp2": self.s_rp2, "rs2": self.s_rs2, "trim_out": self.s_trim_out}
        cmd = cmd.strip('\n\r')
        print(("cmd: >%s<" % cmd))
        value = runBash(cmd)
        print(value)
        print((report(value, "cmd")))


    def trim_to_subsample(self):

        if os.path.exists(self.s_rp1):
            shutil.copy2(self.s_rp1, self.s_pe_c_r1)
            os.remove(self.s_rp1)

        if os.path.exists(self.s_rp2):
            shutil.copy2(self.s_rp2, self.s_pe_c_r2)
            os.remove(self.s_rp2)

        os.path.exists(self.s_rs1) and os.remove(self.s_rs1)
        os.path.exists(self.s_rs2) and os.remove(self.s_rs2)
        os.path.exists(self.s_trim_out) and os.remove(self.s_trim_out)

    def star_align(self):
        cmd = r"""STAR --runMode alignReads --runThreadN 12 --genomeDir \
    ../sequence/star_ix --genomeLoad LoadAndKeep \
    --readFilesCommand zcat \
    --readFilesIn /gs/project/wst-164-aa/expr/ex1-gq13-v.0.9/reads/CD45nCD8p_TC_200_1of2-10/run2277_1/CD45nCD8p_TC_200_1of2-10.MPS084738.t30l45.pair1.fastq.gz \
                  /gs/project/wst-164-aa/expr/ex1-gq13-v.0.9/reads/CD45nCD8p_TC_200_1of2-10/run2277_1/CD45nCD8p_TC_200_1of2-10.MPS084738.t30l45.pair2.fastq.gz \
    --outSAMtype BAM SortedByCoordinate \
    --limitBAMsortRAM 10000000000 --outSAMunmapped Within
"""

    def tophat(self):
        cmd = TOPHAT % {"sample": self.s_name, "run": self.s_run, "align_dir": self.tophat_dir,
                        "align_bt2": self.s_align_bt2_dir, "p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2,
                        "out": self.s_tophat_out}
        print(("cmd: ", cmd))
        value = runBash(cmd)
        print(("value: ", value))
        report(value, "cmd")


    def tophat_unmapped_to_subsample(self):
        cmd = BAM_TO_FASTQ % {"bam": self.s_unmapped_bam, "p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2}
        print(("cmd: ", cmd))
        value = runBash(cmd)
        print(("value: ", value))
        report(value, "cmd")

    def tophat_unmapped_repair_pe(self):
        # Remove any reads without a matching pair
        cmd = "samtools view -f1 -b %s > %s" % (self.s_unmapped_bam, self.s_unmapped_paired_bam)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "cmd")
        #Sort the reads according to name
        cmd = "samtools sort -n %s %s " % (self.s_unmapped_paired_bam, self.s_unmapped_paired_sort_bam)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "cmd")
        #repair mate flag
        cmd = "samtools view -h %s | %s/bam_re-pair.py | samtools view -bSo %s -" \
              % (self.s_unmapped_paired_sort_bam_bam, self.pysrc_dir, self.s_unmapped_paired_sort_flt_bam)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "cmd")

    def tophat_unmapped_flt_to_subsample(self):
        cmd = BAM_TO_FASTQ2 % {"bam": self.s_unmapped_paired_sort_flt_bam, "fastx": self.s_unmapped_fastx}
        print(("cmd: ", cmd))
        value = runBash(cmd);
        report(value, "cmd")
        # copy over
        cmd = "cp %s %s" % (self.s_unmapped_fastx1, self.s_pe_r1)
        print(("cmd: ", cmd))
        value = runBash(cmd);
        report(value, "cmd")
        cmd = "cp %s %s" % (self.s_unmapped_fastx2, self.s_pe_r2)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "cmd")
        #compress
        self.subsample_compress()


    def bwa(self):
        cmd = BWA % {"p1": self.s_pe_c_r1, "p2": self.s_pe_c_r2, "s_bwa_sorted": self.s_bwa_sorted,
                     "out": self.s_bwa_out}
        print(("cmd: ", cmd))
        value = runBash(cmd)
        print(("value: ", value))
        report(value, "cmd")

    def extract_softclips(self):
        cmd = EXTRACT_SOFTCLIPS % {"bam": self.s_bwa_sorted}
        print(("cmd: ", cmd))
        value = runBash(cmd)
        print(("value: ", value))
        report(value, "cmd")


    def save_subsample(self):
        shutil.copy2(self.s_pe_c_r1, self.s_pe_bkp_r1)
        shutil.copy2(self.s_pe_c_r2, self.s_pe_bkp_r2)

    def restore_subsample(self):
        shutil.copy2(self.s_pe_bkp_r1, self.s_pe_c_r1)
        shutil.copy2(self.s_pe_bkp_r2, self.s_pe_c_r2)


    def process_accepted_hits(self):

        # need an index
        runBash("samtools index %s" % self.s_accepted_hits_bam)

        hit_cnts = {"genome": 0, "spikes": 0}
        hit_cnts
        p = re.compile('spike(\d*)')

        f = pysam.Samfile(self.s_accepted_hits_bam, 'rb')
        print((f.count()))
        print((f.getrname(83)))
        #for alignedread in samfile.fetch('1', 100, 120):
        #for alignedread in samfile.fetch():
        for aln in f.fetch(until_eof=True):
            contig = f.getrname(aln.tid)
            m = p.match(contig)
            if m:
                #print "m: ", m.group(0), m.group(1)
                hit_cnts["spikes"] += 1
            else:
                #print 'contig: ',contig
                hit_cnts["genome"] += 1

        f.close()
        print(("hit_cnts genome,spikes: ", hit_cnts["genome"], hit_cnts["spikes"]))

        # set up the modifying iterators

    # it = pysam_in.fetch(until_eof=True)
    #
    #            # function to check if processing should start
    #            pre_check_f = lambda x: None
    #
    #            if "unset-unmapped-mapq" in options.methods:
    #                def unset_unmapped_mapq(i):
    #                    for read in i:
    #                        if read.is_unmapped:
    #                            read.mapq = 0
    #                        yield read
    #                it = unset_unmapped_mapq(it)
    #
    # def test(self):
    #                     #bam_writer = HTSeq.FastqReader
    #                      #samfile = pysam.Samfile("ex1.bam", "rb")
    #
    #     in1 = iter(HTSeq.FastqReader("/home/dbadescu/expr/pip-dbg/tophat3/Paul3_SC_C11_G_C65/run1704_1/test.1.fastq"))
    #     in2 = iter(HTSeq.FastqReader("/home/dbadescu/expr/pip-dbg/tophat3/Paul3_SC_C11_G_C65/run1704_1/test.2.fastq"))
    #     #out1 = open( self.s_pe_r1, "w" )
    #     #out2 = open( self.s_pe_r2, "w" )
    #     out1 = open("/home/dbadescu/expr/pip-dbg/tophat3/Paul3_SC_C11_G_C65/run1704_1/test.1.fa", "w")
    #     out2 = open("/home/dbadescu/expr/pip-dbg/tophat3/Paul3_SC_C11_G_C65/run1704_1/test.2.fa", "w")
    #
    #     for read1, read2 in itertools.izip(in1, in2):
    #         seq1 = HTSeq.Sequence(read1.seq, read1.name)
    #         seq2 = HTSeq.Sequence(read2.seq, read2.name)
    #
    #         seq1.write_to_fasta_file(out1)
    #         seq2.write_to_fasta_file(out2)
    #     #
    #     out1.close()
    #     out1.close()
    #
    #     #myseq = HTSeq.Sequence( "ACCGTTAC", "my_sequence" )
    #     #my_fasta_file = open( "/home/dbadescu/expr/pip-dbg/tophat3/Paul3_SC_C11_G_C65/run1704_1/test.fa", "w" )
    #     #myseq.write_to_fasta_file( my_fasta_file )
    #     #my_fasta_file.close()
    #
    #
    #
    #     #reads = pysam.Fastafile(, "wb")
    #     #txt = ""
    #     #reads.write(read)
    #
    #     #pairedreads.close()
    #     #samfile.close()

    def sort_and_index_bam(self):
        #Remove any reads without a matching pair
        cmd = "cat %s | java -Djava.io.tmpdir=/gs/scratch/dbadescu -XX:ParallelGCThreads=4 -Xmx25G -jar ${PICARD_HOME}/SortSam.jar INPUT=/dev/stdin CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate OUTPUT=%s MAX_RECORDS_IN_RAM=3750000 2> %s" % (
            self.s_orig_bam, self.s_sorted_bam, self.sort_and_index_bam_log)
        print(("cmd: ", cmd))
        value = runBash(cmd)
        report(value, "cmd")

    def gen_sample_subm(self):

        #header
        self.subm_dir = os.path.join(self.proj_dir, "subm")
        if not os.path.exists(self.subm_dir):
            os.makedirs(self.subm_dir)

        ####################  COMMAND FILES

        for i in range(0, self.sample_lanes.__len__()):
            #sample folders and files
            print(("i: ", i))
            sample_subm_cmd_fname = "sa-%s.sh" % ("{:0>2d}".format(i))

            target_stat_cmd_abs = os.path.join(self.subm_dir, sample_subm_cmd_fname)
            cmd_raw_txt = """
#!/bin/bash

module load mugqic/picard/1.98
module load mugqic/samtools/0.1.18

../pysrc/dnaseq.py --work-sample %(id)s

echo "ok works.."
\n
""" % {"id": i}

            cmdf = open(target_stat_cmd_abs, "wb")
            cmdf.write(cmd_raw_txt)
            cmdf.close()
            #make it executable
            # os.chmod(target_stat_cmd_abs, "0755")


        ############################# SUBMISSION FILE
        # only one file for all samples
        #open file
        submfname = os.path.join(self.subm_dir, 'run-all.sh')
        submf = open(submfname, "wb")
        #header
        subm_raw_txt = """
#!/bin/bash
TIMESTAMP=`date +%FT%H.%M.%S`
WORK_DIR=`pwd`
JOB_OUTPUT_ROOT=$WORK_DIR/job_output
"""
#        print >> submf, subm_raw_txt

        #each sample
        for i in range(0, self.sample_lanes.__len__()):
            subm_samp_raw_txt = """
##################################################
JOB_NAME=%(job_name)s.%(id)s
TARGET_JOB_ID=$(cat $WORK_DIR/sa-%(id)s.sh | qsub -V -m ae -W umask=0002 -A wst-164-aa -d $WORK_DIR -j oe -o $JOB_OUTPUT_ROOT -N $JOB_NAME -l walltime=2:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
##################################################
""" % {"job_name": "saWork", "id": "{:0>2d}".format(i)}
#            print >> submf, subm_samp_raw_txt


        #end the submission file
        submf.close()
        #make it executable
        os.chmod(submfname, "0755")

    def main(self):
        pass

#     def genotype_gvcfs2(self):
#         var_txt = ""
#         print "self.conf.sample_names: "
#         samples = self.conf.sample_names
#         #print samples.shape[0]
#         #print samples.shape[1]
#
#         #for smp in self.conf.sample_names:
#         for smp_idx in range(samples.shape[0]):
#             print "smp_idx: ", smp_idx
#             smp = samples.iloc[smp_idx, 0]
#             print "smp: ", smp
#
#             smp_txt = r"""-V %(aln_dir)s/%(smp)s/%(smp)s.hc.g.vcf \
# """ % {"aln_dir": self.conf.align_dir, "smp": smp}
#             #print "smp_txt: ", smp_txt
#             var_txt += smp_txt
#
#         cmd = r"""java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx27G -jar $GATK_JAR \
# -nt 24 \
# -R $RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh37/genome/Homo_sapiens.GRCh37.fa \
# -T GenotypeGVCFs \
# %s -o %s/hc-merged.vcf
# """ % (var_txt, self.conf.variants2_dir)
#         print cmd, runBash(cmd)
#
#     def make_rename_list(self, caller):
#         #make list of rename
#         ifile = open(os.path.join(self.conf.variants2_dir, 'sample-rename.csv'), "wb")
#         for smp_idx in range(self.conf.sample_names.shape[0]):
#             print "smp_idx: ", smp_idx
#             smp = self.conf.sample_names.iloc[smp_idx, 0]
#             print "smp: ", smp
#
#             ifile.write("""%(smp)s\t%(smp)s-%(clr)s\n""" % {"smp": smp, "clr": caller})
#         ifile.close()
#
#
#     def rename_variants(self):
#         #need specific mugqic/samtools/0.1.19
#         cmd = os.popen('modulecmd python load mugqic/samtools/0.1.19')
#         exec (cmd)
#
#         #first bring samtools bcf to vcf
#         cmd = r"""bcftools view %s/allSamples.merged.bcf > %s/mp-merged.vcf
# """ % (self.conf.variants_dir, self.conf.variants2_dir)
#         print cmd, runBash(cmd)
#
#         #make rename list samtools
#         self.make_rename_list("MP")
#
#         #and rename
#         cmd = r"""vcfrenamesamples -f %(v2dir)s/sample-rename.csv %(v2dir)s/mp-merged.vcf > %(v2dir)s/mp-merged.renamed.vcf
# """ % {"v2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#         #make rename list haplotype caller
#         self.make_rename_list("HC")
#
#         #and rename
#         cmd = r"""vcfrenamesamples -f %(v2dir)s/sample-rename.csv %(v2dir)s/hc-merged.vcf > %(v2dir)s/hc-merged.renamed.vcf
# """ % {"v2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#
#     def combine_variants(self):
#
#         cmd = r"""java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=22 -Xmx27G -jar $GATK_JAR \
# -nt 22 \
# --genotypemergeoption REQUIRE_UNIQUE \
# -R $RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh37/genome/Homo_sapiens.GRCh37.fa \
# -T CombineVariants \
# -V %(v2dir)s/mp-merged.renamed.vcf \
# -V %(v2dir)s/hc-merged.renamed.vcf \
# -o %(v2dir)s/mp-hc-merged.vcf
# """ % {"v2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#
#     def snpid(self):
#
#         cmd = r"""java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=22 -Xmx27G \
# -jar ${SNPEFF_HOME}/SnpSift.jar annotate \
# $RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh37/annotations/Homo_sapiens.GRCh37.dbSNP141.vcf \
# %(var2dir)s/mp-hc-merged.vcf > %(var2dir)s/mp-hc-merged.snpid.vcf
# """ % {"var2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#     def snpeff(self):
#
#         cmd = r"""java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=22 -Xmx27G \
# -jar ${SNPEFF_HOME}/snpEff.jar eff -c ${SNPEFF_HOME}/snpEff.config  -o vcf -i vcf \
# -csvStats -stats hc-merged.snpid.snpeff.vcf.stats.csv GRCh37.74 \
# %(var2dir)s/mp-hc-merged.snpid.vcf > %(var2dir)s/mp-hc-merged.snpid.snpeff.vcf
# """ % {"var2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#     def dbnsfp(self):
#
#         cmd = r"""java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=22 -Xmx27G \
# -jar ${SNPEFF_HOME}/SnpSift.jar dbnsfp \
# -v $RAGLAB_INSTALL_HOME/genomes/dbnsfp/dbNSFP2.4.txt.gz \
# %(var2dir)s/mp-hc-merged.snpid.snpeff.vcf > %(var2dir)s/mp-hc-merged.snpid.snpeff.dbnsfp.vcf
# """ % {"var2dir": self.conf.variants2_dir}
#         print cmd, runBash(cmd)
#
#
#     def copy_reordered(self):
#         pass
#         #print samples.shape[0]
#         #print samples.shape[1]
#         #dir_name = r"""%(align_dir)s/%(smp_name)s""" % {"align_dir":self.conf.align2_dir, "smp_name": smp_name}
#         #src_dir_name = os.path.join(self.conf.align_dir,self.lcnf.s_name)
#
#
#         #dst_f = os.path.join(self.lcnf.loc_dir,file_name)
#         #print "dst_f: ", dst_f
#
#         # shutil.copy(self.lcnf.karyo_bam, self.lcnf.src_f, dst_f)

    # def merge_lanes(self, smp_idx):

    # parameters are found in attributes
    def merge_lanes(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))


        lanes_df = self.conf.sample_lanes

        print("lanes_df: ")
        print(lanes_df)
        print("-------------")
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print(lanes_df)
        print("-------------")

        exit_codes = []

        lanes_st_acc = ""
        accepted_err_arr = []

        # scratch_d = os.getenv('SCRATCH', "")

        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx
            # print row
            print(("spln_row: ", self.spln_row))
            print(("accepted_f: ", self.spln_acc_am))
            
            # check alignment files
            if os.path.exists(self.spln_acc_am):
                
                # sort lane
                # should be extracted
                cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(bamin)s -o %(bamout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "bamin": self.spln_acc_am, "bamout": self.spln_sorted_bam}
                print(cmd)
                p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_tophat_dir, stdout=subprocess.PIPE)
                p1.wait()
                rc1 = p1.returncode
                print(("rc1: ", rc1))
                exit_codes.append(rc1)                
                
                # add it to the list
                lanes_st_acc += r"""%s """ % self.spln_sorted_bam
                    
            else:
                accepted_err_arr.append(self.spln_align_lib_dir)

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_df.to_csv(self.smp_accepted_err_ml_csv, sep=',', header=False, index=False)

            print(("accepted_err_arr: ", accepted_err_arr))
            # print("smp_accepted_err_ml_csv: ", self.smp_accepted_err_ml_csv)

        a1 = os.path.join(self.align_smp_dir, "a1.bam")
        a2 = os.path.join(self.align_smp_dir, "a2.bam")
        a3 = os.path.join(self.align_smp_dir, "a3.bam")
        a4 = os.path.join(self.align_smp_dir, "a4.bam")
        a5 = os.path.join(self.align_smp_dir, "a5.bam")
        
        # clean input links
        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)

        # acc_merg1_f = os.path.join(self.align_smp_dir, "acc_merg1.bam")
       # clean input files
#        for i in [acc_merg1_f]:
#            if os.path.exists(i):
#                os.remove(i)


        f1 = os.path.join(self.align_smp_dir, "%s.acc.mer.f1.bam" % self.smp_name)
        f1_bai = os.path.join(self.align_smp_dir, "%s.acc.mer.f1.bam.bai" % self.smp_name)

        f2 = os.path.join(self.align_smp_dir, "%s.acc.mer.f2.bam" % self.smp_name)
        f2_bai = os.path.join(self.align_smp_dir, "%s.acc.mer.f2.bai" % self.smp_name)

        # f3 = os.path.join(self.align_smp_dir, "f3.bam")
        # f4 = os.path.join(self.align_smp_dir, "f4.bam")

        # f4_txt = os.path.join(self.align_smp_dir, "f4.txt")

        # clean input files
        for i in [f1, f1_bai, f2, f2_bai]:
            if os.path.exists(i):
                os.remove(i)


        if not os.path.exists(a1):
            os.mkfifo(a1)
        if not os.path.exists(a2):
            os.mkfifo(a2)
        if not os.path.exists(a3):
            os.mkfifo(a3)
        if not os.path.exists(a4):
            os.mkfifo(a4)
        if not os.path.exists(a5):
            os.mkfifo(a5)
            

        # redirection is important
        lanes_arr = lanes_st_acc.split()

        print(("lanes_st_acc: ", lanes_st_acc))
        print(("lanes_arr: ", lanes_arr))
        lanes_len = lanes_arr.__len__()

        if lanes_len == 1:
            os.symlink(lanes_arr[0], f1)
        else:
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st_acc, "fout": f1}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print(("rc1: ", rc1))
            exit_codes.append(rc1)
            
        
         # coordinate sort
        #cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, f2, f1)
        #print(cmd)
        #p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
         
        # add read groups
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": f1, "fout": f2, "smp": self.smp_name}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print(("rc3: ", rc3))
        exit_codes.append(rc3)

        # temporary file
        for i in [f1, f1_bai]:
            if os.path.exists(i):
                os.remove(i)
         
 
         
         
#        if not os.path.exists(a2):
#            os.mkfifo(a2)
#        if not os.path.exists(a3):
#            os.mkfifo(a3)
#        if not os.path.exists(a4):
#            os.mkfifo(a4)
#        if not os.path.exists(a5):
#            os.mkfifo(a5)
        
                
        
        # coordinate sort again
        #cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, a4, a3)
        #print(cmd)
        #p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        
        # reorder
#        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
#-XX:ParallelGCThreads=4 \
#-Xms1000M -Xmx1700M \
#-Dsamjdk.use_async_io=true \
#-jar ${PICARD_JAR} ReorderSam \
#VALIDATION_STRINGENCY=SILENT \
#CREATE_INDEX=false \
#INPUT=%(fin)s \
#OUTPUT=%(fout)s \
#REFERENCE=%(ref_genome)s
#""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": a4, "fout": acc_merg1_f, "ref_genome": self.conf.ref_gen_fa}
 #       print(cmd)
 #       p5 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

#        exit_codes_stage = [pp.wait() for pp in [p3, p4, p5]]
#        print("exit_codes stage: ", exit_codes_stage)
#        exit_codes.extend(exit_codes_stage)
 
       # remove duplicates
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=true \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": f2, "fout": self.rmdup_bam, "rmdup_metrics": self.smp_rmdup_ml_metrics}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p6.wait();
        rc6 = p6.returncode
        print(("rc6: ", rc6))
        exit_codes.append(rc6)

#        for i in [a1, a2, a3, a4, a5]:
#            if os.path.exists(i):
#                os.unlink(i)

#        for i in [acc_merg1_f]:
#            if os.path.exists(i):
#                os.remove(i)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)

    def extract_merged_bam_to_fastqs(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        exit_codes = []

        # extract fastq from bam
        pipe1 = os.path.join(self.align_smp_dir, "pipe1")
        r1 = os.path.join(self.align_smp_dir, "r1")
        r2 = os.path.join(self.align_smp_dir, "r2")
        o3 = os.path.join(self.align_smp_dir, "sam_to_fastq.out")
        f3 = 0

        if not os.path.exists(pipe1):
            os.mkfifo(pipe1)

        if not os.path.exists(r1):
            os.mkfifo(r1)

        if not os.path.exists(r2):
            os.mkfifo(r2)


        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_r1_fq_gz, self.smp_r2_fq_gz, o3]:
            if os.path.exists(i):
                os.remove(i)

        # remove duplicates
        # cmd = r"""sambamba sort -n -m 1700MB -F proper_pair %(bam_in)s --tmpdir=%(scratch_d)s -o xzy.bam
# """ % {"scratch_d": self.conf.scratch_loc_dir,
#        "bam_in": self.allmerg_bam}
#         print(cmd)
#         p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
#         p6.wait();
#         rc6 = p6.returncode
#         print("rc6: ", rc6)
#         exit_codes.append(rc6)
#
#         exit(0)
#
        with open(o3, "w") as f3:

            # we are using all reads: self.allmerg_bam
            # or removed duplicate reads: self.rmdup_bam
            p1 = subprocess.Popen("sambamba sort -n -m 1700MB -F proper_pair %s --tmpdir=%s -o /dev/stdout" % (self.allmerg_bam, self.conf.scratch_loc_dir),
                                  shell=True,
                                  cwd=self.align_smp_dir,
                                  stdout=subprocess.PIPE)

            # p2 = subprocess.Popen("java -jar $PICARD_HOME/SamToFastq.jar VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1,r2) , shell=True,
            p2 = subprocess.Popen("java -jar $PICARD_JAR SamToFastq VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1, r2), shell=True,
                                  cwd=self.align_smp_dir,
                                  stdin=p1.stdout,
                                  stderr=f3,
                                  stdout=subprocess.PIPE)

            p3 = subprocess.Popen(r"""pigz < %s > %s""" % (r1, self.smp_r1_fq_gz), shell=True,
                                  cwd=self.align_smp_dir,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)

            p4 = subprocess.Popen(r"""pigz < %s > %s""" % (r2, self.smp_r2_fq_gz), shell=True,
                                   cwd=self.align_smp_dir,
                                   #stdin = r2,
                                   stdout=subprocess.PIPE)

            print((p1.pid, p2.pid, p3.pid, p4.pid))
            exit_codes = [pp.wait() for pp in (p1, p2, p3, p4)]

        print((" exit_codes: ", exit_codes))

        os.unlink(pipe1)
        os.unlink(r1)
        os.unlink(r2)

        print(("self.smp_r1_fq_gz: ", self.smp_r1_fq_gz))

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)



    # parameters are found in attributes
    def merge_lanes_old(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))


        lanes_df = self.conf.sample_lanes
        print("lanes_df: ")
        print(lanes_df)
        print("-------------")
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print(lanes_df)
        print("-------------")
        # lanes_df = lanes_df.reset_index(drop=True)
        # print(lanes_df)
        # print("-------------")

        exit_codes = []

        lanes_st_acc = ""
        accepted_err_arr = []

        # scratch_d = os.getenv('SCRATCH', "")

        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx
            # print row
            print(("spln_row: ", self.spln_row))
            # run_s = row["Run"]
            # region_s = row["Region"]
            # print("--------------------", row["Name"], row["Library"], run_s, region_s)
            #
            # run_region_d = r"""%s/run%s_%s""" % (self.align_smp_dir, run_s, region_s)
            #
            # accepted_f = r"""%s/tophat_out/accepted_hits.bam""" % run_region_d
            #
            print(("accepted_f: ", self.spln_acc_am))

            # check alignment files
            if os.path.exists(self.spln_acc_am):
                lanes_st_acc += r"""%s """ % self.spln_acc_am
            else:
                # accepted_err_arr.append(r"""run%s_%s""" % (run_s, region_s))
                accepted_err_arr.append(self.spln_runreg)

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(self.align_smp_dir, "accepted_err_ml.csv")
            accepted_err_df.to_csv(accepted_err_f, sep=',', header=False, index=False)

            print(("accepted_err_arr: ", accepted_err_arr))
            print(("accepted_err_f: ", accepted_err_f))

        a1 = os.path.join(self.align_smp_dir, "a1.bam")
        a2 = os.path.join(self.align_smp_dir, "a2.bam")
        a3 = os.path.join(self.align_smp_dir, "a3.bam")
        a4 = os.path.join(self.align_smp_dir, "a4.bam")
        a5 = os.path.join(self.align_smp_dir, "a5.bam")

        if not os.path.exists(a1):
            os.mkfifo(a1)
        if not os.path.exists(a2):
            os.mkfifo(a2)
        if not os.path.exists(a3):
            os.mkfifo(a3)
        if not os.path.exists(a4):
            os.mkfifo(a4)
        if not os.path.exists(a5):
            os.mkfifo(a5)

        acc_merg1_f = os.path.join(self.align_smp_dir, "acc_merg1.bam")

        # redirection is important
        cmd = r"""samtools merge -c -p - %(lanes_st_acc)s > %(fout)s""" % {"lanes_st_acc": lanes_st_acc, "fout": a1}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # coordinate sort
        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, a2, a1)
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # add read groups
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": a2, "fout": a3, "smp": self.smp_name}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # coordinate sort again
        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (scratch_d, a4, a3)
        print(cmd)
        p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # reorder
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": a4, "fout": acc_merg1_f, "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in [p1, p2, p3, p4, p5]]
        print(("exit_codes stage: ", exit_codes_stage))
        exit_codes.extend(exit_codes_stage)
        # remove duplicates
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=true \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": acc_merg1_f, "fout": self.rmdup_bam, "rmdup_metrics": self.rmdup_metrics}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p6.wait();
        rc6 = p6.returncode
        print(("rc6: ", rc6))
        exit_codes.append(rc6)

        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [acc_merg1_f]:
            if os.path.exists(i):
                os.remove(i)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)


    def merge_lanes_rnaseqc2(self):

        print(("parameters: self.smp_idx: ", self.smp_idx))
        print(("self.smp_name: ", self.smp_name))

        lanes_df = self.conf.sample_lanes
        print("lanes_df: ")
        print(lanes_df)
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)
        print("-------------")
        print(lanes_df)

        exit_codes = []

        lanes_st = ""

        lanes_st_acc = ""
        lanes_st_unm = ""

        accepted_err_arr = []
        unmapped_err_arr = []

        scratch_d = os.getenv('SCRATCH', "")

        if not os.path.exists(self.align_smp_dir):
            os.makedirs(self.align_smp_dir)

        for idx, row in lanes_df.iterrows():
            # print row
            run_s = row["Run"]
            region_s = row["Region"]
            print(("--------------------", row["Name"], row["Library"], run_s, region_s))
            run_region_d = r"""%s/run%s_%s/tophat_out""" % (self.align_smp_dir, run_s, region_s)

            accepted_f = r"""%s/accepted_hits.bam""" % run_region_d
            unmapped_f = r"""%s/unmapped.bam""" % run_region_d
            unmapped_fixup_sam_f = r"""%s/unmapped_fixup.sam """ % run_region_d
            unmapped_fixup_bam_f = r"""%s/unmapped_fixup.bam """ % run_region_d
            # merged_f = r"""%s/merged.bam """ % run_region_d

            print(("accepted_f: ", accepted_f))
            print(("unmapped_f: ", unmapped_f))

            # check alignment files
            if os.path.exists(accepted_f):
                lanes_st += r"""%s """ % accepted_f
                lanes_st_acc += r"""%s """ % accepted_f
            else:
                accepted_err_arr.append(r"""run%s_%s""" % (run_s, region_s))

            if os.path.exists(unmapped_f):
                # fix the unmapped
                cmd = r"""%(pysrc_dir)s/tophat-recondition.py %(run_region_d)s""" % {"run_region_d": run_region_d,
                                                                                     "pysrc_dir": self.conf.pysrc_dir}
                print(cmd)
                p0 = subprocess.Popen(cmd, shell=True, cwd=run_region_d, stdout=subprocess.PIPE)
                p0.wait()
                rc0 = p0.returncode
                print(("rc0: ", rc0))
                exit_codes.append(rc0)

                cmd = r"""sambamba view -S -f bam -o  %(fout)s %(fin)s""" % {"fin": unmapped_fixup_sam_f, "fout": unmapped_fixup_bam_f}
                print(cmd)
                p0 = subprocess.Popen(cmd, shell=True, cwd=run_region_d, stdout=subprocess.PIPE)
                p0.wait()
                rc0 = p0.returncode
                print(("rc0: ", rc0))
                exit_codes.append(rc0)



                # and add it to the merging
                # lanes_st += r"""%s """ % unmapped_f
                lanes_st += r"""%s """ % unmapped_fixup_bam_f
                lanes_st_unm += r"""%s """ % unmapped_fixup_bam_f
            else:
                unmapped_err_arr.append(r"""run%s_%s""" % (run_s, region_s))

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(self.align_smp_dir, "accepted_err_qc.csv")
            accepted_err_df.to_csv(accepted_err_f, sep=',', header=False, index=False)

            print(("accepted_err_arr: ", accepted_err_arr))
            print(("accepted_err_f: ", accepted_err_f))

        if unmapped_err_arr.__len__() != 0:
            unmapped_err_df = pd.DataFrame.from_items([('run_region', unmapped_err_arr)])
            unmapped_err_f = os.path.join(self.align_smp_dir, "unmapped_err_qc.csv")
            unmapped_err_df.to_csv(unmapped_err_f, sep=',', header=False, index=False)

            print(("unmapped_err_arr: ", unmapped_err_arr))
            print(("unmapped_err_f: ", unmapped_err_f))

        #            cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
        #-XX:ParallelGCThreads=4 \
        #-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx4G \
        #-jar ${PICARD_JAR} MergeBamAlignment \
        #PROGRAM_RECORD_ID=Tophat \
        #PROGRAM_GROUP_VERSION=2.0.0 \
        #PROGRAM_GROUP_COMMAND_LINE=dummy \
        #PAIRED_RUN=true \
        #VALIDATION_STRINGENCY=SILENT \
        #UNMAPPED=%(unmapped)s \
        #ALIGNED=%(accepted)s \
        #REFERENCE_SEQUENCE=%(ref_genome)s \
        #O=%(fout)s
        #""" % {"unmapped": unmapped_f, "accepted": accepted_f, "fout": merged_f, "ref_genome": self.conf.ref_gen_fa}
        #            print cmd, runBash(cmd)


        #print lanes_st



        r1 = os.path.join(self.align_smp_dir, "r1.bam")
        r2 = os.path.join(self.align_smp_dir, "r2.bam")
        r2_sam = os.path.join(self.align_smp_dir, "r2.sam")
        r3 = os.path.join(self.align_smp_dir, "r3.bam")
        r4 = os.path.join(self.align_smp_dir, "r4.bam")
        r5 = os.path.join(self.align_smp_dir, "r5.bam")
        #r6 = os.path.join(run_path,"r6.bam")
        #r7 = os.path.join(run_path,"r7.bam")
        #r8 = os.path.join(run_path,"r8.bam")

        if not os.path.exists(r1):
            os.mkfifo(r1)
        if not os.path.exists(r2):
            os.mkfifo(r2)
        if not os.path.exists(r2_sam):
            os.mkfifo(r2_sam)
        if not os.path.exists(r3):
            os.mkfifo(r3)
        if not os.path.exists(r4):
            os.mkfifo(r4)
        if not os.path.exists(r5):
            os.mkfifo(r5)
            # if not os.path.exists(r6):
        #    os.mkfifo(r6) 
        # if not os.path.exists(r7):
        #    os.mkfifo(r7) 
        # if not os.path.exists(r8):
        #    os.mkfifo(r8)


        all_merg1_f = os.path.join(self.align_smp_dir, "all_merg1.bam")
        all_merg2_f = os.path.join(self.align_smp_dir, "all_merg2.bam")
        all_merg3_f = os.path.join(self.align_smp_dir, "all_merg3.bam")
        all_merg4_f = os.path.join(self.align_smp_dir, "all_merg4.bam")
        # all_merg5_f = os.path.join(self.conf.align2_dir,smp,"all_merg5.bam")
        # all_merg6_f = os.path.join(self.conf.align2_dir,smp,"all_merg6.bam")
        # all_merg7_f = os.path.join(self.conf.align2_dir,smp,"all_merg7.bam")
        # all_merg8_f = os.path.join(self.conf.align2_dir,smp,"all_merg8.bam")

        rmdup_metrics_f = os.path.join(self.align_smp_dir, "%s.rmdup_metrics_qc.csv" % self.smp_name)


        # cmd = r"""samtools merge -c -p - %(lanes_st)s | sambamba sort -n -t 20 -m 1GB -F proper_pair --tmpdir=$SCRATCH -o /dev/stdout /dev/stdin |  samtools fixmate -O bam - %(all_merg1_f)s""" % {"lanes_st": lanes_st, "all_merg1_f": all_merg1_f}

        # redirection is important
        cmd = r"""samtools merge -n - %(lanes_st)s > %(fout)s""" % {"lanes_st": lanes_st, "fout": r1}
        # cmd = r"""sambamba merge %(fout)s %(lanes_st)s """ % {"lanes_st": lanes_st, "fout": r1}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        # get rid of unpaired reads
        # cmd = r"""/home/dbadescu/local/bin/python %(pysrc_dir)s/unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": r1, "fout": r2, "pysrc_dir": self.conf.pysrc_dir}
        cmd = r"""unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": r1, "fout": r2, "pysrc_dir": self.conf.pysrc_dir}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        # cmd = r"""sambamba view -S -f bam -o  %(fout)s %(fin)s""" % {"fin": r2_sam, "fout": r2}
        # print cmd;
        # p22 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (scratch_d, r3, r2)
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s 
""" % {"fin": r3, "fout": r4, "smp": self.smp_name}
        print(cmd)
        p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (scratch_d, r5, r4)
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"fin": r5, "fout": all_merg1_f, "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in [p1, p2, p3, p4, p5, p6]]
        print(("exit_codes stage: ", exit_codes_stage))
        exit_codes.extend(exit_codes_stage)

        cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=false \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"fin": all_merg1_f, "fout": all_merg2_f, "rmdup_metrics": rmdup_metrics_f}
        print(cmd)
        p7 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p7.wait()
        rc7 = p7.returncode
        print(("rc7: ", rc7))
        exit_codes.append(rc7)

        for i in [r1, r2, r2_sam, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [all_merg1_f]:
            if os.path.exists(i):
                os.remove(i)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        if success:
            self.mark_done_smp_idx()

    def merge_lanes_rnaseqc_new_in_test(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        lanes_df = self.conf.sample_lanes

        print("lanes_df: ")
        print(lanes_df)
        print("-------------")
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print(lanes_df)
        print("-------------")

        exit_codes = []


        lanes_st = ""
        accepted_err_arr = []
        unmapped_err_arr = []


        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx

            # print row
            print(("spln_row: ", self.spln_row))
            print(("accepted_f: ", self.spln_acc_am))
            print(("unmapped_f: ", self.spln_unm_am))

            # check alignement files
            if os.path.exists(self.spln_acc_am):
                lanes_st += r"""%s """ % self.spln_acc_am
            else:
                accepted_err_arr.append(self.spln_align_lib_dir)

            if os.path.exists(self.spln_unm_am):
                # fix the unmapped
                cmd = r"""tophat-recondition.py %(run_region_d)s""" % {"run_region_d": self.spln_tophat_dir, "pysrc_dir": self.conf.pysrc_dir}
                print(cmd)
                p0 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
                p0.wait()
                rc0 = p0.returncode
                print(("rc0: ", rc0))
                exit_codes.append(rc0)
                # and add it to the merging

                lanes_st += r"""%s """ % self.spln_unm_fix_am
            else:
                unmapped_err_arr.append(self.spln_align_lib_dir)


        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_df.to_csv(self.smp_accepted_err_mlr_csv, sep='\t', header = False, index = False)
            print(("accepted_err_arr: ", accepted_err_arr))

        if unmapped_err_arr.__len__() != 0:
            unmapped_err_df = pd.DataFrame.from_items([('run_region', unmapped_err_arr)])
            unmapped_err_df.to_csv(self.smp_unmapped_err_mlr_csv, sep='\t', header = False, index = False)
            print(("unmapped_err_arr: ", unmapped_err_arr))

        # ---------------------- local file definitions

        r1 = os.path.join(self.align_smp_dir, "r1.bam")
        r2 = os.path.join(self.align_smp_dir, "r2.bam")
        r3 = os.path.join(self.align_smp_dir, "r3.bam")
        r4 = os.path.join(self.align_smp_dir, "r4.bam")
        r5 = os.path.join(self.align_smp_dir, "r5.bam")
        #r6 = os.path.join(run_path,"r6.bam")
        #r7 = os.path.join(run_path,"r7.bam")
        #r8 = os.path.join(run_path,"r8.bam")


        all_merg1_f = os.path.join(self.align_smp_dir, "all_merg1.bam")
        # all_merg2_f = os.path.join(run_path,"all_merg2.bam")
        # all_merg3_f = os.path.join(run_path,"all_merg3.bam")
        # all_merg4_f = os.path.join(self.conf.align2_dir,smp,"all_merg4.bam")
        # all_merg5_f = os.path.join(self.conf.align2_dir,smp,"all_merg5.bam")
        # all_merg6_f = os.path.join(self.conf.align2_dir,smp,"all_merg6.bam")
        # all_merg7_f = os.path.join(self.conf.align2_dir,smp,"all_merg7.bam")
        # all_merg8_f = os.path.join(self.conf.align2_dir,smp,"all_merg8.bam")

        # -------------------- cleaning

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [r1, r2, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        # for i in [all_merg1_f, self.allmerg_bam, self.smp_markdup_mlr_metrics]:
        #     if os.path.exists(i):
        #         os.remove(i)


        # ------------------------- create local files
        if not os.path.exists(r1):
            os.mkfifo(r1)
        if not os.path.exists(r2):
            os.mkfifo(r2)
        if not os.path.exists(r3):
            os.mkfifo(r3)
        if not os.path.exists(r4):
            os.mkfifo(r4)
        if not os.path.exists(r5):
            os.mkfifo(r5)
        # if not os.path.exists(r6):
        #    os.mkfifo(r6)
        # if not os.path.exists(r7):
        #     os.mkfifo(r7)
        # if not os.path.exists(r8):
        #    os.mkfifo(r8)

        f1 = os.path.join(self.align_smp_dir, "%s.mer.f1.bam" % self.smp_name)
        f1_bai = os.path.join(self.align_smp_dir, "%s.mer.f1.bam.bai" % self.smp_name)

        f2 = os.path.join(self.align_smp_dir, "%s.mer.f2.bam" % self.smp_name)
        f2_bai = os.path.join(self.align_smp_dir, "%s.mer.f2.bai" % self.smp_name)

        f3 = os.path.join(self.align_smp_dir, "f3.bam")
        # f4 = os.path.join(self.align_smp_dir, "f4.bam")

        # f4_txt = os.path.join(self.align_smp_dir, "f4.txt")


        # clean input files
        # for i in [f1, f1_bai, f2, f2_bai]:
        #     if os.path.exists(i):
        #         os.remove(i)

        # ------------------ PROCESSING


        # cmd = r"""samtools merge -c -p - %(lanes_st)s | sambamba sort -n -t 20 -m 1GB -F proper_pair --tmpdir=$SCRATCH -o /dev/stdout /dev/stdin |  samtools fixmate -O bam - %(all_merg1_f)s""" % {"lanes_st": lanes_st, "all_merg1_f": all_merg1_f}

        # redirection is important
        # cmd = r"""samtools merge -c -p - %(lanes_st)s > %(fout)s""" % {"lanes_st": lanes_st, "fout": f1}
        # print(cmd)
        # p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # p1.wait()
        # rc1 = p1.returncode
        # print("rc1: ", rc1)
        # exit_codes.append(rc1)


        # get rid of unpaired reads

        cmd = r"""unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": f1, "fout": f2, "pysrc_dir": self.conf.pysrc_dir}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print(("rc2: ", rc2))
        exit_codes.append(rc2)


        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, f3, f2)
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print(("rc3: ", rc3))
        exit_codes.append(rc3)

        exit(0)


        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r3, "fout": r4, "smp": self.smp_name}
        print(cmd)
        p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, r5, r4)
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r5, "fout": all_merg1_f, "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in (p1, p2, p3, p4, p5, p6)]
        print(("exit_codes stage: ", exit_codes_stage))
        exit_codes.extend(exit_codes_stage)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=false \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": all_merg1_f,
       "fout": self.allmerg_bam,
       "rmdup_metrics": self.smp_markdup_mlr_metrics}
        print(cmd)
        p7 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p7.wait()
        rc7 = p7.returncode
        print(("rc7: ", rc7))
        exit_codes.append(rc7)


        for i in [r1, r2, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [all_merg1_f]:
            if os.path.exists(i):
                os.remove(i)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)

    def merge_lanes_rnaseqc(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        lanes_df = self.conf.sample_lanes

        print("lanes_df: ")
        print(lanes_df)
        print("-------------")
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print(lanes_df)
        print("-------------")

        exit_codes = []


        lanes_st = ""
        accepted_err_arr = []
        unmapped_err_arr = []


        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx

            # print row
            print(("spln_row: ", self.spln_row))
            print(("accepted_f: ", self.spln_acc_am))
            print(("unmapped_f: ", self.spln_unm_am))

            # check alignement files
            if os.path.exists(self.spln_acc_am):
                lanes_st += r"""%s """ % self.spln_acc_am
            else:
                accepted_err_arr.append(self.spln_align_lib_dir)

            if os.path.exists(self.spln_unm_am):
                # fix the unmapped
                cmd = r"""tophat-recondition.py %(run_region_d)s""" % {"run_region_d": self.spln_tophat_dir, "pysrc_dir": self.conf.pysrc_dir}
                print(cmd)
                p0 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
                p0.wait()
                rc0 = p0.returncode
                print(("rc0: ", rc0))
                exit_codes.append(rc0)
                # and add it to the merging

                lanes_st += r"""%s """ % self.spln_unm_fix_am
            else:
                unmapped_err_arr.append(self.spln_align_lib_dir)


        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_df.to_csv(self.smp_accepted_err_mlr_csv, sep='\t', header = False, index = False)
            print(("accepted_err_arr: ", accepted_err_arr))

        if unmapped_err_arr.__len__() != 0:
            unmapped_err_df = pd.DataFrame.from_items([('run_region', unmapped_err_arr)])
            unmapped_err_df.to_csv(self.smp_unmapped_err_mlr_csv, sep='\t', header = False, index = False)
            print(("unmapped_err_arr: ", unmapped_err_arr))

        # ---------------------- local file definitions

        r1 = os.path.join(self.align_smp_dir, "r1.bam")
        r2 = os.path.join(self.align_smp_dir, "r2.bam")
        r3 = os.path.join(self.align_smp_dir, "r3.bam")
        r4 = os.path.join(self.align_smp_dir, "r4.bam")
        r5 = os.path.join(self.align_smp_dir, "r5.bam")
        #r6 = os.path.join(run_path,"r6.bam")
        #r7 = os.path.join(run_path,"r7.bam")
        #r8 = os.path.join(run_path,"r8.bam")


        all_merg1_f = os.path.join(self.align_smp_dir, "all_merg1.bam")
        # all_merg2_f = os.path.join(run_path,"all_merg2.bam")
        # all_merg3_f = os.path.join(run_path,"all_merg3.bam")
        # all_merg4_f = os.path.join(self.conf.align2_dir,smp,"all_merg4.bam")
        # all_merg5_f = os.path.join(self.conf.align2_dir,smp,"all_merg5.bam")
        # all_merg6_f = os.path.join(self.conf.align2_dir,smp,"all_merg6.bam")
        # all_merg7_f = os.path.join(self.conf.align2_dir,smp,"all_merg7.bam")
        # all_merg8_f = os.path.join(self.conf.align2_dir,smp,"all_merg8.bam")

        # -------------------- cleaning

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [r1, r2, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [all_merg1_f, self.allmerg_bam, self.smp_markdup_mlr_metrics]:
            if os.path.exists(i):
                os.remove(i)


        # ------------------------- create local files
        if not os.path.exists(r1):
            os.mkfifo(r1)
        if not os.path.exists(r2):
            os.mkfifo(r2)
        if not os.path.exists(r3):
            os.mkfifo(r3)
        if not os.path.exists(r4):
            os.mkfifo(r4)
        if not os.path.exists(r5):
            os.mkfifo(r5)
        # if not os.path.exists(r6):
        #    os.mkfifo(r6)
        # if not os.path.exists(r7):
        #     os.mkfifo(r7)
        # if not os.path.exists(r8):
        #    os.mkfifo(r8)


        # ------------------ PROCESSING


        # cmd = r"""samtools merge -c -p - %(lanes_st)s | sambamba sort -n -t 20 -m 1GB -F proper_pair --tmpdir=$SCRATCH -o /dev/stdout /dev/stdin |  samtools fixmate -O bam - %(all_merg1_f)s""" % {"lanes_st": lanes_st, "all_merg1_f": all_merg1_f}

        # redirection is important
        cmd = r"""samtools merge -c -p - %(lanes_st)s > %(fout)s""" % {"lanes_st": lanes_st, "fout": r1}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        # get rid of unpaired reads

        cmd = r"""unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": r1, "fout": r2, "pysrc_dir": self.conf.pysrc_dir}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, r3, r2)
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)


        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r3, "fout": r4, "smp": self.smp_name}
        print(cmd)
        p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, r5, r4)
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r5, "fout": all_merg1_f, "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in (p1, p2, p3, p4, p5, p6)]
        print(("exit_codes stage: ", exit_codes_stage))
        exit_codes.extend(exit_codes_stage)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=false \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": all_merg1_f,
       "fout": self.allmerg_bam,
       "rmdup_metrics": self.smp_markdup_mlr_metrics}
        print(cmd)
        p7 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p7.wait()
        rc7 = p7.returncode
        print(("rc7: ", rc7))
        exit_codes.append(rc7)


        for i in [r1, r2, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [all_merg1_f]:
            if os.path.exists(i):
                os.remove(i)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)


    def merge_lanes_rnaseqc_old(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        smp = self.conf.sample_names.iloc[self.smp_idx, 0]

        print(("smp_idx, smp: ", self.smp_idx, self.smp_name))

        lanes_df = self.conf.sample_lanes
        print("lanes_df: ")
        print(lanes_df)
        lanes_df = lanes_df[lanes_df['Name'] == smp].reset_index(drop=True )
        print("-------------")
        print(lanes_df)

        exit_codes = []


        lanes_st = ""
        accepted_err_arr = []
        unmapped_err_arr = []

        # scratch_d = os.getenv('SCRATCH', "")


        run_path = os.path.join(self.conf.align2_dir, self.smp_name)
        if not os.path.exists(run_path):
            os.makedirs(run_path)

        for idx, row in lanes_df.iterrows():
            #print row
            run_s = row["Run"]
            region_s = row["Region"]
            print(("--------------------",row["Name"],row["Library"], run_s, region_s))
            run_region_d = r"""%s/run%s_%s/tophat_out""" % (run_path, run_s, region_s)

            accepted_f = r"""%s/accepted_hits.bam""" % run_region_d
            unmapped_f =  r"""%s/unmapped.bam""" % run_region_d
            unmapped_fixup_f =  r"""%s/unmapped_fixup.bam """ % run_region_d
            merged_f =  r"""%s/merged.bam """ % run_region_d

            print(("accepted_f: ", accepted_f))
            print(("unmapped_f: ", unmapped_f))

            #check alignement files
            if os.path.exists(accepted_f):
                lanes_st += r"""%s """ % accepted_f
            else:
                accepted_err_arr.append(r"""run%s_%s""" % (run_s,region_s))

            if os.path.exists(unmapped_f):
                #fix the unmapped
                cmd = r"""tophat-recondition.py %(run_region_d)s""" % {"run_region_d": run_region_d, "pysrc_dir": self.conf.pysrc_dir}
                print(cmd)
                p0 = subprocess.Popen(cmd, shell=True, cwd=run_region_d, stdout=subprocess.PIPE)
                p0.wait()
                rc0 = p0.returncode
                print(("rc0: ", rc0))
                exit_codes.append(rc0)
                #and add it to the merging
                #lanes_st += r"""%s """ % unmapped_f
                lanes_st += r"""%s """ % unmapped_fixup_f
            else:
                unmapped_err_arr.append(r"""run%s_%s""" % (run_s,region_s))


        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(run_path,"accepted_err.csv")
            accepted_err_df.to_csv(accepted_err_f, sep='\t', header = False, index = False)

            print(("accepted_err_arr: ",accepted_err_arr))
            print(("accepted_err_f: ",accepted_err_f))



        if unmapped_err_arr.__len__() != 0:
            unmapped_err_df = pd.DataFrame.from_items([('run_region', unmapped_err_arr)])
            unmapped_err_f = os.path.join(run_path,"unmapped_err.csv")
            unmapped_err_df.to_csv(unmapped_err_f, sep='\t', header = False, index = False)

            print(("unmapped_err_arr: ", unmapped_err_arr))
            print(("unmapped_err_f: ", unmapped_err_f))

#            cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
#-XX:ParallelGCThreads=4 \
#-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx4G \
#-jar ${PICARD_JAR} MergeBamAlignment \
#PROGRAM_RECORD_ID=Tophat \
#PROGRAM_GROUP_VERSION=2.0.0 \
#PROGRAM_GROUP_COMMAND_LINE=dummy \
#PAIRED_RUN=true \
#VALIDATION_STRINGENCY=SILENT \
#UNMAPPED=%(unmapped)s \
#ALIGNED=%(accepted)s \
#REFERENCE_SEQUENCE=%(ref_genome)s \
#O=%(fout)s
#""" % {"unmapped": unmapped_f, "accepted": accepted_f, "fout": merged_f, "ref_genome": self.conf.ref_gen_fa}
#            print cmd, runBash(cmd)


        #print lanes_st



        r1 = os.path.join(run_path, "r1.bam")
        r2 = os.path.join(run_path, "r2.bam")
        r3 = os.path.join(run_path, "r3.bam")
        r4 = os.path.join(run_path, "r4.bam")
        r5 = os.path.join(run_path, "r5.bam")
        #r6 = os.path.join(run_path,"r6.bam")
        #r7 = os.path.join(run_path,"r7.bam")
        #r8 = os.path.join(run_path,"r8.bam")

        if not os.path.exists(r1):
            os.mkfifo(r1)
        if not os.path.exists(r2):
            os.mkfifo(r2)
        if not os.path.exists(r3):
            os.mkfifo(r3)
        if not os.path.exists(r4):
            os.mkfifo(r4)
        if not os.path.exists(r5):
            os.mkfifo(r5)
        #if not os.path.exists(r6):
        #    os.mkfifo(r6)
        #if not os.path.exists(r7):
        #    os.mkfifo(r7)
        #if not os.path.exists(r8):
        #    os.mkfifo(r8)


        all_merg1_f = os.path.join(run_path, "all_merg1.bam")
        # all_merg2_f = os.path.join(run_path,"all_merg2.bam")
        #all_merg3_f = os.path.join(run_path,"all_merg3.bam")
        #all_merg4_f = os.path.join(self.conf.align2_dir,smp,"all_merg4.bam")
        #all_merg5_f = os.path.join(self.conf.align2_dir,smp,"all_merg5.bam")
        #all_merg6_f = os.path.join(self.conf.align2_dir,smp,"all_merg6.bam")
        #all_merg7_f = os.path.join(self.conf.align2_dir,smp,"all_merg7.bam")
        #all_merg8_f = os.path.join(self.conf.align2_dir,smp,"all_merg8.bam")

        rmdup_metrics_f = os.path.join(run_path, "rmdup_metrics.csv")


        #cmd = r"""samtools merge -c -p - %(lanes_st)s | sambamba sort -n -t 20 -m 1GB -F proper_pair --tmpdir=$SCRATCH -o /dev/stdout /dev/stdin |  samtools fixmate -O bam - %(all_merg1_f)s""" % {"lanes_st": lanes_st, "all_merg1_f": all_merg1_f}

        #redirection is important
        cmd = r"""samtools merge -c -p - %(lanes_st)s > %(fout)s""" % {"lanes_st": lanes_st, "fout": r1}
        print(cmd); p1 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)

        #get rid of unpaired reads

        cmd = r"""unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": r1, "fout": r2, "pysrc_dir": self.conf.pysrc_dir}
        print(cmd); p2 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, r3, r2)
        print(cmd); p3 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)


        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
VALIDATION_STRINGENCY=SILENT \
SORT_ORDER=coordinate \
CREATE_INDEX=false \
MAX_RECORDS_IN_RAM=500000 \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r3, "fout": r4, "smp": smp}
        print(cmd)
        p4 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)

        cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (self.conf.scratch_loc_dir, r5, r4)
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True,cwd=run_path, stdout=subprocess.PIPE)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": r5, "fout": all_merg1_f, }
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)



        exit_codes_stage = [pp.wait() for pp in (p1, p2, p3, p4, p5, p6)]
        print(("exit_codes stage: ", exit_codes_stage))
        exit_codes.extend(exit_codes_stage)


        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=false \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": all_merg1_f, "fout": self.allmerg_bam, "rmdup_metrics": rmdup_metrics_f}
        print(cmd); p7 = subprocess.Popen(cmd, shell=True, cwd=run_path, stdout=subprocess.PIPE)
        p7.wait(); rc7 = p7.returncode
        print(("rc7: ", rc7))
        exit_codes.append(rc7)


        for i in [r1, r2, r3, r4, r5]:
            if os.path.exists(i):
                os.unlink(i)

        for i in [all_merg1_f]:
            if os.path.exists(i):
                os.remove(i)


        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)


    def align_lane_tophat(self):

        args = {"spln_idx": self.spln_idx}
        print(("parameters: \n", args))

        exit_codes = []

        # lanes_df = self.conf.sample_lanes
        # smp_lane = lanes_df.iloc[[self.spln_idx]]

        # print("smp_lane: ", smp_lane)

        # smp = smp_lane.iloc[0, 0]
        # library = smp_lane.iloc[0, 1]
        # run_s = smp_lane.iloc[0, 2]
        # region_s = smp_lane.iloc[0, 3]
        # print("--------------------", smp, library, run_s, region_s)
        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # run_region_src_d = r"""%s/%s/run%s_%s""" % (self.conf.reads2_dir, smp, run_s, region_s)
        # run_region_dst_d = r"""%s/%s/run%s_%s""" % (self.conf.align2_dir, smp, run_s, region_s)

        # if not os.path.exists(run_region_dst_d):
        #     os.makedirs(run_region_dst_d)

        # rp1 = r"""%s/%s.%s.33.pair1.fastq.gz""" % (run_region_src_d, smp, library)
        # rp2 = r"""%s/%s.%s.33.pair2.fastq.gz""" % (run_region_src_d, smp, library)
        # align_out = r"""%s/%s.%s.33.alignout.csv""" % (run_region_dst_d, smp, library)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        cmd = r"""tophat -p 12  -g 1 \
--transcriptome-index %(tra_idx)s \
%(gen_idx)s \
%(rp1)s \
%(rp2)s \
2> %(align_out)s
""" % {"rp1": self.spln_rp1, "rp2": self.spln_rp2,
       "tra_idx": self.conf.tra_idx, "gen_idx": self.conf.gen_idx,
       "align_out": self.spln_tophat_out}

        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)


        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)





    def smp_reorder(self):
        cmd = r"""java -Djava.io.tmpdir=$SCRATCH \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(merged_bam)s \
OUTPUT=%(karyo_bam)s \
REFERENCE=%(ref_genome)s \
MAX_RECORDS_IN_RAM=500000
""" % {"merged_bam": self.merged_bam, "karyo_bam": self.karyo_bam, "ref_genome": self.conf.ref_gen_fa}

        print((cmd, runBash(cmd)))


    def smp_rmdup(self):
        cmd = r"""java -Djava.io.tmpdir=/gs/scratch/dbadescu \
-XX:ParallelGCThreads=4 -Dsamjdk.use_async_io=true -Xmx2G \
-jar ${PICARD_HOME}/MarkDuplicates.jar REMOVE_DUPLICATES=true CREATE_MD5_FILE=true \
VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
TMP_DIR=/gs/scratch/dbadescu \
INPUT=%(karyo_bam)s \
OUTPUT=%(rmdup_bam)s \
METRICS_FILE=%(rmdup_metrics)s \
MAX_RECORDS_IN_RAM=500000
""" % {"karyo_bam": self.lcnf.karyo_bam, "rmdup_bam": self.lcnf.rmdup_bam, "rmdup_metrics": self.lcnf.rmdup_metrics}
        print((cmd, runBash(cmd)))

    def wiggle_old(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        # nmblines=$(samtools view -F 256 -f 81 \
        # %(rmdup_bam)s | wc -l)
        #$ scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc)
        cmd = r"""
genomeCoverageBed -bg -ibam \
%(rmdup_bam)s \
-g %(chrominfo)s \
-split -scale %(scalefactor)s  > \
%(bed_graph)s
bedGraphToBigWig %(bed_graph)s \
%(chrominfo)s \
%(big_wig)s
""" % {"scalefactor": 1, "chrominfo": self.conf.ref_gen_chrominfo, "rmdup_bam": self.rmdup_bam,
       "bed_graph": self.bed_graph, "big_wig": self.big_wig}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))

        # test if procedure success
        success = True if rc1 == 0 else False
        # mark as done
        self.mark_done_args(args, success)



    def rseqc(self):

        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        load_one_module('raglab/rseqc/2.5.0')

        # parameters
        print(("parameters: self.smp_idx: ", self.smp_idx))

        cmd = r"""RPKM_saturation.py -r %(tra_bed12)s \
-d '1++,1--,2+-,2-+' \
-i %(rmdup_bam)s \
-o %(rseqc_smp_prefix)s
""" % {"tra_bed12": self.conf.ref_transcr_nz_bed12,
       "rmdup_bam": self.rmdup_bam,
       "rseqc_smp_prefix": self.rseqc_smp_prefix}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.rseqc_smp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        # test if procedure success
        success = True if rc1 == 0 else False
        # mark as done
        self.mark_done_args(args, success)

        unload_one_module('raglab/rseqc/2.5.0')




    def kallisto_smp_transcr(self):
        args = {"smp_idx": self.smp_idx}
        print(("parameters: \n", args))

        # remove output folder
        if os.path.exists(self.kallisto_smp_dir):
            shutil.rmtree(self.kallisto_smp_dir, ignore_errors=False, onerror=None)

        # with 100 bootstrap
        cmd = r"""kallisto quant -i %(kallisto_kix)s -o %(kallisto_smp_dir)s -b 100 %(smp_r1_fq_gz)s %(smp_r2_fq_gz)s
""" % {"kallisto_kix": self.conf.ref_transcr_kallisto_kix,
       "kallisto_smp_dir": self.kallisto_smp_dir,
       "smp_r1_fq_gz": self.smp_r1_fq_gz,
       "smp_r2_fq_gz": self.smp_r2_fq_gz}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        # test if procedure success
        success = True if rc1 == 0 else False
        # mark as done
        self.mark_done_args(args, success)

    def fpkm_find_genes_reference(self):
        cur_df = pd.DataFrame()

        self.all_genes_fpkm_tracking_ids_df = pd.DataFrame()

        for idx in range(self.nb_smpgrps):
        #for smp_idx in range(2):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            # print(("self.smp_idx: ", self.smp_idx))
            # print(("self.smp_name: ", self.smp_name))

            # read sample matrix
            try:
                cur_df = pd.read_csv(self.genes_fpkm_tracking_f, sep='\t', header=0,
                                     index_col=[0], usecols=[0, 4, 9],
                                     names=["tracking_id", "gene_short_name", "FPKM"])

            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            print("cur_df: ", cur_df)
            cur_df = cur_df[cur_df['FPKM'] != 0]
            cur_df.drop('FPKM', axis=1, inplace=True)
            # names can differ sometimes !
            #print "cur_df: \n", cur_df
            # group_by and take the first occurance
            cur_df = cur_df.groupby(level=0).first()



            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum' : np.sum,'fpkm_mean' : np.mean})
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum': np.sum})
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_avg': np.average})
            # print "cur_fpkm.shape: ", cur_fpkm.shape

            # cur_df.reset_index()
            # cur_df = cur_df.index.unique()
            # cur_df = pd.DataFrame.from_items([("tracking_id", cur_df)])
            # cur_df = cur_df.set_index(["tracking_id"])
            # print "undf: ", undf.shape
            # print "cur_df: ", cur_df

            #  max([np.nonnull(a)row['gene_short_name_x'], row['gene_short_name_y']])
            def f(row):
                a = row['gene_short_name_x']
                b = row['gene_short_name_y']
                # print("a, b: ", a, b)

                c = a if a else b
                return c

            if self.smpgrp_idx == 0:
                self.all_genes_fpkm_tracking_ids_df = cur_df.copy(deep=True)
            else:
                local_df = pd.merge(self.all_genes_fpkm_tracking_ids_df, cur_df,
                                    how='outer', left_index=True,
                                    right_index=True, suffixes=('_x', '_y'))
                local_df['gene_short_name'] = local_df.apply(f, axis=1)
                local_df.drop('gene_short_name_x', axis=1, inplace=True)
                local_df.drop('gene_short_name_y', axis=1, inplace=True)
                # print "local_df: \n", local_df

                self.all_genes_fpkm_tracking_ids_df = local_df
                # print "self.genes_fpkm_tracking_ids_df.----------------: \n", self.all_genes_fpkm_tracking_ids_df
                print(("self.genes_fpkm_tracking_ids_df shape: ", self.all_genes_fpkm_tracking_ids_df.shape))

                #dfx = self.all_genes_fpkm_tracking_ids_df
                #print "dfx: ", dfx
                #print "dfx_unique:", dfx[0].unique()

                # for idx, row in prim_df.iterrows():
                #    if pd.isnull(row["gene_short_name_x"]) or pd.isnull(row["gene_short_name_y"]):
                #        print "--------------------",row["gene_short_name_x"],row["gene_short_name_y"]
                # print "prim_df shape: ",prim_df.shape

                # "Days with rain: {0}".format(num_rain)
        print(("self.all_genes_fpkm_tracking_ids_df: \n", self.all_genes_fpkm_tracking_ids_df))

    def fpkm_find_isofs_reference(self):
        cur_df = pd.DataFrame()

        self.all_isofs_fpkm_tracking_ids_df = pd.DataFrame()

        for idx in range(self.nb_smpgrps):
        #for smp_idx in range(2):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            # print(("self.smp_idx: ", self.smp_idx))
            # print(("self.smp_name: ", self.smp_name))

            # read sample matrix
            try:
                cur_df = pd.read_csv(self.isofs_fpkm_tracking_f, sep='\t', header=0,
                                     index_col=[0], usecols=[0, 4, 9],
                                     names=["tracking_id", "gene_short_name", "FPKM"])
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            cur_df = cur_df[cur_df['FPKM'] != 0]
            cur_df.drop('FPKM', axis=1, inplace=True)
            # names can differ sometimes !
            #print "cur_df: \n", cur_df
            # group_by and take the first occurance
            cur_df = cur_df.groupby(level=0).first()


            def f(row):
                a = row['gene_short_name_x']
                b = row['gene_short_name_y']
                c = max(a, b)
                return c

            if self.smpgrp_idx == 0:
                self.all_isofs_fpkm_tracking_ids_df = cur_df.copy(deep=True)
            else:
                local_df = pd.merge(self.all_isofs_fpkm_tracking_ids_df, cur_df,
                                    how='outer', left_index=True,
                                    right_index=True, suffixes=('_x', '_y'))
                local_df['gene_short_name'] = local_df.apply(f, axis=1)
                local_df.drop('gene_short_name_x', axis=1, inplace=True)
                local_df.drop('gene_short_name_y', axis=1, inplace=True)
                # print "local_df: \n", local_df

                self.all_isofs_fpkm_tracking_ids_df = local_df
                # print "self.genes_fpkm_tracking_ids_df.----------------: \n", self.all_genes_fpkm_tracking_ids_df
                print(("self.isofs_fpkm_tracking_ids_df shape: ", self.all_isofs_fpkm_tracking_ids_df.shape))

        print(("self.all_isofs_fpkm_tracking_ids_df: \n", self.all_isofs_fpkm_tracking_ids_df))

    def save_genes_fpkm_tracking_ids(self):
        self.all_genes_fpkm_tracking_ids_df.to_csv(self.all_genes_fpkm_tracking_ids_f,
                                                   sep=',', header=True, index=True, index_label="tracking_id")
        # time.sleep(5)

    def load_genes_fpkm_tracking_ids(self):

        self.all_genes_fpkm_tracking_ids_df = pd.read_csv(self.all_genes_fpkm_tracking_ids_f,
                                                          sep=',', header=0,
                                                          index_col=[0], usecols=[0, 1],
                                                          names=["tracking_id", "gene_short_name"])

        #, dtype={"idx": np.int, "tracking_id": np.object}
        # print track_df
        print(("self.all_genes_fpkm_tracking_ids_df shape: ", self.all_genes_fpkm_tracking_ids_df.shape))


    def save_isofs_fpkm_tracking_ids(self):
        self.all_isofs_fpkm_tracking_ids_df.to_csv(self.all_isofs_fpkm_tracking_ids_f,
                                                   sep=',', header=True, index=True, index_label="tracking_id")
        # time.sleep(5)

    def load_isofs_fpkm_tracking_ids(self):

        self.all_isofs_fpkm_tracking_ids_df = pd.read_csv(self.all_isofs_fpkm_tracking_ids_f,
                                                          sep=',', header=0,
                                                          index_col=[0], usecols=[0, 1],
                                                          names=["tracking_id", "gene_short_name"])

        #, dtype={"idx": np.int, "tracking_id": np.object}
        # print track_df
        print(("self.all_isofs_fpkm_tracking_ids_df shape: ", self.all_isofs_fpkm_tracking_ids_df.shape))





    def matrix_metrics(self):


        # calculate initial matrix
        print("in matrix_metrics:")

        #        cmd = r"""gtf2tmpMatrix.awk \
        # $RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens/GRCh37_spiked/annotation/GRCh37.gtf \
        # %(init_matrix)s
        # """ % {"init_matrix": self.lcnf.init_matrix_txt}
        #        print cmd, runBash(cmd)
        # load initial matrix
        gr8 = pd.read_csv(self.lcnf.init_matrix_txt,
                          sep='\t', header=False, index_col=[0],
                          names=["Gene", "Symbol"]
        )
        # usecols=[0,1]

        print(("gr8: ", gr8))
        # print "gr8: ", gr8.loc[["ENSG00000273492","ENSG00000273487"],["readcnt"]]

        # for smp_name in ["All_After_Stain_RNeasy-10", "CD45nCD8p_SC_H02_G_C44-10"]:
        # for smp_idx in range(self.conf.sample_names.shape[0]):
        for smp_idx in range(self.conf.sample_names.shape[0]):
            print(("smp_idx: ", smp_idx))
            smp = self.conf.sample_names.iloc[smp_idx, 0]
            print(("smp: ", smp))

            # read sample matrix
            try:
                gr9 = pd.read_csv(os.path.join(self.conf.align2_dir, smp, "%s.readcounts.csv" % smp),
                                  sep='\t', header=False, index_col=[0],
                                  names=["contig", "readcnt"]
                )
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next
                # usecols=[0,1]

            # print "gr9: ", gr9

            # gr8 = pd.merge(gr8, gr9, left_index=True, right_index=True, suffixes=('', '_y'))
            gr8 = pd.merge(gr8, gr9, left_index=True, right_index=True)
            gr8.rename(columns={'readcnt': smp}, inplace=True)

        print(("gr8: ", gr8))
        gr8.to_csv(os.path.join(self.conf.align2_dir, "all.readcounts.csv"),
                   sep='\t', header=True, index=True, index_label="Gene")

    def fpkm_sqlite_find_reference(self):

        #nb_samples = self.conf.sample_names.shape[0]


        import sqlite3
        from pandas.io import sql
        # Create your connection.
        #cnx = sqlite3.connect(':memory:') 
        cnx = sqlite3.connect('../redis/test.db')
        #sql.write_frame(price2, name='price2', con=cnx)
        #p2 = sql.read_frame('select * from price2', cnx)

        cur_df = pd.DataFrame()
        prim_df = pd.DataFrame()
        res_df = pd.DataFrame()

        track_ids = set()  # A new empty set

        for smp_idx in range(self.conf.sample_names.shape[0]):
            print(("smp_idx: ", smp_idx))
            smp = self.conf.sample_names.iloc[smp_idx, 0]
            print(("smp: ", smp))

            #read sample matrix
            try:
                cur_df = pd.read_csv(os.path.join(self.conf.align2_dir, smp, "fpkm_known", "genes.fpkm_tracking"),
                                     sep='\t', header=False, index_col=[0], usecols=[0, 4, 11],
                                     names=["tracking_id", "gene_short_name", "FPKM_conf_hi"]
                )
                # cur_df= cur_df.query('FPKM_conf_hi != 0')
                cur_df = cur_df[cur_df['FPKM_conf_hi'] != 0].reset_index()
                for idx, row in cur_df.iterrows():
                    #print row
                    #if not elem in track_ids:
                    track_ids.add(row['tracking_id'])
                    #        print "--------------------",row["gene_short_name_x"],row["gene_short_name_y"]



                    #now drop the column
                    #cur_df.drop('FPKM_conf_hi', axis=1, inplace=True)
                    #cur_df.drop('gene_short_name', axis=1, inplace=True)

                    #cur_df.to_sql("cur_df", cnx, flavor='sqlite', if_exists='replace', index=True, index_label="tracking_id")

            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            print(("smp: ", smp, len(track_ids)))
        track_a = list(track_ids)
        track_df = pd.DataFrame.from_items([('track_id', track_a)])

        track_df.to_sql("track_df", cnx, flavor='sqlite', if_exists='replace', index=True, index_label="idx")

        track_df = track_df.set_index("track_id")
        track_df.reset_index(level=0, inplace=True)
        track_df.to_csv(os.path.join(self.conf.align2_dir, "track_df.csv"),
                        sep='\t', header=True, index=True, index_label="idx"
        )

        # track_df['idx'] = track_df.index

        return track_df
        #if smp_idx == 0:
        #    prim_df = cur_df.copy(deep=True)
        #else:
        #    prim_df =  pd.merge(prim_df, cur_df, how='outer', left_index=True, right_index=True, suffixes=('_x', '_y'))
        #    print "prim_df shape: ", prim_df.shape
        #    prim_df.to_sql("prim_df", cnx, flavor='sqlite', if_exists='replace', index=True, index_label="tracking_id")

        #    time.sleep(20)


        #for idx, row in prim_df.iterrows():
        #    if pd.isnull(row["gene_short_name_x"]) or pd.isnull(row["gene_short_name_y"]):
        #        print "--------------------",row["gene_short_name_x"],row["gene_short_name_y"]
        #print "prim_df shape: ",prim_df.shape

        #"Days with rain: {0}".format(num_rain)
        #   prim_df.to_csv(os.path.join(self.conf.align2_dir,"all.readcounts.csv"),
        #             sep='\t', header = True, index = True, index_label = "tracking_id"
        #             )


    def readcnt_find_reference(self):
        cur_df = pd.DataFrame()

        self.all_genes_readcnt_tracking_ids_df = pd.DataFrame()

        for smp_idx in range(self.nb_samples):
        # for smp_idx in range(2):
            # set index in order to make all path work
            self.smp_idx = smp_idx
            print(("self.smp_idx: ", self.smp_idx))
            print(("self.smp_name: ", self.smp_name))

            # read sample matrix
            try:
                cur_df = pd.read_csv(self.genes_readcnt_tracking_f, sep='\t', header=False,
                                     index_col=[0], usecols=[0, 1],
                                     names=["tracking_id", "readcnt"])
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            cur_df = cur_df[cur_df['readcnt'] != 0]
            cur_df.drop('readcnt', axis=1, inplace=True)
            # names can differ sometimes !
            # print "cur_df: \n", cur_df
            # group_by and take the first occurance
            cur_df = cur_df.groupby(level=0).first()

            # def f(row):
            #     a = row['gene_short_name_x']
            #     b = row['gene_short_name_y']
            #     c = max(a, b)
            #     return c

            if self.smp_idx == 0:
                self.all_genes_readcnt_tracking_ids_df = cur_df.copy(deep=True)
            else:
                local_df = pd.merge(self.all_genes_readcnt_tracking_ids_df, cur_df,
                                    how='outer', left_index=True,
                                    right_index=True, suffixes=('_x', '_y'))
                # print "local_df: \n", local_df

                self.all_genes_readcnt_tracking_ids_df = local_df
                # print "self.genes_fpkm_tracking_ids_df.----------------: \n", self.all_genes_fpkm_tracking_ids_df
                print(("self.genes_readcnt_tracking_ids_df shape: ", self.all_genes_readcnt_tracking_ids_df.shape))

                #dfx = self.all_genes_fpkm_tracking_ids_df
                #print "dfx: ", dfx
                #print "dfx_unique:", dfx[0].unique()

                # for idx, row in prim_df.iterrows():
                #    if pd.isnull(row["gene_short_name_x"]) or pd.isnull(row["gene_short_name_y"]):
                #        print "--------------------",row["gene_short_name_x"],row["gene_short_name_y"]
                # print "prim_df shape: ",prim_df.shape

                # "Days with rain: {0}".format(num_rain)
        print(("self.all_genes_readcnt_tracking_ids_df: \n", self.all_genes_readcnt_tracking_ids_df))


    def save_genes_readcnt_tracking_ids(self):
        self.all_genes_readcnt_tracking_ids_df.to_csv(self.all_genes_readcnt_tracking_ids_f,
                                                   sep=',', header=True, index=True, index_label="tracking_id")
        # time.sleep(5)



    def load_genes_readcnt_tracking_ids(self):

        self.all_genes_readcnt_tracking_ids_df = pd.read_csv(self.all_genes_readcnt_tracking_ids_f,
                                                          sep=',', header=False,
                                                          index_col=[0], usecols=[0],
                                                          names=["tracking_id"])

        #, dtype={"idx": np.int, "tracking_id": np.object}
        # print track_df
        print(("self.all_genes_readcnt_tracking_ids_df shape: ", self.all_genes_readcnt_tracking_ids_df.shape))

    def readcnt_known_genes_matrix_old(self):

        # calculate initial matrix
        print("in readcnt_known_genes_matrix:")

        # initially only ids
        self.readcnt_known_genes_matrix_df = self.all_genes_readcnt_tracking_ids_df.copy(deep=True)
        print(("self.readcnt_known_genes_matrix_df: ", self.readcnt_known_genes_matrix_df))
        cur_df = pd.DataFrame()

        for smp_idx in range(self.nb_samples):
        # for smp_idx in range(2):
            # set index in order to make all path work
            self.smp_idx = smp_idx
            print(("self.smp_idx: ", self.smp_idx))
            print(("self.smp_name: ", self.smp_name))    # read sample matrix
            try:
                cur_df = pd.read_csv(self.genes_readcnt_tracking_f, sep='\t', header=False,
                                     # index_col=[0],
                                     usecols=[0, 1],
                                     names=["tracking_id", "readcnt"])
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                continue

            cur_agg_readcnt = cur_df.groupby(["tracking_id"])
            cur_readcnt = cur_agg_readcnt["readcnt"].agg({'readcnt_avg': np.average})

            print(("cur_readcnt.shape: ", cur_readcnt.shape))

            self.readcnt_known_genes_matrix_df = pd.merge(self.readcnt_known_genes_matrix_df, cur_readcnt,
                                                       how='inner', left_index=True, right_index=True)
            self.readcnt_known_genes_matrix_df.rename(columns={'readcnt_avg': self.smp_name}, inplace=True)
            print(("smp: %s, rows: %s " % (self.smp_name, self.readcnt_known_genes_matrix_df.shape[0])))




    def kallisto_isofs_matrix(self):

        # calculate initial matrix
        print("in readcnt_known_genes_matrix:")

        # initially only ids
        self.isofs_matrix_df = self.isofs_tracking_ids_df.copy(deep=True)
        self.isofs_matrix_df.set_index(["transcript_id"], inplace=True)

        print(("self.isofs_matrix_df: ", self.isofs_matrix_df))
        cur_df = pd.DataFrame()

        for smp_idx in range(self.nb_samples):
        # for smp_idx in range(2):
            # set index in order to make all path work
            self.smp_idx = smp_idx
            print(("self.smp_idx: ", self.smp_idx))
            print(("self.smp_name: ", self.smp_name))    # read sample matrix
            try:
                cur_df = pd.read_csv(self.isofs_kallisto_tracking_f, sep='\t', header=False,
                                     # index_col=[0],
                                     usecols=[0, 4],
                                     names=["target_id", "tpm"])
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                continue

            cur_agg = cur_df.groupby(["target_id"])
            cur_cnt = cur_agg["tpm"].agg({'cnt_avg': np.average})

            # print("cur_cnt: \n", cur_cnt)

            self.isofs_matrix_df = pd.merge(self.isofs_matrix_df, cur_cnt,
                                                       how='inner', left_index=True, right_index=True)
            # print("self.isofs_matrix_df: \n", self.isofs_matrix_df)

            self.isofs_matrix_df.rename(columns={'cnt_avg': self.smp_name}, inplace=True)
            print(("smp: %s, rows: %s " % (self.smp_name, self.isofs_matrix_df.shape[0])))

    def save_kallisto_isofs_matrix(self):
        self.isofs_matrix_df.to_csv(self.kallisto_isofs_matrix_f,
                                               sep=',', header=True, index=True, index_label="transcript_id")









    def readcnt_rprt(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        print("Not implemented error codes...\n")


        # normal run using the transcriptome gtf
        # ------------------------------------------------------------------
        self.readcnt_prefix = "all"

        # readcnt genes_matrix
        # self.readcnt_find_reference()
        # self.save_genes_readcnt_tracking_ids()
        # self.load_genes_readcnt_tracking_ids()

        self.load_genes_tracking_ids()

        self.readcnt_known_genes_matrix()
        self.save_readcnt_known_genes_matrix()

        # corrected run using the bulk gtf
        # ------------------------------------------------------------------
        self.readcnt_prefix = "bulk_%s" % self.c3p_size

        # readcnt genes_matrix
        # self.readcnt_find_reference()
        # self.save_genes_readcnt_tracking_ids()
        # self.load_genes_readcnt_tracking_ids()

        self.readcnt_known_genes_matrix()
        self.save_readcnt_known_genes_matrix()

        return [0]


    def kallisto_rprt(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))


        # self.load_genes_tracking_ids()
        # self.kallisto_genes_matrix()

        self.load_isofs_tracking_ids()
        self.kallisto_isofs_matrix()
        self.save_kallisto_isofs_matrix()

        # self.save_readcnt_known_genes_matrix()

        # corrected run using the bulk gtf
        # ------------------------------------------------------------------

        # readcnt genes_matrix

        return [0]



    def rpkmsat_rprt(self):
        print("Not implemented ...\n")

        return [0]







    def c3p_chrom_gen(self):


        store = pd.HDFStore(self.conf.ref_transcr_h5)
        dist = int(self.c3p_size)
        chrom = self.c3p_contig_name

        print(store)
        sel = store.select('all/compr', where="seqname == %r & feature == exon" % chrom,
                           columns=['gene_id', 'transcript_id', 'exon_id', 'start', 'end',
                                    'source', 'gene_name', 'transcript_name', 'exon_number', 'strand'])

        all_ex_df = sel
        all_ex_df.reset_index(inplace=True, drop=True)
        # sort for output
        all_ex_df.sort(['gene_id', 'transcript_id', 'end'], inplace=True)
        all_ex_df.to_csv('all_ex_df.csv', sep=","
                           ,header=True
                           ,index=True
                           ,index_label="idx"
                           )



        # print("all_ex_df: \n", all_ex_df)

        # exit(0)

        tr_gn_df = all_ex_df.groupby('transcript_id').first()
        tr_gn_df.drop('exon_id', axis=1, inplace=True)
        tr_gn_df.drop('start', axis=1, inplace=True)
        tr_gn_df.drop('end', axis=1, inplace=True)
        # print("tr_gn_df: \n", tr_gn_df)


        tr_fp_df = pd.read_csv(self.isofs_fpkm_tracking_f, sep='\t', header=False,
                                         index_col=[0],
                                         usecols=[0, 9],
                                         names=["transcript_id", "fp"]
                                  )
        # print("tr_fp_df: \n", tr_fp_df)

        # join t_g_f

        # tr_gn_fp_df = pd.merge(tr_gn_df, tr_fp_df, how='inner', left_on=['transcript_id'], right_on=['transcript_id'])
        all_tr_gn_fp_df = pd.merge(tr_gn_df, tr_fp_df, how='inner', left_index=True, right_index=True)
        # print("tr_gn_fp_df: \n", tr_gn_fp_df)
        all_tr_gn_fp_df.reset_index(level=0, inplace=True)

        g = all_tr_gn_fp_df.sort('fp', ascending=False).groupby(['gene_id'], as_index=False)
        # print("g: ", g)
        tr_gn_fp_df = g.agg({'transcript_id': lambda x: x.iloc[0], 'fp': lambda x: x.iloc[0]})
        # gb.rename(columns={'score': 'cnt'}, inplace=True)

        # gb_min = g.agg({'rname': lambda x: x.iloc[0], 'score': 'min'})
        # gb['score_min'] = gb_min.score
        # print ("gb_cnt: ", gb_cnt)
        # print ("gb_min: ", gb_min)
        # gb_cnt['bar'] = df.bar.map(str) + " is " + df.foo
        # gb = g.apply(self.raw_stats)


        # print("tr_gn_fp_df: \n", tr_gn_fp_df)
        # tr_gn_fp_df is the list of best ranking fpkm transcript per gene

        # here we delete fpkm ==0
        tr_gn_fp_df=tr_gn_fp_df[tr_gn_fp_df['fp'] != 0]


        # calculate best transcripts boundaries based on their exomes and their strand

        best_tr_ex_df = pd.merge(tr_gn_fp_df, all_ex_df, how='inner',
                                 left_on=['transcript_id'], right_on=['transcript_id'])

        print(("best_tr_ex_df: \n", best_tr_ex_df))

        # aggregate to extract limits
        g = best_tr_ex_df.groupby(['transcript_id'], as_index=False)
        print(("g: \n", g))

        gr = g.agg({'start': 'min', 'end': 'max', 'strand': lambda x: x.iloc[0]
                    ,'fp': lambda x: x.iloc[0]}
                   )

        # gr.to_csv('gr.csv', sep=","
        #                        ,header=True
        #                        ,index=True
        #                        ,index_label="idx"
        #                        )

        start = gr['start'].values
        end = gr['end'].values
        strand_np = gr['strand'].values
        start_p = (gr['start'] + dist).values
        end_m = (gr['end'] - dist).values

        ts = np.where(strand_np == '+', end_m, start)
        te = np.where(strand_np == '+', end, start_p)

        gr.drop('start', axis=1, inplace=True)
        gr.drop('end', axis=1, inplace=True)
        gr.insert(2, 'ts', ts)
        gr.insert(3, 'te', te)

        gr['clust'] = np.zeros(gr.shape[0])

        best_tr_info_df = gr.copy()
        best_tr_info_df.reset_index(inplace=True, drop=True)
        best_tr_info_df.drop_duplicates()

        # print("best_tr_info_df: \n", best_tr_info_df)

            # print("df.shape before drop: \n", df.shape)
            # df.drop_duplicates(cols=["idx_seq"], inplace=True)
            # print("df.shape after drop: \n", df.shape)


        # clustering

        n = gr.shape[0]
        x = scipy.sparse.lil_matrix( (n,n) )

        ts = best_tr_info_df['ts'].values
        te = best_tr_info_df['te'].values

        for (i, j) in itertools.product(list(range(n)), repeat=2):
            sx = ts[i]
            sy = ts[j]
            ex = te[i]
            ey = te[j]

            if (sx <= ey) & (sy <= ex):
                x[i,j] = 1

        # connected components
        (n_components, labels) = scipy.sparse.csgraph.connected_components(x)
        # print("n_components: ", n_components)
        # print("labels: ", labels, labels.size)

        best_tr_info_df['clust'] = labels
        print(("best_tr_info_df: \n", best_tr_info_df))

        best_tr_info_df.to_csv('best_tr_info_df.csv', sep=","
                               ,header=True
                               ,index=True
                               ,index_label="idx"
                               )

        # g = best_tr_info_df.groupby(['clust'], as_index=False)
        # print("g: \n", g)

        # gr = g.agg({'transcript_id': 'count'}
        #            )

        # print("gr: \n", gr)

        # gr.to_csv('clust_count.csv', sep=","
        #           , header=True
        #           , index=True
        #           , index_label="idx"
        #           )

        # stage 3)
        # within cluster, setect T according to high fpkm

        best_clust_tr_df = best_tr_info_df.sort('fp', ascending=False).groupby(['clust'], as_index=False) \
            .agg({'transcript_id': lambda x: x.iloc[0], \
                  'fp': lambda x: x.iloc[0], \
                  'ts': lambda x: x.iloc[0], \
                  'te': lambda x: x.iloc[0], \
                  'strand': lambda x: x.iloc[0]})\
            .reindex_axis(['clust','transcript_id', 'fp', 'ts', 'te', 'strand'], axis=1)

        # best_clust_tr_df.to_csv('best_clut_tr_info_df.csv', sep=","
        #                    ,header=True
        #                    ,index=True
        #                    ,index_label="idx"
        #                    )
        best_clust_tr_df.drop('clust', axis=1, inplace=True)
        best_clust_tr_df.drop('fp', axis=1, inplace=True)


        clust_exons_df = pd.merge(best_clust_tr_df, all_ex_df, how='inner',
                                 left_on=['transcript_id'], right_on=['transcript_id'])



        clust_exons_df.drop('strand_y', axis=1, inplace=True)
        clust_exons_df.rename(columns={'strand_x': 'strand'}, inplace=True)


        # print("clust_exons_df: \n", clust_exons_df)

        # filter exons
        ex_columns = ['gene_name', 'transcript_id', 'transcript_name', 'exon_id','exon_number', 'seqname', 'start', 'end', 'source']
        ex_ = pd.DataFrame(columns=ex_columns)


        # transcript
        ts_np = clust_exons_df['ts'].values
        te_np = clust_exons_df['te'].values
        # exome
        es_np = clust_exons_df['start'].values
        ee_np = clust_exons_df['end'].values

        # strand
        st_np = clust_exons_df['strand'].values
        sp_np = (st_np == '+')
        sn_np = (st_np == '-')

        # corrections for positive strand

        # line end
        le_np = np.maximum(te_np - dist, ts_np)

        # keep end
        ke_np = (ee_np > le_np)

        # correct exome start
        cs_np = np.maximum(es_np, le_np)

        # corrections for negative strand

        # line start
        ls_np = np.minimum(ts_np + dist, te_np)

        # keep start
        ks_np = (es_np < ls_np)

        # correct exome end
        ce_np = np.minimum(ee_np, ls_np)

        # final exome coordinates
        # final exome start
        fs_np = np.select([sp_np, sn_np], [cs_np, es_np])

        # final exome end
        fe_np = np.select([sp_np, sn_np], [ee_np, ce_np])

        # final keep
        kf_np = np.select([sp_np, sn_np], [ke_np, ks_np], default=False)

        clust_exons_df['kf'] = kf_np
        clust_exons_df['fs'] = fs_np
        clust_exons_df['fe'] = fe_np

        print(("clust_exons_df: \n", clust_exons_df.shape))

        clust_exons_df.to_csv('clust_exons_df.csv', sep=","
                       ,header=True
                       ,index=True
                       ,index_label="idx"
                       )

        # keep only the selected ones
        cle_df = clust_exons_df.query('kf == True')
        print(("cle_df: \n", cle_df.shape))

        clust_gtf_f = open(self.c3p_smp_size_contig_gtf, "w")

       # '', 'exon_id','exon_number', 'seqname', 'start', 'end', 'source'

        for source, \
            start, \
            end, \
            strand, \
            gene_id, \
            gene_name, \
            transcript_id, \
            transcript_name, \
            exon_id, \
            exon_number \
            in zip(cle_df['source'],
                              cle_df['fs'],
                              cle_df['fe'],
                              cle_df['strand'],
                              cle_df['gene_id'],
                              cle_df['gene_name'],
                              cle_df['transcript_id'],
                              cle_df['transcript_name'],
                              cle_df['exon_id'],
                              cle_df['exon_number']
                             ):

            exon_row = r"""%(chrm)s%(sep)s%(source)s%(sep)sexon%(sep)s%(start)s%(sep)s%(end)s%(sep)s.%(sep)s%(strand)s%(sep)s.%(sep)sgene_id "%(gene_id)s"; transcript_id "%(transcript_id)s"; gene_name "%(gene_name)s"; transcript_name "%(transcript_name)s"; exon_id "%(exon_id)s"; exon_number %(exon_number)s;""" \
                % {"chrm": chrom,
                   "source": source,
                   "sep": "\t",
                   "start": start,
                   "end": end,
                   "strand": strand,
                   "gene_id": gene_id,
                   "gene_name": gene_name,
                   "transcript_id": transcript_id,
                   "transcript_name": transcript_name,
                   "exon_id": exon_id,
                   "exon_number": exon_number
                   }
            clust_gtf_f.write(exon_row)
            clust_gtf_f.write("\n")

        clust_gtf_f.close()

        store.close()


        # del sel
        # del groups
        # del gr_res
        gc.collect()

    def c3p_gtf_gen(self):

        args = {"rprt_idx": self.rprt_idx}
        print(("parameters: \n", args))

        exit_codes = []

        # only one report == 1
        if self.rprt_idx == 1:
            # calculate for the bulk sample
            # from rnaseq/init_proj()
            # the fpkm values come from the smp_idx
            self.smp_idx = self.c3p_smp_idx
            # the c3p values are already loaded
            self.c3p_smp_gtf_gen()

        exit_codes = [0]
        # print("exit_codes: ", exit_codes)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print(("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success))

        # mark as done
        self.mark_done_args(args, success)



    def c3p_smp_gtf_gen(self):

        for i in [self.c3p_smp_size_gtf]:
            if os.path.exists(i):
                os.remove(i)

        # fasta start
        cmd = r"""touch %s""" % self.c3p_smp_size_gtf
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print(("rc3: ", rc3))

        for chrom in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
          "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y"]:
            self.c3p_contig_name = chrom
            self.c3p_chrom_gen()

            # add to gtf
            cmd = r"""cat %s >> %s""" % (self.c3p_smp_size_contig_gtf, self.c3p_smp_size_gtf)
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print(("rc3: ", rc3))






    # ############################################################################
    # ##########################     Push jobs        ############################
    # ############################################################################


    # def push_jobs_sample_qa(self):
    #     rc = self.redis_obj.redis_client
    #     samples = self.conf.sample_names
    #     nb_samples = samples.shape[0]
    #
    #     rc.delete("sample_qa_all")
    #     rc.delete("sample_qa")
    #     done_jobs = rc.lrange("sample_qa_done", 0, -1)
    #     print "done_jobs: ", done_jobs
    #     submitted_jobs = []
    #
    #
    #     #rc.set("shutdown",0)
    #     #rc.set("active_queue","call_hc")
    #     for smp_idx in range(nb_samples):
    #         #print "sample,contig: %s, %s" % (smp_idx, contig_idx)
    #         args = {"smp_idx": smp_idx}
    #         args = json.dumps(args)
    #         rc.rpush("sample_qa_all", args)
    #         if args in done_jobs:
    #             print "job done"
    #         else:
    #             rc.rpush("sample_qa", args)
    #             submitted_jobs.append(args)
    #
    #     return submitted_jobs.__len__()
    #
    #
    #
    # def push_jobs_comb_vars(self):
    #     print "in push_jobs_comb_vars.."
    #     rc = self.redis_obj.redis_client
    #
    #     rc.delete("comb_vars_all")
    #     rc.delete("comb_vars")
    #     done_jobs = rc.lrange("comb_vars_done", 0, -1)
    #     print "done_jobs: ", done_jobs
    #     submitted_jobs = []
    #
    #     #rc.set("shutdown",0)
    #     #rc.set("active_queue","call_hc")
    #     args = {"noarg": 0};
    #     args = json.dumps(args)
    #     rc.rpush("comb_vars_all", args)
    #     if args in done_jobs:
    #         print "job done"
    #     else:
    #         rc.rpush("comb_vars", args)
    #         submitted_jobs.append(args)
    #
    #     return submitted_jobs.__len__()

    #




    # ###########################################
    # ##############################   Administration


    # def loop_todo(self, gid):
    #     # rc = self.redis_obj.redis_client
    #     #report and register
    #     #zscore = rc.zscore("client_hosts",host)
    #     #print "host,zscore: ",host, zscore
    #     #if zscore is not None:
    #     #    rc.zincrby("client_hosts", host)
    #     #else:
    #     #    rc.zadd("client_hosts", 1, host)
    #
    #     # loads and dumps are complimentary operations
    #     # args2 = json.dumps(args)
    #     # r_server.rpop("mylist")
    #     if self.queue_name == "trim_lane_trimmomatic" and lid < 6:
    #         args = rc.lpop(active_queue)
    #         if not args:
    #             # check if all done
    #             if rc.llen("trim_lane_trimmomatic_done") == rc.llen("trim_lane_trimmomatic_all"):
    #                 rc.set("active_queue", "align_lane_tophat")
    #         else:
    #             print "args: ", args
    #             args = json.loads(args)
    #             smp_lane_idx = int(args["smp_lane_idx"])
    #             self.trim_lane_trimmomatic(smp_lane_idx)
    #
    #     elif active_queue == "align_lane_tophat" and lid < 6:
    #         args = rc.lpop(active_queue)
    #         if not args:
    #             #check if all done
    #             if rc.llen("align_lane_tophat_done") == rc.llen("align_lane_tophat_all"):
    #                 rc.set("active_queue", "wait")
    #         else:
    #             print "args: ", args
    #             args = json.loads(args)
    #             smp_lane_idx = int(args["smp_lane_idx"])
    #             self.align_lane_tophat(smp_lane_idx)
    #     elif active_queue == "merge_lanes_rnaseqc" and lid < 6:
    #         args = rc.lpop(active_queue)
    #         if not args:
    #             #check if all done
    #             if rc.llen("merge_lanes_rnaseqc_done") == rc.llen("merge_lanes_rnaseqc_all"):
    #                 rc.set("active_queue", "wait")
    #         else:
    #             print "args: ", args
    #             args = json.loads(args)
    #             smp_idx = int(args["smp_idx"])
    #             self.merge_lanes_rnaseqc(smp_idx)
    #
    #     elif active_queue == "sample_qa" and lid < 12:
    #         #args = rc.brpop(active_queue,0)[1]
    #         args = rc.lpop(active_queue)
    #         if not args:
    #             pass
    #             #check if all done
    #             #if rc.llen("call_hc_done")==rc.llen("call_hc_all"):
    #             #    rc.set("active_queue","cat_hc")
    #         else:
    #             print "args: ", args
    #             args = json.loads(args)
    #             smp_idx = int(args["smp_idx"])
    #             self.sample_qa(smp_idx)
    #             #deregister
    #             #zscore = rc.zscore("client_hosts",host)
    #             #print "host,zscore: ",host, zscore
    #             #if zscore is not None:
    #             #    if zscore == 0:
    #             #        rc.zrem("client_hosts", host)
    #             #    else:
    #             #        rc.zincrby("client_hosts", host, -1)
    #

    def loop(self, gid):
        # self.gid = gid
        # print("in loop...", gid)
        # # recuperate local id
        # lid = int(self.redis_obj.redis_client.hget("client:%s" % gid, "lid"))
        #
        # while self.redis_obj.redis_client.get("shutdown") == "0":
        #     time.sleep(8)
        #     print("self.redis_obj.queue_active: ", self.redis_obj.queue_active)
        #     print("lid: ", lid)
        self.redis_obj.gid = gid

        print(("in loop...", gid))
        # recuperate local id
        lid = int(self.redis_obj.redis_client.hget("client:%s" % gid, "lid"))
        print(("lid: ", lid))
        # set pid
        self.redis_obj.register_pid()
        print(("pid: ", self.redis_obj.pid))

        while self.redis_obj.redis_client.get("shutdown") == "0":
            time.sleep(8)
            print(("self.redis_obj.queue_active: ", self.redis_obj.queue_active))
            print(("lid: ", lid))


            if self.redis_obj.queue_active == "wait" and lid < 6:
                # to do something waiting
                pass
            elif self.redis_obj.queue_active == "finished":
                self.redis_obj.redis_client.set("shutdown", 1)

            elif self.redis_obj.queue_active == "trim_lane_trimmomatic" and lid < 6:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "align_lane_tophat" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "merge_lanes" and lid < 6:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "merge_lanes_rnaseqc" and lid < 4:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "c3p_gtf_gen" and lid < 1:
                self.unpack_execute_active_queue()


            elif self.redis_obj.queue_active == "extract_merged_bam_to_fastqs" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "kallisto_smp_transcr" and lid < 6:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "rseqc" and lid < 8:
                self.unpack_execute_active_queue()


        print("loop finished ok..")


    def subm_client_jobs(self):

        cmd = r"""TARGET_JOB_ID=$(echo "naseq_server.py --spawn-clients-blocking" | qsub -V -m ae -W umask=0002 -A wst-164-aa -d %(work_dir)s -j oe -o %(job_output_dir)s -N spawclb -l walltime=6:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo $TARGET_JOB_ID
""" % {"work_dir": self.conf.pysrc_dir, "job_output_dir": self.conf.job_output_dir}
        print(cmd)

        p = subprocess.Popen(cmd, shell=True,
                             cwd=self.conf.subm_dir,
                             #stdin = r2,
                             stdout=subprocess.PIPE)
        p.wait()
        out = p.stdout.read().strip()

        print(("out: ", out))




        #loads and dumps are complimentary operations
        #args2 = json.dumps(args)

    # idx = rc.lpop(active_queue)
    # if not idx: continue




# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    pass
