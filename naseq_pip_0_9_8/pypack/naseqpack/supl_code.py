

def gtf_to_csv(self):

        # Input files.
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/ucRn6sp3p92-em-v83.gtf"
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/test.gtf"
        # GENCODE = self.conf.ref_gen_transcr_gtf
        # GENCODE = "/gs/project/wst-164-aa/share/raglab_prod/igenomes/ucMm10p5a3/orig/mm10/gencode.vM16.primary_assembly.annotation.gtf.gz"


        # NCBI_ENSEMBL = args['-n']

        # Output file prefix.
        # GENE_LENGTHS = "ncbi_ensembl_coding_lengths.txt.gz"

        # with log("Reading the Gencode annotation file: {}".format(GENCODE)):
        #     gc = GTF_ENCODE.dataframe(GENCODE)
        #
        # print("gc: \n ", gc, gc.columns)

        # cols_df = pd.DataFrame({'columns': gc.columns})



        # Convert columns to proper types.
        gc.start = gc.start.astype(int)
        gc.end = gc.end.astype(int)

        # chrs_df = gc.groupby('seqname').first().iloc[:,[]].reset_index('seqname') #.index #.values #.loc[:,['seqname']]
        # print("chrs_df: ", chrs_df)
        # "/gs/project/wst-164-ab/share/expr/a005-d030/conf/gencode.csv"
        gc.to_csv("/gs/project/wst-164-aa/share/raglab_prod/igenomes/ucMm10p5a3/orig/mm10/gencode.vM16.primary_assembly.annotation.csv",
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


        gc2 = gc.copy()

        # all transcripts
        sel = gc2.loc[:, ['transcript_id', 'transcript_name', 'gene_id', 'gene_name', 'source']]


        sel.set_index(['transcript_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.isofs_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # all genes
        sel = gc2.loc[:, ['gene_id', 'gene_name', 'source']]
        sel.set_index(['gene_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.genes_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # Sort in place.
        # gc.sort(['seqname', 'start', 'end'], inplace=True)

        # print("exon: \n ", exon)
        #


    def gtf_parse(self):

        pd.set_option('io.hdf.default_format', 'table')

        # Input files.
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/ucRn6sp3p92-em-v83.gtf"
        GENCODE      = "/gs/project/wst-164-ab/share/raglab_prod/igenomes/ucMm10sp3p92/pre-gtf/gencode.vM9.primary_assembly.annotation.gtf"
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/test.gtf"

        # NCBI_ENSEMBL = args['-n']

        # Output file prefix.
        # GENE_LENGTHS = "ncbi_ensembl_coding_lengths.txt.gz"

        with log("Reading the Gencode annotation file: {}".format(GENCODE)):
            gc = GTF_ENCODE.dataframe(GENCODE)

        print("gc: \n ", gc, gc.columns)

        cols_df = pd.DataFrame({'columns': gc.columns})



        # Convert columns to proper types.
        gc.start = gc.start.astype(int)
        gc.end = gc.end.astype(int)

        chrs_df = gc.groupby('seqname').first().iloc[:,[]].reset_index('seqname') #.index #.values #.loc[:,['seqname']]
        print("chrs_df: ", chrs_df)

        chrs_df.to_csv("/gs/project/wst-164-ab/share/raglab_prod/igenomes/ucMm10sp3p92/pre-gtf/ge_gtf_chrs.csv",
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )

        cols_df.to_csv("/gs/project/wst-164-ab/share/raglab_prod/igenomes/ucMm10sp3p92/pre-gtf/ge_gtf_cols.csv",
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


        # Sort in place.
        # gc.sort(['seqname', 'start', 'end'], inplace=True)

        # print("exon: \n ", exon)
        #
        # gc.collect()

