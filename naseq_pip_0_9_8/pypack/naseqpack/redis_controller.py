#!/usr/bin/env python

from __future__ import print_function

import shutil

#
import numpy as np
import pandas as pd

import os
# from treedict import TreeDict
import redis
import time
import socket
import json
import psutil

import sys

import subprocess

from naseq_utils import *

import threading



class RedisController(object):
    def __init__(self, conf):

        self.queue_work = None
        self.conf = conf
        self.redis_host = None
        self.gid = None

        print("in RedisController(object)..")

# ##################################### config
    @property
    def redis_conf(self):
        return os.path.join(os.getenv('REDIS_HOME', ""), "redis.conf")

    @property
    def redis_log(self):
        return os.path.join(self.conf.redis_dir, "redis_server.log")

    @property
    def redis_csv(self):
        return os.path.join(self.conf.redis_dir, "redis_server.csv")

# ################################## qgraph

    # this is forwarded to/from redis
    # @property
    # def queue_active(self):
    #     return self.redis_client.get("active_queue")
    #
    # @queue_active.setter
    # def queue_active(self, val):
    #     self.redis_client.set("active_queue", val)

    # create redis graph using dictionary
    def qgraph_config(self, graph_dict):

        rc = self.redis_client

        # clean
        # for key in rc.scan_iter("qgraph:*"):
        #     rc.delete(key)


        print("graph_dict: ", graph_dict)
        # vertices are stored in a set qgraph:qnames

        # clean graph definitions
        rc.delete("qgraph:names")
        # dependencies are stored in another set qgraph:deps:vertex
        for key in rc.scan_iter("qgraph:deps:*"):
            rc.delete(key)
        for key in rc.scan_iter("qgraph:props:*"):
            rc.delete(key)

        # update with keys
        for key, value in graph_dict.items():
            print("key: ", key, " value: ", value)
            # qscore = float(value["qscore"])

            # nsp.redis_obj.redis_client.hset("next_queue", key, value)
            rc.sadd("qgraph:names", key)
            # set initial state to closed
            rc.hset("qgraph:props:%s" % key, "state", "closed")


            for kdep in value['deps']:
                print("kdep: ", kdep)
                rc.sadd("qgraph:deps:%s" % key, kdep)


    # check mark open vertices without dependencies
    #  or having satisfied all of them
    def qgraph_ckeck_mark_open(self):

        rc = self.redis_client
        #  we do not want done queues to be reopened
        closed_queues = [x for x in rc.smembers("qgraph:names") if rc.hget("qgraph:props:%s" % x, "state") == "closed"]
        for queue in closed_queues:
            state = rc.hget("qgraph:props:%s" % queue, "state")
            deps = rc.smembers("qgraph:deps:%s" % queue)
            deps_state = [rc.hget("qgraph:props:%s" % x, "state") for x in deps]
            is_all_deps_done = all(i == "done" for i in deps_state)
            # deps =  [x for x in S if x % 2 == 0]
            # for dp in self.redis_client.sismember("qgraph:deps:%s" % key, kdep)
            # print("queue: ", queue, "state: ", state, "deps: ", deps, "deps_state: ", deps_state, "is_all_deps_done: ", is_all_deps_done, "len: ", len(deps), file=sys.stderr, flush=True)
            # open
            if is_all_deps_done or len(deps) == 0:
                rc.hset("qgraph:props:%s" % queue, "state", "open")

            state = rc.hget("qgraph:props:%s" % queue, "state")
            # print("new_state: ", state, file=sys.stderr, flush=True)

    # check mark done vertices
    def qgraph_ckeck_mark_done(self):

        rc = self.redis_client

        open_queues = [x for x in rc.smembers("qgraph:names") if rc.hget("qgraph:props:%s" % x, "state") == "open"]
        for queue in open_queues:
            # check this queue
            self.qwork = queue
            jobs_done_len = rc.llen(self.qwork_done)
            jobs_all_len = rc.llen(self.qwork_all)
            # if all jobs are done
            # or there are no jobs to do in the first place
            # mark this queue as done
            if jobs_done_len >= jobs_all_len or jobs_all_len == 0:
                rc.hset("qgraph:props:%s" % queue, "state", "done")
                print("queue %s is now done ..." % queue, file=sys.stderr, flush=True)

    def qgraph_job_status(self):
        rc = self.redis_client

        for queue in sorted(rc.smembers("qgraph:names")):
            # check this queue
            self.qwork = queue
            jobs_all = rc.llen(self.qwork_all)
            jobs_todo = rc.llen(self.qwork_todo)
            jobs_done = rc.llen(self.qwork_done)
            jobs_fail = rc.llen(self.qwork_fail)
            state = rc.hget("qgraph:props:%s" % self.qwork, "state")

            if state == "closed":
                state_str = "     "
            elif state == "open":
                state_str = "++++++++"
            else:
                state_str = state


            if jobs_todo != 0:
                jobs_todo_str = "+> %s <+" % jobs_todo
            else:
                jobs_todo_str = "        "

            if jobs_fail != 0:
                jobs_fail_str = "--> %s <-" % jobs_fail
            else:
                jobs_fail_str = "         "


            print("{:20s} {}\t({}/{})\t{}\t{}".format(queue, state_str, jobs_done, jobs_all, jobs_fail_str, jobs_todo_str))
            # print("%s = %s\t\t(%s/%s)\t(-%s+)\t%s" % (queue, state, jobs_done, jobs_all, jobs_fail, jobs_todo_str), file=sys.stderr, flush=True)


    @property
    def qgraph_prop_open(self):

        rc = self.redis_client

        open_queues = [x for x in rc.smembers("qgraph:names") if rc.hget("qgraph:props:%s" % x, "state") == "open"]
        return open_queues


    @property
    def qgraph_all_done(self):

        rc = self.redis_client

        all_queues = rc.smembers("qgraph:names")
        queues_state = [rc.hget("qgraph:props:%s" % x, "state") for x in all_queues]
        is_all_done = all(i == "done" for i in queues_state)

        return is_all_done

# ###################################   qwork
    # this is local
    @property
    def qwork(self):
        return self.__qwork

    @qwork.setter
    def qwork(self, val):
        self.__qwork = val

    @property
    def qwork_all(self):
        return r"""qgraph:qwork:all:%s""" % self.qwork

    @property
    def qwork_todo(self):
        return r"""qgraph:qwork:todo:%s""" % self.qwork

    @property
    def qwork_fail(self):
        return r"""qgraph:qwork:fail:%s""" % self.qwork

    @property
    def qwork_done(self):
        return r"""qgraph:qwork:done:%s""" % self.qwork



# ###################################   queue
    # this is local
    @property
    def queue_work(self):
        return self.__queue_work

    @queue_work.setter
    def queue_work(self, val):
        self.__queue_work = val

    @property
    def queue_work_all(self):
        return r"""%s_all""" % self.queue_work

    @property
    def queue_work_fail(self):
        return r"""%s_fail""" % self.queue_work

    @property
    def queue_work_done(self):
        return r"""%s_done""" % self.queue_work


    # this is forwarded to/from redis
    @property
    def queue_active(self):
        return self.redis_client.get("active_queue")

    @queue_active.setter
    def queue_active(self, val):
        self.redis_client.set("active_queue", val)

    @property
    def queue_active_all(self):
        return r"""%s_all""" % self.queue_active

    @property
    def queue_active_done(self):
        return r"""%s_done""" % self.queue_active

# ############################################  clients
    @property
    def redis_client(self):
        return self.__redis_client

    @redis_client.setter
    def redis_client(self, val):
        self.__redis_client = val

############################################

    def conf_redis_server(self):
        print("in conf_redis_server...")
        #clear all worker logs
        filelist = [f for f in os.listdir(self.conf.redis_dir) if (f.endswith(".log") or f.endswith(".csv"))]
        for f in filelist:
            os.remove(os.path.join(self.conf.redis_dir, f))


        # configuration neeeds module load
        # self.lcnf.redis_conf = os.path.join(os.getenv('REDIS_HOME', ""), "redis.conf")
        # self.lcnf.redis_log = os.path.join(self.conf.redis_dir, "redis_server.log")

        # self.lcnf.redis_csv = os.path.join(self.conf.redis_dir, "redis_server.csv")

        self.local_host = socket.gethostname()
        print("local_host: ", self.local_host)

        self.redis_host = self.local_host
        print("redis_host: ", self.redis_host)

        d = {"host_name": pd.Series([self.redis_host], index=[0])}
        pdsrv = pd.DataFrame(d)
        print(pdsrv)
        pdsrv.to_csv(self.redis_csv, sep='\t',
                     header=True, index=True, index_label="idx")

        #EVAL "for i, name in ipairs(redis.call('KEYS', ARGV[1])) do redis.call('DEL', name); end" 0 client:*

    def conf_redis_client(self):

        #cmd = os.popen('modulecmd python load raglab/redis/2.8.18')
        #exec (cmd)

        # configuration neeeds module load

        if os.path.exists(self.redis_csv):
            pdsrv = pd.read_csv(self.redis_csv
                                , sep='\t', header=0, index_col=[0],
                                #names=["Gene","Symbol"]
                                )

            print("pdsrv: ", pdsrv)
            self.redis_host = pdsrv["host_name"][0]
            print("redis_host: ", self.redis_host)

        self.local_host = socket.gethostname()
        print("local_host: ", self.local_host)


    # def spawnth_redis(self,conf,log):
    def spawnth_redis(self):
        # do some long download here
        # print("conf: ", conf)
        # print("log: ", log)
        # cmd = r"""redis-server %s >& %s &""" % (self.redis_conf, self.redis_log)
        # print(cmd)
        # redis_p = subprocess.Popen(['redis-server', self.redis_conf], shell=False,
        #                            cwd=self.conf.redis_dir,
                                   # stdin = r2,
                                   # stdout=subprocess.PIPE)

        f = open("blah.txt", "w")
        subprocess.call(['redis-server', self.redis_conf], cwd=self.conf.redis_dir , stdout=f, shell=True)

        # redis_p.wait()


    def start_redis(self):

        print("in start_redis...")

        cmd = r"""redis-cli ping"""
        print(cmd)
        x = runBash(cmd).decode("utf-8")
        print("x: ", x)
        if x == "PONG":
            print("Redis server is already started..")
        else:
            print("Starting Redis server...")
# cmd = r"""nohup redis-server %s >& %s &""" % (self.redis_conf, self.redis_log)
            # threadd = threading.Thread(target=self.spawnth_redis, args=[self.redis_conf,self.redis_log])

            threadd = threading.Thread(target=self.spawnth_redis, args=())
            threadd.daemon = True
            threadd.start()
            time.sleep(10)



    def connect_redis(self):
        print("in connect_redis...")
        # if self.redis_host is not None:
        #     print("host: ", self.redis_host)

        # else:
        #     print("SERVERLESS MODE")

        self.redis_client = redis.StrictRedis(host=self.redis_host, port=6379, db=0, decode_responses=True)
        print("host: ", self.redis_host)

        # try:
        #     self.redis_client = redis.StrictRedis(host=self.redis_host, port=6379, db=0, decode_responses=True)
            # res = self.redis_client.ping()

        # except IOError:
        #     print("IOERRor")
        # except redis.exceptions.ConnectionError:
        #     print("redis.exceptions.ConnectionError")
        # except Exception as e:
        #     print("Exception \n", e)
        # else:
        #     print("error \n")


        # finally:
        #     print("in finally")
        #     print("host: ", self.redis_host)



    def connect_redis_localhost(self):
        print("in connect_redis_localhost...")

        self.redis_host = socket.gethostname()
        print("host: ", self.redis_host)
        self.redis_client = redis.StrictRedis(host=self.redis_host, port=6379, db=0, decode_responses=True)

        # make shure it does not shutdown itself
        self.redis_client.set("shut_server", "0")
        # nor the controller
        self.redis_client.set("shut_ctrl", "0")
        # self.redis_client.save()

    def del_qwork_jobs(self):
        self.redis_client.delete(self.qwork_done)
        self.redis_client.delete(self.qwork_all)
        self.redis_client.delete(self.qwork_fail)
        self.redis_client.delete(self.qwork_todo)

    def push_qjobs_args(self, jobs_args):

        if self.redis_client is None:
            print("queue_work: %s, ------- SERVERLESS MODE " % self.qwork)
            return 0


        print("--------------------------------")
        print("---QWORK --------- %s --------------------" % self.qwork)
        print("--- all, todo, done, fail ")
        print("--------------------------------")
        print(self.redis_client.llen(self.qwork_all),
              self.redis_client.llen(self.qwork_todo),
              self.redis_client.llen(self.qwork_done),
              self.redis_client.llen(self.qwork_fail))
        print("--------------------------------")


        # clean
        # for key in self.redis_client.scan_iter("qgraph:qwork:*"):
        #     self.redis_client.delete(key)


        self.redis_client.delete(self.qwork_all)
        self.redis_client.delete(self.qwork_fail)
        self.redis_client.delete(self.qwork_todo)

        done_jobs = self.redis_client.lrange(self.qwork_done, 0, -1)
        print("done_jobs: ", done_jobs.__len__())
        submitted_jobs = []

        for job_args in jobs_args:

            args = json.dumps(job_args)

            self.redis_client.rpush(self.qwork_all, args)
            if args in done_jobs:
                pass
                # print("job done")
            else:
                self.redis_client.rpush(self.qwork_todo, args)
                submitted_jobs.append(args)

        print("submitted_jobs: ", submitted_jobs)
        print("qwork: %s, jobs: %s " % (self.qwork, submitted_jobs.__len__()))
        return submitted_jobs.__len__()


    def push_jobs_args(self, jobs_args):

        if self.redis_client is None:
            print("queue_work: %s, ------- SERVERLESS MODE " % self.queue_work)
            return 0



        print("---")
        print("self.queue_work_done: ", self.queue_work_done)

        print("self.queue_work: ", self.queue_work)

        self.redis_client.delete(self.queue_work_all)
        self.redis_client.delete(self.queue_work_fail)

        self.redis_client.delete(self.queue_work)

        done_jobs = self.redis_client.lrange(self.queue_work_done, 0, -1)
        print("done_jobs: ", done_jobs.__len__())
        submitted_jobs = []

        for job_args in jobs_args:

            args = json.dumps(job_args)

            self.redis_client.rpush(self.queue_work_all, args)
            if args in done_jobs:
                pass
                # print("job done")
            else:
                self.redis_client.rpush(self.queue_work, args)
                submitted_jobs.append(args)

        print("submitted_jobs: ", submitted_jobs)
        print("queue_work: %s, jobs: %s " % (self.queue_work, submitted_jobs.__len__()))
        return submitted_jobs.__len__()


    def spawnth_ctrl(self):

        ctrl_log = os.path.join(self.conf.redis_dir, "ctrl.log")
        ctrl_err = os.path.join(self.conf.redis_dir, "ctrl.err")
        fout = open(ctrl_log, "w")
        ferr = open(ctrl_err, "w")
        subprocess.call(['naseq_server.py --ctrl'], cwd=self.conf.pysrc_dir, stdout=fout, stderr=ferr, shell=True)


    def spawn_ctrl(self):

        print("Starting Controller server...")

        threadct = threading.Thread(target=self.spawnth_ctrl, args=())
        threadct.daemon = True
        threadct.start()
        # time.sleep(10)

    def spawnth_nodectrl(self):

        nodectrl_log = os.path.join(self.conf.redis_dir, "nodectrl-%s.log" % socket.gethostname())
        nodectrl_err = os.path.join(self.conf.redis_dir, "nodectrl-%s.err" % socket.gethostname())
        fout = open(nodectrl_log, "w")
        ferr = open(nodectrl_err, "w")
        subprocess.call(['naseq_server.py --nodectrl'], cwd=self.conf.pysrc_dir, stdout=fout, stderr=ferr, shell=True)



    def spawn_nodectrl(self):

        # check if already running
        cmd = "curl -I http://localhost:5000 2>/dev/null | head -n 1 | cut -d$' ' -f2"
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.pysrc_dir, stdout=subprocess.PIPE)
        status = p1.communicate()[0].decode("utf-8").rstrip()
        print ("nodectrl port results: ", status)
        if status == '':
            # pass

            # cmd = r"""nohup naseq_server.py --nodectrl >& %s &""" % nodectrl_log_f
            threadnct = threading.Thread(target=self.spawnth_nodectrl, args=())
            threadnct.daemon = True
            threadnct.start()

    def spawnth_client(self, idx=1, gid="11", local_host="localhost"):

        worker_log = os.path.join(self.conf.redis_dir, "wk-%s-%s-%s.log" % (local_host, idx, gid))
        worker_err = os.path.join(self.conf.redis_dir, "wk-%s-%s-%s.err" % (local_host, idx, gid))

        # cannot be fragmented
        cmd = r"""naseq_server.py --loop %(gid)s""" \
              % {"gid": gid
                 }
        print("cmd: ", cmd)

        # cwd = "%s/spawn" % self._ctx.conf.proj_dir
        flog = open(worker_log, "w")
        ferr = open(worker_err, "w")

        # print("cwd: ", cwd)
        subprocess.call([cmd], shell=True, cwd=self.conf.pysrc_dir, stdout=flog, stderr=ferr)



    def spawn_clients(self, ppn, pmem):

        print("in redis_controller.spawn_clients...")

        # save submited limits in redis
        self.redis_client.hset("node:%s" % self.local_host,  "ppn", ppn)
        self.redis_client.hset("node:%s" % self.local_host,  "pmem", pmem)

        self.thr_clnt_a = []
        for i in range(ppn):
            # wait for checks and creation of folders
            time.sleep(2)

            # transaction
            pipe = self.redis_client.pipeline()
            pipe.incr('last_client')
            # pipe.get('last_client')
            gids = pipe.execute()
            gid = gids[0]
            print("gid: ", gid)

            self.redis_client.hset("client:%s" % gid, "gid", gid)
            self.redis_client.hset("client:%s" % gid, "hst", self.local_host)
            self.redis_client.hset("client:%s" % gid, "lid", i)

            elem = threading.Thread(target=self.spawnth_client,
                                    kwargs={"idx": i, "gid": gid, "local_host": self.local_host})
            self.thr_clnt_a.append(elem)
            self.thr_clnt_a[i].daemon = True
            self.thr_clnt_a[i].start()


    def register_pid(self):

        p = psutil.Process()
        
        self.pid = p.pid

        self.redis_client.hset("client:%s" % self.gid, "pid", self.pid)



    def wait_for_shutdown_clients(self):

        while self.redis_client.get("shut_clients") == "0":
            time.sleep(10)

    def wait_for_shutdown_server(self):
        print("in wait_for_shutdown_server..")

        # rrs = self.redis_client.get("shutdown_server")
        # print("rrs: ", rrs)

        while self.redis_client.get("shut_server") == "0":
            # print("waiting")
            time.sleep(10)

        print("ending")
        self.redis_client.save()
        self.redis_client.shutdown()
