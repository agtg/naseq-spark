#!/usr/bin/env python

from collections import OrderedDict

import subprocess
import optparse
import re
import os
import glob
import csv
import sys, random, itertools

# import HTSeq
import shutil

#from numpy.core.tests.test_unicode import assign_values
from numpy.distutils.system_info import dfftw_threads_info

import pysam
from pysam import VariantFile
import csv


import numpy as np
import time
import pandas as pd

import itertools

from naseq import NaSeqPip
from scan_config_dnaseq import ScanConfigDnaSeq
from naseq_utils import *
from redis_controller import RedisController
from naseq_servers import NaseqServers
from sc_qc import ScQc

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

# why not again


import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib.backends.backend_pdf import PdfPages

import datetime

#from jpype import *
import jaydebeapi

import jinja2

from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
from pyspark.sql.types import StructField, StructType, LongType, IntegerType
# spark is an existing SparkSession.
from pyspark.sql import Row
from pyspark.sql import DataFrame


# spark is an existing SparkSession.
from pyspark.sql.functions import lit

from pyspark.sql import functions as pyspfunc
from pyspark.sql.types import *

# import binning as bn
import re

import json
from functools import partial

import pathlib
from ast import literal_eval



# with composition
class DnaSeqPip(NaSeqPip, object):

    def __init__(self):

        self.nb_rprts = 1

        self.conf = None

        # composition
        self.redis_obj = None

        super(DnaSeqPip, self).__init__()



    def load_modules(self):
        pass

        # load bioinformatics libraries
        # system libraries are in ~/.bash_profile
        # load_one_module('raglab/naseq_pip/0.9.4')
        #load_one_module('raglab/sambamba/0.6.5')
        #load_one_module('raglab/htslib/1.3.1')
        #load_one_module('raglab/samtools/1.3.1')
        #load_one_module('raglab/bcftools/1.3.1')
        #load_one_module('raglab/bedtools/2.25.0')
        #load_one_module('raglab/hisat2/2.0.5')
        #load_one_module('raglab/blast/2.3.0')
        #load_one_module('raglab/gatk/3.6.0')
        #load_one_module('raglab/bamutil/1.0.13')
        #load_one_module('raglab/bowtie2/2.2.3')
        #load_one_module('raglab/tophat/2.0.13')
        #load_one_module('raglab/picard/1.141')
        #load_one_module('raglab/rnaseqc/1.1.8')
        #load_one_module('raglab/trimmomatic/0.33')
        #load_one_module('raglab/cufflinks/2.2.1')
        #load_one_module('raglab/snpeff/4_1_k')
        #load_one_module('raglab/jvarkit/15.12.07')
        #load_one_module('raglab/ucscapps/340')
        #load_one_module('raglab/bedops/2.4.7')
        #load_one_module('raglab/seqtk/1.2-r95')
        #load_one_module('raglab/bbmap/36.49')
        #load_one_module('raglab/fml_convert/15.02.04')
        #load_one_module('raglab/bwa/0.7.15')


        # load_one_module('raglab/kallisto/0.42.2.1')
        # load_one_module('raglab/splitNreads/15.11.27')
        # load_one_module('raglab/abra/0.96')

        # RNA migrated modules
        #

        # new Big Data modules



    def init_proj(self):

        self.load_modules()

        cnf = ScanConfigDnaSeq()
        cnf.scan_local()

        cnf.set_proj_dirs()
        cnf.set_proj_files_path_names()


        cnf.load_sample_lanes()
        cnf.load_sample_names()
        # load also the grpsmps
        cnf.load_sample_grps()
        cnf.load_sample_grps_smps()

        if cnf.seq_exp in ["rnvar", "var"]:
            cnf.load_target_karyo_bed()
            cnf.load_contig_names()

        #cnf.load_in_validated_smps()

            cnf.load_regions()

        # composition
        # project configuration
        self.conf = cnf

        # print self.conf.makeReport() #self.conf.items()

        # dynamically add server objects at runtime
        # redis object
        self.redis_obj = RedisController(cnf)
        # servers object
        self.servers_obj = NaseqServers(self)
        # sc_qc object
        self.sc_qc_obj = ScQc(self)

        # take defaults from configuration file
        if hasattr(self.conf, 'caller_prefix'):
            self.caller_prefix = self.conf.caller_prefix

        if hasattr(self.conf, 'use_callers'):
            self.use_callers = self.conf.use_callers

        # initialize fastq reads location to configuration folder
        self.reads_dir = self.conf.reads_dir

    @property
    def contig_idx(self):
        return int(self.__contig_idx)

    @contig_idx.setter
    def contig_idx(self, val):
        self.__contig_idx = val

    @property
    def contig_name(self):
        return self.conf.contig_names.iloc[self.contig_idx, 1]

    @property
    def nb_contigs(self):
        return self.conf.contig_names.shape[0]


    @property
    def contig_bed(self):
        return os.path.join(self.conf.contig_bed_dir, "%s.bed" % self.contig_name)


# ---------------------------------
#     @property
#     def mer_smp_bam(self):
#         return os.path.join(self.align_smp_dir, "%s.mer.bam" % self.smp_name)
#
#     @property
#     def mer_smp_bai(self):
#         return os.path.join(self.align_smp_dir, "%s.mer.bai" % self.smp_name)
#
#
#     @property
#     def mer_smp_glob(self):
#         return os.path.join(self.align_smp_dir, "%s.mer.*" % self.smp_name)
#

    # @property
    # def mer_smp_ontarget_bam(self):
    #     return os.path.join(self.align_smp_dir, "%s.mer.ontarget.bam" % self.smp_name)

# ---------------------------------------


    @property
    def mer_dupl_stats_csv(self):
        return os.path.join(self.conf.metrics2_dir, "mer.dupl.csv")

    @property
    def smp_targ_stats_csv(self):
        return os.path.join(self.align_smp_dir, "%s.smp.targ.stats.csv" % self.smp_name)

    @property
    def targ_stats_csv(self):
        return os.path.join(self.conf.metrics2_dir, "targ.csv")


# ---------------------------  DEPTH
    @property
    def qc_grp_depth_file(self):
        return self.__qc_grp_depth_file

    @qc_grp_depth_file.setter
    def qc_grp_depth_file(self, val):
        self.__qc_grp_depth_file = val

    @property
    def qc_grp_depth_flt_dup(self):
        return self.__qc_grp_depth_flt_dup

    @qc_grp_depth_flt_dup.setter
    def qc_grp_depth_flt_dup(self, val):
        self.__qc_grp_depth_flt_dup = val

    @property
    def smp_depth_conf_csv(self):
        return os.path.join(self.conf.conf_dir, "%s-smp-depth.csv" % self.conf.proj_abbrev)


    @property
    def smpgrp_depth_csv(self):
        return os.path.join(self.align_smpgrp_dir, "%s.%s.%s.%s.grp.depth.csv" % (self.smpgrp_name,
                                                                           self.stats_depth,
                                                                           self.qc_depth_grp_filter,
                                                                           self.qc_depth_grp_file))

    @property
    def smp_depth_stats_summary(self):
        return os.path.join(self.align_smpgrp_dir, "%s.%s.depth.csv.sample_summary" % (self.smpgrp_name, self.conf.stats_depth))


    # ------------------ Grouped Graphics need index
    @property
    def graph_grp_idx(self):
        return self.__graph_grp_idx

    @graph_grp_idx.setter
    def graph_grp_idx(self, val):
        self.__graph_grp_idx = val

    @property
    def depth_stats_dir(self):
        this_dir = os.path.join(self.conf.metrics2_dir, "depth_stats")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def smpgrp_depth_csv(self):
        return os.path.join(self.align_smpgrp_dir, "%s.grp.depth.%s.%s.%s.csv" % (self.smpgrp_name,
                                                                                  self.stats_depth,
                                                                                  self.qc_grp_depth_file,
                                                                                  self.qc_grp_depth_flt_dup))
    @property
    def smpgrp_depth_stats_csv(self):
        return os.path.join(self.depth_stats_dir, "grp.depth.%s.%s.%s.%s.%s.csv" % (self.min_base,
                                                                                    self.stats_depth,
                                                                                    self.qc_grp_depth_file,
                                                                                    self.qc_grp_depth_flt_dup,
                                                                                    self.cls_idx))

    @property
    def depth_stats_pdf(self):
        return os.path.join(self.depth_stats_dir, "qc-depth-%s-%s.pdf" % (self.conf.proj_abbrev, self.conf.stats_depth))


    @property
    def depth_stats_grp_dir(self):
        this_dir = os.path.join(self.depth_stats_dir, "grp-%s" % self.graph_grp_idx)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def depth_stats_grp_csv(self):
        return os.path.join(self.depth_stats_grp_dir, "depth_stats.csv")

    @property
    def ral_smp_contigs_dir_glo(self):
        this_dir = os.path.join(self.align_smp_dir_glo, "ral_smp_contigs")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def ral_smp_contigs_dir_loc(self):
        this_dir = os.path.join(self.align_smp_dir_loc, "ral_smp_contigs")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def ral_smp_contig_intervals(self):
        return os.path.join(self.ral_smp_contigs_dir_loc, "%s.%s.forIndelRealigner.intervals" % (self.smp_name, self.contig_name))

    @property
    def ral_smp_contig_bam(self):
        return os.path.join(self.ral_smp_contigs_dir_glo, "%s.%s.ral.bam" % (self.smp_name, self.contig_name))

    @property
    def ral_smp_aln_bam(self):
        return os.path.join(self.align_smp_dir_loc, "%s.ral.aln.bam" % self.smp_name)

    @property
    def ral_smp_bam(self):
        return os.path.join(self.align_smp_dir_glo, "%s.ral.bam" % self.smp_name)

    @property
    def rca_smp_rpt_dir(self):
        this_dir = os.path.join(self.align_smp_dir, "rca_smp_rpt")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def rca_smp_pre_grp(self):
        return os.path.join(self.rca_smp_rpt_dir, "%s.pre.grp" % self.smp_name)

    @property
    def rca_smp_pre_pdf(self):
        return os.path.join(self.rca_smp_rpt_dir, "%s.pre.pdf" % self.smp_name)

    @property
    def rca_smp_pst_grp(self):
        return os.path.join(self.rca_smp_rpt_dir, "%s.pst.grp" % self.smp_name)

    @property
    def rca_smp_pst_pdf(self):
        return os.path.join(self.rca_smp_rpt_dir, "%s.pst.pdf" % self.smp_name)

    @property
    def smp_gtk_am(self):
        return os.path.join(self.align3_smp_dir_glo, "%s.smp.gtk.bam" % self.smp_name)

    @property
    def smp_gtk_ai(self):
        return os.path.join(self.align3_smp_dir_glo, "%s.smp.gtk.bai" % self.smp_name)

    # --- we need both realigned and recalibrated

    @property
    def grp_oth_am(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.oth.bam" % self.smpgrp_name)

    @property
    def grp_oth_ai(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.oth.bai" % self.smpgrp_name)


    @property
    def grp_gtk_am(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.gtk.bam" % self.smpgrp_name)

    @property
    def grp_gtk_ai(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.gtk.bai" % self.smpgrp_name)


# --------------------- Callers locations

    @property
    def caller_prefix(self):
        return self.__caller_prefix

    @caller_prefix.setter
    def caller_prefix(self, val):
        self.__caller_prefix = val

    @property
    def caller_name(self):
        return self.__caller_name

    @caller_name.setter
    def caller_name(self, val):
        self.__caller_name = val

    @property
    def use_callers(self):
        return self.__use_callers

    @use_callers.setter
    def use_callers(self, val):
        self.__use_callers = val

    @property
    def caller_dir(self):
        loc_dir = os.path.join(self.conf.variants2_dir, "caller-%s-%s" % (self.caller_prefix, self.caller_name))
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def caller_vcf(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/genome_fa/enGRCh37.fa")
        return os.path.join(self.caller_dir, "%s-merged.vcf" % self.caller_name)

    @property
    def caller_renamed_vcf(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/genome_fa/enGRCh37.fa")
        return os.path.join(self.conf.variants2_dir, "%s-renamed.vcf" % self.caller_name)

    @property
    def caller_renamed_vcf_idx(self):
        return os.path.join(self.conf.variants2_dir, "%s-renamed.vcf.idx" % self.caller_name)

    @property
    def caller_contig_bams_dir(self):
        loc_dir = os.path.join(self.caller_dir, "contig_bams")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def caller_contig_vcfs_dir(self):
        loc_dir = os.path.join(self.caller_dir, "contig_vcfs")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def caller_smp_dir(self):
        this_dir = os.path.join(self.caller_dir, self.smpgrp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def caller_smp_contigs_dir(self):
        this_dir = os.path.join(self.caller_smp_dir, "contigs")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def caller_smp_contig_vcf(self):
        return os.path.join(self.caller_smp_contigs_dir, "%s.%s.hc.g.vcf.gz" % (self.smpgrp_name, self.contig_name))

    @property
    def caller_smp_contig_bam(self):
        return os.path.join(self.caller_smp_contigs_dir, "%s.%s.hc.bam" % (self.smpgrp_name, self.contig_name))

    @property
    def caller_smp_contig2_bam(self):
        return os.path.join(self.caller_smp_contigs_dir, "%s.%s.hc2.bam" % (self.smpgrp_name, self.contig_name))


    @property
    def caller_smp_vcf(self):
        return os.path.join(self.caller_smp_dir, "%s.hc.g.vcf.gz" % self.smpgrp_name)

    @property
    def caller_contig_bam(self):
        return os.path.join(self.caller_contig_bams_dir, "%s.hc.bam" % self.contig_name)

    @property
    def caller_contig_cf(self):
        return os.path.join(self.caller_contig_vcfs_dir, "%s.hc.vcf.gz" % self.contig_name)

    
    @property
    def caller_cf(self):
        return os.path.join(self.caller_dir, "%s-merged.vcf" % self.caller_name)

    @property
    def caller_ctg_f(self):
        return os.path.join(self.caller_dir, "%s-merged-ctgs.csv" % self.caller_name)

    @property
    def caller_err_ctg_f(self):
        return os.path.join(self.caller_dir, "%s-merged-err-ctgs.csv" % self.caller_name)


    @property
    def region_idx(self):
        return int(self.__region_idx)

    @region_idx.setter
    def region_idx(self, val):
        self.__region_idx = val

    @property
    def region_name(self):
        return self.conf.region_names.iloc[self.region_idx, 0]

    @property
    def region_contig(self):
        return self.conf.region_names.iloc[self.region_idx, 1]

    @property
    def region_start(self):
        return self.conf.region_names.iloc[self.region_idx, 2]

    @property
    def region_stop(self):
        return self.conf.region_names.iloc[self.region_idx, 3]


    @property
    def nb_regions(self):
        return self.conf.region_names.shape[0]

    @property
    def region_bed(self):
        return os.path.join(self.conf.region_bed_dir, "%s.bed" % self.region_name)

    @property
    def caller_regions_dir(self):
        this_dir = os.path.join(self.caller_dir, "regions")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir
        

    @property
    def caller_region_cf(self):
        return os.path.join(self.caller_regions_dir, "allSamples.%s.vcf.gz" % self.region_name)


    @property
    def caller_reg_f(self):
        return os.path.join(self.caller_dir, "%s-merged-regions.csv" % self.caller_name)

    @property
    def caller_err_reg_f(self):
        return os.path.join(self.caller_dir, "%s-merged-err-regions.csv" % self.caller_name)


    @property
    def merged_cf(self):
        return os.path.join(self.conf.variants2_dir, "%s-merged.vcf.gz" % self.use_callers)

    @property
    def normed_cf(self):
        return os.path.join(self.conf.variants2_dir, "%s-normed.vcf.gz" % self.use_callers)

    @property
    def gts_caller_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "%s.gts.%s.csv" % (self.conf.proj_abbrev, self.caller_name))



    @property
    def snpid_cf(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.snpid.vcf.gz" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def snpeff_cf(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.snpeff.vcf.gz" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def gemini_db(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.gemini.db" % (self.conf.proj_abbrev, self.use_callers))


    @property
    def snpeff_stats_csv(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.snpeff.stats.csv" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def snpeff_stats_html(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.snpeff.stats.html" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def dbnsfp_cf(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.dbnsfp.vcf.gz" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def dbnsfp_cf_tbi(self):
        return os.path.join(self.conf.variants2_dir, "%s-%s.dbnsfp.vcf.gz.tbi" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def dbnsfp_cf_depl_file(self):
        return os.path.join(self.conf.vcfs_dir, "%s-%s.dbnsfp.vcf.gz" % (self.conf.proj_abbrev, self.use_callers))

    @property
    def dbnsfp_cf_tbi_depl_file(self):
        return os.path.join(self.conf.vcfs_dir, "%s-%s.dbnsfp.vcf.gz.tbi" % (self.conf.proj_abbrev, self.use_callers))


    # merging samples into samplegroups
    def merge_smpgrps_gtk(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        exit_codes = []
        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)

        smpgrps_smps_df = self.conf.sample_grps_smps
        # print("smpgrps_smps_df: \n", smpgrps_smps_df)

        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)

        # filter
        smpgrps_smps_df = smpgrps_smps_df[smpgrps_smps_df.index == self.smpgrp_idx]

        print("-------------")
        print(smpgrps_smps_df)

        exit_codes = []

        smps_rca_acc_st = ""
        smps_rca_err_ar = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.grp_gtk_am, self.grp_gtk_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        # for f in glob.glob(self.mer_smp_glob):
        #     os.remove(f)


        for idx, row in smpgrps_smps_df.iterrows():
            print("row: ", row[0])
            self.smp_idx = row[0]

            # print("self.smp_idx: ", self.smp_idx, "self.smp_row: ", self.smp_row)
            print("self.smp_idx: ", self.smp_idx)
            # print("self.smp_gtk_am: ", self.smp_gtk_am)

            # check alignment files and add them to the list to be merged
            # this comes from GATK
            if os.path.exists(self.smp_gtk_am):
                smps_rca_acc_st += r"""%s """ % self.smp_gtk_am

        f1 = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam" % self.smpgrp_name)
        f1_bai = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam.bai" % self.smpgrp_name)

        # clean input files
        for i in [f1, f1_bai]:
            if os.path.exists(i):
                os.remove(i)

        smps_len = smps_rca_acc_st.split().__len__()

        # print("smps_rca_acc_st: ", smps_rca_acc_st)
        # print("smps_len: ", smps_len)

        # one sample does not need renaming
        # link directly to final output
        if smps_len == 0:

            # if merging is not specified, take the same sample name and link later
            # merging has to be specified only for supplementary smpgrp samples

            self.smp_idx = self.smpgrp_idx
            os.symlink(self.smp_gtk_am, self.grp_gtk_am)
            os.symlink(self.smp_gtk_ai, self.grp_gtk_ai)

            exit_codes.append(0)
        elif smps_len == 1:
            # to implement doing copies
            exit(0)

        elif smps_len > 1:
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": smps_rca_acc_st,
                                                                           "fout": f1}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            # rename sample to new smpgrp name
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=ILLUMINA RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": f1,
       "fout": self.grp_gtk_am,
       "smp": self.smpgrp_name}
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)
            exit_codes.append(rc3)

            # temporary file
            for i in [f1, f1_bai]:
                if os.path.exists(i):
                    os.remove(i)

        self.mark_exit_args(exit_codes, args)

    # merging samples into samplegroups
    def merge_smpgrps_oth(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        exit_codes = []
        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)

        smpgrps_smps_df = self.conf.sample_grps_smps
        # print("smpgrps_smps_df: \n", smpgrps_smps_df)

        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)

        # filter
        smpgrps_smps_df = smpgrps_smps_df[smpgrps_smps_df.index == self.smpgrp_idx]

        print("-------------")
        print(smpgrps_smps_df)

        exit_codes = []

        smps_ral_acc_st = ""
        smps_ral_err_ar = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.grp_oth_am, self.grp_oth_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        # for f in glob.glob(self.mer_smp_glob):
        #     os.remove(f)

        for idx, row in smpgrps_smps_df.iterrows():
            print("row: ", row[0])
            self.smp_idx = row[0]

            # print("self.smp_idx: ", self.smp_idx, "self.smp_row: ", self.smp_row)
            print("self.smp_idx: ", self.smp_idx)

            # check alignment files and add them to the list to be merged
            # this is comming from ABRA
            if os.path.exists(self.smp_oth_am):
                smps_ral_acc_st += r"""%s """ % self.smp_oth_am

        f1 = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam" % self.smpgrp_name)
        f1_bai = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam.bai" % self.smpgrp_name)

        # clean input files
        for i in [f1, f1_bai]:
            if os.path.exists(i):
                os.remove(i)

        smps_len = smps_ral_acc_st.split().__len__()

        # print("smps_rca_acc_st: ", smps_rca_acc_st)
        # print("smps_len: ", smps_len)

        # one sample does not need renaming
        # link directly to final output
        if smps_len == 0:

            # if merging is not specified, take the same sample name and link later
            # merging has to be specified only for supplementary smpgrp samples
            self.smp_idx = self.smpgrp_idx
            # also realigned
            os.symlink(self.smp_oth_am, self.grp_oth_am)
            os.symlink(self.smp_oth_ai, self.grp_oth_ai)

            exit_codes.append(0)
        elif smps_len == 1:
            # to implement doing copies
            exit(0)

        elif smps_len > 1:

            # --------------- also realigned we need
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": smps_ral_acc_st,
                                                                           "fout": f1}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            # rename sample to new smpgrp name
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=ILLUMINA RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": f1,
       "fout": self.grp_oth_am,
       "smp": self.smpgrp_name}
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)
            exit_codes.append(rc3)

            # temporary file
            for i in [f1, f1_bai]:
                if os.path.exists(i):
                    os.remove(i)

        self.mark_exit_args(exit_codes, args)


    def push_jobs_region_idx(self):
        #print("in push_jobs_region_idx...")
        jobs_args = []
        #print("self.nb_regions: ", self.nb_regions)
        for region_idx in range(self.nb_regions):
            # print("region: %s" % regions_idx
            args = {"region_idx": region_idx}
            jobs_args.append(args)

        self.redis_obj.push_jobs_args(jobs_args)




    def sort_lane(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        func_impl = thiscallersname() + "_" + self.conf.rmdup_type

        print("func_impl: ", func_impl)

        # exit_codes = list(itertools.chain.from_iterable(exit_codes))
        # [leaf for tree in forest for leaf in tree]
 
        exit_codes = self.__getattribute__(func_impl)()


        print("exit_codes: ", exit_codes)

        exit_codes = [item for sublist in exit_codes for item in sublist]
        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0
        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)
        # mark as done
        self.mark_done_args(args, success)



    def sort_lane_picard(self):

        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        # delete output files
        for i in [self.spln_sort_bam]:
            if os.path.exists(i):
                os.remove(i)

        print("self.spln_runreg: ", self.spln_runreg)

        # scratch_d = os.getenv('LSCRATCH', "")


        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(spln_bam)s -o %(spln_sort_bam)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "spln_bam": self.spln_bam, "spln_sort_bam": self.spln_sort_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]





    def prepare_sort(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        exit_codes = []

         # eliminate singletons
        cmd = r"""sambamba view -F "proper_pair and not secondary_alignment" \
-f bam %(spln_bam)s > %(spln_proper_bam)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "spln_bam": self.spln_bam,
       "spln_proper_bam": self.spln_proper_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)


        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G -n %(spln_proper_bam)s -o %(spln_sort_bam)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "spln_proper_bam": self.spln_proper_bam,
       "spln_sort_bam": self.spln_sort_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        cmd = r"""bedtools bamtobed -bedpe -i %(spln_sort_bam)s > %(spln_bedpe)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "spln_sort_bam": self.spln_sort_bam,
       "spln_bedpe": self.spln_bedpe
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return exit_codes

    def join_indexes(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        print("self.spln_molids_fq: ", self.spln_molids_fq)

        file = pysam.Fastqfile(self.spln_molids_fq)
        ln = len( [ x for x in file ])
        print("ln: ", ln)
        file.close()

        self.spln_hplex_qc['fq_read_cnt'] = ln

        # columns = ['rname', 'idx_seq']
        # idx_df = pd.DataFrame(index=np.arange(0, ln), columns=columns)
        # print("df_: \n", idx_df, idx_df.dtypes)

        # dtypes = {'rname': '|S100', 'idx_seq': '|S20'}

        # for c in idx_df.columns:
        #     idx_df[c] = idx_df[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        # print("idx_df: \n", idx_df, idx_df.dtypes)


        # update with numpy is faster than pandas (not properly indexed)
        rname_arr = np.empty([ln, 2], dtype="|S100")
        file =  pysam.Fastqfile(self.spln_molids_fq)
        i = 0
        for s in file:

            # Setting With Enlargement
            rname_arr[i, 0] = s.name
            rname_arr[i, 1] = s.sequence

            # idx_df.ix[i,0] = s.name
            # idx_df.ix[i,1] = s.sequence
            i += 1
            # print("s: ", s)
            # print("s.sequence: ", s.sequence)
            # print("s.quality: ", s.quality)
            # print("s.name: ", s.name)
            # print("comment: ", s.comment)

            if i % 200000 == 0:
                print("i: ", i)
            #     print("idx_df: \n", idx_df)
            #     time.sleep(10)
        df = pd.DataFrame({'rname': rname_arr[:, 0], 'idx_seq': rname_arr[:, 1]})
        dtypes = {'rname': '|S100', 'idx_seq': '|S20'}
        for c in df.columns:
            df[c] = df[c].astype(dtypes[c])

        print("df before groupby: \n", df.shape, df.dtypes)


        idx_cnt_df = df.groupby(["idx_seq"]).first()
        print("idx_cnt_df-------------------: \n", idx_cnt_df.shape, idx_cnt_df.dtypes)
        self.spln_hplex_qc['idx_cnt'] = idx_cnt_df.shape[0]

        df.set_index(["rname"], inplace=True)
        df = df.groupby(df.index).first()
        print("df after groupby: \n", df.shape, df.dtypes)
        # df.set_index(["rname"], inplace=True)
        # df.re(["idx_seq"], inplace=True)
        # print("df.shape before drop: \n", df.shape)
        # df.drop_duplicates(cols=["idx_seq"], inplace=True)
        # print("df.shape after drop: \n", df.shape)
        df.sort_index(inplace=True)


        # print("df: \n", df, df.dtypes)

        # --------------------

        file = self.spln_bedpe

        bpe = pd.read_table(file,
                                # compression="gzip",
                                header=None,
                                index_col=[6],
                                names=['r1_chr', 'r1_s', 'r1_e',
                                       'r2_chr', 'r2_s', 'r2_e',
                                       'rname', 'score', 'r1_st', 'r2_st'])

        print("bpe: \n", bpe)

        # uniquify index
        bpe = bpe.groupby(bpe.index).first()
        print("bpe after groupby: \n", bpe.shape, bpe.dtypes)

        self.spln_hplex_qc['proper'] = bpe.shape[0]
        bpe.sort_index(inplace=True)

        # read_gtf()

        # samtools view -bf 0x2 1.spln.bam | bedtools bamtobed -i stdin > 2.bedpe


        merged_df = pd.merge(df, bpe, how='inner', left_index=True, right_index=True)

        # move index to datacolumn
        merged_df = merged_df.reset_index('rname')

        print("merged_df: ", merged_df)
        # self.readcnt_known_genes_matrix_df.rename(columns={'readcnt_avg': self.smp_name}, inplace=True)

        self.store.put('all/compr', merged_df, format='table', complib='blosc', chunksize=2000000,
                  data_columns=['idx_seq', 'rname', 'r1_chr', 'r1_s', 'r1_e',
                                'r2_chr', 'r2_s', 'r2_e',
                                'score', 'r1_st', 'r2_st'])

        return [0]


    @staticmethod
    def raw_stats(df):

        # print("df: ", df.shape, df.dtypes)

        # return pd.Series({'pvalue': 1112, 'mean_ratio': 'aa'})
        return pd.Series([df['rname'].iloc[0], df.shape[0], df['score'].max(), df['score'].min()], index=['rname', 'cnts', 'score_max', 'score_med'])
        # print("res: ", res)
        # return res

    def select_best(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        merged_df = self.store.select('all/compr')
        print("merged_df: \n", merged_df)

        # first row with maximum score value
        # gb = merged_df.sort('score', ascending=False).groupby(['idx_seq', 'r1_chr', 'r1_s', 'r2_e'], as_index=False).first()
        g = merged_df.sort('score', ascending=False).groupby(['idx_seq', 'r1_chr', 'r1_s', 'r2_e'], as_index=False)
        print("g: ", g)
        gb = g.agg({'rname': lambda x: x.iloc[0], 'score': 'count'})
        gb.rename(columns={'score': 'cnt'}, inplace=True)

        gb_min = g.agg({'rname': lambda x: x.iloc[0], 'score': 'min'})
        gb['score_min'] = gb_min.score

        # print ("gb_cnt: ", gb_cnt)
        # print ("gb_min: ", gb_min)

        # gb_cnt['bar'] = df.bar.map(str) + " is " + df.foo
        # gb = g.apply(self.raw_stats)


        print("gb: ", gb)



        self.store.put('all/dedup', gb, format='table', complib='blosc', chunksize=2000000,
                  data_columns=True)

        return [0]

    def print_best(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        sel2 = self.store.select('all/dedup', columns=['rname', 'idx_seq', 'cnt', 'score_min'])
        print("sel.shape: \n", sel2.shape)
        # drop True removes the index from the data columns
        # drop False keeps it and prevents the drop_duplicates from droping columns
        sel2.set_index(['rname'], drop=True, inplace=True)

        self.spln_hplex_qc['min'] = "%.2f" % sel2['cnt'].min()
        self.spln_hplex_qc['max'] = "%.2f" % sel2['cnt'].max()
        self.spln_hplex_qc['median'] = "%.2f" % sel2['cnt'].quantile(q=0.50)
        self.spln_hplex_qc['q25'] = "%.2f" % sel2['cnt'].quantile(q=0.25)
        self.spln_hplex_qc['q75'] = "%.2f" % sel2['cnt'].quantile(q=0.75)
        self.spln_hplex_qc['mad'] = "%.2f" % sel2['cnt'].mad()
        self.spln_hplex_qc['mean'] = "%.2f" % sel2['cnt'].mean()
        self.spln_hplex_qc['std'] = "%.2f" % sel2['cnt'].std()

        # print("quartiles: ", a, b, c, d, e, f, g, h)






        print("sel2.shape before uniquify index: \n", sel2.shape)
        # uniquify index
        sel2 = sel2.groupby(sel2.index).first()
        print("sel2 after groupby: \n", sel2.shape, sel2.dtypes)

        print("sel2.shape before drop: \n", sel2.shape)
        # sel2.drop_duplicates(inplace=True)
        print("sel2.shape after drop: \n", sel2.shape)
        sel2.sort_index(inplace=True)

        print("sel2: \n", sel2, sel2.dtypes)

        self.spln_hplex_qc['dedup_cnt'] = sel2.shape[0]


        df = pd.DataFrame(columns=['proper', 'fq_read_cnt', 'idx_cnt', 'dedup_cnt',
                                   'min', 'max', 'median', 'mad', 'q25', 'q75', 'mean', 'std'], index=[1])
        df.loc[1] = pd.Series(self.spln_hplex_qc)

        df.to_csv(self.one_hplex_qc_stats_csv,
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )

        print("df: \n", df)


        infile = pysam.AlignmentFile(self.spln_sort_bam, "rb")
        out_acc = pysam.AlignmentFile(self.spln_acc_bam, "wb", template=infile)
        # out_rej = pysam.AlignmentFile("rej.bam", "wb", template=infile)

        i =0
        for s in infile:

            if i % 100000 == 0:
                print("i: ", i)



            if s.query_name in sel2.index:
            # if s.query_name in rnames2:
            #     xmtag = sel2.at[s.query_name, 'idx_seq']

                # print("xmtag: ", xmtag)

                # s.tags["MQ"] = 0
                s.setTag("XM", sel2.at[s.query_name, 'idx_seq'])
                s.setTag("XC", sel2.at[s.query_name, 'cnt'])
                s.setTag("XS", sel2.at[s.query_name, 'score_min'])

                # print("s.tags: ", s.tags)

                out_acc.write(s)


            # if s.query_name in rnames2:
            #     out_acc.write(s)



            i+=1

        out_acc.close()
        # out_rej.close()

        # print("self.spln_hplex_qc: ", self.spln_hplex_qc)

        return [0]


    def sort_lane_hplexhs(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        exit_codes = []

        # delete output files
        for i in [self.spln_proper_bam, self.spln_sort_bam, self.spln_bedpe,
                  self.spln_acc_bam, self.spln_h5store, self.one_hplex_qc_stats_csv]:
            if os.path.exists(i):
                os.remove(i)

        print("self.spln_runreg: ", self.spln_runreg)

        # scratch_d = os.getenv('LSCRATCH', "")


        pd.set_option('io.hdf.default_format', 'table')
        self.store = pd.HDFStore(self.spln_h5store)

        exit_codes.append(self.prepare_sort())
        exit_codes.append(self.join_indexes())
        exit_codes.append(self.select_best())
        exit_codes.append(self.print_best())

        print(self.store)
        self.store.close()

        return [exit_codes]



    def stats_spln_insert(self):
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        # delete output files
        for i in [self.one_spln_insert_stats_csv, self.one_spln_insert_stats_pdf]:
            if os.path.exists(i):
                os.remove(i)

        print("self.spln_runreg: ", self.spln_runreg)

        # scratch_d = os.getenv('LSCRATCH', "")


        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CollectInsertSizeMetrics \
ASSUME_SORTED=true \
I=%(fin)s \
O=%(fout_csv)s \
H=%(fout_pdf)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_sort_bam,
       "fout_csv": self.one_spln_insert_stats_csv,
       "fout_pdf": self.one_spln_insert_stats_pdf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0
        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)
        # mark as done
        self.mark_done_args(args, success)

    def merge_lanes2(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"smp_idx": self.smp_idx}
        print("parameters: \n", args)

        func_impl = thiscallersname() + "_" + self.conf.rmdup_type

        exit_codes = self.__getattribute__(func_impl)()

        print("exit_codes: ", exit_codes)

        exit_codes = [item for sublist in exit_codes for item in sublist]
        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0
        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)
        # mark as done
        self.mark_done_args(args, success)

    def merge_lanes_picard2(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        lanes_df = self.conf.sample_lanes
        print("lanes_df: ")
        print(lanes_df)
        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print("-------------")
        print(lanes_df)

        exit_codes = []

        lanes_st_acc = ""
        accepted_err_arr = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.mer_smp_bam, self.mer_smp_dupl_stats]:
            if os.path.exists(i):
                os.remove(i)


        for f in glob.glob(self.mer_smp_glob):
            os.remove(f)


        for idx, row in lanes_df.iterrows():
            self.spln_idx=idx
            # print row
            print("self.spln_idx: ", self.spln_idx, "self.spln_row: ", self.spln_row)

            print("self.spln_bam: ", self.spln_bam)

            # check alignment files
            if os.path.exists(self.spln_bam):
                lanes_st_acc += r"""%s """ % self.spln_bam
            else:
                accepted_err_arr.append(self.spln_align_lib_dir)

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(self.align_smp_dir, "accepted_err_ml.csv")
            accepted_err_df.to_csv(accepted_err_f, sep=',', header=False, index=False)

            print("accepted_err_arr: ", accepted_err_arr)
            print("accepted_err_f: ", accepted_err_f)


        a1 = os.path.join(self.align_smp_dir, "%s.mer.a1" % self.smp_name)
        a2 = os.path.join(self.align_smp_dir, "%s.mer.a2" % self.smp_name)
        a3 = os.path.join(self.align_smp_dir, "%s.mer.a3" % self.smp_name)
        a4 = os.path.join(self.align_smp_dir, "%s.mer.a4" % self.smp_name)
        a5 = os.path.join(self.align_smp_dir, "%s.mer.a5" % self.smp_name)

        # clean input links
        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)



        # acc_merg1_f = os.path.join(self.align_smp_dir, "acc_merg1.bam")

        f1 = os.path.join(self.align_smp_dir, "%s.mer.f1.bam" % self.smp_name)
        f1_bai = os.path.join(self.align_smp_dir, "%s.mer.f1.bam.bai" % self.smp_name)

        f2 = os.path.join(self.align_smp_dir, "%s.mer.f2.bam" % self.smp_name)
        f2_bai = os.path.join(self.align_smp_dir, "%s.mer.f2.bai" % self.smp_name)

        # f3 = os.path.join(self.align_smp_dir, "f3.bam")
        # f4 = os.path.join(self.align_smp_dir, "f4.bam")

        # f4_txt = os.path.join(self.align_smp_dir, "f4.txt")


        # clean input files
        for i in [f1, f1_bai, f2, f2_bai]:
            if os.path.exists(i):
                os.remove(i)


        if not os.path.exists(a1):
            os.mkfifo(a1)
        if not os.path.exists(a2):
            os.mkfifo(a2)
        if not os.path.exists(a3):
            os.mkfifo(a3)
        if not os.path.exists(a4):
            os.mkfifo(a4)
        if not os.path.exists(a5):
            os.mkfifo(a5)


        # redirection is important
        # cmd = r"""samtools merge -c -p - %(lanes_st_acc)s > %(fout)s""" % {"lanes_st_acc": lanes_st_acc, "fout": a1}
        lanes_arr = lanes_st_acc.split()

        print("lanes_st_acc: ", lanes_st_acc)
        print("lanes_arr: ", lanes_arr)
        lanes_len = lanes_arr.__len__()

        if lanes_len == 1:
            os.symlink(lanes_arr[0], f1)
            exit_codes.append(0)
        else:
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st_acc, "fout": f1}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

        # coordinate sort
        # cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (scratch_d, a2, a1)
        # print(cmd)
        # p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # add read groups
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": f1, "fout": f2, "smp": self.smp_name}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)
        exit_codes.append(rc3)

        # temporary file
        for i in [f1, f1_bai]:
            if os.path.exists(i):
                os.remove(i)


        # remove duplicates


        # "Estimates the size of a library based on the number of paired end molecules
# observed and the number of unique pairs observed. Based on the
# Lander-Waterman equation that states:
#
# C/X = 1 - exp( -N/X )
#
# where X = number of distinct molecules in library N = number of read pairs C
# = number of distinct fragments observed in read pairs"


        if self.conf.merge_lanes_gatk_markdup:
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
    -XX:ParallelGCThreads=8 \
    -Xms8G -Xmx12G \
    -Dsamjdk.use_async_io=true \
    -jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=%(merge_lanes_gatk_rmdup)s \
    TMP_DIR=%(scratch_d)s \
    VALIDATION_STRINGENCY=SILENT \
    ASSUME_SORTED=true \
    CREATE_INDEX=true \
    INPUT=%(fin)s \
    OUTPUT=%(fout)s \
    METRICS_FILE=%(dup_metrics)s \
    MAX_RECORDS_IN_RAM=5000000
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "merge_lanes_gatk_rmdup": self.conf.merge_lanes_gatk_rmdup,
           "fin": f2,
           "fout": self.mer_smp_bam,
           "dup_metrics": self.mer_smp_dupl_stats}
            print(cmd)
            p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p4.wait()
            rc4 = p4.returncode
            print("rc4: ", rc4)
            exit_codes.append(rc4)
            # temporary file
            for i in [f2, f2_bai]:
                if os.path.exists(i):
                    os.remove(i)

        else:
            # just link fin to fout
            os.symlink(f2, self.mer_smp_bam)
            os.symlink(f2_bai, self.mer_smp_bai)
            exit_codes.append(0)
            # f2 is not temporary anymore, it is the real one



        # exit_codes_stage = [pp.wait() for pp in [p1, p3, p4] ]
        # print("exit_codes stage: ", exit_codes_stage)
        # exit_codes.extend(exit_codes_stage)

        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)

        # for i in [acc_merg1_f, f1,f2,f3]:
        #     if os.path.exists(i):
        #         os.remove(i)

        return [exit_codes]


    def merge_lanes_hplexhs2(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        lanes_df = self.conf.sample_lanes
        print("lanes_df: ")
        print(lanes_df)
        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)
        lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        print("-------------")
        print(lanes_df)

        exit_codes = []

        lanes_st_acc = ""
        accepted_err_arr = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.mer_smp_bam, self.mer_smp_dupl_stats]:
            if os.path.exists(i):
                os.remove(i)


        for f in glob.glob(self.mer_smp_glob):
            os.remove(f)


        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx
            # print row
            print("self.spln_idx: ", self.spln_idx, "self.spln_row: ", self.spln_row)

            print("self.spln_acc_bam: ", self.spln_acc_bam)

            # check alignment files
            if os.path.exists(self.spln_acc_bam):
                lanes_st_acc += r"""%s """ % self.spln_acc_bam
            else:
                accepted_err_arr.append(self.spln_align_lib_dir)

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(self.align_smp_dir, "accepted_err_ml.csv")
            accepted_err_df.to_csv(accepted_err_f, sep=',', header=False, index=False)

            print("accepted_err_arr: ", accepted_err_arr)
            print("accepted_err_f: ", accepted_err_f)


        a1 = os.path.join(self.align_smp_dir, "%s.mer.a1" % self.smp_name)
        a2 = os.path.join(self.align_smp_dir, "%s.mer.a2" % self.smp_name)
        a3 = os.path.join(self.align_smp_dir, "%s.mer.a3" % self.smp_name)
        a4 = os.path.join(self.align_smp_dir, "%s.mer.a4" % self.smp_name)
        a5 = os.path.join(self.align_smp_dir, "%s.mer.a5" % self.smp_name)

        # clean input links
        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)



        # acc_merg1_f = os.path.join(self.align_smp_dir, "acc_merg1.bam")

        f1 = os.path.join(self.align_smp_dir, "%s.mer.f1.bam" % self.smp_name)
        f1_bai = os.path.join(self.align_smp_dir, "%s.mer.f1.bam.bai" % self.smp_name)

        f2 = os.path.join(self.align_smp_dir, "%s.mer.f2.bam" % self.smp_name)
        f2_bai = os.path.join(self.align_smp_dir, "%s.mer.f2.bai" % self.smp_name)

        # f3 = os.path.join(self.align_smp_dir, "f3.bam")
        # f4 = os.path.join(self.align_smp_dir, "f4.bam")

        # f4_txt = os.path.join(self.align_smp_dir, "f4.txt")


        # clean input files
        for i in [f1, f1_bai, f2, f2_bai]:
            if os.path.exists(i):
                os.remove(i)


        if not os.path.exists(a1):
            os.mkfifo(a1)
        if not os.path.exists(a2):
            os.mkfifo(a2)
        if not os.path.exists(a3):
            os.mkfifo(a3)
        if not os.path.exists(a4):
            os.mkfifo(a4)
        if not os.path.exists(a5):
            os.mkfifo(a5)


        # redirection is important
        # cmd = r"""samtools merge -c -p - %(lanes_st_acc)s > %(fout)s""" % {"lanes_st_acc": lanes_st_acc, "fout": a1}
        lanes_arr = lanes_st_acc.split()

        print("lanes_st_acc: ", lanes_st_acc)
        print("lanes_arr: ", lanes_arr)
        lanes_len = lanes_arr.__len__()

        if lanes_len == 1:
            os.symlink(lanes_arr[0], f1)
        else:
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st_acc, "fout": f1}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

        # coordinate sort
        # cmd = "sambamba sort -t 6 --memory-limit=1700M --tmpdir=%s -o %s %s " % (scratch_d, a2, a1)
        # print(cmd)
        # p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        # add read groups
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=library_id RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "fin": f1, "fout": f2, "smp": self.smp_name}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)
        exit_codes.append(rc3)

        # temporary file
        for i in [f1, f1_bai]:
            if os.path.exists(i):
                os.remove(i)


        # remove duplicates


        # "Estimates the size of a library based on the number of paired end molecules
# observed and the number of unique pairs observed. Based on the
# Lander-Waterman equation that states:
#
# C/X = 1 - exp( -N/X )
#
# where X = number of distinct molecules in library N = number of read pairs C
# = number of distinct fragments observed in read pairs"


        if self.conf.merge_lanes_gatk_markdup:
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
    -XX:ParallelGCThreads=8 \
    -Xms8G -Xmx12G \
    -Dsamjdk.use_async_io=true \
    -jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=%(merge_lanes_gatk_rmdup)s \
    TMP_DIR=%(scratch_d)s \
    VALIDATION_STRINGENCY=SILENT \
    ASSUME_SORTED=true \
    CREATE_INDEX=true \
    INPUT=%(fin)s \
    OUTPUT=%(fout)s \
    METRICS_FILE=%(dup_metrics)s \
    MAX_RECORDS_IN_RAM=5000000
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "merge_lanes_gatk_rmdup": self.conf.merge_lanes_gatk_rmdup,
           "fin": f2,
           "fout": self.mer_smp_bam,
           "dup_metrics": self.mer_smp_dupl_stats}
            print(cmd)
            p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p4.wait()
            rc4 = p4.returncode
            print("rc4: ", rc4)
            exit_codes.append(rc4)
            # temporary file
            for i in [f2, f2_bai]:
                if os.path.exists(i):
                    os.remove(i)

        else:
            # just link fin to fout
            os.symlink(f2, self.mer_smp_bam)
            os.symlink(f2_bai, self.mer_smp_bai)
            exit_codes.append(0)
            # f2 is not temporary anymore, it is the real one



        # exit_codes_stage = [pp.wait() for pp in [p1, p3, p4] ]
        # print("exit_codes stage: ", exit_codes_stage)
        # exit_codes.extend(exit_codes_stage)

        for i in [a1, a2, a3, a4, a5]:
            if os.path.exists(i):
                os.unlink(i)

        # for i in [acc_merg1_f, f1,f2,f3]:
        #     if os.path.exists(i):
        #         os.remove(i)

        return [exit_codes]

    def stats_dupl(self):
        print("in stats_dupl() ...")

        columns = ['sample', 'perc_dupl', 'library_size']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'perc_dupl': 'float64', 'library_size': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)


        for smp_idx in range(self.nb_samples):
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smp_idx = smp_idx
            print("self.smp_idx: ", self.smp_idx)
            print("self.smp_name: ", self.smp_name)
            print("self.mer_smp_dupl_stats: ", self.mer_smp_dupl_stats)
            # read sample matrix
            try:
                #positional reindexing
                # cur_df = pd.read_csv(self.mer_smp_dupl_stats, sep='\t', header=0,
                #                      comment='#',
                #                      index_col=[0], usecols=[0, 7, 8]
                                     # ,
                                     # names=["library", "perc_dup", "library_size"]
                                     # )
                cur_df = pd.read_csv(self.mer_smp_dupl_stats, sep='\t', header=0,
                                     comment='#',
                                     index_col=[0],
                                     skip_blank_lines=True,
                                     nrows=1

                                     # ,
                                     # names=["library", "perc_dup", "library_size"]
                                     )

            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            cur_df = cur_df[cur_df.index == "library_id"]
            print("cur_df: ", cur_df)



            perc_dupl = cur_df["PERCENT_DUPLICATION"][0]
            library_size = cur_df["ESTIMATED_LIBRARY_SIZE"][0] / 1000000.0


            # Setting With Enlargement
            df_.loc[len(df_)+1] = [self.smp_name, perc_dupl, library_size]

        df_.sort(['library_size'], ascending=[0], inplace=True)
        df_ = df_.reset_index(drop=True)
        print("df_: \n", df_)

        df_.to_csv(self.mer_dupl_stats_csv,
                           sep=',', header=True
                           , index=True
                           , index_label="idx"
                           )

    def stats_dupl_graph(self):
        df_ = pd.read_csv(self.mer_dupl_stats_csv, sep=',', header=False,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6],
                          names=['idx', 'sample', 'perc_dupl', 'library_size'])

        print("df_: ", df_)

        xvals = [float(i) for i in range(df_.shape[0])]
        print("xvals: \n", xvals)

        xlabsmps = df_['sample'].values

        print("xlabsmps: \n", xlabsmps)

        percdups = [float(i) * 100 for i in df_['perc_dupl'].values]

        print("percdups: \n", percdups)

        libsizes = [float(i) for i in df_['library_size'].values]
        print("libsizes: \n", libsizes)


        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width)
        pd.options.display.mpl_style = 'default'
        with PdfPages("../metrics2/qc-dupl-%s.pdf" % self.conf.proj_abbrev) as pdf:
            plt.figure(figsize=(fig_width, 8))
            # plt.set_dpi(100)
            # plt.plot(xvals, libsizes, 'b-')
            # plt.fill_between(xvals, percdups, libsizes)
            plt.plot(xvals, libsizes, 'b-')
            ax = plt.gca()
            ax.set_ylabel('Library size (M)', color='b')
            for tl in ax.get_yticklabels():
                tl.set_color('b')

            # ax1.set_xlabel('time (s)')
            # Make the y-axis label and tick labels match the line color.
            plt.xticks(np.arange(min(xvals), max(xvals)+1, 1))
            ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
            # plt.yticks(np.arange(min(libsizes), max(libsizes)+1, 20))


            # for tl in ax.get_xticklabels():
            #     tl.set_text()
            #     tl.set_color('g')
                # tl.set_rotation(45)
                # tl.set_fontsize(10)


            ax2 = plt.twinx()
            # s2 = np.sin(2*np.pi*t)
            ax2.plot(xvals, percdups, 'r-')
            # ax2.yticks(np.linspace(-1,1,5,endpoint=True))
            plt.yticks(np.arange(min(percdups), max(percdups)+5, 5))
            ax2.set_ylabel('Percent duplication', color='r')

            # start, end = ax2.get_ylim()
            # ax2.yaxis.set_ticks(np.arange(start, end, 0.712123))
            # ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
            # ax2.set_ylabel('sin', color='r')
            for tl in ax2.get_yticklabels():
                tl.set_color('r')

            # for tl in ax2.get_xticklabels():
            #     tl.set_color('g')

            # plt.fill_between(xvals, percdups, libsizes, alpha=0.5)




            # plt.set_ylabel('between y1 and y2')
            # plt.set_xlabel('x')

            # ax1.set_xlabel('time (s)')
            # Make the y-axis label and tick labels match the line color.
            # plt.set_ylabel('exp', color='b')
            # for tl in ax1.get_yticklabels():
            #     tl.set_color('b')

            # ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 8))
            # legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

            # xlabels = ax.get_xticklabels()
            # for i in range(len(xlabels)):
            #     label = xlabels[i].get_text()
            #     print("i: ", i, " label: ", label)
            #     raw = df_.ix[i, "sample"]
            #     print("row: ", raw)
            #     xlabels[i] = raw
                # print(i, xlabels[i])

            # labels = [item.get_text() for item in ax.get_xticklabels()]
            # labels[1] = 'Testing'
            # ax.set_xticklabels(labels)


            # ax2.set_xticklabels(["" for x in xlabels])
            # print("xlabels: ", xlabels.inspect)
            # ax2.set_xticklabels(xlabsmps, rotation=40, fontsize=12, ha='right')
            # for label in plt.xaxis.get_xticklabels():
            #     label.set_text("ssssssssss")
            #     label.set_rotation(45)
            #     label.set_fontsize(10)

            # for k in ax.get_xmajorticklabels():
            #     if some-condition:
            #         k.set_color(any_colour_you_like)

            plt.title('Duplication rates',  fontsize= 22)
            # plt.subplots_adjust(top=0.90, bottom=0.20, left=0.10, right=0.90, hspace = 0.1, wspace = 0.2)
            plt.tight_layout()


            pdf.savefig()  # saves the current figure into a pdf page

            # plt.title('Page Two')
            # pdf.savefig()  # saves the current figure into a pdf page

            plt.close()

    def stats_smp_targ(self):
        args = {"smp_idx": self.smp_idx}

        print("parameters: \n", args)
        print("smp_name: ", self.smp_name)

        # common name, simplify code
        smp_idx = self.smp_idx
        smp_name = self.smp_name
        smp_bam = self.mer_smp_bam
        smp_dir = self.align_smp_dir
        smp_targ_stats_csv = self.smp_targ_stats_csv

        rows_list = []
        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        # for i in [self.mer_smp_ontarget_bam, smp_targ_stats_csv]:

        for i in [smp_targ_stats_csv]:
            if os.path.exists(i):
                os.remove(i)


        cmd = r"""samtools view -c -f 4 %(smp_bam)s
""" % {"smp_bam": smp_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=smp_dir, stdout=subprocess.PIPE)
        unmapped_str = p1.communicate()[0].decode("utf-8").rstrip()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)


        cmd = r"""samtools view -c -F 4 %(smp_bam)s
""" % {"smp_bam": smp_bam}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=smp_dir, stdout=subprocess.PIPE)
        mapped_str = p2.communicate()[0].decode("utf-8").rstrip()
        rc2 = p2.returncode
        print("rc2: ", rc2)
        exit_codes.append(rc2)



#        cmd = r"""samtools view -bq 1 %(mer_smp_bam)s | wc -l
#""" % {"mer_smp_bam": self.mer_smp_bam}
        cmd = r"""sambamba view -c -F "mapping_quality >= 1" %(smp_bam)s
""" % {"smp_bam": smp_bam}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=smp_dir, stdout=subprocess.PIPE)
        unique_str = p3.communicate()[0].decode("utf-8").rstrip()
        rc3 = p3.returncode
        print("rc3: ", rc3)
        exit_codes.append(rc3)

        # ontarget
        # cmd = r"""bedtools intersect \
# -abam %(fin)s \
# -b %(target_bed)s \
# -wa > %(fout)s
# """ % {"fin": self.mer_smp_bam, "target_bed": self.conf.target_bed, "fout": self.mer_smp_ontarget_bam}
#         print(cmd)
#         p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
#         p4.wait()
#         rc4 = p4.returncode
#         print("rc4: ", rc4)
#         exit_codes.append(rc4)



        cmd = r"""samtools view -c -F 4 -L %(target_bed)s %(smp_bam)s
""" % {"target_bed": self.conf.target_karyo_bed,
       "smp_bam": smp_bam}
        print(cmd)
        p5 = subprocess.Popen(cmd, shell=True, cwd=smp_dir, stdout=subprocess.PIPE)
        on_mapped_str = p5.communicate()[0].decode("utf-8").rstrip()
        rc5 = p5.returncode
        print("rc5: ", rc5)
        exit_codes.append(rc5)

        # cmd = r"""samtools view -bq 1 %(mer_smp_bam)s | wc -l
# """ % {"mer_smp_bam": self.mer_smp_ontarget_bam}
        cmd = r"""sambamba view -c -F "mapping_quality >= 1" -L %(target_bed)s %(smp_bam)s
""" % {"target_bed": self.conf.target_karyo_bed,
       "smp_bam": smp_bam}
        print(cmd)
        p6 = subprocess.Popen(cmd, shell=True, cwd=smp_dir, stdout=subprocess.PIPE)
        on_unique_str = p6.communicate()[0].decode("utf-8").rstrip()
        rc6 = p1.returncode
        print("rc6: ", rc6)
        exit_codes.append(rc6)

        # rc1 = p1.returncode
        # print("rc1: ", rc1)
        # exit_codes.append(rc1)

        columns = ['sample', 'unmapped', 'all_maped', 'all_unique', 'on_mapped', 'on_unique']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_)

        # Setting With Enlargement
        df_.loc[len(df_)+1] = [smp_name, unmapped_str, mapped_str, unique_str, on_mapped_str, on_unique_str]

        # dict1 = dict(sample='', unmapped=unmapped_str, all_maped=0.0, all_unique=0.0, on_mapped=0.0, on_unique=0.0)
        # rows_list.append(dict1)
        # df = pd.DataFrame(rows_list)
        # print("df: \n", df)
        #
        # df_.append(df, ignore_index=True)
        print("df_: \n", df_)

        df_.to_csv(smp_targ_stats_csv,
                           sep=',', header=True
                           ,index=False
                           #, index_label = "idx"
                           )

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)

        # mark as done
        self.mark_done_args(args, success)


        # for row in rows:
        #
        #         dict1 = {}
        #         get the row in dictionary format
                # dict1.update(blah..)
                #
                #
        #



    def stats_targ(self):

        columns = ['sample', 'on_unique', 'on_multiple', 'off_unique', 'off_multiple', 'unmapped']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'on_unique': 'float64', 'on_multiple': 'float64',
               'off_unique': 'float64', 'off_multiple': 'float64',
               'unmapped': 'float64'}
        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)


        for smp_idx in range(self.nb_samples):
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smp_idx = smp_idx
            print("self.smp_idx: ", self.smp_idx)
            print("self.smp_name: ", self.smp_name)
            print("smp_targ: ", self.smp_targ_stats_csv)
            # read sample matrix
            try:
                #positional reindexing
                cur_df = pd.read_csv(self.smp_targ_stats_csv, sep=',', header=0,
                                     index_col=[0], usecols=[0, 1, 2, 3, 4, 5],
                                     names=["sample", "unmapped", "all_mapped",
                                            "all_unique", "on_mapped", "on_unique"],
                                     dtype={'sample': np.object,
                                            'unmapped': np.integer,
                                            'all_mapped': np.integer,
                                            'all_unique': np.integer,
                                            'on_mapped': np.integer,
                                            'on_unique': np.integer}
                                     )

                # dtype={'sample': np.object,
                #                             'unmapped': 'int',
                #                             'all_mapped': np.float64,
                #                             'all_unique': np.float64,
                #                             'on_mapped': np.float64,
                #                             'on_unique': np.float64}

            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next


            # cur_df = cur_df[cur_df['sample'] == self.smp_name]
            cur_df = cur_df[cur_df.index == self.smp_name]
            print("cur_df: ", cur_df)
            unmapped = cur_df["unmapped"][0]
            all_mapped = cur_df["all_mapped"][0]
            all_unique = cur_df["all_unique"][0]
            on_mapped = cur_df["on_mapped"][0]
            on_unique = cur_df["on_unique"][0]

            on_multiple = on_mapped - on_unique
            off_unique = all_unique - on_unique
            off_multiple = all_mapped - (on_unique + on_multiple + off_unique)

            # Setting With Enlargement
            df_.loc[len(df_)+1] = [self.smp_name, on_unique, on_multiple, off_unique, off_multiple, unmapped]

        print("df_: \n", df_)

        df_.sort(['sample'], ascending=[1], inplace=True)
        df_ = df_.reset_index(drop=True)

        df_.to_csv(self.targ_stats_csv,
                           sep=',', header=True
                           , index=True
                           , index_label="idx"
                           )

    def stats_targ_graph(self):

        df_ = pd.read_csv(self.targ_stats_csv, sep=',', header=0,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6],
                          names=['idx', 'sample', 'on_unique', 'on_multiple', 'off_unique', 'off_multiple', 'unmapped'])

        print("df_: ", df_)

        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width)
        pd.options.display.mpl_style = 'default'
        with PdfPages("../metrics2/qc-targ-%s.pdf" % self.conf.proj_abbrev) as pdf:
            plt.figure()
            # plt.set_dpi(100)

            ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 9))
            legend = ax.legend(loc=9, bbox_to_anchor=(-0.3, 0.9))

            xlabels = ax.get_xticklabels()
            for i in range(len(xlabels)):
                label = xlabels[i].get_text()
                print("i: ", i, " label: ", label)
                raw = df_.ix[i, "sample"]
                print("row: ", raw)
                xlabels[i] = raw
                # print(i, xlabels[i])

            # labels = [item.get_text() for item in ax.get_xticklabels()]
            # labels[1] = 'Testing'
            # ax.set_xticklabels(labels)


            # ax.set_xticklabels(["" for x in xlabels])
            # print("xlabels: ", xlabels.inspect)
            ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
            # for label in labels:
            #     label.set_rotation(45)
            #     label.set_fontsize(10)

            # for k in ax.get_xmajorticklabels():
            #     if some-condition:
            #         k.set_color(any_colour_you_like)

            plt.title('Target coverage',  fontsize= 22)
            plt.subplots_adjust(top=0.95, bottom=0.20, left=0.3, right=0.99, hspace = 0.1, wspace = 0.2)
            # plt.tight_layout()


            pdf.savefig()  # saves the current figure into a pdf page

            # plt.title('Page Two')
            # pdf.savefig()  # saves the current figure into a pdf page

            plt.close()

            # We can also set the file's metadata via the PdfPages object:
            d = pdf.infodict()
            d['Title'] = 'Multipage PDF Example'
            d['Author'] = 'Raglab'
            d['Subject'] = ''
            d['Keywords'] = ''
            d['CreationDate'] = datetime.datetime(2009, 11, 13)
            d['ModDate'] = datetime.datetime.today()


    def smpgrp_depth_calc_bplot(self):

        exit_codes = []

        print ("self.conf.qc_grp_depth_file: ", self.conf.qc_grp_depth_file)
        print ("self.conf.qc_grp_depth_flts_dup: ", self.conf.qc_grp_depth_flts_dup)

        for fl in self.conf.qc_grp_depth_file:
            self.qc_grp_depth_file = fl

            if fl == "gatk":
                inbam = self.grp_gtk_am
            elif fl == "abra":
                inbam = self.grp_oth_am
            elif fl == "exp":
                inbam = self.grp_exp_am



            for flt in self.conf.qc_grp_depth_flts_dup:
                self.qc_grp_depth_flt_dup = flt

                # erase output files
                # this ensures secure restart from partial previous output
                for i in [self.smpgrp_depth_csv]:
                    if os.path.exists(i):
                        os.remove(i)


                # if flt == "all":
                cmd = r"""cat %(fin)s %(filter)s | bedtools coverage -sorted -hist \
        -a %(target_bed)s \
        -b stdin  \
        -g %(ref_genome)s |  awk '$1 == "all"' > %(fout)s
        """ % {"fin": inbam,
               "filter": self.sambamba_filter_txt(self.sambamba_filter_dup(flt)),
               "target_bed": self.conf.target_karyo_bed,
               "ref_genome": self.conf.ref_gen_genome_chrominfo,
               "fout": self.smpgrp_depth_csv}
                print(cmd)
                p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
                p1.wait()
                # unmapped_str = p1.communicate()[0].rstrip()
                rc1 = p1.returncode
                print("rc1: ", rc1)
                exit_codes.append(rc1)

        # cmd = r"""bedtools coverage -sorted -hist \
# -a %(target_bed)s \
# -b %(fin)s  \
# -g %(ref_genome)s |  awk '$1 == "all"' > %(fout)s
# """ % {"target_bed": self.conf.target_karyo_bed,
#        "ref_genome": self.conf.ref_gen_genome_chrominfo,
#        "fin": self.grp_gtk_am,
#        "fout": self.smpgrp_depth_csv}



            # this selects only the deduplicate
            # | sambamba view -F "not duplicate" /dev/stdin -f bam |
            # if flt == "ded":
            #     cmd = r"""cat %(fin)s | sambamba view -F "not duplicate" /dev/stdin -f bam | bedtools coverage -sorted -hist \
        # -a %(target_bed)s \
        # -b stdin  \
        # -g %(ref_genome)s |  awk '$1 == "all"' > %(fout)s
        # """ % {"target_bed": self.conf.target_karyo_bed,
        #        "ref_genome": self.conf.ref_gen_genome_chrominfo,
        #        "fin": inbam,
        #        "fout": self.smpgrp_depth_csv}
        #
        #         print(cmd)
        #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        #         unmapped_str = p1.communicate()[0].rstrip()
        #         rc1 = p1.returncode
        #         print("rc1: ", rc1)
        #         exit_codes.append(rc1)

        return exit_codes

    def smpgrp_depth_calc_regular(self):

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smpgrp_depth_csv]:
            if os.path.exists(i):
                os.remove(i)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms10G -Xmx20G \
-jar ${GATK_JAR} -T DepthOfCoverage \
-nt 12 \
-R %(ref_genome)s \
-L %(target_bed)s \
-I %(fin)s \
-o %(fout)s \
-ct 1 -ct 5 -ct 10 -ct 100 -ct 500 \
--omitIntervalStatistics \
--omitDepthOutputAtEachBase \
--filter_reads_with_N_cigar
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "ref_genome": self.conf.ref_gen_fa,
       "target_bed": self.conf.target_karyo_bed,
       "fin": self.grp_gtk_am,
       "fout": self.smpgrp_depth_csv}

        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        unmapped_str = p1.communicate()[0].rstrip()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return exit_codes

    def smpgrp_depth_calc_deep(self):

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smpgrp_depth_csv]:
            if os.path.exists(i):
                os.remove(i)

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms10G -Xmx20G \
-jar ${GATK_JAR} -T DepthOfCoverage \
-nt 12 \
-R %(ref_genome)s \
-L %(target_bed)s \
-I %(fin)s \
-o %(fout)s \
-ct 1 -ct 5 -ct 10 -ct 100 -ct 500 -ct 1000 -ct 2000 \
--omitIntervalStatistics \
--omitDepthOutputAtEachBase \
--filter_reads_with_N_cigar
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "ref_genome": self.conf.ref_gen_fa,
       "target_bed": self.conf.target_karyo_bed,
       "fin": self.grp_gtk_am,
       "fout": self.smpgrp_depth_csv}

        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        unmapped_str = p1.communicate()[0].rstrip()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return exit_codes

    def smpgrp_calc(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)

        print("smpgrp_name: ", self.smpgrp_name)

        exit_codes = []

        self.stats_depth = "bplot"
        res = self.smpgrp_depth_calc_bplot()
        exit_codes.extend(res)
        #
        # self.stats_depth = "regular"
        # res = self.smpgrp_depth_calc_regular()
        # exit_codes.extend(res)

        # self.stats_depth = "deep"
        # res = self.smpgrp_depth_calc_deep()
        # exit_codes.extend(res)

        self.mark_exit_args(exit_codes, args)



    def smpgrp_depth_stats_regular(self):

        columns = ['sample', 'y500', 'y100', 'y10', 'y5', 'y1']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'y500': 'float64', 'y100': 'float64',
                  'y10': 'float64', 'y5': 'float64', 'y1': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)


        for idx in range(self.nb_smpgrps):
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            print("self.smpgrp_idx: ", self.smpgrp_idx)
            print("self.smpgrp_name: ", self.smpgrp_name)
            # print("smp_targ: ", self.smp_targ_stats_csv)
            # read sample matrix
            try:
                #positional reindexing
                cur_df = pd.read_csv(self.smp_depth_stats_summary, sep='\t', header=0,
                                     index_col=[0], usecols=[0, 6, 7, 8, 9, 10],
                                     names=["sample", "x1", "x5", "x10", "x100", "x500"])
            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next


            # cur_df = cur_df[cur_df['sample'] == self.smp_name]
            cur_df = cur_df[cur_df.index == self.smpgrp_name]
            print("cur_df: ", cur_df)

            x1 = cur_df["x1"][0]
            x5 = cur_df["x5"][0]
            x10 = cur_df["x10"][0]
            x100 = cur_df["x100"][0]
            x500 = cur_df["x500"][0]

            # y500 = x500
            # y100 = x100 - x500
            # y10 = x10 - (x100 + x500)
            # y5 = x5 - (x10 + x100 + x500)
            # y1 = x1 - (x5 + x10 + x100 + x500)

            y500 = x500
            y100 = x100 - x500
            y10 = x10 - x100
            y5 = x5 - x10
            y1 = x1 - x5

            # Setting With Enlargement
            df_.loc[len(df_)+1] = [self.smpgrp_name, y500, y100, y10, y5, y1]

        df_.sort(['sample'], ascending=[1], inplace=True)
        df_ = df_.reset_index(drop=True)
        # df_= df_.set_index(["sample"])
        print("df_: \n", df_)


        df_.to_csv(self.smpgrp_depth_stats_csv,
                   sep=',', header=True
                   , index=True
                   , index_label="idx"
                   )

    def smpgrp_depth_stats_deep(self):

        print("in stats_depth_deep ...")

        columns = ['sample', 'y2000', 'y1000', 'y500', 'y100', 'y10', 'y5', 'y1']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'y2000': 'float64', 'y1000': 'float64', 'y500': 'float64', 'y100': 'float64',
                  'y10': 'float64', 'y5': 'float64', 'y1': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)


        for idx in range(self.nb_smpgrps):
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            print("self.smpgrp_idx: ", self.smpgrp_idx)
            print("self.smpgrp_name: ", self.smpgrp_name)
            # print("smp_targ: ", self.mer_smp_targ_stats_csv)
            # read sample matrix
            try:
                #positional reindexing
                cur_df = pd.read_csv(self.smp_depth_stats_summary, sep='\t', header=0,
                                     index_col=[0], usecols=[0, 6, 7, 8, 9, 10, 11, 12],
                                     names=["sample", "x1", "x5", "x10", "x100", "x500", "x1000", "x2000"])
            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next


            # cur_df = cur_df[cur_df['sample'] == self.smp_name]
            cur_df = cur_df[cur_df.index == self.smpgrp_name]
            print("cur_df: ", cur_df)

            x1 = cur_df["x1"][0]
            x5 = cur_df["x5"][0]
            x10 = cur_df["x10"][0]
            x100 = cur_df["x100"][0]
            x500 = cur_df["x500"][0]
            x1000 = cur_df["x1000"][0]
            x2000 = cur_df["x2000"][0]


            # y500 = x500
            # y100 = x100 - x500
            # y10 = x10 - (x100 + x500)
            # y5 = x5 - (x10 + x100 + x500)
            # y1 = x1 - (x5 + x10 + x100 + x500)

            y2000 = x2000
            y1000 = x1000 - x2000
            y500 = x500 - x1000
            y100 = x100 - x500
            y10 = x10 - x100
            y5 = x5 - x10
            y1 = x1 - x5

            # Setting With Enlargement
            df_.loc[len(df_)+1] = [self.smpgrp_name, y2000, y1000, y500, y100, y10, y5, y1]

        df_.sort(['sample'], ascending=[1], inplace=True)
        df_ = df_.reset_index(drop=True)
        # df_= df_.set_index(["sample"])
        print("df_: \n", df_)


        df_.to_csv(self.smpgrp_depth_stats_csv,
                   sep=',', header=True
                   , index=True
                   , index_label="idx"
                   )

    def smpgrp_depth_stats_bplot(self):

        for fl in self.conf.qc_grp_depth_file:
            self.qc_grp_depth_file = fl

            for flt_dup in self.conf.qc_grp_depth_flts_dup:
                self.qc_grp_depth_flt_dup = flt_dup

                # erase output files
                # this ensures secure restart from partial previous output
                for i in [self.smpgrp_depth_stats_csv]:
                    if os.path.exists(i):
                        os.remove(i)

                clsidxes = np.unique(self.conf.sample_grps['cls_idx'].values)
                # print("clsidxes: ", clsidxes)
                for clsidx in clsidxes:
                    self.cls_idx = clsidx

                    for minbase in [0, 1]:
                        self.min_base = minbase

                        columns = ['smpgrp_idx', 'smpgrp_name','name', 'q1', 'q2', 'q3', 'q4', 'q5']
                        df_ = pd.DataFrame(columns=columns)
                        print("df_: \n", df_, df_.dtypes)

                        dtypes = {'smpgrp_idx': 'object', 'smpgrp_name': 'object','name': 'object',
                                  'q1': 'float64', 'q2': 'float64', 'q3': 'float64',
                                  'q4': 'float64', 'q5': 'float64'}

                        for c in df_.columns:
                            df_[c] = df_[c].astype(dtypes[c])
                            # print(mydata['y'].dtype)   #=> int64
                        print("df_: \n", df_, df_.dtypes)

                        smpgrps_df = self.conf.sample_grps[self.conf.sample_grps['cls_idx'] == self.cls_idx]
                        smpgrps_df = smpgrps_df.query('skip != 1')

                        for idx, row in smpgrps_df.iterrows():
                        # for idx in self.conf.sample_grps.query('skip != 1').index.values:
                        # for smp_idx in range(1):
                            # set index in order to make all path work
                            self.smpgrp_idx = idx
                            print("self.smpgrp_idx: ", self.smpgrp_idx)
                            print("self.smpgrp_name: ", self.smpgrp_name)


                            # print("smp_targ: ", self.smp_targ_stats_csv)
                            # read sample matrix
                            try:

                                #positional reindexing
                                cur_df = pd.read_csv(self.smpgrp_depth_csv, sep='\t', header=None,
                                                     # index_col=[0],
                                                     usecols=[1, 2],
                                                     skiprows=minbase,
                                                     names=["depth", "nbases"])
                            except Exception as inst:
                                print(type(inst))  # the exception instance
                                print(inst.args)  # arguments stored in .args
                                print(inst)  # __str__ allows args to be printed directly
                                print("smp: ERRORR----------------------------------")
                                next

                            # cur_df = cur_df[cur_df['sample'] == self.smp_name]
                            # cur_df = cur_df[cur_df.index == self.smp_name]
                            print("cur_df: ", cur_df)

                            # This is a diry hack to cope with bedtools negative values BUG
                            cur_df.loc[cur_df.nbases <0 , "nbases"] = 0
                            # cur_df.loc[cur_df.nbases <0 , "nbases"] = cur_df.nbases

                            data = np.repeat(cur_df["depth"].values, cur_df["nbases"].values)
                            print ("data: \n", data)

                            print(cur_df["depth"].values, cur_df["nbases"].values)

                            qmed = np.percentile(data, 50)
                            q25 = np.percentile(data, 25)
                            q75 = np.percentile(data, 75)

                            whis = 1.5
                            # get high extreme
                            iq = q75 - q25
                            hi_val = q75 + whis * iq
                            wisk_hi = np.compress(data <= hi_val, data)
                            if len(wisk_hi) == 0 or np.max(wisk_hi) < q75:
                                wisk_hi = q75
                            else:
                                wisk_hi = max(wisk_hi)

                            # get low extreme
                            lo_val = q25 - whis * iq
                            wisk_lo = np.compress(data >= lo_val, data)
                            if len(wisk_lo) == 0 or np.min(wisk_lo) > q25:
                                wisk_lo = q25
                            else:
                                wisk_lo = min(wisk_lo)

                            # print(wisk_lo, q25, qmed, q75, wisk_hi)


                            # Setting With Enlargement
                            df_.loc[len(df_)+1] = [self.smpgrp_idx, self.smpgrp_name, row['short_name'], wisk_lo, q25, qmed, q75, wisk_hi]

                        df_.set_index(['smpgrp_idx'], inplace=True)
                        df_.sort_index(inplace=True)

                        # df_.sort(['sample'], ascending=[1], inplace=True)
                        # df_ = df_.reset_index(drop=True)
                        # df_= df_.set_index(["sample"])
                        print("df_: \n", df_)


                        df_.to_csv(self.smpgrp_depth_stats_csv,
                                   sep=',', header=True
                                   , index=True
                                   , index_label="smpgrp_idx"
                                   )

        return 0

    def smpgrp_depth_stats(self):

        self.stats_depth = "bplot"
        res = self.smpgrp_depth_stats_bplot()

        # self.conf.stats_depth = "regular"
        # res = self.stats_depth_regular()

        # self.conf.stats_depth = "deep"
        # res = self.stats_depth_deep()


    def gen_stats_depth_graph_files(self):

        stats_df = pd.read_csv(self.smpgrp_depth_stats_csv, sep=',', header=False,
                               index_col=[1],
                               # usecols=[1, 2, 3, 4, 5, 6, 7],
                               # names=["smp_idx", "smp_name", "grp",
                               #        "sbgrp", "order_id", "graph_name",
                               #        "other_name"]
                               )
        print("stats_df: \n", stats_df)

        # create group csvs
        conf_df = pd.read_csv(self.smp_depth_conf_csv, sep=',', header=False,
                                     index_col=[1],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                              )
        merged_df = pd.merge(conf_df, stats_df, how='inner', left_index=True, right_index=True)
        merged_df.drop('other_name', axis=1, inplace=True)
        merged_df.drop('smp_idx_x', axis=1, inplace=True)
        merged_df.drop('smp_idx_y', axis=1, inplace=True)


        print("merged_df: \n", merged_df)

        groups_idx = np.unique(merged_df["grp"].values)
        for grp_id in groups_idx:
            self.graph_grp_idx = grp_id

            print("grp_id: ", grp_id)
            # mer_grp_df = merged_df[merged_df['grp'] == grp]
            mer_grp_df = merged_df.query('grp == @grp_id').copy()
            mer_grp_df.drop('grp', axis=1, inplace=True)
            mer_grp_df.drop('sbgrp', axis=1, inplace=True)
            mer_grp_df.reset_index(level=0, inplace=True)
            # move index to datacolumn
            # merged_df = merged_df.reset_index('rname')

            mer_grp_df.set_index(['order_id'], inplace=True, drop=True)
            mer_grp_df.sort_index(inplace=True)

            # mer_grp_df.sort('order_id', ascending=True, inplace=True)
            print("mer_grp_df: \n", mer_grp_df)
            mer_grp_df.to_csv(self.depth_stats_grp_csv,
                           sep=',', header=True
                           ,index=True
                           # ,index_label="smp_idx"
                           )


    def smpgrp_depth_graph_regular(self):

        df_ = pd.read_csv(self.smpgrp_depth_stats_csv, sep=',', header=0,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6],
                          names=['idx', 'sample', 'y500', 'y100', 'y10', 'y5', 'y1'])

        print("df_: ", df_)

        # plt.figure()
# ax = dfPlot.plot()
# legend = ax.legend(loc='center left', bbox_to_anchor=(1,0.5))
# labels = ax.get_xticklabels()
# for label in labels:
#     label.set_rotation(45)
#     label.set_fontsize(10)

        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width )
        pd.options.display.mpl_style = 'default'
        with PdfPages(self.depth_stats_pdf) as pdf:
            plt.figure()
            # plt.set_dpi(100)

            ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 8))
            legend = ax.legend(loc=9, bbox_to_anchor=(-0.15, 0.9))

            xlabels = ax.get_xticklabels()
            for i in range(len(xlabels)):
                label = xlabels[i].get_text()
                print("i: ", i, " label: ", label)
                raw = df_.ix[i, "sample"]
                print("row: ", raw)
                xlabels[i] = raw
                # print(i, xlabels[i])

            # labels = [item.get_text() for item in ax.get_xticklabels()]
            # labels[1] = 'Testing'
            # ax.set_xticklabels(labels)


            # ax.set_xticklabels(["" for x in xlabels])
            # print("xlabels: ", xlabels.inspect)
            ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
            # for label in labels:
            #     label.set_rotation(45)
            #     label.set_fontsize(10)

            # for k in ax.get_xmajorticklabels():
            #     if some-condition:
            #         k.set_color(any_colour_you_like)

            plt.title('Depth of coverage',  fontsize= 22)
            plt.subplots_adjust(top=0.95, bottom=0.20, left=0.2, right=0.99, hspace = 0.1, wspace = 0.2)
            # plt.tight_layout()


            pdf.savefig()  # saves the current figure into a pdf page

            # plt.title('Page Two')
            # pdf.savefig()  # saves the current figure into a pdf page

            plt.close()

            # We can also set the file's metadata via the PdfPages object:
            d = pdf.infodict()
            d['Title'] = 'Multipage PDF Example'
            d['Author'] = 'Jouni K. Sepp\xe4nen'
            d['Subject'] = 'How to create a multipage pdf file and set its metadata'
            d['Keywords'] = 'PdfPages multipage keywords author title subject'
            d['CreationDate'] = datetime.datetime(2009, 11, 13)
            d['ModDate'] = datetime.datetime.today()

            # plt.figure(figsize=(3, 3))
            # plt.title('Page One')
            # ax = df_.plot(kind='bar', stacked=True)
            # ax.title('Bla')
            # fig = ax.get_figure()
            # pdf.savefig(fig)
            #
            # plt.close()

            # As many times as you like, create a figure fig and save it:

            # When no figure is specified the current figure is saved
            # pdf.savefig()



        # fig = ax.get_figure()
        # fig.savefig('asdf.pdf')
        # pp.savefig(fig)

        # pp.savefig(plot2)
        # pp.savefig(plot3)
        # pp.close()

    def smpgrp_depth_graph_deep(self):

        df_ = pd.read_csv(self.smpgrp_depth_stats_csv, sep=',', header=0,
                          index_col=[0], usecols=[0, 1, 2, 3, 4, 5, 6, 7, 8],
                          names=['idx', 'sample', 'y2000', 'y1000', 'y500', 'y100', 'y10', 'y5', 'y1'])

        print("df_: ", df_)

        # plt.figure()
# ax = dfPlot.plot()
# legend = ax.legend(loc='center left', bbox_to_anchor=(1,0.5))
# labels = ax.get_xticklabels()
# for label in labels:
#     label.set_rotation(45)
#     label.set_fontsize(10)

        fig_width = 8 + round(self.nb_samples / 3, 2)
        print("fig_width: ", fig_width )
        pd.options.display.mpl_style = 'default'
        with PdfPages(self.depth_stats_pdf) as pdf:
            plt.figure()
            # plt.set_dpi(100)

            ax = df_.plot(kind='bar', stacked=True, fontsize=14, width=0.8, figsize=(fig_width, 8))
            legend = ax.legend(loc=9, bbox_to_anchor=(-0.15, 0.9))

            xlabels = ax.get_xticklabels()
            for i in range(len(xlabels)):
                label = xlabels[i].get_text()
                print("i: ", i, " label: ", label)
                raw = df_.ix[i, "sample"]
                print("row: ", raw)
                xlabels[i] = raw
                # print(i, xlabels[i])

            # labels = [item.get_text() for item in ax.get_xticklabels()]
            # labels[1] = 'Testing'
            # ax.set_xticklabels(labels)


            # ax.set_xticklabels(["" for x in xlabels])
            # print("xlabels: ", xlabels.inspect)
            ax.set_xticklabels(xlabels, rotation=40, fontsize=12, ha='right')
            # for label in labels:
            #     label.set_rotation(45)
            #     label.set_fontsize(10)

            # for k in ax.get_xmajorticklabels():
            #     if some-condition:
            #         k.set_color(any_colour_you_like)

            plt.title('Depth of coverage',  fontsize= 22)
            plt.subplots_adjust(top=0.95, bottom=0.20, left=0.2, right=0.99, hspace = 0.1, wspace = 0.2)
            # plt.tight_layout()


            pdf.savefig()  # saves the current figure into a pdf page

            # plt.title('Page Two')
            # pdf.savefig()  # saves the current figure into a pdf page

            plt.close()

            # We can also set the file's metadata via the PdfPages object:
            d = pdf.infodict()
            d['Title'] = 'Multipage PDF Example'
            d['Author'] = 'Jouni K. Sepp\xe4nen'
            d['Subject'] = 'How to create a multipage pdf file and set its metadata'
            d['Keywords'] = 'PdfPages multipage keywords author title subject'
            d['CreationDate'] = datetime.datetime(2009, 11, 13)
            d['ModDate'] = datetime.datetime.today()

    def smpgrp_depth_graph(self):

        # self.conf.stats_depth_calc = "bplot"
        # res = self.stats_depth_graph_bplot()

        self.stats_depth = "regular"
        res = self.smpgrp_depth_graph_regular()

        self.stats_depth = "deep"
        res = self.smpgrp_depth_graph_deep()

    def stats_smp(self):

        # .csv then .pdf

        # duplication
        # if self.conf.merge_lanes_gatk_markdup:
        #     self.stats_dupl()
        #     self.stats_dupl_graph()

        # target
        self.stats_targ()
        self.stats_targ_graph()

        return [0]

    def stats_smpgrp(self):

        # .csv then .pdf

        # depth
        self.smpgrp_depth_stats()
        # self.smpgrp_depth_graph()

        return [0]


    # collect all stats and graphs after merge smpgrps
    def stats_qc(self):

        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        reports = {
            # 0: self.stats_lane,
            # 1: self.stats_smp,
            2: self.stats_smpgrp}

        exit_codes = reports[self.rprt_idx]()
        # print("exit_codes: ", exit_codes)

        self.mark_exit_args(exit_codes, args)

    def indel_realigner(self):
        args = OrderedDict([("smp_idx", self.smp_idx), ("contig_idx", self.contig_idx)])

        print("parameters: \n", args)

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.ral_smp_contig_intervals):
            os.remove(self.ral_smp_contig_intervals)

        if os.path.exists(self.ral_smp_contig_bam):
            os.remove(self.ral_smp_contig_bam)

# , ral_smp_contig_bam, ral_smp_aln_bam, ral_smp_contig_intervals,

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-XX:PermSize=256M -XX:MaxPermSize=512M -Xms1000M -Xmx1700M \
-jar ${GATK_JAR} -T RealignerTargetCreator \
%(extra)s \
-R %(ref_genome)s \
-I %(source_bam)s \
-o %(ral_smp_contig_intervals)s \
-L %(contig_bed)s \
--filter_reads_with_N_cigar
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "extra": self.conf.indel_realigner_realigner_target_creator_extra,
       "ref_genome": self.conf.ref_gen_fa,
       "source_bam": self.smp_clp_non_gtk_am,
       "ral_smp_contig_intervals": self.ral_smp_contig_intervals,
       "contig_bed": self.contig_bed}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.ral_smp_contigs_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        #add unmapped reads at the end
        if self.contig_idx == (self.nb_contigs - 1):
            contig_unmapped_clause = "-L unmapped"
        else:
            contig_unmapped_clause = ""

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-XX:PermSize=256M -XX:MaxPermSize=512M -Xms1000M -Xmx1700M \
-jar ${GATK_JAR} -T IndelRealigner \
%(extra)s \
-R %(ref_genome)s \
-I %(source_bam)s \
-targetIntervals %(ral_smp_contig_intervals)s \
-o %(ral_smp_contig_bam)s \
-L %(contig_bed)s %(contig_unmapped_clause)s \
--filter_reads_with_N_cigar
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "extra": self.conf.indel_realigner_indel_realigner_extra,
       "ref_genome": self.conf.ref_gen_fa,
       "source_bam": self.smp_clp_non_gtk_am,
       "ral_smp_contig_intervals": self.ral_smp_contig_intervals,
       "ral_smp_contig_bam": self.ral_smp_contig_bam,
       "contig_bed": self.contig_bed, "contig_unmapped_clause": contig_unmapped_clause}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.ral_smp_contigs_dir_glo, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print("rc2: ", rc2)
        exit_codes.append(rc2)

        self.mark_exit_args(exit_codes, args)

    def cat_realigned(self):

        # args = {"smp_idx": self.smp_idx}
        # print("parameters: \n", args)

        exit_codes = []

        var_txt = ""
        for contig_idx in range(self.nb_contigs):
            self.contig_idx = contig_idx
            print("contig: %s" % self.contig_name)
            # concatenate only valid files
            if os.path.exists(self.ral_smp_contig_bam) and os.stat(self.ral_smp_contig_bam).st_size != 0:
                smp_txt = r"""%s """ % self.ral_smp_contig_bam
                # print "smp_txt: ", smp_txt
                var_txt += smp_txt

        print("var_txt: ", var_txt)

        # temporary file
        for i in [self.ral_smp_aln_bam]:
            if os.path.exists(i):
                os.remove(i)

        smp_arr = var_txt.split()

        print("var_txt: ", var_txt)
        print("smp_arr: ", smp_arr)
        smp_len = smp_arr.__len__()

        if smp_len == 1:
            os.symlink(smp_arr[0], self.ral_smp_aln_bam)
            exit_codes.append(0)
        else:
            cmd = r"""samtools cat -o %(ral_smp_aln_bam)s %(var_txt)s""" \
                  % {"var_txt": var_txt, "ral_smp_aln_bam": self.ral_smp_aln_bam}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

        return [exit_codes]



    def sort_realigned(self):

        # args = {"smp_idx": self.smp_idx}
        # print("parameters: \n", args)

        exit_codes = []

        # temporary file
        for i in [self.ral_smp_bam]:
            if os.path.exists(i):
                os.remove(i)

        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(ral_smp_aln_bam)s -o %(ral_smp_bam)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "ral_smp_aln_bam": self.ral_smp_aln_bam,
       "ral_smp_bam": self.ral_smp_bam}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_loc, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print("rc2: ", rc2)
        exit_codes.append(rc2)

        # cmd = r"""samtools index %(ral_smp_bam)s
# """ % {"ral_smp_bam": self.ral_smp_bam}
#         print(cmd)
        # p3 = subprocess.Popen(cmd, shell=True, cwd=self.caller_smp_dir, stdout=subprocess.PIPE)
        # p3.wait()
        # rc3 = p3.returncode
        # print("rc3: ", rc3)
        # exit_codes.append(rc3)

        return [exit_codes]


    # java -jar $GATK_JAR UpdateVCFSequenceDictionary -V h4Hg38_known_sites.vcf.gz \
    # --source-dictionary ../sequence/genome_fa/h4Hg38.dict --output h4Hg38_known_sites2.vcf.gz
    # otherwise just
    #  bgzip h4Hg38_known_sites.vcf
    # tabix -p vcf h4Hg38_known_sites.vcf.gz

    def base_recalibrator(self):
        # args = {"smp_idx": self.smp_idx}
        # print("parameters: \n", args)

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_gtk_am, self.smp_gtk_ai,
                  self.rca_smp_pre_grp,
                  self.rca_smp_pre_pdf,
                  self.rca_smp_pst_grp,
                  self.rca_smp_pst_pdf]:
            if os.path.exists(i):
                os.remove(i)

        # even more preventive
        # remove output folder
        if os.path.exists(self.rca_smp_rpt_dir):
            shutil.rmtree(self.rca_smp_rpt_dir, ignore_errors=False, onerror=None)

        #  -T BaseRecalibrator -nct %(cor_tsk)s -knownSites %(known_sites)s \
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} \
BaseRecalibrator \
-R %(ref_genome)s \
-I %(inbam)s \
--known-sites %(known_sites)s \
-O %(rca_smp_pre_grp)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "ref_genome": self.conf.ref_gen_fa,
       "inbam": self.smp_clp_non_gtk_am,
       "known_sites": self.conf.known_sites_cf,
       "rca_smp_pre_grp": self.rca_smp_pre_grp
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.rca_smp_rpt_dir)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # -T PrintReads -nct %(cor_tsk)s --disable_indel_quals
        # PrintReads -BQSR %(rca_smp_pre_grp)s
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} \
ApplyBQSR \
-I %(inbam)s \
--bqsr-recal-file %(rca_smp_pre_grp)s \
-O %(outbam)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "ref_genome": self.conf.ref_gen_fa,
       "inbam": self.smp_clp_non_gtk_am,
       "rca_smp_pre_grp": self.rca_smp_pre_grp,
       "outbam": self.smp_gtk_am
       }
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo)
        p2.wait()
        rc2 = p2.returncode
        print("rc2: ", rc2)
        exit_codes.append(rc2)

        # and index
        cmd = r"""sambamba index -t 12 %(in)s %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.smp_gtk_am,
       "out": self.smp_gtk_ai}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]


    def grind_smps_gtk(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"smp_idx": self.smp_idx}
        print("parameters: \n", args)

        exit_codes = []

        # exit_codes += self.ral_abra()


        # exit_codes += self.cat_realigned()
        # exit_codes += self.sort_realigned()
        exit_codes += self.base_recalibrator()

        print("exit_codes: ", exit_codes)

        self.mark_exit_args(exit_codes, args)

    def grind_smps_oth(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"smp_idx": self.smp_idx}
        print("parameters: \n", args)

        exit_codes = []
        if self.conf.realign_abra:
            exit_codes += self.ral_abra()
        else:
            # link abra
            for i in [self.smp_oth_am,
                      self.smp_oth_ai]:
                if os.path.islink(i):
                    os.unlink(i)
                if os.path.exists(i):
                    os.remove(i)
            # link
            os.symlink(self.smp_clp_non_oth_am, self.smp_oth_am)
            os.symlink(self.smp_clp_non_oth_ai, self.smp_oth_ai)


        self.mark_exit_args(exit_codes, args)

    def sample_qa(self, smp_idx):
        
        self.fix_nm(smp_idx)
        
        #mark as done
        # args = {"smp_idx":smp_idx};args = json.dumps(args)
        # self.lcnf.redis_client.rpush("sample_qa_done",args)

        

    def call_hc(self):

        self.caller_name = "HC"

        # args = {"smpgrp_idx": self.smpgrp_idx, "contig_idx": self.contig_idx}
        args = OrderedDict([("smpgrp_idx", self.smpgrp_idx), ("contig_idx", self.contig_idx)])

        print("parameters: \n", args)

        exit_codes = []

#https://www.broadinstitute.org/gatk/guide/tagged?tag=downsampling
#-dt turns downsampling down
#default -dcov 250
#-nt 1 -nct 8 \


        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.caller_smp_contig_bam):
            os.remove(self.caller_smp_contig_bam)

        if os.path.exists(self.caller_smp_contig_vcf):
            os.remove(self.caller_smp_contig_vcf)
        # --analysis_type HaplotypeCaller --reference_sequence %(ref_genome)s --out %(caller_smp_contig_vcf)s \
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=1 \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} \
HaplotypeCaller \
%(extra)s \
--native-pair-hmm-threads 1 \
-R %(ref_genome)s \
-I %(source_bam)s \
-O %(caller_smp_contig_vcf)s \
-bamout %(caller_smp_contig_bam)s \
-L %(contig_bed)s
 """ % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "extra": self.conf.call_hc_haplotype_caller_extra,
        "ref_genome": self.conf.ref_gen_fa,
        "source_bam": self.grp_gtk_am,
        "caller_smp_contig_vcf": self.caller_smp_contig_vcf,
        "caller_smp_contig_bam": self.caller_smp_contig2_bam,
        "contig_bed": self.contig_bed}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_smp_contigs_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)


        # add smp id with read group for IGV debugging
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=1 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=ragdbg_id RGLB=libdbg_id RGPL=ILLUMINA RGPU=ragdbg_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.caller_smp_contig2_bam,
       "fout": self.caller_smp_contig_bam,
       "smp": self.smpgrp_name}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.caller_smp_contigs_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)
        exit_codes.append(rc3)

        self.mark_exit_args(exit_codes, args)


    def cat_hc(self):

        self.caller_name = "HC"
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)



        var_txt = ""
        for contig_idx in range(self.nb_contigs):
            self.contig_idx = contig_idx
            print("contig: %s" % self.contig_name)
            # concatenate only valid files
            # from GATK4 -V is replaced with -I
            if os.path.exists(self.caller_smp_contig_vcf) and os.stat(self.caller_smp_contig_vcf).st_size != 0:
                smp_txt = r"""-I %(caller_smp_contig_vcf)s \
""" % {"caller_smp_contig_vcf": self.caller_smp_contig_vcf}
                # print "smp_txt: ", smp_txt
                var_txt += smp_txt

        print("var_txt: ", var_txt)
        # this is not usefull for gvcf, only for vcfs --excludeNonVariants
        # -jar ${GATK_JAR} -T CombineVariants  --assumeIdenticalSamples \ -R %(ref_genome)s \ -o
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=%(cor_tsk)s \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} \
MergeVcfs  \
%(var_txt)s -O %(caller_smp_vcf)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "ref_genome": self.conf.ref_gen_fa,
       "var_txt": var_txt,
       "caller_smp_vcf": self.caller_smp_vcf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_smp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)

    def cat_hc_bam(self):

        self.caller_name = "HC"
        exit_codes = []

        args = {"contig_idx": self.contig_idx}
        print("parameters: \n", args)


        var_txt = ""
        # for idx in range(self.nb_smpgrps):

        # for smpgrp_idx in range(self.nb_smpgrps):
        for idx in self.conf.sample_grps.query('skip != 1').index.values:
            self.smpgrp_idx = idx
            # print("idx: ", idx)

            smp_txt = r"""%s """ % self.caller_smp_contig_bam
            var_txt += smp_txt
            
        #erase output files
        #this ensures secure restart from partial previous output
        if os.path.exists(self.caller_contig_bam):
            os.remove(self.caller_contig_bam)


        var_arr = var_txt.split()

        # print("var_txt: ", var_txt)
        # print("var_arr: ", var_arr)
        var_len = var_arr.__len__()

        # samtools merge needs at least two bams to merge
        if var_len == 1:
            os.symlink(var_arr[0], self.caller_contig_bam)
            success = True

        else:
            # print "var_txt: ", var_txt
            # samtools mpileup -L 1000 -E -q 1 -t DP -t SP -g -u
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" \
                  % {"lanes_st_acc": var_txt,
                     "fout": self.caller_contig_bam}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_contig_bams_dir)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            # cmd = r"""samtools merge \
    # %(caller_contig_bam)s \
    # %(var_txt)s
    # """ % {"caller_contig_bam": self.caller_contig_bam, "var_txt": var_txt}
    #         print(cmd)
    #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_contig_bams_dir, stdout=subprocess.PIPE)
    #         p1.wait()
    #         rc1 = p1.returncode
    #         print("rc1: ", rc1)

        self.mark_exit_args(exit_codes, args)

    def genotype_gvcf_contig(self):

        self.caller_name = "HC"
        exit_codes = []

        args = {"contig_idx": self.contig_idx}
        print("parameters: \n", args)

        var_txt = ""
        # for smp in self.conf.sample_names:
        # for idx in range(self.nb_smpgrps):
        # for smpgrp_idx in range(self.nb_smpgrps):
        for idx in self.conf.sample_grps.query('skip != 1').index.values:

            self.smpgrp_idx = idx
            print("idx: ", self.smpgrp_idx, "smp_name: ", self.smpgrp_name)

            smp_txt = r"""-V %s """ % self.caller_smp_vcf
            # print "smp_txt: ", smp_txt
            var_txt += smp_txt

        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.caller_contig_cf):
            os.remove(self.caller_contig_cf)

        # -dcov 1000 \
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} -T GenotypeGVCFs \
-nt %(cor_tsk)s \
%(extra)s \
-R %(ref_genome)s \
%(var_txt)s \
-L %(contig_bed)s \
-o %(caller_cf)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "extra": self.conf.genotype_gvcf_contig_genotypegvcfs_extra,
       "ref_genome": self.conf.ref_gen_fa,
       "var_txt": var_txt,
       "contig_bed": self.contig_bed,
       "caller_cf": self.caller_contig_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_contig_vcfs_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)


    def cat_hc_contigs(self):

        self.caller_name = "HC"
        exit_codes = []

        args = {"noarg_idx": self.noarg_idx}
        print("parameters: \n", args)
        # rc.set("shutdown",0)
        # rc.set("active_queue","call_hc")
        hc_ctg_arr = []
        hc_err_ctg_arr = []

        var_txt = ""
        for contig_idx in range(self.nb_contigs):
            self.contig_idx = contig_idx
            print("contig: %s" % self.contig_name)
            # concatenate only valid files
            if os.path.exists(self.caller_contig_cf) and os.stat(self.caller_contig_cf).st_size != 0:
                hc_ctg_arr.append(self.caller_contig_cf)

                ctg_txt = r"""-V %s """ % self.caller_contig_cf
                var_txt += ctg_txt
            else:
                hc_err_ctg_arr.append(self.caller_contig_cf)

        print("var_txt: ", var_txt)

        hc_ctg_df = pd.DataFrame.from_items([('contig', hc_ctg_arr)])
        hc_err_df = pd.DataFrame.from_items([('contig', hc_err_ctg_arr)])

        hc_ctg_df.to_csv(self.caller_ctg_f,
                         sep='\t', header=False
                         , index=False
                         #, index_label = "idx"
                         )

        hc_err_df.to_csv(self.caller_err_ctg_f,
                         sep='\t', header = False
                         , index = False
                         #, index_label = "idx"
                         )


        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.caller_cf):
            os.remove(self.caller_cf)

        # cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
# -XX:ParallelGCThreads=4 \
# -XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx4G \
# -cp ${GATK_JAR} org.broadinstitute.gatk.tools.CatVariants \
# -R %(ref_genome)s \
# %(var_txt)s -out %(caller_cf)s
# """ % {"scratch_d": self.conf.scratch_loc_dir, "ref_genome": self.conf.ref_gen_fa,
#        "var_txt": var_txt,
#        "caller_cf": self.caller_cf}

        cmd = r"""bcftools concat --remove-duplicates -O v -a -f %(caller_ctg_f)s > %(caller_cf)s
""" % {"caller_ctg_f": self.caller_ctg_f,
       "caller_cf": self.caller_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)


    def call_mp(self):

        self.caller_name = "MP"
        exit_codes = []

        args = {"region_idx": self.region_idx}
        print("parameters: \n", args)

        # all samples have same folder

        
        var_txt = ""
        # for idx in range(self.nb_smpgrps):
        for idx in self.conf.sample_grps.query('skip != 1').index.values:
            self.smpgrp_idx = idx
            # print("idx": idx)

            # entry point is the realigned, comming from other indel realigner
            smp_txt = r"""%(smp_bam)s """ % {"smp_bam": self.grp_oth_am}
            var_txt += smp_txt
            
        # print("self.conf.region_names-----------------------------------:", self.conf.region_names.iloc[self.region_idx,0])

        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.caller_region_cf):
            os.remove(self.caller_region_cf)

        # print "var_txt: ", var_txt
        # samtools mpileup -L 1000 -E -q 1 -t DP -t SP -g -u
        cmd = r"""samtools mpileup -u -d 1000 \
-f %(ref_genome)s \
-r %(region_filter)s \
-l %(region_bed)s %(var_txt)s
""" % {"ref_genome": self.conf.ref_gen_fa,
       "region_filter": "%s:%s-%s" % (self.region_contig, self.region_start, self.region_stop),
       "region_bed": self.region_bed, "var_txt": var_txt}
        print(cmd)
        #
        p1 = subprocess.Popen(cmd, shell=True
                              ,cwd=self.caller_regions_dir
                              ,stdout=subprocess.PIPE)


        # bcftools view -vcg - > %(region_bcf)s
        cmd = "bcftools call --variants-only -m -O z - > %(region_cf)s" % {"region_cf": self.caller_region_cf}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True
                              ,cwd=self.caller_regions_dir
                              ,stdin= p1.stdout
                              ,stdout=subprocess.PIPE)

        # p3
        exit_codes_stage = [pp.wait() for pp in [p1, p2]]
        exit_codes.extend(exit_codes_stage)

        cmd = r"bcftools index --tbi %(region_cf)s" % {"region_cf": self.caller_region_cf}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.caller_regions_dir)
        p3.wait()
        rc3 = p3.returncode
        exit_codes.append(rc3)
        
        self.mark_exit_args(exit_codes, args)


    def call_ug(self):

        self.caller_name = "UG"
                
        args = {"region_idx": self.region_idx}
        print("parameters: \n", args)

        exit_codes = []


        # samples = self.conf.valid_smps

        # all samples have same folder
        var_txt = ""
        # for smp_idx in range(0,41):
        for idx in self.conf.sample_grps.query('skip != 1').index.values:
            self.smpgrp_idx = idx
            # print("idx": idx)
            #smp_bam = os.path.join(self.conf.align2_dir,smp,"%s.fixed.bam" % smp)
            smp_txt = r"""-I %(smp_bam)s """ % {"smp_bam": self.grp_gtk_am}
            var_txt += smp_txt
            
        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.ug_region_cf):
            os.remove(self.ug_region_cf)

# -nt 1 -nct 8 \            
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=%(cor_tsk)s \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} -T UnifiedGenotyper \
 --logging_level DEBUG \
-filterRNC -filterMBQ -filterNoBases \
%(var_txt)s \
--reference_sequence %(ref_genome)s \
-L %(region_bed)s \
--out %(region_cf)s \
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "var_txt": var_txt,
       "ref_genome": self.conf.ref_gen_fa,
       "region_bed": self.region_bed,
       "region_cf": self.ug_region_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.ug_regions_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)
        

            
            #cmd2 = """java -jar $PICARD_HOME/picard.jar ValidateSamFile VALIDATION_STRINGENCY=STRICT \
#MODE=VERBOSE IGNORE_WARNINGS=false \
#IGNORE=INVALID_TAG_NM \
#REFERENCE_SEQUENCE=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/genomes/species/Homo_sapiens.GRCh37/genome/Homo_sapiens.GRCh37.fa VALIDATE_INDEX=true \
#INPUT=%(smp_bam)s
#""" % {"smp_bam": smp_bam}
#            print cmd2
#            res = runBash(cmd2)
#            print res
#            match = re.search(r'^No\serrors\sfound', res)
#            if match:
#                print "matchObj.group() : ", match.group()
#                smp_txt = r"""-I %(smp_bam)s """ % {"smp_bam": smp_bam}
#                var_txt += smp_txt
                #print "matchObj.group(1) : ", matchObj.group(1)
                #print "matchObj.group(2) : ", matchObj.group(2)
 #           else:
 #               print "No match!!"


        #print "var_txt: ", var_txt
        #samtools mpileup -L 1000 -E -q 1 -t DP -t SP -g -u
#-nt 1 -nct 24 --logging_level DEBUG --log_to_file test.out --min_base_quality_score 30 \
# -stand_call_conf 50 -stand_emit_conf 10 -dt none  \
#-glm BOTH \
#--max_alternate_alleles 6 \
#java -jar $PICARD_HOME/picard.jar ValidateSamFile VALIDATION_STRINGENCY=STRICT MODE=VERBOSE IGNORE_WARNINGS=false REFERENCE_SEQUENCE=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/genomes/species/Homo_sapiens.GRCh37/genome/Homo_sapiens.GRCh37.fa VALIDATE_INDEX=true INPUT=../alignment/AFN-00310-02/AFN-00310-02.sorted.dup.recal.bam
#-nt 1 -nct 24 \

    def cat_mp(self):

        self.caller_name = "MP"
        exit_codes = []
        
        args = {"noarg_idx": self.noarg_idx}
        print("parameters: \n", args)
        #rc.set("shutdown",0)
        #rc.set("active_queue","call_hc")
        mp_var_arr = []
        mp_err_arr = []
        for reg_idx in range(self.nb_regions):
            self.region_idx = reg_idx
            if not os.path.exists(self.caller_region_cf):
                 mp_err_arr.append(self.region_idx)
            else:
                 mp_var_arr.append(self.caller_region_cf)
        #print "var_txt: ", var_txt
        #print "var_arr: ", var_arr
            
            
        mp_reg_df = pd.DataFrame.from_items([('region', mp_var_arr)])
        mp_err_df = pd.DataFrame.from_items([('region', mp_err_arr)])
        
        mp_reg_df.to_csv(self.caller_reg_f,
                         sep='\t', header = False
                         , index = False
                         #, index_label = "idx"
                         )
        mp_err_df.to_csv(self.caller_err_reg_f,
                         sep='\t', header = False
                         , index = False
                         #, index_label = "idx"
                         )
                   
        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.caller_cf):
            os.remove(self.caller_cf)
            
# samtools mpileup -L 1000 -E -q 1 -t DP -t SP -g -u
        cmd = r"""bcftools concat --remove-duplicates -O v -a -f %(caller_reg_f)s > %(caller_cf)s
""" % {"caller_reg_f": self.caller_reg_f,
       "caller_cf": self.caller_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.caller_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)

            

    def cat_ug(self):

        self.caller_name = "UG"
        exit_codes = []
        
        args = {"noarg_idx": self.noarg_idx}
        print("parameters: \n", args)
        #rc.set("shutdown",0)
        #rc.set("active_queue","call_hc")
        ug_var_arr = []
        ug_err_arr = []
        for reg_idx in range(self.nb_regions):
            self.region_idx = reg_idx
            if not os.path.exists(self.ug_region_cf):
                 ug_err_arr.append(self.region_idx)
            else:
                 ug_var_arr.append(self.ug_region_cf)            
        #print "var_txt: ", var_txt
        #print "var_arr: ", var_arr
            
            
        ug_reg_df = pd.DataFrame.from_items([('region', ug_var_arr)])
        ug_err_df = pd.DataFrame.from_items([('region', ug_err_arr)])
        
        ug_reg_df.to_csv(self.ug_reg_f,
                           sep='\t', header = False
                           , index = False
                           #, index_label = "idx"
                           )
        ug_err_df.to_csv(self.ug_err_reg_f,
                   sep='\t', header = False
                   , index = False
                   #, index_label = "idx"
                   )
#samtools mpileup -L 1000 -E -q 1 -t DP -t SP -g -u
                   
        # erase output files
        # this ensures secure restart from partial previous output
        if os.path.exists(self.ug_cf):
            os.remove(self.ug_cf)
            
        cmd = r"""bcftools concat --remove-duplicates -O v -a -f %(ug_reg_f)s > %(ug_cf)s
""" % {"ug_reg_f": self.ug_reg_f, "ug_cf": self.ug_cf }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.caller_ug_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        self.mark_exit_args(exit_codes, args)
    
    
    def comb_vars(self):

        exit_codes = []
        exit_codes += self.rename_variants()
        exit_codes += self.combine_variants()
        self.norm_variants()
        self.snpid()
        self.snpeff()
        # self.gemini_load_snpeff()

        self.dbnsfp()

        return [exit_codes]



    # def genotype_gvcfs2(self):
    #
    #     var_txt = ""
    #     for smp in self.conf.sample_names:
        # for smp_idx in range(self.nb_samples):
        #     self.smp_idx = smp_idx
        #     print("smp_idx: ", self.smp_idx, "smp_name: ", self.smp_name)
        #
        #     smp_txt = r"""-V %s """ % self.caller_smp_vcf
        #     print "smp_txt: ", smp_txt
            # var_txt += smp_txt
        #
        # erase output files
        # this ensures secure restart from partial previous output
        # if os.path.exists(self.caller_cf):
        #     os.remove(self.caller_cf)
        #
        # cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
# -XX:ParallelGCThreads=8 \
# -XX:PermSize=512M -XX:MaxPermSize=1G -Xms8G -Xmx16G \
# -jar ${GATK_JAR} -T GenotypeGVCFs \
# -nt 12 -dcov 1000 \
# --max_alternate_alleles 12 \
# -R %(ref_genome)s \
# %(var_txt)s -o %(caller_cf)s
# """ % {"scratch_d": self.conf.scratch_loc_dir, "ref_genome": self.conf.ref_gen_fa,
#        "var_txt": var_txt, "caller_cf": self.caller_cf}
#         print(cmd)
#         p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.caller_hc_dir, stdout=subprocess.PIPE)
#         p1.wait()
#         rc1 = p1.returncode
#         print("rc1: ", rc1)
#         return rc1



#
#     #def chunker(seq, size):
#     #    return (seq[pos:pos + size] for pos in xrange(0, len(seq), size))
#
#
#
    def make_rename_list(self, caller):
        #make list of rename
        ren_f = open(os.path.join(self.conf.variants2_dir, 'sample-rename.csv'), "w")
        smpgrps_df = self.conf.sample_grps.query('skip != 1')

        for idx, row in smpgrps_df.iterrows():
        # for idx in self.conf.sample_grps.query('skip != 1').index.values:
        # for smp_idx in range(1):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            # print("self.smpgrp_idx: ", self.smpgrp_idx)
            # print("self.smpgrp_name: ", self.smpgrp_name)
            smp = self.smpgrp_name
            print("idx: %s, smp: %s" % (self.smpgrp_idx, smp) )
            ren_f.write("""%(smp)s\t%(smp)s-%(clr)s\n""" % {"smp": smp, "clr": caller})
        ren_f.close()


    def rename_variants(self):

        exit_codes = []

        #first bring samtools bcf to vcf
         #cmd = r"""bcftools view %s/allSamples.merged.bcf > %s/mp-merged.vcf
 #""" % (self.conf.variants_dir,self.conf.variants2_dir)
 #        print cmd, runBash(cmd)

        ren_csv = os.path.join(self.conf.variants2_dir, 'sample-rename.csv')


        for cler in self.conf.use_callers.split('-'):

            self.caller_name = cler

            #make rename list
            self.make_rename_list(self.caller_name)

            # clean output files
            for i in [self.caller_renamed_vcf, self.caller_renamed_vcf_idx]:
                if os.path.exists(i):
                    os.remove(i)

            #and rename
            cmd = r"""vcfrenamesamples -E -f %(ren_csv)s %(caller_vcf)s > %(caller_renamed_vcf)s
    """ % {"ren_csv": ren_csv,
           "caller_vcf" : self.caller_vcf,
           "caller_renamed_vcf": self.caller_renamed_vcf}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print(("rc1: ", rc1))
            exit_codes.append(rc1)

            return [exit_codes]


    def combine_variants(self):

# -V %(v2dir)s/ug-renamed.vcf \

        exit_codes = []

        vtext = ""
        for cler in self.conf.use_callers.split('-'):

            self.caller_name = cler

            vtext += r"""-V %s """ % self.caller_renamed_vcf

            # print("vtext: ", vtext)

            f3 = open(os.path.join(self.conf.variants2_dir, "combine_variants-%s.log" % self.caller_name), "w")

            cmd = r"""module unload %(jdk7_mod)s; \
    module load %(jdk8_mod)s; \
    java -Djava.io.tmpdir=%(scratch_d)s \
    -XX:ParallelGCThreads=4 \
    -Xms1000M -Xmx%(mem_tsk)sM \
    -jar ${GATK_JAR} \
    -nt %(cor_tsk)s \
    --genotypemergeoption REQUIRE_UNIQUE \
    -R %(ref_genome)s \
    -T CombineVariants -dt NONE \
    %(vtext)s
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "cor_tsk": self.cor_tsk,
           "mem_tsk": self.mem_tsk,
           "jdk7_mod": self.conf.jdk7_mod,
           "jdk8_mod": self.conf.jdk8_mod,
           "ref_genome": self.conf.ref_gen_fa,
           "vtext": vtext}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir,
                                  stdout=subprocess.PIPE, stderr = f3)

            cmd = r"""bgzip -c > %(merged_cf)s
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "cor_tsk": self.cor_tsk,
           "mem_tsk": self.mem_tsk,
           "merged_cf": self.merged_cf}

            p2 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir,
                                  stdin=p1.stdout, stdout=subprocess.PIPE, stderr=f3)

            exit_codes_stage = [pp.wait() for pp in [p1, p2]]
            print("exit_codes stage: ", exit_codes_stage)
            exit_codes.extend(exit_codes_stage)

            cmd = r"""tabix -p vcf %(merged_cf)s
    """ % {"merged_cf": self.merged_cf}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir,
                                  stdout=subprocess.PIPE, stderr=f3)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            f3.close()



        return exit_codes


    # def combine_variants_old(self):
    #     cmd = r"""module unload %(jdk7_mod)s; \
# module load %(jdk8_mod)s; \
# java -Djava.io.tmpdir=%(scratch_d)s \
# -XX:ParallelGCThreads=%(cor_tsk)s \
# -Xms1000M -Xmx%(mem_tsk)sM \
# -jar ${GATK_JAR} \
# -nt 12 \
# --genotypemergeoption REQUIRE_UNIQUE \
# -R %(ref_genome)s \
# -T CombineVariants \
# -V %(v2dir)s/hc-renamed.vcf \
# -V %(v2dir)s/mp-renamed.vcf \
# -V %(v2dir)s/ug-renamed.vcf \
# -o %(merged_cf)s
# """ % {"scratch_d": self.conf.scratch_loc_dir, "ref_genome": self.conf.ref_gen_fa,
#        "v2dir": self.conf.variants2_dir,
#        "merged_cf": self.merged_cf}
#         print(cmd)
#         runBash(cmd)


    def norm_variants(self):

        load_one_module('raglab/vt/15.09.08')

        # load_one_module('raglab/gemini/0.17.0')

        # zless
        cmd = r"""zcat %(merged_cf)s \
   | sed 's/ID=AD,Number=./ID=AD,Number=R/' \
   | vt decompose -s - \
   | vt normalize -r %(ref_genome)s - | bgzip -c > %(normed_cf)s
""" % {"merged_cf": self.merged_cf,
        "ref_genome": self.conf.ref_gen_fa,
       "normed_cf": self.normed_cf
        }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        unload_one_module('raglab/vt/15.09.08')

        cmd = r"""tabix -p vcf %(normed_cf)s
""" % {"normed_cf": self.normed_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

    # check if it worked  zcat ../variants2/taa-hc-mp-ug.snpid.vcf.gz | awk '$3~/^(rs.+)/'
    def snpid(self):

        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/SnpSift.jar annotate \
%(dbsnp_cf)s \
%(normed_cf)s | bgzip -c > %(snpid_cf)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "dbsnp_cf": self.conf.dbsnp_cf,
       "normed_cf": self.normed_cf,
       "snpid_cf": self.snpid_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""tabix -p vcf %(snpid_cf)s
""" % {"snpid_cf": self.snpid_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)



    def snpeff(self):


 # java -jar snpEff.jar -v GRCh37.75 /gs/project/wst-164-aa/exome/wg-rl-gm-v.0.6/variants2/hc-mp-ug-merged.vcf > /gs/project/wst-164-aa/exome/wg-rl-gm-v.0.6/variants2/hc-mp-ug-merged.snpid.vcf
 # -c %(snpeff_config)s  \
        cmd = r"""zcat %(snpid_cf)s | java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/snpEff.jar eff \
-dataDir %(snpeff_data)s \
-configOption ucHg19.75.genome=Homo_sapiens \
-o vcf -i vcf \
-csvStats %(snpeff_stats_csv)s -stats %(snpeff_stats_html)s -formatEff -classic \
%(snpeff_genome)s \
  | bgzip -c > %(snpeff_cf)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "snpeff_stats_csv": self.snpeff_stats_csv,
       "snpeff_stats_html": self.snpeff_stats_html,
       "snpeff_cf": self.snpeff_cf,
       "snpid_cf": self.snpid_cf,
       "var2dir": self.conf.variants2_dir,
       "snpeff_data": self.conf.snpeff_data,
       "snpeff_genome": self.conf.snpeff_genome}
       # "snpeff_config": self.conf.snpeff_config,
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""tabix -p vcf %(snpeff_cf)s
""" % {"snpeff_cf": self.snpeff_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)


    def gemini_load_snpeff(self):

        load_one_module('raglab/gemini/0.17.0')

        cmd = r"""module unload raglab/python/3.5.0; \
module load raglab/python/2.7.9; \
gemini load --cores 12 -t snpEff \
--skip-cadd --skip-gerp-bp \
-v %(snpeff_cf)s %(gemini_db)s
""" % {"snpeff_cf": self.snpeff_cf,
       "gemini_db": self.gemini_db,
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        unload_one_module('raglab/gemini/0.17.0')


    def dbnsfp(self):
        
        # cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
# -XX:ParallelGCThreads=8 \
# -XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx16G \
# -jar ${SNPEFF_HOME}/SnpSift.jar dbnsfp \
# -v -db %(dbnsfp_txt)s \
# %(snpeff_cf)s > %(var2dir)s/hc-mp-ug-merged.snpid.snpeff.dbnsfp.vcf
# """ % {"scratch_d": self.conf.scratch_loc_dir, "var2dir": self.conf.variants2_dir,
#

        cmd = r"""zcat %(snpeff_cf)s | java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/SnpSift.jar dbnsfp \
-db %(dbnsfp_db)s /dev/stdin \
| bgzip -c > %(dbnsfp_cf)s
""" % {"scratch_d": self.conf.scratch_loc_dir, "var2dir": self.conf.variants2_dir,
       "dbnsfp_db": self.conf.dbnsfp_db,
       "snpeff_cf": self.snpeff_cf,
       "dbnsfp_cf": self.dbnsfp_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""tabix -p vcf %(dbnsfp_cf)s
""" % {"dbnsfp_cf": self.dbnsfp_cf}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.variants2_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)


        # link
        for i in [self.dbnsfp_cf_depl_file, self.dbnsfp_cf_tbi_depl_file]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)
        os.symlink(self.dbnsfp_cf, self.dbnsfp_cf_depl_file)
        os.symlink(self.dbnsfp_cf_tbi, self.dbnsfp_cf_tbi_depl_file)


    def vcfouts(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        # depending if executing from master or slave node
        self.servers_obj.is_master = True

        # load it inside environment
        self.servers_obj.load_spark_env()



        # exec(open("%s/servers/spark/localhost/conf/spark-env.py" % nsp.conf.proj_dir).read())

        #envir = os.environ
        #print("envir: ", envir)

        #os.environ['SPARK_HOME']="/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-2.0.0-inst"

        # Append pyspark  to Python Path
        # sys.path.append("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-2.0.0-inst/python")

        # try:
        #     from pyspark import SparkContext
        #     from pyspark import SparkConf
        #     print("Successfully imported Spark Modules")
        #
        # except ImportError as e:
        #     print("Can not import Spark Modules", e)
        #     sys.exit(1)
        #
        # Initialize SparkContext
        # sc = SparkContext('local')


        master = "spark://%s:7077" % os.environ["SPARK_MASTER_HOST"]
        print("master: ", master)

        sconf = SparkConf()
        sc_all = sconf.getAll()
        print("sc_all: ", sc_all)

        #os.environ["HIVE_HOME"]="/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/servers/spark/localhost"
        # this is where hive-site.xml is read form classpath
        # print("spark_classpath:", os.environ["SPARK_CLASSPATH"])
        # print("hadoop_classpath:", os.environ["HADOOP_CLASSPATH"])
        print("conf_dir: ", os.environ["SPARK_CONF_DIR"])

        # hadoop conf dir is for finding the hive-site.xml
        print("HADOOP_CONF_DIR---: ", os.environ["HADOOP_CONF_DIR"])

        # os.environ["SPARK_CLASSPATH"]="/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/servers/spark/localhost/conf"
        # os.environ["HADOOP_CLASSPATH"]="/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/servers/spark/localhost/conf"

        #sconf.setMaster(master)\
        #    .setAppName("PythonSSQL")\
        #    .setSparkHome("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/servers/spark/localhost")


        #sctx = SparkContext.getOrCreate(sconf)
        #hivectx = HiveContext(sctx)
        #spark = SQLContext.getOrCreate(hivectx).sparkSession

        #sc_all = sconf.getAll()
        #print("sc_all2: ", sc_all)


        # then we dynamically load all parameters
        spark = SparkSession.builder\
            .master(master)\
            .appName("PythonSQL")\
            .enableHiveSupport()\
            .config("spark.io.compression.codec", "lz4")\
            .config("spark.sql.parquet.compression.codec", "snappy")\
            .config("spark.ui.enabled", "false")\
            .config("spark.executor.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.eventLog.enabled", "true")\
            .config("spark.eventLog.dir", "%s/logs" % self.conf.proj_dir)\
            .config("spark.driver.allowMultipleContexts", "true")\
            .config("spark.executor.instances", "1")\
            .config("spark.scheduler.mode", "FAIR")\
            .config("spark.executor.cores", "6")\
            .config("spark.executor.memory", "2g")\
            .config("spark.driver.memory", "6g")\
            .getOrCreate()


    # .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % nsp.conf.proj_dir)\
            # .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % nsp.conf.proj_dir)\

    #

        # aa = spark.sql("show tables")
        # aa.show()


        # bb= spark.sql("select * from bab where chrom = 'chrM'")
        # bb.show()

        # spark is an existing SparkSession.

        sc = spark.sparkContext



        # http://stackoverflow.com/questions/33681487/how-do-i-add-a-new-column-to-spark-data-frame-pyspark
# If you want to add content of an arbitrary RDD as a column you can
#
#     add row numbers to existing data frame
#     call zipWithIndex on RDD and convert it to data frame
#     join both using index as a join key


        # lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a030-HC.dbnsfp.vcf"))
        # lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a013-HC.dbnsfp.vcf"))
        lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a038-HC.dbnsfp.vcf"))

        bodyf = lines.filter(lambda line: not re.search(r"^#", line))

        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))

        colnames = headerf.collect()[0]

        # header2 = lines.filter(lambda line: re.search(r"^##INFO", line))
        # header3 = header2.collect()
        # for i in range(len(header3)):
        #     print(header3[i])

        names = colnames.split("\t")
        print("names: \n", names)

        # exit(0)

        V = sc.broadcast(names)
        print(V.value)

        parts2 = bodyf.flatMap(partial(self.worker_func, colnames=V))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        outfolder = os.path.join(self.conf.proj_dir,"srvf/filef", "vcf_out.txt")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # mytx.write.csv(outfolder)

        # mytx.coalesce(1).rdd.map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        # parts2.coalesce(1).map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)

        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()

        parts_ds = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                          ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                          info=p[7]))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)


        parts_df = spark.createDataFrame(parts_ds)
        print("size: ", parts_df.count())
        parts_df.show()
        spark.sql("drop table if exists vcf_out")
        parts_df.write.saveAsTable("vcf_out")

        # read the initial dataframe without index
        # dfNoIndex = sc.read.parquet(dataframePath)
        # Need to zip together with a unique integer

        # lambda (x, y): x + y
        # lambda x_y: x_y[0] + x_y[1]
        # First create a new schema with uuid field appended
        newSchema = StructType([StructField("uuid", IntegerType(), False)] + parts_df.schema.fields)
        # zip with the index, map it to a dictionary which includes new field
        df = parts_df.rdd.zipWithIndex()\
            .map(lambda row_id: dict(itertools.chain(row_id[0].asDict().items(), [("uuid", row_id [1])])))\
            .toDF(newSchema)

        spark.sql("drop table if exists vcf_out_ids")
        df.write.saveAsTable("vcf_out_ids")

        exit(0)

        schemaPeople.write.partitionBy("sample", "chrom").saveAsTable("a013_sch_peo5")







        exit(0)

        b = sc.broadcast(colnames)
        print(b.value)


        twof = bodyf.map(lambda x: [x, b.value])

        # print("twof: ", twof.collect())

        # [1, 2, 3, 4, 5]
        # >>> sc.parallelize([0, 0]).flatMap(lambda x: b.value).collect()
        # [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
        # >>> b.unpersist()

        # twof = sc.union([bodyf, headerf])
        # twof = headerf.union(headerf)

        # headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        parts2 = twof.flatMap(self.myFunc)
        # people = parts2.map(lambda p: Row(rowid = p[0], sample=p[1], val=p[2]))

        # schemaPeople = spark.createDataFrame(people)
        # schemaPeople.show()

        # zz = parts2.collect()
        # print("zz: ", zz )
        # exit(0)
        # parts = headerf.map(lambda l: l.split("\t"))


        # parts = lines.map(lambda l: l.split(","))
        zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)
        # exit(0)

        people = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                          ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                          info=p[7], format=p[8], sample=p[9], gts=p[10], anest={"aa" : p[0], "bb" : p[1]}))

        schemaPeople = spark.createDataFrame(people)
        print("size: ", schemaPeople.count())
        schemaPeople.show()

        exit(0)

        schemaPeople.write.partitionBy("sample", "chrom").saveAsTable("a013_sch_peo5")



        for cler in self.conf.use_callers.split('-'):
            print("cler: ", cler)
            self.caller_name = cler
            # extract all samples, filter only this caller
            # smps_in = VariantFile(self.normed_cf)  # auto-detect input format

            dbgfile = "/gs/project/wst-164-ab/share/rnvar/a030-x053-1735-e09/a004-variants3/HC-MP-normed.vcf.gz"
            smps_in = VariantFile(dbgfile)  # auto-detect input format

            all_smps = list((smps_in.header.samples))
            smps_in.close()

            # print(all_smps)
            pat = "^(.+)-%s$" % cler
            smp2 = [[x, re.search(pat, x).group(1)] for x in all_smps if re.search(pat, x)]
            smpsren = [x for x, y in smp2]
            smpsori = [y for x, y in smp2]
            print(smpsren)
            print(smpsori)


            fhof = open(self.gts_caller_matrix_f, 'w')
            fho = csv.writer(fhof, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)

            # row = ['Spam'] * 5 + ['Baked Beans']
            # row = (['Spam', 'Lovely Spam', 'Wonderful Spam'])
            # row.encode('utf-8')

            # bcf_in = VariantFile(self.normed_cf)  # auto-detect input format
            bcf_in = VariantFile(dbgfile)  # auto-detect input format


            bcf_in.subset_samples(smpsren)
            smps = list((bcf_in.header.samples))

            row = ["CHROM", "POS", "REF", "ALT"]
            # header names removes caller scrambling
            row.extend(smpsori)
            fho.writerow(row)

            bcf_out = VariantFile('-', 'w', header=bcf_in.header)

            row_pos = 0
            # for rec in bcf_in.fetch('chr1', 1, 300000):
            for rec in bcf_in.fetch():
                # bcf_out.write(rec)
                # for i in rec:
                #     print(i)
                # rec2 = str(rec.__str__).split(" ")
                rec2 = str(rec)
                rec3 = rec2.rstrip().split("\t")
                #
                chrom, pos, id, ref, alt = rec3[0:5]
                qual, filter, info, format = rec3[5:9]
                del rec3[0:9]

                # pos = rec3.pop(0)
                # id = rec3.pop(0)
                #
                # POS     ID      REF     ALT
                # print(chrom, pos, ref, alt)
                # print(qual, filter, info, format)
                format_arr = format.split(":")
                format_len = format_arr.__len__()
                # print(rec3)
                # assert(len(smps) == len(rec3))
                assert(len(smps) == len(rec3))

                row = []
                row.extend([chrom, pos, ref, alt])

                # extract sample genotypes
                smps_fmt_dict = {}
                for idx in range(len(rec3)):
                    smp = smps[idx]
                    gtstr = rec3.pop(0)
                    gtstr_arr = gtstr.split(":")
                    gtstr_len = gtstr_arr.__len__()
                    assert(format_len == gtstr_len)
                    # print(smp, gtstr)
                    smps_fmt_dict = OrderedDict()
                    # extract format fields
                    fmt_dict = OrderedDict()
                    # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
                    for j in range(format_len):
                        fmtkey = format_arr[j]
                        fmtval = gtstr_arr[j]
                        # print(fmtkey, fmtval)
                        fmt_dict[fmtkey] = fmtval
                    smps_fmt_dict[smp] = fmt_dict
                    # decode the genotype
                    gt = fmt_dict['GT']
                    if gt in ['./.', '0/.', './0', '1/.', './1']:
                        gtval = -1
                    elif gt in ['0/0']:
                        gtval = 0
                    elif gt in ['1/0', '0/1']:
                        gtval = 1
                    elif gt in ['1/1']:
                        gtval = 2
                    row.append(gtval)
                # print(smps_fmt_dict)

                fho.writerow(row)

                # for k in smps_fmt_dict.keys():
                    # print(k, smps_fmt_dict[k]['GT'])



                # print (rec.info)
                # print (rec.info.keys())
                # print (rec.info["DP"])
                # print(rec.format.keys())
                # print(list(rec.samples))
                # print(list(rec.genotype))
                # for i in rec.samples:
                #     print("i: ", i)
                    # print(rec.genotype(i))

                if row_pos % 5000 == 0:
                    print("---- %s ---" % row_pos)
                row_pos += 1
                # print(rec.__str__)
                # print(rec.alleles)

            fhof.close()

            # time.sleep(5)
            # print("flush...", flush=True)
            # bcf_out.close()

            # print("flush...", flush=True)
            # smps = list((bcf_in.header.samples))
            # print("samples: ", smps)

            # row = (['Spam', 'Lovely Spam', 'Wonderful Spam'])
            # row.encode('utf-8')

            # fho.writerow(smps)
            # csvfile.close()



            # "normed_cf": self.normed_cf,

    @staticmethod
    def overlaping_bins(row):


        # bin = row[0]
        assigned_bin = row[0]
        # assigned_bin = bn.assign_bin(seg[0], seg[1])
        covered_interval = bn.covered_interval(assigned_bin)
        overlapping_bins = bn.overlapping_bins(covered_interval[0], covered_interval[1])


        res = []
        rowvals = []

        for overbin in overlapping_bins:
            # rowvals.append(assigned_bin)
            # rowvals.append(overbin)

            # res.append([assigned_bin, overbin])
            res.append(Row(assigned_bin = assigned_bin, overbin=overbin))
        # for i in range(len(names)):
        #     res.append([names[i], vals[i]])

        # words2 = words[9:]
        # len = words2.__len__

        return res
        # return [len(words), 2]
        # return [[0, "smp1", 5], [1, "smp2", 3]]

    @staticmethod
    def parse_gtf(row):

        cols = {}

        columns = row.split("\t")

        attribute = columns[8]
        attributes = attribute.rstrip("\n").split(";")
        attrs = {}
        attrs['len'] = ''

        res = []
        rowvals = []



        # rowvals.append()

        for attr in attributes:
            val = attr.rstrip().lstrip().split(" ")
            if len(val) == 2:
                attrs[val[0]] = json.loads(val[1])


        # res.append(Row(exon_id=attrs['exon_id']))
        res.append(Row(seqname = columns[0],
                       source = columns[1],
                       feature = columns[2],
                       start = int(columns[3]),
                       end = int(columns[4]),
                       score = columns[5],
                       strand = columns[6],
                       frame = columns[7],
                       exon_id=(attrs['exon_id'] if 'exon_id' in attrs else None),
                       exon_number=(attrs['exon_number'] if 'exon_number' in attrs else None),
                       transcript_id=(attrs['transcript_id'] if 'transcript_id' in attrs else None),
                       transcript_name=(attrs['transcript_name'] if 'transcript_name' in attrs else None),
                       transcript_source=(attrs['transcript_source'] if 'transcript_source' in attrs else None),
                       tss_id=(attrs['tss_id'] if 'tss_id' in attrs else None),
                       gene_id=(attrs['gene_id'] if 'gene_id' in attrs else None),
                       gene_name=(attrs['gene_name'] if 'gene_name' in attrs else None),
                       gene_biotype=(attrs['gene_biotype'] if 'gene_biotype' in attrs else None),
                       gene_source=(attrs['gene_source'] if 'gene_source' in attrs else None)
                       ))




        return res



# saveAsTable in spark is not compatible with hive. Workaround from cloudera website:
#
# df.registerTempTable(tempName)
# hsc.sql(s"""
# CREATE TABLE $tableName (
# // field definitions   )
# STORED AS $format """)
# hsc.sql(s"INSERT INTO TABLE $tableName SELECT * FROM $tempName")
#
# http://www.cloudera.com/documentation/enterprise/release-notes/topics/cdh_rn_spark_ki.html


    def spark_parse_gtf(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        # depending if executing from master or slave node
        self.servers_obj.is_master = True

        # load it inside environment
        self.servers_obj.load_spark_env()

        master = "spark://%s:7077" % os.environ["SPARK_MASTER_HOST"]
        print("master: ", master)

        sconf = SparkConf()
        sc_all = sconf.getAll()
        print("sc_all: ", sc_all)
        print("conf_dir: ", os.environ["SPARK_CONF_DIR"])
        # hadoop conf dir is for finding the hive-site.xml
        print("HADOOP_CONF_DIR---: ", os.environ["HADOOP_CONF_DIR"])

        # then we dynamically load all parameters
        spark = SparkSession.builder\
            .master(master)\
            .appName("PythonSQL")\
            .enableHiveSupport()\
            .config("spark.io.compression.codec", "lz4")\
            .config("spark.sql.parquet.compression.codec", "snappy")\
            .config("spark.ui.enabled", "false")\
            .config("spark.executor.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.eventLog.enabled", "true")\
            .config("spark.eventLog.dir", "%s/logs" % self.conf.proj_dir)\
            .config("spark.driver.allowMultipleContexts", "true")\
            .config("spark.executor.instances", "1")\
            .config("spark.scheduler.mode", "FAIR")\
            .config("spark.executor.cores", "8")\
            .config("spark.executor.memory", "8g")\
            .config("spark.driver.memory", "10g")\
            .config("spark.network.timeout", "300000")\
            .getOrCreate()

        sc = spark.sparkContext

        # read file
        # segs = spark.read.load(os.path.join(self.conf.proj_dir,"srvf/filef", "segs.csv"), format="csv",
        #                         header="True", inferSchema="True").rdd.map(json.dumps)

        # segs2 = segs\
        #     .pipe("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/srvf/filef/tokenize.py", os.environ)

        # gtf = sc.textFile(os.path.join(self.conf.proj_dir, "srvf/filef", "small.gtf")).flatMap(self.parse_gtf)
        # gtf = sc.textFile(os.path.join(self.conf.proj_dir, "srvf/filef", "ucHg19sp3p92v75_genes.gtf")).flatMap(self.parse_gtf)

        mytx = spark.sql("select seqname, start, end, coalesce(exon_id, seqname) as exon_id from annot_gtf where feature='exon'")
        # mytx.show()
        # mytx.write.partitionBy("feature", "seqname").saveAsTable("annot2_gtf")
        print("count: ", mytx.show())


        # segs2 = segs.map(json.dumps)\

        # segs2 = segs\
        #     .pipe("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/srvf/filef/tokenize.py", os.environ)\
        #     .map(lambda l: l.split(","))

        # segs2 = spark.sql("select * from bins").rdd.map(list)

        outfolder = os.path.join(self.conf.proj_dir,"srvf/filef", "gtf_out.csv")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # mytx.write.csv(outfolder)

        # mytx.coalesce(1).rdd.map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        mytx.coalesce(1).rdd.map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # segs.show()

        # segs = spark.sql("select bin from bins").rdd
        # segs = spark.sql("select bin from bins").rdd.map(tuple)
        # segs = spark.sql("select bin from bins").rdd.map(list)
        # seg_arr = gtf.collect()[:5]
        # print("\n \n \t zz: \n", seg_arr)

        # annot_gtf = spark.createDataFrame(gtf)
        # spark.sql("drop table annot_gtf")
        # annot_gtf.write.saveAsTable("annot_gtf")
        # print("size: ", annot_gtf.count())
        # annot_gtf.show()

        # zz = parts2.take(10)
        # print("zz: ", zz)

        # (sc.textFile(input)
   # .map(json.dumps).pipe("perl tokenize.pl", os.environ).map(json.loads)
   # .flatMap(lambda tokens: map(lambda x: (x, 1), tokens))
   # .reduceByKey(lambda x,y: x + y)
   # .saveAsTextFile(output))

        spark.stop()



    # https://wiki.ufal.ms.mff.cuni.cz/spark:recipes:using-perl-via-pipes
    def spark_pipes(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        # depending if executing from master or slave node
        self.servers_obj.is_master = True

        # load it inside environment
        self.servers_obj.load_spark_env()

        master = "spark://%s:7077" % os.environ["SPARK_MASTER_HOST"]
        print("master: ", master)

        sconf = SparkConf()
        sc_all = sconf.getAll()
        print("sc_all: ", sc_all)
        print("conf_dir: ", os.environ["SPARK_CONF_DIR"])
        # hadoop conf dir is for finding the hive-site.xml
        print("HADOOP_CONF_DIR---: ", os.environ["HADOOP_CONF_DIR"])

        # then we dynamically load all parameters
        spark = SparkSession.builder\
            .master(master)\
            .appName("PythonSQL")\
            .enableHiveSupport()\
            .config("spark.io.compression.codec", "lz4")\
            .config("spark.sql.parquet.compression.codec", "snappy")\
            .config("spark.ui.enabled", "false")\
            .config("spark.executor.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.eventLog.enabled", "true")\
            .config("spark.eventLog.dir", "%s/logs" % self.conf.proj_dir)\
            .config("spark.driver.allowMultipleContexts", "true")\
            .config("spark.executor.instances", "1")\
            .config("spark.scheduler.mode", "FAIR")\
            .config("spark.executor.cores", "8")\
            .config("spark.executor.memory", "8g")\
            .config("spark.driver.memory", "10g")\
            .config("spark.network.timeout", "300000")\
            .getOrCreate()

        sc = spark.sparkContext

        # read file
        segs = spark.read.load(os.path.join(self.conf.proj_dir,"srvf/filef", "segs.csv"), format="csv",
                                header="True", inferSchema="True").rdd.map(json.dumps)

        segs2 = segs\
            .pipe("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/srvf/filef/tokenize.py", os.environ)

        # segs = sc.textFile(os.path.join(self.conf.proj_dir, "srvf/filef", "segs.csv"))

        # segs2 = segs.map(json.dumps)\

        # segs2 = segs\
        #     .pipe("/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/srvf/filef/tokenize.py", os.environ)\
        #     .map(lambda l: l.split(","))

        # segs2 = spark.sql("select * from bins").rdd.map(list)

        outfolder = os.path.join(self.conf.proj_dir, "srvf/filef", "segs_out.csv")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        segs2.coalesce(1).saveAsTextFile(outfolder)
        # segs.show()

        # segs = spark.sql("select bin from bins").rdd
        # segs = spark.sql("select bin from bins").rdd.map(tuple)
        # segs = spark.sql("select bin from bins").rdd.map(list)
        seg_arr = segs2.collect()[:5]
        print("zz: ", seg_arr)

        # (sc.textFile(input)
   # .map(json.dumps).pipe("perl tokenize.pl", os.environ).map(json.loads)
   # .flatMap(lambda tokens: map(lambda x: (x, 1), tokens))
   # .reduceByKey(lambda x,y: x + y)
   # .saveAsTextFile(output))

        spark.stop()


    def trassembler(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        # depending if executing from master or slave node
        self.servers_obj.is_master = True

        # load it inside environment
        self.servers_obj.load_spark_env()

        master = "spark://%s:7077" % os.environ["SPARK_MASTER_HOST"]
        print("master: ", master)

        sconf = SparkConf()
        sc_all = sconf.getAll()
        print("sc_all: ", sc_all)
        print("conf_dir: ", os.environ["SPARK_CONF_DIR"])
        # hadoop conf dir is for finding the hive-site.xml
        print("HADOOP_CONF_DIR---: ", os.environ["HADOOP_CONF_DIR"])

        # then we dynamically load all parameters
        spark = SparkSession.builder\
            .master(master)\
            .appName("PythonSQL")\
            .enableHiveSupport()\
            .config("spark.io.compression.codec", "lz4")\
            .config("spark.sql.parquet.compression.codec", "snappy")\
            .config("spark.ui.enabled", "false")\
            .config("spark.executor.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self.conf.proj_dir)\
            .config("spark.eventLog.enabled", "true")\
            .config("spark.eventLog.dir", "%s/logs" % self.conf.proj_dir)\
            .config("spark.driver.allowMultipleContexts", "true")\
            .config("spark.executor.instances", "1")\
            .config("spark.scheduler.mode", "FAIR")\
            .config("spark.executor.cores", "8")\
            .config("spark.executor.memory", "8g")\
            .config("spark.driver.memory", "10g")\
            .config("spark.network.timeout", "300000")\
            .getOrCreate()

# .config("hive.exec.dynamic.partition", "true")\
            # .config("hive.exec.dynamic.partition.mode", "nonstrict")\


        # aa = spark.sql("show tables")
        # aa.show()

        # bb= spark.sql("select * from bab where chrom = 'chrM'")
        # bb.show()

        # segs = spark.read.load(os.path.join(self.conf.proj_dir,"srvf/filef", "segments.csv"), format="csv",
        #                        header="True", inferSchema="True")

        # .map(tuple)
        # original is Row objects
        segs = spark.sql("select bin from bins").rdd
        # segs = spark.sql("select bin from bins").rdd.map(tuple)
        # segs = spark.sql("select bin from bins").rdd.map(list)


        seg_arr = segq.collect()[:5]
        print("zz: ", seg_arr)


        # segs.write.partitionBy("chrom", "bin").saveAsTable("segs")

        # parts2 = segs.flatMap(self.overlaping_bins)
        # bin_overlaps = spark.createDataFrame(parts2)
        # bin_overlaps.write.saveAsTable("bin_overlaps")
        # print("size: ", bin_overlaps.count())
        # bin_overlaps.show()

        # zz = parts2.take(10)
        # print("zz: ", zz)

        overl = spark.sql("select se_as.seg_idx as as_seg_idx, "
                          "  se_as.chrom as as_chrom, "
                          "  se_as.start as as_start, "
                          "  se_as.end as as_end, "
                          "  se_ov.seg_idx as ov_seg_idx, "
                          "  se_ov.start as ov_start, "
                          "  se_ov.end as ov_end, "
                          "  eo.assigned_bin as as_bin, "
                          "  eo.overbin as ov_bin "
                          "from exis_uni_overlaps eo "
                          "  join segs se_as on se_as.bin = eo.assigned_bin "
                          "  join segs se_ov on se_ov.bin = eo.overbin "
                          "where se_as.chrom = 'chr1' and "
                          "  se_as.chrom = se_ov.chrom and "
                          "  se_as.end > se_ov.start and "
                          "  se_as.start < se_ov.end ")

 # "  eo.assigned_bin = 1407 and "
 #  "  eo.overbin = 1407 and "

        # overl.write.partitionBy("as_chrom").saveAsTable("overlo2")
        # overl.write.saveAsTable("overl7")
        # overl.cache()
        # overl.write.mode("append").partitionBy("as_bin", "ov_bin").saveAsTable("overl_as_ov_bin2")
        overl.write.mode("append").saveAsTable("overl_as_ov_bin6")
        overl.show()

        overl_all = spark.sql("select * from overl_as_ov_bin6")
        print("count: ", overl_all.count())

        # sc = spark.sparkContext


        exit(0)



        # http://stackoverflow.com/questions/33681487/how-do-i-add-a-new-column-to-spark-data-frame-pyspark
# If you want to add content of an arbitrary RDD as a column you can
#
#     add row numbers to existing data frame
#     call zipWithIndex on RDD and convert it to data frame
#     join both using index as a join key


        # lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a030-HC.dbnsfp.vcf"))
        lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a013-HC.dbnsfp.vcf"))
        bodyf = lines.filter(lambda line: not re.search(r"^#", line))

        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))

        colnames = headerf.collect()[0]

        # names = colnames.split("\t")
        # print("names: \n", names)
        # exit(0)


        b = sc.broadcast(colnames)
        print(b.value)


        twof = bodyf.map(lambda x: [x, b.value])

        # print("twof: ", twof.collect())

        # [1, 2, 3, 4, 5]
        # >>> sc.parallelize([0, 0]).flatMap(lambda x: b.value).collect()
        # [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
        # >>> b.unpersist()

        # twof = sc.union([bodyf, headerf])
        # twof = headerf.union(headerf)

        # headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        parts2 = twof.flatMap(self.myFunc)
        # people = parts2.map(lambda p: Row(rowid = p[0], sample=p[1], val=p[2]))

        # schemaPeople = spark.createDataFrame(people)
        # schemaPeople.show()

        # zz = parts2.collect()
        # print("zz: ", zz )
        # exit(0)
        # parts = headerf.map(lambda l: l.split("\t"))


        # parts = lines.map(lambda l: l.split(","))
        zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)
        # exit(0)

        people = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                          ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                          info=p[7], format=p[8], sample=p[9], gts=p[10], anest={"aa" : p[0], "bb" : p[1]}))

        schemaPeople = spark.createDataFrame(people)
        print("size: ", schemaPeople.count())
        schemaPeople.show()

        exit(0)

        schemaPeople.write.partitionBy("sample", "chrom").saveAsTable("a013_sch_peo5")



        for cler in self.conf.use_callers.split('-'):
            print("cler: ", cler)
            self.caller_name = cler
            # extract all samples, filter only this caller
            # smps_in = VariantFile(self.normed_cf)  # auto-detect input format

            dbgfile = "/gs/project/wst-164-ab/share/rnvar/a030-x053-1735-e09/a004-variants3/HC-MP-normed.vcf.gz"
            smps_in = VariantFile(dbgfile)  # auto-detect input format

            all_smps = list((smps_in.header.samples))
            smps_in.close()

            # print(all_smps)
            pat = "^(.+)-%s$" % cler
            smp2 = [[x, re.search(pat, x).group(1)] for x in all_smps if re.search(pat, x)]
            smpsren = [x for x, y in smp2]
            smpsori = [y for x, y in smp2]
            print(smpsren)
            print(smpsori)


            fhof = open(self.gts_caller_matrix_f, 'w')
            fho = csv.writer(fhof, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)

            # row = ['Spam'] * 5 + ['Baked Beans']
            # row = (['Spam', 'Lovely Spam', 'Wonderful Spam'])
            # row.encode('utf-8')

            # bcf_in = VariantFile(self.normed_cf)  # auto-detect input format
            bcf_in = VariantFile(dbgfile)  # auto-detect input format


            bcf_in.subset_samples(smpsren)
            smps = list((bcf_in.header.samples))

            row = ["CHROM", "POS", "REF", "ALT"]
            # header names removes caller scrambling
            row.extend(smpsori)
            fho.writerow(row)

            bcf_out = VariantFile('-', 'w', header=bcf_in.header)

            row_pos = 0
            # for rec in bcf_in.fetch('chr1', 1, 300000):
            for rec in bcf_in.fetch():
                # bcf_out.write(rec)
                # for i in rec:
                #     print(i)
                # rec2 = str(rec.__str__).split(" ")
                rec2 = str(rec)
                rec3 = rec2.rstrip().split("\t")
                #
                chrom, pos, id, ref, alt = rec3[0:5]
                qual, filter, info, format = rec3[5:9]
                del rec3[0:9]

                # pos = rec3.pop(0)
                # id = rec3.pop(0)
                #
                # POS     ID      REF     ALT
                # print(chrom, pos, ref, alt)
                # print(qual, filter, info, format)
                format_arr = format.split(":")
                format_len = format_arr.__len__()
                # print(rec3)
                # assert(len(smps) == len(rec3))
                assert(len(smps) == len(rec3))

                row = []
                row.extend([chrom, pos, ref, alt])

                # extract sample genotypes
                smps_fmt_dict = {}
                for idx in range(len(rec3)):
                    smp = smps[idx]
                    gtstr = rec3.pop(0)
                    gtstr_arr = gtstr.split(":")
                    gtstr_len = gtstr_arr.__len__()
                    assert(format_len == gtstr_len)
                    # print(smp, gtstr)
                    smps_fmt_dict = OrderedDict()
                    # extract format fields
                    fmt_dict = OrderedDict()
                    # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
                    for j in range(format_len):
                        fmtkey = format_arr[j]
                        fmtval = gtstr_arr[j]
                        # print(fmtkey, fmtval)
                        fmt_dict[fmtkey] = fmtval
                    smps_fmt_dict[smp] = fmt_dict
                    # decode the genotype
                    gt = fmt_dict['GT']
                    if gt in ['./.', '0/.', './0', '1/.', './1']:
                        gtval = -1
                    elif gt in ['0/0']:
                        gtval = 0
                    elif gt in ['1/0', '0/1']:
                        gtval = 1
                    elif gt in ['1/1']:
                        gtval = 2
                    row.append(gtval)
                # print(smps_fmt_dict)

                fho.writerow(row)

                # for k in smps_fmt_dict.keys():
                    # print(k, smps_fmt_dict[k]['GT'])



                # print (rec.info)
                # print (rec.info.keys())
                # print (rec.info["DP"])
                # print(rec.format.keys())
                # print(list(rec.samples))
                # print(list(rec.genotype))
                # for i in rec.samples:
                #     print("i: ", i)
                    # print(rec.genotype(i))

                if row_pos % 5000 == 0:
                    print("---- %s ---" % row_pos)
                row_pos += 1
                # print(rec.__str__)
                # print(rec.alleles)

            fhof.close()



    def comb_vars_rprt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        print("Not implemented error codes...\n")

        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        if self.rprt_idx == 0:
            # normal
            # -------
            exit_codes += self.comb_vars()

        else:
            pass

        self.mark_exit_args(exit_codes, args)

    def rprts(self):

        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        reports = {
            0: self.comb_vars_rprt,
            # 1: self.fpkm_genes_rprt,
            # 2: self.fpkm_isofs_rprt,
            3: self.readcnt_rprt,
               # 4: self.kallisto_rprt,
               # 6: self.rnaseqc_fin_rprt,
               # 7: self.rpkmsat_rprt,
            8: self.deploy_web
               }


        exit_codes = reports[self.rprt_idx]()
        # print("exit_codes: ", exit_codes)

        self.mark_exit_args(exit_codes, args)

    def test_servers(self):

        self.servers_obj.test_zk()



# ##############################################################################
# ###########################     Push jobs        ############################
# #############################################################################

#     def push_jobs_sample_qa(self):
#         rc = self.lcnf.redis_client
#         samples = self.conf.sample_names
#         nb_samples = samples.shape[0]
#
#         rc.delete("sample_qa_all")
#         rc.delete("sample_qa")
#         done_jobs = rc.lrange("sample_qa_done", 0, -1)
#         print "done_jobs: ", done_jobs
#         submitted_jobs = []
#
#
#         #rc.set("shutdown",0)
#         #rc.set("active_queue","call_hc")
#         for smp_idx in range(nb_samples):
#             #print "sample,contig: %s, %s" % (smp_idx, contig_idx)
#             args = {"smp_idx":smp_idx}
#             args = json.dumps(args)
#             rc.rpush("sample_qa_all",args)
#             if args in done_jobs:
#               print "job done"
#             else:
#               rc.rpush("sample_qa",args)
#               submitted_jobs.append(args)
#
#         return submitted_jobs.__len__()
#
            

    #
    #
    #
    # def push_jobs_comb_vars(self):
    #     print "in push_jobs_comb_vars.."
    #     rc = self.lcnf.redis_client
    #
    #     rc.delete("comb_vars_all")
    #     rc.delete("comb_vars")
    #     done_jobs = rc.lrange("comb_vars_done", 0, -1)
    #     print "done_jobs: ", done_jobs
    #     submitted_jobs = []
    #
    #     #rc.set("shutdown",0)
    #     #rc.set("active_queue","call_hc")
    #     args = {"noarg": 0};args = json.dumps(args)
    #     rc.rpush("comb_vars_all",args)
    #     if args in done_jobs:
    #         print "job done"
    #     else:
    #         rc.rpush("comb_vars",args)
    #         submitted_jobs.append(args)
    #
    #     return submitted_jobs.__len__()
      
   
############################################
###############################   Administration   
    
#     def subm_client_jobs(self):
#
#         cmd = r"""TARGET_JOB_ID=$(echo "./dnaseq_server.py --spawn-clients-blocking" | qsub -V -m ae -W umask=0002 -A wst-164-aa -d %(work_dir)s -j oe -o %(job_output_dir)s -N spawclb -l walltime=6:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
# echo $TARGET_JOB_ID
# """ % {"work_dir": self.conf.pysrc_dir, "job_output_dir": self.conf.job_output_dir}
#         print cmd
#
#         p = subprocess.Popen( cmd ,shell=True,
#                                    cwd=self.conf.subm_dir,
#                                    #stdin = r2,
#                                    stdout = subprocess.PIPE)
#         p.wait()
#         out = p.stdout.read().strip()
#
#         print "out: ",out
                

                
    def loop(self, gid):
        self.redis_obj.gid = gid

        print("in loop...", gid)
        # recuperate local id
        lid = int(self.redis_obj.redis_client.hget("client:%s" % gid, "lid"))
        print("lid: ", lid)
        # set pid
        self.redis_obj.register_pid()
        print("pid: ", self.redis_obj.pid)

        while self.redis_obj.redis_client.get("shut_queues") == "0" and not self.redis_obj.qgraph_all_done:
            time.sleep(8)
            print("active_queues: ", self.redis_obj.qgraph_prop_open)
            print("lid: ", lid)
            print("all done: ",  self.redis_obj.qgraph_all_done)

            cand_qtsks = []
            for tsk, props in self.conf.tsk_nde_rsc_dict.items():
                print("key: ", tsk, "props: ", props)
                if tsk in self.redis_obj.qgraph_prop_open \
                        and lid < props["tsk_nde"]:
                    cand_qtsks.append(tsk)

            print("candidate task queues: ", cand_qtsks)

            # try them out
            for cnd in cand_qtsks:
                self.redis_obj.qwork = cnd
                self.cor_tsk = self.conf.tsk_nde_rsc_dict[cnd]["cor_tsk"]
                self.mem_tsk = self.conf.tsk_nde_rsc_dict[cnd]["mem_tsk"]

                self.unpack_execute_qwork()

        print("loop finished ok..")

    def pipe_ini(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)
        exit_codes = []


        spark, sc = self.servers_obj.get_new_spark_session(1,10,"pipe_ini")


        # clean hive folder
        if os.path.exists(self.hivef_dir_glo):
            shutil.rmtree(self.hivef_dir_glo, ignore_errors=False, onerror=None)
        # recreate
        if not os.path.exists(self.hivef_dir_glo):
            pathlib.Path(self.hivef_dir_glo).mkdir(parents=True)



        self.sc_qc_obj.spark_work_reference(self.conf.ref_gen_assembly)
        # self.sc_qc_obj.spark_work_reference("hyHg38")

        # create empty metadata for working tables
        self.sc_qc_obj.spark_create_tables_meta()

        spark.stop()

        # this is only once
        self.prep_gene_model()


        exit_codes.append(0)
        self.mark_exit_args(exit_codes, args)


    """
        # old ways of doing from exons, now from features
        ##### self.sc_qc_obj.spark_valid_bins()
    """

    def fq2unal_parq_ini(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        # afl deconvolution
        # self.reads_dir = self.conf.raw_afl_dir
        # get it from configuration parsion in scan_config_naseq
        self.reads_dir = self.conf.reads_dir

        spark, sc = self.servers_obj.get_new_spark_session(2,2,"fq2unal_parq_ini")


        tbl_name="unal_parq"
        # spark.sql("truncate table %s" % tbl_name)

        lanes_df = self.conf.sample_lanes
        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name]
        # print("lanes_df: \n", lanes_df)

        exit_codes = []

        # for f in glob.glob(self.mer_smp_glob):
        #     os.remove(f)

        for idx, row in lanes_df.iterrows():
            self.spln_idx=idx
            # print row
            # print("self.spln_idx: ", self.spln_idx, "self.spln_row: ", self.spln_row)

            # print("spln_align_lib_dir_glo: ", self.spln_align_lib_dir_glo)
            # print("proj_dir_glo: ", self.conf.proj_dir_glo)
            # print("self.cor_tsk: ", self.cor_tsk)
            # print("self.mem_tsk: ", self.mem_tsk)

            print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

            unal_parq_tbl=os.path.join(self.hivef_dir_glo, "unal_parq")
            unal_parq_prt="%s/smp_name=%s/rnln=%s" % (unal_parq_tbl,self.spln_smp,self.spln_runreg)

            print("unal_parq_prt: ", unal_parq_prt)

            # remove working folder
            if os.path.exists(unal_parq_prt):
                shutil.rmtree(unal_parq_prt, ignore_errors=False, onerror=None)
            # recreate
            if not os.path.exists(unal_parq_prt):
                pathlib.Path(unal_parq_prt).mkdir(parents=True)

        spark.stop()

        exit_codes.append(0)
        self.mark_exit_args(exit_codes, args)

    # may be caled with different library
    # just appends
    def fq2unal_parq(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        # afl deconvolution
        self.reads_dir = self.conf.raw_afl_dir

        exit_codes = []


        print("spln_align_lib_dir_glo: ", self.spln_align_lib_dir_glo)
        # print("proj_dir_glo: ", self.conf.proj_dir_glo)
        print("self.cor_tsk: ", self.cor_tsk)
        print("self.mem_tsk: ", self.mem_tsk)

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))



        # export FQ_SM="mut"
        # export FQ_RL="run4815_2"
        # export FQ_FILE="${FQ_SM}_${FQ_LB}"

        unal_parq_tbl=os.path.join(self.hivef_dir_glo, "unal_parq")
        unal_parq_prt="%s/smp_name=%s/rnln=%s" % (unal_parq_tbl,self.spln_smp,self.spln_runreg)

        print("unal_parq_prt: ", unal_parq_prt)

        # remove working folder
        if os.path.exists(unal_parq_prt):
            shutil.rmtree(unal_parq_prt, ignore_errors=False, onerror=None)
        # recreate
        if not os.path.exists(unal_parq_prt):
            pathlib.Path(unal_parq_prt).mkdir(parents=True)

        r1 = os.path.join(self.spln_align_lib_dir_glo, "r1.fastq")
        r2 = os.path.join(self.spln_align_lib_dir_glo, "r2.fastq")
        if not os.path.exists(r1):
            os.mkfifo(r1)

        if not os.path.exists(r2):
            os.mkfifo(r2)


        p1 = subprocess.Popen(r"""zcat %(p1)s > %(r1)s"""
                              % {"cor_tsk": self.cor_tsk,
                                 "mem_tsk": self.mem_tsk,
                                 "p1": self.spln_p1,
                                 "r1": r1}, shell=True,
                                cwd=self.spln_align_lib_dir_glo,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)

        p2 = subprocess.Popen(r"""zcat %(p2)s > %(r2)s"""
                              % {"cor_tsk": self.cor_tsk,
                                 "mem_tsk": self.mem_tsk,
                                 "p2": self.spln_p2,
                                 "r2": r2}, shell=True,
                                cwd=self.spln_align_lib_dir_glo,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)
        cmd = r"""unalbam2parq 500001 %(unal_parq_prt)s %(r1)s %(r2)s """ % \
              {"cor_tsk": self.cor_tsk,
               "mem_tsk": self.mem_tsk,
               "unal_parq_prt":unal_parq_prt,
               "r1": r1,
               "r2": r2}
        print(cmd)
        p3 = subprocess.Popen(cmd,shell=True,
                             cwd=self.spln_align_lib_dir_glo,
                              stdout=subprocess.PIPE)

        print(p1.pid, p2.pid, p3.pid)
        exit_codes = [pp.wait() for pp in (p1, p2, p3)]

        print(" exit_codes: ", exit_codes)

        os.unlink(r1)
        os.unlink(r2)


        self.mark_exit_args(exit_codes, args)

    def fq2unal_parq_fin(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"fq2unal_parq_fin")


        tbl_name="unal_parq"
        spark.sql("msck repair table %s" % tbl_name)

        spark.stop()
        exit_codes.append(0)
        self.mark_exit_args(exit_codes, args)

    #     du -hs a101-x101-yamanaka-e11/srvf/hivef/clasif_seqs/*/*/*
    def clasif_seqs(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        lanes_df = self.conf.sample_lanes

        exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(2,10,"clasif_seqs")


        for idx, row in lanes_df.iterrows():
            # if idx==0 or idx>=8:
            if idx>=0:
            # if True:
                self.spln_idx = idx
                print(("-----idx---- >%s< -----idx------" % idx, self.spln_smp, self.spln_lib, self.spln_runreg))
                exit_codes += self.sc_qc_obj.spark_parse_clasif_r1(self.spln_smp, self.spln_runreg)
                pass
            else:
                pass

        for idx, row in lanes_df.iterrows():
            # if idx>=4:
            if idx>=0:
                self.spln_idx=idx
                print(("-----idx---- >%s< -----idx------" % idx, self.spln_smp, self.spln_lib, self.spln_runreg))
            #     for reg_typ in ["T", "H", "M"]:
                for reg_typ in ["H"]:
                    # no bchver specific yet, needed for no throw
                    exit_codes+=self.sc_qc_obj.spark_parse_r2_join_r1(self.spln_smp, self.spln_runreg, reg_typ)
                    pass

        spark.stop()
        self.mark_exit_args(exit_codes, args)


    """
            # for idx, row in lanes_df.iterrows():
        #     self.spln_idx=idx
        #     print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))
        #     for reg_typ in ["T", "H", "M"]:
        #     for reg_typ in ["H"]:
                # exit_codes+=self.sc_qc_obj.spark_clasif_seqs_to_fq(self.spln_smp, self.spln_runreg, reg_typ)

    """

    # should be on reg_al, now is on spln
    def clasifseqs2fq(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        # for seq_typ in ["T","H","M"]:
        for seq_typ in ["H"]:

            clasif_seqs_tbl=os.path.join(self.hivef_dir_glo, "clasif_seqs")
            part_d="%s/smp_name=%s/rnln=%s/seq_typ=%s" % (clasif_seqs_tbl,self.spln_smp,self.spln_runreg,seq_typ)

            print("clasif_seqs_prt: ", part_d)


            tbl_d = self.spln_align_runreg_dir_glo

            out_f=os.path.join(tbl_d, "%s_%s_%s" % (self.spln_smp, self.spln_runreg, "%sP" % seq_typ))

            params={"partd": part_d, "outf": out_f}
            as_json=json.dumps(params, sort_keys=False)
            print("as_json: ", as_json)

            pipe_env=os.environ
            pipe_env["PIPECMD_OPTS"]=as_json

            # r1="%s.R1.fastq" % out_f
            # print("r1: ", r1)
            #
            # r2="%s.R2.fastq" % out_f
            # print("r2: ", r2)
            # if not os.path.exists(r1):
            #     os.mkfifo(r1)
            # if not os.path.exists(r2):
            #     os.mkfifo(r2)
            #
            # out_r1="%s.R1.fastq.gz" % out_f
            # out_r2="%s.R2.fastq.gz" % out_f

            # cmd = r"""clasifseqs2fq"""
            cmd = r"""bsk_clasif2fq2"""
            print(cmd)
            p1 = subprocess.Popen(cmd,shell=True,
                             cwd=tbl_d, env=pipe_env)

            # cmd=r"""cat %(r1)s | pigz --fast -p 8 --stdout -f > %(out_r1)s""" % {"r1": r1, "out_r1": out_r1}
            # cmd=r"""gzip -c %(r1)s > %(out_r1)s""" % {"r1": r1, "out_r1": out_r1}
            # print(cmd)
            # p2 = subprocess.Popen(cmd, shell=True,
            #                     cwd=tbl_d)
            # cmd=r"""cat %(r2)s | pigz --fast -p 8 --stdout -f > %(out_r2)s""" % {"r2": r2, "out_r2": out_r2}
            # cmd=r"""gzip -c %(r2)s > %(out_r2)s""" % {"r2": r2, "out_r2": out_r2}
            # print(cmd)
            # p3 = subprocess.Popen(cmd, shell=True,
            #                     cwd=tbl_d)


            # print(p1.pid, p2.pid, p3.pid)
            print(p1.pid)

            # exit_codes = [pp.wait() for pp in [p1,p2,p3]]
            exit_codes = [pp.wait() for pp in [p1]]
            # exit_codes = [pp.wait() for pp in [p3]]
            print(" exit_codes: ", exit_codes)

            # os.unlink(r1)
            # os.unlink(r2)

        self.mark_exit_args(exit_codes, args)


    # should be on reg_al, now is on spln
    def align_regal(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        # for regal in ["HP","TP","MP"]:
        for regal in ["HP"]:
        # for regal in ["MP"]:
            if (regal=="HP"):
                extra = "--rf --rna-strandness RF"
            if (regal=="TP"):
                extra = "--fr --rna-strandness FR"
            if (regal=="MP"):
                extra = ""

            self.spln_regal=regal
            print("spln_align_regal_dir_glo: ", self.spln_align_regal_dir_glo)
            aliparq_d = os.path.join(self.spln_align_regal_dir_glo,"aliparq")
            # remove output folder
            if os.path.exists(aliparq_d):
                shutil.rmtree(aliparq_d, ignore_errors=False, onerror=None)
            # and recreate, because is needed
            if not os.path.exists(aliparq_d):
                pathlib.Path(aliparq_d).mkdir(parents=True)

            # delete output files
            for i in [self.spln_align_regal_bam]:
                if os.path.exists(i):
                    os.remove(i)


            p1 = os.path.join(self.spln_align_regal_dir_glo, "pipe1.sam")
            p2 = os.path.join(self.spln_align_regal_dir_glo, "pipe2.bam")
            p3 = os.path.join(self.spln_align_regal_dir_glo, "pipe3.bam")
            if not os.path.exists(p1):
                os.mkfifo(p1)
            if not os.path.exists(p2):
                os.mkfifo(p2)
            if not os.path.exists(p3):
                os.mkfifo(p3)

            s1 = subprocess.Popen(r"""bam2segs %(p3)s aliparq 4000001"""
                                  % {"p3": p3}, shell=True,
                                  cwd=self.spln_align_regal_dir_glo,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)

            # s2 = subprocess.Popen(r"""sambamba sort -t %(cor_tsk)s -o %(regal_bam)s --tmpdir=%(scratch_d)s/tmp %(p2)s"""
            # "scratch_d": self.conf.scratch_loc_dir
            s2 = subprocess.Popen(r"""samtools sort --threads %(cor_tsk)s -m %(mem_tsk)sM %(p2)s -o %(regal_bam)s """
                                  % {"cor_tsk": self.cor_tsk,
                                     "mem_tsk": self.mem_tsk,
                                     "p2": p2,
                                     "regal_bam": self.spln_align_regal_bam}, shell=True,
                                  cwd=self.spln_align_regal_dir_glo,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)

            s3 = subprocess.Popen(r"""sambamba view -S %(p1)s -f bam -t 10 -l 9 | tee %(p2)s  > %(p3)s"""
                                  % {"cor_tsk": self.cor_tsk,
                                     "regal_bam": self.spln_align_regal_bam,
                                     "p1": p1,
                                     "p2": p2,
                                     "p3": p3}, shell=True,
                                  cwd=self.spln_align_regal_dir_glo,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)


            print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))



            fq_lb="%s_%s_%s" % (self.spln_smp,self.spln_runreg,self.spln_regal)
            cmd = r"""hisat2 --rg-id=%(fq_lb)s --rg SM:%(fq_sm)s --rg LB:%(fq_lb)s \
        --rg PL:ILLUMINA --rg PU:RAGLAB \
        --threads %(cor_tsk)s %(extra)s \
        -x %(hisat_ix)s \
        -1 ../%(fq_lb)s.R1.fastq.gz -2 ../%(fq_lb)s.R2.fastq.gz > %(p1)s""" % \
                  {"cor_tsk": self.cor_tsk,
                   "mem_tsk": self.mem_tsk,
                   "fq_sm": self.spln_smp,
                   "fq_lb": fq_lb,
                   "extra": extra,
                   "hisat_ix": self.conf.hisat_transcr_ix,
                   "p1": p1}
            print(cmd)
            s4 = subprocess.Popen(cmd,shell=True,
                                 cwd=self.spln_align_regal_dir_glo,
                                  stdout=subprocess.PIPE)

            print(s1.pid, s2.pid, s3.pid, s4.pid)
            exit_codes += [pp.wait() for pp in (s1, s2, s3, s4)]

            os.unlink(p3)
            os.unlink(p2)
            os.unlink(p1)


        print(" exit_codes: ", exit_codes)

        self.mark_exit_args(exit_codes, args)



    def segs2cnts(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        lanes_df = self.conf.sample_lanes

        exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(2,10,"segs2cnts")

        # find idx for restart
        # last line into config file
        # ls -ltrd /lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a099-x099-rima-e8/srvf/hivef/segs/smp_name\=200*/*/*
        # cat ../conf/all-sample-info.csv | grep 200305-WT-GV-SC-2OF16_A099
        for idx, row in lanes_df.iterrows():
            if row.skip==1 or idx<0:
                continue
            self.spln_idx=idx
            print("idx: ", idx)
            # print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))
            # for reg_al in ["HP","MP","TP"]:
            for reg_al in ["HP"]:
                exit_codes+=self.sc_qc_obj.spark_alipark2bin_segs(reg_al)
                pass

        spark.stop()
        self.mark_exit_args(exit_codes, args)


    def gncnts_unpiv(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        # lanes_df = self.conf.sample_lanes

        exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"gncnts_unpiv")

        # if os.path.exists(tbl_d):
        #     shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)

        smps_df = self.conf.sample_names
        smps_df = smps_df.query('skip != 1')

        # find idx for restart
        # last line into config file
        # ls -ltrd /lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a099-x099-rima-e8/srvf/hivef/gncnts/feat_mdl\=STE/cnt_typ\=UDHP/smp_name*
        # ls /lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a101-x101-yamanaka-e11/srvf/hivef/gncnts/feat_mdl\=STE/cnt_typ\=UDHP/smp_name\=A1/
        # cat ../conf/one-sample-info.csv | grep 200305-MU-MI-SC-51OF51_A099

        for feat_mdl in ["STE"]:
        # for feat_mdl in ["STE","SGS"]:
            # filter samples
            for idx, row in smps_df.iterrows():
                if False and (idx==0 or idx>=2):
                    continue

                print("--------- gncnts_unpiv ----------  smp_idx: %s -----------------------------" % idx)

                self.smp_idx = idx
                print(("smp_idx: ", self.smp_idx))
                print(("smp_name: ", self.smp_name))
                # exit_codes+=self.sc_qc_obj.spark_deconvol_umis_reg_al("T","TP",feat_mdl)
                exit_codes+=self.sc_qc_obj.spark_deconvol_umis_reg_al("H","HP",feat_mdl)
                pass

            # for cnt_typ in ["UDTP","UDHP"]:
            for cnt_typ in ["UDHP"]:
                exit_codes+=self.sc_qc_obj.spark_gncnts_unpiv("gncnts",self.conf.proj_abbrev,cnt_typ,feat_mdl)
                pass


        spark.stop()
        self.mark_exit_args(exit_codes, args)

    def gncnts_unpiv_tags(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        # args = {"rprt_idx": self.rprt_idx}
        # print("parameters: \n", args)

        # lanes_df = self.conf.sample_lanes

        # exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"gncnts_unpiv_tags")

        # if os.path.exists(tbl_d):
        #     shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)

        smps_df = self.conf.sample_names
        smps_df = smps_df.query('skip != 1')

        for feat_mdl in ["STE"]:
        # for feat_mdl in ["STE","SGS"]:
        #     for cnt_typ in ["UDTP","UDHP"]:
            for cnt_typ in ["UDHP"]:
                self.sc_qc_obj.spark_gncnts_unpiv("gncnts_tags","comb",cnt_typ,feat_mdl)
                pass


        spark.stop()
        # self.mark_exit_args(exit_codes, args)


    def bam10x_cb(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "bam10x2orc_cb")

        bam10x_d="/home/dbadescu/share/over/duny/bsk_naseq/bam10x2orc"
        in_f = os.path.join(bam10x_d,"rdal")
        tbl2 = spark.read.orc(in_f)
        tbl2 = tbl2.withColumn('cb', tbl2["tags.cb"])
        tbl2.show()
        # tbl2.registerTempTable("bam10x")


        # remove working folder
        tbl_name = "bam10x"
        spark.sql("drop table if exists %s" % tbl_name)

        # tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
        nob_d = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1/srvf/hivef"
        tbl_d = os.path.join(nob_d, tbl_name)


        # if os.path.exists(tbl_d):
        #     shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)
        # recreate
        # if not os.path.exists(tbl_d):
        #     pathlib.Path(tbl_d).mkdir(parents=True)

        # tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["cb"], mode="overwrite")
        tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        cmd = """select row_number() over (order by cb) as rowid,cb
        from
        (select distinct tags.cb as cb
         from bam10x
         order by cb) cbs
         """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()

        # remove working folder
        tbl_name = "bam_valid_cb"
        spark.sql("drop table if exists %s" % tbl_name)

        # tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
        nob_d = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1/srvf/hivef"
        tbl_d = os.path.join(nob_d, tbl_name)
        tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")


        # we need to create a working cbs table, join it with the ids
        # then query unique ids, and partition by that.

        nb_parts = tbl2.count()
        print(nb_parts)

        rdd0 = tbl2.rdd.map(lambda el: (el['cb'], el)).partitionBy(nb_parts, self.cust_partitioner)
        rdd1 = rdd0.map(lambda x: json.dumps(x[1].asDict(), sort_keys=False))
        rdd1.saveAsTextFile(os.path.join(nob_d, "inp.json"))


        dfp = tbl.toPandas()
        print("dfp: \n", dfp)
        dfp = dfp.sort_values(by=['rowid'], ascending=[True])

        out_f = os.path.join(bam10x_d,"bam10x_ids.csv")
        dfp.to_csv(out_f, sep=","
             , header=True
             , index=True
             , index_label="idx"
             )

        # spark.catalog.dropTempView("bam10x")
        spark.stop()

    def bam10x_snps_vaf(self,proj_name,seq_typ):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, thiscallersname())

        # , index_cols=[0]

        # df1 = pd.read_csv(os.path.join(self.conf.proj_dir, "bam_tenx/res","snp.list2.csv"),
        #                                 sep=',', header=None, usecols=[0,1,2,3,4,5,6,7]
        #                   , names=["cb", "chrom", "pos","ref", "alt", "het", "hom", "ale"]
        #                 )

        pdf = pd.read_csv(os.path.join(self.conf.proj_dir, "bam_tenx",seq_typ,"res",proj_name,"snp.vaf.csv"),
                          sep=',', header=None, usecols=[0, 1, 2, 3, 4, 5, 6, 7, 8]
                          , names=["cb", "chrom", "pos", "ref", "alt","vaf","ale","het", "hom"]
                          )

        print(pdf)

        spark.conf.set("spark.sql.execution.arrow.enabled", "true")

        df = spark.createDataFrame(pdf)
        df.show()

        tbl_name = "snp_vaf"
        nob_d = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1/srvf/hivef"
        tbl_d = os.path.join(nob_d, tbl_name)


        # if os.path.exists(tbl_d):
        #     shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)
        # recreate
        # if not os.path.exists(tbl_d):
        #     pathlib.Path(tbl_d).mkdir(parents=True)

        # tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["cb"], mode="overwrite")
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # pos_ref_alt
        # at least 2 cells mutated per position
        tbl_name = "snp_vaf_pos_ref_alt"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql("""select chrom,pos,ref,alt,count(*) as cnt
                          from snp_vaf
                          group by chrom,pos,ref,alt
                          having cnt >= 2
                          order by cnt desc""")
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # we need to export those positions to be used for coverage detection by jl script
        #

        cmd="""select distinct chrom,
                               (pos-1) as pos0,
                               pos
        from snp_vaf_pos_ref_alt
        order by chrom,pos
        """
        df = spark.sql(cmd)
        dfp = df.toPandas()

        print("dfp: \n", dfp)
        # dfp = dfp.sort_values(by=['rowid'], ascending=[True])

        out_f = os.path.join(os.path.join(self.conf.proj_dir, "bam_tenx",seq_typ,"res",proj_name, "reg-cov.bed"))
        dfp.to_csv(out_f, sep='\t'
                   , header=False
                   , index=False
                   )

        # cbs
        tbl_name = "snp_vaf_cbs"
        tbl_d = os.path.join(nob_d, tbl_name)
        # df = spark.sql("select distinct cb from snp_vaf")
        df = spark.sql("""
        select distinct sv.cb
        from snp_vaf_pos_ref_alt svpra
         join snp_vaf sv on sv.chrom = svpra.chrom and
                            sv.pos=svpra.pos and
                            sv.ref=svpra.ref and
                            sv.alt=svpra.alt
        """)
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # cb_pos_ref_alt (svcpra)
        # this is the mutated table canvas
        # selected to be at least 2 distinct cb per mutation alt
        tbl_name = "snp_vaf_cb_pos_ref_alt"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql("select * "
                       "from snp_vaf_cbs svc"
                       " cross join snp_vaf_pos_ref_alt svpra")
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        spark.stop()

    def bam10x_snps_vaf2(self,proj_name,seq_typ):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, thiscallersname())
        nob_d = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1/srvf/hivef"

        # coverage
        pdf = pd.read_csv(os.path.join(self.conf.proj_dir,"bam_tenx",seq_typ,"res",proj_name, "cov.pos.csv"),
                          sep='\t', header=None, usecols=[0, 1, 2, 3]
                          , names=["cb", "chrom", "pos", "cov"],
                          dtype={'cb': object, 'chrom': object, 'pos': int, 'cov': float}
                          )
        print(pdf)

        tbl_name = "cov_pos"
        tbl_d = os.path.join(nob_d, tbl_name)
        schema = StructType(
            [StructField("cb", StringType(), True),
             StructField("chrom", StringType(), True),
             StructField("pos", LongType(), True),
             StructField("cov", FloatType(), True)
             ])

        df = spark.createDataFrame(pdf, schema=schema)
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # cb_pos_ref_alt_vaf (svcprav)
        tbl_name = "snp_vaf_cb_pos_ref_alt_vaf"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql(
            """select svcpra.cb,
                       svcpra.chrom,
                       svcpra.pos,
                       svcpra.ref,
                       svcpra.alt,
                       nvl(sv.vaf,0.0) as vaf
                from snp_vaf_cb_pos_ref_alt svcpra
                 join cov_pos cp on cp.cb=svcpra.cb and
                                    cp.chrom = svcpra.chrom and
                                    cp.pos = svcpra.pos
                 left outer join snp_vaf sv on sv.cb=svcpra.cb and
                                    sv.chrom = svcpra.chrom and
                                    sv.pos = svcpra.pos and
                                    sv.ref= svcpra.ref and
                                    sv.alt= svcpra.alt
                where cp.cov >=6 
                order by svcpra.cb,
                       svcpra.chrom,
                       svcpra.pos,
                       svcpra.ref,
                       svcpra.alt
       """)
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")
        dfp = df.toPandas()

        df2=dfp.pivot_table(
            index=["chrom","pos","ref","alt"],
            columns="cb",
            values="vaf"
        ).reset_index()
        df2.index.name = df2.columns.name = None
        #
        print(df2)
        #
        out_f=os.path.join(self.conf.proj_dir, "bam_tenx",seq_typ,"res",proj_name,"vaf.mt.pivot.csv")

        df2.to_csv(out_f, sep=","
                   , header=True
                   , index=True
                   , index_label="idx"
                   )


        spark.stop()


    def bam10x_snps_vaf3(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, thiscallersname())
        nob_d = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1/srvf/hivef"


        # cbs
        tbl_name = "snp_vaf_cbs"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql("select distinct cb "
                       "from snp_vaf")
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")



        # cb_pos_ref_alt
        tbl_name = "snp_vaf_cb_pos_ref_alt"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql("select * "
                       "from snp_vaf_cbs svc"
                       " cross join snp_vaf_pos_ref_alt svpra")
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # cb_pos_ref_alt_vaf
        tbl_name = "snp_vaf_cb_pos_ref_alt_vaf"
        tbl_d = os.path.join(nob_d, tbl_name)
        df = spark.sql(
            """select svcpra.cb,
                       svcpra.chrom,
                       svcpra.pos,
                       svcpra.ref,
                       svcpra.alt,
                       nvl(sv.vaf,0.0) as vaf
                from snp_vaf_cb_pos_ref_alt svcpra
                 join cov_pos cp on cp.cb=svcpra.cb and
                                    cp.chrom = svcpra.chrom and
                                    cp.pos = svcpra.pos
                 left outer join snp_vaf sv on sv.cb=svcpra.cb and
                                    sv.chrom = svcpra.chrom and
                                    sv.pos = svcpra.pos and
                                    sv.ref= svcpra.ref and
                                    sv.alt= svcpra.alt
                where cp.cov >=6 
                order by svcpra.cb,
                       svcpra.chrom,
                       svcpra.pos,
                       svcpra.ref,
                       svcpra.alt
       """)
        spark.sql("drop table if exists %s" % tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")
        dfp = df.toPandas()

        df2=dfp.pivot_table(
            index=["chrom","pos","ref","alt"],
            columns="cb",
            values="vaf"
        ).reset_index()
        df2.index.name = df2.columns.name = None
        #
        print(df2)
        #
        out_f=os.path.join(self.conf.proj_dir, "bam_tenx/res","vaf.mt.pivot.csv")

        df2.to_csv(out_f, sep=","
                   , header=True
                   , index=True
                   , index_label="idx"
                   )


        spark.stop()


    # Dummy implementation assuring that data for each country is in one partition
    @staticmethod
    def cust_partitioner(ch_rowid):
        # return hash(seqname)
        return int(ch_rowid)


    def gen_tr_cov_prop(self):


        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        # args = {"rprt_idx": self.rprt_idx}
        # print("parameters: \n", args)

        # lanes_df = self.conf.sample_lanes
        # exit_codes = []
        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "gen_tr_cov_prop")

        # spark = self.servers_obj spark_session
        # sc = spark.sparkContext

        cmd = """
select ex.seqname,
       tr.transcript_id,
       tr.strand,
       min(ex.start) as tr_start,
       max(ex.end) as tr_end
from transcripts tr
 join transcript_exons te on te.transcript_id = tr.transcript_id
 join exons ex on ex.exon_id=te.exon_id
group by ex.seqname,
      tr.transcript_id,
      tr.strand            
order by seqname,
         tr_start,
         tr_end
                """

        print(cmd)
        tbl = spark.sql(cmd)
        udf_assign_bin = pyspfunc.udf(self.sc_qc_obj.assign_bin, returnType=IntegerType())
        tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('tr_start', 'tr_end'))
        tbl3=tbl2.orderBy(["ucscbin","tr_start","tr_end"], ascending=[0, 0, 0])

        tbl_name = "chrtr"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl3.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["seqname"], mode="overwrite")


        # transcript exons overlaps
        cmd="""select ct.seqname,
        ct.ucscbin,
       ct.transcript_id,
       ct.strand,
       ct.tr_start,
       ct.tr_end,
       ex.exon_id,
       ex.start,
       ex.end
from chrtr ct
 join transcript_exons te on te.transcript_id = ct.transcript_id
 join exons ex on ex.exon_id=te.exon_id and
                  ex.seqname=ct.seqname
"""

        # order by seqname,tr_start,tr_end,transcript_id,strand,start,end,exon_id

        print(cmd)
        tbl = spark.sql(cmd)
        tbl_name = "tr_ex_lim"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["seqname"], mode="overwrite")
        # .coalesce(1)
        tbl.repartition(200,["seqname","ucscbin"])\
            .sortWithinPartitions(["seqname","ucscbin","tr_start","tr_end","transcript_id","strand","start","end","exon_id"],
                                  ascending=[True,True,True,True,True,True,True,True,True])\
            .write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["seqname","ucscbin"], mode="overwrite")

        # add this model to the feature model (transcriptome limits instead of exons)
        feat_mdl="TEX"
        # print(self.overlapping_bins(840))

        # https://changhsinlee.com/pyspark-udf/

        overlapping_bins_list_udf = pyspfunc.udf(self.sc_qc_obj.overlapping_bins,
                                                 returnType=ArrayType(IntegerType()))
        # overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=StringType())

        cmd = """
        select seqname,ucscbin,count(*) as transcriptsperbin
        from tr_ex_lim
        group by seqname,ucscbin
        order by seqname,ucscbin
        """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()
        tbl2 = tbl.withColumn('overbins', overlapping_bins_list_udf('ucscbin'))
        tbl2.show()
        tbl2.registerTempTable("fvb_tr")

        cmd = """
    select seqname,
           ucscbin,
           overbins,
           overbin
    from fvb_tr
    lateral view explode(overbins) fvb2 as overbin
           """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()

        tbl_name = "feat_mdl_bins"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)

        prt_d = "%s/feat_mdl=%s" % (tbl_d, feat_mdl)
        # if not os.path.exists(prt_d):
        #     pathlib.Path(prt_d).mkdir(parents=True)

        tbl.repartition(1, "seqname").write.option("path", prt_d).mode("overwrite").partitionBy(["seqname"]).orc(
            prt_d)
        spark.sql("msck repair table %s" % tbl_name)

        spark.dropTempTable("fvb_tr")
        spark.stop()

        # ################################################################################
        cmd = """select row_number() over (order by seqname) as rowid,
                               seqname                                      
        from
        (
        select distinct seqname
        from chrtr
        order by seqname
        )
                """
        print(cmd)
        tbl = spark.sql(cmd)
        tbl_name = "chrs_from_tr"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        """select ch.rowid as ch_rowid,
                      ch.seqname
            from chrs_from_tr ch"""


        # cmd="""select count(*) as cnt
        # from chrs_from_tr"""
        # res_df = spark.sql(cmd)
        # nb_part=res_df.first()["cnt"]
        # print("nb_part :", nb_part)

        # cmd="""select *
# from tr_ex_lim
# """

# """where seqname='chr18' and
#       ucscbin=104"""

        # print(cmd)
        # res_df = spark.sql(cmd)
        #
        # rdd0=res_df.rdd.map(lambda el: (el['ch_rowid'], el)).partitionBy(nb_part, self.cust_partitioner)
        # rdd1=rdd0.map(lambda x: json.dumps(x[1].asDict(), sort_keys=False))

        # res_df2=res_df.repartition("seqname", "ucscbin") \
        #     .sortWithinPartitions(["seqname", "ucscbin"], ascending=[True, True])

        # rdd_json =res_df_rdd.map(lambda x: json.dumps(x.asDict(), sort_keys=False))
        # rdd1.saveAsTextFile("/home/dbadescu/share/over/duny/trex2percov/inp.json")

        spark.stop()

    def test88(self):
        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "gen_tr_cov_prop")

        # spark = self.servers_obj spark_session
        # sc = spark.sparkContext


        cmd="""select distinct seqname,ucscbin,overbin
                from feat_mdl_bins
                where feat_mdl='TEX'
        """
        tbl = spark.sql(cmd)
        tbl.show(20)
        tbl.registerTempTable("texbins")

        cmd = """
        select distinct cigar_op_ref_name,ucscbin
from segs
where smp_name='FLEX1USPRI1P8_A091' and
      rnln='1001_1001' and
      reg_al='HP'
      """
        print(cmd)
        tbl = spark.sql(cmd)
        tbl.show(20)
        tbl.registerTempTable("segbins")

        cmd = """select row_number() over (order by tb.seqname,tb.ucscbin,sg.ucscbin) as rowid,
                    tb.seqname as chrom,
                    tb.ucscbin as texbin,
                    sg.ucscbin as segbin
        from texbins tb
         join segbins sg on sg.cigar_op_ref_name=tb.seqname and
                         sg.ucscbin=tb.overbin
        """
        print(cmd)
        tbl = spark.sql(cmd)
        tbl.show(20)
        # tbl.registerTempTable("segbins")

        spark.stop()


    @staticmethod
    def runproc(cmd):
        proc = subprocess.Popen(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                )
        stdout, stderr = proc.communicate()

        return proc.returncode, stdout, stderr

    def iter_tr_cov_prop2(self):
        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "iter_tr_cov_prop2")

        cmd = """select distinct seqname,ucscbin,overbin
                        from feat_mdl_bins
                        where feat_mdl='TEX'
                """
        tbl = spark.sql(cmd)
        tbl.show(20)
        tbl.registerTempTable("texbins")

        cmd = """
        select distinct cigar_op_ref_name,ucscbin
        from segs
        where smp_name='%s' and
              rnln='%s' and 
              reg_al='%s' and
              cigar_op_ref_name like 'chr%s'
              """ % (self.spln_smp,self.spln_runreg,self.spln_regal,"%")
        print(cmd)
        tbl = spark.sql(cmd)
        tbl.show(20)
        tbl.registerTempTable("segbins")

        cmd = """select row_number() over (order by tb.seqname,tb.ucscbin,sg.ucscbin) as rowid,
                            tb.seqname as chrom,
                            tb.ucscbin as texbin,
                            sg.ucscbin as segbin
                from texbins tb
                 join segbins sg on sg.cigar_op_ref_name=tb.seqname and
                                 sg.ucscbin=tb.overbin
                where tb.seqname in ('chr1','chr2')
                """
        print(cmd)
        tbl = spark.sql(cmd)
        tbl.show(20)
        # tbl.registerTempTable("segbins")


        # cmd="""
        # select distinct cigar_op_ref_name
        # from segs
        # where smp_name='%s' and
        #       rnln='%s' and
        #       reg_al='%s' and
        #       cigar_op_ref_name like 'chr%s'
        # order by cigar_op_ref_name""" % (self.spln_smp,self.spln_runreg,self.spln_regal,"%")
        # print(cmd)

        # res_df = spark.sql(cmd).withColumnRenamed("cigar_op_ref_name","chrom")
        # res_df.show(10)
        nb_parts=tbl.count()
        print(nb_parts)

        rdd0 = tbl.rdd.map(lambda el: (el['rowid'], el)).partitionBy(nb_parts, self.cust_partitioner)
        rdd1 = rdd0.map(lambda x: json.dumps(x[1].asDict(), sort_keys=False))

        # nb_part = 1
        # rdd0 = res_df2.rdd.map(lambda el: (el['ch_rowid'], el)).partitionBy(nb_part, self.cust_partitioner)
        # rdd1 = rdd0.map(lambda x: json.dumps(x[1].asDict(), sort_keys=False))

        # res_df1=res_df.repartition(nb_chroms, ["chrom"])
        # res_df1=res_df.repartition("chrom")
        # res_df1.show(20)
        # print(res_df1.rdd.getNumPartitions())

        # one file
        # rdd1 = res_df1.rdd.coalesce(1).map(lambda x: json.dumps(x.asDict(), sort_keys=False))

        # this was the one
        ########################################################################################
        # rdd1 = res_df1.rdd.map(lambda x: json.dumps(x.asDict(), sort_keys=False))
        ##########################################################################################3

        # myjson = "/home/dbadescu/share/over/duny/bsk_naseq/chroms.json"
        # if os.path.exists(myjson):
        #     shutil.rmtree(myjson, ignore_errors=False, onerror=None)
        # rdd1.coalesce(1).saveAsTextFile(myjson)
        # rdd1.saveAsTextFile(myjson)

        pipe_env = os.environ
        params = {"smp_name": self.spln_smp, "rnln": self.spln_runreg,
                  "reg_al": self.spln_regal,
                  "tr_ex_lim_d": os.path.join(self.hivef_dir_glo, "tr_ex_lim"),
                  "segs_d": os.path.join(self.hivef_dir_glo, "segs")
                  }
        as_json = json.dumps(params, sort_keys=False)
        print("as_json: ", as_json)
        pipe_env["PIPECMD_OPTS"] = as_json

        tbl3 = rdd1.pipe("bsk_trex2percov", pipe_env)

        tbl4 = tbl3.map(lambda x: literal_eval(x))
        # tbl4 = tbl3.map(lambda x: json.loads(x))

        schema = StructType(
            [StructField("chrom", StringType(), True),
             StructField("texbin", LongType(), True),
             StructField("segbin", LongType(), True),
             StructField("transcript_id", StringType(), True),
             StructField("pos", LongType(), True),
             StructField("cov", FloatType(), True)
             ])

        df5 = spark.createDataFrame(tbl4, schema=schema)
        df5.show(3, False)

        tbl_name = "tr_cov_prop"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
        prt_d = "%s/smp_name=%s/rnln=%s/reg_al=%s" % (
        tbl_d, self.spln_smp, self.spln_runreg, self.spln_regal)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)
        df5.write.option("path", prt_d).mode("overwrite").partitionBy(["chrom"]).orc(prt_d)
        # df5.write.option("path", tbl_d).mode("overwrite").orc(tbl_d)
        spark.sql("msck repair table %s" % tbl_name)

        print("df5 row cnt: ", df5.count())

        # no more need for ucscbins, aggregate
        cmd = """select smp_name,
                                rnln,
                                reg_al,
                                chrom,
                                transcript_id,
                                pos, 
                                sum(cov) as cov
                from tr_cov_prop
                group by smp_name,
                         rnln,
                         reg_al,
                         chrom,
                         transcript_id,
                         pos
                        """
        tbl = spark.sql(cmd)
        tbl_name = "tr_cov_prop_nopart"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["smp_name", "rnln", "reg_al"],
                                                    mode="overwrite")

        spark.stop()


    def export_tr_cov_prop(self):

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "export_tr_cov_prop")


        # export reg_al
        cmd="""select smp_name,rnln,reg_al, pos,
	  percentile_approx(cov,0.05) as q05,
       percentile_approx(cov,0.25) as q25,
       percentile_approx(cov,0.50) as q50, 
       percentile_approx(cov,0.75) as q75,
       percentile_approx(cov,0.95) as q95
from tr_cov_prop_nopart
where smp_name = '%s' and
       rnln = '%s' and
       cov!=0
group by smp_name,rnln,reg_al, pos
order by reg_al,pos
        """ % (self.spln_smp,self.spln_runreg)

        tbl = spark.sql(cmd)
        dfp = tbl.toPandas()
        print("dfp: \n", dfp)
        dfp = dfp.sort_values(by=['reg_al','pos'], ascending=[True,True])

        out_f = os.path.join(self.conf.matr_dir, "tr_cov_prop_%s_%s_byregal.csv" % (self.spln_smp,self.spln_runreg))
        dfp.to_csv(out_f, sep=","
             , header=True
             , index=True
             , index_label="idx"
             )

        # export
        cmd = """select smp_name,rnln,pos,
        	  percentile_approx(cov,0.05) as q05,
               percentile_approx(cov,0.25) as q25,
               percentile_approx(cov,0.50) as q50, 
               percentile_approx(cov,0.75) as q75,
               percentile_approx(cov,0.95) as q95
        from tr_cov_prop_nopart
        where smp_name = '%s' and
               rnln = '%s' and
               cov!=0
        group by smp_name,rnln,pos
        order by pos
                """ % (self.spln_smp, self.spln_runreg)

        tbl = spark.sql(cmd)
        dfp = tbl.toPandas()
        print("dfp: \n", dfp)
        dfp = dfp.sort_values(by=['pos'], ascending=[True])

        out_f = os.path.join(self.conf.matr_dir, "tr_cov_prop_%s_%s_noregal.csv" % (self.spln_smp,self.spln_runreg))
        dfp.to_csv(out_f, sep=","
             , header=True
             , index=True
             , index_label="idx"
             )

        spark.stop()

    def iter_tr_cov_prop(self):

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "iter_tr_cov_prop")

        cmd = """select rowid,seqname
        from chrs_from_tr 
        where seqname in ('chr17','chr18','chr19')
                """
        res_df = spark.sql(cmd)

        for x in res_df.toLocalIterator():
            seqname=x["seqname"]
            nb_part=x["rowid"]

            cmd2="""select *
from tr_ex_lim
where seqname='%s'
            """  % seqname
            print(cmd2)
            res_df2 = spark.sql(cmd2)
            res_df2.show(5)

            # only one partition from select
            nb_part=1
            rdd0 = res_df2.rdd.map(lambda el: (el['ch_rowid'], el)).partitionBy(nb_part, self.cust_partitioner)
            rdd1 = rdd0.map(lambda x: json.dumps(x[1].asDict(), sort_keys=False))


            bamf=self.spln_align_regal_bam
            bamfchr="%s.%s.bam" % (bamf, seqname)
            # (self.spln_smp,self.spln_runreg,self.spln_regal)

            pipe_env = os.environ
            params = {"redis_server": "dunarel", "redis_port": 6379, "smp_name": self.spln_smp, "rnln": self.spln_runreg, "reg_al": self.spln_regal}
            as_json = json.dumps(params, sort_keys=False)
            print("as_json: ", as_json)
            pipe_env["PIPECMD_OPTS"] = as_json

            # tbl3=res_df.rdd.pipe("dpiperdd", pipe_env)
            # tbl3 = rdd_json.pipe("dpiperdd", pipe_env)

            cmd=r"""sambamba slice -o %(bamfchr)s %(bamf)s %(chr)s""" % {"bamfchr": bamfchr, "bamf": bamf, "chr": seqname}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_regal_dir_glo, stdout=subprocess.PIPE,
                                  env=pipe_env)
            stdout, stderr = p1.communicate()
            # p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            print(stdout)
            print(stderr)
            if rc1==0:
                cmd = r"""sambamba view -c %(bamfchr)s""" % {"bamfchr": bamfchr}
                print(cmd)
                p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_regal_dir_glo, stdout=subprocess.PIPE,
                                      env=pipe_env)
                stdout, stderr = p1.communicate()
                # p1.wait()
                rc1 = p1.returncode
                print("rc1: ", rc1)
                nbreads=int(stdout.decode('ascii'))
                print("nbreads: ", nbreads)
                print(stderr)


                if nbreads>1000:
                    cmd = r"""genomecov2redis.sh %(bamfchr)s %(chr)s""" % {"bamfchr": bamfchr, "chr": seqname}
                    print(cmd)
                    p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_regal_dir_glo, stdout=subprocess.PIPE,
                                          env=pipe_env)
                    p1.wait()
                    rc1 = p1.returncode
                    print("rc1: ", rc1)
                    rdd1 = res_df2.rdd.coalesce(1).map(lambda x: json.dumps(x.asDict(), sort_keys=False))
                    # one file
                    myjson="/home/dbadescu/share/over/duny/bsk_naseq/inp.json"
                    if os.path.exists(myjson):
                        shutil.rmtree(myjson, ignore_errors=False, onerror=None)
                    # rdd1.coalesce(1).saveAsTextFile(myjson)
                    rdd1.saveAsTextFile(myjson)

                    pipe_env = os.environ
                    params = {"redis_server": "dunarel", "redis_port": 6379, "smp_name": self.spln_smp,
                              "rnln": self.spln_runreg, "reg_al": self.spln_regal, "chrom": seqname}
                    as_json = json.dumps(params, sort_keys=False)
                    print("as_json: ", as_json)
                    pipe_env["PIPECMD_OPTS"] = as_json

                    tbl3 = rdd1.pipe("bsk_trex2percov", pipe_env)

                    tbl4 = tbl3.map(lambda x: literal_eval(x))
                    # tbl4 = tbl3.map(lambda x: json.loads(x))

                    schema = StructType(
                        [StructField("transcript_id", StringType(), True),
                         StructField("pos", LongType(), True),
                         StructField("cov", FloatType(), True)
                         ])

                    df5 = spark.createDataFrame(tbl4, schema=schema)
                    df5.show(3, False)

                    tbl_name = "tr_cov_prop"
                    tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
                    prt_d = "%s/smp_name=%s/rnln=%s/reg_al=%s/chrom=%s" % (tbl_d, self.spln_smp, self.spln_runreg, self.spln_regal, seqname)
                    if not os.path.exists(prt_d):
                        pathlib.Path(prt_d).mkdir(parents=True)
                    df5.write.option("path", prt_d).mode("overwrite").orc(prt_d)
                    # df5.write.option("path", tbl_d).mode("overwrite").orc(tbl_d)
                    spark.sql("msck repair table %s" % tbl_name)

                    print("df5 row cnt: ", df5.count())


                    # to delete
                    # redis-cli --raw keys "OOCYTECDNA*" | xargs redis-cli del
                    cmd = r"""redis-cli del %(smp)s:%(rnln)s:%(reg_al)s:%(chr)s""" \
                          % {"smp": self.spln_smp,
                             "rnln": self.spln_runreg,
                             "reg_al": self.spln_regal,
                             "chr": seqname}
                    print(cmd)
                    p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_regal_dir_glo, stdout=subprocess.PIPE)
                    p1.wait()
                    rc1 = p1.returncode
                    print("rc1: ", rc1)

                    # exit(1)

                # print("tbl3c: ", tbl3c)


                # nb_part = res_df.first()["cnt"]
                # print("nb_part :", nb_part)












        # rdd_json.coalesce(1).saveAsTextFile("/home/dbadescu/share/over/duny/trex2percov/inp.json")


        # cmd2 = r"""
        # select qname,seq,qual
        # from unal_parq
        # where read_nb=1 and
        #       smp_name='wt' and
        #       rnln='run4815_1' and
        #       qname in ('D00280:347:HYHV3BCXY:1:1109:20524:37820','D00280:347:HYHV3BCXY:1:1109:20524:39225')
        #     	"""

        # cmd2 = r"""
        #         select qname,seq,qual
        #         from unal_parq
        #         where read_nb=1 and
        #               smp_name = '%s' and
        #               rnln= '%s'
        #         """ % (smp_name, rnln)
        #
        # print("cmd2: ", cmd2)
        #
        # res_df = spark.sql(cmd2)
        # res_df.show(3, False)

        # tbl3c = res_df.collect()

        # for x in res_df.toLocalIterator():
        #     as_json=json.dumps(x.asDict(), sort_keys=False)
        # print("as_json: ", as_json)
        # print("tbl3: ", as_json)
        # print("tbl3c: ", tbl3c)

        spark.stop()







    def gen_tbl_extetr(self):

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"gen_tbl_extetr")

        # ex+te+tr
        cmd="""
        select ex.exon_id,
               ex.seqname,
               ex.start,
               ex.end,
               ex.ucscbin,
               tr.transcript_id,
               tr.gene_id,
               tr.strand
        from exons ex
         join transcript_exons te on te.exon_id=ex.exon_id
         join transcripts tr on tr.transcript_id=te.transcript_id
        """

        print(cmd)
        tbl = spark.sql(cmd)

        tbl_name="extetr"
        tbl_d=os.path.join(self.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["seqname"], mode="overwrite")

        spark.stop()

    def gen_feat_model_gene_span(self,feat_mdl):

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"gen_feat_model_gene_span")

        # create table partitioned
        tbl_name="feats"
        tbl_d=os.path.join(self.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)

        # create stranded gene_span model
        cmd = """
        select sp.gene_id,
        sp.gene_id || '_' || sp.seqname || '_' || sp.strand as feat_id,
        sp.seqname,
        sp.strand,
        sp.start,
        sp.end
from
(
select strand,
       gene_id,
       seqname,
       min(start) as start,
       max(end) as end
from extetr
where regexp_extract(gene_id, '^(.*)(PAR)(.*)', 2) == ''
group by strand,
         gene_id,
         seqname
) sp
        """
        print(cmd)
        tbl = spark.sql(cmd)
        udf_assign_bin = pyspfunc.udf(self.sc_qc_obj.assign_bin, returnType=IntegerType())
        tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('start', 'end'))
        tbl2.show()

        prt_d="%s/feat_mdl=%s" % (tbl_d,feat_mdl)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)

        tbl2.write.option("path", prt_d).mode("overwrite").partitionBy(["seqname"]).orc(prt_d)
        spark.sql("msck repair table %s" % tbl_name)

        # export table
        # out_f="/home/badescud/projects/rrg-ioannisr/share/raglab_prod/igenomes/%s/templ/hive/%s" % (genome, table)
        out_f=os.path.join(self.conf.proj_dir, "templ/hive", tbl_name)
        tbl2.write.orc(out_f, mode="overwrite")

        spark.stop()

    def gen_feat_model_tr_ex(self,feat_mdl):

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"gen_feat_model_tr_ex")

        # create table partitioned
        tbl_name="feats"
        tbl_d=os.path.join(self.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)

        # create stranded gene_span model
        cmd = """
select sp.gene_id,
        sp.exon_id as feat_id,
        sp.seqname,
        sp.strand,
        sp.start,
        sp.end
from
(
select strand,
       seqname,
       gene_id,
       exon_id,
       min(start) as start,
       max(end) as end
from extetr
where regexp_extract(gene_id, '^(.*)(PAR)(.*)', 2) == ''
group by strand,
       seqname,
       gene_id,
       exon_id
) sp
order by strand,
       seqname,
       gene_id,
       exon_id
        """
        print(cmd)
        tbl = spark.sql(cmd)
        udf_assign_bin = pyspfunc.udf(self.sc_qc_obj.assign_bin, returnType=IntegerType())
        tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('start', 'end'))
        tbl2.show()

        prt_d="%s/feat_mdl=%s" % (tbl_d,feat_mdl)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)

        tbl2.write.option("path", prt_d).mode("overwrite").partitionBy(["seqname"]).orc(prt_d)
        spark.sql("msck repair table %s" % tbl_name)

        # export table
        # out_f="/home/badescud/projects/rrg-ioannisr/share/raglab_prod/igenomes/%s/templ/hive/%s" % (genome, table)
        # out_f=os.path.join(self.conf.proj_dir, "templ/hive", tbl_name)
        # tbl2.write.parquet(out_f, mode="overwrite")

        spark.stop()



    def spark_valid_feat_bins(self, feat_mdl):

        spark, sc = self.servers_obj.get_new_spark_session(1,10,"spark_valid_feat_bins")


        # print(self.overlapping_bins(840))

        # https://changhsinlee.com/pyspark-udf/

        overlapping_bins_list_udf = pyspfunc.udf(self.sc_qc_obj.overlapping_bins, returnType=ArrayType(IntegerType()))
        # overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=StringType())

        cmd = """
       select seqname,
              ucscbin,
              count(*) as exonsperbin
 from feats
 where feat_mdl='%(feat_mdl)s'
 group by seqname,ucscbin
 order by seqname,ucscbin""" \
              %{"feat_mdl": feat_mdl}
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()
        tbl2 = tbl.withColumn('overbins', overlapping_bins_list_udf('ucscbin'))
        tbl2.show()

        tbl_name = "fvb3"
        tbl_d = os.path.join(self.hivef_dir_glo, tbl_name)
        spark.sql("drop table if exists %s" % tbl_name)

        tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        # tbl2.write.saveAsTable("fvb3", format="parquet", mode="overwrite")

        cmd="""
 select seqname,
        ucscbin,
        overbins,
        overbin
 from fvb3
 lateral view explode(overbins) fvb2 as overbin
        """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()

        tbl_name="feat_mdl_bins"
        tbl_d=os.path.join(self.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)


        prt_d="%s/feat_mdl=%s" % (tbl_d,feat_mdl)
        # if not os.path.exists(prt_d):
        #     pathlib.Path(prt_d).mkdir(parents=True)

        tbl.repartition(1,"seqname").write.option("path", prt_d).mode("overwrite").partitionBy(["seqname"]).orc(prt_d)
        spark.sql("msck repair table %s" % tbl_name)



        # spark.sql("drop table if exists fvb4")
        # tbl.write.saveAsTable("fvb4", format="parquet", mode="overwrite")
        #
        # schema = tbl.schema
        # print("schema: \n", schema.jsonValue())

        spark.stop()



    def prep_gene_model(self):

        self.gen_tbl_extetr()
        # import exon -transcript basic model

        # create SGS model from exons
        # extetr and feat tables
        self.gen_feat_model_gene_span("SGS")
        self.gen_feat_model_tr_ex("STE")

        # mark which bins are used for the model
        for feat_mdl in ["SGS","STE"]:
        # for feat_mdl in ["STE"]:
            self.spark_valid_feat_bins(feat_mdl)
            pass


    def import_gncnts_tags(self,feat_mdl_a,cnt_typ_a,tag_a):

        spark, sc = self.servers_obj.get_new_spark_session(2,10,"import_gncnts_tags")


        # remove working folder
        tbl_name="gncnts_tags"
        tbl_d=os.path.join(self.hivef_dir_glo, tbl_name)

        if os.path.exists(tbl_d):
            shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)
        # recreate
        if not os.path.exists(tbl_d):
            pathlib.Path(tbl_d).mkdir(parents=True)

        spark.sql("msck repair table %s" % tbl_name)

        tbl_name_in="gncnts"
        for feat_mdl in feat_mdl_a:
            for cnt_typ in cnt_typ_a:
                for tag in tag_a:
                    prt_d="%s/feat_mdl=%s/cnt_typ=%s/tag=%s" % (tbl_d,feat_mdl,cnt_typ,tag)
                    in_f=os.path.join(self.conf.proj_dir, "srvf/filef", "%s_%s_%s_%s" % (tbl_name_in, feat_mdl, cnt_typ, tag))
                    tbl2=spark.read.parquet(in_f)
                    tbl2.write.option("path", prt_d).mode("overwrite").partitionBy(["smp_name"]).parquet(prt_d)

        tbl_name="gncnts_tags"
        spark.sql("msck repair table %s" % tbl_name)

        spark.stop()





"""
            if self.redis_obj.queue_active == "wait" and lid < 6:
                # to do something waiting
                pass
            elif self.redis_obj.queue_active == "finished":
                self.redis_obj.redis_client.set("shutdown", 1)

            elif self.redis_obj.queue_active == "spln_calc" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "grind_lanes" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "indel_realigner" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "grind_smps_gtk" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "merge_smpgrps" and lid < 1:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "smpgrp_calc" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "stats_qc" and lid < 2:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "wiggle" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "qnsort" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "raw_counts" and lid < 8:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "fpkm_known" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "fpkm_rabt" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "fpkm_denovo" and lid < 8:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "rnaseqc_ini_rprt" and lid < 3:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "call_hc" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "cat_hc" and lid < 2:
                self.unpack_execute_active_queue()

            elif self.redis_obj.queue_active == "cat_hc_bam" and lid < 2:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "genotype_gvcf_contig" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "cat_hc_contigs" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "call_mp" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "cat_mp" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "call_ug" and lid < 8:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "cat_ug" and lid < 1:
                self.unpack_execute_active_queue()
            elif self.redis_obj.queue_active == "rprts" and lid < 1:
                self.unpack_execute_active_queue()
"""




            #elif self.redis_obj.queue_active == "grind_lanes_clp" and lid < 1:
             #   self.unpack_execute_active_queue()
            #elif self.redis_obj.queue_active == "grind_lanes_spl_clp" and lid < 1:
             #   self.unpack_execute_active_queue()


            # elif self.redis_obj.queue_active == "cat_realigned" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "sort_realigned" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "base_recalibrator" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "rna_rprts" and lid < 1:
            #     self.unpack_execute_active_queue()






            # elif self.redis_obj.queue_active == "trim_lane_trimmomatic" and lid < 1:
            #     self.unpack_execute_active_queue()

            # elif self.redis_obj.queue_active == "stats_spln_blast" and lid < 8:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "align_lane_bwa" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "sort_lane" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "stats_spln_insert" and lid < 8:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "merge_lanes" and lid < 1:
            #     self.unpack_execute_active_queue()
            # elif self.redis_obj.queue_active == "stats_smp_targ" and lid < 8:
            #     self.unpack_execute_active_queue()
            #


            



    #  #loads and dumps are complimentary operations
    #  #args2 = json.dumps(args)
    # #idx = rc.lpop(active_queue)
    # #if not idx: continue
    # def loop(self, gid):
    #
    #     rc =  self.lcnf.redis_client
    #     self.gid = gid
    #     print "in loop...", gid
    #
    #     #recuperate local id
    #     lid = int(rc.hget("client:%s" % gid, "lid"))
    #
    #
    #
    #     #report and register
    #     #zscore = rc.zscore("client_hosts",host)
    #     #print "host,zscore: ",host, zscore
    #     #if zscore is not None:
    #     #    rc.zincrby("client_hosts", host)
    #     #else:
    #     #    rc.zadd("client_hosts", 1, host)
    #
    #     #loads and dumps are complimentary operations
    #     #args2 = json.dumps(args)
    #

            

# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    pass

