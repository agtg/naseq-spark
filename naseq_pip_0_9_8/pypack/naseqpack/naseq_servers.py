
# from mixin import Mixin, mixin

import subprocess
import jinja2
from naseq_utils import *

import http
import json
import pprint

import requests
import pandas as pd
import json
import itertools

from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
from pyspark.sql.types import StructField, StructType, LongType, IntegerType
# spark is an existing SparkSession.
from pyspark.sql import Row

# import binning as bn
import re

# with composition
# class NaseqServers(Mixin):

import threading


class NaseqServers(object):

    def __init__(self, ctx):
        self._ctx = ctx
        # defaults to each host a name
        self.is_master = False
        self.master_host_name = "localhost"
        self.jupy_port = 9926

    @property
    def is_master(self):
        return self.__is_master

    @is_master.setter
    def is_master(self, val):
        self.__is_master = val

    @property
    def loc_dir_name(self):

        if self.is_master:
            res = self.master_host_name
        else:
            res = self._ctx.redis_obj.local_host

        return res


#  -------------- SERVERS


    @property
    def servers_dir(self):
        loc_dir = os.path.join(self._ctx.conf.proj_dir, "servers")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_loc_dir(self):
        loc_dir = os.path.join(self.servers_dir, "zk")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_loc_conf_dir(self):
        loc_dir = os.path.join(self.zk_loc_dir, self.loc_dir_name)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_conf_dir(self):
        loc_dir = os.path.join(self.zk_loc_conf_dir, "conf")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_log_dir(self):
        loc_dir = os.path.join(self.zk_loc_conf_dir, "log")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_data_dir(self):
        loc_dir = os.path.join(self.zk_loc_conf_dir, "data")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def derby_loc_dir(self):
        loc_dir = os.path.join(self.servers_dir, "derby")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def zk_zoo_name(self):
        return "zoo.cfg"

    @property
    def derby_loc_conf_dir(self):

        loc_dir = os.path.join(self.derby_loc_dir, self.loc_dir_name)

        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def derby_conf_dir(self):
        loc_dir = os.path.join(self.derby_loc_conf_dir, "conf")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def derby_log_dir(self):
        loc_dir = os.path.join(self.derby_loc_conf_dir, "log")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def derby_properties_name(self):
        return "derby.properties"

    @property
    def derby_properties_depl_file(self):
        return os.path.join(self.derby_conf_dir, self.derby_properties_name)

    @property
    def zk_zoo_depl_file(self):
        return os.path.join(self.zk_conf_dir, self.zk_zoo_name)


    @property
    def drill_loc_dir(self):
        loc_dir = os.path.join(self.servers_dir, "drill")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def drill_loc_conf_dir(self):
        loc_dir = os.path.join(self.drill_loc_dir, self._ctx.redis_obj.local_host)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def drill_conf_dir(self):
        loc_dir = os.path.join(self.drill_loc_conf_dir, "conf")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def drill_log_dir(self):
        loc_dir = os.path.join(self.drill_loc_conf_dir, "log")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def drill_pid_dir(self):
        loc_dir = os.path.join(self.drill_loc_conf_dir, "pid")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def drill_env_name(self):
        return "drill-env.sh"

    @property
    def drill_env_depl_file(self):
        return os.path.join(self.drill_conf_dir, self.drill_env_name)

    @property
    def drill_override_name(self):
        return "drill-override.conf"

    @property
    def drill_override_depl_file(self):
        return os.path.join(self.drill_conf_dir, self.drill_override_name)

    @property
    def templ_drill_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "drill")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def templ_zk_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "zk")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def templ_derby_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "derby")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def spark_loc_dir(self):
        loc_dir = os.path.join(self.servers_dir, "spark")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_loc_conf_dir(self):

        loc_dir = os.path.join(self.spark_loc_dir, self.loc_dir_name)

        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_conf_dir(self):
        loc_dir = os.path.join(self.spark_loc_conf_dir, "conf")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_log_dir(self):
        loc_dir = os.path.join(self.spark_loc_conf_dir, "log")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_pid_dir(self):
        loc_dir = os.path.join(self.spark_loc_conf_dir, "pid")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_local_dir(self):
        loc_dir = os.path.join(self.spark_loc_conf_dir, "local")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_work_dir(self):
        loc_dir = os.path.join(self.spark_loc_conf_dir, "work")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_env_sh_name(self):
        return "spark-env.sh"

    @property
    def spark_env_py_name(self):
        return "spark-env.py"

    @property
    def spark_env_sh_depl_file(self):
        return os.path.join(self.spark_conf_dir, self.spark_env_sh_name)

    @property
    def spark_env_py_depl_file(self):
        return os.path.join(self.spark_conf_dir, self.spark_env_py_name)

    @property
    def spark_hive_site_name(self):
        return "hive-site.xml"

    @property
    def spark_hive_site_depl_file(self):
        return os.path.join(self.spark_conf_dir, self.spark_hive_site_name)

    @property
    def spark_defaults_name(self):
        return "spark-defaults.conf"

    @property
    def spark_defaults_depl_file(self):
        return os.path.join(self.spark_conf_dir, self.spark_defaults_name)

    @property
    def spark_log4j_name(self):
        return "log4j.properties"

    @property
    def spark_log4j_depl_file(self):
        return os.path.join(self.spark_conf_dir, self.spark_log4j_name)

    @property
    def ssh_config_name(self):
        return "ssh_config"

    @property
    def ssh_config_depl_file(self):
        return os.path.join(self._ctx.conf.proj_dir, self.ssh_config_name)


    @property
    def templ_spark_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "spark")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spark_session(self):
        return self.__spark_session

    @spark_session.setter
    def spark_session(self, val):
        self.__spark_session = val




    def start_zk_server(self):
        print("in test zk ...")

        # keep configurations at same location
        self.is_master = True

        # load_one_module('raglab/zookeeper/3.4.7')
        print("redis_host: ", self._ctx.redis_obj.redis_host)
        # print("localhost: ", self._ctx.redis_obj.local_host)

        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_zk_dir))
        # drill_env
        template = env.get_template(self.zk_zoo_name)
        template_vars = {"zk_data_dir" : self.zk_data_dir}
        txt_out = template.render(template_vars)

        print("self.zk_zoo_depl_file: ", self.zk_zoo_depl_file)

        f = open(self.zk_zoo_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # this one is empty
        self.zk_log_dir

        # start zookeper
        zk_nohup_log = os.path.join(self.zk_loc_conf_dir, "zk_nohup.log")
        my_env = os.environ
        my_env["ZOOCFGDIR"] = self.zk_conf_dir
        my_env["ZOO_LOG_DIR"] = self.zk_log_dir

        cmd = r"""nohup zkServer.sh restart >& %s &""" % zk_nohup_log
        print(cmd)
        ct = subprocess.Popen(cmd, env=my_env, shell=True,
                              cwd=self.zk_loc_conf_dir,
                              stdout=subprocess.PIPE)
        ct.wait()


    def start_drillbit(self):

        print("in start_drillbit ...")


        # load_one_module('raglab/drill/1.5.0')

        # create local configuration folders
        self.drill_conf_dir
        self.drill_log_dir
        self.drill_pid_dir


        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_drill_dir))
        # drill_env
        template = env.get_template(self.drill_env_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         "local_host": self._ctx.redis_obj.local_host}
        txt_out = template.render(template_vars)

        f = open(self.drill_env_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # dill_override
        template = env.get_template(self.drill_override_name)
        template_vars = {"redis_host": self._ctx.redis_obj.redis_host}
        txt_out = template.render(template_vars)

        f = open(self.drill_override_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # start drill
        # rm -fr ../servers/drill
        # . ../servers/drill/hb-2r05-n17/conf/drill-env.sh
        # $DRILL_HOME/bin/drillbit.sh restart

        drill_proc_log = os.path.join(self.drill_loc_conf_dir, "drill_proc.log")
        cmd = r""". %(drill_env)s; \
nohup $DRILL_HOME/bin/drillbit.sh restart >& %(logfile)s &""" % \
              {"drill_env": self.drill_env_depl_file,
               "logfile": drill_proc_log}
        print(cmd)
        ct = subprocess.Popen(cmd, shell=True,
                              cwd="%s/bin" % os.environ['DRILL_HOME'],
                              stdout=subprocess.PIPE)
        ct.wait()

        #conn = jaydebeapi.connect('org.hsqldb.jdbcDriver',
        #                          ['jdbc:hsqldb:mem:.', 'SA', ''],
        #                          '/path/to/hsqldb.jar',)

        # conn = jaydebeapi.connect('org.apache.drill.jdbc.Driver',
        #                           ['jdbc:drill:drillbit=localhost;schema=sys', 'admin', 'admin'],
        #                           '/gs/project/wst-164-ab/share/expr/a010-x036/build/drill-jdbc-all-1.6.0-SNAPSHOT.jar',)


        # curs = conn.cursor()
        #curs.execute('create table CUSTOMER'
        #            '("CUST_ID" INTEGER not null,'
        #           ' "NAME" VARCHAR not null,'
        #           ' primary key ("CUST_ID"))'
        #         )

        #curs.execute("insert into CUSTOMER values (1, 'John')")
        # curs.execute('show tables;')
        # res = curs.fetchall()
        # print("res: ", res)

    def drill_config_init(self):
        # create a file and put it on the classpath
        # bootstrap-storage-plugins.json


        # do not foget to copy derbyclient to jar
        # verify with  tail -f  ../servers/drill/hb-2r03-n23/log/drillbit.out

        # Create a connection to the local instance of Drill
        drillRestConn = http.client.HTTPConnection("%s:8047" % self._ctx.redis_obj.redis_host)

        # First GET standard 'dfs' plugin via REST
        # drillRestConn.request("GET","/storage/dfs.json")
        drillRestConn.request("GET", "/storage/hive.json")
        drillRestResponse = drillRestConn.getresponse()
        # storageDict = json.loads(drillRestResponse.read().decode("utf-8"))

        pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(storageDict)

        storageDict = {}
        storageDict["name"] = "hive"
        storageDict["type"] = "hive"
        # storageDict["enabled"] = True
        storageDict["config"] = {}
        storageDict["config"]["configProps"] = {}
        storageDict["config"]["configProps"]["javax.jdo.option.ConnectionURL"] = "jdbc:derby://%s:15270/hivem;create=true;" % self._ctx.redis_obj.redis_host
        storageDict["config"]["configProps"]["javax.jdo.option.ConnectionUserName"] = "app2"
        storageDict["config"]["configProps"]["javax.jdo.option.ConnectionPassword"] = "app2"
        storageDict["config"]["configProps"]["hive.metastore.warehouse.dir"] = "%s/srvf/hivef" % self._ctx.conf.proj_dir
        storageDict["config"]["configProps"]["fs.default.name"] = "file:///"
        storageDict["config"]["configProps"]["hive.metastore.sasl.enabled"] = "false"

        # storageDict["config"]["workspaces"]["pro"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/project/wst-164-ab/share',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["loc"] = { 'defaultInputFormat': None,
        #                                        'location': '/localscratch/dbadescu',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["glo"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/scratch/dbadescu',
        #                                        'writable': True}
        #
        # del storageDict["config"]["workspaces"]["root"]
        # del storageDict["config"]["workspaces"]["tmp"]
        #
        pp.pprint(storageDict)
        # {"type": "hive", "enabled": true,"configProps": {"javax.jdo.option.ConnectionURL": "jdbc:derby://hb-2r05-n23:15270/hivem;create=true;","javax.jdo.option.ConnectionUserName": "app2","javax.jdo.option.ConnectionPassword": "app2","hive.metastore.warehouse.dir": "/gs/project/wst-164-aa/share/rnvar/a032-x062-e01/srvf/hivef","fs.default.name": "file:///","hive.metastore.sasl.enabled": "false"}}



        # Now modify it into one for HDFS
        # storageDict["name"] = "share"
        # storageDict["config"]["connection"] = "hdfs://<ADDRESS OF HDFS NAMENODE>:8020"
        # storageDict["config"]["workspaces"]["pro"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/project/wst-164-ab/share',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["loc"] = { 'defaultInputFormat': None,
        #                                        'location': '/localscratch/dbadescu',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["glo"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/scratch/dbadescu',
        #                                        'writable': True}
        #
        # del storageDict["config"]["workspaces"]["root"]
        # del storageDict["config"]["workspaces"]["tmp"]
        #
        # pp.pprint(storageDict)
        #
        storageJson = json.dumps(storageDict)
        #
        # Then POST it back via REST
        pluginHeader = {"Content-Type": "application/json"}
        drillRestConn.request("POST", "/storage/hive.json", storageJson, pluginHeader)
        drillRestReponse = drillRestConn.getresponse()


        # print("drillRestReponse :", drillRestReponse)
        pp.pprint(drillRestReponse.read())

    # default config:
    #     {
  # "type": "hive",
  # "enabled": false,
  # "configProps": {
  #   "hive.metastore.uris": "",
  #   "javax.jdo.option.ConnectionURL": "jdbc:derby:;databaseName=../sample-data/drill_hive_db;create=true",
  #   "hive.metastore.warehouse.dir": "/tmp/drill_hive_wh",
  #   "fs.default.name": "file:///",
  #   "hive.metastore.sasl.enabled": "false"
  # }
  # }

    #     {
  # "type": "hive",
  # "enabled": true,
  # "configProps": {
  #   "javax.jdo.option.ConnectionURL": "jdbc:derby://cp2533:15270/hivem;create=true;",
  #   "javax.jdo.option.ConnectionUserName": "app2",
  #   "javax.jdo.option.ConnectionPassword": "app2",
  #   "hive.metastore.warehouse.dir": "/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/srvf/hivef",
  #   "fs.default.name": "file:///",
  #   "hive.metastore.sasl.enabled": "false"
  # }
  # }

    def drill_config_rest(self):

        # Create a connection to the local instance of Drill
        drillRestConn = http.client.HTTPConnection("localhost:8047")

        # First GET standard 'dfs' plugin via REST
        # drillRestConn.request("GET","/storage/dfs.json")
        drillRestConn.request("GET", "/storage/share.json")
        drillRestResponse = drillRestConn.getresponse()
        storageDict = json.loads(drillRestResponse.read().decode("utf-8"))

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(storageDict)


        # Now modify it into one for HDFS
        # storageDict["name"] = "share"
        # storageDict["config"]["connection"] = "hdfs://<ADDRESS OF HDFS NAMENODE>:8020"
        # storageDict["config"]["workspaces"]["pro"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/project/wst-164-ab/share',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["loc"] = { 'defaultInputFormat': None,
        #                                        'location': '/localscratch/dbadescu',
        #                                        'writable': True}
        # storageDict["config"]["workspaces"]["glo"] = { 'defaultInputFormat': None,
        #                                        'location': '/gs/scratch/dbadescu',
        #                                        'writable': True}
        #
        # del storageDict["config"]["workspaces"]["root"]
        # del storageDict["config"]["workspaces"]["tmp"]
        #
        # pp.pprint(storageDict)
        #
        # storageJson = json.dumps(storageDict)
        #
        # Then POST it back via REST
        # pluginHeader = {"Content-Type": "application/json"}
        # drillRestConn.request("POST", "/storage/hdfs.json", storageJson, pluginHeader)
        # drillRestReponse = drillRestConn.getresponse()

        # print("drillRestReponse :", drillRestReponse)

    def drill_rest_sql(self):

        # sqlline -u "jdbc:drill:zk=hb-2r03-n76";
        # !quit
        # select * from sys.drillbits;

        url = "http://localhost:8047/query.json"
        # employee_query = """SELECT * from sys.drillbitsmanagement_role, COUNT( employee_id ) as roleCount
        # FROM cp.`employee.json`
        # GROUP BY management_role
        # ORDER BY roleCount DESC"""
        cmd_query = """select * from sys.drillbits
        """

        data = {"queryType" : "SQL", "query": cmd_query }
        data_json = json.dumps(data)

        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=data_json, headers=headers)
        res = response.json()

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(res)

        print("res: ", res)

        df = pd.DataFrame( response.json()['rows'] )
        print( df.head() )

    def load_spark_env(self):

        # load it inside environment
        exec(open(self.spark_env_py_depl_file).read())

    # --hiveconf hive.server2.thrift.port=10000 \
    def spawnth_thriftserver(self, name="blabla"):

        # cannot be fragmented
        cmd = r""". %(spark_env)s; $SPARK_HOME/bin/spark-class org.apache.spark.deploy.SparkSubmit \
        --class org.apache.spark.sql.hive.thriftserver.HiveThriftServer2 \
        --name Thrift_JDBC --executor-memory 4g --total-executor-cores 4 --executor-cores 4 --conf spark.executor.instances=1 \
        --hiveconf hive.metastore.warehouse.dir=%(proj_dir)s/srvf/hivef \
        --hiveconf javax.jdo.option.ConnectionURL="jdbc:derby://%(redis_host)s:15270/hivem;create=true;" \
        --hiveconf javax.jdo.option.ConnectionDriverName=org.apache.derby.jdbc.ClientDriver \
    --hiveconf javax.jdo.option.ConnectionUserName=app2 \
    --hiveconf javax.jdo.option.ConnectionPassword=app2 \
    --hiveconf hive.server2.transport.mode=http \
    --hiveconf hive.server2.thrift.http.port=10001 \
    --hiveconf hive.server2.thrift.http.max.worker.threads=500 \
    --hiveconf hive.server2.thrift.http.min.worker.threads=5 \
    --hiveconf hive.server2.thrift.http.path=cliservice \
    --hiveconf hive.server2.thrift.bind.host=%(local_host)s \
    --hiveconf hive.server2.authentication=NOSASL \
    --hiveconf hive.metastore.sasl.enabled=false \
    --master spark://%(redis_host)s:7077
    """ \
              % {"spark_env": self.spark_env_sh_depl_file,
                 "proj_dir": self._ctx.conf.proj_dir,
                 "redis_host": self._ctx.redis_obj.redis_host,
                 "local_host": "localhost"
                 }

        print(cmd)

        cwd = "%s/spawn" % self._ctx.conf.proj_dir
        flog = open("%s/thrift.log" % cwd, "w")
        ferr = open("%s/thrift.err" % cwd, "w")

        flog.write("cmd %s \n" % cmd)


        print("cwd: ", cwd)
        subprocess.call([cmd], shell=True, cwd=cwd, stdout=flog, stderr=ferr)

    def spawnth_locslv(self, idx=1, redis_host="localhost", cores="4", mem="8G", webui_base=8100):

        webui_port=webui_base+idx
        # cannot be fragmented
        cmd = r""". %(spark_env)s; $SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker --webui-port %(webui_port)s -c %(cores)s -m %(mem)s spark://%(redis_host)s:7077""" \
              % {"spark_env": self.spark_env_sh_depl_file,
                 "webui_port": webui_port,
                 "redis_host": redis_host,
                 "cores": cores,
                 "mem": mem
                 }

        print("cmd: ", cmd)

        cwd = "%s/spawn" % self._ctx.conf.proj_dir
        flog = open("%s/locslv_%s.log" % (cwd, idx), "w")
        ferr = open("%s/locslv_%s.err" % (cwd, idx), "w")

        print("cwd: ", cwd)
        subprocess.call([cmd], shell=True, cwd=cwd, stdout=flog, stderr=ferr)

    def spawnth_derby_server(self):

            java_cmd = "$JAVA_HOME/bin/java"
            derby_conf = "%s/servers/derby/localhost/conf" % self._ctx.conf.proj_dir
            classp = "$DERBY_HOME/lib/derby.jar:" \
                     "$DERBY_HOME/lib/derbynet.jar:" \
                     "$DERBY_HOME/lib/derbytools.jar:" \
                     "$DERBY_HOME/lib/derbyclient.jar"

            cmd = r"""%(java_cmd)s -Dderby.system.home=%(derby_conf)s -classpath %(classp)s org.apache.derby.drda.NetworkServerControl start""" \
                  % {"java_cmd": java_cmd,
                     "derby_conf": derby_conf,
                     "classp": classp
                     }

            print("cmd: ", cmd)

            cwd = "%s/spawn" % self._ctx.conf.proj_dir
            flog = open("%s/derby_srv.log" % cwd, "w")
            ferr = open("%s/derby_srv.err" % cwd, "w")

            my_env = os.environ
            my_env["DERBY_OPTS"] = r"""-Dderby.system.home=%s""" % self.derby_conf_dir

            print("cwd: ", cwd)
            subprocess.call([cmd], shell=True, cwd=cwd, stdout=flog, stderr=ferr, env=my_env)

    def start_derby_server(self):
        print("in start_derby_server...")

        # keep configurations at same location
        self.is_master = True

        # load_one_module('raglab/derby/10.10.2.0')
        print("redis_host: ", self._ctx.redis_obj.redis_host)
        # print("localhost: ", self._ctx.redis_obj.local_host)

        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_derby_dir))
        # drill_env
        template = env.get_template(self.derby_properties_name)
        template_vars = {"derby_log_dir": self.derby_log_dir}
        txt_out = template.render(template_vars)

        f = open(self.derby_properties_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # this one is empty
        # self.derby_log_dir

        # start derby
        # derby_nohup_log = os.path.join(self.derby_loc_conf_dir, "derby_nohup.log")
        # my_env = os.environ
        # my_env["DERBY_OPTS"] = r"""-Dderby.system.home=%s""" % self.derby_conf_dir


        # cmd = r"""startNetworkServer >& %s &""" % (derby_nohup_log)
        # print(cmd)
        # ct = subprocess.Popen(cmd, shell=True, env=my_env,
        #                       cwd=self.derby_loc_conf_dir,
        #                       stdout=subprocess.PIPE)
        # ct.wait()

        thr_derby = threading.Thread(target=self.spawnth_derby_server, kwargs=dict())
        thr_derby.daemon = True
        thr_derby.start()
        #
        # not delay anymore

        print(" start_derby_server done")


    def spawnth_thriftserver2(self, name="blabla"):

        # cmd = r""". %(spark_env)s; %(proj_dir)s/spawn/spn_thrift.sh %(name)s""" \
        #       % {"spark_env": self.spark_env_sh_depl_file,
        #          "proj_dir": self._ctx.conf.proj_dir,
        #          "name": name}

        java_cmd="/home/dbadescu/share/raglab_prod/software/java/jdk1.8.0_91-inst/bin/java"

        classp="/home/dbadescu/share/proj_aba/b002-qualivz-e4/servers/spark/localhost/conf/:" \
               "/home/dbadescu/share/raglab_prod/software/spark/spark-2.4.3-inst/jars/*:" \
               "/home/dbadescu/share/raglab_prod/software/derby/db-derby-10.10.2.0-bin/lib/derbyclient.jar:" \
               "/home/dbadescu/share/raglab_prod/software/spark/spark-2.4.3-jars/:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/etc/hadoop/:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/common/lib/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/common/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/lib/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/yarn/lib/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/yarn/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/mapreduce/lib/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/mapreduce/*:" \
               "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/contrib/capacity-scheduler/*.jar"

        # cannot be fragmented
        cmd = r""". %(spark_env)s; %(java_cmd)s -cp %(classp)s -Xmx6g -Dorg.xerial.snappy.tempdir=%(proj_dir)s/work org.apache.spark.deploy.SparkSubmit --conf spark.executor.instances=1 --class org.apache.spark.sql.hive.thriftserver.HiveThriftServer2 --name ThriftJDBC --total-executor-cores 2 --executor-cores 2 spark-internal""" \
            % {"spark_env": self.spark_env_sh_depl_file,
               "proj_dir": self._ctx.conf.proj_dir,
               "java_cmd": java_cmd,
               "classp": classp}

        print("cmd: ", cmd)

        cwd = "%s/spawn" % self._ctx.conf.proj_dir
        flog = open("%s/thrift.log" % cwd, "w")
        ferr = open("%s/thrift.err" % cwd, "w")

        print("cwd: ", cwd)
        subprocess.call([cmd], shell=True, cwd=cwd, stdout=flog, stderr=ferr)

    def spawnth_spark_master(self, host="localhost", port="7077", webui_port="8080"):

        spark_proc_log = os.path.join(self.spark_loc_conf_dir, "spark_proc_master.log")
        spark_proc_err = os.path.join(self.spark_loc_conf_dir, "spark_proc_master.err")

        # cannot be fragmented
        cmd = r""". %(spark_env)s; $SPARK_HOME/bin/spark-class org.apache.spark.deploy.master.Master --host %(host)s --port %(port)s --webui-port %(webui_port)s""" \
              % {"spark_env": self.spark_env_sh_depl_file,
                 "host":host,
                 "port":port,
                 "webui_port":webui_port
                 }

        print("cmd: ", cmd)

        flog = open(spark_proc_log, "w")
        ferr = open(spark_proc_err, "w")

        cwd = "%s/spawn" % self._ctx.conf.proj_dir

        print("cwd: ", cwd)
        print("spark_proc_log: ", spark_proc_log, " spark_proc_err: ", spark_proc_err)
        subprocess.call([cmd], shell=True,
                        cwd=cwd,
                        stdout=flog, stderr=ferr)



    def spawnth_spark_master2(self, host="localhost", port="7077", webui_port="8080"):

        spark_proc_log = os.path.join(self.spark_loc_conf_dir, "spark_proc_master.log")
        spark_proc_err = os.path.join(self.spark_loc_conf_dir, "spark_proc_master.err")

        # cmd = r""". %(spark_env)s; %(proj_dir)s/spawn/spnsprk.sh %(webui_port)s""" \
        #       % {"spark_env": self.spark_env_sh_depl_file,
        #          "proj_dir": self._ctx.conf.proj_dir,
        #          "webui_port": webui_port}

        java_cmd = "/home/dbadescu/share/raglab_prod/software/java/jdk1.8.0_91-inst/bin/java"
        classp = "/home/dbadescu/share/proj_aba/b002-qualivz-e4/servers/spark/localhost/conf/:" \
                 "/home/dbadescu/share/raglab_prod/software/spark/spark-2.4.3-inst/jars/*:" \
                 "/home/dbadescu/share/raglab_prod/software/derby/db-derby-10.10.2.0-bin/lib/derbyclient.jar:" \
                 "/home/dbadescu/share/raglab_prod/software/spark/spark-2.4.3-jars/:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/etc/hadoop/:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/common/lib/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/common/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/lib/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/hdfs/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/yarn/lib/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/yarn/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/mapreduce/lib/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/share/hadoop/mapreduce/*:" \
                 "/home/dbadescu/share/raglab_prod/software/hadoop/hadoop-2.7.3-inst/contrib/capacity-scheduler/*.jar"

        # cannot be fragmented
        cmd = r""". %(spark_env)s; %(java_cmd)s -cp %(classp)s -Xmx2g org.apache.spark.deploy.master.Master --host dunarel --port 7077 --webui-port 8088""" \
              % {"spark_env": self.spark_env_sh_depl_file,
                 "java_cmd": java_cmd,
                 "classp": classp}

        print("cmd: ", cmd)

        flog = open(spark_proc_log, "w")
        ferr = open(spark_proc_err, "w")

        cwd = "%s/spawn" % self._ctx.conf.proj_dir

        print("cwd: ", cwd)
        print("spark_proc_log: ", spark_proc_log, " spark_proc_err: ", spark_proc_err)
        subprocess.call([cmd], shell=False,
                        cwd=cwd,
                        stdout=flog, stderr=ferr)




        # --total-executor-cores 4 --executor-cores 1 --executor-memory 2g
        # --conf spark.executor.instances 2
        #  --num-executors 2




        # print(cmd)
        # ['$SPARK_HOME/sbin/start-master.sh']
        # cmd_arr=[]
        #
        #/home/dbadescu/share/proj_aba/b002-qualivz-e4/pysrc/spnsprk.sh
        # subprocess.call(["$SPARK_HOME/sbin/start-master.sh"], shell=True,
        #                 cwd="%s/sbin" % os.environ['SPARK_HOME'],
        #                 stdout=f, stderr=f2)
        # $SPARK_HOME/sbin/spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host dunarel --port 7077 --webui-port 8080

    def start_spark_master(self):

        print("in start_sparkmaster ...")
        # keep configurations at same location
        self.is_master = True


        # load_one_module('raglab/drill/1.5.0')

        # create local configuration folders
        self.spark_conf_dir
        self.spark_log_dir
        self.spark_pid_dir
        self.spark_local_dir
        self.spark_work_dir

        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_spark_dir))
        # spark_env
        template = env.get_template(self.spark_env_sh_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         # "local_host": self._ctx.redis_obj.local_host,
                         "redis_host": self._ctx.redis_obj.redis_host,
                         "loc_dir_name": self.loc_dir_name}
        txt_out = template.render(template_vars)

        f = open(self.spark_env_sh_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # python too
        template = env.get_template(self.spark_env_py_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         # "local_host": self._ctx.redis_obj.local_host,
                         "redis_host": self._ctx.redis_obj.redis_host,
                         "loc_dir_name": self.loc_dir_name}
        txt_out = template.render(template_vars)

        f = open(self.spark_env_py_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # hive_site.xml
        template = env.get_template(self.spark_hive_site_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         "redis_host": self._ctx.redis_obj.redis_host
                         # "redis_host": "localhost"
                         }
        txt_out = template.render(template_vars)

        f = open(self.spark_hive_site_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # spark-defaults.conf
        template = env.get_template(self.spark_defaults_name)
        template_vars = {"redis_host": self._ctx.redis_obj.redis_host,
                         "proj_dir" : self._ctx.conf.proj_dir}
        txt_out = template.render(template_vars)

        f = open(self.spark_defaults_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # log4j.properties
        template = env.get_template(self.spark_log4j_name)
        template_vars = {}
        txt_out = template.render(template_vars)

        f = open(self.spark_log4j_depl_file, 'w')
        f.write(txt_out)
        f.close()


        # start drill
        # rm -fr ../servers/drill
        # . ../servers/drill/hb-2r05-n17/conf/drill-env.sh
        # $DRILL_HOME/bin/drillbit.sh restart

# nohup $SPARK_HOME/sbin/start-master.sh >& %(logfile)s &""" % \

        # control with
        # ps - efj | grep dbadescu

        # no paranthesis after funtion
        threadd1 = threading.Thread(target=self.spawnth_spark_master, kwargs=dict(host=self._ctx.redis_obj.redis_host,
                                                                                  port="7077", webui_port="8082"))
        threadd1.daemon = True
        threadd1.start()
        # time.sleep(10)
        print("..Spark server started done")

        # cwd = "%s/work" % os.environ['SPARK_HOME']
        # sprk_srv_log = "%s/dscripts/sprk_srv.log" % os.environ['SPARK_HOME']
        # cmd = r""". %(spark_env)s; \
        #         $SPARK_HOME/sbin/start-thriftserver.sh --total-executor-cores 4 --executor-cores 4 --conf spark.executor.instances=1  >& %(logfile)s &""" % \
              # {"spark_env": self.spark_env_sh_depl_file,
              #  "logfile": spark_proc_thrift_log}
        # print(cmd)
        # ct = subprocess.Popen(cmd, shell=True,
        #                       cwd="%s/sbin" % os.environ['SPARK_HOME'],
        #                       stdout=subprocess.PIPE)
        # ct.wait()


    def start_thrift_server(self):

        threadd2 = threading.Thread(target=self.spawnth_thriftserver,
                                kwargs=dict(name="ThriftJDBC"))
        threadd2.daemon = True
        threadd2.start()
        #
        # not delay anymore
        ##########################   time.sleep(10)
        print("..Thrift server started done")

    def start_thrift_server2(self):

        spark_proc_thrift_log = os.path.join(self.spark_loc_conf_dir, "spark_proc_thrift.log")
        cmd = r""". %(spark_env)s; $SPARK_HOME/sbin/start-thriftserver.sh --executor-memory 4g --total-executor-cores 4 --executor-cores 4 --conf spark.executor.instances=1  >& %(logfile)s &""" % \
              {"spark_env": self.spark_env_sh_depl_file,
               "logfile": spark_proc_thrift_log}
        print(cmd)
        ct = subprocess.Popen(cmd, shell=True,
                              cwd="%s/sbin" % os.environ['SPARK_HOME'],
                              stdout=subprocess.PIPE)
        ct.wait()

        # conn = jaydebeapi.connect('org.hsqldb.jdbcDriver',
        #                          ['jdbc:hsqldb:mem:.', 'SA', ''],
        #                          '/path/to/hsqldb.jar',)

        # conn = jaydebeapi.connect('org.apache.drill.jdbc.Driver',
        #                           ['jdbc:drill:drillbit=localhost;schema=sys', 'admin', 'admin'],
        #                           '/gs/project/wst-164-ab/share/expr/a010-x036/build/drill-jdbc-all-1.6.0-SNAPSHOT.jar',)

        # curs = conn.cursor()
        # curs.execute('create table CUSTOMER'
        #            '("CUST_ID" INTEGER not null,'
        #           ' "NAME" VARCHAR not null,'
        #           ' primary key ("CUST_ID"))'
        #         )

        # curs.execute("insert into CUSTOMER values (1, 'John')")
        # curs.execute('show tables;')
        # res = curs.fetchall()
        # print("res: ", res)

    def start_spark_locslv(self, nb_workers=3,cores="4", mem="8G"):

        print("in start_spark_locslv...")
        print("redis_host: ", self._ctx.redis_obj.redis_host)

        self.thr_locsrv_a = []
        for i in range(0, nb_workers+1):
            elem=threading.Thread(target=self.spawnth_locslv,
                                               kwargs={"idx": i, "redis_host": self._ctx.redis_obj.redis_host,
                                                       "cores": cores, "mem":mem, "webui_base":8100})
            self.thr_locsrv_a.append(elem)
            self.thr_locsrv_a[i].daemon = True
            self.thr_locsrv_a[i].start()
        #
        # not delay anymore
        ##########################   time.sleep(10)
        print("..start_spark_locslv done")


    def start_spark_slave(self):

        print("in start_sparkslave ...")


        # load_one_module('raglab/drill/1.5.0')

        # create local configuration folders
        # self.spark_conf_dir
        # self.spark_log_dir
        # self.spark_pid_dir

        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_spark_dir))
        # drill_env
        template = env.get_template(self.spark_env_sh_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         "local_host": self._ctx.redis_obj.local_host,
                         "redis_host": self._ctx.redis_obj.redis_host}
        txt_out = template.render(template_vars)
        print("txt_out: \n", txt_out.encode('utf-8'))

        f = open(self.spark_env_sh_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # python too
        template = env.get_template(self.spark_env_py_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         # "local_host": self._ctx.redis_obj.local_host,
                         "redis_host": self._ctx.redis_obj.redis_host,
                         "loc_dir_name": self.loc_dir_name}
        txt_out = template.render(template_vars)

        f = open(self.spark_env_py_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # hive_site.xml
        template = env.get_template(self.spark_hive_site_name)
        template_vars = {"proj_dir" : self._ctx.conf.proj_dir,
                         "redis_host": self._ctx.redis_obj.redis_host
                         # "redis_host": "localhost"
                         }
        txt_out = template.render(template_vars)

        f = open(self.spark_hive_site_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # spark-defaults.conf
        template = env.get_template(self.spark_defaults_name)
        template_vars = {"redis_host": self._ctx.redis_obj.redis_host}
        txt_out = template.render(template_vars)

        f = open(self.spark_defaults_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # log4j.properties
        template = env.get_template(self.spark_log4j_name)
        template_vars = {}
        txt_out = template.render(template_vars)

        f = open(self.spark_log4j_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # dill_override
        # template = env.get_template(self.drill_override_name)
        # template_vars = {"redis_host": self._ctx.redis_obj.redis_host}
        # txt_out = template.render(template_vars)

        # f = open(self.drill_override_depl_file, 'w')
        # f.write(txt_out)
        # f.close()

        # start drill
        # rm -fr ../servers/drill
        # . ../servers/drill/hb-2r05-n17/conf/drill-env.sh
        # $DRILL_HOME/bin/drillbit.sh restart

        # no paranthesis after funtion
        self.start_spark_locslv(nb_workers=2, cores="4", mem="8G")

        # spark_proc_log = os.path.join(self.spark_loc_conf_dir, "spark_proc_slave.log")
        # cmd = r""". %(spark_env)s; \
# $SPARK_HOME/sbin/start-slave.sh -c 4 -m 8G spark://%(redis_host)s:7077 >& %(logfile)s &""" % \
#               {"spark_env": self.spark_env_sh_depl_file,
#                "redis_host": self._ctx.redis_obj.redis_host,
#                "logfile": spark_proc_log}
#         print(cmd)
#         ct = subprocess.Popen(cmd, shell=True,
#                               cwd="%s/sbin" % os.environ['SPARK_HOME'],
#                               stdout=subprocess.PIPE)
#         ct.wait()

    def create_ssh_config(self):


        # location is in spark
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_spark_dir))

        # ssh_config should be one of the last
        template = env.get_template(self.ssh_config_name)

        local_forward = ""
        rngs = []

        rng1 = range(8080, 8090)
        rng2 = range(4050, 4057)
        rng3 = range(10000, 10006)
        # this is for drill
        rng4 = range(2181, 2182)
        rng5 = range(8047, 8048)
        rng6 = range(31010, 31013)
        rng7 = range(self.jupy_port, self.jupy_port+1)
        # this is for derby
        rng8 = range(15270, 15271)
        print("rng7: ", rng7)
        # find ports
        # netstat -a | grep LISTENING


        for port in itertools.chain(rng1, rng2, rng3, rng4, rng5, rng6, rng7,rng8):
            local_forward += "\t LocalForward %(port)s %(host)s:%(port)s \n" % {
                "host": self._ctx.redis_obj.redis_host, "port": port}
        print("host: %s", self._ctx.redis_obj.redis_host)

        template_vars = {"local_forward": local_forward}
        txt_out = template.render(template_vars)

        f = open(self.ssh_config_depl_file, 'w')
        f.write(txt_out)
        f.close()

    def start_jupyter_server(self):

        jupy_log = os.path.join(self._ctx.conf.redis_dir, "jupy.log")
        jupy_dir = os.path.join(self._ctx.conf.pysrc_dir)
        cmd = r"""nohup jupyter-notebook --no-browser \
--ip='*' --port %(jupy_port)s \
--notebook-dir=%(jupy_dir)s </dev/null &> %(jupy_log)s &""" \
              % {"jupy_port": self.jupy_port,
                 "jupy_dir": jupy_dir,
                 "jupy_log": jupy_log}
        # cmd = r"""nohup naseq_server.py --ctrl </dev/null >%s 2>&1 &""" % ctrl_log
        print(cmd)
        # once stdout is redirected you should never write to it in child process
        # write to stderr
        ct = subprocess.Popen(cmd, shell=True,
                              cwd=jupy_dir)
        ct.wait()


    def set_spark_session(self,sess_name):

        # depending if executing from master or slave node
        self.is_master = True

        # load it inside environment
        self.load_spark_env()

        master = "spark://%s:7077" % os.environ["SPARK_MASTER_HOST"]
        print("master: ", master)

        sconf = SparkConf()
        sc_all = sconf.getAll()
        print("sc_all: ", sc_all)

        print("conf_dir: ", os.environ["SPARK_CONF_DIR"])

        # hadoop conf dir is for finding the hive-site.xml
        print("HADOOP_CONF_DIR---: ", os.environ["HADOOP_CONF_DIR"])

        if (sess_name is None) or sess_name=="":
            sess_name="pySparkSQL"

        # .config("spark.jars", "/home/badescud/projects/rrg-ioannisr/share/raglab_prod/software/spark/spark-2.3.1-jars/spark-avro_2.11-4.0.0.jar")\
        # .config("spark.sql.files.maxPartitionBytes", "268435456") \
            # then we dynamically load all parameters
        spark = SparkSession.builder\
            .master(master)\
            .appName(sess_name)\
            .enableHiveSupport()\
            .config("spark.io.compression.codec", "lz4")\
            .config("spark.sql.parquet.compression.codec", "snappy") \
            .config("spark.sql.orc.compression.codec", "snappy") \
            .config("spark.ui.enabled", "false")\
            .config("spark.executor.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraClassPath", "%s" % os.environ["SPARK_CONF_DIR"])\
            .config("spark.driver.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self._ctx.conf.proj_dir)\
            .config("spark.executor.extraJavaOptions", "-Dorg.xerial.snappy.tempdir=%s/work" % self._ctx.conf.proj_dir)\
            .config("spark.eventLog.enabled", "false")\
            .config("spark.driver.allowMultipleContexts", "false")\
            .config("spark.scheduler.mode", "FIFO")\
            .config("spark.executor.instances", "1")\
            .config("spark.executor.cores", "1")\
            .config("spark.executor.memory", "8g")\
            .config("spark.driver.instances", "1")\
            .config("spark.driver.cores", "1")\
            .config("spark.driver.memory", "10g")\
            .config("spark.sql.shuffle.partitions", "4000") \
            .config("spark.default.parallelism", "4000") \
            .config("spark.rpc.numRetries", "10")\
            .config("spark.rpc.retry.wait", "20s")\
            .config("spark.driver.maxResultSize", "12g")\
            .config("spark.network.timeout", "400s")\
            .config("spark.executor.heartbeatInterval", "20s") \
            .config("hive.default.fileformat", "orc") \
            .config("hive.default.fileformat.managed", "orc") \
            .config("orc.compress", "SNAPPY") \
            .config("hive.exec.orc.default.compress", "SNAPPY") \
            .config("hive.exec.orc.zerocopy", "true") \
            .getOrCreate()

        # .config("spark.sql.hive.filesourcePartitionFileCacheSize", "262144000")

        # .config("spark.jars.packages", "org.apache.spark:spark-core_2.11:2.4.3") \
            #
        # .config("spark.sql.hive.convertMetastoreOrc", "true")\

# install jar to local maven repository ~/.m2
# mvn install:install-file -Dfile=/home/dbadescu/share/raglab_prod/softbase/download/spark-avro_2.11-2.4.1.jar -DgroupId=org.apache.spark -DartifactId=spark-avro_2.11 -Dversion=2.4.1 -Dpackaging=jar
# ls /home/dbadescu/.m2/repository/org/apache/spark/spark-core_2.11/2.4.1/spark-core_2.11-2.4.1.pom

# mvn install:install-file -Dfile=$DNLD/spark-avro_2.11-2.4.1.jar -DgroupId=org.apache.spark -DartifactId=spark-core_2.11 -Dversion=2.4.1 -Dpackaging=jar


#spark-core_2.11

            # .config("spark.scheduler.mode", "FAIR")\
        spark.conf.set("hive.metastore.warehouse.dir", "%s/srvf/hivef" % os.environ["PROJ_DIR"])

        self.spark_session = spark

            # .config("spark.eventLog.dir", "%s/logs" % self._ctx.conf.proj_dir)\
        # .config("spark.scheduler.mode", "FAIR")\

    def close_spark_session(self):

        self.spark_session = None

    def get_new_spark_session(self,exec_nb,sleep_secs,sess_name):

        self.set_spark_session(sess_name)
        spark = self.spark_session
        sc = spark.sparkContext
        while True:
            executor_count = len(sc._jsc.sc().statusTracker().getExecutorInfos()) - 1
            print("executor_count: ", executor_count)
            if executor_count>=exec_nb:
                print("ready to go executor_count: ", executor_count)
                break
            else:
                print("waiting for workers: ", executor_count)
                time.sleep(sleep_secs)
        return [spark,sc]

