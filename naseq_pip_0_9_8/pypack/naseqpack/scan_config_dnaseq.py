# -*- coding: utf-8 -*-
"""
Created on Wed Nov 12 15:24:32 2014

@author: root
"""

from __future__ import print_function


import imp
import os

pypack = os.environ['NASEQ_PYPACK']

scan_config_naseq = imp.load_source('scan_config_naseq', "%s/naseqpack/scan_config_naseq.py" % (pypack) )
ScanConfig = scan_config_naseq.ScanConfig

# from scan_config_naseq import ScanConfig


import os 
import csv
import pandas as pd

# from treedict import TreeDict

dnaseq_utils = imp.load_source('dnaseq_utils', "%s/naseqpack/dnaseq_utils.py" % (pypack) )
from dnaseq_utils import *


class ScanConfigDnaSeq(ScanConfig, object):

    def __init__(self):

        super(ScanConfigDnaSeq, self).__init__()


    @property
    def variants_dir(self):
        loc_dir = os.path.join(self.proj_dir, "variants")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def variants2_dir(self):
        loc_dir = os.path.join(self.proj_dir, "variants2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def vcfs_dir(self):
        loc_dir = os.path.join(self.proj_dir, "vcfs")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def subm_dir(self):
        loc_dir = os.path.join(self.proj_dir, "subm")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def job_output_dir(self):
        loc_dir = os.path.join(self.subm_dir, "job_output")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

# --------------------------------------------------- ASSEMBLY
#     @property
#     def ref_gen_assembly(self):
#         return "ucscHg19"
        # return "enGRCh37"

    # @property
    # def adapt_fa(self):
    #     return os.path.join(self.naseq_home_dir, "supl/adapters-truseq.fa")

    @property
    def ref_gen_fa(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/genome_fa/enGRCh37.fa")
        return self.ref_gen_genome_fa

    @property
    def ref_gen_bwa(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/bwa_ix/enGRCh37.fa")
        return self.ref_gen_bwa_ix_genome_fa


    @property
    def ref_gen_chrominfo(self):
        # return os.path.join(self.raglab_install_home,
        #                     "igenomes/enGRCh37/sequence/genome_fa/enGRCh37.chrominfo.txt")
        return self.ref_gen_genome_chrominfo

    @property
    def gen_idx(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/bt2_ix/enGRCh37")
        return self.ref_gen_bt2_ix

    # GATK karyordered
    @property
    def known_sites_cf(self):
        # return os.path.join(self.raglab_install_home,
        #                     "genomes/species/Homo_sapiens.GRCh37/annotations/Homo_sapiens.GRCh37.dbSNP142.vcf.gz")
        # return os.path.join(self.raglab_install_home,
        #                     "igenomes/ucHg19/annot/Hg19_dbsnp144_karyo2.vcf.gz")
        # return os.path.join(self.raglab_install_home,
        #                     "igenomes/ucHg19/annot/GRCh37.p13_dbsnp144.vcf.gz")

        return os.path.join(self.ref_gen_assembly_dir,
                            "annot/%(ref_assembly)s_known_sites2.vcf.gz" % {
                                "ref_assembly": self.ref_gen_assembly})





    # works with GRCh37 even for hg19 chr names
    @property
    def dbsnp_cf(self):
        # return os.path.join(self.raglab_install_home,
        #                     "genomes/species/Homo_sapiens.GRCh37/annotations/Homo_sapiens.GRCh37.dbSNP142.vcf.gz")
        # return os.path.join(self.raglab_install_home,
        #                     "igenomes/ucHg19/annot/Hg19_dbsnp144_karyo2.vcf.gz")
        return os.path.join(self.raglab_install_home,
                            "igenomes/ucHg19/annot/GRCh37.p13_dbsnp144.vcf.gz")

        # zcat ../variants2/taa-hc-mp-ug.snpid.vcf.gz | awk '$3~/^(rs.+)/'

    @property
    def snpeff_data(self):
        return os.path.join(self.raglab_install_home,
                            "igenomes/ucHg19/annot/snpeff_data")

    @property
    def snpeff_config(self):
        return os.path.join(self.raglab_install_home,
                            "igenomes/ucHg19/annot/snpeff_data/snpEff.config")

    @property
    def snpeff_genome(self):
        return "ucHg19.75"
        # return "GRCh37.75"



    @property
    def dbnsfp_db(self):
        # return os.path.join(self.raglab_install_home,
                            # "genomes/species/Homo_sapiens.GRCh37/annotations/dbNSFP/dbNSFP2.8.txt.gz")
        return os.path.join(self.raglab_install_home,
                            "igenomes/ucHg19/annot/dbNSFP2.9.txt.gz")

    def scan_local(self):
        print("in ScanConfigDnaSeq scan_local...")
        # conf.orig_dir = os.path.join(conf.proj_dir, "orig")
        super(ScanConfigDnaSeq, self).scan_local()
        return self



    def load_sample_lanes(self):
        print("self.conf_dir: ", self.conf_dir)
        conf_csv = os.path.join(self.conf_dir, "all-sample-info.csv")

        # ["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"]

        gr8 = pd.read_csv(conf_csv, sep=',', header=0,
                          dtype={'skip': int, 'smp_name': object, 'lib': object, 'run': object, 'lane': object},
                          index_col=[0]
                          )

        # gr8.rename(columns={'Library Barcode': 'Library'}, inplace=True)


        # gr8 = pd.read_csv(conf_csv, sep=',', header=0, usecols=[0, 1, 3, 5],
        #                   dtype={'Name': object, 'Library Barcode': object, 'Run': object, 'Region': object})
        # gr8.rename(columns={'Library Barcode': 'Library'}, inplace=True)

        self.sample_lanes = gr8

        # print("self.sample_lanes: ", self.sample_lanes)

        return self

    # def load_sample_lanes(self):
    #     print("self.conf_dir: ", self.conf_dir)
    #     conf_csv = os.path.join(self.conf_dir, "all-sample-info.csv")
    #
    #
    #     run is a string has leading zeros
        # gr8 = pd.read_csv(conf_csv, sep=',', header=False,
        #                   usecols=[0, 1, 3, 5],
        #                   dtype={'Run': 'S75'}
        # )
        #
        # gr8.rename(columns={'Library Barcode': 'Library'}, inplace=True)
        #
        #
        #
        # self.sample_lanes = gr8
        #
        #
        # print("self.sample_lanes: ", self.sample_lanes)
        #
        # return self


    # names are as defined by header
    def load_sample_names(self):

        gr8 = pd.read_csv(os.path.join(self.conf_dir, 'one-sample-info.csv'),
               sep=',',
               header = 0,
               index_col=[0]
               #usecols=[0,1]
               )

        #print "gr8: ", gr8
        self.sample_names = gr8

        return self

    # names are positional
    def load_sample_grps(self):

        gr8 = pd.read_csv(os.path.join(self.conf_dir, 'grp-sample-info.csv'),
               sep=',',
               header = 0,
               index_col=[0]
               #usecols=[0,1]
               )

        # print("gr8: ", gr8, file=sys.stderr, flush=True)

        header_data_row = ["skip", "smpgrp_name", "short_name", "cls_idx", "vshort_name" ]
        # df = pandas.read_csv(csv_file, skiprows=[0,1,2,3,4,5], names=header_row)
        gr8.columns = header_data_row


        self.sample_grps = gr8

        return self

    # takes an external dataset from encompassing class
    def save_sample_grps(self, df_):

        df_.to_csv(os.path.join(self.conf_dir, 'grp-sample-info.csv'),
                   sep=',', header=True
                   , index=True
                   , index_label="smp_idx")



    def load_sample_grps_smps(self):

        gr8 = pd.read_csv(os.path.join(self.conf_dir, 'smpgrp-smps.csv'),
               sep=',',
               header = 0,
               index_col=[0]
               #usecols=[0,1]
               )

        #print "gr8: ", gr8
        self.sample_grps_smps = gr8

        return self


    def gen_contigs_old(self):
        target_df = self.all_regions

        #print target_df
        #print "target_df shape: ", target_df.shape

        gr_target_df = target_df.groupby(['contig'])
        #print "gr_target_df"
        #print gr_target_df.describe()
        gr_contigs = pd.DataFrame.from_items([('contig', gr_target_df.groups.keys())])
        gr_contigs = gr_contigs.sort(['contig'], ascending=[1]).reset_index()
        gr_contigs.drop('index', axis=1, inplace=True)
        #save contigs list
        gr_contigs.to_csv(self.target_contigs_csv,
                          sep='\t', header = True
                          , index = True
                          , index_label = "idx"
                          )
        #print "gr_contigs"
        #print gr_contigs







    # def load_contig_names(self):
    #
    #     self._load_contig_names()

        # if not os.path.exists(self.contigs_csv):
        #     self.gen_targets()
        # else:
        #     self._load_contig_names()





        # print target_df
        # print "target_df shape: ", target_df.shape


    # def _load_regions(self, nb_regions):
    #
        # if not os.path.exists(self.regions_csv):
        #     self.gen_region_beds(nb_regions)
        
        # self.region_names = pd.read_csv(self.regions_csv,
        #               sep='\t', header = False
        #               ,index_col=[0]
        #               ,usecols=[0,1]
        #               ,names=["idx", "region"]
        #               )
        #
        #
        # print target_df
        # print "target_df shape: ", target_df.shape
    #



                    
                    