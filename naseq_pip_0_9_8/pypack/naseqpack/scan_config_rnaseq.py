# -*- coding: utf-8 -*-
"""
Created on Wed Nov 12 15:24:32 2014

@author: root
"""
from scan_config_naseq import ScanConfig

import os 
import csv
import pandas as pd

# from treedict import TreeDict

import imp
import os

pypack = os.environ['NASEQ_PYPACK']

rnaseq_utils = imp.load_source('rnaseq_utils', "%s/naseqpack/rnaseq_utils.py" % (pypack) )

from rnaseq_utils import *

class ScanConfigRnaSeq(ScanConfig, object):

    def __init__(self):

        super(ScanConfigRnaSeq, self).__init__()

    @property
    def variants2_dir(self):
        return os.path.join(self.proj_dir, "variants2")



# --------------------------------------------------- ASSEMBLY
#     @property
#     def ref_gen_assembly(self):
#         return "ucscHg19"
        # return "enGRCh37"

    @property
    def adapt_fa(self):
        # return  os.path.join(self.supl_dir, "adapters-nextera.fa")
        return os.path.join(self.naseq_home_dir, "supl/adapters-nextera.fa")

        # cls.conf.adapt_fa = os.path.join(cls.conf.supl_dir, "adapt-ngopt.fa")
        # cls.conf.adapt_fa = os.path.join(cls.conf.supl_dir, "adapt-nextera-ngopt.fa")



    @property
    def ref_gen_fa(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37sp/sequence/genome_fa/enGRCh37sp.fa")
        return self.ref_gen_genome_fa

    @property
    def ref_gen_chrominfo(self):
        # return os.path.join(self.raglab_install_home,
        #                     "igenomes/enGRCh37/sequence/genome_fa/enGRCh37.chrominfo.txt")
        return self.ref_gen_genome_chrominfo

    @property
    def gen_idx(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/sequence/bt2_ix/enGRCh37")
        return self.ref_gen_bt2_ix


    @property
    def tra_idx(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37/transcr/enGRCh37_genes")
        return self.ref_gen_transcr_ix




    # @property
    # def tra_bed12(self):
    #     return os.path.join(self.raglab_install_home,
                                          # "igenomes/enGRCh37sp/annotation/genes_gtf/enGRCh37v75sp_genes_nonzero.bed")
        # return self.conf.ref_transcr_nz_bed12




    def scan_local(self):
        print("in ScanConfigRnaSeq scan_local...")
        # conf.orig_dir = os.path.join(conf.proj_dir, "orig")
        super(ScanConfigRnaSeq, self).scan_local()
        return self

    def set_proj_dirs(self):
        pass


    def set_proj_files_path_names(self):
        pass


        #self.lcnf.init_matrix_txt = os.path.join(self.conf.align2_dir,"initMatrix.txt")
        #cls.init_matrix_txt = os.path.join(os.getenv('RAGLAB_INSTALL_HOME', ""),
        #                "genomes/species/Homo_sapiens/GRCh37_spiked/orig_sequence","initMatrix.txt")
        #print "cls.init_matrix_txt: ", cls.init_matrix_txt

        #self.lcnf.init_matrix_txt = os.path.join(self.conf.align2_dir,"initMatrix.txt")
        #self.lcnf.init_matrix_txt = os.path.join(os.getenv('RAGLAB_INSTALL_HOME', ""),"genomes/species/Homo_sapiens/GRCh37_spiked/orig_sequence","initMatrix.txt")
        #print "self.lcnf.init_matrix_txt: ",self.lcnf.init_matrix_txt
        
          

    # @classmethod
    # def load_sample_lanes_old(cls):
    #
    #     cls.conf.sample_lanes = []
    #     ifile  = open(os.path.join(cls.conf.conf_dir, 'all-sample-info.csv'), "rb")
    #     reader = csv.DictReader(ifile, delimiter=',')
    #     for line in reader:
    #         cls.conf.sample_lanes.append(line)
    #     #print "sample_lanes len: ", cls.conf.sample_lanes.__len__()
    #     ifile.close()
    #
    #     #print "cls.conf.sample_lanes: ", cls.conf.sample_lanes
    #     return cls.conf
    

    def load_sample_lanes(self):
        print(("self.conf_dir: ", self.conf_dir))
        conf_csv = os.path.join(self.conf_dir, "all-sample-info.csv")


        gr8 = pd.read_csv(conf_csv, sep=',', header=0, usecols=[0, 1, 3, 5],
                          dtype={'Name': object, 'Library Barcode': object, 'Run': object, 'Region': object})
        gr8.rename(columns={'Library Barcode': 'Library'}, inplace=True)
        
        self.sample_lanes = gr8
        
        print(("gr8: ", gr8))
        
      
        #print "cls.conf.sample_lanes: ", cls.conf.sample_lanes
        return self
    
    
    def load_sample_names(self):
        
        gr8 = pd.read_csv(os.path.join(self.conf_dir, 'one-sample-info.csv'),
               sep=',', 
               header = 0,
               index_col=[0]
               #usecols=[0,1]
               )
        
        #print "gr8: ", gr8
        self.sample_names = gr8
        
        return self




