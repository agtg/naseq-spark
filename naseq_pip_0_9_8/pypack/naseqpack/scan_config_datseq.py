# -*- coding: utf-8 -*-
"""
Created on Wed Nov 12 15:24:32 2014

@author: root
"""

from scan_config_naseq import ScanConfig

import os 
import csv
import numpy as np
import pandas as pd


# from treedict import TreeDict


class ScanConfigDatSeq(ScanConfig, object):


    @property
    def depl_dir(self):
        return self.__depl_dir

    @depl_dir.setter
    def depl_dir(self, val):
        self.__depl_dir = val


    @property
    def subm_dir(self):
        loc_dir = os.path.join(self.proj_dir, "subm")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def job_output_dir(self):
        loc_dir = os.path.join(self.subm_dir, "job_output")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    def scan_local(self):
        print("in ScanConfigDnaSeq scan_local...")
        # conf.orig_dir = os.path.join(conf.proj_dir, "orig")
        super(ScanConfigDatSeq, self).scan_local()
        return self
        


    @property
    def valid_smps_csv(self):
        return os.path.join(self.conf_dir, "valid_smps.csv")

    @property
    def invalid_smps_csv(self):
        return os.path.join(self.conf_dir, "invalid_smps.csv")


    def load_htchip_colnames(self):

        htchip_colnames_csv = os.path.join(self.conf_dir, "htchip_colnames.csv")
        # dtype={'idx': np.int8, 'col_idx': object, 'col_name': object})
        #gr8.rename(columns={'Library Barcode': 'Library'}, inplace=True)


        if os.path.exists(htchip_colnames_csv):

            self.htchip_colnames_df = pd.read_csv(htchip_colnames_csv, sep=',',
                                                  header=0,
                                                  index_col=[0],
                                                  # usecols=[0, 1, 2],
                              dtype={'idx': np.int8, 'col_idn': object, 'col_name': object})
        else:
            self.htchip_colnames_df = None

        return self
        
