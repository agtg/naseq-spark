#!/usr/bin/env python

import subprocess
import optparse
import re
import os
import csv
import sys, random, itertools
# import HTSeq
import shutil

import numpy as np
import pandas as pd

# from treedict import TreeDict
import redis
import time

import socket
import json

FASTQC="""
 %(bin_dir)s/FastQC/fastqc -o %(fastqc_dir)s --extract \
 -k 8 \
-a %(adapters)s \
-contaminants %(contaminants)s \
-f fastq %(p1)s %(p2)s"""

#  ILLUMINACLIP:%(adapter)s:2:30:15:8:true \
#  HEADCROP:12 TRAILING:30 MINLEN:32 \
#ILLUMINACLIP:<fastaWithAdaptersEtc>:<seed mismatches>:<palindrome clip threshold>:<simple clip threshold>:<minAdapterLength>:<keepBothReads>
TRIM="""
java -XX:ParallelGCThreads=1 -Xmx2G -cp $TRIMMOMATIC_JAR \
  org.usadellab.trimmomatic.TrimmomaticPE -threads 6 -phred33 \
  %(p1)s \
  %(p2)s \
  %(rp1)s \
  %(rs1)s \
  %(rp2)s \
  %(rs2)s \
  ILLUMINACLIP:%(adapter)s:4:30:30:8:true \
  HEADCROP:18 TRAILING:30 MINLEN:80 \
  > %(trim_out)s
"""

PIGZ="""
pigz %(p1)s %(p2)s"""

#specify this only once
#-G %(align_bt2)s/hg1k.gtf \
TOPHAT="""
tophat --rg-library "MPS073409" --rg-platform "ILLUMINA" --rg-platform-unit "1" \
--rg-center "McGill University and Genome Quebec Innovation Centre" \
--rg-sample %(sample)s \
--rg-id %(run)s --library-type fr-unstranded \
-o %(align_dir)s -p 12 \
--transcriptome-index %(align_bt2)s/hg1k.gtf.idx \
--b2-very-sensitive \
%(align_bt2)s/hg1k \
%(p1)s \
%(p2)s > %(out)s
"""

BAM_TO_FASTQ="""
java -Djava.io.tmpdir=/gs/scratch/dbadescu -XX:ParallelGCThreads=7 \
    -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx40G -jar \
    $PICARD_HOME/SamToFastq.jar \
    INPUT=%(bam)s \
    VALIDATION_STRINGENCY=LENIENT \
    FASTQ=%(p1)s \
    SECOND_END_FASTQ=%(p2)s
"""

BAM_TO_FASTQ2="""
bam2fastx -q -Q -A -P -o %(fastx)s %(bam)s
"""



BWA="""
bwa mem  -M -t 12 -R \
'@RG\tID:lib0002_X_X\tSM:sampleB\tLB:lib0002\tPU:runX_X\tCN:McGill University and Genome Quebec Innovation Center\tPL:Illumina' \
/software/areas/genomics/genomes/Homo_sapiens/hg1k_v37_withSpikes/fasta/bwa/hg1k_v37.fasta \
    %(p1)s %(p2)s \
 | java -Djava.io.tmpdir=/gs/scratch/dbadescu -XX:ParallelGCThreads=4 -Xmx25G -jar ${PICARD_HOME}/SortSam.jar  \
    INPUT=/dev/stdin \
    CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate \
    OUTPUT=%(s_bwa_sorted)s \
    MAX_RECORDS_IN_RAM=3750000 > %(out)s
"""

EXTRACT_SOFTCLIPS="""
java -jar $BVATOOLS_JAR extractsclip \
--bam %(bam)s \
--prefix %(bam)s
"""

# FASTQ="""
# ../FastQC/fastqc -o ../fastqc --extract \
# -a ../adapters/adapters-nextera.txt \
# -contaminants ../adapters/adapters-nextera.txt \
# -f fastq %(p1)s %(p2)s"""



# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    pass
