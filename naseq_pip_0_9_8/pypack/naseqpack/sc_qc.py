
# from mixin import Mixin, mixin

import subprocess
import jinja2
from naseq_utils import *

import http
import json
import pprint

import requests
import pandas as pd
import numpy as np
from scipy import stats as stats

import json
import itertools
import shutil
import os
import gzip
from math import ceil


from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
from pyspark.sql.types import StructField, StructType, LongType, IntegerType, StringType, FloatType
# from pyspark.sql import Column
from pyspark.sql.functions import lit
from pyspark.sql.functions import Column

from ast import literal_eval


from functools import reduce  # For Python 3.x
from pyspark.sql import DataFrame


# spark is an existing SparkSession.
from pyspark.sql import Row

from pyspark.sql.functions import lit

from pyspark.sql import functions as pyspfunc
from pyspark.sql.types import *


# import binning as bn
import re
# import binning as bn
import re

import json
from functools import partial
import pprint as pp

import GTF_ENCODE                      # https://gist.github.com/slowkow/8101481

import pyarrow as pa
import pyarrow.parquet as pq

import binning
import sys
import msgpack
import json
import pathlib

import pyarrow.orc as orc

# with composition
# class NaseqServers(Mixin):


class ScQc(object):

    def __init__(self, ctx):
        self._ctx = ctx
        # defaults to each host a name
        scqc3_conf_data = open(os.path.join(self._ctx.conf.conf_dir,"scqc3_conf.json")).read()
        self.scqc3_conf = json.loads(scqc3_conf_data)

    @property
    def esnv_min_vaf(self):
        return self.__esnv_min_vaf

    @esnv_min_vaf.setter
    def esnv_min_vaf(self, val):
        self.__esnv_min_vaf = val

    @property
    def esnv_min_nb_blk(self):
        return self.__esnv_min_nb_blk

    @esnv_min_nb_blk.setter
    def esnv_min_nb_blk(self, val):
        self.__esnv_min_nb_blk = val

    @property
    def esnv_min_nb_scs(self):
        return self.__esnv_min_nb_scs

    @esnv_min_nb_scs.setter
    def esnv_min_nb_scs(self, val):
        self.__esnv_min_nb_scs = val

    @property
    def expr_vaf_mtx_f(self):
        return os.path.join(
            self._ctx.conf.matr_dir,
            "%s.expr_vaf_mtx_%s_%s.csv"
            % (self._ctx.conf.proj_abbrev,
               self.esnv_min_vaf,
               self.esnv_min_nb_scs)
        )


    @staticmethod
    def myFunc(vcfrow):

        names = vcfrow[1].split("\t")
        vals = vcfrow[0].split("\t")

        res = []
        rowvals = []


        for regidx in range(7):
            rowvals.append(vals[regidx])

        info = vals[7]
        format = vals[8]


        rowvals.append(info)
        rowvals.append(format)

        format_arr = format.split(":")
        format_len = format_arr.__len__()




        for smpidx in range(9, len(vals)):
            # sample name
            rowvals2 = rowvals.copy()

            rowvals2.append(names[smpidx])
            # sample value
            gts = vals[smpidx]
            gts_arr = gts.split(":")
            gts_len = gts_arr.__len__()
            # extract format fields
            # fmt_dict = OrderedDict()
            fmt_dict = {}
            # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
            for j in range(format_len):
                fmtkey = format_arr[j].lower()
                fmtval = gts_arr[j]
                # print(fmtkey, fmtval)
                fmt_dict[fmtkey] = fmtval

            rowvals2.append(fmt_dict)
            # rowvals2.append(vals[smpidx])

            # output row
            res.extend([rowvals2])

        # for i in range(len(names)):
        #     res.append([names[i], vals[i]])

        # words2 = words[9:]
        # len = words2.__len__

        return res
        # return [len(words), 2]
        # return [[0, "smp1", 5], [1, "smp2", 3]]

    @staticmethod
    def worker_func(vcfrow, colnames):

        vals = vcfrow.split("\t")
        names = colnames.value

        # names = vcfrow[1].split("\t")
        # vals = vcfrow[0].split("\t")

        res = []
        rowvals = []


        for regidx in range(7):
            rowvals.append(vals[regidx])

        info = vals[7]
        # format = vals[8]


        # rowvals.append(info)

        info_arr = info.split(";")
        info_dict = {}
        for j in range(len(info_arr)):
            score_key_arr = info_arr[j].split("=")
            if len(score_key_arr)==2:
                fmtkey = score_key_arr[0].lower()
                # fmtkey = score_key_arr[0]
                fmtval = score_key_arr[1]
                # print(fmtkey, fmtval)
                info_dict[fmtkey] = fmtval

        rowvals.append(info_dict)

        # output row
        res.extend([rowvals])

        # format_arr = format.split(":")
        # format_len = format_arr.__len__()

        # for smpidx in range(9, len(vals)):
            # sample name
            # rowvals2 = rowvals.copy()
            #
            # rowvals2.append(names[smpidx])
            # sample value
            # gts = vals[smpidx]
            # gts_arr = gts.split(":")
            # gts_len = gts_arr.__len__()
            # extract format fields
            # fmt_dict = OrderedDict()
            # fmt_dict = {}
            # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
            # for j in range(format_len):
            #     fmtkey = format_arr[j].lower()
            #     fmtval = gts_arr[j]
            #     print(fmtkey, fmtval)
                # fmt_dict[fmtkey] = fmtval

            # rowvals2.append(fmt_dict)
            # rowvals2.append(vals[smpidx])

            # output row
            # res.extend([rowvals2])

        # for i in range(len(names)):
        #     res.append([names[i], vals[i]])

        # words2 = words[9:]
        # len = words2.__len__

        return res
        # return [len(words), 2]
        # return [[0, "smp1", 5], [1, "smp2", 3]]



        # return [m.group(1) for m in (re.search(regex, l) for l in lines) if m]
        # return [m.group(1) for l in lines for m in [regex.search(l)] if m]

    # http://stackoverflow.com/questions/2436607/how-to-use-re-match-objects-in-a-list-comprehension
    # def filterPick(lines, regex):
    #     matches = map(re.compile(regex).match, lines)
    #     return [m.group(1) for m in matches if m]

        # return [m.group(1) for m in (re.search(regex, l) for l in lines) if m]
        # return [m.group(1) for l in lines for m in [regex.search(l)] if m]

    @staticmethod
    def wk_snv_smp_gts2(vcfrow, colnames):

        vals = vcfrow.split("\t")
        names = colnames.value
        # names2 = names

        # cler = "HC"
        # pat = "^(.+)-(%s)$" % cler
        # pat = "^(.+)-(.+)$"
        # matches = map(re.compile(pat).match, names)
        # names2 = [[m.group(1), m.group(2)] for m in matches if m]
        # matches = map(re.compile(pat).match, names)
        # clers2 = [m.group(2) for m in matches if m]


        # names = vcfrow[1].split("\t")
        # vals = vcfrow[0].split("\t")

        res = []
        rowvals = []


        for regidx in range(7):
            rowvals.append(vals[regidx])

        # info = vals[7]
        format = vals[8]


        # rowvals.append(info)

        # info_arr = info.split(";")
        # info_dict = {}
        # for j in range(len(info_arr)):
        #     score_key_arr = info_arr[j].split("=")
        #     if len(score_key_arr)==2:
        #         fmtkey = score_key_arr[0].lower()
        #         fmtkey = score_key_arr[0]
                # fmtval = score_key_arr[1]
                # print(fmtkey, fmtval)
                # info_dict[fmtkey] = fmtval
        #
        # rowvals.append(info_dict)

        # output row
        # res.extend([rowvals])

        format_arr = format.split(":")
        format_dict = {}
        format_len = format_arr.__len__()

        # would be 0 = 9 for all colnames
        # for smpidx in range(9, len(vals)):
        for smpidx in range(0, len(names)):
            # sample name
            rowvals2 = rowvals.copy()

            rowvals2.append(names[smpidx])
            rowvals2.append("HC")
            # sample value
            gts = vals[9 + smpidx]
            gts_arr = gts.split(":")
            gts_len = gts_arr.__len__()
            # extract format fields
            # fmt_dict = OrderedDict()
            fmt_dict = {}
            # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
            for j in range(format_len):
                fmtkey = format_arr[j].lower()
                fmtval = gts_arr[j]
            #     print(fmtkey, fmtval)
                fmt_dict[fmtkey] = fmtval

            rowvals2.append(fmt_dict)
            # rowvals2.append(vals[smpidx])

            #
            gtval = -2
            vaf = -1.0
            dp = 0.0 if fmt_dict['dp'] in ['.'] else float(fmt_dict['dp'])
            # dp = float(fmt_dict['dp'])
            ad = fmt_dict['ad'].rstrip().split(",")
            ad_a = float(ad[0])
            ad_b = float(ad[1])

            gt = fmt_dict['gt']
            if gt in ['./.', '0/.', './0', '1/.', './1']:
                gtval = -1
            elif gt in ['0/0']:
                gtval = 0
                vaf = 0.0
            elif gt == "1/0":
                gtval = 1
                if dp != 0.0:
                    vaf = float(ad_a/dp)
            elif gt == "0/1":
                gtval = 1
                if dp != 0.0:
                    vaf = float(ad_b/dp)
            elif gt in ['1/1']:
                gtval = 2
                vaf = 1.0

            rowvals2.append(gtval)
            rowvals2.append(vaf)
            rowvals2.append(dp)
            # rowvals2.append(ad)
            rowvals2.append(ad_a)
            rowvals2.append(ad_b)

            # output row
            res.extend([rowvals2])

        return res


    @staticmethod
    def wk_snv_smp_gts(vcfrow, colnames):

        vals = vcfrow.split("\t")
        names = colnames.value

        cler = "HC"
        # pat = "^(.+)-(%s)$" % cler
        pat = "^(.+)-(.+)$"
        matches = map(re.compile(pat).match, names)
        names2 = [[m.group(1), m.group(2)] for m in matches if m]
        # matches = map(re.compile(pat).match, names)
        # clers2 = [m.group(2) for m in matches if m]


        # names = vcfrow[1].split("\t")
        # vals = vcfrow[0].split("\t")

        res = []
        rowvals = []


        for regidx in range(7):
            rowvals.append(vals[regidx])

        # info = vals[7]
        format = vals[8]


        # rowvals.append(info)

        # info_arr = info.split(";")
        # info_dict = {}
        # for j in range(len(info_arr)):
        #     score_key_arr = info_arr[j].split("=")
        #     if len(score_key_arr)==2:
        #         fmtkey = score_key_arr[0].lower()
        #         fmtkey = score_key_arr[0]
                # fmtval = score_key_arr[1]
                # print(fmtkey, fmtval)
                # info_dict[fmtkey] = fmtval
        #
        # rowvals.append(info_dict)

        # output row
        # res.extend([rowvals])

        format_arr = format.split(":")
        format_dict = {}
        format_len = format_arr.__len__()

        # would be 0 = 9 for all colnames
        # for smpidx in range(9, len(vals)):
        for smpidx in range(0, len(names2)):
            # sample name
            rowvals2 = rowvals.copy()

            rowvals2.append(names2[smpidx][0])
            rowvals2.append(names2[smpidx][1])
            # sample value
            gts = vals[9 + smpidx]
            gts_arr = gts.split(":")
            gts_len = gts_arr.__len__()
            # extract format fields
            # fmt_dict = OrderedDict()
            fmt_dict = {}
            # [("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])
            for j in range(format_len):
                fmtkey = format_arr[j].lower()
                fmtval = gts_arr[j]
            #     print(fmtkey, fmtval)
                fmt_dict[fmtkey] = fmtval

            rowvals2.append(fmt_dict)
            # rowvals2.append(vals[smpidx])

            #
            gtval = -2
            vaf = -1.0
            dp = 0.0 if fmt_dict['dp'] in ['.'] else float(fmt_dict['dp'])
            # dp = float(fmt_dict['dp'])
            ad = fmt_dict['ad'].rstrip().split(",")
            ad_a = float(ad[0])
            ad_b = float(ad[1])

            gt = fmt_dict['gt']
            if gt in ['./.', '0/.', './0', '1/.', './1']:
                gtval = -1
            elif gt in ['0/0']:
                gtval = 0
                vaf = 0.0
            elif gt == "1/0":
                gtval = 1
                if dp != 0.0:
                    vaf = float(ad_a/dp)
            elif gt == "0/1":
                gtval = 1
                if dp != 0.0:
                    vaf = float(ad_b/dp)
            elif gt in ['1/1']:
                gtval = 2
                vaf = 1.0

            rowvals2.append(gtval)
            rowvals2.append(vaf)
            rowvals2.append(dp)
            # rowvals2.append(ad)
            rowvals2.append(ad_a)
            rowvals2.append(ad_b)

            # rowvals2.append(names2)
            # output row
            res.extend([rowvals2])

        # for i in range(len(names)):
        #     res.append([names[i], vals[i]])

        # words2 = words[9:]
        # len = words2.__len__

        return res
        # return [len(words), 2]
        # return [[0, "smp1", 5], [1, "smp2", 3]]

    def spark_parse_dna_snvs(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        lines2 = sc.textFile(os.path.join(self._ctx.conf.proj_dir, "exome_vcf", "exome_pdxs.vcf.gz"))


        lines = lines2.repartition(500)

        bodyf = lines.filter(lambda line: not re.search(r"^#", line))
        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        colnames = headerf.collect()[0]

        names = colnames.split("\t")
        names[0] = "CHROM"
        print("names: \n", names)

        V = sc.broadcast(names)
        print(V.value)

        parts2 = bodyf.flatMap(partial(self.worker_func, colnames=V))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        # outfolder = os.path.join(self.conf.proj_dir, "srvf/filef", "rna_snvs")
        # if os.path.exists(outfolder):
        #     shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # mytx.write.csv(outfolder)

        # mytx.coalesce(1).rdd.map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        # parts2.coalesce(1).map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)
        # exit(0)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()

        parts_ds = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                          ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                          info=p[7]))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)


        parts_df = spark.createDataFrame(parts_ds)
        print("size: ", parts_df.count())
        parts_df.show()
        # spark.sql("drop table if exists vcf_out")
        # parts_df.write.saveAsTable("vcf_out")

        # read the initial dataframe without index
        # dfNoIndex = sc.read.parquet(dataframePath)
        # Need to zip together with a unique integer

        # lambda (x, y): x + y
        # lambda x_y: x_y[0] + x_y[1]
        # First create a new schema with uuid field appended
        newSchema = StructType([StructField("snv_id", IntegerType(), False)] + parts_df.schema.fields)
        # zip with the index, map it to a dictionary which includes new field
        df = parts_df.rdd.zipWithIndex()\
            .map(lambda row_id: dict(itertools.chain(row_id[0].asDict().items(), [("snv_id", row_id[1])])))\
            .toDF(newSchema)

        spark.sql("drop table if exists dna_snvs")
        df.write.saveAsTable("dna_snvs")

    def spark_parse_dna_snv_smps(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        lines2 = sc.textFile(os.path.join(self._ctx.conf.proj_dir, "exome_vcf", "exome_pdxs.vcf.gz"))

        lines = lines2.repartition(500)

        bodyf = lines.filter(lambda line: not re.search(r"^#", line))
        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        colnames = headerf.collect()[0]

        # header2 = lines.filter(lambda line: re.search(r"^##INFO", line))
        # header3 = header2.collect()
        # for i in range(len(header3)):
        #     print(header3[i])

        names = colnames.split("\t")
        names[0] = "CHROM"
        names = names[9:]
        print("names: \n", names)

        # exit(0)

        V = sc.broadcast(names)
        print(V.value)

        parts2 = bodyf.flatMap(partial(self.wk_snv_smp_gts2, colnames=V))

        zz = parts2.take(10)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        # exit(0)
        # outfolder = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "rna_snv_smps")
        # if os.path.exists(outfolder):
        #     shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # parts2.write.csv(outfolder)

        # parts2.coalesce(1).map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        # parts2.coalesce(1).map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)
        #
        # exit(0)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()

        parts_ds = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                            ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                            smp_name=p[7],cler=p[8],
                                            gt_map=p[9], gts=p[10], vaf=float(p[11]), dp=float(p[12]),
                                            ada=float(p[13]), adb=float(p[14])
                                            ))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)


        parts_df = spark.createDataFrame(parts_ds)
        print("size: ", parts_df.count())
        parts_df.show()
        spark.sql("drop table if exists dna_snv_smps")
        parts_df.write.saveAsTable("dna_snv_smps")


    def spark_parse_rna_snvs(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a038-HC.dbnsfp.vcf"))
        # lines2 = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a035-HC.dbnsfp.vcf.gz"))
        # lines2 = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a029-HC.dbnsfp.vcf.gz"))
        # print("dbnsf_file: ", self._ctx.dbnsfp_cf_depl_file)
        lines2 = sc.textFile(self._ctx.dbnsfp_cf_depl_file)


        lines = lines2.repartition(500)

        bodyf = lines.filter(lambda line: not re.search(r"^#", line))
        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        colnames = headerf.collect()[0]

        # header2 = lines.filter(lambda line: re.search(r"^##INFO", line))
        # header3 = header2.collect()
        # for i in range(len(header3)):
        #     print(header3[i])

        names = colnames.split("\t")
        names[0] = "CHROM"
        print("names: \n", names)

        # exit(0)

        V = sc.broadcast(names)
        print(V.value)

        parts2 = bodyf.flatMap(partial(self.worker_func, colnames=V))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        # outfolder = os.path.join(self.conf.proj_dir, "srvf/filef", "rna_snvs")
        # if os.path.exists(outfolder):
        #     shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # mytx.write.csv(outfolder)

        # mytx.coalesce(1).rdd.map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        # parts2.coalesce(1).map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)
        # exit(0)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()

        parts_ds = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                          ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                          info=p[7]))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)


        parts_df = spark.createDataFrame(parts_ds)
        print("size: ", parts_df.count())
        parts_df.show()
        # spark.sql("drop table if exists vcf_out")
        # parts_df.write.saveAsTable("vcf_out")

        # read the initial dataframe without index
        # dfNoIndex = sc.read.parquet(dataframePath)
        # Need to zip together with a unique integer

        # lambda (x, y): x + y
        # lambda x_y: x_y[0] + x_y[1]
        # First create a new schema with uuid field appended
        newSchema = StructType([StructField("snv_id", IntegerType(), False)] + parts_df.schema.fields)
        # zip with the index, map it to a dictionary which includes new field
        df = parts_df.rdd.zipWithIndex()\
            .map(lambda row_id: dict(itertools.chain(row_id[0].asDict().items(), [("snv_id", row_id [1])])))\
            .toDF(newSchema)

        spark.sql("drop table if exists rna_snvs")
        df.write.saveAsTable("rna_snvs")

    def spark_parse_rna_snv_smps(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # lines = sc.textFile(os.path.join(self.conf.proj_dir,"srvf/filef", "a038-HC.dbnsfp.vcf"), minPartitions=128)
        # lines2 = sc.textFile( os.path.join(self.conf.proj_dir,"srvf/filef", "a035-HC.dbnsfp.vcf.gz"))
        # lines2 = sc.textFile( os.path.join(self.conf.proj_dir,"srvf/filef", "a029-HC.dbnsfp.vcf.gz"))
        lines2 = sc.textFile(self._ctx.dbnsfp_cf_depl_file)

        lines = lines2.repartition(500)

        bodyf = lines.filter(lambda line: not re.search(r"^#", line))
        headerf = lines.filter(lambda line: re.search(r"^#CHROM", line))
        colnames = headerf.collect()[0]

        # header2 = lines.filter(lambda line: re.search(r"^##INFO", line))
        # header3 = header2.collect()
        # for i in range(len(header3)):
        #     print(header3[i])

        names = colnames.split("\t")
        names[0] = "CHROM"
        names = names[9:]
        print("names: \n", names)

        # exit(0)

        V = sc.broadcast(names)
        print(V.value)

        parts2 = bodyf.flatMap(partial(self.wk_snv_smp_gts, colnames=V))

        zz = parts2.take(10)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        # exit(0)
        outfolder = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "rna_snv_smps")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # parts2.write.csv(outfolder)

        # parts2.coalesce(1).map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        # parts2.coalesce(1).map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)
        #
        # exit(0)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()

        parts_ds = parts2.map(lambda p: Row(chrom=p[0], pos=p[1], id=p[2],
                                            ref=p[3], alt=p[4], qual=p[5], filter=p[6],
                                            smp_name=p[7],cler=p[8],
                                            gt_map=p[9], gts=p[10], vaf=float(p[11]), dp=float(p[12]),
                                            ada=float(p[13]), adb=float(p[14])
                                            ))

        # zz = parts2.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)


        parts_df = spark.createDataFrame(parts_ds)
        print("size: ", parts_df.count())
        parts_df.show()
        spark.sql("drop table if exists rna_snv_smps")
        parts_df.write.saveAsTable("rna_snv_smps")

        # exit(0)
        # read the initial dataframe without index
        # dfNoIndex = sc.read.parquet(dataframePath)
        # Need to zip together with a unique integer

        # lambda (x, y): x + y
        # lambda x_y: x_y[0] + x_y[1]
        # First create a new schema with uuid field appended
        # newSchema = StructType([StructField("snv_id", IntegerType(), False)] + parts_df.schema.fields)
        # zip with the index, map it to a dictionary which includes new field
        # df = parts_df.rdd.zipWithIndex()\
        #     .map(lambda row_id: dict(itertools.chain(row_id[0].asDict().items(), [("snv_id", row_id [1])])))\
        #     .toDF(newSchema)

        # spark.sql("drop table if exists vcf_out_ids")
        # df.write.saveAsTable("vcf_out_ids")


    def spark_parse_chrom_cnvrt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        sc_grp_f = os.path.join(self._ctx.conf.conf_dir, "sc-grp-sample-info.csv")

        chrom_cnvrt_f = os.path.join(self._ctx.conf.proj_dir, "exome_vcf", "chrom_cnvrt.csv")
        chrom_cnvrt_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(chrom_cnvrt_f)

        zz = chrom_cnvrt_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists chrom_cnvrt")
        chrom_cnvrt_df.write.saveAsTable("chrom_cnvrt")

        print(chrom_cnvrt_df.schema.fields)


    def spark_register_pdx_ex_vdt(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        table_name = self.scqc3_conf["pdx_ex"]

        dest_table_name = "pdx_exo_sel_snvs"
        table_path = "/gs/project/wst-164-aa/share/rnvar/a040-x067-mm-scs/exome_vcf/%s" % table_name
        tbl = spark.read.parquet(table_path)
        spark.sql("drop table if exists %s" % dest_table_name)
        tbl.write.saveAsTable(dest_table_name)

    def spark_save_pdx_ex_vdt(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        for i in [["pdx1735ex", "20160212_Paul1735PDX_Unsorted"],
                  ["pdx1876ex", "20160509_Paul_1876_Unsorted"],
                  ["pdxhci001ex", "20160518_Paul_HCI001_Unsorted"],
                  ["pdxbcm2665ex", "20160429_Paul_BCM2665_Unsorted"]]:

            out_f = os.path.join(self._ctx.conf.proj_dir, "exome_vcf", i[0])
            if os.path.exists(out_f):
                shutil.rmtree(out_f, ignore_errors=False, onerror=None)

            spark.sql("drop table if exists %s" % i[0])

            # create table %s as

            cmd = r"""
            select distinct chrom_hg19 as chrom,
            	dss.pos,
            	dss.ref,
            	dss.alt,
            	dss.vaf
            	from dna_snv_smps dss
            	join chrom_cnvrt cc on cc.chrom_ens = dss.chrom
            	where smp_name = '%s' and
            	vaf > 0"""\
                  % (i[1])
            print("cmd: ", cmd)
            res_df = spark.sql(cmd)

            res_df.write.parquet(out_f)
            # also in database
            res_df.write.saveAsTable(i[0])

            print(res_df)

        for i in [["pdx1735vdt", "pdx1735ex"],
                  ["pdx1876vdt", "pdx1876ex"],
                  ["pdxhci001vdt", "pdxhci001ex"],
                  ["pdxbcm2665vdt", "pdxbcm2665ex"]]:

            out_f = os.path.join(self._ctx.conf.proj_dir, "exome_vcf", i[0])
            if os.path.exists(out_f):
                shutil.rmtree(out_f, ignore_errors=False, onerror=None)

            spark.sql("drop table if exists %s" % i[0])

            cmd = r"""select chrom,pos,ref,alt
            from %s pdxe
            except
            select chrom,pos,ref,alt
            from rna_snvs"""\
                  % (i[1])
            print("cmd: ", cmd)
            res_df = spark.sql(cmd)

            res_df.write.parquet(out_f)
            # also in database
            res_df.write.saveAsTable(i[0])

            print(res_df)

    def spark_parse_sc_grp(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        sc_grp_f = os.path.join(self._ctx.conf.conf_dir, "sc-grp-sample-info.csv")
        sc_grp_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(sc_grp_f)

        zz = sc_grp_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists sc_grp")
        sc_grp_df.write.saveAsTable("sc_grp")

        print(sc_grp_df.schema.fields)

    def spark_parse_ctl_grps(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        ctl_grps_f = os.path.join(self._ctx.conf.conf_dir, "ctl-grps.csv")
        ctl_grps_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(ctl_grps_f)

        zz = ctl_grps_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists ctl_grps")
        ctl_grps_df.write.saveAsTable("ctl_grps")

        print(ctl_grps_df.schema.fields)

    def spark_parse_csv(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_f = os.path.join(self._ctx.conf.conf_dir, "%s.csv" % tbl_name)
        tbl_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(tbl_f)

        zz = tbl_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl_df.write.saveAsTable(tbl_name)

        print(tbl_df.schema.fields)

    def spark_import_simple_csv(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "%s.csv" % tbl_name)
        tbl_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(tbl_f)

        zz = tbl_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl_df.write.format("orc").saveAsTable(tbl_name)

        print(tbl_df.schema.fields)

    def spark_import_simple_json(self, tbl_name):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "%s.json" % tbl_name)
        tbl_df = spark.read.format("json").load(tbl_f)

        zz = tbl_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl_df.write.saveAsTable(tbl_name)
        #
        print(tbl_df.schema.fields)

    def spark_export_simple_csv(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", tbl_name)
        csv_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "%s.csv" % tbl_name)

        if os.path.exists(out_f):
            shutil.rmtree(out_f, ignore_errors=False, onerror=None)

         # create table %s as

        cmd = r"""select * from %s""" % (tbl_name)
        print("cmd: ", cmd)

        res_df = spark.sql(cmd)

        rdd0 = [res_df.columns]
        # [["gene_id", "gene_name"]]
        a = sc.parallelize(rdd0).map(lambda x_y: ",".join(map(str, x_y))).repartition(1)
        # order by first column regex
        b = res_df.rdd.map(lambda x_y: ",".join(map(str, x_y))).repartition(1).sortBy(lambda line: re.search(r"""^(.+),""", line).group(1))
        # wp2.map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)

        a.union(b).coalesce(1).saveAsTextFile(out_f)
        shutil.copy(os.path.join(out_f, "part-00000"), csv_f)
        # remove temp parallel folder
        if os.path.exists(out_f):
            shutil.rmtree(out_f, ignore_errors=False, onerror=None)


        # res_df.write.csv(out_f)

        # res_df.coalesce(1).write.csv(out_f)
        # shutil.copy(os.path.join(out_f, "part-00000"), csv_f)


    def spark_export_parquet(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", tbl_name)
        if os.path.exists(out_f):
            shutil.rmtree(out_f, ignore_errors=False, onerror=None)

         # create table %s as

        cmd = r"""select * from %s""" % (tbl_name)
        print("cmd: ", cmd)

        res_df = spark.sql(cmd)

        res_df.write.parquet(out_f)

    def spark_import_json_file(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        in_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef",  "%s.json" % tbl_name)

        tbl = spark.read.json(in_f)
        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.saveAsTable(tbl_name)

        tbl.printSchema()

    def spark_import_parquet(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        in_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", tbl_name)

        tbl = spark.read.parquet(in_f)
        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.saveAsTable(tbl_name)

    def spark_import_orc(self, tbl_name):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        in_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", tbl_name)

        tbl = spark.read.orc(in_f)
        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.saveAsTable(tbl_name)

    def spark_create_gene_filter(self, gene_filter):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # create filter table on intersection with general tracking ids
        spark.sql("drop table if exists flt_mtx_ids")


        # index for genes for that filter
        cmd = "create table flt_mtx_ids as " \
              "select gti.gene_id " \
              "from %s gti " \
              % gene_filter
        print(cmd)
        spark.sql(cmd)




    # how to loop
    def gmean_exmp(self, gene_filter):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        df = spark.sql(
            "select smp_name,grp_idxs "
            "from meta_smps")

        pandas_df = df.toPandas()
        print("pandas_df: ", pandas_df)

        tbl = df.rdd.collect()
        for row in tbl:
            print("row: ", row.asDict())


        # create filter table on intersection with general tracking ids
        spark.sql("drop table if exists flt_mtx_ids")


        # index for genes for that filter
        cmd = "create table flt_mtx_ids as " \
              "select gti.gene_id " \
              "from gene_trk_ids gti " \
              " join %s fgi on fgi.gene_id = gti.gene_id " \
              "order by gti.gene_id " \
              % gene_filter
        print(cmd)
        spark.sql(cmd)

        # get those values for defining the numpy array size and header
        cmd = "select gene_id from flt_mtx_ids"
        rownames = spark.sql(cmd).toPandas().gene_id.values
        print("rownames: ", rownames, rownames.shape)



        # meta_arr = np.reshape(rownames, (rownames.shape[0], 1))

        # meta_arr = np.empty([rownames.shape[0], 1], dtype=int)
        # meta_arr = np.empty(np.newaxis, dtype=int)


        # create table to append to
        schema = StructType([
                StructField("gene_id", StringType()),
                StructField("readcnt", FloatType(), True),
                StructField("smp_name", StringType(), True),

            ])
        ndf = spark.createDataFrame([], schema=schema)

        spark.sql("drop table if exists meta_flt_smp_cnts")
        ndf.write.mode("overwrite").saveAsTable("meta_flt_smp_cnts")


        tbl = df.rdd.toLocalIterator()
        for row in tbl:
            rdict = row.asDict()
            print(rdict)
            smp_name = rdict["smp_name"]
            grp_idxs = rdict["grp_idxs"]
            print("flds: ", smp_name, grp_idxs)

            # one average per sample
            meta_arr = np.empty(shape=[0, rownames.shape[0]], dtype=np.float)
            print("meta_arr: ", meta_arr, meta_arr.shape)
            # now for each sample index
            for idx in grp_idxs:
                print("idx: ", idx)

                # return readcounts for smpgrp_idx
                # order is important as this
                # should be the same in adding numpy columns later
                cmd = "select fmi.gene_id, " \
                      "       coalesce(gsc.readcnt,0) as readcnt " \
                      "from gene_smp_cnts gsc " \
                      " right outer join flt_mtx_ids fmi on fmi.gene_id = gsc.gene_id " \
                      " join sc_grp sg on sg.smpgrp_name = gsc.smp_name " \
                      "where sg.smpgrp_idx = %s " \
                      "order by fmi.gene_id" \
                      % idx
                print(cmd)
                smp_df = spark.sql(cmd)

                smp_df.show()
                smp_pdf = smp_df.toPandas()
                # correct for log +1
                readcnt_arr = smp_pdf.readcnt.values + 0
                readcnt_arr = readcnt_arr[np.newaxis,:]
                # print("readcnt_arr: \n", readcnt_arr, readcnt_arr.shape)

                meta_arr = np.append(meta_arr, readcnt_arr, axis=0)


            print(smp_name)
            # print(meta_arr)
            # print(meta_arr.shape)


            # HERE WE COMPUTE AVERAGES
            meta_arr = meta_arr + 1
            mn = stats.gmean(meta_arr, axis=0)

            # mn = np.mean(meta_arr, axis=0)

            print(meta_arr, meta_arr.shape)
            print(mn, mn.shape)

            # time.sleep(10)

            # s0 = pd.Series(mn, index=rownames)
            # s0 = pd.Series(mn)
            # , columns=['one', 'two', 'three']

            # print(s0)
            print(np.repeat(1,3))
            z =np.array(np.repeat(smp_name, len(rownames)), dtype=np.dtype(object))
            print(z, z.dtype)
            data = np.zeros((len(rownames),), dtype=[('gene_id', np.dtype(object)),
                                                     ('readcnt', float),
                                                     ('smp_name', np.dtype(object))
                                                     ])
            dt =  pd.DataFrame(data)
            # data[:] = [(1,2.,'Hello'), (2,3.,"World")]

            dt['gene_id'] = rownames
            dt['smp_name'] = z
            dt['readcnt'] = mn.tolist()
            print(data, dt)
            print("dt is available")
            sdf = spark.createDataFrame(dt, schema=ndf.schema).repartition(20)
            print("created sdf")

            sdf.write.mode("append").saveAsTable("meta_flt_smp_cnts")
            print("wrote sdf")
            time.sleep(5)






        # sample2 = rdd.map(lambda x: [x["smp_name"], x["grp_idxs"]])
        # sample2 = rdd.map(lambda x: x.asDict())
        # print("sample2: ", sample2)

        # print([x for x in rdd.toLocalIterator()])


        # print(df.schema)
        # print(df.collect())

        # dfdict = df.collect()
        # for i in dfdict:
        #     print(i)

        # print(df["smp_name"])
        # print(df["grp_idxs"])


        # for j in df["smp_name"]:
        #     print(j)




        arr = np.array([
            [1, 100],
            [2, 200],
            [1, 100],
            [2, 200],
            [1, 100],
            [2, 200]
        ])

        from scipy import stats as scp
        mn = scp.gmean(arr, axis=1)

        print(arr, mn)




    def spark_join_rna_snv_smp_vafs(self, dna_filter=False):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        pp.pprint(self.scqc3_conf)

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # zz = ss_grp_df.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        # 4
        spark.sql("drop table if exists rna_snv_smp_vafs")
        if dna_filter:
            cmd =\
                "create table rna_snv_smp_vafs as "\
                "select rs.snv_id,	rss.smp_name, rss.vaf "\
                "from rna_snv_smps rss "\
                " join rna_snvs rs on rs.chrom = rss.chrom and "\
                "   rs.pos = rss.pos and "\
                "   rs.ref = rss.ref and "\
                "   rs.alt = rss.alt " \
                " join pdx_exo_sel_snvs pess on pess.chrom = rss.chrom and "\
                "   pess.pos = rss.pos and "\
                "   pess.ref = rss.ref and "\
                "   pess.alt = rss.alt"
        else:
            cmd = \
                "create table rna_snv_smp_vafs as " \
                "select rs.snv_id,	rss.smp_name, rss.vaf " \
                "from rna_snv_smps rss "\
                " join rna_snvs rs on rs.chrom = rss.chrom and "\
                "   rs.pos = rss.pos and "\
                "   rs.ref = rss.ref and "\
                "   rs.alt = rss.alt"


        print("4: cmd: ", cmd)
        spark.sql(cmd)
        nb_keys = spark.sql("select count(*) as cnt from rna_snv_smp_vafs").collect()[0]["cnt"]
        print("4: rna_snv_smp_vafs nb_keys: ", nb_keys)

        # 5 count the number of allele occurance in each group
        spark.sql("drop table if exists rna_snv_grp_smps")
        cmd = "create table rna_snv_grp_smps as " \
            "select rssv.snv_id, " \
            "   rs.sc_grp_idx, " \
            "   count(rs.smpgrp_name) as nb_smps "\
            "from rna_snv_smp_vafs rssv "\
            " join sc_grp rs on rs.smpgrp_name = rssv.smp_name "\
            "where rssv.vaf >= %(minvaf)s "\
            "group by rssv.snv_id, "\
            "	rs.sc_grp_idx " \
            % {"minvaf": self.esnv_min_vaf}
        print("cmd: ", cmd)
        spark.sql(cmd)
        nb_keys = spark.sql("select count(*) as cnt from rna_snv_grp_smps").collect()[0]["cnt"]
        print("5: rna_snv_grp_smps nb_keys: ", nb_keys)

        # 6
        spark.sql("drop table if exists sel_rna_snv_ids")
        cmd = \
            "create table sel_rna_snv_ids as " \
            "select snv_id " \
            "from rna_snv_grp_smps ct " \
            "where ct.sc_grp_idx in ( %s ) and ct.nb_smps >= %s " \
            "intersect " \
            "select snv_id " \
            "from rna_snv_grp_smps sc " \
            "where sc.sc_grp_idx in ( %s ) and sc.nb_smps >= %s " \
            % (", ".join(repr(e) for e in self.scqc3_conf["esnv_grbulks"]),
               self.esnv_min_nb_blk,
               ", ".join(repr(e) for e in self.scqc3_conf["esnv_grscels"]),
               self.esnv_min_nb_scs)

        print("cmd: ", cmd)
        spark.sql(cmd)
        nb_keys = spark.sql("select count(*) as cnt from sel_rna_snv_ids").collect()[0]["cnt"]
        print("6: sel_rna_snv_ids nb_keys: ", nb_keys)



        # ss_grp_df.write.saveAsTable("ss_grp")
        #
        # print(ss_grp_df.schema.fields)

    @staticmethod
    def f(splitIndex, it, col_lst):

        dic = {}
        for row in it:
            # dic["smp_name"] = i[1]["smp_name"]
            smp = row[1]["smp_name"]
            dic[smp] = row[1]["vaf"]
            # lst.append("_%s_" % i[0])

        # lst2 = list(set(lst))
        # return iter(lst)
        res = []
        res.append(splitIndex)
        # res.append(dic)
        # res.append(list(dic.keys()))
        for j in col_lst.value:
            vv = dic.get(j, -5)
            res.append(vv)
        # for key, val in dic.items():
        #     res.append("key-%s-" % key)
        #     res.append("val-%s-" % val)

        res2 = []
        res2.append(res)

        time.sleep(2)

        if len(dic.keys()) > 0:
            return res2
        else:
            return []
        # yield
        # yield splitIndex

    @staticmethod
    def f_unpiv_smp_cnts(splitIndex, it, col_lst):

        # collect all values
        dic = {}
        dicp = {}
        for row in it:
            # dic["smp_name"] = i[1]["smp_name"]
            part_id = row[0]
            dicrow = row[1]

            if not part_id in dic.keys():
                dic[part_id] = {}
                dicp[part_id] = {}

            dic[part_id][dicrow["smp_name"]] = dicrow["val"]
            dicp[part_id]["gene_id"] = dicrow["gene_id"]
            dicp[part_id]["gene_name"] = dicrow["gene_name"]
            dicp[part_id]["source"] = dicrow["source"]

        # return results
        tblres = []
        # scroll results one partition id and one dict
        for part_id, dicgenepart in dic.items():

            # lst2 = list(set(lst))
            # return iter(lst)
            # row
            rowres = []
            rowres.append(part_id)
            rowres.append(splitIndex)
            rowres.append(len(dic))
            rowres.append(dicp[part_id]["gene_id"])
            rowres.append(dicp[part_id]["gene_name"])
            rowres.append(dicp[part_id]["source"])

            # rowres.append(len(dicsnvpart))
            # res.append(dic)
            # res.append(list(dic.keys()))

            # missing gene count values are 0
            for j in col_lst.value:
                vv = dicgenepart.get(j, 0)
                rowres.append(vv)

            # add row to table results
            tblres.append(rowres)

        if len(tblres) > 0:
            return tblres
        else:
            return []

    @staticmethod
    def f_unpiv_smps(splitIndex, it, info_lst, cnt_lst, col_lst):


        # collect all values
        dic = {}
        for row in it:
            # dic["smp_name"] = i[1]["smp_name"]
            snv_id = row[1]["snv_id"]
            smp = row[1]["smp_name"]

            if snv_id in dic.keys():
                dic[snv_id][smp] = row[1]["vaf"]
            else:
                dic[snv_id] = {}
                dic[snv_id][smp] = row[1]["vaf"]


        # lst.append("_%s_" % i[0])

        # return results
        tblres = []
        for snv_id, dicsnvpart in dic.items():

            # lst2 = list(set(lst))
            # return iter(lst)
            # row
            rowres = []
            rowres.append(snv_id)
            rowres.append(splitIndex)
            rowres.append(len(dic))
            # rowres.append(len(dicsnvpart))
            # res.append(dic)
            # res.append(list(dic.keys()))

            # added info
            for j in info_lst:
                vv = dicsnvpart.get(j, 0)
                rowres.append(vv)

            # first column counts
            for j in cnt_lst.value:
                vv = dicsnvpart.get(j, 0)
                rowres.append(vv)

            for j in col_lst.value:
                vv = dicsnvpart.get(j, -3)
                rowres.append(vv)
            # for key, val in dic.items():
            #     res.append("key-%s-" % key)
            #     res.append("val-%s-" % val)

            # add row to table results
            tblres.append(rowres)

        # time.sleep(2)

        if len(tblres) > 0:
            return tblres
        else:
            return []
        # yield
        # yield splitIndex

    @staticmethod
    def f2(iterator):

        lst = []
        for i in iterator:
            lst.append("_%s_" % i[0])

        lst2 = list(set(lst))

        return iter(lst)
        # return [lst2]
        # yield
        # yield splitIndex

    def rna_snv_smp_vaf_unpiv(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))


        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        # 7
        spark.sql("drop table if exists rna_snv_smp_vafs_cnts")

        cmd = \
            "create table rna_snv_smp_vafs_cnts as "\
            "select rsgs.snv_id, "\
            "       concat('CNT_',cg.name) as smp_name, "\
        	"       rsgs.nb_smps as vaf "\
            "from rna_snv_grp_smps rsgs "\
            " join ctl_grps cg on cg.id = rsgs.sc_grp_idx "
        print("cmd: ", cmd)

        spark.sql(cmd)
        nb_keys = spark.sql("select count(*) as cnt from rna_snv_smp_vafs_cnts").collect()[0]["cnt"]
        print("7: rna_snv_smp_vafs_cnts nb_keys: ", nb_keys)


        # 8
        # "union all
# select rs.snv_id,'REF', rs.ref
# from rna_snvs rs
# union all
# select rs.snv_id,'ALT', rs.alt
# from rna_snvs rs
# union all
# select rs.snv_id,'POS', rs.pos
# from rna_snvs rs"

        piv_chrom = spark.sql(
            "select snv_id, 'CHROM' as smp_name , chrom as vaf "
            "from rna_snvs "
        )
        piv_pos = spark.sql(
            "select snv_id, 'POS' as smp_name , pos as vaf "
            "from rna_snvs "
        )
        piv_ref = spark.sql(
            "select snv_id, 'REF' as smp_name , ref as vaf "
            "from rna_snvs "
        )
        piv_alt = spark.sql(
            "select snv_id, 'ALT' as smp_name , alt as vaf "
            "from rna_snvs "
        )

        info_col_names = ["CHROM", "POS", "REF", "ALT"]

        piv_cnts = spark.sql(
            "select rssvc.snv_id, "
            "    rssvc.smp_name, "
            "    rssvc.vaf "
            "from rna_snv_smp_vafs_cnts rssvc "
            "   join sel_rna_snv_ids srsi on srsi.snv_id = rssvc.snv_id "
        )
        piv_vaf = spark.sql(
            "select srsi.snv_id, "
            "    rssv.smp_name, "
            "    rssv.vaf "
            "from rna_snv_smp_vafs rssv "
            "   join sel_rna_snv_ids srsi on srsi.snv_id = rssv.snv_id "
            "")

        piv_sel_ids = spark.sql(
            "select snv_id from sel_rna_snv_ids")


        piv_ds = piv_chrom\
            .union(piv_pos)\
            .union(piv_ref)\
            .union(piv_alt)\
            .union(piv_cnts)\
            .union(piv_vaf)

        piv_rdd = piv_ds.join(piv_sel_ids, piv_ds.snv_id == piv_sel_ids.snv_id).rdd


        #   "where srsi.snv_id in (10, 20)"
        # ss_grp_f = os.path.join(self.conf.conf_dir, "ss-grp-sample-info.csv")
        # ss_grp_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(ss_grp_f)

        piv2_rdd = piv_rdd.map(lambda x: [x.snv_id, x.asDict()])
        # piv2_rdd = piv_rdd.map(tuple) .collectAsMap()
        # piv2_rdd = piv_rdd.map(tuple)
        zz = piv2_rdd.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)

        keys_lst = piv2_rdd.keys().collect()
        # sorted(list(set(keys_lst)))
        # set([object.id for object in keys_lst])
        nb_keys = len(set(keys_lst))

        nb_keys = 99

        nb_keys = spark.sql("select count(*) as cnt from rna_snvs").collect()[0]["cnt"]
        print("nb_keys: ", nb_keys)

        # wp = piv2_rdd.partitionBy(nb_keys, lambda k: int(k))
        wp = piv2_rdd.partitionBy(500, lambda k: int(k))
        num_part = wp.getNumPartitions()
        print("num_part: ", num_part)

        # zz = wp.collect()
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        col_names = spark.sql(
            "select smpgrp_name "\
            "from sc_grp "\
            "where skip=0 "\
            "order by sc_ord_idx"
        ).rdd.map(lambda x: x.smpgrp_name).collect()

        cnt_names = spark.sql(
            "select concat('CNT_',cg.name) as smp_name "\
            "from ctl_grps cg "\
            "order by cg.id "
        ).rdd.map(lambda x: x.smp_name).collect()



        # col_names = ["SC_C09_G__C16_A038", "SC_G06_G__C85_A038"]
        Cnts = sc.broadcast(cnt_names)
        print(Cnts.value)

        Cols = sc.broadcast(col_names)
        print(Cols.value)

        # parts2 = bodyf.flatMap(partial(self.worker_func, colnames=V))

        # wp2 = wp.mapPartitionsWithIndex(partial(self.f, col_lst=V))
        wp2 = wp.mapPartitionsWithIndex(partial(self.f_unpiv_smps, info_lst=info_col_names, cnt_lst=Cnts, col_lst=Cols))


        # wp2 = wp.mapPartitions(self.f2)
        # zz = wp2.collect()[0:300]
        # print("zz: ", zz)

        outfolder = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "snv_smp_vcf_unpiv")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # parts2.write.csv(outfolder)

        # parts2.coalesce(1).map(lambda x_y: "{0},{1}".format(x_y[0], x_y[1])).saveAsTextFile(outfolder)
        # this removes paranthesis, brackets => just raw text csv
        rdd0 = [itertools.chain(["SNV_ID", "PART_IDX", "PART_RNB"], info_col_names, cnt_names, col_names)]
        a = sc.parallelize(rdd0).map(lambda x_y: ",".join(map(str, x_y))).repartition(1)

        b = wp2.map(lambda x_y: ",".join(map(str, x_y))).repartition(1).sortBy(lambda line: int(re.search(r"""^(\d+),""", line).group(1)))
        # wp2.map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)

        a.union(b).coalesce(1).saveAsTextFile(outfolder)
        shutil.copy(os.path.join(outfolder, "part-00000"), self.expr_vaf_mtx_f)

            # .map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)
        print("wp3: ")


            # .saveAsTextFile(outfolder)
        #
        # exit(0)
        # mytx.rdd.map(tuple).coalesce(1).saveAsTextFile(outfolder)

        # parts2.show()


        # zz = piv2_rdd.take(5)
        # zz = parts2.collect()[:5]
        # print("zz: ", zz)

        # spark.sql("drop table if exists ss_grp")
        # ss_grp_df.write.saveAsTable("ss_grp")

        # print(ss_grp_df.schema.fields)

    @property
    def templ_jupy_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "jupy")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def jupy_vari_dir(self):
        loc_dir = os.path.join(self._ctx.conf.pysrc_dir,
                               "vari_%s_%s" % (self.esnv_min_vaf, self.esnv_min_nb_scs)
                               )
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def figs_expr_dir(self):
        loc_dir = os.path.join(os.path.join(self._ctx.conf.proj_dir, "sc_qc", "figs"),
                               "expr"
                               )
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def figs_vari_dir(self):
        loc_dir = os.path.join(os.path.join(self._ctx.conf.proj_dir, "sc_qc", "figs"),
                               "vari_%s_%s" % (self.esnv_min_vaf, self.esnv_min_nb_scs)
                               )
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def fig_dir(self):
        return self.__fig_dir

    @fig_dir.setter
    def fig_dir(self, val):
        self.__fig_dir = val

    @property
    def fig_name_gen(self):
        return self.__fig_name_gen

    @fig_name_gen.setter
    def fig_name_gen(self, val):
        self.__fig_name_gen = val

    @property
    def fig_name_param(self):
        return self.__fig_name_param

    @fig_name_param.setter
    def fig_name_param(self, val):
        self.__fig_name_param = val



    @property
    def fig2a_ipynb_name(self):
        return "scqc3-fig_a.ipynb"

    @property
    def fig2a_ipynb_depl_file(self):
        return os.path.join(self._ctx.conf.pysrc_dir, self.fig2a_ipynb_name)

    @property
    def fig2b_ipynb_name(self):
        return "scqc3-fig_b.ipynb"

    @property
    def fig2b_ipynb_depl_file(self):
        return os.path.join(self._ctx.conf.pysrc_dir, self.fig2b_ipynb_name)

    @property
    def fig2c_ipynb_name(self):
        return "scqc3-fig_c.ipynb"

    @property
    def fig2c_ipynb_depl_file(self):
        return os.path.join(self._ctx.conf.pysrc_dir, self.fig2c_ipynb_name)

    @property
    def fig2d_ipynb_name(self):
        return "scqc3-fig_d.ipynb"

    @property
    def fig2e_ipynb_name(self):
        return "scqc3-fig_e.ipynb"

    @property
    def fig2d_ipynb_depl_file(self):
        return os.path.join(self.jupy_vari_dir, self.fig2d_ipynb_name)

    @property
    def fig2e_ipynb_depl_file(self):
        return os.path.join(self.jupy_vari_dir, self.fig2e_ipynb_name)


    @property
    def templ_jupy_dir(self):
        loc_dir = os.path.join(self._ctx.conf.templ_dir, "jupy")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    def gen_expr_datasets_src(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext


        nb_keys = spark.sql("select count(*) as cnt from gene_trk_ids").collect()[0]["cnt"]

        #  + 1 is for the 0 based index
        # last_gene_row = int(nb_keys) - 92 - 3 +1
        # last_ercc_row = int(nb_keys) +1

        last_ercc_row = int(nb_keys) - 1
        last_gene_row = last_ercc_row - (92+3)

# gene: [0, last_gene_row-1 ]
# ercc: [last_gene_row-1, last_ercc_row-1]


        datasets_src = r"""
last_gene_row = %s
last_ercc_row = %s
project_root = "%s"
exper_name = "%s-%s"
fig_name = "%s"
# [) open interval
datasets = {"gene": ["matr/%s.%s.readcnt.known.genes.csv", 0, last_gene_row+1 ],
            "ercc": ["matr/%s.%s.readcnt.known.genes.csv", last_gene_row+1, last_ercc_row+1]
}
        """ % (last_gene_row,
               last_ercc_row,
               self._ctx.conf.proj_dir,
               self._ctx.conf.proj_abbrev,
               self._ctx.conf.exper_abbrev,
               self.fig_name_gen,
               self._ctx.conf.proj_abbrev,
               self._ctx.readcnt_prefix,
               self._ctx.conf.proj_abbrev,
               self._ctx.readcnt_prefix
               )

        # print("datasets_src: \n", datasets_src)
        return datasets_src

    def gen_vari_datasets_src(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        nb_keys = spark.sql("select count(*) as cnt from sel_rna_snv_ids").collect()[0]["cnt"]

        datasets_src = r"""last_gene_row=%s
project_root = "%s"
exper_name = "%s-%s"
fig_name = "%s"
datasets = {"gene": ["%s", 0, last_gene_row]
}""" \
                       % (nb_keys,
                          self._ctx.conf.proj_dir,
                          self._ctx.conf.proj_abbrev,
                          self._ctx.conf.exper_abbrev,
                          self.fig_name_gen,
                          self.expr_vaf_mtx_f
                       )


        print("datasets_src: \n", datasets_src)
        return datasets_src


    # put an -1 on sc_ord_idx in sc-grp-sample-info.csv for each position that is skip=1
    # then just increment over this position to the next
    def gen_expr_run_datasets_src(self, fig_name, nb_sup_cols):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        run_datasets_lines = ""
        run_datasets_arr = []
        run_datasets_line_src = ""

        for rds in self.scqc3_conf[fig_name]["run_datasets"]:
            pp.pprint(rds)

            grx = rds["groups_x"]
            grx_ranges_arr = []
            for gr in grx:
                print("gr: ", gr)
                grmin, grmax = spark.sql(
                    "select min(sg.sc_ord_idx) as grmin, "
                    "       max(sg.sc_ord_idx) as grmax "
                    "from sc_grp sg "
                    "where sg.sc_grp_idx = %s and "
                    "      sg.sc_ord_idx>=0 " % gr).collect()[0]

                lst = range(nb_sup_cols+int(grmin), nb_sup_cols+int(grmax)+1)
                print("x_lst: ", lst)
                grx_ranges_arr.extend(lst)

            grx_ranges = list(set(grx_ranges_arr))
            grx_ranges.sort()
            pp.pprint(grx_ranges)
            #
            gry = rds["groups_y"]
            gry_ranges_arr = []
            for gr in gry:
                print("gr: ", gr)
                grmin, grmax = spark.sql(
                    "select min(sg.sc_ord_idx) as grmin, "
                    "       max(sg.sc_ord_idx) as grmax "
                    "from sc_grp sg "
                    "where sg.sc_grp_idx = %s and "
                    "      sg.sc_ord_idx>=0 " % gr).collect()[0]

                lst = range(nb_sup_cols+int(grmin), nb_sup_cols+int(grmax)+1)
                print("y_lst: ", lst, grmin, grmax)
                gry_ranges_arr.extend(lst)

            gry_ranges = list(set(gry_ranges_arr))
            gry_ranges.sort()
            pp.pprint(gry_ranges)

            run_datasets_line = r"""["%s", "%s", "%s", %s, "%s", %s, "%s"]""" \
                                % (rds["name"],
                                   rds["dataset"],
                                   rds["title_fig"],
                                   grx_ranges,
                                   rds["title_x"],
                                   gry_ranges,
                                   rds["title_y"])

            run_datasets_arr.append(run_datasets_line)

        run_datasets_src = "run_datasets = [ %s ]" % ",\n".join(run_datasets_arr)

        # print("run_datasets_src: \n", run_datasets_src)
        return run_datasets_src

    # put an -1 on sc_ord_idx in sc-grp-sample-info.csv for each position that is skip=1
    # then just increment over this position to the next
    def gen_vari_run_datasets_src(self, fig_name, nb_sup_cols):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        run_datasets_lines = ""
        run_datasets_arr = []
        run_datasets_line_src = ""

        for rds in self.scqc3_conf[fig_name]["run_datasets"]:
            pp.pprint(rds)

            grx = rds["groups_x"]
            grx_ranges_arr = []
            for gr in grx:
                print("gr: ", gr)
                grmin, grmax = spark.sql(
                    "select min(sg.sc_ord_idx) as grmin, "
                    "       max(sg.sc_ord_idx) as grmax "
                    "from sc_grp sg "
                    "where sg.sc_grp_idx = %s and "
                    "      sg.sc_ord_idx>=0 " % gr).collect()[0]

                lst = range(nb_sup_cols+int(grmin), nb_sup_cols+int(grmax)+1)
                print("x_lst: ", lst)
                grx_ranges_arr.extend(lst)

            grx_ranges = list(set(grx_ranges_arr))
            grx_ranges.sort()
            pp.pprint(grx_ranges)
            #
            gry = rds["groups_y"]
            gry_ranges_arr = []
            for gr in gry:
                print("gr: ", gr)
                grmin, grmax = spark.sql(
                    "select min(sg.sc_ord_idx) as grmin, "
                    "       max(sg.sc_ord_idx) as grmax "
                    "from sc_grp sg "
                    "where sg.sc_grp_idx = %s and "
                    "      sg.sc_ord_idx>=0 " % gr).collect()[0]

                lst = range(nb_sup_cols+int(grmin), nb_sup_cols+int(grmax)+1)
                print("y_lst: ", lst, grmin, grmax)
                gry_ranges_arr.extend(lst)

            gry_ranges = list(set(gry_ranges_arr))
            gry_ranges.sort()
            pp.pprint(gry_ranges)

            run_datasets_line = r"""["%s", "%s", "%s", %s, "%s", %s, "%s"]""" \
                                % (rds["name"],
                                   rds["dataset"],
                                   rds["title_fig"],
                                   grx_ranges,
                                   rds["title_x"],
                                   gry_ranges,
                                   rds["title_y"])

            run_datasets_arr.append(run_datasets_line)

        run_datasets_src = "run_datasets = [ %s ]" % ",\n".join(run_datasets_arr)

        print("run_datasets_src: \n", run_datasets_src)
        return run_datasets_src


    def deploy_figs2a_b_c(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        exit_codes = []

        #  +1 for 1 based indices from sql
        # NBSUPCOL = 3

        dtasets = [
            ["fig2a", self.fig2a_ipynb_name, self.fig2a_ipynb_depl_file],
            ["fig2b", self.fig2b_ipynb_name, self.fig2b_ipynb_depl_file],
            ["fig2c", self.fig2c_ipynb_name, self.fig2c_ipynb_depl_file],
            ]


        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_jupy_dir))

        for fig_name, ipynb_name, depl_name in dtasets:

            self.fig_name_gen = fig_name

            # python too
            template = env.get_template(ipynb_name)

            datasets_src = self.gen_expr_datasets_src()
            run_datasets_src = self.gen_expr_run_datasets_src(fig_name, self.scqc3_conf["expr_nbsupcol"])

            print("datasets_src: ", datasets_src)
            print("run_datasets_src: ", run_datasets_src)

            template_vars = {
                "datasets_src": json.dumps(datasets_src),
                "run_datasets_src": json.dumps(run_datasets_src),
                "figs_dir": self.figs_expr_dir
                             }

            if fig_name == "fig2c":
                template_vars["nb_sim"] = self.scqc3_conf["nb_sim"][fig_name]


            # debug
            # template_vars = {
            #     "datasets_src": "\n",
            #     "run_datasets_src": "\n"
            #                  }

            txt_out = template.render(template_vars)

            f = open(depl_name, 'w')
            f.write(txt_out)
            f.close()

            # run it
            cmd = r"""jupyter nbconvert --to notebook --execute %s """ % ipynb_name
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self._ctx.conf.pysrc_dir)
            p1.wait()
            rc1 = p1.returncode
            print(("rc1: ", rc1))
            exit_codes.append(rc1)

        return [exit_codes]

    def deploy_fig2d_e(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        #  +1 for 1 based indices from sql
        NBSUPCOL = self.scqc3_conf["vari_nbsupcol"]

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        exit_codes = []
        dtasets = [
            ["fig2d", self.fig2d_ipynb_name, self.fig2d_ipynb_depl_file],
            ["fig2e", self.fig2e_ipynb_name, self.fig2e_ipynb_depl_file],

            ]


        # deploy configuration
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.templ_jupy_dir))

        for fig_name, ipynb_name, ipnb_depl in dtasets:

            self.fig_name_gen = fig_name

            # python too
            template = env.get_template(ipynb_name)

            datasets_src = self.gen_vari_datasets_src()
            run_datasets_src = self.gen_vari_run_datasets_src(fig_name, self.scqc3_conf["vari_nbsupcol"])

            # print("datasets_src: ", datasets_src)
            # print("run_datasets_src: ", run_datasets_src)

            template_vars = {
                "datasets_src": json.dumps(datasets_src),
                "run_datasets_src": json.dumps(run_datasets_src),
                "figs_dir": self.figs_vari_dir
                             }
            if fig_name == "fig2e":
                template_vars["nb_sim"] = self.scqc3_conf["nb_sim"][fig_name]

            txt_out = template.render(template_vars)

            f = open(ipnb_depl, 'w')
            f.write(txt_out)
            f.close()

            # run it
            cmd = r"""jupyter nbconvert --to notebook --execute %s""" % ipynb_name
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.jupy_vari_dir)
            p1.wait()
            rc1 = p1.returncode
            print(("rc1: ", rc1))
            exit_codes.append(rc1)

        return [exit_codes]


    @property
    def readcnt_known_genes_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "%s.%s.readcnt.known.genes.csv" % (self.conf.proj_abbrev, self.readcnt_prefix))



    ################################ READCOUNTS

    def spark_parse_readcnt_known_genes_matrix(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        dfs = []

        smpgrps = self._ctx.conf.sample_grps.query('skip != 1').index.values
        # print("smpgrps: ", smpgrps)
        # for smpgrp_idx in range(self._ctx.nb_smpgrps):
        for smpgrp_idx in smpgrps:
            # for smp_idx in range(2):
            # set index in order to make all path work
            self._ctx.smpgrp_idx = smpgrp_idx
            print(("self.smpgrp_idx: ", self._ctx.smpgrp_idx))
            print(("self.smpgrp_name: ", self._ctx.smpgrp_name))  # read sample matrix

            indiv_schema = StructType(
                [StructField("gene_id", StringType(), False),
                 StructField("readcnt", IntegerType(), False)
                 ]
            )

            # try:

            cur_df2 = spark.read.csv(
                self._ctx.genes_readcnt_tracking_f,
                header=False,
                mode="DROPMALFORMED",
                sep="\t",
                schema=indiv_schema)
            cur_df = cur_df2.withColumn("smp_name", lit(self._ctx.smpgrp_name))


                # cur_df = pd.read_csv(self._ctx.genes_readcnt_tracking_f, sep='\t', header=None,
                #                      index_col=[0],
                                     # usecols=[0, 1],
                                     # names=["gene_id", "readcnt"])
            # except Exception as inst:
                # print((type(inst)))  # the exception instance
                # print((inst.args))  # arguments stored in .args
                # print(inst)  # __str__ allows args to be printed directly
                # print("smp: ERRORR----------------------------------")
                # continue
            dfs.append(cur_df)

            print("cur_df: ", cur_df.count(), cur_df.take(2))
            # cur_agg = cur_df.groupby(["gene_id"])
            # cur_cnt = cur_agg["readcnt"].agg({'cnt_avg': np.average})

            # print(("cur_cnt.shape: ", cur_cnt.shape))

            # self.genes_matrix_df = pd.merge(self.genes_matrix_df, cur_cnt,
            #                                 how='inner', left_index=True, right_index=True)
            # self.genes_matrix_df.rename(columns={'cnt_avg': self.smpgrp_name}, inplace=True)
            # print(("smpgrp_name: %s, rows: %s " % (self.smpgrp_name, self.genes_matrix_df.shape[0])))
        print("dfs: ", dfs)
        res_df = reduce(DataFrame.unionAll, dfs)
        print("res_df size: ", res_df.count())
        spark.sql("drop table if exists gene_smp_cnts")
        res_df.write.saveAsTable("gene_smp_cnts")

    def spark_parse_genes_tracking_ids(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        idv_arr = []
        idv_arr.append(StructField("idx", IntegerType(), False))
        idv_arr.append(StructField("gene_id", StringType(), False))
        idv_arr.append(StructField("gene_name", StringType(), False))
        idv_arr.append(StructField("source", StringType(), False))
        idv_sch = StructType(idv_arr)

        print("idv_sch: ", idv_sch)


        cur_df = spark.read.csv(
                self._ctx.conf.genes_tracking_ids_csv,
                header=True,
                mode="DROPMALFORMED",
                sep=",",
                schema=idv_sch)
        print("cur_df count: ", cur_df.count())
        cur_df.show(2)

        spark.sql("drop table if exists gene_trk_ids")
        cur_df.write.saveAsTable("gene_trk_ids")

    def gene_flt_smp_cnt_unpiv(self, include_meta_smps=False):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        # select data with index

        # if gene_filter:
        #     piv0_rdd = spark.sql(
        #         "select gti.idx as part_id, "
        #         "	gti.gene_id, "
        #         "gti.gene_name, "
        #         "	gti.source, "
        #         "	gsc.smp_name, "
        #         "	gsc.readcnt as val "
        #         "from gene_smp_cnts  gsc "
        #         " join gene_trk_ids gti on gti.gene_id = gsc.gene_id "
        #         " join %s fgi on fgi.gene_id = gti.gene_id " % gene_filter
        #     ).rdd
        # else:
        #     piv0_rdd = spark.sql(
        #         "select gti.idx as part_id, "
        #         "	gti.gene_id, "
        #         "gti.gene_name, "
        #         "	gti.source, "
        #         "	gsc.smp_name, "
        #         "	gsc.readcnt as val "
        #         "from gene_smp_cnts  gsc "
        #         " join gene_trk_ids gti on gti.gene_id = gsc.gene_id "
        #     ).rdd

        piv0 = spark.sql(
            "select gsc.gene_id as gene_id, "
            "       gsc.readcnt as readcnt, "
            "       gsc.smp_name as smp_name "
            "from gene_smp_cnts  gsc "
            " join flt_mtx_ids fmi on fmi.gene_id = gsc.gene_id"
            )
        # print("piv0: ", piv0.count(), piv0.show())

        if include_meta_smps:
            piv1 = spark.sql(
                "select mfsc.gene_id as gene_id, "
                "       mfsc.readcnt as readcnt, "
                "       mfsc.smp_name as smp_name "
                "from meta_flt_smp_cnts mfsc "
            )
            print(piv1.count())
            piv2 = piv0.union(piv1)
            # piv2 = piv0

            # piv2.write.mode("overwrite").saveAsTable("piv2")
            # print("piv2: ", piv2.count(), piv2.show())
        else:
            piv2 = piv0
            # piv2.write.mode("overwrite").saveAsTable("piv2")
            # print("piv2: ", piv2.count(), piv2.show())

        piv3 = spark.sql(
            "select gti.idx as part_id, "
            "       gti.gene_id, "
            "       gti.gene_name, "
            "	    gti.source "
            "from gene_trk_ids gti"
        )

        piv = piv2.join(piv3, piv2.gene_id == piv3.gene_id).select(
            piv3.part_id,
            piv2.gene_id,
            piv3.gene_name,
            piv3.source,
            piv2.smp_name,
            piv2.readcnt.alias("val")
            )

        # print("piv: ", piv.count(),  piv.show())
        # piv.write.mode("overwrite").saveAsTable("piv")

        piv_rdd = piv.rdd


        # tupple rdd with index and row as dictionnary,
        #  => easy to collect only some fields in client side procedure
        piv2_rdd = piv_rdd.map(lambda x: [x.part_id, x.asDict()])

        wp = piv2_rdd.partitionBy(500, lambda k: int(k))
        num_part = wp.getNumPartitions()
        print("num_part: ", num_part)

        simple_col_names = spark.sql(
            "select smpgrp_name "\
            "from sc_grp "\
            "where skip=0 "\
            "order by sc_ord_idx"
        ).rdd.map(lambda x: x.smpgrp_name).collect()

        print(simple_col_names)

        if include_meta_smps:
            cmd = "select smp_name from meta_smps"
            meta_col_names = spark.sql(cmd).toPandas().smp_name.values.tolist()
            # col_names = list([itertools.chain(simple_col_names, meta_col_names)])
            col_names = simple_col_names + meta_col_names
            # col_names = simple_col_names

            # col_names = simple_col_names.append(meta_col_names)
        else:
            col_names = simple_col_names

        print(col_names)


        # col_names = ["SC_C09_G__C16_A038", "SC_G06_G__C85_A038"]
        V = sc.broadcast(col_names)
        print(V.value)

        # parts2 = bodyf.flatMap(partial(self.worker_func, colnames=V))

        # wp2 = wp.mapPartitionsWithIndex(partial(self.f, col_lst=V))
        wp2 = wp.mapPartitionsWithIndex(partial(self.f_unpiv_smp_cnts, col_lst=V))
        # now we have an rdd

        # wp2 = wp.mapPartitions(self.f2)
        # zz = wp2.collect()[0:300]
        # print("zz: ", zz)

        outfolder = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gene_flt_smp_cnt_unpiv")
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder, ignore_errors=False, onerror=None)

        # parts2.write.csv(outfolder)

        rdd0 = [itertools.chain(["GENE_IDX", "PART_IDX", "PART_RNB", "GENE_ID", "GENE_NAME", "SOURCE"], col_names)]
        a = sc.parallelize(rdd0).map(lambda x_y: ",".join(map(str, x_y))).repartition(1)

        b = wp2.map(lambda x_y: ",".join(map(str, x_y))).repartition(1).sortBy(lambda line: int(re.search(r"""^(\d+),""", line).group(1)))
        # wp2.map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)

        a.union(b).coalesce(1).saveAsTextFile(outfolder)
        shutil.copy(os.path.join(outfolder, "part-00000"), self._ctx.readcnt_known_genes_matrix_f)

            # .map(lambda x_y: ",".join(map(str,x_y))).saveAsTextFile(outfolder)
        print("wp3: ")


    def gtf_to_csv(self):

        # Input files.
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/ucRn6sp3p92-em-v83.gtf"
        # GENCODE      = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL/test.gtf"
        # GENCODE = self._ctx.conf.ref_gen_transcr_gtf
        # GENCODE = "/gs/project/wst-164-aa/share/raglab_prod/igenomes/ucMm10p5a3/orig/mm10/gencode.vM16.primary_assembly.annotation.gtf"

        GENCODE = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gencode.gtf")



        # NCBI_ENSEMBL = args['-n']

        # Output file prefix.
        # GENE_LENGTHS = "ncbi_ensembl_coding_lengths.txt.gz"

        with log("Reading the Gencode annotation file: {}".format(GENCODE)):
            gc = GTF_ENCODE.dataframe(GENCODE)

        print("gc: \n ", gc, gc.columns)

        cols_df = pd.DataFrame({'columns': gc.columns})



        # Convert columns to proper types.
        gc.start = gc.start.astype(int)
        gc.end = gc.end.astype(int)

        # chrs_df = gc.groupby('seqname').first().iloc[:,[]].reset_index('seqname') #.index #.values #.loc[:,['seqname']]
        # print("chrs_df: ", chrs_df)
        gc.to_csv(os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gencode.csv"),
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


    def gtf_to_csv_old_cont(self):

        exit(0)

        gc2 = gc.copy()

        # all transcripts
        sel = gc2.loc[:, ['transcript_id', 'transcript_name', 'gene_id', 'gene_name', 'source']]


        sel.set_index(['transcript_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.isofs_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # all genes
        sel = gc2.loc[:, ['gene_id', 'gene_name', 'source']]
        sel.set_index(['gene_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.genes_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # Sort in place.
        # gc.sort(['seqname', 'start', 'end'], inplace=True)

        # print("exon: \n ", exon)
        #








    def spark_save_gtf_embl_seqnames(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd = r"""
            select distinct seqname
            from gtf
            where feature='exon'
            order by seqname
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pandas_df = res_df.toPandas()
        print("pandas_df: ", pandas_df)

        pandas_df.to_csv(os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gtf_embl_seqnames.csv"),
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


    def spark_parse_gtf_csv(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        gtf_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gencode.csv")
        gtf_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(gtf_f)

        zz = gtf_df.take(5)
        # zz = parts2.collect()[:5]
        print("zz: ", zz)


        spark.sql("drop table if exists gtf")
        #
        tbl_name = "gtf"
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        gtf_df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",
                                                       partitionBy=["feature"])
        #
        # gtf_df.write.saveAsTable("gtf")

        # print(gtf_df.schema.fields)


    def spark_loadcover_redis(self,smp_name, reg_al, seqname, in_d):
            spark = self._ctx.servers_obj.spark_session
            sc = spark.sparkContext

            # with open(in_d) as file:
            #     data = orc.ORCFile(file)
            #     df = data.read().to_pandas()



            tbl = spark.read.format("orc").load(in_d)
            df = tbl.toPandas()
            print(df)

            for i in df.index:
                val = df.loc[i,"seq"]
                print("val: ", val)

                # if math.isnan(val):
                #     df.set_value(i, 'Age', -1)




    def spark_loadcover_orc(self,smp_name, reg_al, in_d):
            spark = self._ctx.servers_obj.spark_session
            sc = spark.sparkContext

            # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example.parquet"
            # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example_baz_part.parquet"
            # tbl = spark.read.parquet(in_f).where("1=1")

            # tbl = spark.read.format("com.databricks.spark.avro").load(in_f).where("1=1")
            # tbl= tbl.withColumn('prt', tbl.two)

            # in_f="/home/badescud/scratch/over/duny/qualviz/avro"
            # in_f="/home/badescud/scratch/over/duny/qualviz_G002Normal/avro"
            # in_f="/home/badescud/scratch/over/duny/qvz_Jurkat_C9m_Naive-2of2/avro"
            # in_f="/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/Ascites5_h2Mm10/avro"
            # in_f = "/home/dbadescu/share/raglab_prod/igenomes/hyMm10/srvf/filef/bamal"
            # in_f = "/home/dbadescu/share/raglab_prod/igenomes/hyMm10/srvf/filef/bamalorc"

            # tbl = spark.read.format("com.databricks.spark.avro").load(in_f)
            # org.apache.spark#spark-avro_2.11;2.4.1
            # tbl = spark.read.format("avro").load(in_f)

            tbl = spark.read.format("orc").load(in_d)

            tbl_name = "cover"
            # spark.sql("drop table if exists %s" % tbl_name)

            tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
            udf_assign_bin = pyspfunc.udf(self.assign_bin, returnType=IntegerType())
            tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('pos', 'pos')).withColumnRenamed("seq","chrom")
            print("tbl_d: ", tbl_d)

            tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
            prt_d = "%s/smp_name=%s/reg_al=%s" % (tbl_d, smp_name, reg_al)

            if not os.path.exists(prt_d):
                pathlib.Path(prt_d).mkdir(parents=True)

            tbl2.write.option("path", prt_d).mode("overwrite").partitionBy(["chrom"]).orc(prt_d)
            spark.sql("msck repair table %s" % tbl_name)

            # tbl2.write.option("path", tbl_d).saveAsTable("cover", format="orc", mode="overwrite", partitionBy=["seq"])

    def spark_create_table_utrs(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd=r"""select row_number() over (order by seqname_idx,start,end) as rowid,
                       ucsc_fa as chrom,
                       start,
                       end                                      
from
(
select cn.idx as seqname_idx,
       cn.ucsc_fa,
       gtfutr.start,
       gtfutr.end
from
(
select distinct seqname,start,end
--select *
--select distinct idx
from gtf
where feature='UTR'
) gtfutr
join contig_names cn on cn.embl_gtf=gtfutr.seqname
order by seqname_idx
        )"""
        print(cmd)
        res_df = spark.sql(cmd)
        res_df.show(3, False)

        tbl_name = "utrs"
        spark.sql("drop table if exists %s" % tbl_name)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        udf_assign_bin = pyspfunc.udf(self.assign_bin, returnType=IntegerType())
        tbl2 = res_df.withColumn('ucscbin', udf_assign_bin('start', 'end'))
        tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["chrom"])


    # os.path.exists(segs_d):
    # shutil.rmtree(segs_d, ignore_errors=False, onerror=None)

    def spark_valid_utrs_bins(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # print(self.overlapping_bins(840))

        # https://changhsinlee.com/pyspark-udf/

        overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=ArrayType(IntegerType()))
        # overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=StringType())

        cmd = """
          select chrom,
                 ucscbin,
                 count(*) as utrsperbin
    from utrs
    group by chrom,ucscbin
    order by chrom,ucscbin"""
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()
        tbl2 = tbl.withColumn('overbins', overlapping_bins_list_udf('ucscbin'))
        tbl2.show()

        tbl_name = "utr_overbins"
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        spark.sql("drop table if exists %s" % tbl_name)

        tbl2.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")


        cmd = """
    select chrom,
           ucscbin,
           overbin
    from utr_overbins
    lateral view explode(overbins) fvb2 as overbin
           """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()

        tbl_name = "utr_overbins_expl"
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)

        spark.sql("drop table if exists %s" % tbl_name)

        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")


    def spark_cov_per_base(self,smp_name,fastq_bases):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        # $(cat $RAGLAB_INSTALL_HOME/../rnvar/a088-x088-oocytes-e4/raw_reads/20181201PP-9-OOCYTES_A088/1001_1002/20181201PP-9-OOCYTES_A088.P1H03_F03R08_A00266_0112_2_S40.33.pair1.fastq.gz|wc -l)/4*100*2|bc
        # fastq_bases=606647800

        cmd="""select ucov.rowid,
       chrom,
       start,
       end,
       covbas,
       end-start as span,
       covbas/(end-start) as covperbase,
       covbas*1000000/(end-start)/%(fastq_bases)s as covperseq       
from
(
select rowid,sum(cov) as covbas
from 
(
select ut.rowid,cv.cov
from utr_overbins_expl ob
 join utrs ut on ut.chrom=ob.chrom and
                 ut.ucscbin=ob.ucscbin
 join cover cv on cv.chrom = ob.chrom and
                  cv.ucscbin= ob.overbin
where cv.smp_name='%(smp_name)s' and
      cv.pos>= ut.start and 
      cv.pos<=ut.end
)
group by rowid
) ucov
join utrs ut on ut.rowid=ucov.rowid
order by covbas desc
        """ %{"smp_name": smp_name,
              "fastq_bases": fastq_bases}

        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()

        tbl_name = "utr_cov_base"
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        spark.sql("drop table if exists %s" % tbl_name)

        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

    def spark_export_utr_cov_base(self,tag):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd="""select *
        from utr_cov_base
        """
        tbl = spark.sql(cmd)
        tbl.show()

        dfp = tbl.toPandas()
        print("dfp: \n", dfp)
        dfp = dfp.sort_values(by=['rowid'], ascending=True)

        out_f = os.path.join(self._ctx.conf.matr_dir, "utr_cov_base_%s.csv" % tag)
        dfp.to_csv(out_f, sep=","
                   , header=True
                   , index=True
                   , index_label="idx"
                   )


    def spark_extract_gtf_normalize(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # exons
        spark.sql("drop table if exists exons")
        spark.sql("drop table if exists exons0")

        # primary key is exon_id,seqname because of PAR regions
        cmd = """create table exons0 as
                select distinct exon_id,
                       regexp_extract(exon_id, '([^.]*).([0-9]*)', 1) as exon_uv,
                    regexp_extract(exon_id, '([^.]*).([0-9]*)', 2) as exon_ve,
                    seqname,
                    start,
                    end
                from gtf
                where feature='exon'
                order by exon_id
                """
        print(cmd)
        spark.sql(cmd)

        cmd = """create table exons as
                select row_number() over (order by exon_id) as rowid,*
                from
                (
                select exon_id, exon_uv, exon_ve, seqname, start, end
                from exons0
                union all
                select exon_id, exon_uv, exon_ve, seqname, start, end
                from exons_supl
                )"""

        print(cmd)
        spark.sql(cmd)

        # transcripts
        spark.sql("drop table if exists transcripts")
        spark.sql("drop table if exists transcripts0")
        #
        cmd = """create table transcripts0 as
                select distinct transcript_id,
                       regexp_extract(transcript_id, '([^.]*).([0-9]*)', 1) as transcript_uv,
                    regexp_extract(transcript_id, '([^.]*).([0-9]*)', 2) as transcript_ve,
                    transcript_name,
                    seqname,
                    start,
                    end,
                    strand,
                    gene_id,
                    tag,
                    transcript_type,
                    source,
                    havana_transcript,
                    ccdsid,
                    protein_id,
                    transcript_support_level,
                    ont
                from gtf
                where feature='transcript'
                order by transcript_id
                """
        print(cmd)
        spark.sql(cmd)

        cmd = """create table transcripts as
                select row_number() over (order by transcript_id) as rowid,*
                from
                (
                select transcript_id,
                       transcript_uv,
                    transcript_ve,
                    transcript_name,
                    seqname,
                    start,
                    end,
                    strand,
                    gene_id,
                    tag,
                    transcript_type,
                    source,
                    havana_transcript,
                    ccdsid,
                    protein_id,
                    transcript_support_level,
                    ont
                from transcripts0
                union all
                select transcript_id,
                       transcript_uv,
                    transcript_ve,
                    transcript_name,
                    seqname,
                    start,
                    end,
                    strand,
                    gene_id,
                    '' as tag,
                    transcript_type,
                    source,
                    '' as havana_transcript,
                    '' as ccdsid,
                    '' as protein_id,
                    '' as transcript_support_level,
                    '' as ont
                from transcripts_supl
                )"""
        print(cmd)
        spark.sql(cmd)


        # genes
        spark.sql("drop table if exists genes")
        spark.sql("drop table if exists genes0")
        #
        cmd = """create table genes0 as
                select distinct gene_id,
                       regexp_extract(gene_id, '([^.]*).([0-9]*)', 1) as gene_uv,
                    regexp_extract(gene_id, '([^.]*).([0-9]*)', 2) as gene_ve,
                    gene_name,
                    seqname,
                    start,
                    end,
                    strand,
                    gene_type,
                    source,
                    havana_gene,
                    level,
                    tag
                from gtf
                where feature='gene'
                order by gene_id
                """
        print(cmd)
        spark.sql(cmd)

        cmd = """create table genes as
                select row_number() over (order by gene_id) as rowid,*
                from
                (
                select gene_id,gene_uv,gene_ve,gene_name,seqname,start,end,strand,gene_type,source,
                havana_gene,level,tag
                from genes0
                union all
                select gene_id,gene_uv,gene_ve,gene_name,seqname,start,end,strand,gene_type,source,
                '' as havana_gene,'' as level,'' as tag
                from genes_supl
                )"""
        print(cmd)
        spark.sql(cmd)

        # transcript_exons
        spark.sql("drop table if exists transcript_exons")
        spark.sql("drop table if exists transcript_exons0")
        #
        cmd = """create table transcript_exons0 as
                select distinct transcript_id,
                    exon_number,
                    exon_id
                from gtf
                where feature='exon'
                order by transcript_id,exon_number
                """
        print(cmd)
        spark.sql(cmd)

        cmd = """create table transcript_exons as
                select row_number() over (order by transcript_id,exon_number) as rowid,*
                from
                (
                select transcript_id,exon_number,exon_id
                from transcript_exons0
                union all
                select transcript_id,exon_number,exon_id
                from transcript_exons_supl
                )
                """
        print(cmd)
        spark.sql(cmd)

    def spark_prepare_gtf_exons(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # exons
        spark.sql("drop table if exists gtf_exons")
        #
        cmd = """create table gtf_exons as
                select row_number() over (order by chr_idx,start,end) as rowid,*
                from
                (
                select cn.idx as chr_idx,
                    cn.ucsc_fa as seqname,
                    tr.source,
                    ex.start,
                    ex.end,
                    tr.strand,
                    gn.gene_uv,
                    gn.gene_name,
                    gn.gene_type,
                    tr.transcript_uv,
                    tr.transcript_name,
                    tr.transcript_type,
                    ex.exon_uv,
                    te.exon_number
                from transcript_exons te
                 join transcripts tr on tr.transcript_id=te.transcript_id
                 join genes gn on gn.gene_id=tr.gene_id
                 join exons ex on ex.exon_id=te.exon_id and ex.seqname=tr.seqname
                 join contig_names cn on cn.gencode_gtf=ex.seqname
                )"""
        print(cmd)
        spark.sql(cmd)

    # remove supplementary chromosomes from this gtf
    # remove PAR
    def spark_prepare_gtf_exons_ucsc(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # exons
        spark.sql("drop table if exists gtf_exons_ucsc")
        #
        cmd = """create table gtf_exons_ucsc as
                select row_number() over (order by chr_idx,start,end) as rowid,*
                from
                (
                select cn.idx as chr_idx,
                    cn.ucsc_fa as seqname,
                    tr.source,
                    ex.start,
                    ex.end,
                    tr.strand,
                    gn.gene_uv,
                    gn.gene_name,
                    gn.gene_type,
                    tr.transcript_uv,
                    tr.transcript_name,
                    tr.transcript_type,
                    ex.exon_uv,
                    te.exon_number
                from transcript_exons te
                 join transcripts tr on tr.transcript_id=te.transcript_id
                 join genes gn on gn.gene_id=tr.gene_id
                 join exons ex on ex.exon_id=te.exon_id and ex.seqname=tr.seqname
                 join contig_names cn on cn.gencode_gtf=ex.seqname
                where not exists (select *
	                              from contig_names_supl cns
                                  where cns.contig=cn.ucsc_fa) and
	                  tr.tag  != 'PAR'
                )"""
        print(cmd)
        spark.sql(cmd)

    def spark_work_unal(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        spark.sql("drop table if exists unal_parq")

        in_f="/home/badescud/projects/rrg-ioannisr/share/rnvar/a083-x083-oocytes-e1/raw_seqs/Oocytes_1of4_2/run001_1/unal_parq_exp.snappy.parquet"

        tbl = spark.read.parquet(in_f).where("1=0")
        tbl= tbl.withColumn('smp_name', lit("smp_name")).withColumn('rnln', lit("rnln"))
        tbl.write.saveAsTable("unal_parq", format="parquet", mode="overwrite", partitionBy=["smp_name","rnln"])

## tbl= df.withColumn('smp_name', lit("smp_name")).withColumn('reg_al', lit("reg_al")).withColumn('rnln', lit("rnln"))

    def spark_work_reference(self,genome):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="exons"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        # tbl= df.withColumn('smp_name', lit("smp_name")).withColumn('rnln', lit("rnln"))
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        # df.write.option("path", tbl_d).saveAsTable(tbl_name, format="parquet", mode="overwrite", partitionBy=["seqname","ucscbin"])
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["seqname"])

        # lab_dir is share
        in_f = "%s/raglab_prod/igenomes/%s/templ/hive/%s" % (self._ctx.conf.lab_dir,genome, tbl_name)
        print("in_f: ", in_f)
        tbl = spark.read.orc(in_f)
        # tbl = spark.read.parquet(in_f)

        udf_assign_bin = pyspfunc.udf(self.assign_bin, returnType=IntegerType())
        tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('start', 'end'))
        tbl2.show()

        # repartition("ucscbin")
        # tbl2.write.partitionBy("seqname","ucscbin").parquet(tbl_d,mode="overwrite")
        tbl2.write.partitionBy("seqname").orc(tbl_d,mode="overwrite")
        spark.sql("msck repair table %s" % tbl_name)

        # tbl2.repartition("ucscbin").write.option("path", tbl_d).partitionBy("seqname","ucscbin").saveAsTable(tbl_name, format="parquet", mode="overwrite")
        # tbl2.repartition("seqname").write.option("path", tbl_d).partitionBy("seqname").saveAsTable(tbl_name, format="parquet", mode="overwrite")
        # tbl2.repartition("seqname").write.option("path", tbl_d).partitionBy("seqname").saveAsTable(tbl_name, format="parquet", mode="overwrite", partitionBy=["seqname"])

        for tbl_name in ["transcripts", "transcript_exons", "genes"]:
            print("table : ", tbl_name)
            spark.sql("drop table if exists %s" % tbl_name)
            in_f = "%s/raglab_prod/igenomes/%s/templ/hive/%s" % (self._ctx.conf.lab_dir,genome, tbl_name)
            print("in_f: ", in_f)
            tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
            tbl = spark.read.orc(in_f)
            tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")
            # tbl = spark.read.parquet(in_f)
            # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="parquet", mode="overwrite")

    def spark_create_tables_meta_test(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="clasif_seqs_r2"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","rnln","seq_typ"])



        pass

    def spark_create_tables_meta(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        # unal_parq is parquet because writen by Dlang
        spark.sql("drop table if exists unal_parq")
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "unal_parq_schema.json")
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl= df.withColumn('smp_name', lit("smp_name")).withColumn('rnln', lit("rnln"))
        tbl_name="unal_parq"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable("unal_parq", format="parquet", mode="overwrite", partitionBy=["smp_name","rnln"])


        spark.sql("drop table if exists clasif_seqs_r1")
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "clasif_seqs_r1_schema.json")
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_name="clasif_seqs_r1"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","rnln","seq_typ"])

        tbl_name="clasif_seqs_r2"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","rnln","seq_typ"])

        # without ucscbin partition (nobins)
        tbl_name = "segs"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "segs_schema_nobins.json")
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable("segs", format="orc", mode="overwrite",
                                                    partitionBy=["smp_name", "rnln", "reg_al", "cigar_op_ref_name"])


        # tbl_name="clasif_seqs"
        # spark.sql("drop table if exists %s" % tbl_name)
        # in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        # file = open(in_f, "r")
        # schema_json = file.read()
        # new_schema = StructType.fromJson(json.loads(schema_json))
        # print("new_schema: ", new_schema)
        # tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        # tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","rnln","seq_typ"])
        #
        tbl_name = "clasif_seqs"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name", "rnln", "seq_typ"])


        tbl_name="umis"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","reg_al","chrom"])

        tbl_name="gncnts"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["feat_mdl","cnt_typ","smp_name"])


        # create table partitioned
        tbl_name="feats"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["feat_mdl","seqname"])


        # create table partitioned
        tbl_name="feat_mdl_bins"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        print("tbl_d: ", tbl_d)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["feat_mdl","seqname"])



        # tbl= df.withColumn('smp_name', lit("smp_name")).withColumn('rnln', lit("rnln"))
        # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="parquet", mode="overwrite", partitionBy=["smp_name","rnln"])
        # tbl= df.withColumn('smp_name', lit("smp_name")).withColumn('rnln', lit("rnln")).withColumn('reg_typ', lit("reg_typ"))

        pass

    def spark_create_tables_meta_seg(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # with ucscbin partition
        # tbl_name = "segs"
        # spark.sql("drop table if exists %s" % tbl_name)
        # in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "segs_schema_ucscbins.json")
        # file = open(in_f, "r")
        # schema_json = file.read()
        # new_schema = StructType.fromJson(json.loads(schema_json))
        # print("new_schema: ", new_schema)
        # tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        # tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable("segs", format="orc", mode="overwrite",
        #                                             partitionBy=["smp_name", "rnln", "reg_al", "cigar_op_ref_name",
        #                                                          "ucscbin"])

        # without ucscbin partition (nobins)
        # tbl_name = "segs"
        # spark.sql("drop table if exists %s" % tbl_name)
        # in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "segs_schema_nobins.json")
        # file = open(in_f, "r")
        # schema_json = file.read()
        # new_schema = StructType.fromJson(json.loads(schema_json))
        # print("new_schema: ", new_schema)
        # tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        # tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable("segs", format="orc", mode="overwrite",
        #                                             partitionBy=["smp_name", "rnln", "reg_al", "cigar_op_ref_name"])

        tbl_name = "clasif_seqs"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",
                                                    partitionBy=["smp_name", "rnln", "seq_typ"])


    def spark_create_tables_meta2(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="gncnts_tags"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["feat_mdl","cnt_typ","tag","smp_name"])

    def spark_create_tables_meta4(self):
            spark = self._ctx.servers_obj.spark_session
            sc = spark.sparkContext

            tbl_name = "cover"
            spark.sql("drop table if exists %s" % tbl_name)
            in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
            file = open(in_f, "r")
            schema_json = file.read()
            new_schema = StructType.fromJson(json.loads(schema_json))
            print("new_schema: ", new_schema)
            df = spark.createDataFrame(sc.emptyRDD(), new_schema)
            tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
            print("tbl_d: ", tbl_d)
            df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",
                                                       partitionBy=["smp_name", "reg_al", "chrom"])

            # remove working folder
            # if os.path.exists(unal_parq_prt):
            #     shutil.rmtree(unal_parq_prt, ignore_errors=False, onerror=None)
            # recreate
            # if not os.path.exists(unal_parq_prt):
            #     pathlib.Path(unal_parq_prt).mkdir(parents=True)


    def spark_create_tables_meta5(self):

        # spark = self._ctx.servers_obj.spark_session
        # sc = spark.sparkContext

        spark, sc = self._ctx.servers_obj.get_new_spark_session(1, 10, "create_tables_meta5")

        tbl_name="tr_cov_prop"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        df.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite", partitionBy=["smp_name","rnln","reg_al","chrom"])

        spark.stop()

    def spark_create_tables_meta6(self):

        spark, sc = self._ctx.servers_obj.get_new_spark_session(1, 10, "create_tables_meta6")

        tbl_name = "segs"
        spark.sql("drop table if exists %s" % tbl_name)
        in_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "segs_schema.json")
        file = open(in_f, "r")
        schema_json = file.read()
        new_schema = StructType.fromJson(json.loads(schema_json))
        print("new_schema: ", new_schema)
        tbl = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable("segs", format="orc", mode="overwrite",
                                                    partitionBy=["smp_name", "rnln", "reg_al",
                                                                 "cigar_op_ref_name", "ucscbin"])
        spark.stop()

    def spark_export_gncnts_tag(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="gncnts"

        for feat_mdl in ["STE","SGS"]:
            for cnt_typ in ["UDTP","UDHP"]:

                cmd2 = r"""
                    select *
                    from gncnts
                    where feat_mdl='%s' and
                          cnt_typ='%s'
                """ % (feat_mdl, cnt_typ)

                print("cmd2: ", cmd2)

                res_df = spark.sql(cmd2)
                res_df.show(3, False)

                out_f=os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "%s_%s_%s_%s" % (tbl_name, feat_mdl, cnt_typ, self._ctx.conf.proj_abbrev ))
                res_df.write.orc(out_f, mode="overwrite")


    def spark_import_gncnts_tag(self, feat_mdl, cnt_typ, tag):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="gncnts"
        in_f=os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "%s_%s_%s_%s" % (tbl_name, feat_mdl, cnt_typ, tag ))




    def spark_export_reference(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # genome = "hyMm10"
        genome = self._ctx.conf.ref_gen_assembly

        for table in ["exons", "transcripts", "transcript_exons", "genes", "contig_names"]:
            print("table : ", table)

            tbl = spark.table(table)
            # out_f="/home/badescud/projects/rrg-ioannisr/share/raglab_prod/igenomes/%s/templ/hive/%s" % (genome, table)
            out_f=os.path.join(self._ctx.conf.proj_dir, "templ/hive", table)
            print("out_f: ", out_f)
            tbl.write.orc(out_f, mode="overwrite")

            # spark.sql("drop table if exists %s" % table)
            # in_f="/home/badescud/projects/rrg-ioannisr/share/raglab_prod/igenomes/%s/srvf/hivef/%s" % (genome, table)
            # tbl = spark.read.format("com.databricks.spark.csv").load(in_f)
            # tbl.write.saveAsTable(table, format="parquet", mode="overwrite")




    def spark_work_segs(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        spark.sql("drop table if exists segs")

        in_f="/home/badescud/projects/rrg-ioannisr/share/rnvar/a083-x083-oocytes-e1/templ/hive/segs3p.snappy.parquet"

        tbl = spark.read.parquet(in_f).where("1=0")
        tbl= tbl.withColumn('smp_name', lit("smp_name")).withColumn('region', lit("region"))

        segs_d = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a083-x083-oocytes-e1/srvf/hivef/segs"
        tbl.write.option("path", segs_d).saveAsTable("segs", format="parquet", mode="overwrite", partitionBy=["smp_name","region"])

        smp_name= "Oocytes_1of4_2"
        region = "reg3p"

        if os.path.exists(segs_d):
            shutil.rmtree(segs_d, ignore_errors=False, onerror=None)

        os.mkdir(segs_d)
        reg3p_part_d = os.path.join(segs_d,"smp_name=%s" % smp_name,"region=%s" % region)

        src_part_d = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a083-x083-oocytes-e1/raw_seqs/%s/%s" % (smp_name, region)

        print(reg3p_part_d)
        print(src_part_d)
        shutil.copytree(src_part_d, reg3p_part_d, symlinks=False, ignore=None)

        spark.sql("msck repair table segs")

    @staticmethod
    def assign_bin(start, end):

        return binning.assign_bin(start,end)

    @staticmethod
    def overlapping_bins(bin):

        rng = binning.covered_interval(bin)
        rng_start, rng_stop = rng
        # print("rng: ",rng, rng_start, rng_stop)
        lst = binning.overlapping_bins(rng_start,rng_stop)
        # print("lst: ", lst)
        # st = ''.join(lst)
        # print("st: ", st)

        return lst

    @staticmethod
    def parse_r1(seq,qual):


# regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 2) as umi,
# regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 3) as seq_rest,

        match = re.search(r'^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', seq)
        if match:
            dbg=match.group(1)
        else:
            dbg=""

        dict = {'debug': dbg,
                'seq_seq': 'ACGT',
                'seq_qual': 'ZZZZ'}

        # print(dict)
        return dict

    def spark_fq2unal_parq(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="unal_parq"
        spark.sql("msck repair table %s" % tbl_name)


    def spark_parse_clasif_r1(self, smp_name, rnln):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # cmd2 = r"""
        # select qname,seq,qual
        # from unal_parq
        # where read_nb=1 and
        #       smp_name='wt' and
        #       rnln='run4815_1' and
        #       qname in ('D00280:347:HYHV3BCXY:1:1109:20524:37820','D00280:347:HYHV3BCXY:1:1109:20524:39225')
        #     	"""

        cmd2 = r"""
        select qname,seq,qual
        from unal_parq
        where read_nb=1 and
              smp_name = '%s' and
              rnln= '%s'
        """ % (smp_name, rnln)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)
        res_df.show(3, False)

        # tbl3c = res_df.collect()

        # for x in res_df.toLocalIterator():
        #     as_json=json.dumps(x.asDict(), sort_keys=False)
            # print("as_json: ", as_json)
            # print("tbl3: ", as_json)
        # print("tbl3c: ", tbl3c)

        rdd_json=res_df.rdd.map(lambda x: json.dumps(x.asDict(), sort_keys=False))

        # for x in rdd_json.toLocalIterator():
            # print("x: ", x)
            # as_json=json.dumps(x.asDict(), sort_keys=False)
            # print("as_json: ", as_json)
            # print("tbl4: ", x)

        # exit(0)

        # udf_parse_r1 = pyspfunc.udf(self.parse_r1, returnType=StructType(
        #     [StructField("debug", StringType(), True),
        #      StructField("seq_seq", StringType(), True),
        #      StructField("seq_qual", StringType(), True)])
        #                             )


        # tbl2 = res_df.withColumn('r1', udf_parse_r1('seq', 'qual')).select("r1.seq_qual","r1.debug")
        # tbl3 = tbl2.select("r1.*")

        # tbl3=res_df.rdd.pipe("wc -l").collect()
        # tbl3=res_df.rdd.pipe("awk '{print \"||\"$0\"||\"}'")

        # myrdd = sc.parallelize([(1,"4",33),(4,"5",6),(7,"8",9)], 3)
        # myrddc = myrdd.collect()
        # for y in myrdd.toLocalIterator():
        #     print("myrdd: ", y)
        # print("myrddc: ", myrddc)

        # df = myrdd.toDF(["a","b","c"])
        # df.show()

        # env = { "LD_LIBRARY_PATH" }

        # print(os.environ)

        pipe_env=os.environ
        pipe_env["PIPECMD_READ"]="1"
        # pipe_env["PIPECMD_REGION"]="H"

        params={"bchver": self._ctx.conf.biochem_ver}
        as_json=json.dumps(params, sort_keys=False)
        print("as_json: ", as_json)
        pipe_env["PIPECMD_OPTS"]=as_json

        # encode json instead of Row()
        tbl3=rdd_json.pipe("bsk_piperdd", pipe_env)


        # tbl3c = tbl3.collect()

        # for x in tbl3.toLocalIterator():
        #     print("tbl3: ", x)
        # print("tbl3c: ", tbl3c)

        # exit(1)
        # dir_f = "/home/badescud/scratch/test6/segs5c"
        # shutil.rmtree(dir_f, ignore_errors=True, onerror=None)
        # tbl3.saveAsTextFile(dir_f)

        tbl4=tbl3.map(lambda x: literal_eval(x))

        schema = StructType(
            [StructField("qname", StringType(),True),
             StructField("seq", StringType(),True),
             StructField("qual", StringType(),True),
             StructField("seq_typ", StringType(),True),
             StructField("seq_seq", StringType(),True),
             StructField("seq_qua", StringType(),True),
             StructField("umi_seq", StringType(),True),
             StructField("umi_qua", StringType(),True),
             StructField("ada_seq", StringType(),True),
             StructField("ada_qua", StringType(),True)
             ])

        # ndf = spark.createDataFrame(tbl3, schema=schema)

        # df = tbl3.toDF(["d","e","f"])
        # tbl4=tbl3.map(lambda x: (x, ))
        # for x in tbl4.toLocalIterator():
        #     print("tbl4: ", x)

        # df = tbl4.toDF(["z"])
        # df = tbl4.toDF(["a","b","c"])
        df5 = spark.createDataFrame(tbl4, schema=schema)
        df5.show(3, False)
        print("row cnt: ", df5.count())

        # dir_f = "/home/badescud/scratch/test6/segs5d"
        # shutil.rmtree(dir_f, ignore_errors=True, onerror=None)
        # df5.rdd.saveAsTextFile(dir_f)

            # tbl.repartition("ucscbin").write.option("path", segs_d).saveAsTable("segs4", format="parquet", mode="overwrite", partitionBy=["smp_name","reg_al","cigar_op_ref_name","ucscbin"])
        # df5.write.option("path", segs_d).saveAsTable("segs5", format="parquet", mode="overwrite")


        tbl_name="clasif_seqs_r1"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        prt_d="%s/smp_name=%s/rnln=%s" % (tbl_d,smp_name,rnln)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)

        df5.write.option("path", prt_d).mode("overwrite").partitionBy(["seq_typ"]).orc(prt_d)
        spark.sql("msck repair table %s" % tbl_name)

        return [0]


    def spark_parse_r2_join_r1(self, smp_name, rnln, seq_typ):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

      # qname in ('D00280:347:HYHV3BCXY:1:1101:10030:78591','D00280:347:HYHV3BCXY:1:1101:10034:67795')

        cmd2 = r"""
        select up.qname,
       up.seq,
       up.qual
from clasif_seqs_r1 cs1
 join unal_parq up on up.smp_name=cs1.smp_name and
                      up.rnln=cs1.rnln and
                      up.qname=cs1.qname
where up.read_nb=2 and
      cs1.smp_name='%s' and
      cs1.rnln='%s' and
      cs1.seq_typ='%s'
       """ % (smp_name, rnln, seq_typ)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)
        res_df.show(3, False)
        print("res_df row cnt: ", res_df.count())

        rdd_json=res_df.rdd.map(lambda x: json.dumps(x.asDict(), sort_keys=False))

        pipe_env=os.environ
        pipe_env["PIPECMD_READ"]="2"
        pipe_env["PIPECMD_REGION"]=seq_typ

        params={"bchver": self._ctx.conf.biochem_ver}
        as_json=json.dumps(params, sort_keys=False)
        print("as_json: ", as_json)
        pipe_env["PIPECMD_OPTS"]=as_json

        tbl3=rdd_json.pipe("bsk_piperdd", pipe_env)

        tbl4=tbl3.map(lambda x: literal_eval(x))

        schema = StructType(
            [StructField("qname", StringType(),True),
             StructField("seq", StringType(),True),
             StructField("qual", StringType(),True),
             StructField("seq_seq", StringType(),True),
             StructField("seq_qua", StringType(),True)
             ])

        df5 = spark.createDataFrame(tbl4, schema=schema).withColumn("seq_typ", lit(seq_typ))
        df5.show(3, False)
        print("df5 row cnt: ", df5.count())


        tbl_name="clasif_seqs_r2"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        prt_d="%s/smp_name=%s/rnln=%s/seq_typ=%s" % (tbl_d, smp_name, rnln, seq_typ)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)
        df5.write.option("path", prt_d).mode("overwrite").orc(prt_d)
        spark.sql("msck repair table %s" % tbl_name)

        #    join sequences
        cmd2 = r"""
            select cs1.qname,
            cs1.umi_seq,
           cs1.umi_qua,
           cs1.seq_seq as seq_seq1,
           cs1.seq_qua as seq_qua1,
            cs2.seq_seq as seq_seq2,
           cs2.seq_qua as seq_qua2
     from clasif_seqs_r1 cs1
      join clasif_seqs_r2 cs2 on cs2.smp_name=cs1.smp_name and
                                cs2.rnln=cs1.rnln and
                                cs2.seq_typ=cs1.seq_typ and
                                cs2.qname=cs1.qname
     where cs1.smp_name='%s' and
          cs1.rnln='%s' and
          cs1.seq_typ='%s'
          """ % (smp_name, rnln, seq_typ)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)
        res_df.show(3, False)

        tbl_name="clasif_seqs"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        prt_d="%s/smp_name=%s/rnln=%s/seq_typ=%s" % (tbl_d, smp_name, rnln, seq_typ)
        if not os.path.exists(prt_d):
            pathlib.Path(prt_d).mkdir(parents=True)

        # res_df.write\
        #     .option("parquet.block.size", 1073741824)\
        #     .option("path", prt_d)\
        #     .mode("overwrite").parquet(prt_d)

        res_df.write \
            .option("path", prt_d) \
            .mode("overwrite").orc(prt_d)


        spark.sql("msck repair table %s" % tbl_name)

        return [0]

    def spark_clasif_seqs_to_fq(self, smp_name, rnln, seq_typ):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # spark.sql("drop table if exists unal_5pfrag0")

        cmd2 = r"""
        select qname,umi_seq,umi_qua,
               seq_seq1,seq_qua1,
               seq_seq2,seq_qua2
        from clasif_seqs
        where smp_name = '%s' and
              rnln= '%s' and
              seq_typ='%s' and
              length(seq_seq1) > 30 and
              length(seq_seq2) > 30
        """ % (smp_name, rnln, seq_typ)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)

        # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_5pfrag0.fq")
        # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_5pfrag0.fq")

        tbl_d = self._ctx.spln_align_runreg_dir_glo

        out_f1=os.path.join(tbl_d, "%s_%s_%s.R1.fastq" % (smp_name, rnln, "%sP" % seq_typ))
        out_f2=os.path.join(tbl_d, "%s_%s_%s.R2.fastq" % (smp_name, rnln, "%sP" % seq_typ))
        print ("out_f :", out_f1)

        # out_h1 = gzip.open(out_f1, "w")
        # out_h2 = gzip.open(out_f2, "w")

        out_h1 = open(out_f1, "w")
        out_h2 = open(out_f2, "w")

        for x in res_df.rdd.toLocalIterator():

            str1 = """@%(qname)s 1:N:0:%(umi_seq)s:%(umi_qua)s\n%(seq)s\n+\n%(qual)s\n"""\
                   % {"qname": x.qname,
                           "umi_seq": x.umi_seq,
                           "umi_qua": x.umi_qua,
                           "seq": x.seq_seq1,
                           "qual": x.seq_qua1
                           }

            # out_h1.write(str1.encode('utf-8'))
            out_h1.write(str1)

            str2 = """@%(qname)s 2:N:0:%(umi_seq)s:%(umi_qua)s\n%(seq)s\n+\n%(qual)s\n"""\
                   % {"qname": x.qname,
                           "umi_seq": x.umi_seq,
                           "umi_qua": x.umi_qua,
                           "seq": x.seq_seq2,
                           "qual": x.seq_qua2
                           }

            # out_h2.write(str2.encode('utf-8'))
            out_h2.write(str2)
        out_h1.close()
        out_h2.close()

        return [0]


    def spark_alipark2bin_segs(self, reg_al):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # tbl.printSchema()

        udf_assign_bin = pyspfunc.udf(self.assign_bin, returnType=IntegerType())

        in_f  = os.path.join(self._ctx.spln_align_runreg_dir_glo, "%s/aliparq" % (reg_al))

        tbl_name="segs"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        prt_d = os.path.join(tbl_d, "smp_name=%s/rnln=%s/reg_al=%s" % (self._ctx.spln_smp,self._ctx.spln_runreg,reg_al))

        # if not os.path.exists(prt_d):
        #     pathlib.Path(prt_d).mkdir(parents=True)

        # tbl = spark.read.parquet(in_f).limit(10)
        # tbl.show()
        tbl = spark.read.parquet(in_f)
        tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('cigar_op_ref_start', 'cigar_op_ref_end'))
        # tbl2.show()

        # tbl.write.mode("append").partitionBy("cigar_op_ref_name").parquet(out_f)
        print("prt_d: ", prt_d)
        # pip3 install py4j==0.10.7
        # pip3 install pyspark==2.3.1

        # old command
        # tbl2.repartition("cigar_op_ref_name")\
        #     .sortWithinPartitions(["cigar_op_ref_start","ucscbin"],ascending=[True,False])\
        #     .write.option("path", prt_d)\
        #     .option("parquet.block.size", 1073741824)\
        #     .mode("overwrite")\
        #     .partitionBy("cigar_op_ref_name")\
        #     .orc(prt_d)
        # tbl.repartition("seqname") \
        #     .sortWithinPartitions(["tr_start", "tr_end", "transcript_id", "strand", "start", "end", "exon_id"],
        #                           ascending=[True, True, True, True, True, True, True]) \
        #     .write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["seqname"], mode="overwrite")

        tbl2.repartition("cigar_op_ref_name")\
            .sortWithinPartitions(["cigar_op_ref_start","ucscbin"],ascending=[True,True])\
            .write.option("path", prt_d)\
            .mode("overwrite")\
            .partitionBy("cigar_op_ref_name")\
            .orc(prt_d)

        # withucsbins partitioning command -- start
        # tbl2.repartition(800,["cigar_op_ref_name","ucscbin"]) \
        #     .sortWithinPartitions(["cigar_op_ref_name","ucscbin","cigar_op_ref_start", "cigar_op_ref_end"], ascending=[True, True,True, True]) \
        #     .write.option("path", prt_d) \
        #     .mode("overwrite") \
        #     .partitionBy(["cigar_op_ref_name","ucscbin"]) \
        #     .orc(prt_d)
        # withucsbins partitioning command --end

        spark.sql("msck repair table %s" % tbl_name)

        return [0]


    def spark_work_umi_schemas_segs(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # tbl.printSchema()

        udf_assign_bin = pyspfunc.udf(self.assign_bin, returnType=IntegerType())

        # spark.sql("truncate table segs")

        # append for each runlane to the segs of the aligned region
        for smp in ["wt"]:
            # for rnln in ["run001_1", "run001_2"]:
            for rnln in ["run4815_1"]:

                in_f  = os.path.join(self._ctx.conf.proj_dir, "raw_reads/%s/%s/reg5pse" % (smp, rnln))
                # out_f = os.path.join(test_d, "srvf/hivef/segs/smp_name=%s/reg_al=reg5pse/rnln=%s" % (smp,rnln))
                # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/hivef/segs/smp_name=%s/reg_al=reg5pse/rnln=%s" % (smp,rnln))
                tbl_name="segs"
                tbl_d = "/home/badescud/scratch/test6/%s" % tbl_name

                out_f = os.path.join(tbl_d, "smp_name=%s/reg_al=reg5pse/rnln=%s" % (smp,rnln))


                # tbl = spark.read.parquet(in_f).limit(10)
                # tbl.show()
                tbl = spark.read.parquet(in_f)
                tbl2 = tbl.withColumn('ucscbin', udf_assign_bin('cigar_op_ref_start', 'cigar_op_ref_end'))
                # tbl2.show()

                # tbl.write.mode("append").partitionBy("cigar_op_ref_name").parquet(out_f)
                print("out_f: ", out_f)
                # pip3 install py4j==0.10.7
                # pip3 install pyspark==2.3.1
                tbl2.repartition("cigar_op_ref_name").sortWithinPartitions(["cigar_op_ref_start","ucscbin"],ascending=[True,False]).write.option("path", out_f).mode("overwrite").partitionBy("cigar_op_ref_name").parquet(out_f)
        spark.sql("msck repair table segs")

    def spark_work_umi_schemas_segs2(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl = spark.table("segs")

        spark.sql("drop table if exists segs4")

        spark.sql("SET hive.merge.sparkfiles = true")
        spark.sql("SET hive.merge.mapredfiles = true")
        spark.sql("SET hive.merge.mapfiles = true")


        segs_d = "/home/badescud/scratch/test6/segs4"
        tbl.repartition("ucscbin").write.option("path", segs_d).saveAsTable("segs4", format="parquet", mode="overwrite", partitionBy=["smp_name","reg_al","cigar_op_ref_name","ucscbin"])

        # spark.sql("truncate table segs")


    def spark_table_to_schema(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        spark.sql("drop table if exists unal_5pfrag0")

        # tbl = spark.table("unal_5pfrag0")
        # schema_json = tbl.schema.json()
        # out_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "unal_5pfrag0.json")
        # with open(out_f, "w") as text_file:
        #     text_file.write(schema_json)
        # print("schema_json: ", schema_json)

        # in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "reg5p.snappy.parquet")
        # tbl = spark.read.parquet(in_f).where("1=0")
        # schema_json = tbl.schema.json()
        # out_f = os.path.join(self._ctx.conf.proj_dir, "templ/hive", "reg5p_schema.json")
        # with open(out_f, "w") as text_file:
        #     text_file.write(schema_json)
        # print("schema_json: ", schema_json)



    def spark_work_umis_schemas_unal_5pfrag0(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext


        # spark.sql("truncate table unal_5pfrag0")

        smp_name = 'wt'
        # smp_name = "Oocytes_1of4_2"

        rnln = 'run4815_1'
        self.spark_load_umis_unal_5pfrag0_lane(smp_name, rnln)

        # self.spark_load_umis_unal_5pfrag0_lane(smp_name, "run001_1")
        # self.spark_load_umis_unal_5pfrag0_lane("Oocytes_1of4_2", "run001_2")

        spark.sql("msck repair table unal_5pfrag0")





        #
        # tbl.printSchema()

    def spark_load_umis_unal_5pfrag0_lane(self, smp_name, rnln):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd = """
        select qname,umi,seq_cor,qa
from
(
select qname,
       umi,
case regexp_extract(seq_rest, '^(.*)(A{12,})', 2) != ''
WHEN true
THEN regexp_extract(seq_rest, '^(.*)(A{12,})', 1)
ELSE seq_rest
END as seq_cor,
qa
from
(
select  qname,
regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 2) as umi,
regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 3) as seq_rest,
regexp_extract(qanno, '^1:([N/Y]):0',1) as qa
from unal_parq
where regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 2) != '' and
      regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 1) = '' and
read_nb=1 and
smp_name = '%s' and
rnln= '%s'
)
)
where length(seq_cor)> 50
""" % (smp_name, rnln)
        print(cmd)
        tbl = spark.sql(cmd)

        tbl_name="unal_5pfrag0"
        tbl_d = "/home/badescud/scratch/test6/%s" % tbl_name

        # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/hivef/unal_5pfrag0/smp_name=%s/rnln=%s" % (smp_name, rnln))
        out_f = os.path.join(tbl_d, "smp_name=%s/rnln=%s" % (smp_name, rnln))
        tbl.write.option("path", tbl_d).mode("overwrite").parquet(out_f)




    def spark_deconvol_umis(self, smp_name, rnln):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        spark.sql("drop table if exists unal_3pfrag0")
        # this is before reformat only before space
        # regexp_extract(qname, '^(.*)\\\\s+(.*)+', 1) as qname_orig,
        cmd = """create table unal_3pfrag0 as
                select qname,
                       regexp_extract(seq, '^(.*)(.{10})+AC(?:T{20,})', 1) as adapt,
                       regexp_extract(seq, '^(.*)(.{10})+AC(?:T{20,})', 2) as umi,
                       regexp_extract(qanno, '^1:([N/Y]):0',1) as qa
                from unal_parq
                where regexp_extract(seq, '^(.*)(.{10})+AC(?:T{20,})', 2) != '' and
                        read_nb=0 and
                        smp_name = '%s' and
                        rnln= '%s'
                """ % (smp_name, rnln)
        print(cmd)
        spark.sql(cmd)

        spark.sql("drop table if exists unal_3pseq0")
        #
        cmd = """create table unal_3pseq0 as
                select qname,
                        CASE regexp_extract(seq, '^(.*?)(A{20,})', 1) != ''
                        WHEN true THEN regexp_extract(seq, '^(.*?)(A{20,})', 1)
                        ELSE regexp_extract(seq, '.{20}(.*)', 1)  end as seq_cor
                from unal_parq
                where qname in (select qname
                                from unal_3pfrag0) and
                        read_nb=1 and
                        smp_name = '%s'
                """ % smp_name
        print(cmd)
        spark.sql(cmd)

        cmd2 = r"""
        select qname,seq_cor
        from unal_3pseq0
        where length(seq_cor) > 50
            	"""
        print("cmd2: ", cmd2)

        # res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_3pseq0.fq")
        out_h = open(out_f, "w")

        # for x in res_df.rdd.toLocalIterator():
        #     out_h.write("""@%(qname)s\n%(seq)s\n+\n%(qual)s\n"""
        #                 % {"qname": x.qname,
        #                    "seq": x.seq_cor,
        #                    "qual": "F"*len(x.seq_cor)
        #                    })
        #
        # out_h.close()

        # spark.sql("drop table if exists unal_5pfrag0")

        cmd = """create table unal_5pfrag0 as
select qname_orig,umi,seq_cor
from
(
select qname_orig,
       umi,
case regexp_extract(seq_rest, '^(.*)(A{12,})', 2) != ''
WHEN true
THEN regexp_extract(seq_rest, '^(.*)
ELSE seq_rest
END as seq_cor
from
(
select  regexp_extract(qname, '^(.*)\\\\s+(.*)+', 1) qname_orig,
regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 2) as umi,
regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 3) as seq_rest
from parq_arrow
where regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 2) != '' and
      regexp_extract(seq, '^(.*)(.{10})+(?:ACGC)(?:G{3,9})(.*)', 1) = '' and
read_nb=1
)
)
where length(seq_cor)> 50
"""
        print(cmd)
        # spark.sql(cmd)

        cmd2 = r"""
        select qname_orig,seq_cor
        from unal_5pfrag0
            	"""
        print("cmd2: ", cmd2)

        # res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_5pfrag0.fq")
        out_h = open(out_f, "w")

        # for x in res_df.rdd.toLocalIterator():
        #     out_h.write("""@%(qname)s\n%(seq)s\n+\n%(qual)s\n"""
        #                 % {"qname": x.qname_orig,
        #                    "seq": x.seq_cor,
        #                    "qual": "F"*len(x.seq_cor)
        #                    })

        out_h.close()

    def spark_deconvol_umis_reg5p_lane(self, smp_name, rnln):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # spark.sql("drop table if exists unal_5pfrag0")



        cmd2 = r"""
        select qname,seq_cor
        from unal_5pfrag0
        where smp_name = '%s' and
              rnln= '%s'
            	""" % (smp_name, rnln)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)

        # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_5pfrag0.fq")
        # out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "unal_5pfrag0.fq")

        out_f=os.path.join(self._ctx.conf.proj_dir,"raw_reads",smp_name,rnln, "unal_5pfrag0.fq")
        print ("out_f :", out_f)
        out_h = open(out_f, "w")


        for x in res_df.rdd.toLocalIterator():
            out_h.write("""@%(qname)s\n%(seq)s\n+\n%(qual)s\n"""
                        % {"qname": x.qname,
                           "seq": x.seq_cor,
                           "qual": "F"*len(x.seq_cor)
                           })

        out_h.close()


    def spark_deconvol_umis2(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        spark.sql("drop table if exists umis0")
        cmd = """create table umis0 as
       select gn_umi_cnt.gene_id,
       gn.gene_name,
       gn_umi_cnt.dif_umi
from
(
select umis_per_gene.gene_id,
       count(*) as dif_umi
from
(
select umis.gene_id,
       umis.umi,
       count(*) as umi_per_gene
from
(
select ex.exon_id,
       ex.seqname,
       ex.start,
       ex.end,
       abs.qname,
       fr.umi,
       tr.transcript_id,
       tr.gene_id
from exons ex
 join segs abs on abs.cigar_op_ref_name=ex.seqname and
                          ((abs.cigar_op_ref_start < ex.start and abs.cigar_op_ref_end >= ex.start) or
                           (abs.cigar_op_ref_start >= ex.start and abs.cigar_op_ref_end <= ex.end))
 join unal_3pfrag0 fr on fr.qname=abs.qname
 join transcript_exons te on te.exon_id=ex.exon_id
 join transcripts tr on tr.transcript_id=te.transcript_id and
                        tr.strand = abs.strand
) umis
group by umis.gene_id, umis.umi
) umis_per_gene
group by umis_per_gene.gene_id
) gn_umi_cnt
   join genes gn on gn.gene_id = gn_umi_cnt.gene_id
order by dif_umi desc,
         gene_id
        """
        print(cmd)
        spark.sql(cmd)

    def spark_valid_bins(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext


        # print(self.overlapping_bins(840))

        # https://changhsinlee.com/pyspark-udf/

        overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=ArrayType(IntegerType()))
        # overlapping_bins_list_udf = pyspfunc.udf(self.overlapping_bins, returnType=StringType())

        cmd = """
       select seqname,
              ucscbin,
              count(*) as exonsperbin
 from exons
 group by seqname,ucscbin
 order by seqname,ucscbin"""
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()
        tbl2 = tbl.withColumn('overbins', overlapping_bins_list_udf('ucscbin'))
        tbl2.show()
        spark.sql("drop table if exists svb3")
        tbl2.write.saveAsTable("svb3", format="parquet", mode="overwrite")

        cmd="""
 select seqname,
        ucscbin,
        overbins,
        overbin
 from svb3
 lateral view explode(overbins) svb2 as overbin
        """
        print(cmd)

        tbl = spark.sql(cmd)
        tbl.show()
        spark.sql("drop table if exists svb4")
        tbl.write.saveAsTable("svb4", format="parquet", mode="overwrite")



    ###3        # spark.select('ucscbin', square_list_udf('integer_arrays')).show()
    #####    # spark.catalog.dropTempView("svb")
    #####    # tbl2.registerTempTable("svb")


    def spark_deconvol_umis_reg_al(self, seq_typ, reg_al,feat_mdl):


        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="umis"
        # spark.sql("drop table if exists %s" % tbl_name)
        # in_f= os.path.join(self._ctx.conf.proj_dir, "templ/hive", "%s_schema.json" % tbl_name)
        # file = open(in_f, "r")
        # schema_json = file.read()
        # new_schema = StructType.fromJson(json.loads(schema_json))
        # print("new_schema: ", new_schema)
        # df = spark.createDataFrame(sc.emptyRDD(), new_schema)
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # print("tbl_d: ", tbl_d)
        # df.write.option("path", tbl_d).saveAsTable(tbl_name, format="parquet", mode="overwrite", partitionBy=["smp_name","reg_al","chrom"])

        # if os.path.exists(tbl_d):
        #     shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)


        # spark.sql("truncate table if exists gncnts")

        # cs+sg
        cmd="""
        select cs.umi_seq,
               cs.qname,
               cs.rnln,
               sg.cigar_op_ref_name,
               sg.cigar_op_ref_start,
               sg.cigar_op_ref_end,
               sg.ucscbin,
               sg.strand
        from clasif_seqs cs
         join segs sg on cs.rnln=sg.rnln and
                         cs.qname=sg.qname
        where cs.smp_name='%(smp_name)s' and
              sg.smp_name='%(smp_name)s' and
              cs.seq_typ='%(seq_typ)s' and
              sg.reg_al='%(reg_al)s'
        """ % {"smp_name": self._ctx.smp_name,
               "seq_typ": seq_typ,
               "reg_al": reg_al}
        print(cmd)
        tbl = spark.sql(cmd)
        #
        tbl_name="cssg"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["cigar_op_ref_name"], mode="overwrite")

        cmd="""
 select ft.seqname as chrom,
        ft.ucscbin as ftbin,
        ft.overbin as sgbin
 from feat_mdl_bins ft
 where ft.feat_mdl='%(feat_mdl)s' and
        (ft.seqname,ft.overbin) in (select cigar_op_ref_name,ucscbin
                                    from cssg)
        """ %{"feat_mdl": feat_mdl}
        print(cmd)
        tbl = spark.sql(cmd)

        tbl_name="ftsgs"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)

        spark.sql("drop table if exists %s" % tbl_name)
        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", partitionBy=["chrom"], mode="overwrite")
        # spark.sql("msck repair table %s" % tbl_name)

        cmd="""
        select distinct chrom
        from ftsgs
        """
        print(cmd)
        tbl = spark.sql(cmd)
        res_df=tbl.rdd.map(lambda row: row.asDict()["chrom"])
        chroms = res_df.collect()

        tbl3=tbl.collect()

        for chrom in chroms:
            print("chrom: ", chrom)

            cmd="""
            select chrom,
                   ftbin,
                   sgbin
            from ftsgs
            where chrom = '%(chrom)s'
            """ % {"chrom": chrom}
            # print(cmd)
            tbl = spark.sql(cmd)
            tbl_name="chrftsg"
            tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
            spark.sql("drop table if exists %s" % tbl_name)
            tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

            # join STRANDED
            cmd = """
            select  ftsg.ftbin,
                    ftsg.sgbin,
                    ft.feat_id,
                    ft.start,
                    ft.end,
                    sg.qname,
                    sg.rnln,
                    sg.umi_seq,
                    ft.gene_id
            from chrftsg ftsg
             join feats ft on ft.ucscbin=ftsg.ftbin
             join cssg sg on sg.ucscbin=ftsg.sgbin and
                             sg.strand=ft.strand and
                             ((sg.cigar_op_ref_start < ft.start and sg.cigar_op_ref_end >= ft.start) or
                             (sg.cigar_op_ref_start >= ft.start and sg.cigar_op_ref_end <= ft.end))
            where   ft.seqname='%(chrom)s' and
                    sg.cigar_op_ref_name='%(chrom)s'
            """ % {"chrom": chrom}

            # print(cmd)
            tbl = spark.sql(cmd)
            # tbl.show()

            tbl_name="umis"
            tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)

            prt_d = os.path.join(tbl_d, "smp_name=%s/reg_al=%s/chrom=%s" % (self._ctx.smp_name, reg_al, chrom))
            print("prt_d: ", prt_d)
            tbl.write.option("path", prt_d).mode("overwrite").orc(prt_d)

        tbl_name="umis"
        spark.sql("msck repair table %s" % tbl_name)

        cmd = """
select '%(smp_name)s' as smp_name,
       gn_umi_cnt.gene_id,
       gn.gene_name,
       cast(gn_umi_cnt.dif_umi as double) as cnt
from
(
select umis_per_gene.gene_id,
       count(*) as dif_umi
from
(
select umis.gene_id,
       umis.umi_seq,
       count(*) as umi_per_gene
from umis
where smp_name='%(smp_name)s' and
      reg_al='%(reg_al)s'
group by umis.gene_id, umis.umi_seq
) umis_per_gene
group by umis_per_gene.gene_id
) gn_umi_cnt
   join genes gn on gn.gene_id = gn_umi_cnt.gene_id
order by dif_umi desc,
         gene_id
        """ % {"smp_name": self._ctx.smp_name,
               "reg_al": reg_al}

        print(cmd)
        tbl = spark.sql(cmd)
        tbl.show()
        print("nbrows: ",tbl.count())

        # umis = U, Deduplicated =D
        cnt_typ="UD%s" % reg_al
        tbl_name="gncnts"
        tbl_d=os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        prt_d = os.path.join(tbl_d, "feat_mdl=%s/cnt_typ=%s/smp_name=%s" % (feat_mdl,cnt_typ, self._ctx.smp_name))
        print("prt_d: ", prt_d)
        tbl.write.option("path", prt_d).mode("overwrite").orc(prt_d)
        print("in msck..")
        spark.sql("msck repair table %s" % tbl_name)

        return [0]

    def spark_unpart_clasif_seqs(self):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        #tbl_name = "clasif_seqs2"

        cmd = """
                select * 
                from clasif_seqs
                """
        #print(cmd)
        #tbl = spark.sql(cmd).coalesce(200).repartition("smp_name")
        #tbl.show()
        #tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        #tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",partitionBy=["smp_name"])

        tbl_name = "clasif_seqs"
        spark.sql("drop table if exists %s" % tbl_name)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # cmd = """ls -1 %(dir)s | parallel --max-args=1 echo %(dir)s/{1}""" % {"dir": tbl_d}
        cmd = """ls -1 %(dir)s | parallel --max-args=1 rm -fr %(dir)s/{1}""" % {"dir": tbl_d}
        print(cmd)
        os.system(cmd)

        return [0]
    
    # https://www.2daygeek.com/linux-check-count-inode-usage/
    # [dbadescu@abacus2 rnvar]$ echo "Detailed Inode usage for: $(pwd)" ; for d in `find -maxdepth 1 -type d |cut -d\/ -f2 |grep -xv . |sort`; do c=$(find $d |wc -l) ; printf "$c\t\t- $d\n" ; done ; printf "Total: \t\t$(find $(pwd) | wc -l)\n"
    def spark_unpart_clasif_seqs_r2(self):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name = "clasif_seqs2_r2"

        cmd = """
                select * 
                from clasif_seqs_r2
                """
        # print(cmd)
        # tbl = spark.sql(cmd).coalesce(200).repartition("smp_name")
        # tbl.show()
        # tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",partitionBy=["smp_name"])

        tbl_name = "clasif_seqs_r2"
        spark.sql("drop table if exists %s" % tbl_name)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        cmd = """ls -1 %(dir)s | parallel --max-args=1 rm -fr %(dir)s/{1}""" % {"dir": tbl_d}
        print(cmd)
        os.system(cmd)
        #
        return [0]


    def spark_unpart_umis(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        tbl_name="umis2"

        cmd = """
                select * 
                from umis
                """
        # print(cmd)
#        tbl = spark.sql(cmd).coalesce(200)
 #        tbl.show()

  #      tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
  #      spark.sql("drop table if exists %s" % tbl_name)
  #        tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite")

        tbl_name="umis3"

        cmd = """
            select * 
            from umis2
            """
        print(cmd)
        # tbl = spark.sql(cmd).repartition(10)
        #tbl = spark.sql(cmd).repartition("smp_name","reg_al")

        #tbl.show()
        #tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        #tbl.write.option("path", tbl_d).saveAsTable(tbl_name, format="orc", mode="overwrite",partitionBy=["smp_name","reg_al"])

        tbl_name = "umis"
        spark.sql("drop table if exists %s" % tbl_name)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        #if os.path.exists(tbl_d):
        #    shutil.rmtree(tbl_d, ignore_errors=False, onerror=None)

        cmd="""ls -1 %(dir)s | parallel --max-args=1 ls %(dir)s/{1}""" \
            % {"dir": tbl_d}
        print(cmd)

        os.system(cmd)


        return [0]



        # cmd =
        """
select '%(smp_name)s' as smp_name,
       gn_umi_cnt.gene_id,
       gn.gene_name,
       cast(gn_umi_cnt.dif_umi as double) as cnt
from
(
select umis_per_gene.gene_id,
       count(*) as dif_umi
from
(
select umis.gene_id,
       umis.umi_seq,
       count(*) as umi_per_gene
from
(
select exsg.chrom,
       exsg.exbin,
       exsg.sgbin,
       ex.exon_id,
       ex.seqname,
       ex.start,
       ex.end,
       sg.qname,
       sg.rnln,
       cs.umi_seq,
       tr.transcript_id,
       tr.gene_id
 from exsgs exsg
  join exons ex on ex.seqname=exsg.chrom and
                   ex.ucscbin=exsg.exbin
  join segs sg on sg.cigar_op_ref_name=exsg.chrom and
                  sg.ucscbin=exsg.sgbin
  join clasif_seqs cs on cs.smp_name=sg.smp_name and
                          cs.rnln=sg.rnln and
                          cs.qname=sg.qname and
                          cs.seq_typ='%(seq_typ)s'
  join transcript_exons te on te.exon_id=ex.exon_id
  join transcripts tr on tr.transcript_id=te.transcript_id and
                        tr.strand = sg.strand
 where sg.smp_name='%(smp_name)s' and
       sg.reg_al='%(reg_al)s' and
       ((sg.cigar_op_ref_start < ex.start and sg.cigar_op_ref_end >= ex.start) or
       (sg.cigar_op_ref_start >= ex.start and sg.cigar_op_ref_end <= ex.end))
) umis
group by umis.gene_id, umis.umi_seq
) umis_per_gene
group by umis_per_gene.gene_id
) gn_umi_cnt
   join genes gn on gn.gene_id = gn_umi_cnt.gene_id
order by dif_umi desc,
         gene_id   # % {"smp_name": self._ctx.smp_name,
        #    "seq_typ": seq_typ,
        #    "reg_al": reg_al}
    """

    @staticmethod
    def f3(ix,it):

        lst = []
        for i in it:
            i["prtix"]=ix
            lst.append(i)
        return iter(lst)

    def spark_gncnts_unpiv(self, gntbl,mtx_name,cnt_typ,feat_mdl):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # cmd2 = r"""
        # select gene_id,
        #         gene_name,
        #         smp_name,
        #         cnt
        # from gncnts
        # where cnt_typ='%s' and
        #       gene_id < 'ENSMUSG00000000058.6'
        # """ % (cnt_typ)

        cmd = r"""
    select distinct smp_name
    from %s
    where cnt_typ='%s' and
          feat_mdl='%s'
    order by smp_name
    """ % (gntbl, cnt_typ,feat_mdl)

        print("cmd: ", cmd)

        res_df = spark.sql(cmd)
        res_df.show(20, True)

        res_df=res_df.rdd.map(lambda row: row.asDict()["smp_name"])
        smps = res_df.collect()
        print("smps: ", smps)

        # smps=[]
        # for x in res_df.toLocalIterator():
        #     smps.append(x)

        params={"smp_names": smps}
        as_json=json.dumps(params, sort_keys=False)
        print("as_json: ", as_json)

        cmd2 = r"""
        select gene_id,
                gene_name,
                smp_name,
                cnt
        from %s
        where cnt_typ='%s' and
              feat_mdl='%s'
        """ % (gntbl,cnt_typ,feat_mdl)

        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)
        res_df.show(20, False)

        pipe_env=os.environ
        # _keys = ('smp_names', 'name', 'value')
        # _values = (["wt","mut"], 'name', 'value')
        # as_msgpack_b = msgpack.packb(_values, use_bin_type=True)
        # as_msgpack_s = as_msgpack_b.decode(encoding='utf-8', errors='strict')
        # params={"smp_names": ["wt","mut"]}
        # as_json=json.dumps(params, sort_keys=False)
        pipe_env["PIPECMD_OPTS"]=as_json

        print("as_json: ", as_json)

        # dataframe.repartition('id') creates 200 partitions with ID partitioned based on Hash Partitioner.
        #  Dataframe Row's with the same ID always goes to the same partition.
        # If there is DataSkew on some ID's, you'll end up with inconsistently sized partitions.

        # tbl3=res_df.repartition(["gene_id"]).rdd.mapPartitionsWithIndex(lambda ix, it: [(ix, list(it))])
        # tbl3=res_df.repartition(4,"gene_id").rdd.mapPartitionsWithIndex(lambda ix, it: [(ix, ["a","b","c"])])
        tbl3=res_df.repartition(2,"gene_id").rdd.map(lambda row: row.asDict())
        tbl4=tbl3.mapPartitionsWithIndex(self.f3, preservesPartitioning=True)
        tbl5=tbl4.map(lambda row: json.dumps(row, sort_keys=False))
            # .rdd.mapPartitionsWithIndex(lambda ix, it: [(ix, ["a","b","c"])])
        # tbl3=res_df.rdd.map(lambda x: [x.gene_id, x.asDict()])
        # tbl4=tbl3.partitionBy(1000)

        tbl6=tbl5.pipe("smp_cnt_unpiv",
                             pipe_env)
        tbl7=tbl6.map(lambda x: literal_eval(x))

        schema_a = []
        schema_a.append(StructField("gene_id", StringType(),True))
        schema_a.append(StructField("gene_name", StringType(),True))
        for smp in smps:
            schema_a.append(StructField(smp, DoubleType(),True))


        schema = StructType(schema_a)
        df5 = spark.createDataFrame(tbl7, schema=schema)
        df5.show(20, False)

        tbl_name="udhp"
        spark.sql("drop table if exists %s" % tbl_name)
        tbl_d = os.path.join(self._ctx.hivef_dir_glo, tbl_name)
        # out_f = os.path.join(tbl_d, "smp_name=%s/rnln=%s" % (smp_name,rnln))
        out_f = tbl_d
        print("out_f: ", out_f)
        # tbl.write.option("path", tbl_d).mode("overwrite").parquet(out_f)
        df5.write.option("path", out_f).saveAsTable(tbl_name, format="parquet", mode="overwrite")

        dfp=df5.toPandas()
        print("dfp: \n", dfp)
        dfp=dfp.sort_values(by=['gene_id'], ascending=True)

        out_f = os.path.join(self._ctx.conf.matr_dir,"%s_%s_%s.csv" %(mtx_name, cnt_typ, feat_mdl))
        dfp.to_csv(out_f, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        return [0]

        # df5.write.option("path", out_f).mode("overwrite").parquet(out_f)
        # spark.sql("msck repair table %s" % tbl_name)

        # tbl3c = tbl7.collect()
        # print("tbl3c: ", tbl3c)

        # for x in tbl3.toLocalIterator():
        #     print("tbl3: ", x)


        # udf_parse_r1 = pyspfunc.udf(self.parse_r1, returnType=StructType(
        #     [StructField("debug", StringType(), True),
        #      StructField("seq_seq", StringType(), True),
        #      StructField("seq_qual", StringType(), True)])
        #                             )


        # tbl2 = res_df.withColumn('r1', udf_parse_r1('seq', 'qual')).select("r1.seq_qual","r1.debug")
        # tbl3 = tbl2.select("r1.*")

        # tbl3=res_df.rdd.pipe("wc -l").collect()
        # tbl3=res_df.rdd.pipe("awk '{print \"||\"$0\"||\"}'")

        # myrdd = sc.parallelize([(1,"4",33),(4,"5",6),(7,"8",9)], 3)
        # myrddc = myrdd.collect()
        # for y in myrdd.toLocalIterator():
        #     print("myrdd: ", y)
        # print("myrddc: ", myrddc)

        # df = myrdd.toDF(["a","b","c"])
        # df.show()

        # env = { "LD_LIBRARY_PATH" }

        # print(os.environ)



        # exit(1)
        # dir_f = "/home/badescud/scratch/test6/segs5c"
        # shutil.rmtree(dir_f, ignore_errors=True, onerror=None)
        # tbl3.saveAsTextFile(dir_f)

        # tbl4=tbl3.map(lambda x: literal_eval(x))



        # ndf = spark.createDataFrame(tbl3, schema=schema)

        # df = tbl3.toDF(["d","e","f"])
        # tbl4=tbl3.map(lambda x: (x, ))
        # for x in tbl4.toLocalIterator():
        #     print("tbl4: ", x)

        # df = tbl4.toDF(["z"])
        # df = tbl4.toDF(["a","b","c"])

        # dir_f = "/home/badescud/scratch/test6/segs5d"
        # shutil.rmtree(dir_f, ignore_errors=True, onerror=None)
        # df5.rdd.saveAsTextFile(dir_f)

            # tbl.repartition("ucscbin").write.option("path", segs_d).saveAsTable("segs4", format="parquet", mode="overwrite", partitionBy=["smp_name","reg_al","cigar_op_ref_name","ucscbin"])
        # df5.write.option("path", segs_d).saveAsTable("segs5", format="parquet", mode="overwrite")

        # tbl_name="clasif_seqs_r1"
        # tbl_d = "/home/badescud/scratch/test6/%s" % tbl_name
        # out_f = os.path.join(tbl_d, "smp_name=%s/rnln=%s" % (smp_name,rnln))
        # print("out_f: ", out_f)
        # tbl.write.option("path", tbl_d).mode("overwrite").parquet(out_f)
        # df5.write.option("path", out_f).saveAsTable(tbl_name, format="parquet", mode="overwrite", partitionBy=["seq_typ"])
        # df5.write.option("path", out_f).mode("overwrite").partitionBy(["seq_typ"]).parquet(out_f)
        # spark.sql("msck repair table %s" % tbl_name)



        # createDataFrame
        # schema = StructType(
        #     [StructField("qn", StringType(),True),
        #      StructField("seq", StringType(), True)])
        # ndf = spark.createDataFrame(tbl3, schema=schema)
        # ndf = sc.to_df()

        # ndf.show()


        # for x in ndf.toLocalIterator():
        #     print("x: ", x)

        # tbl3.printSchema()
        # tbl3.show()


    def spark_save_umis0(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd2 = r"""
        select gene_id,
        gene_name,
        dif_umi
    from tst
    order by dif_umi desc,
         gene_id
            	"""
        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "umis0.csv")
        out_h = open(out_f, "w")

        for x in res_df.rdd.toLocalIterator():
            out_h.write("""%(gene_id)s,%(gene_name)s,%(dif_umi)s\n"""
                        % {"gene_id": x.gene_id,
                           "gene_name": x.gene_name,
                           "dif_umi": x.dif_umi
                           })

        out_h.close()


    def spark_save_umis5p(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd2 = r"""
        select gene_id,
        gene_name,
        dif_umi
    from tst
    order by dif_umi desc,
         gene_id
            	"""
        print("cmd2: ", cmd2)

        res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "wt_umis5p_v3.csv")
        out_h = open(out_f, "w")

        for x in res_df.rdd.toLocalIterator():
            out_h.write("""%(gene_id)s,%(gene_name)s,%(dif_umi)s\n"""
                        % {"gene_id": x.gene_id,
                           "gene_name": x.gene_name,
                           "dif_umi": x.dif_umi
                           })

        out_h.close()



    def spark_save_gtf_csv(self):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # cmd = r"""
        # select *
        # from gtf_exons
        # where seqname='%s'
        # order by chr_idx,start,end
        #     	"""\
        #           % ("chr5")
        # print("cmd: ", cmd)

        cmd2 = r"""
        select *
        from gtf_exons
        order by chr_idx,start,end
            	"""
        print("cmd2: ", cmd2)

        # res_df = spark.sql(cmd)
        res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gencode-exons.csv")
        out_h = open(out_f, "w")

        for x in res_df.rdd.toLocalIterator():
            # print("x :", x)
            # out_h.write("""%(smp)s\t%(smp)s-%(clr)s\n""" % {"smp": smp, "clr": caller})
            # """chr    HAVANA  exon    57289845        57290016        .       +       .       gene_id "ENSMUSG00000026827.12"; transcript_id "ENSMUST00000141536.7"; gene_type "protein_coding"; gene_name "Gpd2"; transcript_type "processed_transcript"; transcript_name "Gpd2-204"; exon_number 3; exon_id "ENSMUSE00001311614.1"; level 2; transcript_support_level "5"; havana_gene "OTTMUSG00000012561.1"; havana_transcript "OTTMUST00000030013.1";
# """
            out_h.write("""%(chr)s\t%(source)s\texon\t%(start)s\t%(end)s\t.\t%(strand)s\t.\tgene_id "%(gene_id)s"; transcript_id "%(transcript_id)s"; gene_type "%(gene_type)s"; gene_name "%(gene_name)s"; transcript_type "%(transcript_type)s"; transcript_name "%(transcript_name)s"; exon_number %(exon_nb)s; exon_id "%(exon_id)s";\n"""
                        % {"chr": x.seqname,
                           "source": x.source,
                           "start": x.start,
                           "end": x.end,
                           "strand": x.strand,
                           "gene_id": x.gene_uv,
                           "transcript_id": x.transcript_uv,
                           "gene_type": x.gene_type,
                           "gene_name": x.gene_name,
                           "transcript_type": x.transcript_type,
                           "transcript_name": x.transcript_name,
                           "exon_nb": x.exon_number,
                           "exon_id": x.exon_uv,
                           })

        out_h.close()

    def spark_save_gtf_ucsc_csv(self):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # cmd = r"""
        # select *
        # from gtf_exons
        # where seqname='%s'
        # order by chr_idx,start,end
        #     	"""\
        #           % ("chr5")
        # print("cmd: ", cmd)

        cmd2 = r"""
        select *
        from gtf_exons_ucsc
        order by chr_idx,start,end
            	"""
        print("cmd2: ", cmd2)

        # res_df = spark.sql(cmd)
        res_df = spark.sql(cmd2)

        out_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gencode-exons-ucsc.csv")
        out_h = open(out_f, "w")

        for x in res_df.rdd.toLocalIterator():
            # print("x :", x.seqname, x.start)
            # out_h.write("""%(smp)s\t%(smp)s-%(clr)s\n""" % {"smp": smp, "clr": caller})
            # """chr    HAVANA  exon    57289845        57290016        .       +       .       gene_id "ENSMUSG00000026827.12"; transcript_id "ENSMUST00000141536.7"; gene_type "protein_coding"; gene_name "Gpd2"; transcript_type "processed_transcript"; transcript_name "Gpd2-204"; exon_number 3; exon_id "ENSMUSE00001311614.1"; level 2; transcript_support_level "5"; havana_gene "OTTMUSG00000012561.1"; havana_transcript "OTTMUST00000030013.1";
# """
            out_h.write("""%(chr)s\t%(source)s\texon\t%(start)s\t%(end)s\t.\t%(strand)s\t.\tgene_id "%(gene_id)s"; transcript_id "%(transcript_id)s"; gene_type "%(gene_type)s"; gene_name "%(gene_name)s"; transcript_type "%(transcript_type)s"; transcript_name "%(transcript_name)s"; exon_number %(exon_nb)s; exon_id "%(exon_id)s";\n"""
                        % {"chr": x.seqname,
                           "source": x.source,
                           "start": x.start,
                           "end": x.end,
                           "strand": x.strand,
                           "gene_id": x.gene_uv,
                           "transcript_id": x.transcript_uv,
                           "gene_type": x.gene_type,
                           "gene_name": x.gene_name,
                           "transcript_type": x.transcript_type,
                           "transcript_name": x.transcript_name,
                           "exon_nb": x.exon_number,
                           "exon_id": x.exon_uv,
                           })

        out_h.close()

    def spark_save_ref_gen_orig_contigs(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd = r"""
            select ucsc_fa as contig
            from contig_names
            order by idx
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pandas_df = res_df.toPandas()
        print("pandas_df: ", pandas_df)

        pandas_df.to_csv(self._ctx.conf.ref_gen_orig_contigs_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

    def spark_load1xbam(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example.parquet"
        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example_baz_part.parquet"
        #tbl = spark.read.parquet(in_f).where("1=1")

        # tbl = spark.read.format("com.databricks.spark.avro").load(in_f).where("1=1")
        # tbl= tbl.withColumn('prt', tbl.two)

        spark.sql("drop table if exists x10bam")
        # in_f="/home/badescud/scratch/over/duny/qualviz/avro"
        # in_f="/home/badescud/scratch/over/duny/qualviz_G002Normal/avro"
        # in_f="/home/badescud/scratch/over/duny/qvz_Jurkat_C9m_Naive-2of2/avro"
        # in_f="/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/Ascites5_h2Mm10/avro"
        in_f="/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/qvz_G002Tumor/avro"




        # tbl = spark.read.format("com.databricks.spark.avro").load(in_f)
        # org.apache.spark#spark-avro_2.11;2.4.1
        tbl = spark.read.format("avro").load(in_f)
        tbl.write.saveAsTable("x10bam", format="parquet", mode="overwrite")

        # print(tbl)
        # print(tbl.show())
        # print(tbl.printSchema())
        # print("--------------------")

        # create filter table on intersection with general tracking ids
        spark.sql("drop table if exists cb_gx_ub_rd")


        # index for genes for that filter
        cmd = r"""create table cb_gx_ub_rd as
        select t1.cb,
       count(t1.gx) as ngx,
       sum(t1.nub) as nnub,
       sum(t1.nnrd) as nnnrd,
       sum(t1.nnrd)/sum(t1.nub) as dupl
from
(
select t0.cb,
       t0.gx,
       count(t0.ub) as nub,
       sum(t0.nrd) as nnrd
from
(
select cb,
       gx,
       ub,
       count(qname) nrd
from x10bam
group by cb,gx,ub
) t0
group by t0.cb,
         t0.gx
) t1
group by t1.cb
"""
        print(cmd)
        spark.sql(cmd)

             # create filter table on intersection with general tracking ids
        spark.sql("drop table if exists rng_cb_gx_ub_rd")

        # index for genes for that filter
        cmd = r"""create table rng_cb_gx_ub_rd as
        select (ub_deb_rng || ub_fin_rng || gx_deb_rng || gx_fin_rng) as rng_id,
       ceil(ub_deb_rng+(ub_fin_rng-ub_deb_rng)/2) as mid_ub,
       ceil(gx_deb_rng+(gx_fin_rng-gx_deb_rng)/2) as mid_gx,
       *
from
(
select cb,
       floor((nnub)/100)*100 as ub_deb_rng,
       floor((nnub)/100)*100+99 ub_fin_rng,
       floor((ngx)/100)*100 as gx_deb_rng,
       floor((ngx)/100)*100+99 gx_fin_rng,
       ngx,
       nnub,
       nnnrd,
       dupl
from cb_gx_ub_rd
)
"""
        print(cmd)
        spark.sql(cmd)


    def spark_savestats1xbam(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        pd.set_option('display.float_format', lambda x: '%.2f' % x)

        cmd = r"""
select 'ALL_READS' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
--where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
--where (avg_ub<1000 or avg_gx<100)
--where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
--where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
--where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd1_df = res_df.toPandas()
        print("pandas_df: ", pd1_df)

        cmd = r"""
select 'FREE_FLOATING_cb_is_empty' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb=='empty'
) t0
group by rng_id
order by avg_ub desc
)
--where (avg_ub<1000 or avg_gx<100)
--where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
--where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
--where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd2_df = res_df.toPandas()
        print("pandas_df: ", pd2_df)

        cmd = r"""
select 'EMPTY_GEMS_cb_non_empty_avg_ub_less1000_or_avg_gxless100' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
where (avg_ub<1000 or avg_gx<100)
--where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
--where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
--where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd3_df = res_df.toPandas()
        print("pandas_df: ", pd3_df)

        cmd = r"""
select 'DEBRIS_avg_gxmore100_less1000' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
--where (avg_ub<1000 or avg_gx<100)
where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
--where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
--where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd4_df = res_df.toPandas()
        # print("pandas_df: ", pd4_df)

        cmd = r"""
select 'GOOD_CELLS_avg_gxmore1000_less5000' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
--where (avg_ub<1000 or avg_gx<100)
--where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
--where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd5_df = res_df.toPandas()

        cmd = r"""
select 'GREAT_CELLS_avg_gxmore5000' as categ,sum(ncb) as sum_gems, sum(avgrds) as sum_avg_reads,mean(dup) as mean_dup
from
(
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
--where (avg_ub<1000 or avg_gx<100)
--where (avg_ub>1000 and avg_gx>=100 and avg_gx<1000)
--where (avg_ub>1000 and avg_gx>=1000 and avg_gx<5000)
where (avg_ub>1000 and avg_gx>=5000)
order by avg_gx desc
)
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pd6_df = res_df.toPandas()

        pandas_df=pd.concat((pd1_df,pd2_df,pd3_df,pd4_df,pd5_df,pd6_df),axis=0)
        pandas_df=pandas_df.round(1)

        pandas_df.to_csv("/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/qvz_G002Tumor/res/umibamstats.csv", sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
        )

    def spark_savestats2xbam(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext
        # pd.set_option('display.float_format', lambda x: '%.2f' % x)

        cmd = r"""
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
where (avg_ub<1000 or avg_gx<100)
order by avg_gx desc

            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pandas_df = res_df.toPandas()
        # print("pandas_df: ", pd1_df)

        pandas_df=pandas_df.round(1)

        pandas_df.to_csv("/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/qvz_G002Tumor/res/noise.csv", sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
        )


        cmd = r"""
select rng_id,
       avg_ub,
       avg_gx,
       ncb,
       dup,
       avgrds
from
(
select rng_id,
       floor(avg(nnub)) as avg_ub,
       floor(avg(ngx)) as avg_gx,
       count(cb) as ncb,
       mean(dupl) as dup,
       mean(nnub*dupl) as avgrds
from
(
select *
from rng_cb_gx_ub_rd
where cb!='empty'
) t0
group by rng_id
order by avg_ub desc
)
where (avg_ub>1000 and avg_gx>100)
order by avg_gx desc

            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pandas_df = res_df.toPandas()
        # print("pandas_df: ", pd1_df)

        pandas_df=pandas_df.round(1)

        pandas_df.to_csv("/home/dbadescu/share/rnvar/b001-qualivz-e1/qvz/qvz_G002Tumor/res/gems.csv", sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
        )







    def spark_test_pyarrow(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example.parquet"
        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example_baz_part.parquet"
        tbl = spark.read.parquet(in_f).where("1=1")

        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example.avro"
        # tbl = spark.read.format("com.databricks.spark.avro").load(in_f).where("1=1")
        tbl= tbl.withColumn('prt', tbl.two)
        tbl.write.saveAsTable("example", format="parquet", mode="overwrite", partitionBy="prt")




        print(tbl)
        print(tbl.show())
        print(tbl.printSchema())
        print("--------------------")




        df = pd.DataFrame({'one': [-1, np.nan, 2.5],
                           'two': ['foo', 'bar', 'baz'],
                           'three': [True, False, True]})

        # table = pa.Table.from_pandas(df)
        # pq.write_table(table, 'example.parquet')

        # parquet_file = pq.ParquetFile('example.parquet')
        # parquet_file = pq.ParquetFile('example.parquet')
        # meta = parquet_file.metadata
        # schem = parquet_file.schema
        # print(meta)
        # print(schem)
        # print(meta.num_row_groups)

        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/gtf"
        # in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/example_baz_part.parquet"
        in_f="/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/gtf_arrow/1.parquet"

        tbl = spark.read.parquet(in_f).where("1=0")
        tbl= tbl.withColumn('prt', tbl.seqname)
        tbl.write.saveAsTable("example_gtf2", format="parquet", mode="overwrite", partitionBy="prt")

        # dataset = pq.ParquetDataset('/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/gtf')
        # meta = dataset.metadata
        # schem = dataset.schema
        # print(meta)
        # print(schem)
        # table2 = dataset.read()

        # df = spark.read.parquet('/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/parquet14')
        # df.write.mode("overwrite").partitionBy("seqname2").saveAsTable("gtf_imported_table_arrow")

        # ards = pq.ParquetWriter('/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/gtf_arrow/1.parquet',
        #                        table2.schema,
        #                        use_dictionary=True,
        #                         flavor='spark')


        # for i in range(3):
        #     ards.write_table(table2)





        # native_f = pa.NativeFile('example.parquet', mode="rb")


        # table2 = pq.read_table('example.parquet', columns=['one', 'three'])
        # table2 = pq.read_table('/gs/project/wst-164-aa/share/raglab_prod/igenomes/hyMm10/srvf/hivef/gtf', flavor='spark')

        # df2 = table2.to_pandas()
        #
        # print(df2)
        #











        cmd = r"""
            select ucsc_fa as contig
            from contig_names
            order by idx
            	"""
        # print("cmd: ", cmd)
        # res_df = spark.sql(cmd)

        # pandas_df = res_df.toPandas()
        # print("pandas_df: ", pandas_df)

        # pandas_df.to_csv(self._ctx.conf.ref_gen_orig_contigs_csv, sep=","
        #            ,header=True
        #            ,index=True
        #            ,index_label="idx"
        #            )

    def spark_homol_mm(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        # self.spark_import_simple_csv("hom_all_org")
        # self.spark_import_simple_csv("en_refseq_hg")
        # self.spark_import_simple_csv("en_refseq_mm")
        # self.spark_import_simple_json("hgnc_complete")
        # self.spark_import_simple_csv("hgnc_homol")

        # self.spark_export_simple_csv("genes")
        # self.spark_import_simple_csv("genes_hg")
        self.spark_import_simple_csv("cell_cycle_gene_list")
        # self.spark_export_simple_csv("extended_cell_cycle_gene_list")







    def spark_save_genes_tracking_ids(self):
        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        cmd = r"""
            select  gn.gene_uv as gene_id,
                    gn.gene_name,
                    gn.gene_type as source
            from genes gn
            order by gn.rowid
            	"""
        print("cmd: ", cmd)
        res_df = spark.sql(cmd)

        pandas_df = res_df.toPandas()
        print("pandas_df: ", pandas_df)

        pandas_df.to_csv(self._ctx.conf.genes_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # pandas_df.to_csv(os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gtf_embl_seqnames.csv"),
        #                sep=',', header=True
        #                , index=True
        #                , index_label="idx"
        #                )







    def spark_export_gene_mut(self):

        spark = self._ctx.servers_obj.spark_session
        sc = spark.sparkContext

        gene_mut_f = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gene_mut_cnt")
        gene_mut_csv = os.path.join(self._ctx.conf.proj_dir, "srvf/filef", "gene_mut_cnt.csv")
        if os.path.exists(gene_mut_f):
            shutil.rmtree(gene_mut_f, ignore_errors=False, onerror=None)


        rdd0 = [["gene_id", "gene_name", "mut_cnt"]]
        a = sc.parallelize(rdd0).map(lambda x_y: ",".join(map(str, x_y))).repartition(1)

        b = spark.sql("select * from gene_mut_cnt").rdd.map(lambda x_y: ",".join(map(str, x_y))).repartition(1)
        # wp2.map(lambda x_y: ",".join(map(str, x_y))).saveAsTextFile(outfolder)

        a.union(b).coalesce(1).saveAsTextFile(gene_mut_f)
        shutil.copy(os.path.join(gene_mut_f, "part-00000"), gene_mut_csv)


        # a = spark.sql("select * from gene_mut_cnt").rdd.map(lambda x_y: ",".join(map(str, x_y))).repartition(1)
        # coalesce(1).write.format("csv").option("header", "true").save(gene_mut_f)
        # parts2.write.csv(outfolder)
        # shutil.copy(os.path.join(gene_mut_f, "part-r-00000"), gene_mut_csv)

    def spark_parse_config(self):


        self.spark_parse_ctl_grps()

        # this is done for readcnt_rprt
        self.spark_parse_sc_grp()
        self.spark_parse_genes_tracking_ids()

    # "pdx1876vdt","pdx1735vdt" ,"pdxbcm2665vdt", "pdxhci001vdt"
    def spark_parse_dna_pdx_ex_vdt(self):
        # import inside database the appropriate file filter
        self.spark_register_pdx_ex_vdt()

    # this should be run in mouse single cell project
    def spark_mouse_scs(self):


        self.spark_parse_dna_snvs()
        self.spark_parse_dna_snv_smps()
        # convert Tim's ENSEMBL to hb19 chrom names
        self.spark_parse_chrom_cnvrt()

        # filtering on mouse data and
        # save exome filtered
        self.spark_save_pdx_ex_vdt()


    def spark_work_expr(self):

        self._ctx.readcnt_prefix = "all"
        self.gene_flt_smp_cnt_unpiv()
        # self.deploy_figs2a_b_c()


    def spark_work_esnv(self, dna_filter=False):

        self.spark_parse_rna_snvs()
        exit(0)
        self.spark_parse_rna_snv_smps()


        esnv_params = self.scqc3_conf["esnv_params"]

            # [(0.1, 1, 2), (0.1, 1, ceil(37/2))]
        # print(json.dumps(esnv_params))



        for ep in esnv_params:
            self.esnv_min_vaf = ep[0]
            self.esnv_min_nb_blk = ep[1]
            self.esnv_min_nb_scs = ep[2]

            self.spark_join_rna_snv_smp_vafs(dna_filter=dna_filter)
            self.rna_snv_smp_vaf_unpiv()
            # self.deploy_fig2d_e()






        # self.scqc3_conf["esnv_mi