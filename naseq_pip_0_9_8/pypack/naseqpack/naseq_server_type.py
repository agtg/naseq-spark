
from enum import Enum
class NaSeqServerType(Enum):
    dna_seq = 1
    rna_seq = 2
    dat_seq = 3

