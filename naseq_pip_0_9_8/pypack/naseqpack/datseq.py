#!/usr/bin/env python


import os
import shutil
import re
import csv
import subprocess
import pandas as pd
import gzip

from naseq import NaSeqPip

import imp
import os

# pypack = os.environ['NASEQ_PYPACK']

# scan_config_datseq = imp.load_source('scan_config_datseq', "%s/naseqpack/scan_config_datseq.py" % (pypack) )

from scan_config_datseq import ScanConfigDatSeq
from naseq_utils import *

from redis_controller import RedisController
from naseq_servers import NaseqServers

import redis

import socket
import json

import GTF_ENCODE                      # https://gist.github.com/slowkow/8101481
import gc
import os
import subprocess

import tables

import itertools

import time
import numpy as np
import scipy.sparse

import gzip
import time
import sys

import pysam
import gzip



class DatSeqPip(NaSeqPip, object):

    def __init__(self):

        self.conf = None
        # composition
        self.redis_obj = None

        super(DatSeqPip, self).__init__()

        # self.depl_dir = os.path.join(self.proj_dir, "paired_reads")

        self.dat_smp = ""
        self.dat_lib = ""
        self.dat_run = ""
        self.fastq_dir_name = None
        self.smp_ren_dict = {}
        self.smp_lib_ren_dict = {}
        self.smp_skp_arr = []
        self.orientation = 0

        # need initialize inherited properties
        self.htchipcol_idx = 0




    def load_modules(self):
        pass

        # load bioinformatics libraries
        # system libraries are in ~/.bash_profile
        # load_one_module('raglab/naseq_pip/0.9.4')
        #load_one_module('raglab/sambamba/0.6.5')
        # load_one_module('raglab/htslib/1.3.1')
        # load_one_module('raglab/samtools/1.3.1')
        # load_one_module('raglab/bcftools/1.3.1')
        # load_one_module('raglab/bedtools/2.25.0')
        # load_one_module('raglab/hisat2/2.0.4')
        # load_one_module('raglab/blast/2.3.0')
        # load_one_module('raglab/gatk/3.6.0')
        # load_one_module('raglab/bameutil/1.0.13')
        # load_one_module('raglab/bowtie2/2.2.3')
        # load_one_module('raglab/tophat/2.0.13')
        # load_one_module('raglab/picard/1.141')
        # load_one_module('raglab/rnaseqc/1.1.8')
        # load_one_module('raglab/trimmomatic/0.33')
        # load_one_module('raglab/cufflinks/2.2.1')
        # load_one_module('raglab/snpeff/4_1_k')
        # load_one_module('raglab/jvarkit/15.12.07')
        # load_one_module('raglab/ucscapps/340')
        # load_one_module('raglab/bedops/2.4.7')
        # load_one_module('raglab/seqtk/1.2-r95')
        # load_one_module('raglab/bbmap/36.49')
        # load_one_module('raglab/fml_convert/15.02.04')
        # load_one_module('raglab/bwa/0.7.15')

        # load_one_module('raglab/kallisto/0.42.2.1')


    def init_proj(self):

        print("in datseq.py, init_proj..")

        self.load_modules()

        cnf = ScanConfigDatSeq()
        cnf.scan_local()

        cnf.set_proj_dirs()
        cnf.set_proj_files_path_names()

        # composition
        # project configuration
        self.conf = cnf

        #cnf.load_sample_names()
        if 'htchip_demul' in self.conf.dat_stages_arr:
            self.conf.load_htchip_colnames()
            self.htchip_init_exp_data()
        if 'extract_bams' in self.conf.dat_stages_arr:
            # needs to start class for later submit initialization
            try:
                self.load_bam_names()
            except Exception:
                pass


        # print self.conf.makeReport() #self.conf.items()

        # dynamically add server objects at runtime
        # redis object
        self.redis_obj = RedisController(cnf)
        # servers object
        self.servers_obj = NaseqServers(self)




    @property
    def fastq_dir_name(self):
        return self.__fastq_dir_name

    @fastq_dir_name.setter
    def fastq_dir_name(self, val):
        self.__fastq_dir_name = val

    @property
    def fastq_dir(self):
        # print(self.fastq_dir_name)
        if (self.fastq_dir_name is None) or (self.fastq_dir_name==""):
            this_dir = os.path.join(self.conf.proj_dir, "fastq")
        else:
            this_dir = self.fastq_dir_name

        # print(this_dir)

        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def depl_dir(self):
        return self.__depl_dir

    @depl_dir.setter
    def depl_dir(self, val):
        self.__depl_dir = val

    @property
    def bam_names_csv(self):
        return os.path.join(self.conf.conf_dir, 'bam_names.csv')

    @property
    def bam_idx(self):
        return int(self.__bam_idx)

    @bam_idx.setter
    def bam_idx(self, val):
        self.__bam_idx = val

    @property
    def bam_name(self):
        return self.bams_df.iloc[self.bam_idx, 0]

    @property
    def nb_bams(self):
        return self.bams_df.shape[0]

    @property
    def bam_dir(self):
        this_dir = os.path.join(self.conf.proj_dir, "bam")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def bam_f(self):
        return os.path.join(self.bam_dir, self.bam_name)


    @property
    def dat_smp(self):
        return self.__dat_smp

    @dat_smp.setter
    def dat_smp(self, val):
        self.__dat_smp = val

    @property
    def dat_smp_ren(self):
        return self.__dat_smp_ren

    @dat_smp_ren.setter
    def dat_smp_ren(self, val):
        self.__dat_smp_ren = val


    # 1) searchin self.smp_ren_dict -> rename to final
    # if not in list of raname try to remove the old prefix and suffix
    # if not both are found than do not remove them and just add new prefix and suffix
    def dat_smp_transform(self, new_pref, new_suff, old_pref="", old_suff=""):

        body = ""
        pat = r"""^(%s)(\S*)(%s)$""" % (old_pref, old_suff)
        # print("pat: ", pat)
        p = re.compile(pat)

        print("self.dat_smp: ", self.dat_smp)
        print("self.smp_ren_dict: ", self.smp_ren_dict)


        if (self.dat_smp, self.dat_lib) in list(self.smp_lib_ren_dict.keys()):
            self.dat_smp_ren = self.smp_lib_ren_dict[(self.dat_smp, self.dat_lib)]
        elif self.dat_smp in list(self.smp_ren_dict.keys()):
            self.dat_smp_ren = self.smp_ren_dict[self.dat_smp]
        else:
            m = p.match(self.dat_smp)
            if m:
                old_pref = m.group(1)
                body = m.group(2)
                old_suff = m.group(3)
                print("old_pref, body, old_suff: >%s<, >%s<, >%s<", old_pref, body, old_suff)
                # transformation

            else:
                body = self.dat_smp

            self.dat_smp_ren = new_pref + body + new_suff
            # only uppercase
            self.dat_smp_ren = self.dat_smp_ren.upper()


    def check_smp_ren_skp(self):

        skp = False
        if self.dat_smp_ren in self.smp_skp_arr:
            skp = True

        return skp


    @property
    def dat_mach(self):
        return self.__dat_mach

    @dat_mach.setter
    def dat_mach(self, val):
        self.__dat_mach = val

    @property
    def dat_lib(self):
        return self.__dat_lib

    @dat_lib.setter
    def dat_lib(self, val):
        self.__dat_lib = val

    @property
    def dat_run(self):
        return self.__dat_run

    @dat_run.setter
    def dat_run(self, val):
        self.__dat_run = val

    @property
    def dat_lane(self):
        return self.__dat_lane

    @dat_lane.setter
    def dat_lane(self, val):
        self.__dat_lane = val


    @property
    def dat_runreg(self):
        return self.__dat_runreg

    @dat_runreg.setter
    def dat_runreg(self, val):
        self.__dat_runreg = val

    @property
    def dat_runreg_ren(self):
        return self.__dat_runreg_ren

    @dat_runreg_ren.setter
    def dat_runreg_ren(self, val):
        self.__dat_runreg_ren = val

    @property
    def dat_fastq_smp_dir(self):
        this_dir = os.path.join(self.fastq_dir, self.dat_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def dat_depl_smp_dir_ren(self):
        this_dir = os.path.join(self.depl_dir, self.dat_smp_ren)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def dat_fastq_runreg_dir(self):
        this_dir = os.path.join(self.dat_fastq_smp_dir, self.dat_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def dat_fastq_lib_dir(self):
        this_dir = os.path.join(self.dat_fastq_runreg_dir, self.dat_lib)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def dat_depl_runreg_dir_ren(self):
        this_dir = os.path.join(self.dat_depl_smp_dir_ren, self.dat_runreg_ren)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def read1_name(self):
        return os.path.join(self.dat_fastq_runreg_dir, "%s.%s.33.pair1.fastq.gz" % (self.dat_smp, self.dat_lib))

    @property
    def read2_name(self):
        return os.path.join(self.dat_fastq_runreg_dir, "%s.%s.33.pair2.fastq.gz" % (self.dat_smp, self.dat_lib))


    @property
    def sym_dst_name(self):
        return "%s.%s.33.pair%s.fastq.gz" % (self.dat_smp_ren, self.dat_lib, self.dat_direct)

    @property
    def sym_dst_single_name(self):
        return "%s.%s.33.single.fastq.gz" % (self.dat_smp_ren, self.dat_lib)


    @property
    def sym_dst_smp_name(self):
        return "%s.mer.bam" % (self.dat_smp_ren)


    @property
    def sym_dst_f(self):
        return os.path.join(self.dat_depl_runreg_dir_ren, self.sym_dst_name)

    @property
    def sym_dst_single_f(self):
        return os.path.join(self.dat_depl_runreg_dir_ren, self.sym_dst_single_name)


    @property
    def sym_dst_bam(self):
        return os.path.join(self.dat_depl_smp_dir_ren, self.sym_dst_smp_name)



    # def extract_bams_paral(self, nbproc):
    #     pass
    #
        # pool = Pool(processes=nbproc)
        # fool[1] = pool.map(unwrap_self_f, zip([self]*len(names), names))
        # pool.close()
        # pool.join()
        # print fool


    # def link_bam(self, bam_name):
    #     sym_src_file = os.path.join(self.bam_dir, bam_name)
    #     sym_dst_file = os.path.join(self.fastq_dir, bam_name)
    #     if os.path.exists(sym_dst_file):
    #         os.remove(sym_dst_file)
    #         os.symlink(sym_rel_path, sym_dst_file)


    # PAY attention to run prefix
    def extract_one_read_group(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        args = {"bam_idx": self.bam_idx}
        print("parameters: \n", args)

        exit_codes = []

        print("bam: %s" % self.bam_name)

        # bam_f = os.path.join(self.fastq_dir,bam_name)

        print("bam_f: ", self.bam_f)

        p1 = subprocess.Popen(r"""samtools view -H %s | grep '^@RG'""" % self.bam_f, shell=True,
                              cwd=self.fastq_dir,
                              stdout=subprocess.PIPE)
        res = p1.communicate()[0].decode("utf-8").rstrip()
        print("---------------------res: >%s< " % res)

        # p = re.compile('@RG\\tID:(.*)\\tPL:(.*)\\tPU:(.*)\\tLB:(.*)\\tSM:(.*)\\tCN:(.*)$')
        p = re.compile('^@RG\s+ID:(\S*)\s+PL:(\S*)\s+PU:(\S*)\s+LB:(\S*)\s+SM:(\S*)')


        m = p.match(res)
        print("-------------m: ", m)
        rid = ''
        rpl = ''
        rpu = ''
        rlb = ''
        rsm = ''

        if m:
            self.rid = m.group(1)
            self.rpl = m.group(2)
            self.dat_runreg = m.group(3)
            self.dat_lib = m.group(4)
            self.dat_smp = m.group(5)

        else:
            print("error in readgroups, using defaults")
            p = re.compile('^@RG\s+ID:(\S*).*SM:(\S*)')


            m = p.match(res)
            print("-------------m: ", m)

            self.dat_smp = m.group(2)
            self.rid = "rid"
            self.rpl = "rpl"
            self.dat_runreg = "run999_999"
            self.dat_lib = "lib0"



        print(self.rid, self.rpl, self.dat_runreg, self.dat_lib, self.dat_smp)

        # remove output analysis folder
        if os.path.exists(self.dat_fastq_lib_dir):
            shutil.rmtree(self.dat_fastq_lib_dir, ignore_errors=False, onerror=None)
        # and filenames
        for i in [self.read1_name, self.read2_name]:
            if os.path.exists(i):
                os.remove(i)

        # #######create run

        print("self.run_path: ", self.dat_fastq_lib_dir)

        # extract fastq from bam
        pipe1 = os.path.join(self.dat_fastq_lib_dir, "pipe1")
        r1 = os.path.join(self.dat_fastq_lib_dir, "r1")
        r2 = os.path.join(self.dat_fastq_lib_dir, "r2")
        o3 = os.path.join(self.dat_fastq_lib_dir, "sam_to_fastq.out")
        f3 = 0

        if not os.path.exists(pipe1):
            os.mkfifo(pipe1)

        if not os.path.exists(r1):
            os.mkfifo(r1)

        if not os.path.exists(r2):
            os.mkfifo(r2)


        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.read1_name, self.read2_name, o3]:
            if os.path.exists(i):
                os.remove(i)


        with open(o3, "w") as f3:


            #  paired
            cmd = r"""sambamba sort -n \
-t %(cor_tsk)s -m %(mem_tsk)sM --tmpdir=%(scratch_d)s \
-F "paired" %(infile)s \
-o /dev/stdout""" % {"cor_tsk": self.cor_tsk,
                     "mem_tsk": self.mem_tsk,
                     "scratch_d": self.conf.scratch_loc_dir,
                     "infile": self.bam_f}
            print(cmd)
            p1 = subprocess.Popen(cmd,
                                  shell=True,
                                  cwd=self.dat_fastq_lib_dir,
                                  stdout=subprocess.PIPE)

            # p2 = subprocess.Popen("java -jar $PICARD_HOME/SamToFastq.jar VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1,r2) , shell=True,
            p2 = subprocess.Popen("java -jar $PICARD_JAR SamToFastq VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1, r2), shell=True,
                                  cwd=self.dat_fastq_lib_dir,
                                  stdin=p1.stdout,
                                  stderr=f3,
                                  stdout=subprocess.PIPE)

            p3 = subprocess.Popen(r"""pigz < %s > %s""" % (r1, self.read1_name), shell=True,
                                  cwd=self.dat_fastq_lib_dir,
                                  #stdin = r1,
                                  stdout = subprocess.PIPE)

            p4 = subprocess.Popen(r"""pigz < %s > %s""" % (r2, self.read2_name), shell=True,
                                   cwd=self.dat_fastq_lib_dir,
                                   #stdin = r2,
                                   stdout=subprocess.PIPE)

            print(p1.pid, p2.pid, p3.pid, p4.pid)
            exit_codes = [pp.wait() for pp in (p1, p2, p3, p4)]

        print(" exit_codes: ", exit_codes)

        os.unlink(pipe1)
        os.unlink(r1)
        os.unlink(r2)

        self.mark_exit_args(exit_codes, args)


    def sort_lane(self):

        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        print("self.spln_runreg: ", self.spln_runreg)

        scratch_d = os.getenv('LSCRATCH', "")


        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(spln_bam)s -o %(spln_sort_bam)s
""" % {"scratch_d": scratch_d, "spln_bam": self.spln_bam, "spln_sort_bam": self.spln_sort_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_runreg_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0

        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)

        # mark as done
        self.mark_done_args(args, success)



    # def extract_read_groups(self):
    #
    #     m = re.match("^(.*)\.bam", bam_name)
    #     bam_prefix = m.group(1)
    #     print("bam_prefix: ", bam_prefix)

        #extract reads
        #bam_f = os.path.join(self.bam_dir,bam_name)
        #print "bam_f: ",bam_f
        #p0 = subprocess.Popen(r"""samtools split %(bam_f)s""" % {"bam_f": bam_f}, shell=True, cwd=self.fastq_dir);
        #res = p0.communicate()[0]
        #print "res: ", res
        #enumerate the extracted read group bams
        #rg_bam_files = [f for f in os.listdir(self.fastq_dir) if re.match("^%s\_\d+\.bam" % bam_prefix , f)]
        #print "rg_bam_files: ",rg_bam_files
        #for rg_bam_name in rg_bam_files:
        #    self.extract_one_read_group(rg_bam_name)


    def scan_bam_dir(self):

        self.bam_files = [f for f in os.listdir(self.bam_dir) if re.match('(.+)\.bam$', f)]
        print("files: ", self.bam_files)

    def save_bam_names(self):
        bams_df = pd.DataFrame.from_items([('bam_name', self.bam_files)])
        bams_df.reset_index()

        bams_df.to_csv(self.bam_names_csv,
                           sep=',', header=True
                           , index=True
                           , index_label="idx"
                           )

    def load_bam_names(self):
        self.bams_df = pd.read_csv(self.bam_names_csv,
                  sep=',', header=0
                  ,index_col=[0]
                  ,usecols=[0, 1]
                  ,names=["idx", "bam_name"]
                  )

        # print "self.bams_df: \n", self.bams_df

    def push_qjobs_bam_idx(self):
        # print("in push_jobs_bam_idx...")
        jobs_args = []
        # print("self.nb_bams: ", self.nb_bams)
        for bam_idx in range(self.nb_bams):
            # print("bam: %s" % bam_idx)
            args = {"bam_idx": bam_idx}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    # uses sparse indexes (skiping skip column)
    def push_qjobs_htchipcol_idx(self):

        jobs_args = []
        # print("self.nb_bams: ", self.nb_bams)

        #print("self.htchip_colnames_df \n", self.conf.htchip_colnames_df)
        #self.conf.htchip_colnames_df.loc[:,]

        cols = self.conf.htchip_colnames_df.query('skip != 1').index.values
        for idx in cols:
            args = {"htchipcol_idx": int(idx)}
            print("args: ", args)
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)



    def bam_hs00_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find sample names
        p = re.compile('^(\S*)\.(\S*)\.mer\.bam$')

        # filter only bam files, not log files
        for read in [f for f in os.listdir(self.bam_data_dir) if re.match('^(\S*)\.(\S*)\.mer\.bam$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(2)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # self.dat_runreg =  m.group(1)

                # self.dat_lib = "hplexlib"

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.bam_data_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_bam: ", self.sym_dst_bam)
            os.symlink(sym_src_f, self.sym_dst_bam)
            # print sample_f,lane_run_f,read_f

    # prepends 1 to the run and lane
    def correct_zero_start_run_lane(self, runreg):

        print("runreg: ", runreg)

        run = ""
        lane = ""
        res = ""
        # regular expression to find sample names
        # p = re.compile('^run(\d?)(\S*)_(\d?)(\S*)$')
        p = re.compile('^(\d?)(\S*)_(\d?)(\S*)$')
        m = p.match(runreg)
        if m:
            digirun = m.group(1)
            if digirun == "0":
                digirun = "10"
            restrun = m.group(2)

            digilane = m.group(3)
            if digilane == "0":
                digilane = "10"
            restlane = m.group(4)

            res = "%s%s_%s%s" % (digirun, restrun, digilane, restlane)

        else:
            exit(1)

        return res

    # deployment file format parsings
    def raw_afl_deploy(self, pref, suff, old_pref, old_suff):
        print("one--------------")
        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^(\S*)\.(\S*)\.33\.pair(\d).fastq.gz$')


        for sample_f in [f for f in os.listdir(self.fastq_dir_name) if os.path.isdir(os.path.join(self.fastq_dir_name, f))]:
            self.dat_smp = sample_f

            # remove old prefix, old suffix
            # to do with regex
            print("sample_f: ", self.dat_smp)
            # transform sample name
            self.dat_smp_transform(pref, suff, old_pref, old_suff)

            lanes = os.listdir(os.path.join(self.fastq_dir_name, self.dat_smp))
            print("lanes: ", lanes)
            for lane_run in lanes:
                #
                self.dat_runreg = lane_run
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                runreg_dirs = self.dat_fastq_runreg_dir
                print("runreg_dirs: ", runreg_dirs)

                # filter only fastq files, not log files
                for direct in [1,2]:
                    self.dat_lib = "afl"
                    self.dat_direct = direct

                    read = "%s_%s_MP.R%s.fastq.gz" % (self.dat_smp,self.dat_runreg,self.dat_direct)
                    # print("read: ", read)
                    # deploy if not skipped
                    if not self.check_smp_ren_skp():
                        print("-----------------------read_f: \n", read)
                        sym_src_f = os.path.join(self.dat_fastq_runreg_dir, read)
                        # also replace sample name in file name and reconstitute read name
                        print("sym_src_f: ", sym_src_f)
                        print("sym_dst_f: ", self.sym_dst_f)
                        os.symlink(sym_src_f, self.sym_dst_f)
                        # print(sample_f,lane_run_f,read_f)


    # deployment file format parsings
    def fastq_gq00_deploy(self, pref, suff, old_pref, old_suff):
        print("one--------------")
        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^(\S*)\.(\S*)\.33\.pair(\d).fastq.gz$')


        for sample_f in [f for f in os.listdir(self.fastq_dir_name)]:

            self.dat_smp = sample_f

            # remove old prefix, old suffix
            # to do with regex
            print("sample_f: ", self.dat_smp)
            # transform sample name
            self.dat_smp_transform(pref, suff, old_pref, old_suff)

            lanes = os.listdir(os.path.join(self.fastq_dir_name, self.dat_smp))
            print("lanes: ", lanes)
            for lane_run in lanes:
                #
                self.dat_runreg = lane_run
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                runreg_dirs = self.dat_fastq_runreg_dir
                print("runreg_dirs: ", runreg_dirs)

                # filter only fastq files, not log files
                for read in [f for f in os.listdir(self.dat_fastq_runreg_dir) if re.match('(.+)\.fastq.gz$', f)]:
                    print("read: ", read)
                    #
                    m = p.match(read)
                    if m:
                        self.dat_smp = m.group(1)
                        self.dat_lib = m.group(2)
                        self.dat_direct = m.group(3)
                        print("aaa")
                    else:
                        print('No match')

                    print("bbb")

                    # deploy if not skipped
                    if not self.check_smp_ren_skp():
                        # print("-----------------------read_f: \n", read)
                        sym_src_f = os.path.join(self.dat_fastq_runreg_dir, read)
                        # also replace sample name in file name and reconstitute read name
                        print("sym_src_f: ", sym_src_f)
                        print("sym_dst_f: ", self.sym_dst_f)
                        os.symlink(sym_src_f, self.sym_dst_f)
                        # print(sample_f,lane_run_f,read_f)

    def fastq_gqfq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^HI\.(\d*)\.(\d*)\.([^.]*)\.([^.]*)_R(\d)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir_name) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_runreg = "run%s_1%s" %( m.group(1), m.group(2) )
                print("dat_runreg: ", self.dat_runreg)
                self.dat_lib = m.group(3)
                self.dat_direct = m.group(5)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir_name, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # OocytecDNA-157pg_MPS12341161_A12_8182_S10_L001_R1_001.fastq.gz
    def fastq_gq2fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^([^_]*)\_([^_]*)\_([^_]*)\_([^_]*)\_([^_]*)\_([^_]*)\_R(\d)\_(.+)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_lib = "%s%s" %( m.group(2), m.group(3))
                self.dat_direct = m.group(7)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    #  HI.5215.002.N711---S503.C11_R2.fastq.gz
    # 1.	5215
    # 2.	002
    # 3.	N711---S503
    # 4.	C11
    # 5.	2
    # No more run text in runreg, difference to gq1fq
    def fastq_gq3fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^HI\.(\d*)\.(\d*)\.([^.]*)\.([^.]*)_R(\d)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir_name) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_runreg = "%s_1%s" %( m.group(1), m.group(2) )
                print("dat_runreg: ", self.dat_runreg)
                self.dat_lib = m.group(3)
                self.dat_direct = m.group(5)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir_name, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f


    # 20181129nn-5-Oocytes_P1H02_F02R08_A00266_0112_2_S24_L002_R1_001.fastq.gz
    def fastq_nvfq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^([^_]*)_([^_]*)_([^_]*)_([^_]*)_([^_]*)_([^_]*)_([^_]*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_runreg = "%s_1%s" %(m.group(10), m.group(8) )
                self.dat_lib = "%s_%s_%s_%s_%s_%s" %( m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7))
                self.dat_direct = m.group(9)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # Oocytes_3of4_2-33512_S14_L001_I1_001.fastq.gz
    # 2770_2-467707_S27_L004_R1_001.fastq.gz
    def fastq_nv2fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(.*)-(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_runreg = "9%s_8%s" %(m.group(5), m.group(3) )
                self.dat_lib = "%s" %( m.group(2))
                self.dat_direct = m.group(4)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # WT2-MII-5Oocytes-50ng5PCR-1uLFlex-SPRIp8_RAG852A1-1_S2_L001_R1_001.fastq.gz
    # 1.	WT2-MII-5Oocytes
    # 2.	5PCR
    # 3.	1uL
    # 4.	p8
    # 5.	RAG852A1
    # 6.	1_S2
    # 7.	001
    # 8.	1
    # 9.	001
    def fastq_nv3fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(.*)-50ng([^-]*)-([^_]*)Flex-SPRI([^_]*)_([^_]*)-(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = "FL%s_%s_%s" %(m.group(3), m.group(2), m.group(4))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_runreg = "%s_%s" %(m.group(7),m.group(9))
                self.dat_lib = "%s_%s_%s" %( m.group(5),m.group(6),m.group(9))
                self.dat_direct = m.group(8)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # 157pg_MPS12341161_A12_8182_S10_L001_R1_001.fastq.gz
    # ^(.*)-([^_]*)_(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$
    # 1. OocytecDNA
    # 2. 157pg
    # 3. MPS12341161_A12_8182_S10
    # 4. 001
    # 5. 1
    # 6. 001
    def fastq_nv4fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(.*)-([^_]*)_(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = "FL%s" %(m.group(2))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_runreg = "%s_%s" %(m.group(6),m.group(4))
                self.dat_lib = "%s" %( m.group(3))
                self.dat_direct = m.group(5)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f


    # 20190718-WT1-GV-5oocytes-1of2_2-657567_S1_L002_R1_001.fastq.gz
    # ^(\d*)(-*)(.*)(_2)-(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$
    # 1.	20190718
    # 2. -
    # 3.	WT1-GV-5oocytes-1of2
    # oocytes - 1
    # of2
    # 4. _2
    # 5. 657567
    # _S1
    # 6. 002
    # 7. 1
    # 8. 001
    def fastq_nv5fq_deploy(self, pref, suff, old_pref, old_suff):

            # regular expression to find tag and direction
            # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
            ptrn = '^(\d*)(-*)(.*)(_2)-(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
            p = re.compile(ptrn)

            # filter only fastq files, not log files
            for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
                print("read: ", read)
                #
                m = p.match(read)
                if m:
                    # print("1: ",m.group(1))
                    # print("2: ", m.group(2))
                    # print("3: ", m.group(3))
                    # print("4: ", m.group(4))
                    # print("5: ", m.group(5))


                    self.dat_smp = "%s" % (m.group(3))
                    print("sample_f: ", self.dat_smp)
                    # transform sample name
                    # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                    #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                    # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                    # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                    self.dat_lib = "%s_%s_%s_%s" % (m.group(5), m.group(4), m.group(1), m.group(2))
                    # print("6: ", m.group(6))
                    # print("7: ", m.group(7))

                    self.dat_runreg = "%s_%s" % (m.group(8), m.group(6))

                    self.dat_direct = m.group(7)

                    self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                    self.dat_smp_transform(pref, suff, old_pref, old_suff)

                    # print("self.dat_runreg: ", self.dat_runreg)
                    # print("self.dat_lib: ", self.dat_lib)
                    # print("self.dat_smp: ", self.dat_smp)
                    # print("self.dat_direct: ", self.dat_direct)


                else:
                    print('No match')

                # deploy if not skipped
                if not self.check_smp_ren_skp():
                    # print("-----------------------read_f: \n", read)
                    sym_src_f = os.path.join(self.fastq_dir, read)

                    # also replace sample name in file name and reconstitute read name
                    print("sym_src_f: ", sym_src_f)
                    print("sym_dst_f: ", self.sym_dst_f)
                    os.symlink(sym_src_f, self.sym_dst_f)
                    # print sample_f,lane_run_f,read_f

    # XX-3_2-646660_S9_L001_R2_001.fastq.gz
    # 1.XX-3
    # 2._2
    # 3.646660_S9
    # 4.001
    # 5.2
    # 6.001
    def fastq_nv6fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(.*)(_2)-(.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:
                # print("1: ",m.group(1))
                # print("2: ", m.group(2))
                # print("3: ", m.group(3))
                # print("4: ", m.group(4))
                # print("5: ", m.group(5))

                self.dat_smp = "%s" % (m.group(1))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                self.dat_lib = "%s_%s" % (m.group(3), m.group(2))
                # print("6: ", m.group(6))
                # print("7: ", m.group(7))

                self.dat_runreg = "%s_%s" % (m.group(4), m.group(6))

                self.dat_direct = m.group(5)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # 12E_H723-H516_L-00060_S60_L002_R1_001.fastq.gz
    # 1.    12E
    # 2.	H723-H516_L-00060_S60
    # 3.	002
    # 4.	1
    # 5.	001
    def fastq_nv7fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(.*)_(.*_.*_.*)_L([^_]*)_R([^_]*)_([^_]*)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:
                # print("1: ",m.group(1))
                # print("2: ", m.group(2))
                # print("3: ", m.group(3))
                # print("4: ", m.group(4))
                # print("5: ", m.group(5))

                self.dat_smp = "L%s_%s" % (m.group(3), m.group(1))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                # self.dat_lib = "%s_%s" % (m.group(3), m.group(2))
                self.dat_lib = "%s" % (m.group(2))
                # print("6: ", m.group(6))
                # print("7: ", m.group(7))

                self.dat_runreg = "%s_%s" % (m.group(5), m.group(5))
                # self.dat_runreg = "%s" % (m.group(5))

                self.dat_direct = m.group(4)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f



    def fastq_nv8fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^([^_]+)_([^_]+)_([^_]+)_L([^_]+)_R([^_]+)_([^_]+)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:
                # print("1: ",m.group(1))
                # print("2: ", m.group(2))
                print("3: ", m.group(3))
                ptrn2 = '^(\d*)of(\d*)$'
                p2 = re.compile(ptrn2)
                m2 = p2.match(m.group(3))
                smp2_name = m.group(3)

                if m2:
                    print("1: ", m2.group(1))
                    print("2: ", m2.group(2))
                    smp2_name=m2.group(1)


                # self.dat_smp = "%son%s%s%s" % (m.group(4),m.group(1),m.group(2),m.group(3))
                self.dat_smp = "%s-%s" % (m.group(2),smp2_name)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                # self.dat_lib = "%s_%s" % (m.group(3), m.group(2))
                self.dat_lib = "%s" % (m.group(4))
                # print("6: ", m.group(6))
                # print("7: ", m.group(7))

                self.dat_runreg = "%s_%s" % (m.group(7), m.group(5))
                # self.dat_runreg = "%s" % (m.group(5))

                self.dat_direct = m.group(6)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # Rima S4 lane with dates
    # 22020-05-27-MUT-MI-SC-1of95_SLI871A246-1_S44_L004_R2_001.fastq.gz
    # Match 1
    # 1.	20
    # 2.	20
    # 3.	05
    # 4.	27
    # 5.	MUT-MI-SC-1of95
    # 6.	SLI871A246-1
    # 7.	S44
    # 8.	004
    # 9.	2
    # 10.	001
    def fastq_nv9fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^(\d{2})(\d{2})-(\d+)-(\d+)-(.+)_([^_]+)_([^_]+)_L([^_]+)_R([^_]+)_([^_]+)\.fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:
                # print("1: ",m.group(1))
                # print("2: ", m.group(2))
                # print("3: ", m.group(3))
                # ptrn2 = '^(\d*)of(\d*)$'
                # p2 = re.compile(ptrn2)
                # m2 = p2.match(m.group(3))
                # smp2_name = m.group(3)
                #
                # if m2:
                #     print("1: ", m2.group(1))
                #     print("2: ", m2.group(2))
                #     smp2_name=m2.group(1)
                #

                # self.dat_smp = "%son%s%s%s" % (m.group(4),m.group(1),m.group(2),m.group(3))
                self.dat_smp = "%s%s%s-%s" % (m.group(2), m.group(3), m.group(4), m.group(5))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                # self.dat_lib = "%s_%s" % (m.group(3), m.group(2))
                self.dat_lib = "%s%s" % (m.group(6), m.group(7))
                # print("6: ", m.group(6))
                # print("7: ", m.group(7))

                self.dat_runreg = "1%s_1%s" % (m.group(10), m.group(8))
                # self.dat_runreg = "%s" % (m.group(5))

                self.dat_direct = m.group(9)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                # print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)

                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # Keerthana S4 lane concat
    # 2A1_R1.fastq.gz
    # Match 1
    # 1.	A1
    # 2.	1
    def fastq_nv10fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        ptrn = '^([^_]+)_R([^_]+).fastq.gz$'
        p = re.compile(ptrn)

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(ptrn, f)]:
            print("read: ", read)
            m = p.match(read)
            if m:
                print("1: ",m.group(1))
                print("2: ", m.group(2))

                # print("3: ", m.group(3))
                # ptrn2 = '^(\d*)of(\d*)$'
                # p2 = re.compile(ptrn2)
                # m2 = p2.match(m.group(3))
                # smp2_name = m.group(3)
                #
                # if m2:
                #     print("1: ", m2.group(1))
                #     print("2: ", m2.group(2))
                #     smp2_name=m2.group(1)
                #

                # self.dat_smp = "%son%s%s%s" % (m.group(4),m.group(1),m.group(2),m.group(3))
                self.dat_smp = "%s" % (m.group(1))
                print("sample_f: ", self.dat_smp)
                # transform sample name
                # uses         self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                #             "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                # self.dat_runreg = "run%s%s_1%s%s" %( m.group(4), m.group(8), m.group(5), m.group(6))
                # self.dat_lib = "%s_%s" % (m.group(3), m.group(2))
                #self.dat_lib = "%s%s" % (m.group(6), m.group(7))
                self.dat_lib = "lib101"
                # print("6: ", m.group(6))
                # print("7: ", m.group(7))

                self.dat_runreg = "201_301"
                # self.dat_runreg = "%s" % (m.group(5))

                self.dat_direct = m.group(2)

                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                print("self.dat_runreg: ", self.dat_runreg)
                print("self.dat_lib: ", self.dat_lib)
                print("self.dat_smp: ", self.dat_smp)
                print("self.dat_direct: ", self.dat_direct)


            # else:
            #     print('No match')

            # deploy if not skipped
            if not self.check_smp_ren_skp():
                print("-----------------------read_f: \n", read)
                sym_src_f = os.path.join(self.fastq_dir, read)
                #
                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)
                # print sample_f,lane_run_f,read_f

    # Base Space fastq from one folder
    def fastq_bsfq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^(.*)_([^_]*)_R(\d)_([^.]*)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_runreg = "run%s_%s" %( m.group(2), m.group(4) )
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                self.dat_lib = m.group(2)
                self.dat_direct = m.group(3)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_f: ", self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_f)
            # print sample_f,lane_run_f,read_f

    # only deploy Read2
    def fastq_htfq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^HI\.(\d*)\.(\d*)\.([^.]*)\.([^.]*)\.R2\.fastq.gz$')
        # only deploy

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.R2\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # ensure no quotion problem with the lane, prefix with 1
                self.dat_runreg = "run%s_1%s" %( m.group(1), m.group(2) )
                self.dat_lib = m.group(3)
                # self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_single_f: ", self.sym_dst_single_f)
            #os.symlink(sym_src_f, self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_single_f)

            # print sample_f,lane_run_f,read_f

    # only deploy Read2
    def fastq_htfq1_deploy(self, pref, suff, old_pref, old_suff):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^HI\.(\d*)\.(\d*)\.([^.]*)\.([^.]*)\.R2\.fastq.gz$')
        # only deploy

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.R2\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # ensure no quotion problem with the lane, prefix with 1
                self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                self.dat_lib = m.group(3)
                # self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_single_f: ", self.sym_dst_single_f)
            #os.symlink(sym_src_f, self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_single_f)

            # print sample_f,lane_run_f,read_f

    def ren_shuf_fastq_files(self):

        exit_codes = []

        # con_csv = "/gs/project/wst-164-ab/share/data/027-camps-RISC/doc/fastq_convert.csv"
        # src_dir = "/gs/project/wst-164-ab/share/data/027-camps-RISC/fastq_orig"
        # dst_dir = "/gs/project/wst-164-ab/share/data/027-camps-RISC/fastq"

        # con_csv = "/gs/project/wst-164-ab/share/data/x049-sanger-ss/doc/sc_files_06.csv"
        # src_dir = "/gs/project/wst-164-ab/share/data/x049-sanger-ss/fastq_gz"
        # dst_dir = "/gs/project/wst-164-ab/share/data/x049-sanger-ss/fastq_ren"

        con_csv = "/gs/project/wst-164-aa/share/rnvar/t001-y001/conf/ren_shuf_fastqs.csv"
        src_dir = "/gs/project/wst-164-aa/share/rnvar/t001-y001/fastq_gz"
        dst_dir = "/gs/project/wst-164-aa/share/rnvar/t001-y001/fastq_ren"


        condf = pd.read_csv(con_csv,
                            sep=',', header=0
                            # ,index_col=[0]
                            , usecols=[0, 1, 2, 3, 4, 5, 6, 7]
                            # , names=["idx", "contig"]
                            )

        print("condf: \n", condf)

        for idx, row in condf.iterrows():
            print("row: ", idx, row[0])

            suffix = ""
            if row[6] == 1:
                suffix = "R1.fastq.gz"

            if row[7] == 1:
                suffix = "R2.fastq.gz"

            cmd = r"""cp %(fin)s %(fout)s
            """ % {"scratch_d": self.conf.scratch_loc_dir,
                   "fin": os.path.join(src_dir, row[0]),
                   "fout": os.path.join(dst_dir, "%s.%s.%s.%s.%s_%s" % (row[1], row[2], row[3], row[4], row[5], suffix))
                   }
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=dst_dir, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)
            exit_codes.append(rc3)

        return [exit_codes]

    def red_shuf_fastq_files(self, max_reads, take_reads):

        # prepare random choice, same for all files
        # max_reads =  100000000
        # take_reads =   5000000

        exit_codes = []


        # con_csv = "/gs/project/wst-164-ab/share/data/027-camps-RISC/doc/fastq_convert.csv"
        # src_dir = "/gs/project/wst-164-ab/share/data/027-camps-RISC/fastq_orig"
        # dst_dir = "/gs/project/wst-164-ab/share/data/027-camps-RISC/fastq"

        con_csv = os.path.join(self.conf.conf_dir, "fastq_convert2.csv")
        src_dir = os.path.join(self.conf.proj_dir, "fastq2_orig")
        # dst_dir = os.path.join(self.conf.proj_dir, "fastq2_ren")
        dst_dir = os.path.join(self.conf.proj_dir, "fqren_90k_50k")


        condf = pd.read_csv(con_csv,
                            sep=',', header=0
                            # ,index_col=[0]
                            , usecols=[0, 1, 2, 3, 4, 5, 6, 7]
                            # , names=["idx", "contig"]
                            )

        print("condf: \n", condf)


        a = np.random.choice(max_reads, take_reads, replace=False)
        b = np.sort(a, axis=None)

        print("a: ", a)
        print("b: ", b)


        for idx, row in condf.iterrows():
            # if idx > 0:
            #     continue
            print("row: ", idx, row[0])


            suffix = ""
            if row[6] == 1:
                suffix = "R1.fastq.gz"

            if row[7] == 1:
                suffix = "R2.fastq.gz"


            cmd = r"""cp %(fin)s %(fout)s
            """ % {"scratch_d": self.conf.scratch_loc_dir,
                   "fin": os.path.join(src_dir, row[0]),
                   "fout": os.path.join(dst_dir, "%s.%s.%s.%s.%s_%s" % (row[1], row[2], row[3], row[4], row[5], suffix))
                   }
            print(cmd)
            # p3 = subprocess.Popen(cmd, shell=True, cwd=dst_dir, stdout=subprocess.PIPE)
            # p3.wait()
            # rc3 = p3.returncode
            # print("rc3: ", rc3)
            # exit_codes.append(rc3)

            fin = os.path.join(src_dir, row[0])
            fout = os.path.join(dst_dir, "%s.%s.%s.%s.%s_%s" % (row[1], row[2], row[3], row[4], row[5], suffix))


            print("fin: ", fin)
            print("fout: ", fout)
            # process extraction of input file
            fidx = 0
            aidx = 0

            # with open(fout, 'w') as fho:
            with gzip.open(fout, 'wb') as fho:
                with pysam.FastxFile(fin) as fhi:
                    for entry_in in fhi:
                        if fidx > (max_reads-1) or aidx > (take_reads -1):
                            break
                        # if chosen
                        if fidx == b[aidx]:
                            # print("fidx: ", fidx)
                            # print(entry_in.name)
                            # print(entry_in.sequence)
                            # print(entry_in.comment)
                            # print(entry_in.quality)
                            aidx += 1
                            # fho.write(">%s\n%s\n+\n%s\n"  % (entry_in.name, entry_in.sequence, "".join([chr(ord(x) + 33)) for x in read.query_qualities)
                            str_entry = "@%s %s\n%s\n+\n%s\n"  % (entry_in.name, entry_in.comment, entry_in.sequence, entry_in.quality)
                            fho.write(str_entry.encode('utf-8'))
                        # next try
                        fidx += 1
                        # print("fidx: ", fidx)




        return [exit_codes]


    # only deploy one read (GQ style single ended)
    # read_depl:
    # read 1 = R1
    # read 2 = R2
    # tech_pref = "HI" / "MI"
    def fastq_rnfq1_deploy(self, tech_pref, read_depl, pref, suff, old_pref, old_suff):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        expr = "^%s\.(\d*)\.(\d*)\.([^.]*)\.([^.]*)_%s\.fastq\.gz$" % (tech_pref, read_depl)
        print("expr: ", expr)
        p = re.compile(expr)
        # only deploy

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match("(.+)_%s\.fastq\.gz$" % (read_depl), f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # ensure no quotion problem with the lane, prefix with 1
                self.dat_runreg = "run%s_%s" % (m.group(1), m.group(2))
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                self.dat_lib = m.group(3)
                # self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_single_f: ", self.sym_dst_single_f)
            # os.symlink(sym_src_f, self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_single_f)

            # print sample_f,lane_run_f,read_f
    # Hani
    # only deploy one read (GQ style single ended)
    def fastq_hnfq1_deploy(self, pref, suff, old_pref, old_suff):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        expr = "(.*)_L([^_]*)_R1_([^_]*)\.fastq\.gz$"
        print("expr: ", expr)
        p = re.compile(expr)
        # only deploy

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match(expr , f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # ensure no quotion problem with the lane, prefix with 1
                self.dat_runreg = "run%s_%s" % (m.group(3), m.group(2))
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                # self.dat_lib = m.group(3)
                self.dat_lib = "lib1"
                # self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_single_f: ", self.sym_dst_single_f)
            # os.symlink(sym_src_f, self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_single_f)

    # only deploy one read (GQ style single ended)
    # Claire collab single ended
    def fastq_clfq1_deploy(self, pref, suff, old_pref, old_suff):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        expr = "([^.]*)_R1_filtered\.fastq\.gz$"
        print("expr: ", expr)
        p = re.compile(expr)
        # only deploy

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match("([^.]*)_R1_filtered\.fastq\.gz$", f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                # ensure no quotion problem with the lane, prefix with 1
                self.dat_runreg = "run%s_%s" % ("100", "1")
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                self.dat_lib = "lib111"
                # self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_single_f: ", self.sym_dst_single_f)
            # os.symlink(sym_src_f, self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_single_f)


    # chee on miseq
    def fastq_chee_fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^MI\.([^.]*)\.([^.]*)\.([^.]*)\.([^.]*)_R(\d)\.fastq.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.fastq.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(4)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                self.dat_runreg = "run%s_%s" %( m.group(1), m.group(2) )
                self.dat_lib = m.group(3)
                self.dat_direct = m.group(5)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_f: ", self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_f)
            # print sample_f,lane_run_f,read_f

    # nalin exomes on surecall
    def fastq_nalin_fq_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        # p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz'cd)
        #p = re.compile('^([^_]*)_([^_]*)_([^_]*)_([^_]*)_([^_]*)_R(\d)\.fastq\.gz$')
        p = re.compile('^(.+)_R(\d)\.fastq\.gz$')

        # filter only fastq files, not log files
        for read in [f for f in os.listdir(self.fastq_dir) if re.match('(.+)\.fastq\.gz$', f)]:
            print("read: ", read)
            #
            m = p.match(read)
            if m:

                self.dat_smp = m.group(1)
                print("sample_f: ", self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)

                self.dat_runreg = "run%s_%s" %( "1", "L1" )
                self.dat_lib = "lib01"
                self.dat_direct = m.group(2)

                # print("self.dat_runreg: ", self.dat_runreg)
                # print("self.dat_lib: ", self.dat_lib)
                # print("self.dat_smp: ", self.dat_smp)
                # print("self.dat_direct: ", self.dat_direct)


            else:
                print('No match')

            # print("-----------------------read_f: \n", read)
            sym_src_f = os.path.join(self.fastq_dir, read)

            # also replace sample name in file name and reconstitute read name
            print("sym_src_f: ", sym_src_f)
            print("sym_dst_f: ", self.sym_dst_f)
            os.symlink(sym_src_f, self.sym_dst_f)
            # print sample_f,lane_run_f,read_f


    def fastq_hp_hs_deploy(self, pref, suff, old_pref, old_suff):

        # run_2475_keepR2_1

        p1 = re.compile('^run\_(\d*)\_keepR\d+\_(\d+)$')
        for f1 in [f for f in os.listdir(self.fastq_dir) if p1.match(f)]:
            # print("f1: ", f1)
            m1 = p1.match(f1)
            self.dat_runreg = r"""run%s_%s""" % (m1.group(1), m1.group(2))
            # a = m.group(1)
            # self.dat_lib = m.group(2)
            # self.dat_direct = m.group(3)
            print("self.dat_runreg: ", self.dat_runreg)

            p2 = re.compile('^Sample\_(\S+)\_(\S+\_\S+)$')
            f2_dir = os.path.join(self.fastq_dir, f1, "Project_nanuq")
            for f2 in [f for f in os.listdir(f2_dir) if p2.match(f)]:
                m2 = p2.match(f2)
                self.dat_smp = m2.group(1)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)
                #
                self.dat_lib = m2.group(2)
                print("--------- self.dat_smp: ", self.dat_smp)
                print("--------- self.dat_lib: ", self.dat_lib)

                p3 = re.compile('^(\S+)\_(\S+\_\S+)\_(\S+)\_(\S+)\_R(\d)\_\S+$')
                f3_dir = os.path.join(self.fastq_dir, f1, "Project_nanuq", f2)
                for f3 in [f for f in os.listdir(f3_dir) if p3.match(f)]:
                    print("f3: ", f3)
                    m3 = p3.match(f3)
                    self.dat_smp = m3.group(1)
                    self.dat_lib = m3.group(2)
                    idx = m3.group(3)
                    lane = m3.group(4)

                    # -1 is for default
                    self.dat_direct = {'1': '1', '3': '2', '2': '3'}.get(m3.group(5), '-1')
                    print("+++++++++++++++++++++++ self.dat_smp: ", self.dat_smp)
                    print("+++++++++++++++++++++++ self.dat_lib: ", self.dat_lib)
                    print("+++++++++++++++++++++++ self.dat_direct: ", self.dat_direct)

                    #
                    sym_src_f = os.path.join(self.fastq_dir, f1, "Project_nanuq", f2, f3)

                    # also replace sample name in file name and reconstitute read name
                    print("sym_src_f: ", sym_src_f)
                    print("sym_dst_f: ", self.sym_dst_f)
                    os.symlink(sym_src_f, self.sym_dst_f)






    def scan_sample_tags(self):
        # create hash for lookup tags --> names
        self.names_for_tags = {}

        ifile = open( self.sample_name_tags, "r")
        reader = csv.DictReader(ifile, delimiter=',')
        for line in reader:
            self.names_for_tags[line["Sample"]] = line["Name"]
        ifile.close()

    def fastq_fl01_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find tag and direction
        p = re.compile('(\w*)(_R)(\d)')

        # default values for all samples
        self.dat_lib = "fl1lib_001"
        self.dat_runreg = "run9999_1"
        self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

        # -----------------   cycle labfiles
        for sample_f in os.listdir(self.fastq_dir):

            m = p.match(sample_f)
            if m:
                sample_tag = m.group(1)
                self.dat_direct = m.group(3)
                self.dat_smp = self.names_for_tags[sample_tag].strip()
                print('Match found: ', sample_tag, self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                sym_src_f = os.path.join(self.fastq_dir, sample_f)
                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)

            else:
                print('No match')

    def fastq_fl02_deploy(self, pref, suff, old_pref, old_suff):


        # regular expression to find tag and direction
        pat = '^L(\d+)_(\w*)_(L\d+)_R(\d)_(\d+)'
        p = re.compile(pat)

        # ############## cycle labfiles

        for sample_f in os.listdir(self.fastq_dir):
            # print("sample_f: ", sample_f)
            # print("pat: ", pat)
            m = p.match(sample_f)
            if m:
                lane = m.group(1)

                sample_tag = m.group(2)
                self.dat_smp = self.names_for_tags[sample_tag].strip()
                print('Match found: ', sample_tag, self.dat_smp)
                # transform sample name
                self.dat_smp_transform(pref, suff, old_pref, old_suff)


                self.dat_lib = "fl2lib_%s" % m.group(3)
                self.dat_direct = m.group(4)
                run = m.group(5)

                self.dat_runreg = "run%s_%s" %(run, lane)
                self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)


                #
                sym_src_f = os.path.join(self.fastq_dir, sample_f)
                # also replace sample name in file name and reconstitute read name
                print("sym_src_f: ", sym_src_f)
                print("sym_dst_f: ", self.sym_dst_f)
                os.symlink(sym_src_f, self.sym_dst_f)

            else:
                print('No match')

    def fastq_fl03_deploy(self, pref, suff, old_pref, old_suff):

        # regular expression to find sample name
        # p0 = re.compile('^('+ old_pref + ')(\w*)(_fastq)')
        # pat1 = r"""^(%s)(\S*)(%s)$""" % (old_pref, old_suff)
        # regular expression to find tag and direction
        pat2 = '^L(\d+)_(\w*)_(L\d+)_R(\d)_(\d+)'
        # p1 = re.compile(pat1)
        p2 = re.compile(pat2)

        for fname in os.listdir(self.fastq_dir):
            # add filename
            # pd_fnames = pd.Series(np.concatenate([pd_fnames, pd.Series([fname], dtype="|S75")]))
            # m1 = p1.match(fname)
            # if m1:
                # print "m: ", m.group(0), m.group(1), m.group(2), m.group(3)
                # lane = region
                # self.dat_smp = m1.group(2)
                # print('Match found: ', self.dat_smp)

            self.dat_smp = fname
            # transform sample name
            self.dat_smp_transform(pref, suff, old_pref, old_suff)
            # cycle sample files
            fname_dir = os.path.join(self.fastq_dir, fname)
            for fname2 in os.listdir(fname_dir):
                m2 = p2.match(fname2)
                if m2:
                    print("m2: ", m2.group(0), m2.group(1), m2.group(2), m2.group(3), m2.group(4), m2.group(5))
                    lane = m2.group(1)
                    self.dat_lib = "fl3lib_%s" % m2.group(3)
                    self.dat_direct = m2.group(4)
                    run = m2.group(5)

                    self.dat_runreg = "run%s_%s" % (run, lane)
                    self.dat_runreg_ren = self.correct_zero_start_run_lane(self.dat_runreg)

                    #
                    sym_src_f = os.path.join(fname_dir, fname2)
                    # also replace sample name in file name and reconstitute read name
                    print("sym_src_f: ", sym_src_f)
                    print("sym_dst_f: ", self.sym_dst_f)
                    os.symlink(sym_src_f, self.sym_dst_f)


    def loop(self, gid):
        self.redis_obj.gid = gid

        print("in loop...", gid)
        # recuperate local id
        lid = int(self.redis_obj.redis_client.hget("client:%s" % gid, "lid"))
        print("lid: ", lid)
        # set pid
        self.redis_obj.register_pid()
        print("pid: ", self.redis_obj.pid)

        while self.redis_obj.redis_client.get("shut_queues") == "0" and not self.redis_obj.qgraph_all_done:
            time.sleep(8)
            print("active_queues: ", self.redis_obj.qgraph_prop_open)
            print("lid: ", lid)
            print("all done: ",  self.redis_obj.qgraph_all_done)

            cand_qtsks = []
            for tsk, props in self.conf.tsk_nde_rsc_dict.items():
                print("key: ", tsk, "props: ", props)
                if tsk in self.redis_obj.qgraph_prop_open \
                        and lid < props["tsk_nde"]:
                    cand_qtsks.append(tsk)

            print("candidate task queues: ", cand_qtsks)

            # try them out
            for cnd in cand_qtsks:
                self.redis_obj.qwork = cnd
                self.cor_tsk = self.conf.tsk_nde_rsc_dict[cnd]["cor_tsk"]
                self.mem_tsk = self.conf.tsk_nde_rsc_dict[cnd]["mem_tsk"]

                self.unpack_execute_qwork()

        print("loop finished ok..")




# ----------------- PROJECTS DEPLOYMENT

    # ----------------------------- INDIVIDUAL FOLDERS

    def deploy_dat_gq00_008(self):

        print("in deploy_dat_gq00_008 ...")


        self.fastq_dir_name = "/gs/project/wst-164-ab/data/008-tonin-p01/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/exome/caa-008/raw_reads"

        self.fastq_gq00_deploy("", "-caa", "", "")


    # ----------------------------- WHOLE PROJECTS

    def deploy_caa(self):
        self.deploy_dat_gq00_008()


    # Leann Dicer1 P2 - Haloplex
    def deploy_haa(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/902-dicer-p2/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/exome/haa-902/raw_reads"

        self.fastq_gq00_deploy("", "-haa", "", "")

    # Leann Dicer1 P4 - Fluidigm
    def deploy_hba(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/903-dicer-p4/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/exome/hba-903/raw_reads"

        self.fastq_gq00_deploy("", "-hba", "", "")

    # Leann Dicer1 P5 - Fluidigm
    def deploy_hca(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/904-dicer-p5/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/exome/hca-904/raw_reads"

        self.fastq_gq00_deploy("", "-hca", "", "")


    # Leann Dicer1 merged(P4,P5) - Fluidigm
    def deploy_hcb(self):


        self.depl_dir = "/gs/project/wst-164-ab/exome/hcb-903-904/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/903-dicer-p4/fastq"
        self.fastq_gq00_deploy("", "-hcb", "", "")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/904-dicer-p5/fastq"

        # self.smp_ren_dict = dict(TC_Nextera_NTC='Paul3_TC_Nextera_NTC',
        #                     TC_Smarter_NTC='Paul3_TC_Smarter_NTC')
        self.fastq_gq00_deploy("", "-hcb", "", "-2")




    def deploy_hda(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/905-dicer-hs/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/exome/hda-905/raw_reads"

        self.fastq_hp_hs_deploy("", "-hda", "", "")

    # Paul9 rnaseq
    def deploy_eda(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/019-er-paul9/fastq/"
        self.depl_dir = "/gs/project/wst-164-ab/expr/eda-019/raw_reads"

        self.fastq_gqfq_deploy("", "-eda", "", "")

    # CTC1 rnaseq
    def deploy_iaa(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/017-er-ctc1/fastq/"
        self.depl_dir = "/gs/project/wst-164-ab/expr/iaa-017/raw_reads"

        self.fastq_gq00_deploy("", "-iaa", "", "")

    # CTC2 rnaseq
    def deploy_iba(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/019-er-paul9/fastq/"
        self.depl_dir = "/gs/project/wst-164-ab/expr/eda-019/raw_reads"

        self.fastq_gqfq_deploy("", "-iba", "", "")


    # HEK nanopore-pacbio-illumina rnaseq
    def deploy_naa(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/020-np_pb_il/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/expr/naa-020/raw_reads"

        self.fastq_gqfq_deploy("", "-naa", "", "")

    # Chee HLA
    def deploy_gaa(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/906-chee-hla/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/hla/gaa-906/raw_reads"

        self.fastq_chee_fq_deploy("", "-gaa", "", "")

    # Nalin exomes
    def deploy_taa(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/908-nalin-exomes/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/exome/taa-908/raw_reads"

        self.fastq_nalin_fq_deploy("", "-taa", "", "")

    # Wafergen
    # def deploy_aaa(self):
    def deploy_a008(self):

        # self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/d01-wfrgn-p01/fastq"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x032-wfrgn-p01/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/exome/a008-x032-x033/raw_reads"
        self.fastq_gq00_deploy("", "-a008", "", "")

        # self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/d02-wfrgn-p01/fastq"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x033-wfrgn-p01/fastq"

        #self.depl_dir = "/gs/project/wst-164-ab/share/exome/aaa-wg-d01-d02/raw_reads"
        self.depl_dir = "/gs/project/wst-164-ab/share/exome/a008-x032-x033/raw_reads"
        self.fastq_gq00_deploy("AFN-0", "-a008", "", "")

    # x034-rat3smp
    def deploy_a009(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x034-rat3smp/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a009-x034/raw_reads"
        self.fastq_gq00_deploy("", "-a009", "", "")



    # Wafergen kind qiagen
    def deploy_aba(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/024-qiagen_bc/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/exome/aba-024/raw_reads"
        self.fastq_gq00_deploy("", "-aba", "", "")

    # Paul haloplex hs deduplicated bams to alignement2
    def deploy_a003(self):

        self.bam_data_dir = "/gs/project/wst-164-aa/share/data/x003-paul-hs/bam"
        self.depl_dir = "/gs/project/wst-164-aa/share/exome/a003-x003/alignment2"
        self.bam_hs00_deploy("", "-a003", "", "")


    # Paul9 rnvar
    def deploy_a004(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/019-er-paul9/fastq/"
        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a004-019/raw_reads"

        self.fastq_gqfq_deploy("", "-a004", "", "")

    # Axel Thomson rat part I -VMP
    def deploy_a005(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x030-x031-axel-ss-rna/fastq-030-vmp/"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a005-d030/raw_reads"

        self.fastq_gqfq_deploy("", "-a005", "", "")

    # Axel Thomson rat part II -SU
    def deploy_a006(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x030-x031-axel-ss-rna/fastq-031-su/"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a006-x031/raw_reads"

        self.fastq_gqfq_deploy("", "-a006", "", "")


    # x036- htchip Paul II 5-10% error
    def deploy_a010(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x036-p12-htchip/fastq-htchip"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a010-x036/raw_reads"
        self.fastq_htfq_deploy("", "-a010", "1868_", "")


    # Paul9 reseq II rnvar
    def deploy_a011(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a011-019-x038-cor/raw_reads"
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/019-er-paul9/fastq"
        self.fastq_gqfq_deploy("", "-a011", "", "")
        # resequencing for higher depth
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x038-paul9/fastq"
        self.fastq_gqfq_deploy("", "-a011", "", "")

    # paul 1876 pdx
    def deploy_a012(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a012-x040/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x040-paul_1876pdx/fastq"
        self.fastq_gqfq_deploy("", "-a012", "", "")

    def deploy_a013(self):

        # cd /gs/project/wst-164-ab/data/006-ti-fludigm/pysrc
         # original Germline , Pre-, Post-treatment
        # def deploy_dat_gq00_06():

        self.depl_dir = "/gs/project/wst-164-ab/share/exome/a013-x041/raw_reads"

        print("in deploy_dat_gq00_06 ...")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/006-ti-fludigm/fastq"

        self.smp_ren_dict = {"Post_Treatment_1735": 'Germline-a013',
                            "Normal": 'Post_Treatment_1735-a013',
                            "Pre_Treatment_1735_lowinput": 'Pre_Treatment_1735-a013'}

        self.fastq_gq00_deploy("", "-a013", "", "")


        # resequencing of the gq00_06 (Germline , Pre-, Post-treatment)
        # def deploy_dat_gq00_13():

        print("in deploy_dat_gq00_13 ...")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/013-ti-fluidigm/fastq"

        self.smp_ren_dict = {"Pre_Treatment_1735_lowinput-13": 'Pre_Treatment_1735-a013',
                            "Paul5_Unamp_gDNA-13": 'P5_GD_UN-a013'}
        self.fastq_gq00_deploy("", "-a013", "", "-13")

        # original Paul4 - part I
        # def deploy_dat_fl01_03():

        print("in deploy_dat_fl01_03 ...")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/013-ti-fluidigm/fastq-fl01-03"

        self.smp_ren_dict = dict()

        self.sample_name_tags = os.path.join("/gs/project/wst-164-ab/data/013-ti-fluidigm/conf", "sample-name-tag2.csv")
        self.scan_sample_tags()

        self.fastq_fl01_deploy("", "-a013", "", "")

        # exit(0)


        # original Paul4 - part II
        # def deploy_dat_fl02_04():

        print("in deploy_dat_fl02_04 ...")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/013-ti-fluidigm/fastq-fl02-04"

        self.smp_ren_dict = dict()

        self.sample_name_tags = os.path.join("/gs/project/wst-164-ab/data/013-ti-fluidigm/conf", "sample-name-tag2.csv")
        self.scan_sample_tags()

        self.fastq_fl02_deploy("", "-a013", "", "")

        # resequencing of the 04 blob
        # def deploy_dat_gq00_004r():

        print("in deploy_dat_gq00_004r ...")
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/004-blob-reseq/fastq"
        self.fastq_gq00_deploy("", "-a013", "", "-04")

        # second resequencing x41
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/x041-da-exomes/fastq-04"
        self.fastq_gq00_deploy("", "-a013", "", "-04")


        # original Paul5 - part II (D11-H9+UN+TC200)
        # def deploy_dat_fl03_05():

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/013-ti-fluidigm/fastq-fl03-05"

        self.smp_ren_dict = dict()

        self.smp_ren_dict = {"Ext6_Test5B_UnAmp_fastq": 'P5_GD_UN-a013',
                            "Ext6_Test5B_TC_200_fastq": 'P5_TC_200-a013',
                            "Ext6_Test5B_G02_Empty_C38_fastq": 'P5_SC_G02_E_C38-a013'}

        self.fastq_fl03_deploy("P5_SC_", "-a013", "Ext6_Test5B_", "_fastq")

        # original Paul5 - part I (A01-D10)
        # def deploy_dat_fl03_07():


        self.fastq_dir_name = "/gs/project/wst-164-ab/data/013-ti-fluidigm/fastq-fl03-07"

        self.smp_ren_dict = dict()

        self.fastq_fl03_deploy("P5_SC_", "-a013", "Ext6A2_Test_", "_fastq")
        #
        print("in deploy_dat ...")

        # resequencing of the paul4 (fl01_03, fl02_04)
        # def deploy_dat_gq00_003():
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/003-da-exomes/fastq"
        self.fastq_gq00_deploy("", "-a013", "", "-03")

        # second resequencing x41
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/x041-da-exomes/fastq-03"
        self.fastq_gq00_deploy("", "-a013", "", "-03")


        # resequencing of the paul5 (fl03_05)
        # def deploy_dat_gq00_005():

        print("in deploy_dat ...")
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/005-da-exomes/fastq"
        self.fastq_gq00_deploy("P5_SC_", "-a013", "", "-05")

        # second resequencing x41
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/x041-da-exomes/fastq-05"
        self.fastq_gq00_deploy("P5_SC_", "-a013", "", "-05")


        # resequencing of the paul5 (fl03_07)
        # def deploy_dat_gq00_007():

        print("in deploy_dat ...")

        self.fastq_dir_name = "/gs/project/wst-164-ab/data/007-da-exomes/fastq"
        self.fastq_gq00_deploy("P5_SC_", "-a013", "", "-07")
        # second resequencing x41
        self.fastq_dir_name = "/gs/project/wst-164-ab/data/x041-da-exomes/fastq-07"
        self.fastq_gq00_deploy("P5_SC_", "-a013", "", "-07")

    # ss4 clontech
    def deploy_a014(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a014-x042/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x042-ss4-clt/fastq"
        self.fastq_gqfq_deploy("", "-a014", "", "")

    # ss4 single cells
    def deploy_a015(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a015-x043/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x043-ss4-sc/fastq"
        self.fastq_gqfq_deploy("", "-a015", "", "")

    # polaris ilona single cells egfr+
    def deploy_a016(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a016-x045/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x045-pol-ilona/fastq"
        self.fastq_bsfq_deploy("", "-a016", "", "")

    # x036- htchip Paul III  ?5-10% error?
    def deploy_a00ht(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        self.fastq_dir_name = "/lb/project/ioannisr/share/expr/a00ht/fastq-htchip"
        self.depl_dir = "/lb/project/ioannisr/share/expr/a00ht/raw_reads"
        self.fastq_htfq1_deploy("", "-a00ht", "1868_", "")

    # x046-paul-HCI001 PDX single cells
    # def deploy_a018(self):
    #
    #     self.depl_dir = "/gs/project/wst-164-ab/share/expr/a018-x046/raw_reads"
    #     self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x046-paul-HCI001/fastq"
    #     self.fastq_gqfq_deploy("", "-a018", "", "")



    # x047-mouse-jiarong-12x cells
    def deploy_a019(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a019-x047/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x047-mouse-jiarong-12x/fastq"
        self.fastq_gqfq_deploy("", "-a019", "", "")

    # Axel Thomson rat part III -VP
    def deploy_a021(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x048-axel-vp/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a021-x048-axel-vp/raw_reads"

        self.fastq_gqfq_deploy("", "-a021", "", "")

    def deploy_a024(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/027-camps-RISC/fastq"
        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a024-027-camps-risc/raw_reads"

        self.fastq_rnfq1_deploy("HI", "R1", "", "-a024", "", "")

    def deploy_a025(self):

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x049-sanger-ss/fastq_ren"
        self.depl_dir = "/gs/project/wst-164-aa/share/expr/a025-x049-sanger-sc/raw_reads"

        self.fastq_rnfq1_deploy("HI", "R1", "", "-a025", "", "")

    # a026-x056-1876-e12
    def deploy_a026(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a026-x056-1876-e12/raw_reads"
        self.smp_ren_dict = dict(EGFRN_TC_RNEASY_1OF2='EGFRn_TC_RNEASY_1OF2-a026',
                                 EGFRN_TC_RNEASY_2OF2='EGFRn_TC_RNEASY_2OF2-a026')

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x056-1876-e12/fastq1"
        self.fastq_gqfq_deploy("", "-a026", "", "")

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x056-1876-e12/fastq2"
        self.fastq_gqfq_deploy("", "-a026", "", "")

    # a027-x058-hci001-e14
    def deploy_a027(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a027-x058-hci001-e14/raw_reads"
        # self.smp_ren_dict = dict(EGFRN_TC_RNEASY_1OF2='EGFRn_TC_RNEASY_1OF2-a026',
        #                          EGFRN_TC_RNEASY_2OF2='EGFRn_TC_RNEASY_2OF2-a026')

        self.smp_skp_arr = ["WB_C15", "B_C53", "B_C80",
                            "B_C79", "WB_C34", "WB_C35",
                            "B_OFF_C37", "B_C87", "WB_C86",
                            "WB_C41", "B_C90", "B_C43",
                            "B_C91", "WB_C46", "WB_C94",
                            "WB_C24", "WB_C27", "WB_C26",
                            "WB_C75", "B_C78", "WB_C76",
                            "WB_C77", "B_C33", "B_C31",
                            "TC_1000_1of2"]

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x058-hci001-e14/fastq1"
        self.fastq_gqfq_deploy("", "-a027", "", "")


    # 1735 pdx E10 - x054 clontech
    def deploy_a028(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a028-x054-1735-e10/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x054-1735-e10/fastq_egfr"

        self.smp_ren_dict = dict(Clontech_EGFRp_TC_200='TC_EGFRp_200-a028',
                                 Clontech_EGFRn_TC_200='TC_EGFRn_200-a028',
                                 Clontech_EGFRp_TC_RNEASY_1OF2='TC_EGFRp_RNEASY_1OF2-a028',
                                 Clontech_EGFRp_TC_RNEASY_2OF2='TC_EGFRp_RNEASY_2OF2-a028',
                                 Clontech_EGFRn_TC_RNEASY_1OF2='TC_EGFRn_RNEASY_1OF2-a028',
                                 Clontech_EGFRn_TC_RNEASY_2OF2='TC_EGFRn_RNEASY_2OF2-a028',
                                 Clontech_EGFR_TC_NTC='TC_EGFR_NTC-a028')


        self.fastq_gqfq_deploy("", "-a028", "", "")

    # bcm2665 E15 x059
    def deploy_a029(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/expr/a029-x059-BCM2665-e15/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x059-bcm2665-e15/fastq_3642_7"

        self.smp_ren_dict = dict(Add_to_B_C53='A_B_C53-a029',
                                 Add_to_B_C57='A_B_C57-a029',
                                 Add_to_WB_C12='A_WB_C12-a029',
                                 Add_to_B_C11='A_B_C11-a029',
                                 Add_to_WB_C14='A_WB_C14-a029',
                                 Add_to_B_C60='A_B_C60-a029',
                                 Add_to_B_C13='A_B_C13-a029',
                                 Add_to_B_C61='A_B_C61-a029',
                                 BOFF_C26='A_B_OFF_C26-a029'
                                )

        self.smp_skp_arr = ["A_B_C81-a029", "A_B_C20-a029", "A_B_C02-a029"]

        self.fastq_gqfq_deploy("A_", "-a029", "", "")

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x059-bcm2665-e15/fastq_3642_8"
        # from more precise
        self.smp_lib_ren_dict = {("EGFRp_TC_200", "N703---S511"): "TC_200_EGFRp-a029",
                                 ("EGFRp_TC_RNeasy", "N704---S511"): "TC_RN_EGFRp-a029",
                                 ("EGFRp_TC_200", "N705---S511"): "TC_200_EGFRn-a029",
                                 ("EGFRp_TC_RNeasy", "N706---S511"): "TC_RN_EGFRn-a029"
                                 }
        # or less precise
        self.smp_ren_dict = dict(Add_to_B_C53='B_B_C53-a029',
                                 Add_to_B_C57='B_B_C57-a029',
                                 Add_to_WB_C12='B_WB_C12-a029',
                                 Add_to_B_C11='B_B_C11-a029',
                                 Add_to_WB_C14='B_WB_C14-a029',
                                 Add_to_B_C60='B_B_C60-a029',
                                 Add_to_B_C13='B_B_C13-a029',
                                 Add_to_B_C61='B_B_C61-a029',
                                 Unsorted_TC_RNeasy='TC_RN_UNS-a029',
                                 EGFRp_TC_RNeasy='TC_RN_EGFRp-a029',
                                 EGFRn_TC_RNeasy='TC_RN_EGFRn-a029',
                                 SMARTer_NTC='TC_NTC_SMRT-a029',
                                 Unsorted_TC_1000_1of2='TC_1000_UNS_12-a029',
                                 Unsorted_TC_1000_2of2='TC_1000_UNS_22-a029',
                                 Unsorted_TC_200='TC_200_UNS-a029'

                         )
        # skip final file names
        self.smp_skp_arr = ["B_B_C69-a029",
                            "B_WB_C29-a029",
                            "B_B_C22-a029",
                            "B_B_C93-a029",
                            "TC_RN_UNS-a029",
                            "TC_RN_EGFRp-a029",
                            "TC_RN_EGFRn-a029",
                            "TC_NTC_SMRT-a029"]

        self.fastq_gqfq_deploy("B_", "-a029", "", "")

    # debug deploy 1867 donwsized
    def deploy_a001(self):

        # self.depl_dir = "/gs/project/wst-164-aa/share/expr/a001-d001/raw_reads"
        self.depl_dir = "/gs/project/wst-164-ab/share/data/d001-1876-sml/raw_reads"
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/d001-1876-sml/fqren_90k_50k"

        self.smp_ren_dict = {}
        self.smp_skp_arr = []

        self.fastq_gqfq_deploy("", "-a001", "", "")

    # Paul9 reseq III
    def deploy_a030(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a030-x053-1735-e09/raw_reads"
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-019"

        # skip final file names
        self.smp_skp_arr = ["20160201_Clontech_A03_SG_C01-a030",
                            "20160201_Clontech_A03_SG_C01-a030",
                            "20160201_SS4_TC_NTC-a030",
                            "20160201_SS4_A01_MG_C03-a030"]
        self.fastq_gqfq_deploy("", "-a030", "", "")
        # resequencing for higher depth
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-x038"

        # skip final file names
        self.smp_skp_arr = ["20160201_Clontech_A03_SG_C01-a030",
                            "20160201_Clontech_A03_SG_C01-a030",
                            "20160201_SS4_TC_NTC-a030",
                            "20160201_SS4_A01_MG_C03-a030"]
        self.fastq_gqfq_deploy("", "-a030", "", "")

                # egfr controls
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x054-1735-e10/fastq_egfr"

        self.smp_ren_dict = dict(Clontech_EGFRp_TC_200='TC_EGFRP_200_A034',
                                 Clontech_EGFRn_TC_200='TC_EGFRN_200_A034',
                                 Clontech_EGFRp_TC_RNEASY_1OF2='TC_EGFRP_RNEASY_1OF2_A034',
                                 Clontech_EGFRp_TC_RNEASY_2OF2='TC_EGFRP_RNEASY_2OF2_A034',
                                 Clontech_EGFRn_TC_RNEASY_1OF2='TC_EGFRN_RNEASY_1OF2_A034',
                                 Clontech_EGFRn_TC_RNEASY_2OF2='TC_EGFRN_RNEASY_2OF2_A034',
                                 Clontech_EGFR_TC_NTC='TC_EGFR_NTC_A034')


        self.fastq_gqfq_deploy("", "_A034", "", "")

    # Paul8 old - updated pipeline
    def deploy_a031(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a031-x052-1735-e08/raw_reads"
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x052-1735-e08/fastq_2503_7_8"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"Paul8_SMARTer_NTC": 'E08_TC_NTC_SMRT-a031',
                             "Tag_NTC": 'E08_TC_NTC_Tag-a031',
                             "Paul8_TC_200_1of2": 'E08_TC_200_UNS_12-a031',
                             "Paul8_TC_200_2of2": 'E08_TC_200_UNS_22-a031',
                             "Paul8_TC_RNeasy_1of2": 'E08_TC_RN_UNS_12-a031'
                             }
        self.smp_skp_arr = []
        self.fastq_gq00_deploy("E08_", "-a031", "Paul8_", "")

    # CTC pre-treatment
    def deploy_a032(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a032-x062-e01/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x062-ctc-8XPE100_P0466204-pre-treat-11/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"SMARTer_NTC_11": 'NTC_SMRT_U_A032',
                             "CD45nCD8p_TC_RNeasy_1of2_11": "TC_RN_NP_12_A032",
                             "CD45nCD8p_TC_RNeasy_2of2_11": "TC_RN_NP_22_A032",
                             "CD45pCD8n_TC_RNeasy_1of2_11": "TC_RN_PN_12_A032",
                             "CD45pCD8n_TC_RNeasy_2of2_11": "TC_RN_PN_22_A032",
                             "All_Before_Stain_11": "ALL_BEF_STA_U_A032",
                             "CD45nCD8n_TC_RNeasy_11": "TC_RN_NN_A032",
                             "CD45pCD8p_TC_RNeasy_11": "TC_RN_PP_A032",
                             "CD45nCD8n_TC_200_1of2_11": "TC_200_NN_12_A032",
                             "CD45nCD8n_TC_200_2of2_11": "TC_200_NN_22_A032",
                             "CD45nCD8p_TC_200_1of2_11": "TC_200_NP_12_A032",
                             "CD45nCD8p_TC_200_2of2_11": "TC_200_NP_22_A032",


                             }
        
        # this is applied on final data, so final names should be there (uppercase)
        # self.smp_skp_arr = ["ALL_BEFORE_STAIN_11_NP_A032",
        #     "CD45NCD8N_TC_200_1OF2_11_NP_A032", "CD45NCD8N_TC_200_2OF2_11_NP_A032",
        #                     "TC_200_1OF2_NP_A032", "TC_200_2OF2_NP_A032",
        #                     "SC_A01_G_C03_NP_A032", "SC_B01_G_C09_NP_A032", "SC_B02_G_C08_NP_A032",
        #                     "SC_B04_G_C55_NP_A032", "SC_C01_G_C15_NP_A032", "SC_C03_G_C13_NP_A032",
        #                     "SC_C08_G_C17_NP_A032", "SC_D01_G_C21_NP_A032", "SC_D02_G_C20_NP_A032",
        #                     "SC_D04_G_C67_NP_A032", "SC_D07_GTINY_C24_NP_A032",
        #                     "SC_D10_G_C22_NP_A032", "SC_E01_EMPTY_C25_NP_A032",
        #                     "SC_E03_G_C27_NP_A032", "SC_E04_G_C75_NP_A032", "SC_E08_G_C29_NP_A032",
        #                     "SC_E10_G_C30_NP_A032", "SC_F01_G_C31_NP_A032", "SC_F02_G_C32_NP_A032",
        #                     "SC_F04_G_C81_NP_A032", "SC_F08_G_C35_NP_A032", "SC_G02_G_C38_NP_A032",
        #                     "SC_G12_G_C88_NP_A032", "SC_H01_G_C43_NP_A032", "SC_H07_G_C46_NP_A032",
        #                     "SC_H09_G_C96_NP_A032",
        #                     ]

        self.fastq_gq00_deploy("", "_NP_A032", "CD45nCD8p_", "_11")

    # paul3 resequenced
    def deploy_a035(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a035-1735-e03-015-x065/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/015-er-paul3/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"TC_Smarter_NTC": "NTC_SMRT_11_U_A035",
                             "TC_Nextera_NTC" : "NTC_NXTR_11_U_A035",
                             "Paul3_TC_RNeasy_1of2" : "TC_RN_12_U_A035",
                             "Paul3_TC_RNeasy_2of2" : "TC_RN_22_U_A035",
        #                      "CD45pCD8n_TC_RNeasy_2of2_11" : "TC_RN_22_PN_A032",
        #                      "All_Before_Stain_11" : "ALL_BEF_STA_U_A032",
                             # "CD45nCD8n_TC_RNeasy_11" : "TC_RN_11_NN_A032",
                             # "CD45pCD8p_TC_RNeasy_11" : "TC_RN_11_PP_A032",
                             }

        self.fastq_gq00_deploy("A_", "_A035", "Paul3_", "")

        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x065-1735-e03/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"Paul3_SC_H05_VERYBIG_G_C92": 'B_SC_H05_VB_G_C92_A035',
        #                      "CD45nCD8p_TC_RNeasy_1of2_11" : "TC_RN_12_NP_A032",
        #                      "CD45nCD8p_TC_RNeasy_2of2_11" : "TC_RN_22_NP_A032",
        #                      "CD45pCD8n_TC_RNeasy_1of2_11" : "TC_RN_12_PN_A032",
        #                      "CD45pCD8n_TC_RNeasy_2of2_11" : "TC_RN_22_PN_A032",
        #                      "All_Before_Stain_11" : "ALL_BEF_STA_U_A032",
                             # "CD45nCD8n_TC_RNeasy_11" : "TC_RN_11_NN_A032",
                             # "CD45pCD8p_TC_RNeasy_11" : "TC_RN_11_PP_A032",
                             }

        self.fastq_gq00_deploy("B_", "_A035", "Paul3_", "")

    # cleaned a018
    # x046-paul-HCI001 PDX single cells
    def deploy_a036(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a036-x046-hci001-e13/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x046-paul-HCI001/fastq"

        # this is applied on final data, so final names should be there (uppercase)
        self.smp_skp_arr = ["A-LG_C44_A036", "B-MG_C50_A036", "A-MG_C83_A036",
                            "A-SWG_C74_A036"
                            ]

        self.fastq_gqfq_deploy("", "_A036", "", "")

    # paul 1876 pdx
    def deploy_a037(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a037-x040-1876-e11/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)

        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x040-paul_1876pdx/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"Clontech_TC_NTC": "NTC_CLTH_A037",
                             "Clontech_TC_Rneasy_1of3" : "TC_RN_12_A037",
                             "Clontech_TC_Rneasy_2of3" : "TC_RN_22_A037",

                             }

        # this is applied on final data, so final names should be there (uppercase)
        self.smp_skp_arr = ["MG_C04_A037","MG_C53_A037","MGX_C06_A037",
                            "MG_C54_A037","MGX_C02_A037","MG_C57_A037",
                            "SG_C56_A037","SG_C42_A037","SG_C92_A037",
                            "MGX_C91_A037","MG_C82_A037","MGX_C87_A037","MG_C85_A037"
                            ]


        self.fastq_gqfq_deploy("", "_A037", "", "")


    # Paul9 reseq III -fusion detection
    def deploy_a034(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a034-1735-fusion2/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-019"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A034",
                            "20160201_CLONTECH_A03_SG_C01_A034",
                            "20160201_SS4_TC_NTC_A034",
                            "20160201_SS4_A01_MG_C03_A034"]
        self.fastq_gqfq_deploy("", "_A034", "Paul9_", "")
        # resequencing for higher depth
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-x038"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A034",
                            "20160201_CLONTECH_A03_SG_C01_A034",
                            "20160201_SS4_TC_NTC_A034",
                            "20160201_SS4_A01_MG_C03_A034"]
        self.fastq_gqfq_deploy("", "_A034", "Paul9_", "")

        # egfr controls
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x054-1735-e10/fastq_egfr"

        self.smp_ren_dict = dict(Clontech_EGFRp_TC_200='TC_EGFRP_200_A034',
                                 Clontech_EGFRn_TC_200='TC_EGFRN_200_A034',
                                 Clontech_EGFRp_TC_RNEASY_1OF2='TC_EGFRP_RNEASY_1OF2_A034',
                                 Clontech_EGFRp_TC_RNEASY_2OF2='TC_EGFRP_RNEASY_2OF2_A034',
                                 Clontech_EGFRn_TC_RNEASY_1OF2='TC_EGFRN_RNEASY_1OF2_A034',
                                 Clontech_EGFRn_TC_RNEASY_2OF2='TC_EGFRN_RNEASY_2OF2_A034',
                                 Clontech_EGFR_TC_NTC='TC_EGFR_NTC_A034')


        self.fastq_gqfq_deploy("", "_A034", "", "")

    # Paul9 reseq III -fusion detection 2
    # redeploy only the previously extracted non-canonical reads
    def deploy_a034_2(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a034-1735-fusion2-bbmap/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/rnvar/a034-1735-fusion2/fastq_noncan"

        self.fastq_gqfq_deploy("", "", "", "")

    # Paul9 reseq III
    def deploy_a038(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a038-x053-1735-e09/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)

        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-019"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_SS4_TC_NTC_A038",
                            "20160201_SS4_A01_MG_C03_A038",
                            "SC_E11_G__C77_A038"]
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Paul9_TC_200_1of2='TC_200_U_12_A038',
                                 Paul9_TC_200_2of2='TC_200_U_22_A038',
                                 Paul9_TC_RNeasy_1of2='TC_RN_U_12_A038',
                                 Paul9_TC_RNeasy_2of2='TC_RN_U_22_A038',
                                 Paul9_SMARTer_NTC='NTC_SMRT_U_A038')

        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")
        # resequencing for higher depth
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-x038"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_SS4_TC_NTC_A038",
                            "20160201_SS4_A01_MG_C03_A038"]
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Paul9_TC_200_1of2='TC_200_U_12_A038',
                                 Paul9_TC_200_2of2='TC_200_U_22_A038',
                                 Paul9_TC_RNeasy_1of2='TC_RN_U_12_A038',
                                 Paul9_TC_RNeasy_2of2='TC_RN_U_22_A038',
                                 Paul9_SMARTer_NTC='NTC_SMRT_U_A038')

        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")

        # egfr controls
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x054-1735-e10/fastq_egfr"
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Clontech_EGFRp_TC_200='TC_200_P_A038',
                                 Clontech_EGFRn_TC_200='TC_200_N_A038',
                                 Clontech_EGFRp_TC_RNEASY_1OF2='TC_RN_P_12_A038',
                                 Clontech_EGFRp_TC_RNEASY_2OF2='TC_RN_P_22_A038',
                                 Clontech_EGFRn_TC_RNEASY_1OF2='TC_RN_N_12_A038',
                                 Clontech_EGFRn_TC_RNEASY_2OF2='TC_RN_N_22_A038',
                                 Clontech_EGFR_TC_NTC='NTC_SMRT_S_A038')


        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")

    # Paul9 reseq IV
    def deploy_a038_ctl(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a038-x053-1735-e09-ctl/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        os.mkdir(self.depl_dir)

        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-019"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_SS4_TC_NTC_A038",
                            "20160201_SS4_A01_MG_C03_A038",
                            "SC_E11_G__C77_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_SS4_TC_NTC_A038",
                            "20160201_SS4_A01_MG_C03_A038",
                            "NTC_SMRT_S_A038",
                            "NTC_SMRT_U_A038",
                            "SC_A05_G__C50_A038",
                            "SC_A07_G__C06_A038",
                            "SC_A08_G__C05_A038",
                            "SC_A09_G__C04_A038",
                            "SC_A09_G__C04_A038",
                            "SC_B10_G__C58_A038",
                            "SC_B12_G__C60_A038",
                            "SC_A10_G__C52_A038",
                            "SC_C04_G__C61_A038",
                            "SC_C07_G__C18_A038",
                            "SC_C09_G__C16_A038",
                            "SC_C10_G__C64_A038",
                            "SC_C12_G__C66_A038",
                            "SC_D03_G__C19_A038",
                            "SC_E03_G__C27_A038",
                            "SC_E05_G__C74_A038",
                            "SC_E10_G__C78_A038",
                            "SC_E11_G__C77_A038",
                            "SC_F04_G__C81_A038",
                            "SC_F05_G__C80_A038",
                            "SC_F07_G__C34_A038",
                            "SC_F10_G__C84_A038",
                            "SC_F11_G__C83_A038",
                            "SC_F12_G__C82_A038",
                            "SC_C03_G__C13_A038",
                            "SC_G06_G__C85_A038",
                            "SC_G08_G__C41_A038",
                            "SC_G10_G__C90_A038",
                            "SC_G11_G__C89_A038",
                            "SC_H02_G__C44_A038",
                            "SC_H05_G__C92_A038",
                            "SC_H06_G__C91_A038",
                            "SC_H10_G__C96_A038",
                            "SC_H11_G__C95_A038",
                            "TC_200_N_A038",
                            "TC_200_P_A038",
                            "TC_200_U_12_A038",
                            "TC_200_U_22_A038"]
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Paul9_TC_200_1of2='TC_200_U_12_A038',
                                 Paul9_TC_200_2of2='TC_200_U_22_A038',
                                 Paul9_TC_RNeasy_1of2='TC_RN_U_12_A038',
                                 Paul9_TC_RNeasy_2of2='TC_RN_U_22_A038',
                                 Paul9_SMARTer_NTC='NTC_SMRT_U_A038')

        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")


        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-ctrls-reseq"

        # skip final file names
        # self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A038",
        #                     "20160201_CLONTECH_A03_SG_C01_A038",
        #                     "20160201_SS4_TC_NTC_A038",
        #                     "20160201_SS4_A01_MG_C03_A038",
        #                     "SC_E11_G__C77_A038"]
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(TCRNEASY_1OF2='TC_RN_U_12_A038',
                                 TCRNEASY_2OF2='TC_RN_U_22_A038',
                                 TCRNEASY_EGFRN_1OF2='TC_RN_N_12_A038',
                                 TCRNEASY_EGFRN_2OF2='TC_RN_N_22_A038',
                                 TCRNEASY_EGFRP_1OF2='TC_RN_P_12_A038',
                                 TCRNEASY_EGFRP_2OF2='TC_RN_P_22_A038'
                                 )

        self.fastq_gqfq_deploy("", "_A038", "", "")
        # resequencing for higher depth
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x053-1735-e09/fastq-x038"

        # skip final file names
        self.smp_skp_arr = ["20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_CLONTECH_A03_SG_C01_A038",
                            "20160201_SS4_TC_NTC_A038",
                            "20160201_SS4_A01_MG_C03_A038",
                            "NTC_SMRT_S_A038",
                            "NTC_SMRT_U_A038",
                            "SC_A05_G__C50_A038",
                            "SC_A07_G__C06_A038",
                            "SC_A08_G__C05_A038",
                            "SC_A09_G__C04_A038",
                            "SC_A09_G__C04_A038",
                            "SC_B10_G__C58_A038",
                            "SC_B12_G__C60_A038",
                            "SC_A10_G__C52_A038",
                            "SC_C04_G__C61_A038",
                            "SC_C07_G__C18_A038",
                            "SC_C09_G__C16_A038",
                            "SC_C10_G__C64_A038",
                            "SC_C12_G__C66_A038",
                            "SC_D03_G__C19_A038",
                            "SC_E03_G__C27_A038",
                            "SC_E05_G__C74_A038",
                            "SC_E10_G__C78_A038",
                            "SC_E11_G__C77_A038",
                            "SC_F04_G__C81_A038",
                            "SC_F05_G__C80_A038",
                            "SC_F07_G__C34_A038",
                            "SC_F10_G__C84_A038",
                            "SC_F11_G__C83_A038",
                            "SC_F12_G__C82_A038",
                            "SC_C03_G__C13_A038",
                            "SC_G06_G__C85_A038",
                            "SC_G08_G__C41_A038",
                            "SC_G10_G__C90_A038",
                            "SC_G11_G__C89_A038",
                            "SC_H02_G__C44_A038",
                            "SC_H05_G__C92_A038",
                            "SC_H06_G__C91_A038",
                            "SC_H10_G__C96_A038",
                            "SC_H11_G__C95_A038",
                            "TC_200_N_A038",
                            "TC_200_P_A038",
                            "TC_200_U_12_A038",
                            "TC_200_U_22_A038",
                            ]
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Paul9_TC_200_1of2='TC_200_U_12_A038',
                                 Paul9_TC_200_2of2='TC_200_U_22_A038',
                                 Paul9_TC_RNeasy_1of2='TC_RN_U_12_A038',
                                 Paul9_TC_RNeasy_2of2='TC_RN_U_22_A038',
                                 Paul9_SMARTer_NTC='NTC_SMRT_U_A038')

        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")

        # egfr controls
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x054-1735-e10/fastq_egfr"
        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = dict(Clontech_EGFRp_TC_200='TC_200_P_A038',
                                 Clontech_EGFRn_TC_200='TC_200_N_A038',
                                 Clontech_EGFRp_TC_RNEASY_1OF2='TC_RN_P_12_A038',
                                 Clontech_EGFRp_TC_RNEASY_2OF2='TC_RN_P_22_A038',
                                 Clontech_EGFRn_TC_RNEASY_1OF2='TC_RN_N_12_A038',
                                 Clontech_EGFRn_TC_RNEASY_2OF2='TC_RN_N_22_A038',
                                 Clontech_EGFR_TC_NTC='NTC_SMRT_S_A038')


        self.fastq_gqfq_deploy("", "_A038", "Paul9_", "")

    # Paul8 old - updated pipeline
    def deploy_a039(self):

        self.depl_dir = "/gs/project/wst-164-ab/share/rnvar/a039-x052-1735-e08/raw_reads"
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-ab/share/data/x052-1735-e08/fastq_2503_7_8"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {"Paul8_SMARTer_NTC": 'E08_TC_NTC_SMRT-a031',
                             "Tag_NTC": 'E08_TC_NTC_Tag-a031',
                             "Paul8_TC_200_1of2": 'E08_TC_200_UNS_12-a031',
                             "Paul8_TC_200_2of2": 'E08_TC_200_UNS_22-a031',
                             "Paul8_TC_RNeasy_1of2": 'E08_TC_RN_UNS_12-a031'
                             }
        self.smp_skp_arr = []
        self.fastq_gq00_deploy("E08_", "-a031", "Paul8_", "")

    # Paul9 mouse specific snvs
    def deploy_a040(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a040-x067-mm-scs/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)

        #
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x067-mm-scs/fastq"

        # skip final file names

        self.fastq_gqfq_deploy("", "_A040", "", "")


    # CTC post-treatment
    def deploy_a041(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a041-x063-e02/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        # fastq_2277_1_2
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x063-ctc-8XPE100_P0466204-post-treat-10/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {
            "Tag_NTC-10": "NTC_TAG_U_A041",
            "SMARTer_NTC-10": 'NTC_SMRT_U_A041',
            "CD45nCD8p_TC_RNeasy_1of2-10": "TC_RN_NP_12_A041",
            "CD45nCD8p_TC_RNeasy_2of2-10": "TC_RN_NP_22_A041",
            "CD45pCD8n_RNeasy-10" : "TC_RN_PN_A041",
            "All_Before_Stain_RNeasy-10" : "ALL_BEF_STA_U_A041",
            "All_After_Stain_RNeasy-10": "ALL_AFT_STA_U_A041",
            "CD45nCD8n_RNeasy-10" : "TC_RN_NN_A041",
            "CD45pCD8p_RNeasy-10" : "TC_RN_PP_A041",
            "CD45nCD8p_TC_200_1of2-10": "TC_200_NP_12_A041",
            "CD45nCD8p_TC_200_2of2-10": "TC_200_NP_22_A041",
            }

        # this is applied on final data, so final names should be there (uppercase)
        # self.smp_skp_arr = ["ALL_BEFORE_STAIN_11_NP_A032",
        #     "CD45NCD8N_TC_200_1OF2_11_NP_A032", "CD45NCD8N_TC_200_2OF2_11_NP_A032",
        #                     "TC_200_1OF2_NP_A032", "TC_200_2OF2_NP_A032",
        #                     "SC_A01_G_C03_NP_A032", "SC_B01_G_C09_NP_A032", "SC_B02_G_C08_NP_A032",
        #                     "SC_B04_G_C55_NP_A032", "SC_C01_G_C15_NP_A032", "SC_C03_G_C13_NP_A032",
        #                     "SC_C08_G_C17_NP_A032", "SC_D01_G_C21_NP_A032", "SC_D02_G_C20_NP_A032",
        #                     "SC_D04_G_C67_NP_A032", "SC_D07_GTINY_C24_NP_A032",
        #                     "SC_D10_G_C22_NP_A032", "SC_E01_EMPTY_C25_NP_A032",
        #                     "SC_E03_G_C27_NP_A032", "SC_E04_G_C75_NP_A032", "SC_E08_G_C29_NP_A032",
        #                     "SC_E10_G_C30_NP_A032", "SC_F01_G_C31_NP_A032", "SC_F02_G_C32_NP_A032",
        #                     "SC_F04_G_C81_NP_A032", "SC_F08_G_C35_NP_A032", "SC_G02_G_C38_NP_A032",
        #                     "SC_G12_G_C88_NP_A032", "SC_H01_G_C43_NP_A032", "SC_H07_G_C46_NP_A032",
        #                     "SC_H09_G_C96_NP_A032",
        #                     ]

        self.fastq_gq00_deploy("", "_NP_A041", "CD45nCD8p_", "-10")

    # CTC post-treatment
    def deploy_a041_redo(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a041-x063-e02-redo/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=False, onerror=None)
        # as for deploy_a004()
        # fastq_2277_1_2
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x063-ctc-8XPE100_P0466204-post-treat-10/fastq"

        # without new suffix/prefix - like old (original)
        self.smp_ren_dict = {
            "Tag_NTC-10": "NTC_TAG_U_A041",
            "SMARTer_NTC-10": 'NTC_SMRT_U_A041',
            "CD45nCD8p_TC_RNeasy_1of2-10": "TC_RN_NP_12_A041",
            "CD45nCD8p_TC_RNeasy_2of2-10": "TC_RN_NP_22_A041",
            "CD45pCD8n_RNeasy-10" : "TC_RN_PN_A041",
            "All_Before_Stain_RNeasy-10" : "ALL_BEF_STA_U_A041",
            "All_After_Stain_RNeasy-10": "ALL_AFT_STA_U_A041",
            "CD45nCD8n_RNeasy-10" : "TC_RN_NN_A041",
            "CD45pCD8p_RNeasy-10" : "TC_RN_PP_A041",
            "CD45nCD8p_TC_200_1of2-10": "TC_200_NP_12_A041",
            "CD45nCD8p_TC_200_2of2-10": "TC_200_NP_22_A041",
            }

        self.fastq_gq00_deploy("", "_NP_A041", "CD45nCD8p_", "-10")


    # mouse jiarong2
    def deploy_a042(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a042-x069-e01/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        # as for deploy_a004()
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x069-mm-jiarong2/fastq"
        self.smp_ren_dict = {}
        self.fastq_gq00_deploy("", "_A042", "", "")

    # ruy
    def deploy_a043(self):

        self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a043-x071-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/badescud/projects/rrg-ioannisr/share/data/x071-ruy-mm-p1/fastq_ren"
        self.smp_ren_dict = {}
        self.fastq_gqfq_deploy("", "_A043", "", "")

    # hani
    def deploy_a044(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a044-x072-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x072-hani-obe-e01/fastq"
        self.smp_ren_dict = {}
        self.fastq_hnfq1_deploy("", "_A044", "", "")

    # claire
    def deploy_a045(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/a045-x073-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x073-claire-e01/fastq"
        self.smp_ren_dict = {}
        self.fastq_clfq1_deploy("", "_A045", "", "")

    def deploy_t001(self):


        self.depl_dir = "/gs/project/wst-164-aa/share/rnvar/t001-y001/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)

        self.fastq_dir_name = "/gs/project/wst-164-aa/share/rnvar/t001-y001/fastq_ren"

        self.fastq_rnfq1_deploy("HI", "R1", "", "_T001", "", "")

    # axel vmp c1
    def deploy_a050(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/expr/a050-x077/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x077-axel-vmp-c1-e2/fastq"
        self.smp_ren_dict = {}
        self.fastq_gqfq_deploy("", "_A050", "", "")

    # axel vp c1
    def deploy_a051(self):

        self.depl_dir = "/gs/project/wst-164-aa/share/expr/a051-x078-axel-vp-c1-e2/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/gs/project/wst-164-aa/share/data/x078-axel-vp-c1-e2/fastq"
        self.smp_ren_dict = {}
        self.fastq_gqfq_deploy("", "_A051", "", "")

    def deploy_a083(self):

        self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a083-x083-oocytes-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/badescud/projects/rrg-ioannisr/share/data/x083-oocytes-e1/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv2fq_deploy("", "_A083", "", "")

    def deploy_a084(self):

        # self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a084-x084-oocytes-e1/raw_reads"
        self.depl_dir = "/home/dbadescu/share/rnvar/a084-x084-oograd-e2/raw_reads"

        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        # self.fastq_dir_name = "/home/badescud/scratch/data/x084-oocytes-e2/fastq1"
        self.fastq_dir_name = "/home/dbadescu/share/data/x084-oocytes-e1/fastq2"

        self.smp_ren_dict = {}
        # self.fastq_gq2fq_deploy("", "_A084", "", "")
        self.fastq_nv4fq_deploy("", "_A084", "", "")

    def deploy_a085(self):

        self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a085-x085-oocytes-e2/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/badescud/projects/rrg-ioannisr/share/data/x085-oocytes-e2/fastq2"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        self.fastq_gq00_deploy("", "_A085", "", "")

    def deploy_a086(self):

        self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a086-x086-corina-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/badescud/projects/rrg-ioannisr/share/data/x086-corina-nucl-e1/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        self.fastq_gqfq_deploy("", "_A086", "", "")


    def deploy_a087(self):

        self.depl_dir = "/home/badescud/projects/rrg-ioannisr/share/rnvar/a087-x087-oocytes-e3/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/badescud/projects/rrg-ioannisr/share/data/x087-oocytes-e3/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        self.fastq_gq00_deploy("", "_A087", "", "")

    def deploy_a088(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a088-x088-oocytes-e4/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x088-oocytes-e4/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nvfq_deploy("", "_A088", "", "")


    def deploy_a090(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a090-x090-rui-e5/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/lb/scratch/dbadescu/data/x090-rui-e5/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv2fq_deploy("", "_A090", "", "")

    def deploy_a092(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a092-x091-flex-e2/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x091-flex-e1/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv3fq_deploy("", "_A092", "", "")

    def deploy_a093(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a093-x092-oocytes-e5/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x092-oocytes-e5/fastq-A00266_0272_2"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv5fq_deploy("", "_A093", "", "")

    def deploy_a094(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a094-x093-tak-e1/raw_afl"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x093-tak-e1/fq-A00266_0272_1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv5fq_deploy("", "_A094", "", "")

    def deploy_a094_raw(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a094-x093-tak-e1-raw/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)

        self.fastq_dir_name = "/lb/project/ioannisr/NOBACKUP/dbadescu/rnvar/a094-x093-tak-e1-afl/align2"
        self.smp_ren_dict = {}

        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.raw_afl_deploy("", "", "", "")

    # rima e7
    def deploy_a095(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a095-x094-rima-e7/raw_afl"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)

        self.fastq_dir_name = "/home/dbadescu/share/data/x094-rima-e7/fastqs"
        self.smp_ren_dict = {}
        self.fastq_nv8fq_deploy("", "_A095", "", "")

        # self.fastq_dir_name = "/home/dbadescu/share/data/x094-rima-e7/fastqs-lane1"
        # self.smp_ren_dict = {}
        # self.fastq_nv7fq_deploy("", "_A095", "", "")
        #
        # self.fastq_dir_name = "/home/dbadescu/share/data/x094-rima-e7/fastqs-lane2"
        # self.smp_ren_dict = {}
        # self.fastq_nv7fq_deploy("", "_A095", "", "")

    def deploy_a097(self):
        self.depl_dir = "/home/dbadescu/share/rnvar/a097-x090-rui-e6/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x090-rui-e6/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq00
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv2fq_deploy("", "_A097", "", "")

    # Myriam
    def deploy_a098(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a098-x098-myriam-e1/raw_reads"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x098-myriam-e1/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq3fq
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_gq3fq_deploy("", "_A098", "", "")

    # Rima e08/Novaseq S4
    def deploy_a099(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a099-x099-rima-e8/raw_afl"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x099-rima-e8/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq3fq
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv9fq_deploy("", "_A099", "", "")

    # Keerthana e11/Novaseq S4
    def deploy_a101(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a101-x101-yamanaka-e11/raw_afl"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x101-yamanaka-e11/concaten"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq3fq
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv10fq_deploy("", "", "", "")

    # Keerthana e12/Novaseq S4
    def deploy_a104(self):

        self.depl_dir = "/home/dbadescu/share/rnvar/a104-x104-yamanaka-e12/raw_afl"
        shutil.rmtree(self.depl_dir, ignore_errors=True, onerror=None)
        self.fastq_dir_name = "/home/dbadescu/share/data/x104-yamanaka-e12/fastq1"
        self.smp_ren_dict = {}
        # there is no more run before runlane in gq3fq
        # check also naseq.py/spln_runreg for "run"
        # this is generating no "run"
        self.fastq_nv10fq_deploy("", "", "", "")




    def reference_dict(self):

        # dictionnary
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CreateSequenceDictionary \
REFERENCE=%(ref_genome_fa)s \
OUTPUT=%(ref_dict)s \
GENOME_ASSEMBLY=%(ref_assembly)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "ref_genome_fa": self.conf.ref_gen_genome_fa,
       "ref_dict": self.conf.ref_gen_genome_dict,
       "ref_assembly": self.conf.ref_gen_assembly}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_genome_fa_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def ref_gen_orig_genome_faindex(self):
        # fasta index
        cmd = r"""samtools faidx %(ref_gen_orig_genome_fa)s
""" % {"ref_gen_orig_genome_fa": self.conf.ref_gen_orig_genome_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_orig_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def reference_faindex(self):
        # fasta index
        cmd = r"""samtools faidx %(ref_genome_fa)s
""" % {"ref_genome_fa": self.conf.ref_gen_genome_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_genome_fa_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def reference_chrominfo(self):
        # fasta index
        cmd = r"""cut -f1,2 %(ref_gen_genome_fai)s >  %(ref_gen_genome_chrominfo)s
""" % {"ref_gen_genome_fai": self.conf.ref_gen_genome_fai,
       "ref_gen_genome_chrominfo": self.conf.ref_gen_genome_chrominfo}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_genome_fa_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def star_idx(self):
        # fasta index
        cmd = r"""cut -f1,2 %(ref_genome_fai)s >  %(ref_gen_chrominfo)s
""" % {"ref_genome_fai": self.conf.ref_gen_fai, "ref_gen_chrominfo": self.conf.ref_gen_chrominfo}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_assembly_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def star_align(self):
        cmd = r"""STAR --runMode alignReads --runThreadN 12 --genomeDir \
    ../sequence/star_ix --genomeLoad LoadAndKeep \
    --readFilesCommand zcat \
    --readFilesIn /gs/project/wst-164-aa/expr/ex1-gq13-v.0.9/reads/CD45nCD8p_TC_200_1of2-10/run2277_1/CD45nCD8p_TC_200_1of2-10.MPS084738.t30l45.pair1.fastq.gz \
                  /gs/project/wst-164-aa/expr/ex1-gq13-v.0.9/reads/CD45nCD8p_TC_200_1of2-10/run2277_1/CD45nCD8p_TC_200_1of2-10.MPS084738.t30l45.pair2.fastq.gz \
    --outSAMtype BAM SortedByCoordinate \
    --limitBAMsortRAM 10000000000 --outSAMunmapped Within
"""



    def pyfasta_split_orig_genome(self):

        core = '%(seqid)s.fasta'
        # fasta index
        cmd = r"""pyfasta split --header="%s/%s" %s""" % (self.conf.ref_gen_chrom_dir, core, self.conf.ref_gen_orig_genome_fa)
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_orig_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)




    def chroms_to_genome_fa(self):

        #load_orig_contigs
        orctg = pd.read_csv(self.conf.ref_gen_orig_contigs_csv,sep=','
                            , header = 0
                          #,index_col=[0]
                          ,usecols=[0, 1]
                          ,names=["idx", "contig"]
                          )
        nb_contigs = orctg.shape[0]

        print("nb_contigs: ", nb_contigs)

        for i in [self.conf.ref_gen_genome_fa]:
            if os.path.exists(i):
                os.remove(i)

        # fasta start
        cmd = r"""touch %(ref_genome_fa)s""" % {"ref_genome_fa": self.conf.ref_gen_genome_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_genome_fa_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

        for idx in range(nb_contigs):
            ctg_name = orctg["contig"][idx]


            ctg_fa = os.path.join(self.conf.ref_gen_chrom_dir, r"""%s.fasta""" % ctg_name)

            print("idx: ", idx, "contig_name: ", ctg_name, " ctg_fa: ", ctg_fa)

            #add to fasta
            cmd = r"""cat %(ctg_fa)s >> %(ref_genome_fa)s""" % {"ctg_fa": ctg_fa, "ref_genome_fa": self.conf.ref_gen_genome_fa}
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_genome_fa_dir, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)


    def bwa_index(self):

        for i in [self.conf.ref_gen_bwa_ix_genome_fa]:
            if os.path.exists(i):
                os.unlink(i)

        os.symlink(self.conf.ref_gen_genome_fa, self.conf.ref_gen_bwa_ix_genome_fa)



        cmd = r"""bwa index %(ref_gen_bwa_ix_genome_fa)s""" % {"ref_gen_bwa_ix_genome_fa": self.conf.ref_gen_bwa_ix_genome_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_bwa_ix_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def bt2_index(self):

        for i in [self.conf.ref_gen_bt2_ix_genome_fa]:
            if os.path.exists(i):
                os.unlink(i)

        os.symlink(self.conf.ref_gen_genome_fa, self.conf.ref_gen_bt2_ix_genome_fa)

        cmd = r"""bowtie2-build %(ref_gen_bt2_ix_genome_fa)s %(ref_assembly)s
""" % {"ref_assembly": self.conf.ref_gen_assembly, "ref_gen_bt2_ix_genome_fa": self.conf.ref_gen_bt2_ix_genome_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_bt2_ix_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def gffread_transcripts(self):

        for i in [self.conf.ref_transcr_fa]:
            if os.path.exists(i):
                os.remove(i)

        cmd = r"""gffread -w %(ref_transcr_fa)s -g %(ref_genome_fa)s %(ref_transcr_gtf)s
""" % {"ref_genome_fa": self.conf.ref_gen_genome_fa,
       "ref_transcr_gtf": self.conf.ref_gen_transcr_gtf,
       "ref_transcr_fa": self.conf.ref_transcr_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_bt2_ix_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def kallisto_index(self):

        for i in [self.conf.ref_transcr_kallisto_kix]:
            if os.path.exists(i):
                os.delete(i)

        cmd = r""" kallisto index -i %(kallisto_kix)s %(ref_transcr_fa)s
""" % {"kallisto_kix": self.conf.ref_transcr_kallisto_kix,
       "ref_transcr_fa": self.conf.ref_transcr_fa}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def bt2_transcr_link(self):

        for i in [self.conf.ref_gen_transcr_gtf]:
            if os.path.exists(i):
                os.unlink(i)

        os.symlink(self.conf.ref_orig_transcr_gtf, self.conf.ref_gen_transcr_gtf)

    def transcr_to_nz_bed(self):

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
gtf_to_gff.py %(gtf)s > %(gff3)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "gtf": self.conf.ref_orig_transcr_gtf,
       "gff3": self.conf.ref_transcr_gff3
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
gff_to_bed.py %(gff3)s > %(bed)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "gff3": self.conf.ref_transcr_gff3,
       "bed": self.conf.ref_transcr_bed12
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""awk -F "\t" '$12 != "0," { print $0 }' %(bed)s > %(nz_bed)s
""" % {"bed": self.conf.ref_transcr_bed12,
       "nz_bed": self.conf.ref_transcr_nz_bed12
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

    def transcr_to_target_bed(self):

        cmd = r"""cat %(gtf)s | awk 'BEGIN {OFS="\t"}; {print $1,$4,$5}' |sort -k1,1 -k2,2n  > %(nz_bed3)s
""" % {"gtf": self.conf.ref_orig_transcr_gtf,
       "nz_bed3": self.conf.ref_transcr_bed3
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        cmd = r"""bedtools merge -i %(nz_bed3)s > %(mer_bed3)s
""" % {"nz_bed3": self.conf.ref_transcr_bed3,
       "mer_bed3": self.conf.ref_transcr_mer_bed3
       }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)

        #



    def gtf_to_h5(self):

        #  do not forget to cut the header:
        # cat ucMm10sp3p92_geM9.gtf | egrep -v "(^#.*|^$)" > ucMm10sp3p92_geM9.gtf
        # cat ucMm10sp3p92_geM9.gtf | egrep  "(^#.*|^$)" >ucMm10sp3p92_geM9.gtf.header

        pd.set_option('io.hdf.default_format', 'table')

        store = pd.HDFStore(self.conf.ref_transcr_h5)

        print(store)

        # Input files.
        GENCODE      = self.conf.ref_gen_transcr_gtf
        # NCBI_ENSEMBL = args['-n']

        # Output file prefix.
        # GENE_LENGTHS = "ncbi_ensembl_coding_lengths.txt.gz"

        with log("Reading the Gencode annotation file: {}".format(GENCODE)):
            gc = GTF_ENCODE.dataframe(GENCODE)

        print("gc: \n ", gc)


        # Select just exons of protein coding genes, and columns that we want to use.
        # idx = (gc.feature == 'exon') & (gc.transcript_type == 'protein_coding')
        # idx = (gc.feature == 'exon')
        # exon = gc.ix[idx, ['seqname', 'transcript_type', 'feature', 'start', 'end', 'score', 'strand', 'frame',
        #                    'gene_biotype', 'gene_id', 'gene_name', 'gene_source',
        #                    'transcript_id', 'transcript_name', 'transcript_source', 'exon_id', 'exon_number']]

        # Convert columns to proper types.
        gc.start = gc.start.astype(int)
        gc.end = gc.end.astype(int)

        # Sort in place.
        gc.sort(['seqname', 'start', 'end'], inplace=True)

        # print("exon: \n ", exon)
        #
        # store['exon'] = exon

        # store.put('all/all', gc, format='table', data_columns=True)
        store.put('all/compr', gc, format='table', complib='blosc', chunksize=2000000,
                  data_columns=['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame',
                                'gene_id', 'gene_name', 'gene_source', 'gene_biotype',
                                'ccds_id', 'transcript_id', 'transcript_name', 'transcript_source' ,'tss_id',
                                'exon_id', 'exon_number',
                                'p_id', 'protein_id', 'tag'])
        print(store)

        store.close()
        # gc.collect()

    def chrnames_parse_mm10(self):

        fldr = "/gs/project/wst-164-ab/share/raglab_prod/igenomes/ucMm10sp3p92/pre-gtf"

        uc_fai_df = pd.read_csv(os.path.join(fldr,"ucMm10sp3p92.chrominfo.csv"),
                                sep='\t', header=None
                                # , index_col=[0]
                                , usecols=[0, 1],
                  names=['seqname', 'size'])

        print("uc_fai_df: \n", uc_fai_df)


        seqcore = []
        seqcol = uc_fai_df['seqname'].values

        print("seqcol: ", seqcol)

        un_chrom = None
        un_chrom_version = None
        un_rndm = None

        short_chrom = None

        for i in seqcol:
            m = re.match("^chr([^_]+)_?([^_]+)?_?(random)?$", i)
            if m:
                chrom = m.group(1)
                un_chrom = m.group(2)
                un_rndm = m.group(3)
            else:
                un_chrom = None
                short_chrom = chrom

            # if short_chrom == "M":
            #     short_chrom = "MT"

            # seqcore.append(short_chrom)
            print(i, "--------", chrom, un_chrom, un_rndm, short_chrom)

            #     body = m.group(2)
            #     old_suff = m.group(3)
            #     print("old_pref, body, old_suff: >%s<, >%s<, >%s<", old_pref, body, old_suff)
                # transformation

            # else:
            #     body = self.dat_smp

        uc_fai_df["seqcore"]=seqcore



        #
        rn_fai_df = pd.read_csv(os.path.join(fldr,"RNOR6.fai.csv"),
                        sep='\t', header=None
                        # , index_col=[0]
                        , usecols=[0, 1],
          names=['seqname', 'size'])



        uc_fai_df.set_index(['seqcore'], inplace=True, drop=True)
        rn_fai_df.set_index(['seqname'], inplace=True, drop=True)

        print("uc_fai_df: \n", uc_fai_df)
        print("rn_fai_df: \n", rn_fai_df)

        merg_df = pd.merge(uc_fai_df, rn_fai_df, how='inner', left_index=True, right_index=True)
        # merg_df['eq_size']=merg_df['size_x']==merg_df['size_y']

        # print("merg_df 1: \n", merg_df)


        # verify size
        merg_df = merg_df[merg_df['size_x']==merg_df['size_y']]

        # uniquify
        # http://stackoverflow.com/questions/13035764/remove-rows-with-duplicate-indices-pandas-dataframe-and-timeseries
        # index is already in place (seqcore)
        merg_df = merg_df[~merg_df.index.duplicated(keep='last')]
        merg_df.drop('size_y', axis=1, inplace=True)
        merg_df.rename(columns={'seqname': 'uc_chr', 'size_x': 'size'}, inplace=True)

        # rename index
        # http://stackoverflow.com/questions/19851005/rename-pandas-dataframe-index
        #  df1.rename(index={1: 'a'}) # rename row values 1 to 'a'
        merg_df.index.names = ['en_chr']
        # df1.columns.names = ['column']

        print("merg_df 2: \n", merg_df)

        # now reset index for saving
        # merg_df.sort(['en_chr'], inplace=True)
        merg_df.sort_index(inplace=True)
        merg_df.reset_index(inplace=True, drop=False)


        merg_df.to_csv(os.path.join(fldr,"en_uc_chrs.csv"),
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


        # for idx, row in lanes_df.iterrows():
        #     self.spln_idx=idx


        #


    def chrnames_parse_rn6(self):

        fldr = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL"

        uc_fai_df = pd.read_csv(os.path.join(fldr,"rn6.fai.csv"),
                                sep='\t', header=None
                                # , index_col=[0]
                                , usecols=[0, 1],
                  names=['seqname', 'size'])

        print("uc_fai_df: \n", uc_fai_df)


        seqcore = []
        seqcol = uc_fai_df['seqname'].values

        # print("seqcol: ", seqcol)
        un_chrom = None
        un_chrom_version = None
        un_rndm = None

        short_chrom = None

        for i in seqcol:
            m = re.match("^chr([^_]+)_?([^_]+v\d)?_?(random)?$", i)
            if m:
                chrom = m.group(1)
                un_chromv = m.group(2)
                un_rndm = m.group(3)

                if un_chromv:
                    m2 = re.match("(\w+)v(\d+)", un_chromv)
                    if m2:
                        un_chrom = m2.group(1)
                        un_chrom_version = m2.group(2)
                        short_chrom = r"""%s.%s""" % (un_chrom, un_chrom_version)
                    else:
                        un_chrom = None
                        un_chrom_version = None
                else:
                    un_chrom = None
                    un_chrom_version = None
                    short_chrom = chrom

            else:
                chrom = None
                un_chromv = None
                un_rndm =  None

            if short_chrom == "M":
                short_chrom = "MT"

            seqcore.append(short_chrom)
            # print(i, "--------", chrom, un_chrom, un_chrom_version, un_rndm, short_chrom)

            #     body = m.group(2)
            #     old_suff = m.group(3)
            #     print("old_pref, body, old_suff: >%s<, >%s<, >%s<", old_pref, body, old_suff)
                # transformation

            # else:
            #     body = self.dat_smp

        uc_fai_df["seqcore"]=seqcore



        #
        rn_fai_df = pd.read_csv(os.path.join(fldr,"RNOR6.fai.csv"),
                        sep='\t', header=None
                        # , index_col=[0]
                        , usecols=[0, 1],
          names=['seqname', 'size'])



        uc_fai_df.set_index(['seqcore'], inplace=True, drop=True)
        rn_fai_df.set_index(['seqname'], inplace=True, drop=True)

        print("uc_fai_df: \n", uc_fai_df)
        print("rn_fai_df: \n", rn_fai_df)

        merg_df = pd.merge(uc_fai_df, rn_fai_df, how='inner', left_index=True, right_index=True)
        # merg_df['eq_size']=merg_df['size_x']==merg_df['size_y']

        # print("merg_df 1: \n", merg_df)


        # verify size
        merg_df = merg_df[merg_df['size_x']==merg_df['size_y']]

        # uniquify
        # http://stackoverflow.com/questions/13035764/remove-rows-with-duplicate-indices-pandas-dataframe-and-timeseries
        # index is already in place (seqcore)
        merg_df = merg_df[~merg_df.index.duplicated(keep='last')]
        merg_df.drop('size_y', axis=1, inplace=True)
        merg_df.rename(columns={'seqname': 'uc_chr', 'size_x': 'size'}, inplace=True)

        # rename index
        # http://stackoverflow.com/questions/19851005/rename-pandas-dataframe-index
        #  df1.rename(index={1: 'a'}) # rename row values 1 to 'a'
        merg_df.index.names = ['en_chr']
        # df1.columns.names = ['column']

        print("merg_df 2: \n", merg_df)

        # now reset index for saving
        # merg_df.sort(['en_chr'], inplace=True)
        merg_df.sort_index(inplace=True)
        merg_df.reset_index(inplace=True, drop=False)


        merg_df.to_csv(os.path.join(fldr,"en_uc_chrs.csv"),
                       sep=',', header=True
                       , index=True
                       , index_label="idx"
                       )


        # for idx, row in lanes_df.iterrows():
        #     self.spln_idx=idx


        #

    def gtf_ren_chrnames_rn6(self):

        fldr = "/gs/project/wst-164-ab/data/ucRn6sp3p92/EMBL"

        en_uc_chrs_df = pd.read_csv(os.path.join(fldr,"en_uc_chrs.csv"),
                                sep=',', header=0
                                , index_col=[1]
                                # , usecols=[1, 2]
                                    # , names=['en_chr', 'uc_chr']
                                    )

        print("en_uc_chrs_df: \n", en_uc_chrs_df)

        ln_nb=0

        fout = open(os.path.join(fldr,"ucRn6sp3p92-em-v83.gtf"), "w")

        # with open(os.path.join(fldr,"test.gtf")) as fh:
        with open(os.path.join(fldr, "enRn6sp3p92-em-v83.gtf")) as fh:
            for line in fh:

                m = re.match("^(\S+)\s+(.+)$", line)
                if line.startswith('#'):
                    fout.write("%s\n" % line)
                else:
                    if m:
                        enchr = m.group(1)
                        # ucchr = en_uc_chrs_df.query('grp == @grp_id')
                        ucchr = en_uc_chrs_df.loc[enchr, 'uc_chr']
                        fout.write("%s\t%s\n" % (ucchr, m.group(2)))
                    else:
                        fout.write("# %s \n" % line)

        fout.close()


    def gtf_ren_chrnames_mm10(self):

        fldr = "/gs/project/wst-164-ab/share/raglab_prod/igenomes/ucMm10sp3p92/pre-gtf"

        en_uc_chrs_df = pd.read_csv(os.path.join(fldr, "en_uc_chrs.csv"),
                                sep=',', header=0
                                , index_col=[1]
                                # , usecols=[1, 2]
                                    # , names=['en_chr', 'uc_chr']
                                    )

        print("en_uc_chrs_df: \n", en_uc_chrs_df)

        ln_nb=0

        fout = open(os.path.join(fldr, "ucMm10sp3p92_M9.gtf"), "w")

        # with open(os.path.join(fldr,"test.gtf")) as fh:
        with open(os.path.join(fldr, "enMm10sp3p92_M9.gtf")) as fh:
            for line in fh:

                m = re.match("^(\S+)\s+(.+)$", line)
                if line.startswith('#'):
                    fout.write("%s\n" % line)
                else:
                    if m:
                        enchr = m.group(1)
                        # ucchr = en_uc_chrs_df.query('grp == @grp_id')
                        ucchr = en_uc_chrs_df.loc[enchr, 'uc_chr']
                        fout.write("%s\t%s\n" % (ucchr, m.group(2)))
                    else:
                        fout.write("# %s \n" % line)

        fout.close()





    def h5_to_tracking_ids(self):

        store = pd.HDFStore(self.conf.ref_transcr_h5)

        print(store)
        # sel = store.select('all/compr', where="gene_name == %r & feature == exon" % 'NEAT1',
        #                    columns=['gene_id', 'transcript_id', 'exon_id', 'start', 'end',
        #                             'source', 'gene_name', 'transcript_name', 'exon_number', 'strand'])


        # all transcripts
        sel = store.select('all/compr', columns=['transcript_id', 'transcript_name', 'gene_id', 'gene_name', 'source'])
        sel.set_index(['transcript_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.isofs_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )

        # all genes
        sel = store.select('all/compr', columns=['gene_id', 'gene_name', 'source'])
        sel.set_index(['gene_id'], drop=True, inplace=True)
        sel.sort_index(inplace=True)
        sel = sel.groupby(sel.index).first()

        print("sel: ", sel)
        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        sel.sort_index(inplace=True)

        sel.reset_index(inplace=True, drop=False)
        # sort for output

        sel.to_csv(self.conf.genes_tracking_ids_csv, sep=","
                   ,header=True
                   ,index=True
                   ,index_label="idx"
                   )
        store.close()
        gc.collect()


    def query_h5(self):

        store = pd.HDFStore(self.conf.ref_transcr_h5)

        print(store)
        sel = store.select('all/compr', where="gene_id == %r & feature == exon" % 'ENSG00000182774',
                           columns=['seqname', 'gene_id', 'transcript_id', 'exon_id', 'start', 'end',
                                    'source', 'gene_name', 'transcript_name', 'exon_number', 'strand'])

        # sel = store.select('all/compr', columns=['transcript_id', 'transcript_name', 'gene_id', 'gene_name', 'source'])

        # sel.set_index(['transcript_id'], drop=True, inplace=True)
        # sel.sort_index(inplace=True)
        # sel = sel.groupby(sel.index).first()

        print("sel: \n", sel)

        # sel.drop_duplicates(inplace=True)
        # print("sel: ", sel.shape)
        # sel.sort_index(inplace=True)

        # sel.reset_index(inplace=True, drop=False)
        # sort for output

        # sel.to_csv(self.conf.isofs_tracking_ids_csv, sep=","
        #                    ,header=True
        #                    ,index=True
        #                    ,index_label="idx"
        #                    )
        # exit(0)

        exit(0)





        g = sel.sort('transcript_id', ascending=True).groupby(['transcriptidx_seq', 'r1_chr', 'r1_s', 'r2_e'], as_index=False)
        print("g: ", g)
        gb = g.agg({'rname': lambda x: x.iloc[0], 'score': 'count'})

        gb.rename(columns={'score': 'cnt'}, inplace=True)




        self.spln_hplex_qc['min'] = "%.2f" % sel2['cnt'].min()
        self.spln_hplex_qc['max'] = "%.2f" % sel2['cnt'].max()
        self.spln_hplex_qc['median'] = "%.2f" % sel2['cnt'].quantile(q=0.50)
        self.spln_hplex_qc['q25'] = "%.2f" % sel2['cnt'].quantile(q=0.25)
        self.spln_hplex_qc['q75'] = "%.2f" % sel2['cnt'].quantile(q=0.75)
        self.spln_hplex_qc['mad'] = "%.2f" % sel2['cnt'].mad()
        self.spln_hplex_qc['mean'] = "%.2f" % sel2['cnt'].mean()
        self.spln_hplex_qc['std'] = "%.2f" % sel2['cnt'].std()

        # print("quartiles: ", a, b, c, d, e, f, g, h)






        print("sel2.shape before uniquify index: \n", sel2.shape)
        # uniquify index

        print("sel2 after groupby: \n", sel2.shape, sel2.dtypes)

        print("sel2.shape before drop: \n", sel2.shape)
        # sel2.drop_duplicates(inplace=True)
        print("sel2.shape after drop: \n", sel2.shape)








    def bt2_transcr_index(self):

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
tophat -G %(ref_gen_transcr_gtf)s \
--transcriptome-index=%(ref_gen_transcr_ix)s %(ref_gen_bt2_ix)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "ref_gen_transcr_gtf": self.conf.ref_gen_transcr_gtf,
       "ref_gen_transcr_ix": self.conf.ref_gen_transcr_ix,
       "ref_gen_bt2_ix": self.conf.ref_gen_bt2_ix}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_bt2_ix_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def star_index(self):

        cmd = r"""STAR \
--runThreadN 12 \
--runMode genomeGenerate \
--genomeDir %(star_ix_dir)s \
--genomeFastaFiles %(ref_gen_genome_fa)s \
--sjdbGTFfile %(ref_gen_orig_transcr_gtf)s \
--sjdbOverhang 149 \
--genomeSAsparseD 2
""" % {"star_ix_dir": self.conf.ref_transcr_star_ix_dir,
       "ref_gen_genome_fa": self.conf.ref_gen_genome_fa,
       "ref_gen_orig_transcr_gtf": self.conf.ref_orig_transcr_gtf
       }

        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_transcr_star_ix_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)


    def gen_genomic_target(self):

        # generate the transcriptome target from the nz transcript bed
        # sort -k1,1 -k2,2n  ../all_transcr/hyHg38_ge27/hyHg38_ge27.nz.bed > ../all_transcr/hyHg38_ge27/hyHg38_ge27.nz.sorted.bed
        # bedtools merge -i ../all_transcr/hyHg38_ge27/hyHg38_ge27.nz.sorted.bed > ../all_transcr/hyHg38_ge27/hyHg38_ge27.nz.mer.bed

        cmd = r"""bedtools makewindows -g %(ref_gen_genome_chrominfo)s -w 10000 > %(ref_gen_target_bed)s
""" % {"ref_gen_genome_chrominfo": self.conf.ref_gen_genome_chrominfo,
       "ref_gen_target_bed": self.conf.target_karyo_bed
       }
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.conf.ref_gen_targ_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

        # should put it in orig without the karyo suffix




# pd_names = pd.Series([], dtype="|S75")
# pd_fnames = pd.Series([], dtype="|S75")

    # s = pd.Series([sname], dtype="|S35")
    # pd_names = pd.Series(np. concatenate([pd_names,s]))
    # print("fname: ", fname)
# else:
#     print('No match')


    def raw_reads_to_all_sample_info(self, project_regex):

        # ########### output config file open and header

        ofile = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "w")
        smp_report_csv = open(os.path.join(self.conf.conf_dir, 'smp_report.csv'), "w")

        writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer_smp_report = csv.writer(smp_report_csv, delimiter=',', quotechar='"')

        ofile_header = ["Name", "Library Barcode", "Run Type", "Run", "Quality Offset", "Region", "Status", "FASTQ1", "FASTQ2", "BAM", "Library Source"]

        writer.writerow(ofile_header)
        ###############################################

        sample_names = [f for f in os.listdir(self.reads_dir) ]
        #print "files: ", files

        sample = ''
        run = ''
        lane = ''
        library = ''
        fname1 = ''
        fname2 = ''
        #defaults
        quality = "33"
        status = "Data is valid"

        for sample in sample_names:
            print(sample)
            sample_path = os.path.join(self.reads_dir, sample)
            for run_lane in os.listdir(sample_path):
                print(("run_lane: ", run_lane))
                # extract run and lane
                m = re.match("^run(\S*)_(\S*)$", run_lane)
                run = m.group(1)
                lane = m.group(2)
                print(("runlane: ", run_lane , " run: ", run, " lane: ", lane))
                run_lane_path = os.path.join(sample_path, run_lane)

                # take only first pair
                # for each library
                for fname in [f for f in os.listdir(run_lane_path) if re.match('(.+)\.pair1\.fastq\.gz$', f)]:

                    # print("fname: ", fname)
                    m2 = re.match("^(\S+)\.(\S+)\.33\.pair1\.fastq\.gz$", fname)
                    smp_name = m2.group(1)
                    library = m2.group(2)
                    # reconstruct both pairs
                    # in case we miss one we miss all
                    fname1 = r"""%s.%s.33.pair1.fastq.gz""" % (smp_name, library)
                    fname2 = r"""%s.%s.33.pair2.fastq.gz""" % (smp_name, library)

                    # now we know everything
                    row = [sample, library, "PAIRED_END", run, quality, lane, status, fname1, fname2, "", "Library"]
                    print(("row: ", row))
                    writer.writerow(row)

                    # analyse link only for pair1
                    fname_f = os.path.join(run_lane_path, fname1)

                    # do not report links if files are hardcoded
                    if os.path.islink(fname_f):
                        fname_lnk = os.path.realpath(fname_f)
                        # extract project name

                        patlnk = "^" + project_regex + "([^\/]+\/[^\/]+)\/(\S+)$"
                        print(("patlnk: ", patlnk))
                        print(("fname_lnk: ", fname_lnk))


                        motlnk = re.match(patlnk, fname_lnk)
                        src_folder = motlnk.group(2)
                        src_pair = motlnk.group(3)

                        row2 = [sample, run_lane, library, src_folder, src_pair]
                        writer_smp_report.writerow(row2)



        #
        ofile.close()
        smp_report_csv.close()

    def raw_reads_to_all_sample_info(self, project_regex):

        # ########### output config file open and header

        ofile = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "w")
        smp_report_csv = open(os.path.join(self.conf.conf_dir, 'smp_report.csv'), "w")

        writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer_smp_report = csv.writer(smp_report_csv, delimiter=',', quotechar='"')

        ofile_header = ["Name", "Library Barcode", "Run Type", "Run", "Quality Offset", "Region", "Status", "FASTQ1", "FASTQ2", "BAM", "Library Source"]

        writer.writerow(ofile_header)
        ###############################################

        sample_names = [f for f in os.listdir(self.reads_dir) ]
        #print "files: ", files

        sample = ''
        run = ''
        lane = ''
        library = ''
        fname1 = ''
        fname2 = ''
        #defaults
        quality = "33"
        status = "Data is valid"

        for sample in sample_names:
            print(sample)
            sample_path = os.path.join(self.reads_dir, sample)
            for run_lane in os.listdir(sample_path):
                print(("run_lane: ", run_lane))
                # extract run and lane
                m = re.match("^run(\S*)_(\S*)$", run_lane)
                run = m.group(1)
                lane = m.group(2)
                print(("runlane: ", run_lane , " run: ", run, " lane: ", lane))
                run_lane_path = os.path.join(sample_path, run_lane)

                # take only first pair
                # for each library
                for fname in [f for f in os.listdir(run_lane_path) if re.match('(.+)\.pair1\.fastq\.gz$', f)]:

                    # print("fname: ", fname)
                    m2 = re.match("^(\S+)\.(\S+)\.33\.pair1\.fastq\.gz$", fname)
                    smp_name = m2.group(1)
                    library = m2.group(2)
                    # reconstruct both pairs
                    # in case we miss one we miss all
                    fname1 = r"""%s.%s.33.pair1.fastq.gz""" % (smp_name, library)
                    fname2 = r"""%s.%s.33.pair2.fastq.gz""" % (smp_name, library)

                    # now we know everything
                    row = [sample, library, "PAIRED_END", run, quality, lane, status, fname1, fname2, "", "Library"]
                    print(("row: ", row))
                    writer.writerow(row)

                    # analyse link only for pair1
                    fname_f = os.path.join(run_lane_path, fname1)

                    # do not report links if files are hardcoded
                    if os.path.islink(fname_f):
                        fname_lnk = os.path.realpath(fname_f)
                        # extract project name

                        patlnk = "^" + project_regex + "([^\/]+\/[^\/]+)\/(\S+)$"
                        print(("patlnk: ", patlnk))
                        print(("fname_lnk: ", fname_lnk))


                        motlnk = re.match(patlnk, fname_lnk)
                        src_folder = motlnk.group(2)
                        src_pair = motlnk.group(3)

                        row2 = [sample, run_lane, library, src_folder, src_pair]
                        writer_smp_report.writerow(row2)



        #
        ofile.close()
        smp_report_csv.close()


    def raw_reads_to_all_sample_info_se(self, project_regex, do_report=True):

        # ########### output config file open and header

        ofile = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "w")
        smp_report_csv = open(os.path.join(self.conf.conf_dir, 'smp_report.csv'), "w")

        # writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        # no more quoting everywere
        writer = csv.writer(ofile, delimiter=',', quotechar='"')
        writer_smp_report = csv.writer(smp_report_csv, delimiter=',', quotechar='"')

        # ofile_header = ["Name", "Library Barcode", "Run Type", "Run", "Quality Offset", "Region", "Status", "FASTQ1", "FASTQ2", "BAM", "Library Source"]

        ofile_header = ["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"]

        writer.writerow(ofile_header)
        ###############################################

        sample_names = [f for f in os.listdir(self.reads_dir) ]
        #print "files: ", files

        smp_name = ''
        run = ''
        lane = ''
        lib = ''
        fname1 = ''
        fname2 = ''
        #defaults
        # quality = "33"
        # status = "Data_is_valid"

        i = 0
        for sample in sample_names:
            print(sample)
            sample_path = os.path.join(self.reads_dir, sample)
            for run_lane in os.listdir(sample_path):
                print(("run_lane: ", run_lane))
                # extract run and lane
                m = re.match("^run(\S*)_(\S*)$", run_lane)
                run = m.group(1)
                lane = m.group(2)
                print(("runlane: ", run_lane, " run: ", run, " lane: ", lane))
                run_lane_path = os.path.join(sample_path, run_lane)

                # take only first pair
                # for each library
                for fname in [f for f in os.listdir(run_lane_path) if re.match('(.+)\.single\.fastq\.gz$', f)]:

                    # print("fname: ", fname)
                    m2 = re.match("^(\S+)\.(\S+)\.33\.single\.fastq\.gz$", fname)
                    smp_name = m2.group(1)
                    lib = m2.group(2)
                    # reconstruct both pairs
                    # in case we miss one we miss all
                    fname1 = r"""%s.%s.33.single.fastq.gz""" % (smp_name, lib)
                    # fname2 = r"""%s.%s.33.pair2.fastq.gz""" % (smp_name, library)

                    # now we know everything
                    row = [i, "0", sample, run, lane,  lib, fname1, fname2]
                    print(("row: ", row))
                    writer.writerow(row)
                    i += 1

                    if do_report:
                        # analyse link only for pair1
                        fname_f = os.path.join(run_lane_path, fname1)

                        # do not report links if files are hardcoded
                        if os.path.islink(fname_f):
                            fname_lnk = os.path.realpath(fname_f)
                            # extract project name

                            patlnk = "^" + project_regex + "([^\/]+\/[^\/]+)\/(\S+)$"
                            print(("patlnk: ", patlnk))
                            print(("fname_lnk: ", fname_lnk))


                            motlnk = re.match(patlnk, fname_lnk)
                            src_folder = motlnk.group(2)
                            src_pair = motlnk.group(3)

                            row2 = [sample, run_lane, lib, src_folder, src_pair]
                            writer_smp_report.writerow(row2)



        #
        ofile.close()
        smp_report_csv.close()

    def raw_reads_to_all_sample_info_pe(self, project_regex):

        # ########### output config file open and header

        ofile = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "w")
        smp_report_csv = open(os.path.join(self.conf.conf_dir, 'smp_report.csv'), "w")

        # writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        # no more quoting everywere
        writer = csv.writer(ofile, delimiter=',', quotechar='"')
        writer_smp_report = csv.writer(smp_report_csv, delimiter=',', quotechar='"')

        # ofile_header = ["Name", "Library Barcode", "Run Type", "Run", "Quality Offset", "Region", "Status", "FASTQ1", "FASTQ2", "BAM", "Library Source"]

        ofile_header = ["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"]

        writer.writerow(ofile_header)
        ###############################################

        sample_names = [f for f in os.listdir(self.reads_dir) ]
        #print "files: ", files

        smp_name = ''
        run = ''
        lane = ''
        lib = ''
        fname1 = ''
        fname2 = ''
        #defaults
        # quality = "33"
        # status = "Data_is_valid"

        i = 0
        for sample in sample_names:
            print(sample)
            sample_path = os.path.join(self.reads_dir, sample)
            for run_lane in os.listdir(sample_path):
                print(("run_lane: ", run_lane))
                # extract run and lane
                # m = re.match("^run(\S*)_(\S*)$", run_lane)
                m = re.match("^(\S*)_(\S*)$", run_lane)
                run = m.group(1)
                lane = m.group(2)
                print(("runlane: ", run_lane , " run: ", run, " lane: ", lane))
                run_lane_path = os.path.join(sample_path, run_lane)
                print("run_lane_path: ", run_lane_path)

                # take only first pair
                # for each library
                for fname in [f for f in os.listdir(run_lane_path) if re.match('(.+)\.pair1\.fastq\.gz$', f)]:

                    print("fname: ", fname)
                    m2 = re.match("^(\S+)\.(\S+)\.33\.pair1\.fastq\.gz$", fname)
                    smp_name = m2.group(1)
                    lib = m2.group(2)
                    # reconstruct both pairs
                    # in case we miss one we miss all
                    fname1 = r"""%s.%s.33.pair1.fastq.gz""" % (smp_name, lib)
                    fname2 = r"""%s.%s.33.pair2.fastq.gz""" % (smp_name, lib)
                    # now we know everything
                    row = [i, "0", sample, run, lane,  lib, fname1, fname2]
                    print(("row: ", row))
                    writer.writerow(row)
                    i += 1

                    # analyse link only for pair1
                    fname_f = os.path.join(run_lane_path, fname1)

                    # do not report links if files are hardcoded
                    if os.path.islink(fname_f):
                        fname_lnk = os.path.realpath(fname_f)
                        # extract project name

                        patlnk = "^" + project_regex + "([^\/]+\/[^\/]+)\/(\S+)$"
                        print(("patlnk: ", patlnk))
                        print(("fname_lnk: ", fname_lnk))


                        motlnk = re.match(patlnk, fname_lnk)
                        src_folder = motlnk.group(1)
                        src_pair = motlnk.group(2)

                        row2 = [sample, run_lane, lib, src_folder, src_pair]
                        writer_smp_report.writerow(row2)



        #
        ofile.close()
        smp_report_csv.close()


    def all_sample_to_one_sample_info(self):

        # with pandas structures
        ifile  = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "rb")

        self.conf.sample_lanes = pd.read_csv(ifile,
                                             # names=["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"],
                                             header=0)
        ifile.close()

        sm = self.conf.sample_lanes
        #print sm
        #print sm.index
        #print sm.columns
        #grouped = sm.groupby('name')

        gr2 = sm.groupby(['smp_name'], sort=True)

        gr3 = gr2['smp_name']
        #print gr3.groups.keys()

        gr4 = gr3.count()


        gr5= pd.DataFrame(list(gr3.groups.keys()))
        gr5.columns = ['smp_name']
        #print gr5

        gr5.set_index(["smp_name"], inplace=True)
        gr5.sort_index(inplace=True)
        gr5["skip"] = 0

        gr5.reset_index(inplace=True, drop=False)
        gr5 =gr5[[ "skip", "smp_name"]]

        # def my_test(a):
            # cum_diff = 0
            # for ix in gr5.index():
            #     cum_diff = cum_diff + (a - df['a'][ix])
            # return cum_diff


        def remove_suffix(cell):
            pat = r"""(.+)\_%s$""" % self.conf.proj_abbrev.upper()
            # print("pat: ", pat)

            m = re.match(pat, cell)
            if m:
                old_pref = m.group(1)
                res = old_pref
            else:
                res = cell
            return res

        def replace_suffix(cell):
            pat = r"""(.+)\_%s$""" % self.conf.proj_abbrev.upper()
            # print("pat: ", pat)

            m = re.match(pat, cell)
            if m:
                old_pref = m.group(1)
                res = "%s_%s" % (old_pref, self.conf.exper_abbrev.upper())
            else:
                res = cell
            return res

        # gr5['Value'] = gr5.apply(lambda row: my_test(row['smp_name'], row['skip']), axis=1)

        # gr5["short_name"] = gr5["smp_name"]
        gr5["short_name"] = gr5.apply(lambda row: replace_suffix(row["smp_name"]), axis=1)

        gr5["cls_idx"] = 0

        gr5["vshort_name"] = gr5.apply(lambda row: remove_suffix(row["smp_name"]), axis=1)


        gr5.to_csv(os.path.join(self.conf.conf_dir, 'one-sample-info.csv'),
                           sep=',', header=True
                           , index=True
                           , index_label="smp_idx"
                           )

        #print grouped.groups
        #for name, group in gr3:
        #    print(name)
        #    print(group)

        #self.conf.sample_names = gr3.groups.keys()

    def one_sample_to_grp_sample_info(self):

        # with pandas structures
        ifile  = open(os.path.join(self.conf.conf_dir, 'one-sample-info.csv'), "rb")

        gr1 = pd.read_csv(ifile,
                          # names=["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"],
                          header=0)
        ifile.close()

        gr2 = gr1.copy()
        gr2.rename(columns={'smp_idx': 'smpgrp_idx', 'smp_name': 'smpgrp_name'}, inplace=True)
        # gr2.rename(columns={'smp_name': 'smpgrp_name'}, inplace=True)

        # index name is separate
        gr2.to_csv(os.path.join(self.conf.conf_dir, 'grp-sample-info.csv'),
                           sep=',', header=True
                           , index=False
                           # , index_label="smpgrp_idx"
                           )

    def grp_sample_to_sc_grp_sample_info(self):

        # with pandas structures
        ifile  = open(os.path.join(self.conf.conf_dir, 'grp-sample-info.csv'), "rb")

        gr1 = pd.read_csv(ifile,
                          # names=["spln_idx", "skip", "smp_name", "run", "lane", "lib", "fastq1", "fastq2"],
                          header=0)
        ifile.close()

        gr2 = gr1.copy()
        # gr2.rename(columns={'smp_idx': 'smpgrp_idx', 'smp_name': 'smpgrp_name'}, inplace=True)
        # gr2.rename(columns={'smp_name': 'smpgrp_name'}, inplace=True)
        gr2["sc_grp_idx"] = 0
        gr2["sc_ord_idx"] = 0

        # index name is separate
        gr2.to_csv(os.path.join(self.conf.conf_dir, 'sc-grp-sample-info.csv'),
                           sep=',', header=True
                           , index=False
                           # , index_label="smpgrp_idx"
                           )


    @property
    def htchip_src_dir(self):
        this_dir = os.path.join(self.conf.proj_dir, "fastq")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    #@property
    #def htchip_run(self):
    #    return self.__htchip_run

    #@htchip_run.setter
    #def htchip_run(self, val):
    #    self.__htchip_run = val


    @property
    def htchip_col_src_r1(self):
        return os.path.join(self.htchip_src_dir, "%s.%s.%s.%s.%s_%s_R1.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.htchip_col_name, self.dat_lib))

    @property
    def htchip_col_src_r2(self):
        return os.path.join(self.htchip_src_dir, "%s.%s.%s.%s.%s_%s_R2.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.htchip_col_name, self.dat_lib))

    @property
    def htchip_dst_dir(self):
        this_dir = os.path.join(self.conf.proj_dir, "fastq-htchip")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir


    @property
    def htchip_col_r1_am(self):
        #return os.path.join(self.htchip_dst_dir, "%s.r1.bam" % self.htchipcol_name)
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_R1.bam" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name))

    @property
    def htchip_col_r2_am(self):
        #return os.path.join(self.htchip_dst_dir, "%s.r2.bam" % self.htchipcol_name)
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_R2.bam" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name))


    @property
    def htchip_col_r1(self):
        #return os.path.join(self.htchip_dst_dir, "%s.R1.fastq" % self.htchipcol_name)
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_R1.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name))

    @property
    def htchip_col_r2(self):
        #return os.path.join(self.htchip_dst_dir, "%s.R2.fastq" % self.htchipcol_name)
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_R2.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name))

    @property
    def htchip_col_dir(self):
        this_dir = os.path.join(self.htchip_dst_dir, self.htchip_col_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def htchip_col_row_r1(self):
        #print("----------------------htchip_column_dst_dir: ", self.htchip_column_dst_dir)
        #return os.path.join(self.htchip_col_dir, "%s.R1.fastq" % )
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_%s.R1.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name, self.htchip_row_name))

    @property
    def htchip_col_row_r2(self):
        #return os.path.join(self.htchip_col_dir, "%s.R2.fastq" % self.htchip_row_name)
        return os.path.join(self.htchip_dst_dir, "%s.%s.%s.%s.%s_%s_%s.R2.fastq.gz" % (self.dat_mach, self.dat_run, self.dat_lane, self.htchipcol_idn, self.dat_lib, self.htchipcol_name, self.htchip_row_name))

    @property
    def htchip_handles_for_rows_r1(self):
        return self.__htchip_handles_for_rows_r1

    @htchip_handles_for_rows_r1.setter
    def htchip_handles_for_rows_r1(self, val):
        self.__htchip_handles_for_rows_r1 = val

    @property
    def htchip_handles_for_rows_r2(self):
        return self.__htchip_handles_for_rows_r2

    @htchip_handles_for_rows_r2.setter
    def htchip_handles_for_rows_r2(self, val):
        self.__htchip_handles_for_rows_r2 = val

    @property
    def htchip_row_barcode(self):
        return self.__htchip_row_barcode

    @htchip_row_barcode.setter
    def htchip_row_barcode(self, val):
        self.__htchip_row_barcode = val

    @property
    def htchip_default_barcodes(self):
        return dict(
            CACGTA='01',
            CTCACA='02',
            TGCATC='03',
            TCAGAC='04',
            CGATGT='05',
            TACTGC='06',
            ATGCTC='07',
            CATCTG='08',
            GACTCA='09',
            AGATCG='10',
            ATCAGC='11',
            GCTACA='12',
            CAGATC='13',
            CACAGT='14',
            TACGAG='15',
            CGACTA='16',
            GCATCT='17',
            AGCACT='18',
            ACACTG='19',
            CGTAGA='20',
            ACTCGA='21',
            ACATGC='22',
            CGTCAT='23',
            TGTACG='24',
            GCAGTA='25',
            TCACGT='26',
            ACGTCA='27',
            CTCGAT='28',
            ATCGTG='29',
            GCTGAT='30',
            GTCTAC='31',
            CATGCT='32',
            TAGCAC='33',
            GTGCAT='34',
            TAGTCG='35',
            TCTCAG='36',
            CTAGTC='37',
            TCTAGC='38',
            ATGACG='39',
            GAGCTA='40'
        );


    # this property comes from the barcode
    @property
    def htchip_barcode_row_name(self):

        row_txt = None
        if self.htchip_row_barcode in self.htchip_default_barcodes:
            row_txt = "ROW%s" % self.htchip_default_barcodes[self.htchip_row_barcode]
        else:
            row_txt = 'ROWER'

        return row_txt

    # this is the result that has to be assigned
    @property
    def htchip_row_name(self):
        return self.__htchip_row_name

    @htchip_row_name.setter
    def htchip_row_name(self, val):
        self.__htchip_row_name = val

    @property
    def htchip_stats_fastq_csv(self):
        return os.path.join(self.conf.metrics2_dir, "htchip_stats_fastq.csv")


    #@property
    #def htchipcol_idn(self):
    #    return self.__htchip_col_idn

    #@htchipcol_idn.setter
    #def htchipcol_idn(self, val):
    #    self.__htchip_col_idn = val

    #@property
    #def htchipcol_name(self):
    #    return self.__htchip_col_name

    #@htchipcol_name.setter
    #def htchipcol_name(self, val):
    #    self.__htchip_col_name = val

    # thise are the names obtained, with all errors toghether
    @property
    def htchip_dst_row_names(self):

        dst_row_names_arr = []

        row_txt = None

        # open all row handles
        for bcode in self.htchip_default_barcodes.keys():
            self.htchip_row_barcode = bcode
            dst_row_names_arr.append(self.htchip_barcode_row_name)

        dst_row_names_arr.append('ROWER')

        return dst_row_names_arr



    def htchip_file_row(self, source, name, r1seq, r1qua, r2seq, r2qua):


        self.htchip_handles_for_rows_r1[self.htchip_barcode_row_name].write( ("@%s\n%s\n+\n%s\n" % (name, r1seq, r1qua)).encode('utf-8'))
        self.htchip_handles_for_rows_r2[self.htchip_barcode_row_name].write( ("@%s\n%s\n+\n%s\n" % (name, r2seq, r2qua)).encode('utf-8'))

        #print(source, name, self.htchip_row_barcode, self.htchip_row_name)


    def htchip_order_column_fq(self):

        exit_codes = []

        # delete output files
        #for i in [self.spln_proper_bam, self.spln_sort_bam, self.spln_bedpe,
        #          self.spln_acc_bam, self.spln_h5store, self.one_hplex_qc_stats_csv]:
        #    if os.path.exists(i):
        #       os.remove(i)


        for fq, am in [[self.htchip_col_src_r1, self.htchip_col_r1_am],
                       [self.htchip_col_src_r2, self.htchip_col_r2_am]]:
            # convert to bam
            # also sorts queryname
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
    -XX:ParallelGCThreads=8 \
    -Xms8G -Xmx12G \
    -Dsamjdk.use_async_io=true \
    -jar ${PICARD_JAR} FastqToSam \
    FASTQ=%(fq)s O=%(am)s SM=r1am SO=queryname
    TMP_DIR=%(scratch_d)s \
    VALIDATION_STRINGENCY=SILENT \
    CREATE_INDEX=true
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "fq": fq,
           "am": am
           }
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=self.htchip_dst_dir, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)
            exit_codes.append(rc3)

        for am, fq in [[self.htchip_col_r1_am, self.htchip_col_r1],
                       [self.htchip_col_r2_am, self.htchip_col_r2]]:
            # just output the sorted input
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
    -XX:ParallelGCThreads=8 \
    -Xms8G -Xmx12G \
    -Dsamjdk.use_async_io=true \
    -jar ${PICARD_JAR} SamToFastq \
    I=%(am)s F=%(fq)s
    TMP_DIR=%(scratch_d)s \
    VALIDATION_STRINGENCY=SILENT \
    CREATE_INDEX=true
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "am": am,
           "fq": fq
           }
            print(cmd)
            p3 = subprocess.Popen(cmd, shell=True, cwd=self.htchip_dst_dir, stdout=subprocess.PIPE)
            p3.wait()
            rc3 = p3.returncode
            print("rc3: ", rc3)
            exit_codes.append(rc3)

        # delete intermediary files
        for i in [self.htchip_col_r1_am, self.htchip_col_r2_am]:
            if os.path.exists(i):
                os.remove(i)

        return exit_codes


    def htchip_demultiplex(self):





        exit(0)

        #for idx in range(1):
        for idx in range(nbcols):
            pass


    # conditionaly included from init_proj
    def htchip_init_exp_data(self):

        self.dat_mach = 'HI'

        # nsp.htchip_run = '1868'
        # self.dat_lane = '008'

        self.dat_run = '3530'
        self.dat_lane = '001'

        self.dat_lib = '1868'


    def htchip_demul_col(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        args = {"htchipcol_idx": self.htchipcol_idx}
        print("parameters: \n", args)

        exit_codes = []

        print("i: ", self.htchipcol_idx, self.htchip_col_idn, self.htchip_col_name)
        exit_codes += self.htchip_order_column_fq()


        #base_chip = "/gs/project/wst-164-ab/share/data/test-x035-p12-htchip/fastq-htchip"
        #ord_name = "ord"

        #r1fname = os.path.join(self.htchip_column_dirbase_chip,"%s_R1.fastq" % ord_name)
        #|r2fname = os.path.join(base_chip,"%s_R2.fastq" % ord_name)

        r1f =  pysam.FastqFile(self.htchip_col_r1, "rb")
        r2f =  pysam.FastqFile(self.htchip_col_r2, "rb")

        # initialize row handles container
        self.htchip_handles_for_rows_r1 = {}
        self.htchip_handles_for_rows_r2 = {}


        # open all row handles for rownames (destination files), not barcodes (source files)
        #print("rownames----------: ", self.htchip_dst_row_names)
        for rowname in self.htchip_dst_row_names:

            self.htchip_row_name = rowname
            print("self.htchip_row_name----------: ", self.htchip_row_name)

            #self.htchip_handles_for_rows_r1[rowname] = open(self.htchip_col_row_r1, "w")
            self.htchip_handles_for_rows_r1[rowname] = gzip.open(self.htchip_col_row_r1, "wb")

            #self.htchip_handles_for_rows_r2[rowname] = open(self.htchip_col_row_r2, "w")
            self.htchip_handles_for_rows_r2[rowname] = gzip.open(self.htchip_col_row_r2, "wb")


        #print("len: ", r1f.__next__())

        r1end = False
        r2end = False

        r1_bar = {}
        r1_seq = {}
        r1_qua = {}

        r2_seq = {}
        r2_qua = {}


        i=0
        while not (r1end and r2end):
            try:
                r1l = r1f.__next__()
                #[start:end+1]
                r1_bar[r1l.name] = r1l.sequence[:6]
                r1_seq[r1l.name] = r1l.sequence[6:]
                r1_qua[r1l.name] = r1l.quality[6:]

                # filled one
                if r1l.name in r2_seq and r1l.name in r2_qua:
                    # finished r1l
                    self.htchip_row_barcode = r1_bar[r1l.name]
                    self.htchip_file_row('r1l', r1l.name, r1_seq[r1l.name], r1_qua[r1l.name], r2_seq[r1l.name], r2_qua[r1l.name])
                    r1_seq.pop(r1l.name)
                    r1_qua.pop(r1l.name)
                    r2_seq.pop(r1l.name)
                    r2_qua.pop(r1l.name)
                #


            # print("s.quality: ", s.quality)
            # print("s.name: ", s.name)
            # print("comment: ", s.comment)
            except StopIteration:
                print("r1l ended...")
                r1end = True


            try:
                r2l = r2f.__next__()
                r2_seq[r2l.name] = r2l.sequence
                r2_qua[r2l.name] = r2l.quality

                # filled one
                if r2l.name in r1_bar and \
                   r2l.name in r1_seq and \
                   r2l.name in r1_qua:
                    # finished r2l
                    self.htchip_row_barcode = r1_bar[r1l.name]
                    self.htchip_file_row('r2l', r2l.name, r1_seq[r2l.name], r1_qua[r2l.name], r2_seq[r2l.name], r2_qua[r2l.name])
                    # clean
                    r1_seq.pop(r2l.name)
                    r1_qua.pop(r2l.name)
                    r2_seq.pop(r2l.name)
                    r2_qua.pop(r2l.name)






            except StopIteration:
                print("r2l ended...")
                r2end = True





            #r2l = r2f.__next__()
            #print("r1l: ", i, r1l.name)
            #print("r2l: ", r1l)
            i += 1
            #if i > 10:
            #    exit(0)

        # close all handles
        for rowname in self.htchip_dst_row_names:
            self.htchip_row_name = rowname
            print("self.htchip_row_name----------: ", self.htchip_row_name)

            self.htchip_handles_for_rows_r1[rowname].close()
            self.htchip_handles_for_rows_r2[rowname].close()

        # delete intermediary files
        for i in [self.htchip_col_r1, self.htchip_col_r2]:
            if os.path.exists(i):
                os.remove(i)


        self.mark_exit_args(exit_codes, args)





        #i = 0
        #for s in file:

            # Setting With Enlargement
            #rname_arr[i, 0] = s.name
            #rname_arr[i, 1] = s.sequence

            # idx_df.ix[i,0] = s.name
            # idx_df.ix[i,1] = s.sequence
            #i += 1
            # print("s: ", s)
            # print("s.sequence: ", s.sequence)
            # print("s.quality: ", s.quality)
            # print("s.name: ", s.name)
            # print("comment: ", s.comment)




        #prdf = "/gs/project/wst-164-ab/share/data/test-x035-p12-htchip/fastq-htchip/prd2.bam"

        #infile = pysam.AlignmentFile(prdf, "r")
        #out_acc = pysam.AlignmentFile(self.spln_acc_bam, "wb", template=infile)
        # out_rej = pysam.AlignmentFile("rej.bam", "wb", template=infile)

        #i =0
        #for s in infile:

         #   if i % 100000 == 0:
         #       print("i: ", i)

            #print(str(s))

            #if s.is_read1 =="True":
            #    print("read1: ", s.query_name)

            #if i > 10:
            #    exit(0)

            #if s.query_name in sel2.index:
            # if s.query_name in rnames2:
            #     xmtag = sel2.at[s.query_name, 'idx_seq']

                # print("xmtag: ", xmtag)

                # s.tags["MQ"] = 0
                #s.setTag("XM", sel2.at[s.query_name, 'idx_seq'])
                #s.setTag("XC", sel2.at[s.query_name, 'cnt'])
                #s.setTag("XS", sel2.at[s.query_name, 'score_min'])

                # print("s.tags: ", s.tags)

                #out_acc.write(s)


            # if s.query_name in rnames2:
            #     out_acc.write(s)



#            i+=1

        #out_acc.close()
        # out_rej.close()

    def htchip_stats_fastq(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname() )

        exit_codes = []

        columns = ['idx', 'idn', 'col_name', 'row_name', 'r1_reads', 'r2_reads']

        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)

        dtypes = {'idx': 'int', 'idn': 'object', 'col_name': 'object', 'row_name': 'object',
                  'r1_reads': 'int', 'r2_reads': 'int'}
        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        print("df_: \n", df_, df_.dtypes)



        cols = self.conf.htchip_colnames_df.query('skip != 1').index.values
        for idx in cols:
            self.htchipcol_idx = int(idx)


            for rowname in self.htchip_dst_row_names:
                self.htchip_row_name = rowname

                cmd = r"""zcat %s | echo $((`wc -l`/4))""" % self.htchip_col_row_r1
                # print(cmd)
                p1 = subprocess.Popen(cmd, shell=True, cwd=self.htchip_dst_dir, stdout=subprocess.PIPE)
                colrow_r1_reads = p1.communicate()[0].decode("utf-8").rstrip()
                rc1 = p1.returncode
                # print("rc1: ", rc1)
                exit_codes.append(rc1)

                cmd = r"""zcat %s | echo $((`wc -l`/4))""" % self.htchip_col_row_r2
                # print(cmd)
                p1 = subprocess.Popen(cmd, shell=True, cwd=self.htchip_dst_dir, stdout=subprocess.PIPE)
                colrow_r2_reads = p1.communicate()[0].decode("utf-8").rstrip()
                rc1 = p1.returncode
                # print("rc1: ", rc1)
                exit_codes.append(rc1)

                # print("idx: ", idx,
                #       self.htchipcol_idx,
                #       self.htchipcol_idn,
                #       )
                # Setting With Enlargement
                row = [self.htchipcol_idx,
                       self.htchipcol_idn,
                       self.htchipcol_name,
                       self.htchip_row_name,
                       int(colrow_r1_reads),
                       int(colrow_r2_reads)]
                print("row: ", row)

                df_.loc[len(df_)+1] = row

        print("df_: \n", df_)

        # df_.sort(['sample'], ascending=[1], inplace=True)
        # df_ = df_.reset_index(drop=True)

        df_.to_csv(self.htchip_stats_fastq_csv,
                           sep=',', header=True
                           , index=True
                           , index_label="idx"
                           )


        return exit_codes

    def htchip_debug(self):

        tst_code = "TACTGC"
        ntst = 0
        rtst = 0

        ncode=[0, 0, 0, 0, 0, 0]

        filename = "/lb/project/ioannisr/share/expr/a00ht/fastq/HI.3530.001.N726.TC_200_1OF2_1868_R1.fastq.gz"
        i = 0
        with pysam.FastxFile(filename) as fh:
            for entry in fh:
                # if i>= 100:
                #     break
                # print(entry.name)

                bcode = entry.sequence[0:6]
                for x in range(6):
                    if bcode[x] == "N":
                        ncode[x] += 1

                if bcode == tst_code:
                    ntst += 1
                else:
                    rtst += 1

                print(bcode, entry.sequence)
                # print(entry.comment)
                # print(entry.quality)
                i += 1
        print(ncode, ntst, rtst)




if __name__ == '__main__':
    pass



