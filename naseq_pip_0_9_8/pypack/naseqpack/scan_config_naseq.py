# -*- coding: utf-8 -*-
"""
Created on Wed Nov 12 15:24:32 2014

@author: root
"""
from __future__ import print_function

import os
import csv
import pandas as pd
from collections import OrderedDict

# from treedict import TreeDict
# import ConfigParser
import configparser

import imp
import os

pypack = os.environ['NASEQ_PYPACK']

naseq_server_type = imp.load_source('naseq_server_type', "%s/naseqpack/naseq_server_type.py" % (pypack) )
# NaSeqServerType = naseq_server_type.NaSeqServerType
from naseq_server_type import NaSeqServerType

import subprocess


class ScanConfig(object):

    def get_with_default(self,section,name,default):
        return self.naseq_config.get(section,name) if self.naseq_config.has_section(section) and self.naseq_config.has_option(section, name) else default

    def get_boolean_with_default(self,section,name,default):
        return self.naseq_config.getboolean(section,name) if self.naseq_config.has_section(section) and self.naseq_config.has_option(section, name) else default

    # original is this one
    # self.naseq_config.get()

    def __init__(self):

        # defaults_dict = {'Assembly': None,
        #                  'Transcriptome': None,
        #                  'Target': None,
        #                  'Transcriptome': None,
        #                  'stats_depth_calc': None,
        #                  'rmdup_type': None}

        # self.naseq_config = ConfigParser.ConfigParser(defaults_dict)
        self.naseq_config = configparser.ConfigParser()
        self.load_config()


        self.exper_abbrev = self.get_with_default("Project", "ExperAbbrev", "e01")

        self.ucsc_genome = self.get_with_default("Project", "UcscGenome", "")

        self.ref_gen_assembly = self.get_with_default("Project", "Assembly", None)
        self.ref_transcr = self.get_with_default("Project", "Transcriptome", None)

        self.ref_gen_target = self.get_with_default("Project", "Target", None)
        self.next_queue_dict_name = self.get_with_default("Project", "next_queue_dict_name", None)
        self.qgraph_dict_name = self.get_with_default("Project", "qgraph_dict_name", None)
        self.biochem_ver = self.get_with_default("Project", "biochem_ver", "2")

        self.seq_type = self.get_with_default("Project", "seq_type", None)
        self.seq_exp = self.get_with_default("Project", "seq_exp", None)

        self.trim_readpairs = self.get_with_default("Project", "trim_readpairs", None)
        self.align_readpairs = self.get_with_default("Project", "align_readpairs", None)
        self.merge_readpairs = self.get_boolean_with_default("Project", "merge_readpairs", False)

        self.realign_gatk = self.get_boolean_with_default("Project", "realign_gatk", False)
        self.realign_abra = self.get_boolean_with_default("Project", "realign_abra", False)

        self.reads_loc = self.get_with_default("Project", "reads_loc", "")

        print("reads_loc: %s" % self.reads_loc)
        print("reads_loc: %s" % self.raw_reads_dir)
        print("reads_loc: %s" % self.raw_afl_dir)

        if self.reads_loc is not None:
            if self.reads_loc == "raw":
                self.reads_dir = self.raw_reads_dir
            if self.reads_loc == "afl":
                self.reads_dir = self.raw_afl_dir

        # we need them to be empty to be able to test membership
        self.res_stages = self.get_with_default("Project", "res_stages", "")
        if self.res_stages is not None:
            self.res_stages_arr = self.res_stages.split(", ")

        self.dat_stages = self.get_with_default("Project", "dat_stages", "")
        if self.dat_stages is not None:
            self.dat_stages_arr = self.dat_stages.split(", ")


        self.use_smp_realign = self.get_with_default("Project", "use_smp_realign", None)

        self.caller_prefix = self.get_with_default("Project", "caller_prefix", None)
        self.use_callers = self.get_with_default("Project", "use_callers", None)
        # normalize to - separator
        if self.use_callers is not None:
            self.use_callers = self.use_callers.replace(',', '-')

        self.qc_grp_depth_file = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_depth_file", "").split(',')))
        self.qc_grp_depth_flts_dup = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_depth_flts_dup", "").split(',')))

        self.qc_grp_track_file = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_track_file", "").split(',')))
        self.qc_grp_track_flts_dup = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_track_flts_dup", "").split(',')))
        self.qc_grp_track_flts_uni = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_track_flts_uni", "").split(',')))
        self.qc_grp_track_flts_stn = list(map(lambda x: x.strip(), self.get_with_default("Project", "qc_grp_track_flts_stn", "").split(',')))

        self.grind_callers = list(map(lambda x: x.strip(), self.get_with_default("Project", "grind_callers", "").split(',')))

        self.stats_depth = self.get_with_default("Project", "stats_depth_calc", None)
        self.rmdup_type = self.get_with_default("Project", "rmdup_type", None)
        self.rmdup_level = self.get_with_default("Project", "rmdup_level", None)

        self.extract_noncan = self.get_boolean_with_default("Project", "extract_noncan", False)


        self.c3p_size = self.get_with_default("Project", "c3p_size", None)

        self.c3p_bulk_idx = self.get_with_default("Project", "c3p_bulk_idx", None)

        self.this_python_mod = self.get_with_default("Project", "this_python_mod", None)
        self.alt_python_mod = self.get_with_default("Project", "alt_python_mod", None)

        self.jdk8_mod = self.get_with_default("Project", "jdk8_mod", None)
        self.jdk7_mod = self.get_with_default("Project", "jdk7_mod", None)


        #
        tmp = self.get_with_default("Project", "scratch_loc_dir", None)
        if tmp is not None:
            self.scratch_loc_dir = os.path.expandvars(tmp)

        tmp = self.get_with_default("Project", "scratch_glo_dir", None)
        if tmp is not None:
            self.scratch_glo_dir = os.path.expandvars(tmp)

        tmp = self.get_with_default("Project", "lab_dir", None)
        if tmp is not None:
            reldir = os.path.expandvars(tmp)
            self.lab_dir = os.path.realpath(reldir)
            print("self.lab_dir", self.lab_dir)


        tmp = self.get_with_default("trim_lane_trimmomatic", "adapt", None)
        if tmp is not None:
            self.trim_lane_trimmomatic_adapt = os.path.expandvars(tmp)
        # configuration files have variable substitution by default

        # absolute environment variables
        tmp = self.get_with_default("trim_lane_trimmomatic", "illuminaclip", "")
        if tmp is not None:
            self.trim_lane_trimmomatic_illuminaclip = os.path.expandvars(tmp)

        self.spln_align_aligner = self.get_with_default("spln_align", "aligner", None)

        self.ref_transcr_hisat = self.get_with_default("spln_align", "TanscriptomeHisat", None)
        self.spln_align_extra = self.get_with_default("spln_align", "extra", "")


        #
        # absolute environment variables
        # self.merge_lanes_gatk_markdup = self.get_boolean_with_default("merge_lanes", "gatk_markdup", None)
        # if self.merge_lanes_gatk_markdup is not None:
        #     self.merge_lanes_gatk_rmdup = self.get_boolean_with_default("merge_lanes", "gatk_rmdup", None)

        # absolute environment variables
        self.smp_clp_markdup = self.get_boolean_with_default("smp_dedup", "clp_markdup", None)
        if self.smp_clp_markdup is not None:
            self.smp_clp_rmdup = self.get_boolean_with_default("smp_dedup", "clp_rmdup", None)

        # absolute environment variables
        self.smp_spl_markdup = self.get_boolean_with_default("smp_dedup", "spl_markdup", None)
        if self.smp_spl_markdup is not None:
            self.smp_spl_rmdup = self.get_boolean_with_default("smp_dedup", "spl_rmdup", None)

        self.spl_use = self.get_with_default("smp_dedup", "spl_use", "ded")

        self.raw_counts_extra = self.get_with_default("raw_counts", "extra", "")
        self.fpkm_known_extra = self.get_with_default("fpkm_known", "extra", "")


        # absolute environment variables

        self.indel_realigner_realigner_target_creator_extra = self.get_with_default("indel_realigner", "RealignerTargetCreatorExtra", None)
        self.indel_realigner_indel_realigner_extra = self.get_with_default("indel_realigner", "IndelRealignerExtra", None)


        self.call_hc_haplotype_caller_extra = self.get_with_default("call_hc", "HaplotypeCallerExtra", None)

        self.genotype_gvcf_contig_genotypegvcfs_extra = self.get_with_default("genotype_gvcf_contig", "GenotypeGVCFsExtra", None)


    @property
    def proj_dir(self):

        # return os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        curr_path = os.environ['PWD']
        # print("pwd: ", curr_path)
        absdir = os.path.abspath(os.path.join(curr_path, ".."))
        proj_dir = os.path.realpath(absdir)
        return proj_dir

    @property
    def proj_dir_loc(self):

        relpath = os.path.relpath(self.proj_dir, self.lab_dir)
        # print("relpath: ", relpath, self.proj_dir, self.lab_dir)
        loc_dir  = os.path.join(self.scratch_loc_dir, relpath)
        # print("loc_dir: ", loc_dir)

        # loc_dir2 = os.path.abspath(loc_dir)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def proj_dir_glo(self):

        relpath = os.path.relpath(self.proj_dir, self.lab_dir)
        loc_dir  = os.path.join(self.scratch_glo_dir, relpath)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

        # os.path.commonprefix(['/gs/project/wst-164-aa/share/', "%s/" % self.proj_dir])
        # return os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        # curr_path = os.environ['PWD']
        # print("path: ", thispath)
        # return 0
        # return os.path.abspath(os.path.join(curr_path, ".."))

    @property
    def naseq_home_dir(self):

        # return os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        naseq_home = os.environ['NASEQ_HOME']
        # print("pwd: ", curr_path)
        return os.path.abspath(naseq_home)

    @property
    def raglab_install_home(self):
        return os.getenv('RAGLAB_INSTALL_HOME', "")

    # @property
    # def scratch_loc_dir(self):
    #     return os.getenv('LSCRATCH', "")
        # return os.getenv('SCRATCH', "")

    # @property
    # def scratch_glo_dir(self):
    #     return os.getenv('SCRATCH', "")

    @property
    def conf_dir(self):
        return os.path.join(self.proj_dir, "conf")

    @property
    def fastq_noncan_dir(self):
        this_dir = os.path.join(self.proj_dir, "fastq_noncan")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def bam_dir(self):
        return os.path.join(self.proj_dir, "bam")

    @property
    def raw_reads_dir(self):
        return os.path.join(self.proj_dir, "raw_reads")

    @property
    def raw_afl_dir(self):
        return os.path.join(self.proj_dir, "raw_afl")

    @property
    def reads_dir(self):
        return self.__reads_dir

    @reads_dir.setter
    def reads_dir(self, val):
        self.__reads_dir = val

    # @property
    # def align_dir(self):
    #     return os.path.join(self.proj_dir, "alignment")

    @property
    def align2_dir(self):
        loc_dir = os.path.join(self.proj_dir, "align2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def align2_dir_loc(self):
        loc_dir = os.path.join(self.proj_dir_loc, "align2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def align2_dir_glo(self):
        loc_dir = os.path.join(self.proj_dir_glo, "align2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def srvf_dir_glo(self):
        loc_dir = os.path.join(self.proj_dir_glo, "srvf")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def align3_dir(self):
        loc_dir = os.path.join(self.proj_dir, "align3")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def align3_dir_loc(self):
        loc_dir = os.path.join(self.proj_dir_loc, "align3")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def align3_dir_glo(self):
        loc_dir = os.path.join(self.proj_dir_glo, "align3")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def matr_dir(self):
        loc_dir = os.path.join(self.proj_dir, "matr")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def metr_dir(self):
        loc_dir = os.path.join(self.proj_dir, "metrics2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def deploy_dir(self):
        loc_dir = os.path.join(self.proj_dir, "deploy")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def deploy_dir_glob(self):
        return os.path.join(self.deploy_dir, "*" )

    @property
    def deploy_bams_dir(self):
        loc_dir = os.path.join(self.deploy_dir, "bams")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def ucsc_hub_dir(self):
        loc_dir = os.path.join(self.deploy_dir, "ucsc_hub")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ucsc_hub_big_wig_dir(self):
        loc_dir = os.path.join(self.ucsc_hub_dir, "big_wig")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def templ_dir(self):
        loc_dir = os.path.join(self.proj_dir, "templ")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def templ_jinja_dir(self):
        loc_dir = os.path.join(self.templ_dir, "jinja")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    # @property
    # def reads_dir(self):
    #     return os.path.join(self.proj_dir, "reads")

    @property
    def reads2_dir(self):
        # loc_dir = os.path.join(self.proj_dir, "reads2")
        loc_dir = os.path.join(self.proj_dir_loc, "reads2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def variants_dir(self):
        return os.path.join(self.proj_dir, "variants")

    @property
    def metrics2_dir(self):
        loc_dir = os.path.join(self.proj_dir, "metrics2")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def redis_dir(self):
        loc_dir = os.path.join(self.proj_dir, "redis")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def sc_qc_dir(self):
        return os.path.join(self.proj_dir, "sc_qc")


    @property
    def pysrc_dir(self):
        return os.path.join(self.proj_dir, "pysrc")

    @property
    def supl_dir(self):
        return os.path.join(self.proj_dir, "supl")

    @property
    def naseq_ini(self):
        return os.path.join(self.conf_dir, "naseq.ini")

    @property
    def blast_dbs_dir(self):
        return os.path.join(self.raglab_install_home, "igenomes/blast_dbs")

    @property
    def blast_contam_fa(self):
        return os.path.join(self.blast_dbs_dir, "contam.fa")

    @property
    def blast_contam_subjects_csv(self):
        return os.path.join(self.blast_dbs_dir, "subjects.csv")


    @property
    def valid_smps_csv(self):
        return os.path.join(self.conf_dir, "valid_smps.csv")

    @property
    def invalid_smps_csv(self):
        return os.path.join(self.conf_dir, "invalid_smps.csv")


    @property
    def contig_idx(self):
        return int(self.__contig_idx)

    @contig_idx.setter
    def contig_idx(self, val):
        self.__contig_idx = val

    @property
    def contig_name(self):
        return self.contig_names.iloc[self.contig_idx, 1]

    @property
    def nb_contigs(self):
        return self.contig_names.shape[0]


# ---------------------- ASSEMBLY

    @property
    def ref_gen_assembly(self):
        return self.__ref_gen_assembly

    @ref_gen_assembly.setter
    def ref_gen_assembly(self, val):
        self.__ref_gen_assembly = val


    @property
    def ref_gen_assembly_dir(self):
        return os.path.join(self.raglab_install_home,
                            "igenomes/%(ref_assembly)s"
                            % {"ref_assembly": self.ref_gen_assembly})


# ------------------------------  genome related
    @property
    def ref_gen_sequence_dir(self):
        loc_dir = os.path.join(self.ref_gen_assembly_dir, "sequence")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_gen_chrom_dir(self):
        loc_dir = os.path.join(self.ref_gen_sequence_dir, "chrom")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_gen_genome_fa_dir(self):
        loc_dir = os.path.join(self.ref_gen_sequence_dir, "genome_fa")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir



    @property
    def ref_gen_genome_fa(self):
        return os.path.join(self.ref_gen_genome_fa_dir, "%s.fa" % self.ref_gen_assembly)

    @property
    def ref_gen_genome_dict(self):
        return os.path.join(self.ref_gen_genome_fa_dir, "%s.dict" % self.ref_gen_assembly)

    @property
    def ref_gen_genome_fai(self):
        return os.path.join(self.ref_gen_genome_fa_dir, "%s.fa.fai" % self.ref_gen_assembly)

    @property
    def ref_gen_genome_chrominfo(self):
        return os.path.join(self.ref_gen_genome_fa_dir, "%s.chrominfo.txt" % self.ref_gen_assembly)

    @property
    def ref_gen_bwa_ix_dir(self):
        loc_dir = os.path.join(self.ref_gen_sequence_dir, "bwa_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_gen_bwa_ix_genome_fa(self):
        return os.path.join(self.ref_gen_bwa_ix_dir, "%s.fa" % self.ref_gen_assembly)

    @property
    def ref_gen_bt2_ix_dir(self):
        loc_dir = os.path.join(self.ref_gen_sequence_dir, "bt2_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_gen_bt2_ix_genome_fa(self):
        return os.path.join(self.ref_gen_bt2_ix_dir, "%s.fa" % self.ref_gen_assembly)

    @property
    def ref_gen_bt2_ix(self):
        return os.path.join(self.ref_gen_bt2_ix_dir, "%s" % self.ref_gen_assembly)

    @property
    def ref_gen_bbmap_ix_dir(self):
        loc_dir = os.path.join(self.ref_gen_sequence_dir, "bbmap_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_rrna_dir(self):
        loc_dir = os.path.join(self.ref_gen_assembly_dir, "rrna")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_rrna_fa(self):
        return os.path.join(self.ref_rrna_dir, "%s_rrna.fa" % self.ref_gen_assembly)

# ----------------------------------      Transcriptome related

    @property
    def ref_transcr(self):
        return self.__ref_transcr

    @ref_transcr.setter
    def ref_transcr(self, val):
        self.__ref_transcr = val



    @property
    def ref_all_transcr_dir(self):
        loc_dir = os.path.join(self.ref_gen_assembly_dir, "all_transcr")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_transcr_dir(self):
        loc_dir = os.path.join(self.ref_all_transcr_dir, self.ref_transcr)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_transcr_bt2_ix_dir(self):
        loc_dir = os.path.join(self.ref_transcr_dir, "bt2_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_transcr_hisat2_ix_dir(self):
        loc_dir = os.path.join(self.ref_transcr_dir, "hisat2_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def ref_transcr_star_ix_dir(self):
        loc_dir = os.path.join(self.ref_transcr_dir, "star_ix")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_transcr_h5(self):
        return os.path.join(self.ref_transcr_dir, "%s.h5" % self.ref_transcr)

    @property
    def genes_tracking_ids_csv(self):
        return os.path.join(self.ref_transcr_dir, "%s_genes_tracking_ids.csv" % self.ref_transcr)

    @property
    def isofs_tracking_ids_csv(self):
        return os.path.join(self.ref_transcr_dir, "%s_isofs_tracking_ids.csv" % self.ref_transcr)

    @property
    def ref_transcr_gff3(self):
        return os.path.join(self.ref_transcr_dir, "%s.gff3" % self.ref_transcr)

    @property
    def ref_transcr_bed12(self):
        return os.path.join(self.ref_transcr_dir, "%s.bed" % self.ref_transcr)

    @property
    def ref_transcr_nz_bed12(self):
        return os.path.join(self.ref_transcr_dir, "%s.nz.bed" % self.ref_transcr)

    @property
    def ref_transcr_bed3(self):
        return os.path.join(self.ref_transcr_dir, "%s.bed" % self.ref_transcr)

    @property
    def ref_transcr_mer_bed3(self):
        return os.path.join(self.ref_transcr_dir, "%s.mer.bed" % self.ref_transcr)

    @property
    def ref_transcr_exn_gtf(self):
        return os.path.join(self.ref_transcr_dir, "%s.exn.gtf" % self.ref_transcr)


    @property
    def ref_gen_transcr_gtf(self):
        return os.path.join(self.ref_transcr_bt2_ix_dir, "%s.gtf" % self.ref_transcr)

    # this transcriptome fasta is made by bowtie, with numbers
    @property
    def ref_bt2_transcr_fa(self):
        return os.path.join(self.ref_transcr_bt2_ix_dir, "%s.fa" % self.ref_transcr)

    # this transcriptome fasta is mate by cufflinks/gffread with transcript ids as names
    @property
    def ref_transcr_fa(self):
        return os.path.join(self.ref_transcr_dir, "%s.fa" % self.ref_transcr)


    @property
    def ref_transcr_kallisto_kix(self):
        return os.path.join(self.ref_transcr_dir, "%s.kix" % self.ref_transcr)

    @property
    def ref_gen_transcr_ix(self):
        return os.path.join(self.ref_transcr_bt2_ix_dir, "%s" % self.ref_transcr)

    @property
    def hisat_transcr_ix(self):
        return os.path.join(self.ref_transcr_hisat2_ix_dir, self.ref_transcr_hisat, "%s" % self.ref_transcr_hisat)





# -------------------------   Target related

    @property
    def ref_gen_target(self):
        return self.__ref_gen_target

    @ref_gen_target.setter
    def ref_gen_target(self, val):
        self.__ref_gen_target = val

    @property
    def ref_gen_all_targ_dir(self):
        loc_dir = os.path.join(self.ref_gen_assembly_dir, "all_targ")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def ref_gen_targ_dir(self):
        loc_dir = os.path.join(self.ref_gen_all_targ_dir, self.ref_gen_target)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def target_karyo_bed(self):
        return os.path.join(self.ref_gen_targ_dir, "%s.karyo.bed" % self.ref_gen_target)

    @property
    def target_contigs_csv(self):
        return os.path.join(self.ref_gen_targ_dir, "%s.contigs.csv" % self.ref_gen_target)

    @property
    def target_regions_csv(self):
        return os.path.join(self.ref_gen_targ_dir, "%s.regions.csv" % self.ref_gen_target)


# ------------------------ Supplementary related

    @property
    def bulk_c3p_gtf(self):
        return os.path.join(self.supl_dir, "bulk_c3p.gtf")





#  -----------------------------

    # @property
    # def targ_dir(self):
    #     loc_dir = os.path.join(self.proj_dir, "targ")
    #     if not os.path.exists(loc_dir):
    #         os.makedirs(loc_dir)
    #     return loc_dir

    @property
    def contig_bed_dir(self):
        loc_dir = os.path.join(self.ref_gen_targ_dir, "contig_bed")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def region_bed_dir(self):
        loc_dir = os.path.join(self.ref_gen_targ_dir, "region_bed")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir



    # @property
    # def target_karyo_bed(self):
    #     return os.path.join(self.targ_dir, "target_karyo.bed")


    # -----------------------        RNA related  ---------------------------



# -------------------------- originals
    @property
    def ref_gen_orig_dir(self):
        loc_dir = os.path.join(self.ref_gen_assembly_dir, "orig")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir


    @property
    def ref_gen_orig_genome_fa(self):
        return os.path.join(self.ref_gen_orig_dir, "%s.fa" % self.ref_gen_assembly)

    @property
    def ref_orig_transcr_gtf(self):
        return os.path.join(self.ref_gen_orig_dir, "%s.gtf" % self.ref_transcr)

    @property
    def ref_gen_orig_contigs_csv(self):
        return os.path.join(self.ref_gen_orig_dir, "%s.contigs.csv" % self.ref_gen_assembly)

    @property
    def target_orig_bed(self):
        return os.path.join(self.ref_gen_orig_dir, "%s.bed" % self.ref_gen_target)

    @property
    def target_mod1_bed(self):
        return os.path.join(self.ref_transcr_dir, "%s.mod1.bed" % self.ref_gen_target)

    @property
    def target_mod2_bed(self):
        return os.path.join(self.ref_gen_orig_dir, "%s.mod2.bed" % self.ref_gen_target)

    # this original target bed file should be already karyotipically ordered
    def load_target_orig_bed(self):

        # print("self.target_orig_bed: ", self.target_orig_bed)
        # self.ref_transcr_bed3
        self.orig_regions = pd.read_csv(self.target_orig_bed,
                          sep='\t', header=None
                          ,index_col=[0]
                          ,usecols=[0, 1, 2]
                          ,names=["contig", "start", "stop"]
                          )

        print("self.orig_regions: ", self.orig_regions)

    def load_target_karyo_bed(self):

        # print("self.target_karyo_bed: ", self.target_karyo_bed)

        try:
            self.all_regions = pd.read_csv(self.target_karyo_bed,
                              sep='\t', header=None
                              ,index_col=[0]
                              ,usecols=[0, 1, 2]
                              ,names=["contig", "start", "stop"]
                              )
        except IOError:
            print("MISSING target_karyo_bed, Please generate the Targets")
        else:
            pass
            # print("self.all_regions: \n", self.all_regions)
        finally:
            print("in finally")




    def karyoorder_target_orig_bed(self):

        print("self.target_orig_bed: ", self.target_orig_bed)
        # self.ref_transcr_bed3
        orreg = pd.read_csv(self.target_orig_bed,
                          sep='\t', header=None
                          # ,index_col=[0]
                          ,usecols=[0, 1, 2]
                          ,names=["contig", "start", "stop"]
                          )

        print("orreg: ", orreg)

        # print("self.ref_gen_orig_contigs_csv: ", self.ref_gen_orig_contigs_csv)
        #load_orig_contigs
        orctg = pd.read_csv(self.ref_gen_orig_contigs_csv,
                          sep=',', header = 0
                          #,index_col=[0]
                          ,usecols=[0, 1]
                          ,names=["idx", "contig"]
                          )
        print("orctg: ", orctg)
        nb_contigs = orctg.shape[0]

        print("nb_contigs: ", nb_contigs)

        custom_dict = pd.Series(orctg.idx.values, index=orctg.contig).to_dict()
        print("custom_dict: ", custom_dict)

        orreg['rank'] = orreg['contig'].map(custom_dict)

        # orreg.sort(columns=['rank', 'start', 'stop'], inplace=True)
        # orreg.reset_index(drop=True)
        # this puts the right index but does not move the lines
        # orreg.drop(labels=['rank'], axis=1, inplace=True)
        # move the lines in index order
        # orreg = orreg.sort_index()

        orreg.sort_index(by=['rank', 'start', 'stop'], ascending=[True, True, True], inplace=True)
        orreg.drop(labels=['rank'], axis=1, inplace=True)

        orreg.reset_index(drop=True, inplace=True)

        print("-----------------orreg: \n", orreg)

        orreg.to_csv(self.target_karyo_bed, sep='\t', header=False, index=False)



    def spread_karyo_bed(self):

        cmd = r"""bedtools makewindows -b %(input_bed)s -w 1000 > %(spread_bed)s
""" % {"input_bed": self.target_karyo_bed,
       "spread_bed": self.target_mod1_bed}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.ref_gen_orig_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)

    def gen_target_contigs(self):

        target_df = self.all_regions
        # group by index
        gr_target_df = target_df.groupby(level=0, sort=False).first()

        # drop other columns form target_bed , "start", "stop"
        gr_target_df.drop('start', axis=1, inplace=True)
        gr_target_df.drop('stop', axis=1, inplace=True)
        gr_target_df = gr_target_df.reset_index()

        # save contigs list
        gr_target_df.to_csv(self.target_contigs_csv,
                          sep='\t', header = True
                          , index = True
                          , index_label = "idx"
                          )



    def load_contig_names(self):
        print("in load_contig_names")

        # print("self.target_contigs_csv: ", self.target_contigs_csv)
        try:
            self.contig_names = pd.read_csv(self.target_contigs_csv,
                          sep='\t', header = 0
                          #,index_col=[0]
                          ,usecols=[0, 1]
                          ,names=["idx", "contig"]
                          )
        except IOError:
            print("MISSING contigs_csv, Please generate the Targets")
        else:
            pass
            # print("self.contig_names: \n", self.contig_names)
        finally:
            print("in finally")


    # generates the contig beds
    # chunk size is now by chromosome
    # this ensures that each regions comes from only one chromosome
    # this allow to calculate one unique span for the region
    # usfull to speed-up retrieval using .bai indexes with mpileup -r option
    def gen_target_beds(self, chunk_size):

        print("in gen_target_beds....")
        print("chunk_size: ", chunk_size)
        # self.gen_contigs()
        self.load_contig_names()



        all_regions = self.all_regions.reset_index()
        print("regions: \n", all_regions)

        # this is the global index of the regions

        i = 0
        nb_regions = 0
        reg_contig_names = []
        reg_min_start = []
        reg_max_stop = []

        for contig_idx in range(self.nb_contigs):

        # for contig_idx in range(2):
            self.contig_idx = contig_idx

            print("contig_idx: ", self.contig_idx, "contig_name: ", self.contig_name)
            #filter bed indexes
            #regex = r"^600\d{3}$"
            #contig_bed = rpt[rpt['STK_ID'].str.contains(r'^600\d{3}$' % contig)]

            # print("orig_regions: ", orig_regions)
            contig_bed = all_regions[(all_regions['contig'] == self.contig_name)]


            # print("contig_bed: ", contig_bed)
            # os.path.join(self.conf.contig_bed_dir, "%s.bed" % self.contig_name)
            # self.ref_transcr_mer_bed3,
            contig_bed.to_csv(os.path.join(self.contig_bed_dir, "%s.bed" % self.contig_name), sep='\t',
                              header=False,
                              index=False
                              #, index_label = "idx"
                              )

            # This ensures the index is starting over for chopping
            contig_bed.reset_index(drop=True, inplace=True)

            # nb regions/bed lines, in the contig (not globally like before)
            nb_regions = contig_bed.shape[0]
            print("nb_regions: ", nb_regions)
            # old gen_region_beds go here
            # it chops into the contig bed, not into the global bed as before
            reg_beds = (contig_bed.loc[pos:min(pos + chunk_size -1, nb_regions-1), "contig":"stop"] for pos in range(0, nb_regions, chunk_size))

            for reg in reg_beds:

                print("i, size", i,  reg.shape)

                if reg.shape[0] > 0:

                    # add regions span for index optimisation in samtools
                    reg_contig_names.append(self.contig_name)
                    reg_min_start.append(reg['start'].min())
                    reg_max_stop.append((reg['stop'].max()))

                    reg.to_csv(os.path.join(self.region_bed_dir, "%s.bed" % i),
                               sep='\t', header=False
                               , index=False
                               , index_label = "idx"
                               )
                    i += 1
                else:
                    print("-------------ERROR CHOPPING")
        #
        nb_beds = i
        #
        gr_regions = pd.DataFrame.from_items([('region', range(nb_beds)), ("contig", reg_contig_names),
                                              ('start', reg_min_start),('stop', reg_max_stop)])
        #gr_contigs = gr_contigs.sort(['contig'], ascending=[1]).reset_index()
        #gr_contigs.drop('index', axis=1, inplace=True)
        #save contigs list
        gr_regions.to_csv(self.target_regions_csv,
                          sep='\t', header = True
                          , index = True
                          , index_label="idx"
                          )




        #cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum' : np.sum,'fpkm_mean' : np.mean})
    #gr_chrs = gr_target_df["fpkm"].agg({'fpkm_sum' : np.sum})

    # takes the final target bed file, chunks it and generates the regions beds
    # as well as the csv index
    def gen_region_beds_old(self, chunk_size):


        regions = self.all_regions.reset_index()
        nb_regions = regions.shape[0]

        print("nb_regions: ", nb_regions, "chunk_size: ", chunk_size)
        print("regions: \n", regions)

        #rc.set("shutdown",0)
        #rc.set("active_queue","call_hc")
        reg_beds = (regions.loc[pos:min(pos + chunk_size -1, nb_regions-1), "contig":"stop"] for pos in range(0, nb_regions, chunk_size))

        i=0
        for reg in reg_beds:
            # print i,reg

            reg.to_csv(os.path.join(self.region_bed_dir, "%s.bed" % i),
                              sep='\t', header=False
                              , index=False
                              #, index_label = "idx"
                              )

            i += 1

        nb_beds = i
        #
        gr_regions = pd.DataFrame.from_items([('region', range(nb_beds))])
        #gr_contigs = gr_contigs.sort(['contig'], ascending=[1]).reset_index()
        #gr_contigs.drop('index', axis=1, inplace=True)
        #save contigs list
        gr_regions.to_csv(self.target_regions_csv,
                          sep='\t', header = True
                          , index = True
                          , index_label="idx"
                          )


    def load_regions(self):

        # if not os.path.exists(self.regions_csv):
        #     self.gen_region_beds(nb_regions)

        try:
            self.region_names = pd.read_csv(self.target_regions_csv,
                          sep='\t', header = 0
                          ,index_col=[0]
                          ,usecols=[0, 1, 2, 3, 4]
                          ,names=["idx", "region", "contig", "start", "stop"]
                          )
        except IOError:
            print("MISSING REGIONS")
        else:
            pass
            # print("self.region_names: \n", self.region_names)
        finally:
            print("in finally")

# ------------------------------------------------------

    def scan_local(self):
        print("in scan_local...")
        # local project
        # self.depl_dir = os.path.join(self.proj_dir, "paired_reads")
        print("proj_dir: ", self.proj_dir)
        return self

    def set_proj_dirs(self):
        pass


    def set_proj_files_path_names(self):
        pass

    def load_config(self):
        # load config file
        self.naseq_config.read(self.naseq_ini)



    @property
    def server_type(self):

        project_type_str = self.naseq_config.get("Project", "Type")
        print("project_type: --------------------------", project_type_str)
        if project_type_str == "RNA":
            project_type = NaSeqServerType.rna_seq
        elif project_type_str == "DNA":
            project_type = NaSeqServerType.dna_seq
        elif project_type_str == "DAT":
            project_type = NaSeqServerType.dat_seq
        else:
            project_type = NaSeqServerType.dat_seq

        return project_type

    @property
    def proj_abbrev(self):

        return self.naseq_config.get("Project", "Abbrev")


    # @property
    # def next_queue_dict(self):
    #
    #     return dict(extract_one_read_group='wait')

    @property
    def next_queue_dict(self):

        return dict(dat=self.dat_nqd,
                    exomes=self.exomes_nqd,
                    amplicons=self.amplicons_nqd,
                    haloplex_hs=self.haloplex_hs_nqd,
                    rna=self.rna_nqd,
                    rna2=self.rna2_nqd,
                    rna_only=self.rna_only_nqd,
                    na_comb2=self.na_comb2_nqd,
                    na_modseq=self.na_modseq_nqd)[self.next_queue_dict_name]

    @property
    def qgraph_dict(self):
        return dict(dat=self.dat_qdd,
                    na_comb2=self.na_comb2_qdd,
                    na_modseq=self.na_modseq_qdd)[self.qgraph_dict_name]





    # task graph traversal
    @property
    def dat_qdd(self):
        return OrderedDict([("htchip_demul_col", {"score": 0.0, "deps": []}),
                            ("extract_one_read_group", {"score": 0.0, "deps": ["htchip_demul_col"]})
                            ])

    @property
    def na_comb2_qdd(self):
        return OrderedDict([("spln_calc", {"score": 0.0, "deps": []}),
                            ("grind_lanes", {"score": 0.0, "deps": ["spln_calc"]}),
                            ("merge_smpgrps_exp", {"score": 0.0, "deps": ["grind_lanes"]}),
                            ("wiggle", {"score": 0.0, "deps": ["merge_smpgrps_exp"]}),
                            ("qnsort", {"score": 0.0, "deps": ["merge_smpgrps_exp"]}),
                            ("raw_counts", {"score": 0.0, "deps": ["qnsort"]}),
                            ("readcnt_rprt", {"score": 0.0, "deps": ["raw_counts"]}),
                            ("fpkm_known", {"score": 0.0, "deps": ["merge_smpgrps_exp"]}),
                            ("rnaseqc_ini_rprt", {"score": 0.0, "deps": ["grind_lanes"]}),
                            ("grind_smps_gtk", {"score": 0.0, "deps": ["grind_lanes"]}),
                            ("grind_smps_oth", {"score": 0.0, "deps": ["grind_lanes"]}),
                            ("merge_smpgrps_gtk", {"score": 0.0, "deps": ["grind_smps_gtk"]}),
                            ("call_hc", {"score": 0.0, "deps": ["merge_smpgrps_gtk"]}),
                            ("cat_hc", {"score": 0.0, "deps": ["call_hc"]}),
                            ("cat_hc_bam", {"score": 0.0, "deps": ["call_hc"]}),
                            ("genotype_gvcf_contig", {"score": 0.0, "deps": ["cat_hc"]}),
                            ("cat_hc_contigs", {"score": 0.0, "deps": ["genotype_gvcf_contig"]}),
                            ("merge_smpgrps_oth", {"score": 0.0, "deps": ["grind_smps_oth"]}),
                            ("call_mp", {"score": 0.0, "deps": ["merge_smpgrps_oth"]}),
                            ("cat_mp", {"score": 0.0, "deps": ["call_mp"]}),
                            ("call_ug", {"score": 0.0, "deps": ["merge_smpgrps_gtk"]}),
                            ("cat_ug", {"score": 0.0, "deps": ["call_ug"]}),
                            ("comb_vars_rprt", {"score": 0.0, "deps": ["cat_hc_contigs", "cat_mp", "cat_ug"]}),
                            ("deploy_web", {"score": 0.0, "deps": ["readcnt_rprt", "rnaseqc_ini_rprt", "comb_vars_rprt"]}),
                            ])

    @property
    def na_modseq_qdd(self):
        return OrderedDict([("pipe_ini", {"score": 0.0, "deps": []}),
                            ("fq2unal_parq_ini", {"score": 0.0, "deps": ["pipe_ini"]}),
                            ("fq2unal_parq", {"score": 0.0, "deps": ["fq2unal_parq_ini"]}),
                            ("fq2unal_parq_fin", {"score": 0.0, "deps": ["fq2unal_parq"]}),
                            ("clasif_seqs", {"score": 0.0, "deps": ["fq2unal_parq_fin"]}),
                            ("clasifseqs2fq", {"score": 0.0, "deps": ["clasif_seqs"]}),
                            ("align_regal", {"score": 0.0, "deps": ["clasifseqs2fq"]}),
                            ("segs2cnts", {"score": 0.0, "deps": ["align_regal"]}),
                            ("gncnts_unpiv", {"score": 0.0, "deps": ["segs2cnts"]})
                            ])


    # tasks range ressources
    @property
    def tsk_rng_rsc_dict(self):
        return {"spln_calc": {"cor_tsk_min": 1,
                              "cor_tsk_max": 16,
                              "mem_tsk_min": 3000},
                "grind_lanes": {"cor_tsk_min": 1,
                                "cor_tsk_max": 16,
                                "mem_tsk_min": 3000}
                }

    # tasks node ressources
    @property
    def tsk_nde_rsc_dict(self):

        return OrderedDict([("pipe_ini", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("fq2unal_parq_ini", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("fq2unal_parq", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("fq2unal_parq_fin", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("clasif_seqs", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("clasifseqs2fq", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("align_regal", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("segs2cnts", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("gncnts_unpiv", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("htchip_demul_col", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("extract_one_read_group", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("spln_calc", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("grind_lanes", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("merge_smpgrps_exp", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("wiggle", {"tsk_nde": 8, "cor_tsk": 1, "mem_tsk": 3000}),
                            ("qnsort", {"tsk_nde": 8, "cor_tsk": 1, "mem_tsk": 3000}),
                            ("raw_counts", {"tsk_nde": 8, "cor_tsk": 1, "mem_tsk": 3000}),
                            ("fpkm_known", {"tsk_nde": 8, "cor_tsk": 1, "mem_tsk": 3000}),
                            ("rnaseqc_ini_rprt", {"tsk_nde": 3, "cor_tsk": 4, "mem_tsk": 7000}),
                            ("readcnt_rprt", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("grind_smps_gtk", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("grind_smps_oth", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("merge_smpgrps_gtk", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("call_hc", {"tsk_nde": 12, "cor_tsk": 1, "mem_tsk": 2000}),
                            ("cat_hc", {"tsk_nde": 2, "cor_tsk": 6, "mem_tsk": 10000}),
                            ("cat_hc_bam", {"tsk_nde": 2, "cor_tsk": 6, "mem_tsk": 10000}),
                            ("genotype_gvcf_contig", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 17000}),
                            ("cat_hc_contigs", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("call_mp", {"tsk_nde": 12, "cor_tsk": 1, "mem_tsk": 2000}),
                            ("cat_mp", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("call_ug", {"tsk_nde": 12, "cor_tsk": 1, "mem_tsk": 2000}),
                            ("cat_ug", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("merge_smpgrps_oth", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("comb_vars_rprt", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000}),
                            ("deploy_web", {"tsk_nde": 1, "cor_tsk": 6, "mem_tsk": 20000})])



    @property
    def exomes_nqd(self):
        return dict(trim_lane_trimmomatic='align_lane_bwa',
                    stats_spln_blast='align_lane_bwa',
                    align_lane_bwa='sort_lane',
                    sort_lane='stats_spln_insert',
                    stats_spln_insert='merge_lanes',
                    merge_lanes='stats_smp_targ',
                    stats_smp_targ='stats_smpgrp_depth',
                    stats_smp_depth='stats_qc',
                    stats_qc='indel_realigner',
                    indel_realigner='cat_realigned',
                    cat_realigned='sort_realigned',
                    sort_realigned='base_recalibrator',
                    base_recalibrator='call_hc',
                    call_hc='cat_hc',
                    cat_hc='cat_hc_bam',
                    cat_hc_bam='genotype_gvcf_contig',
                    genotype_gvcf_contig='cat_hc_contigs',
                    cat_hc_contigs='call_mp',
                    call_mp='cat_mp',
                    cat_mp='call_ug',
                    call_ug='cat_ug',
                    cat_ug='rprts',
                    rprts='finished')


    @property
    def amplicons_nqd(self):
        return dict(trim_lane_trimmomatic='align_lane_bwa',
                    stats_spln_blast='align_lane_bwa',
                    align_lane_bwa='sort_lane',
                    sort_lane='stats_spln_insert',
                    stats_spln_insert='merge_lanes',
                    merge_lanes='indel_realigner',

                    stats_smp_targ='stats_smpgrp_depth',

                    indel_realigner='cat_realigned',
                    cat_realigned='sort_realigned',
                    sort_realigned='base_recalibrator',

                    base_recalibrator='merge_smpgrps',

                    merge_smpgrps='stats_smp_targ',


                    stats_smp_depth='stats_qc',
                    stats_qc='call_hc',

                    call_hc='cat_hc',
                    cat_hc='cat_hc_bam',
                    cat_hc_bam='genotype_gvcf_contig',
                    genotype_gvcf_contig='cat_hc_contigs',

                    cat_hc_contigs='call_mp',
                    call_mp='cat_mp',
                    cat_mp='call_ug',
                    call_ug='cat_ug',
                    cat_ug='rprts',
                    rprts='finished')

    @property
    def haloplex_hs_nqd(self):
        return dict(stats_smp_targ='indel_realigner',

                    indel_realigner='cat_realigned',
                    cat_realigned='sort_realigned',
                    sort_realigned='base_recalibrator',

                    base_recalibrator='merge_smpgrps',

                    merge_smpgrps='stats_smpgrp_depth',

                    stats_smp_depth='stats_qc',
                    stats_qc='call_hc',
                    call_hc='cat_hc',
                    cat_hc='cat_hc_bam',
                    cat_hc_bam='genotype_gvcf_contig',
                    genotype_gvcf_contig='cat_hc_contigs',

                    cat_hc_contigs='call_mp',
                    call_mp='cat_mp',
                    cat_mp='call_ug',
                    call_ug='cat_ug',
                    cat_ug='rprts',
                    rprts='finished')


    @property
    def rna_nqd(self):

        return dict(trim_lane_trimmomatic='align_lane_tophat',
                    align_lane_tophat='merge_lanes',
                    merge_lanes='wiggle',
                    wiggle='finished',
                    merge_lanes_rnaseqc='fpkm_known',
                    fpkm_known='c3p_gtf_gen',
                    fpkm_rabt='c3p_gtf_gen',
                    fpkm_denovo='c3p_gtf_gen',
                    c3p_gtf_gen='qnsort',
                    qnsort='raw_counts',
                    raw_counts='extract_merged_bam_to_fastqs',
                    extract_merged_bam_to_fastqs='kallisto_smp_transcr',
                    kallisto_smp_transcr='rseqc',
                    rseqc='rprts',
                    rprts='finished')

    @property
    def rna2_nqd(self):


        return dict(spln_calc_rna='grind_lanes_spl_clp',
                    grind_lanes_spl_clp='indel_realigner',

                    indel_realigner='grind_smps_gtk',
                    grind_smps='finished',
                    merge_smpgrps='finished',
                    call_hc='cat_hc',
                    cat_hc='cat_hc_bam',
                    cat_hc_bam='genotype_gvcf_contig',
                    genotype_gvcf_contig='cat_hc_contigs',
                    cat_hc_contigs='finished',

                    call_mp='cat_mp',
                    cat_mp='finished',
                    call_ug='finished',
                    cat_ug='finished',
                    rprts='finished')



    @property
    def rna_only_nqd(self):
        return dict(spln_calc='grind_lanes',
                    grind_lanes='merge_smpgrps',
                    merge_smpgrps = 'wiggle',
                    wiggle = 'qnsort',
                    qnsort = 'raw_counts',
                    raw_counts = 'fpkm_known',
                    fpkm_known='fpkm_rabt',
                    fpkm_rabt='fpkm_denovo',
                    fpkm_denovo='rna_rprts',
                    rnaseqc_ini_rprt='wait',
                    rna_rprts='finished'
                    )






    @property
    def na_comb2_nqd(self):
        return dict(spln_calc='grind_lanes',
                    grind_lanes='indel_realigner',
                    indel_realigner='grind_smps_gtk',
                    # we keep bams until this stage
                    grind_smps='merge_smpgrps',

                    stats_smp_targ='stats_smpgrp_depth',

                    merge_smpgrps='wiggle',
                    # we keep this stage bams only for the subsequent analysis (45 days)

                    wiggle='qnsort',
                    qnsort='raw_counts',

                    raw_counts='rnaseqc_ini_rprt',
                    rnaseqc_ini_rprt='call_hc',
                    smpgrp_calc='stats_qc',
                    stats_qc='call_hc',
                    call_hc='cat_hc',
                    cat_hc='cat_hc_bam',
                    cat_hc_bam='genotype_gvcf_contig',
                    genotype_gvcf_contig='cat_hc_contigs',
                    cat_hc_contigs='call_mp',
                    call_mp='cat_mp',
                    cat_mp='call_ug',
                    call_ug='cat_ug',
                    cat_ug='rprts',
                    # fpkm_known='fpkm_rabt',
                    # fpkm_rabt='fpkm_denovo',
                    # fpkm_denovo='rnaseqc_ini_rprt',
                    rprts='finished'
                    )





