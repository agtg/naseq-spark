from collections import OrderedDict

import subprocess
import optparse
import re
import os
import csv
import sys, random, itertools
# import HTSeq
import shutil

# import pysam
import numpy as np
import pandas as pd
import shlex
import jinja2
import collections

import imp
import importlib



pypack = os.environ['NASEQ_PYPACK']

import sys

# add package path to system path
sys.path.append("%s/naseqpack" % (pypack))

scan_config_dnaseq = imp.load_source('scan_config_dnaseq', "%s/naseqpack/scan_config_dnaseq.py" % (pypack))
ScanConfigDnaSeq = scan_config_dnaseq.ScanConfigDnaSeq

# from scan_config_dnaseq import ScanConfigDnaSeq

from naseq_utils import *

redis_controller = imp.load_source('redis_controller', "%s/naseqpack/redis_controller.py" % (pypack))
from redis_controller import RedisController

# naseq_servers = importlib.load_source('naseq_servers', "%s/naseqpack/naseq_servers.py" % (pypack) )
# import naseq_servers.NaseqServers


# from treedict import TreeDict
import redis
import time

import socket
import json

import itertools

from mixin import Mixin, mixin


# with composition
# @mixin(NaseqServers)
class NaSeqPip(object):
    def __init__(self):
        self.smp_idx = None
        self.region_idx = None

        self.spln_idx = 0
        self.spln_regal=None
        self.rprt_idx = None
        self.smp_idx = None

        self.spln_hplex_qc = {}

        # These definitely need instantiation
        # they are used in a derived class
        self.cls_idx = 0
        self.min_base = 0

        self.reads_dir = None

    @property
    def reads_dir(self):
        return self.__reads_dir

    @reads_dir.setter
    def reads_dir(self, val):
        self.__reads_dir = val


    @property
    def smp_idx(self):
        return int(self.__smp_idx)

    @smp_idx.setter
    def smp_idx(self, val):
        self.__smp_idx = val

    @property
    def smp_skip(self):
        return self.conf.sample_names.iloc[self.smp_idx, 0]

    @property
    def smp_name(self):
        return self.conf.sample_names.iloc[self.smp_idx, 1]

    @property
    def nb_samples(self):
        return self.conf.sample_names.shape[0]

    # smp groups
    @property
    def smpgrp_idx(self):
        return int(self.__smpgrp_idx)

    @smpgrp_idx.setter
    def smpgrp_idx(self, val):
        self.__smpgrp_idx = val

    @property
    def smpgrp_skip(self):
        return self.conf.sample_grps.iloc[self.smpgrp_idx, 0]

    @property
    def smpgrp_name(self):
        return self.conf.sample_grps.iloc[self.smpgrp_idx, 1]
        # return self.conf.sample_grps.loc[self.smpgrp_idx, "smpgrp_name"]


    @property
    def nb_smpgrps(self):
        return self.conf.sample_grps.shape[0]

    @property
    def noarg_idx(self):
        return int(self.__noarg_idx)

    @noarg_idx.setter
    def noarg_idx(self, val):
        self.__noarg_idx = val

    @property
    def nb_noargs(self):
        return 1

    @property
    def rprt_idx(self):
        return int(self.__rprt_idx)

    @rprt_idx.setter
    def rprt_idx(self, val):
        self.__rprt_idx = val

    @property
    def nb_rprts(self):
        return int(self.__nb_rprts)

    @nb_rprts.setter
    def nb_rprts(self, val):
        self.__nb_rprts = val

    @property
    def cls_idx(self):
        return int(self.__cls_idx)

    @cls_idx.setter
    def cls_idx(self, val):
        self.__cls_idx = val

    @property
    def min_base(self):
        return int(self.__min_base)

    @min_base.setter
    def min_base(self, val):
        self.__min_base = val

    # ########### HTchip indexing

    @property
    def htchipcol_idx(self):
        return int(self.__htchipcol_idx)

    @htchipcol_idx.setter
    def htchipcol_idx(self, val):
        self.__htchipcol_idx = val

    @property
    def htchipcol_idn(self):
        return self.conf.htchip_colnames_df.loc[self.htchipcol_idx, 'col_idn']

    @property
    def htchipcol_name(self):
        return self.conf.htchip_colnames_df.loc[self.htchipcol_idx, 'col_name']

    # ########################################

    @property
    def hivef_dir_glo(self):
        loc_dir = os.path.join(self.conf.srvf_dir_glo, "hivef")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def spln_idx(self):
        return int(self.__spln_idx)

    @spln_idx.setter
    def spln_idx(self, val):
        self.__spln_idx = val

    @property
    def nb_splns(self):
        return self.conf.sample_lanes.shape[0]

    @property
    def spln_row(self):
        # print("----------:", self.conf.sample_lanes.iloc[[self.spln_idx]])
        return self.conf.sample_lanes.iloc[[self.spln_idx]]

    @property
    def spln_skip(self):
        return self.spln_row.iloc[0, 0]

    @property
    def spln_smp(self):
        return self.spln_row.iloc[0, 1]

    @property
    def spln_run(self):
        return self.spln_row.iloc[0, 2]

    @property
    def spln_region(self):
        return self.spln_row.iloc[0, 3]

    @property
    def spln_lib(self):
        return self.spln_row.iloc[0, 4]

    @property
    def spln_runreg(self):
        # return r"""run%s_%s""" % (self.spln_run, self.spln_region)
        return r"""%s_%s""" % (self.spln_run, self.spln_region)

    @property
    def spln_regal(self):
        return self.__spln_regal

    @spln_regal.setter
    def spln_regal(self, val):
        self.__spln_regal = val

    # ----------------------- FASTQ - in raw_reads
    @property
    def spln_fastq_smp_dir(self):
        # this_dir = os.path.join(self.conf.raw_reads_dir, self.spln_smp)
        this_dir = os.path.join(self.reads_dir, self.spln_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_fastq_runreg_dir(self):
        this_dir = os.path.join(self.spln_fastq_smp_dir, self.spln_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    # ----------------- TRIM
    @property
    def spln_trim_smp_dir(self):
        this_dir = os.path.join(self.conf.reads2_dir, self.spln_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_trim_runreg_dir(self):
        this_dir = os.path.join(self.spln_trim_smp_dir, self.spln_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_trim_lib_dir(self):
        this_dir = os.path.join(self.spln_trim_runreg_dir, self.spln_lib)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    # ------------------------- ALIGN
    @property
    def spln_align_smp_dir(self):
        this_dir = os.path.join(self.conf.align2_dir, self.spln_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_smp_dir_loc(self):
        this_dir = os.path.join(self.conf.align2_dir_loc, self.spln_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_smp_dir_glo(self):
        this_dir = os.path.join(self.conf.align2_dir_glo, self.spln_smp)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_runreg_dir(self):
        this_dir = os.path.join(self.spln_align_smp_dir, self.spln_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_runreg_dir_loc(self):
        this_dir = os.path.join(self.spln_align_smp_dir_loc, self.spln_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_runreg_dir_glo(self):
        this_dir = os.path.join(self.spln_align_smp_dir_glo, self.spln_runreg)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_lib_dir(self):
        this_dir = os.path.join(self.spln_align_runreg_dir, self.spln_lib)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_lib_dir_loc(self):
        this_dir = os.path.join(self.spln_align_runreg_dir_loc, self.spln_lib)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_lib_dir_glo(self):
        this_dir = os.path.join(self.spln_align_runreg_dir_glo, self.spln_lib)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_regal_dir_glo(self):
        this_dir = os.path.join(self.spln_align_runreg_dir_glo, self.spln_regal)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def spln_align_regal_bam(self):
        return os.path.join(self.spln_align_regal_dir_glo, "%s_%s_%s.bam" % (self.spln_smp,self.spln_runreg,self.spln_regal))

        # -------------------------------------------------------

    # ############################# Trimmomatic input files
    @property
    def spln_s(self):
        return os.path.join(self.spln_fastq_runreg_dir,
                            r"""%s.%s.33.single.fastq.gz""" % (self.spln_smp, self.spln_lib))

    @property
    def spln_p1(self):
        return os.path.join(self.spln_fastq_runreg_dir, r"""%s.%s.33.pair1.fastq.gz""" % (self.spln_smp, self.spln_lib))

    @property
    def spln_p2(self):
        return os.path.join(self.spln_fastq_runreg_dir, r"""%s.%s.33.pair2.fastq.gz""" % (self.spln_smp, self.spln_lib))

    @property
    def spln_molids_fq(self):
        return os.path.join(self.spln_fastq_runreg_dir, r"""%s.%s.33.pair3.fastq.gz""" % (self.spln_smp, self.spln_lib))

    # ####################################### Trimmomatic output
    @property
    def spln_rp1(self):
        return os.path.join(self.spln_trim_lib_dir, r"""%s.pair1.fastq.gz""" % self.spln_idx)

    @property
    def spln_rp2(self):
        return os.path.join(self.spln_trim_lib_dir, r"""%s.pair2.fastq.gz""" % self.spln_idx)

    @property
    def spln_rs1(self):
        return os.path.join(self.spln_trim_lib_dir, r"""%s.sing1.fastq.gz""" % self.spln_idx)

    @property
    def spln_rs2(self):
        return os.path.join(self.spln_trim_lib_dir, r"""%s.sing2.fastq.gz""" % self.spln_idx)

    @property
    def spln_rs(self):
        return os.path.join(self.spln_trim_lib_dir, r"""%s.sing.fastq.gz""" % self.spln_idx)

    @property
    def spln_trimout_csv(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.trimout.csv""" % self.spln_idx)

    # ------------------- BWA, and other aligners

    # for dna alignement

    # ------------------------------- clipped files

    # aligned
    @property
    def spln_clp_aln_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.clp.aln.bam""" % self.spln_idx)

    # aligned
    @property
    def spln_clp_aln_srt_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.clp.aln.srt.bam""" % self.spln_idx)

    @property
    def spln_clp_rgr_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.clp.rgr.bam""" % self.spln_idx)

    # karyotipic order
    @property
    def spln_clp_krd_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.clp.krd.bam""" % self.spln_idx)

    # karyotipic order
    # @property
    # def spln_clp_srt_am(self):
    #     return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.clp.srt.bam""" % self.spln_idx)

    # fixed mate
    @property
    def spln_clp_fmt_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.clp.fmt.bam""" % self.spln_idx)

    # for rna input
    # aligned in sam format
    @property
    def spln_spl_aln_sam(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.aln.sam""" % self.spln_idx)

    # aligned
    @property
    def spln_spl_aln_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.aln.bam""" % self.spln_idx)

    # aligned
    @property
    def spln_spl_aln_srt_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.aln.srt.bam""" % self.spln_idx)

    @property
    def spln_spl_aln_srt_ai(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.aln.srt.bam.bai""" % self.spln_idx)


    # combined
    # @property
    # def spln_cmb_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln.cmb.bam""" % self.spln_idx)

    # corrected
    # @property
    # def spln_spl_cor_am(self):
    #     return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.cor.bam""" % self.spln_idx)

    # read group added for gatk
    @property
    def spln_spl_rgr_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.rgr.bam""" % self.spln_idx)

    @property
    def spln_spl_rgr_ai(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.rgr.bai""" % self.spln_idx)


    # karyotipic order
    @property
    def spln_spl_krd_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.krd.bam""" % self.spln_idx)

    # fixed mate
    @property
    def spln_spl_fmt_am(self):
        return os.path.join(self.spln_align_lib_dir_loc, r"""%s.spln.spl.fmt.bam""" % self.spln_idx)

    # Cleaned (clipped to chrom + unmapped Mapq=255)
    # this file is only one for end of lane processing (both for clipped or spliced reads)
    # after merging, dna or rna specific procedures will deduplicate and store it.
    @property
    def spln_cln_am(self):
        return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.cln.bam""" % self.spln_idx)

    @property
    def spln_cln_ai(self):
        return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.cln.bai""" % self.spln_idx)

    @property
    def spln_noncan_am(self):
        return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.noncan.bam""" % self.spln_idx)

    @property
    def spln_noncan_ai(self):
        return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.noncan.bai""" % self.spln_idx)

    #
    # @property
    # def spln_spl_srt_am(self):
    #     return os.path.join(self.spln_align_lib_dir_glo, r"""%s.spln.spl.srt.bam""" % self.spln_idx)


    # splited Ns
    # @property
    # def spln_spl_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln.spl.bam""" % self.spln_idx)



    # final variation(clipped) for lane
    # @property
    # def spln_clp_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln.clp.bam""" % self.spln_idx)

    # final expression(spliced) for lane
    # @property
    # def spln_spl_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln.spl.bam""" % self.spln_idx)

    # @property
    # def spln_proper_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.proper.bam""" % self.spln_idx)


    # @property
    # def spln_sort_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln_srt.bam""" % self.spln_idx)

    # @property
    # def spln_acc_bam(self):
    #     return os.path.join(self.spln_align_lib_dir, r"""%s.spln_acc.bam""" % self.spln_idx)



    @property
    def spln_bedpe(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.spln.bedpe""" % self.spln_idx)

    @property
    def spln_h5store(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.spln.h5""" % self.spln_idx)

    @property
    def spln_bwa_log(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.bwa.log""" % self.spln_idx)

    # ------------------- Tophat output

    @property
    def spln_tophat_dir(self):
        return os.path.join(self.spln_align_lib_dir, "tophat_out")

    @property
    def spln_tophat_dir_loc(self):
        return os.path.join(self.spln_align_lib_dir_loc, "tophat_out")

    @property
    def spln_tophat_out(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.%s.33.tophat.out""" % (self.spln_smp, self.spln_lib))

    @property
    def spln_acc_am(self):
        return os.path.join(self.spln_tophat_dir_loc, "accepted_hits.bam")

    @property
    def spln_acc_srt_am(self):
        return os.path.join(self.spln_tophat_dir_loc, "accepted_hits.srt.bam")

    # @property
    # def spln_sorted_bam(self):
    #     return os.path.join(self.spln_tophat_dir, "accepted_hits_sorted.bam")


    @property
    def spln_unm_am(self):
        return os.path.join(self.spln_tophat_dir_loc, "unmapped.bam")

    @property
    def spln_unm_srt_am(self):
        return os.path.join(self.spln_tophat_dir_loc, "unmapped.srt.bam")

        # @property
        # def spln_unm_fix_am(self):
        #     return os.path.join(self.spln_tophat_dir_loc, "unmapped_fixup.bam")

        # @property
        # def spln_unm_fix_srt_am(self):
        #     return os.path.join(self.spln_tophat_dir_loc, "unm.fix.srt.bam")

        # @property
        # def spln_merged_bam(self):
        #     return os.path.join(self.spln_tophat_dir, "merged.bam")

    # ---------------------------------------

    # this name is identical to the dnaSeq mer_smp_bam
    # it is used to keep same rnaseq variable names to link to rnaseq alignenment data
    # in rnvar projects
    # @property
    # def allmerg_bam(self):
    #     return os.path.join(self.align_smp_dir, "%s.mer.bam" % self.smp_name)







    # -------------------  Lane statistics

    @property
    def one_spln_insert_stats_csv(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.insert_stats.csv""" % self.spln_idx)

    @property
    def one_spln_insert_stats_pdf(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.insert_stats.pdf""" % self.spln_idx)

    @property
    def all_spln_insert_stats_csv(self):
        return os.path.join(self.conf.metrics2_dir, "all_spln_insert_stats.csv")

    @property
    def blast_subsmp_reads_nb(self):
        return 20000

    @property
    def one_spln_blast_stats_subsmp_fa(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.blast_subsmp.fa""" % self.spln_idx)

    @property
    def one_spln_blast_stats_out(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.blast_out.txt""" % self.spln_idx)

    @property
    def one_spln_blast_species_csv(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.blast_species.csv""" % self.spln_idx)

    @property
    def all_spln_blast_stats_csv(self):
        return os.path.join(self.conf.metrics2_dir, "all_spln_blast_stats.csv")

    @property
    def spln_hplex_qc(self):
        return self.__spln_hplex_qc

    @spln_hplex_qc.setter
    def spln_hplex_qc(self, val):
        self.__spln_hplex_qc = val

    @property
    def one_hplex_qc_stats_csv(self):
        return os.path.join(self.spln_align_lib_dir, r"""%s.hplex_qc_stats.csv""" % self.spln_idx)

    @property
    def all_hplex_qc_stats_csv(self):
        return os.path.join(self.conf.metrics2_dir, "all_hplex_qc_stats.csv")

    # ------------------ Sample folders

    # different when from GQ pipeline
    @property
    def align_smp_dir(self):
        this_dir = os.path.join(self.conf.align2_dir, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def align_smp_dir_loc(self):
        this_dir = os.path.join(self.conf.align2_dir_loc, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def align_smp_dir_glo(self):
        this_dir = os.path.join(self.conf.align2_dir_glo, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def align3_smp_dir(self):
        this_dir = os.path.join(self.conf.align3_dir, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def align3_smp_dir_loc(self):
        this_dir = os.path.join(self.conf.align3_dir_loc, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def align3_smp_dir_glo(self):
        this_dir = os.path.join(self.conf.align3_dir_glo, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    #  ---------------------------- sample files

    @property
    def smp_mer_am(self):
        return os.path.join(self.align_smp_dir_loc, r"""%s.smp.mer.bam""" % self.smp_name)

    @property
    def smp_mer_ai(self):
        return os.path.join(self.align_smp_dir_loc, r"""%s.smp.mer.bai""" % self.smp_name)

    # @property
    # def smp_mer_glob(self):
    #     return os.path.join(self.align_smp_dir, "%s.smp.mer.*" % self.smp_name)


    @property
    def smp_rgr_am(self):
        return os.path.join(self.align_smp_dir, r"""%s.smp.rgr.bam""" % self.smp_name)

    @property
    def smp_rgr_ai(self):
        return os.path.join(self.align_smp_dir, r"""%s.smp.rgr.bai""" % self.smp_name)



    # @property
    # def smp_clp_rgr_am(self):
    #     return os.path.join(self.align_smp_dir_loc, r"""%s.smp.clp.rgr.bam""" % self.smp_name)

    # @property
    # def smp_clp_rgr_ai(self):
    #     return os.path.join(self.align_smp_dir_loc, r"""%s.smp.clp.rgr.bam.bai""" % self.smp_name)

    # expression analysis starts here and these goes into merge_smpgrps
    @property
    def smp_spl_ddp_am(self):
        return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.spl.ddp.bam""" % self.smp_name)

    @property
    def smp_spl_ddp_ai(self):
        return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.spl.ddp.bai""" % self.smp_name)

    @property
    def smp_spl_ddp_met(self):
        return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.spl.ddp.met""" % self.smp_name)

    # details for expression analysis
    # @property
    # def smp_spl_ddp_uni_am(self):
    #     return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.spl.ddp.uni.bam""" % self.smp_name)
    # @property
    # def smp_spl_ddp_uni_ai(self):
    #     return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.spl.ddp.uni.bai""" % self.smp_name)
    # and statistics
    # @property
    # def smp_spl_ddp_plu_am(self):
    #     return os.path.join(self.align_smp_dir_glo, r"""%s.smp.spl.ddp.plu.bam""" % self.smp_name)
    # @property
    # def smp_spl_ddp_plu_ai(self):
    #     return os.path.join(self.align_smp_dir_glo, r"""%s.smp.spl.ddp.plu.bai""" % self.smp_name)

    @property
    def smp_clp_ddp_am(self):
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.ddp.bam""" % self.smp_name)

    @property
    def smp_clp_ddp_ai(self):
        print("self.align_smp_dir_glo: ", self.align_smp_dir_glo)
        print("self.smp_name: ", self.smp_name)
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.ddp.bai""" % self.smp_name)

    @property
    def smp_clp_ddp_met(self):
        return os.path.join(self.align_smp_dir, r"""%s.smp.clp.ddp.met""" % self.smp_name)

    # gatk splitN files
    @property
    def smp_clp_non_gtk_am(self):
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.non.gtk.bam""" % self.smp_name)

    @property
    def smp_clp_non_gtk_ai(self):
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.non.gtk.bai""" % self.smp_name)

    # other splitN files
    @property
    def smp_clp_non_oth_am(self):
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.non.oth.bam""" % self.smp_name)

    @property
    def smp_clp_non_oth_ai(self):
        return os.path.join(self.align_smp_dir_glo, r"""%s.smp.clp.non.oth.bai""" % self.smp_name)

    # other realigner eg: abra
    # it gives unsorted data
    # this _aln is in fact unsorted
    @property
    def smp_clp_non_oth_ral_am(self):
        return os.path.join(self.align_smp_dir_loc, r"""%s.smp.clp.non.oth.ral.bam""" % self.smp_name)

    # def smp_clp_ral_oth_aln_ai(self):
    #     return os.path.join(self.align_smp_dir_loc, r"""%s.smp.clp.ral.oth.aln.bai""" % self.smp_name)

    # sort it
    # so this is sorted, not the _aln one
    @property
    def smp_oth_am(self):
        return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.oth.bam""" % self.smp_name)

    @property
    def smp_oth_ai(self):
        return os.path.join(self.align3_smp_dir_glo, r"""%s.smp.oth.bai""" % self.smp_name)

    @property
    def smp_clp_ral_oth_dir_loc(self):
        this_dir = os.path.join(self.align_smp_dir_loc, "smp_clp_ral_oth")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            os.chmod(this_dir, 0o777)
        return this_dir

    @property
    def abra_temp_dir(self):
        this_dir = os.path.join(self.smp_clp_ral_oth_dir_loc, "abra_temp_dir")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            os.chmod(this_dir, 0o777)
        return this_dir

    # smpgrp
    @property
    def align_smpgrp_dir(self):
        this_dir = os.path.join(self.conf.align3_dir, self.smpgrp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    # smpgrp
    @property
    def align_smpgrp_dir_glo(self):
        this_dir = os.path.join(self.conf.align3_dir_glo, self.smpgrp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    # ----------------- RNA supersample


    @property
    def grp_exp_am(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.exp.bam" % self.smpgrp_name)

    @property
    def grp_exp_ai(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.grp.exp.bai" % self.smpgrp_name)

    @property
    def deploy_spl_smpgrp_bam(self):
        return os.path.join(self.conf.deploy_bams_dir, "%s.bam" % self.smpgrp_name)

    @property
    def deploy_spl_smpgrp_bai(self):
        return os.path.join(self.conf.deploy_bams_dir, "%s.bai" % self.smpgrp_name)

    # ------------------ WIGGLE

    @property
    def qc_grp_track_file(self):
        return self.__qc_grp_track_file

    @qc_grp_track_file.setter
    def qc_grp_track_file(self, val):
        self.__qc_grp_track_file = val

    @property
    def qc_grp_track_flt_dup(self):
        return self.__qc_grp_track_flt_dup

    @qc_grp_track_flt_dup.setter
    def qc_grp_track_flt_dup(self, val):
        self.__qc_grp_track_flt_dup = val

    @property
    def qc_grp_track_flt_uni(self):
        return self.__qc_grp_track_flt_uni

    @qc_grp_track_flt_uni.setter
    def qc_grp_track_flt_uni(self, val):
        self.__qc_grp_track_flt_uni = val

    @property
    def qc_grp_track_flt_stn(self):
        return self.__qc_grp_track_flt_stn

    @qc_grp_track_flt_stn.setter
    def qc_grp_track_flt_stn(self, val):
        self.__qc_grp_track_flt_stn = val

    @property
    def bed_graph(self):
        return os.path.join(self.align_smpgrp_dir, "%s.grp.%s.%s.%s.%s.bedGraph" % (self.smpgrp_name,
                                                                                    self.qc_grp_track_file,
                                                                                    self.qc_grp_track_flt_dup,
                                                                                    self.qc_grp_track_flt_uni,
                                                                                    self.qc_grp_track_flt_stn))

    # @property
    # def big_wig(self):
    #     return os.path.join(self.align_smp_dir, "%s.bw" % self.smp_name)

    # moved to deployment folder
    # @property
    # def big_wig_prefix(self):
    #     return os.path.join(self.conf.ucsc_hub_big_wig_cls_dir, "%s" % self.smpgrp_name)

    @property
    def big_wig(self):
        return os.path.join(self.align_smpgrp_dir, "%s.grp.%s.%s.%s.%s.bw" % (self.smpgrp_name,
                                                                              self.qc_grp_track_file,
                                                                              self.qc_grp_track_flt_dup,
                                                                              self.qc_grp_track_flt_uni,
                                                                              self.qc_grp_track_flt_stn))

    @property
    def ucsc_hub_big_wig_cls_dir(self):
        loc_dir = os.path.join(self.conf.ucsc_hub_big_wig_dir, "%s-%s-%s-%s" % (self.qc_grp_track_file,
                                                                                self.qc_grp_track_flt_dup,
                                                                                self.qc_grp_track_flt_uni,
                                                                                str(self.cls_idx)))

        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def deploy_big_wig_link(self):
        return os.path.join(self.ucsc_hub_big_wig_cls_dir, "%s.grp.%s.%s.%s.%s.bw" % (self.smpgrp_name,
                                                                                      self.qc_grp_track_file,
                                                                                      self.qc_grp_track_flt_dup,
                                                                                      self.qc_grp_track_flt_uni,
                                                                                      self.qc_grp_track_flt_stn))

    # ----------------  RNA related


    @property
    def stage_abrev(self):
        return self.__stage_abrev

    @stage_abrev.setter
    def stage_abrev(self, val):
        self.__stage_abrev = val

    @property
    def rnaseqc_dir(self):
        loc_dir = os.path.join(self.conf.proj_dir, "rnaseqc")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def rnaseqc_stage_dir(self):
        loc_dir = os.path.join(self.rnaseqc_dir, self.stage_abrev)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def rnaseqc_cls_dir(self):
        loc_dir = os.path.join(self.rnaseqc_stage_dir, str(self.cls_idx))
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def rnaseqc_samples_csv(self):
        return os.path.join(self.rnaseqc_cls_dir, "samples.csv")

    @property
    def rnaseqc_output_dir(self):
        loc_dir = os.path.join(self.rnaseqc_cls_dir, "output")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def rnaseqc_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "rnaseqc_link")

    @property
    def matr_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "matr_link")

    @property
    def metr_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "metr_link")

    @property
    def vcfs_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "vcfs_link")

    @property
    def sc_qc_figs_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "sc_qc_figs")


    @property
    def rnaseqc_log(self):
        return os.path.join(self.rnaseqc_dir, "rnaseqc.log")

    @property
    def rseqc_dir(self):
        loc_dir = os.path.join(self.conf.proj_dir, "rseqc")
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def rseqc_depl_link(self):
        return os.path.join(self.conf.deploy_dir, "rseqc_link")

    # @property
    # def r_rna_fa(self):
    #     return os.path.join(self.conf.raglab_install_home, "igenomes/enGRCh37sp/annotation/rrna/human_all_rRNA.fasta")

    @property
    def tra_gtf(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37sp/annotation/genes_gtf/enGRCh37v75sp_genes.gtf")
        return self.conf.ref_gen_transcr_gtf

    @property
    def exn_gtf(self):
        # return os.path.join(self.raglab_install_home, "igenomes/enGRCh37sp/annotation/genes_gtf/enGRCh37v75sp_genes.gtf")
        return self.ref_gen_transcr_gtf

    # -----------------  DEPLOY UCSC
    @property
    def ucsc_hub_jinja_name(self):
        return "ucsc_hub.txt"

    @property
    def ucsc_hub_depl_file(self):
        return os.path.join(self.conf.ucsc_hub_dir, self.ucsc_hub_jinja_name)

    @property
    def ucsc_genomes_jinja_name(self):
        return "ucsc_genomes.txt"

    @property
    def ucsc_genomes_depl_file(self):
        return os.path.join(self.conf.ucsc_hub_dir, self.ucsc_genomes_jinja_name)

    @property
    def ucsc_track_db_jinja_name(self):
        return "ucsc_track_db.txt"

    @property
    def ucsc_track_db_depl_file(self):
        return os.path.join(self.conf.ucsc_hub_dir, self.ucsc_track_db_jinja_name)

    @staticmethod
    def sambamba_filter_dup(dup):

        filter = ""

        if dup == "ded":
            filter += " and not duplicate"

        return filter

    @staticmethod
    def sambamba_filter_dup_uni_stn(dup, uni, stn):

        filter = ""

        if dup == "ded":
            filter += " and not duplicate"

        if uni == "uni":
            filter += " and mapping quality >= 1"

        if uni == "multi":
            filter += " and mapping quality < 1"

        if stn == "pls":
            filter += " and [XS] == '+'"

        if stn == "mns":
            filter += " and [XS] == '-'"

        return filter

    @staticmethod
    def sambamba_filter_txt(flt):

        # base filter
        filter = "not unmapped" + flt

        ret = r""" | sambamba view -F "%s" /dev/stdin -f bam """ % filter
        return ret

    # ----------------------------------- COMMON METHODS

    def trim_lane_trimmomatic_se(self):

        # args = {"spln_idx": self.spln_idx}
        # print("parameters: \n", args)

        exit_codes = []

        print("self.spln_trim_lib_dir: ", self.spln_trim_lib_dir)

        # remove output folder
        if os.path.exists(self.spln_trim_lib_dir):
            shutil.rmtree(self.spln_trim_lib_dir, ignore_errors=False, onerror=None)

        # ILLUMINACLIP:%(adapt)s:4:30:30:8:true \
        # HEADCROP:12 TRAILING:30 MINLEN:30 \

        # MINLEN:10 \

        # cmd0 = r"""java -XX:ParallelGCThreads=4 -Xmx10G \
        # -cp $TRIMMOMATIC_JAR \
        # org.usadellab.trimmomatic.TrimmomaticSE -threads 12 -phred33 \
        # %(s1)s \
        # %(rs1)s \
        # """
        # cmd0 += self.conf.trim_lane_trimmomatic_illuminaclip

        # cmd0 += r""" 2> %(trim_out)s
        # """
        #         cmd = cmd0 % {"s1": self.spln_s1,
        #                       "rs1": self.spln_rs1,
        # "adapt": self.conf.trim_lane_trimmomatic_adapt,
        # "trim_out": self.spln_trimout_csv}

        cmd = r"""java -XX:ParallelGCThreads=4 -Xmx10G \
-cp $TRIMMOMATIC_JAR \
org.usadellab.trimmomatic.TrimmomaticSE -threads 12 -phred33 \
%(s)s \
%(rs)s \
%(extra)s 2> %(trim_out)s
""" % {"s": self.spln_s,
       "rs": self.spln_rs,
       "extra": self.conf.trim_lane_trimmomatic_illuminaclip,
       "trim_out": self.spln_trimout_csv}

        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_trim_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]

    def trim_lane_trimmomatic_pe(self):

        # args = {"spln_idx": self.spln_idx}
        # print("parameters: \n", args)

        exit_codes = []

        print("self.spln_trim_lib_dir: ", self.spln_trim_lib_dir)

        # remove output folder
        if os.path.exists(self.spln_trim_lib_dir):
            shutil.rmtree(self.spln_trim_lib_dir, ignore_errors=False, onerror=None)

        # ILLUMINACLIP:%(adapt)s:4:30:30:8:true \
        # HEADCROP:12 TRAILING:30 MINLEN:30 \

        # MINLEN:10 \

        print("self.cor_tsk: ", self.cor_tsk)
        print("self.mem_tsk: ", self.mem_tsk)

        print(self.spln_fastq_runreg_dir)
        print(r"""%s.%s.33.pair1.fastq.gz""" % (self.spln_smp, self.spln_lib))



        cmd0 = r"""java -XX:ParallelGCThreads=4 -Xmx%(mem_tsk)sm \
-cp $TRIMMOMATIC_JAR \
org.usadellab.trimmomatic.TrimmomaticPE -threads %(cor_tsk)s -phred33 \
%(p1)s \
%(p2)s \
%(rp1)s \
%(rs1)s \
%(rp2)s \
%(rs2)s \
%(extra)s  >& %(trim_out)s
"""
        cmd = cmd0 % {"cor_tsk": self.cor_tsk,
                      "mem_tsk": self.mem_tsk,
                      "p1": self.spln_p1,
                      "p2": self.spln_p2,
                      "rp1": self.spln_rp1,
                      "rs1": self.spln_rs1,
                      "rp2": self.spln_rp2,
                      "rs2": self.spln_rs2,
                      "extra": self.conf.trim_lane_trimmomatic_illuminaclip,
                      "trim_out": self.spln_trimout_csv
        }
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_trim_lib_dir,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]


        # test if procedure success
        # non_null_codes = [exit_code for exit_code in exit_codes
        #                   if exit_code != 0]
        # success = non_null_codes.__len__() == 0
        #
        # print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)
        #

        # mark as done
        # self.mark_done_args(args, success)

    def trim_pe_to_align_se(self):

        exit_codes = []

        readpairs = r"""%s %s %s %s """ % (self.spln_rp1, self.spln_rs1,
                                           self.spln_rp2, self.spln_rs2)

        cmd = r"""zcat %(readpairs)s | pigz -p 10 -c > %(fout)s """ % {"readpairs": readpairs, "fout": self.spln_rs}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_trim_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]

    def align_lane_bwa(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        print("self.spln_align_lib_dir: ", self.spln_align_lib_dir)

        # remove output folder
        if os.path.exists(self.spln_align_lib_dir):
            shutil.rmtree(self.spln_align_lib_dir, ignore_errors=False, onerror=None)

        f3 = open(self.spln_bwa_log, "w")

        cmd = r"""bwa mem -M -t 12 %(ref_gen_bwa)s %(rp1)s %(rp2)s
""" % {"ref_gen_bwa": self.conf.ref_gen_bwa, "rp1": self.spln_rp1, "rp2": self.spln_rp2}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stderr=f3, stdout=subprocess.PIPE)

        cmd = r"""samtools view -@ 12 -b - -o %(spln_bam)s
""" % {"spln_bam": self.spln_clp_aln_am}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdin=p1.stdout, stderr=f3,
                              stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in [p1, p2]]
        print("exit_codes stage: ", exit_codes_stage)
        exit_codes.extend(exit_codes_stage)

        f3.close()

        return exit_codes

    def align_lane_tophat_se(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        # lanes_df = self.conf.sample_lanes
        # smp_lane = lanes_df.iloc[[self.spln_idx]]

        # print("smp_lane: ", smp_lane)

        # smp = smp_lane.iloc[0, 0]
        # library = smp_lane.iloc[0, 1]
        # run_s = smp_lane.iloc[0, 2]
        # region_s = smp_lane.iloc[0, 3]
        # print("--------------------", smp, library, run_s, region_s)
        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # run_region_src_d = r"""%s/%s/run%s_%s""" % (self.conf.reads2_dir, smp, run_s, region_s)
        # run_region_dst_d = r"""%s/%s/run%s_%s""" % (self.conf.align2_dir, smp, run_s, region_s)

        # if not os.path.exists(run_region_dst_d):
        #     os.makedirs(run_region_dst_d)

        # rp1 = r"""%s/%s.%s.33.pair1.fastq.gz""" % (run_region_src_d, smp, library)
        # rp2 = r"""%s/%s.%s.33.pair2.fastq.gz""" % (run_region_src_d, smp, library)
        # align_out = r"""%s/%s.%s.33.alignout.csv""" % (run_region_dst_d, smp, library)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
tophat -p %(cor_tsk)s \
%(extra)s \
--transcriptome-index %(tra_idx)s \
%(gen_idx)s \
%(rs)s \
2> %(align_out)s
""" % {"cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "extra": self.conf.spln_align_extra,
       "rs": self.spln_rs,
       "tra_idx": self.conf.ref_gen_transcr_ix,
       "gen_idx": self.conf.ref_gen_bt2_ix,
       "align_out": self.spln_tophat_out}
        print(cmd)

        print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)

        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return [exit_codes]

    def align_lane_tophat_pe(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        # lanes_df = self.conf.sample_lanes
        # smp_lane = lanes_df.iloc[[self.spln_idx]]

        # print("smp_lane: ", smp_lane)

        # smp = smp_lane.iloc[0, 0]
        # library = smp_lane.iloc[0, 1]
        # run_s = smp_lane.iloc[0, 2]
        # region_s = smp_lane.iloc[0, 3]
        # print("--------------------", smp, library, run_s, region_s)
        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # run_region_src_d = r"""%s/%s/run%s_%s""" % (self.conf.reads2_dir, smp, run_s, region_s)
        # run_region_dst_d = r"""%s/%s/run%s_%s""" % (self.conf.align2_dir, smp, run_s, region_s)

        # if not os.path.exists(run_region_dst_d):
        #     os.makedirs(run_region_dst_d)

        # rp1 = r"""%s/%s.%s.33.pair1.fastq.gz""" % (run_region_src_d, smp, library)
        # rp2 = r"""%s/%s.%s.33.pair2.fastq.gz""" % (run_region_src_d, smp, library)
        # align_out = r"""%s/%s.%s.33.alignout.csv""" % (run_region_dst_d, smp, library)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
tophat -p %(cor_tsk)s \
%(extra)s \
--transcriptome-index %(tra_idx)s \
%(gen_idx)s \
%(rp1)s \
%(rp2)s \
2> %(align_out)s
""" % {"cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "rp1": self.spln_rp1, "rp2": self.spln_rp2,
       "extra": self.conf.spln_align_extra,
       "tra_idx": self.conf.ref_gen_transcr_ix,
       "gen_idx": self.conf.ref_gen_bt2_ix,
       "align_out": self.spln_tophat_out}

        print(cmd)

        print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)

        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return [exit_codes]

    def align_lane_hisat_transcr_se(self):

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        # cmd = r"""hisat2 %(extra)s -x %(hisat_ix)s -U %(rs)s > %(samout)s
        cmd = r"""hisat2 %(extra)s -x %(hisat_ix)s -U %(rs)s
""" % {"hisat_ix": self.conf.hisat_transcr_ix,
       "rs": self.spln_rs,
       # "samout": self.spln_spl_aln_sam,
       "extra": self.conf.spln_align_extra}
        print(cmd)
        # print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        # p1.wait()
        # rc1 = p1.returncode
        # print(("rc1: ", rc1))
        # exit_codes.append(rc1)

        cmd = r"""samtools view -@ 4 -b - -o %(spln_bam)s
""" % {"spln_bam": self.spln_spl_aln_am
       }
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdin=p1.stdout, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in [p1, p2]]
        print("exit_codes stage: ", exit_codes_stage)
        exit_codes.extend(exit_codes_stage)

        # cmd = r"""cat %(samin)s | samtools view -@ 12 -b - -o %(spln_bam)s
        # """ % {"samin": self.spln_spl_aln_sam,
        #        "spln_bam": self.spln_spl_aln_am}
        #         print(cmd)
        #         p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        #         p2.wait()
        #         rc2 = p2.returncode
        #         print(("rc2: ", rc2))
        #         exit_codes.append(rc2)



        return [exit_codes]

    def align_lane_hisat_transcr_pe(self):

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # remove output folder
        if os.path.exists(self.spln_align_lib_dir_loc):
            shutil.rmtree(self.spln_align_lib_dir_loc, ignore_errors=False, onerror=None)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        # cmd = r"""hisat2 %(extra)s -x %(hisat_ix)s -U %(rs)s > %(samout)s
        cmd = r"""hisat2 --rg-id %(rgid)s --rg LB:rglb --rg PL:ILLUMINA --rg PU:rgpu --rg SM:%(rgsm)s \
%(extra)s -p %(cor_tsk)s -x %(hisat_ix)s -1 %(rp1)s -2 %(rp2)s
""" % {"rgid": self.spln_runreg,
       "rgsm": self.spln_smp,
       "hisat_ix": self.conf.hisat_transcr_ix,
       "rp1": self.spln_rp1,
       "rp2": self.spln_rp2,
       "extra": self.conf.spln_align_extra,
       "cor_tsk": self.cor_tsk
       }
        print(cmd)
        # print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        # p1.wait()
        # rc1 = p1.returncode
        # print(("rc1: ", rc1))
        # exit_codes.append(rc1)

        cmd = r"""samtools view -@ %(cor_tsk)s -b - -o %(spln_bam)s
""" % {"cor_tsk": self.cor_tsk,
       "spln_bam": self.spln_spl_aln_am
       }
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdin=p1.stdout, stdout=subprocess.PIPE)

        exit_codes_stage = [pp.wait() for pp in [p1, p2]]
        print("exit_codes stage: ", exit_codes_stage)
        exit_codes.extend(exit_codes_stage)

        # cmd = r"""cat %(samin)s | samtools view -@ 12 -b - -o %(spln_bam)s
        # """ % {"samin": self.spln_spl_aln_sam,
        #        "spln_bam": self.spln_spl_aln_am}
        #         print(cmd)
        #         p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        #         p2.wait()
        #         rc2 = p2.returncode
        #         print(("rc2: ", rc2))
        #         exit_codes.append(rc2)



        return [exit_codes]

    def align_lane_bbmap_se(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        # lanes_df = self.conf.sample_lanes
        # smp_lane = lanes_df.iloc[[self.spln_idx]]

        # print("smp_lane: ", smp_lane)

        # smp = smp_lane.iloc[0, 0]
        # library = smp_lane.iloc[0, 1]
        # run_s = smp_lane.iloc[0, 2]
        # region_s = smp_lane.iloc[0, 3]
        # print("--------------------", smp, library, run_s, region_s)
        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # run_region_src_d = r"""%s/%s/run%s_%s""" % (self.conf.reads2_dir, smp, run_s, region_s)
        # run_region_dst_d = r"""%s/%s/run%s_%s""" % (self.conf.align2_dir, smp, run_s, region_s)

        # if not os.path.exists(run_region_dst_d):
        #     os.makedirs(run_region_dst_d)

        # rp1 = r"""%s/%s.%s.33.pair1.fastq.gz""" % (run_region_src_d, smp, library)
        # rp2 = r"""%s/%s.%s.33.pair2.fastq.gz""" % (run_region_src_d, smp, library)
        # align_out = r"""%s/%s.%s.33.alignout.csv""" % (run_region_dst_d, smp, library)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        cmd = r"""java -Djava.library.path=$BBMAP_HOME/jni -ea -Xmx31g \
-cp $BBMAP_HOME/current align2.BBMap \
path=%(bbmap_ix)s \
in=%(rs)s \
out=%(samout)s %(extra)s
""" % {"bbmap_ix": self.conf.ref_gen_bbmap_ix_dir,
       "rs": self.spln_rs,
       "samout": self.spln_spl_aln_sam,
       "extra": self.conf.spln_align_extra}
        print(cmd)
        # print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        cmd = r"""cat %(samin)s | samtools view -@ 12 -b - -o %(spln_bam)s
""" % {"samin": self.spln_spl_aln_sam,
       "spln_bam": self.spln_spl_aln_am}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print(("rc2: ", rc2))
        exit_codes.append(rc2)

        return [exit_codes]

    def align_lane_bbmap_pe(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        # lanes_df = self.conf.sample_lanes
        # smp_lane = lanes_df.iloc[[self.spln_idx]]

        # print("smp_lane: ", smp_lane)

        # smp = smp_lane.iloc[0, 0]
        # library = smp_lane.iloc[0, 1]
        # run_s = smp_lane.iloc[0, 2]
        # region_s = smp_lane.iloc[0, 3]
        # print("--------------------", smp, library, run_s, region_s)
        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # run_region_src_d = r"""%s/%s/run%s_%s""" % (self.conf.reads2_dir, smp, run_s, region_s)
        # run_region_dst_d = r"""%s/%s/run%s_%s""" % (self.conf.align2_dir, smp, run_s, region_s)

        # if not os.path.exists(run_region_dst_d):
        #     os.makedirs(run_region_dst_d)

        # rp1 = r"""%s/%s.%s.33.pair1.fastq.gz""" % (run_region_src_d, smp, library)
        # rp2 = r"""%s/%s.%s.33.pair2.fastq.gz""" % (run_region_src_d, smp, library)
        # align_out = r"""%s/%s.%s.33.alignout.csv""" % (run_region_dst_d, smp, library)

        # -g 1 means only one (single), no multiply mapping reads  --max-multihits
        cmd = r"""java -Djava.library.path=$BBMAP_HOME/jni -ea -Xmx31g \
-cp $BBMAP_HOME/current align2.BBMap \
path=%(bbmap_ix)s \
in=%(rp1)s in2=%(rp2)s \
out=%(samout)s %(extra)s
""" % {"bbmap_ix": self.conf.ref_gen_bbmap_ix_dir,
       "rp1": self.spln_rp1, "rp2": self.spln_rp2,
       "samout": self.spln_spl_aln_sam,
       "extra": self.conf.spln_align_extra}
        print(cmd)
        # print("working dir: self.spln_align_lib_dir_loc: ", self.spln_align_lib_dir_loc)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        cmd = r"""cat %(samin)s | samtools view -@ 12 -b - -o %(spln_bam)s
""" % {"samin": self.spln_spl_aln_sam,
       "spln_bam": self.spln_spl_aln_am}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print(("rc2: ", rc2))
        exit_codes.append(rc2)

        return [exit_codes]


    def pst_lane_bwa(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # print row
        print(("spln_row: ", self.spln_row))
        # print(("accepted_f: ", self.spln_acc_am))

        # sort
        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_clp_aln_am,
       "out": self.spln_clp_aln_srt_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # add lane readgroup for GATK
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=rgid RGLB=rglb RGPL=ILLUMINA RGPU=rgpu RGSM=%(spln)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_clp_aln_srt_am,
       "fout": self.spln_clp_rgr_am,
       "spln": self.spln_idx}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # reorder karyotypic
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_clp_rgr_am,
       "fout": self.spln_clp_krd_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # Fix Mate information
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} FixMateInformation \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s \
R=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_clp_krd_am,
       "fout": self.spln_clp_fmt_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # CleanSam
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CleanSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_clp_fmt_am,
       "fout": self.spln_cln_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return exit_codes

    def pst_lane_tophat(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # print row
        print(("spln_row: ", self.spln_row))
        print(("accepted_f: ", self.spln_acc_am))

        lanes_st = ""
        # check alignment files
        if os.path.exists(self.spln_acc_am):
            # sort
            cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_acc_am,
       "out": self.spln_acc_srt_am}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            lanes_st += r"""%s """ % self.spln_acc_srt_am

            # sort
            cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_unm_am,
       "out": self.spln_unm_srt_am}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

            # and add it to the merging
            # lanes_st += r"""%s """ % unmapped_f
            lanes_st += r"""%s """ % self.spln_unm_srt_am

        # check how many results
        lanes_arr = lanes_st.split()

        print(("lanes_st: ", lanes_st))
        print(("lanes_arr: ", lanes_arr))
        lanes_len = lanes_arr.__len__()

        if lanes_len == 1:
            os.symlink(lanes_arr[0], self.spln_spl_aln_am)
        else:
            cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st,
                                                                           "fout": self.spln_spl_aln_am}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print(("rc1: ", rc1))
            exit_codes.append(rc1)

        # add lane readgroup for GATK
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=rgid RGLB=rglb RGPL=ILLUMINA RGPU=rgpu RGSM=%(spln)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_aln_am,
       "fout": self.spln_spl_rgr_am,
       "spln": self.spln_idx}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # reorder karyotypic
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_rgr_am,
       "fout": self.spln_spl_krd_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # Fix Mate information
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} FixMateInformation \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s \
R=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_krd_am,
       "fout": self.spln_spl_fmt_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # CleanSam
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CleanSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_fmt_am,
       "fout": self.spln_cln_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return [exit_codes]

    def pst_lane_bbmap(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # print row
        print("spln_row: ", self.spln_row)

        # sort
        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_spl_aln_am,
       "out": self.spln_spl_aln_srt_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # add lane readgroup for GATK
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=rgid RGLB=rglb RGPL=ILLUMINA RGPU=rgpu RGSM=%(spln)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_aln_srt_am,
       "fout": self.spln_spl_rgr_am,
       "spln": self.spln_idx}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # reorder karyotypic
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_rgr_am,
       "fout": self.spln_spl_krd_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # Fix Mate information
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} FixMateInformation \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s \
R=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_krd_am,
       "fout": self.spln_spl_fmt_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # CleanSam
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CleanSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_fmt_am,
       "fout": self.spln_cln_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return [exit_codes]

    def pst_lane_hisat_transcr(self):

        # args = {"spln_idx": self.spln_idx}
        # print(("parameters: \n", args))

        exit_codes = []

        print(("--------------------", self.spln_smp, self.spln_lib, self.spln_runreg))

        # print row
        print("spln_row: ", self.spln_row)

        # sort -> "out": self.spln_spl_aln_srt_am
        # readgroup is already there
        # cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t %(cor_tsk)s -m %(mem_tsk)sM %(fin)s -o %(fout)s""" \
        cmd = r"""samtools sort --threads %(cor_tsk)s -m %(mem_tsk)sM %(fin)s -o %(fout)s""" \
                      % {"cor_tsk": self.cor_tsk,
                         "mem_tsk": self.mem_tsk,
                         "scratch_d": self.conf.scratch_loc_dir,
                         "fin": self.spln_spl_aln_am,
                         "fout": self.spln_spl_aln_srt_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        if self.conf.rmdup_level == "smp":
            # add sample readgroup for GATK
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=smpid RGLB=rglb RGPL=ILLUMINA RGPU=rgpu RGSM=%(spln)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
    """ % {"scratch_d": self.conf.scratch_loc_dir,
           "fin": self.spln_spl_aln_srt_am,
           "fout": self.spln_spl_rgr_am,
           "spln": self.spln_idx}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

        elif self.conf.rmdup_level == "spln":
            # just link
            os.symlink(self.spln_spl_aln_srt_am, self.spln_spl_rgr_am)
            os.symlink(self.spln_spl_aln_srt_ai, self.spln_spl_rgr_ai)

            # shutil.copy(self.smp_spl_ddp_ai, self.grp_exp_ai)

            exit_codes.append([0])
        else:
            raise Exception("wrong rmdup_level")


        # reorder karyotypic
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_rgr_am,
       "fout": self.spln_spl_krd_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # Fix Mate information
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} FixMateInformation \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s \
R=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_krd_am,
       "fout": self.spln_spl_fmt_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # CleanSam
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} CleanSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_spl_fmt_am,
       "fout": self.spln_cln_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        return [exit_codes]

    def todo_lane(self):

        # clean input files
        # for i in [self.smp_clp_rgr_am, self.smp_clp_rgr_ai,
        #           self.smp_clp_ddp_am, self.smp_clp_ddp_ai]:
        #     if os.path.exists(i):
        #         os.remove(i)


        # if not os.path.exists(a1):
        #     os.mkfifo(a1)
        # if not os.path.exists(a2):
        #     os.mkfifo(a2)
        # if not os.path.exists(a3):
        #     os.mkfifo(a3)
        # if not os.path.exists(a4):
        #     os.mkfifo(a4)
        # if not os.path.exists(a5):
        #     os.mkfifo(a5)


        # if os.path.exists(self.spln_unm_am):
        #     fix the unmapped
        # cmd = r"""tophat-recondition.py %(run_region_d)s""" % {"run_region_d": self.spln_tophat_dir_loc}
        # print(cmd)
        # p0 = subprocess.Popen(cmd, shell=True, cwd=self.spln_tophat_dir_loc, stdout=subprocess.PIPE)
        # p0.wait()
        # rc0 = p0.returncode
        # print(("rc0: ", rc0))
        # exit_codes.append(rc0)

        # cmd = r"""sambamba view -S -f bam -o  %(fout)s %(fin)s""" % {"fin": unmapped_fixup_sam_f, "fout": unmapped_fixup_bam_f}
        # print(cmd)
        # p0 = subprocess.Popen(cmd, shell=True, cwd=run_region_d, stdout=subprocess.PIPE)
        # p0.wait()
        # rc0 = p0.returncode
        # print(("rc0: ", rc0))
        # exit_codes.append(rc0)


        # get rid of unpaired reads
        # cmd = r"""/home/dbadescu/local/bin/python %(pysrc_dir)s/unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": r1, "fout": r2, "pysrc_dir": self.conf.pysrc_dir}
        # cmd = r"""unaligned2mapq0.py < %(fin)s > %(fout)s """ % {"fin": self.spln_spl_aln_am,
        #                                                          "fout": self.spln_spl_cor_am}
        # print(cmd)
        # p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        # p1.wait()
        # rc1 = p1.returncode
        # print(("rc1: ", rc1))
        # exit_codes.append(rc1)

        # cmd = r"""java -jar $PICARD_JAR MergeBamAlignment \
        # UNMAPPED=%(unm)s \
        # ALIGNED=%(acc)s \
        # O=%(fout)s \
        # R=%(ref_genome)s
        # """ % {"acc": self.spln_acc_srt_am,
        #        "unm": self.spln_unm_fix_srt_am,
        #        "fout": self.spln_spl_aln_am,
        #        "ref_genome": self.conf.ref_gen_fa}



        # sort
        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_spl_krd_am,
       "out": self.spln_spl_srt_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # ------------ Turn towards clipped reads ------------------------------------------------------------------------

        # split N reads
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-XX:PermSize=256M -XX:MaxPermSize=512M -Xms1000M -Xmx1700M \
-jar ${GATK_JAR} --analysis_type SplitNCigarReads \
--reference_sequence %(ref_genome)s \
-I %(fin)s \
--out %(fout)s \
-rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS -fixNDN
 """ % {"scratch_d": self.conf.scratch_loc_dir,
        "ref_genome": self.conf.ref_gen_fa,
        "fin": self.spln_spl_srt_am,
        "fout": self.spln_clp_rgr_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # reorder karyotypic
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 -Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} ReorderSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=false \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
REFERENCE=%(ref_genome)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.spln_clp_rgr_am,
       "fout": self.spln_clp_krd_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append(rc1)

        # sort
        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.spln_clp_krd_am,
       "out": self.spln_clp_srt_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)


        # this is it
        # os.symlink(self.spln_spl_bam, self.spln_bam)

    def spln_calc_flx(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        exit_codes += self.pst_lane_hisat_transcr()

        return exit_codes


    def spln_calc_rna(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        print("self.conf.trim_readpairs: ", self.conf.trim_readpairs)
        print("self.conf.spln_align_aligner", self.conf.spln_align_aligner)

        # trim
        if self.conf.trim_readpairs == "se":
            exit_codes += self.trim_lane_trimmomatic_se()
        elif self.conf.trim_readpairs == "pe":
            exit_codes += self.trim_lane_trimmomatic_pe()

        # merge paired end trimming to single end alignment
        if self.conf.merge_readpairs:
            exit_codes += self.trim_pe_to_align_se()

        # align
        if self.conf.spln_align_aligner == "tophat":
            if self.conf.align_readpairs == "se":
                exit_codes += self.align_lane_tophat_se()
            elif self.conf.align_readpairs == "pe":
                exit_codes += self.align_lane_tophat_pe()

        if self.conf.spln_align_aligner == "hisat":
            if self.conf.align_readpairs == "se":
                exit_codes += self.align_lane_hisat_transcr_se()
            elif self.conf.align_readpairs == "pe":
                exit_codes += self.align_lane_hisat_transcr_pe()

        if self.conf.spln_align_aligner == "bbmap":
            if self.conf.align_readpairs == "se":
                exit_codes += self.align_lane_bbmap_se()
            elif self.conf.align_readpairs == "pe":
                exit_codes += self.align_lane_bbmap_pe()

        # if 'spln_trim_se' in self.conf.res_stages_arr:
        #     exit_codes += self.trim_lane_trimmomatic_se()

        # if 'spln_trim_pe' in self.conf.res_stages_arr:
        #     exit_codes += self.trim_lane_trimmomatic_pe()

        # if 'spln_align_se' in self.conf.res_stages_arr:
        #     exit_codes += self.align_lane_tophat_se()
        #
        # if 'spln_align_pe' in self.conf.res_stages_arr:
        #     exit_codes += self.align_lane_tophat_pe()
        #

        if self.conf.spln_align_aligner == "bbmap":
            exit_codes += self.pst_lane_bbmap()
        elif self.conf.spln_align_aligner == "tophat":
            exit_codes += self.pst_lane_tophat()
        elif self.conf.spln_align_aligner == "hisat":
            exit_codes += self.pst_lane_hisat_transcr()

        return exit_codes

    def spln_calc_dna(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        # print("self.conf.spln_readpairs: ", self.conf.spln_readpairs)
        # print("self.conf.spln_align_aligner", self.conf.spln_align_aligner)

        # trim
        if self.conf.trim_readpairs == "se":
            exit_codes += self.trim_lane_trimmomatic_se()
        elif self.conf.trim_readpairs == "pe":
            exit_codes += self.trim_lane_trimmomatic_pe()

        # align
        if self.conf.spln_align_aligner == "bwa":
            if self.conf.align_readpairs == "se":
                pass
            elif self.conf.align_readpairs == "pe":
                exit_codes += self.align_lane_bwa()

        if self.conf.spln_align_aligner == "bwa":
            exit_codes += self.pst_lane_bwa()

        return exit_codes

    def spln_calc(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        if self.conf.seq_type == "dna":
            exit_codes += self.spln_calc_dna()
        elif self.conf.seq_type == "rna":
            exit_codes += self.spln_calc_rna()
            # non-canonical reads only for rna
            # if self.conf.extract_noncan:
            #     exit_codes += self.extract_noncan()

        self.mark_exit_args(exit_codes, args)

    def fail2skip_wiggle(self):

        failed = self.redis_obj.redis_client.lrange("wiggle_fail", 0, -1)
        print("failed: ", failed)

        for job_json in failed:
            print("job: ", job_json)
            kwargs = json.loads(job_json)
            smpgrp_idx = kwargs["smpgrp_idx"]
            print("kwargs: ", kwargs, smpgrp_idx)

            # mark skip
            # df_ = self.conf.sample_grps.copy()
            self.conf.sample_grps.loc[smpgrp_idx, "skip"] = 1
            # print("df_: ", df_)

            # df2_ = .query('skip != 1')
            # take parameters and set corresponding attributes
            # for key, value in kwargs.items():
        #     setattr(self, key, value)

        # .decode("utf-8").rstrip()
        self.conf.save_sample_grps(self.conf.sample_grps)

    def fail2skip_spln_calc(self):

        pass

        # active_gids = nsp.redis_obj.redis_client.lrange("active_gids", 0, -1)
        # print("active_gids: ", active_gids, file=sys.stderr)
        #
        #     for gid in active_gids:
        #         print("gid: ", gid, file=sys.stderr)
        #         hst = nsp.redis_obj.redis_client.hget("client:%s" % gid, "hst")
        #         print("hst: ", hst, file=sys.stderr)
        #
        # kwargs = json.loads(args_json)
        # take parameters and set corresponding attributes
        # for key, value in kwargs.items():
        #     setattr(self, key, value)
        #
        #
        # result = redis.lrange('foo',0, -1)[0].decode()
        #
        # result = result.strip('[]')
        #
        # result = result.split(', ')
        #
        # lastly, if you know all your items in the list are integers
        # result = [int(x) for x in result]

    def merge_lanes(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        lanes_df = self.conf.sample_lanes
        # print("lanes_df: ", lanes_df)
        # print(lanes_df)
        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)
        # print("self.smp_name")
        lanes_df = lanes_df[lanes_df['smp_name'] == self.smp_name]
        lanes_df = lanes_df.query('skip != 1')

        print("-------------")
        # print(lanes_df)

        exit_codes = []

        lanes_st_acc = ""
        lanes_st_aci = ""

        accepted_err_arr = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_mer_am, self.smp_mer_ai]:
            if os.path.exists(i):
                os.remove(i)

        # for f in glob.glob(self.mer_smp_glob):
        #     os.remove(f)


        for idx, row in lanes_df.iterrows():
            self.spln_idx = idx
            # print row
            print("self.spln_idx: ", self.spln_idx, "self.spln_row: ", self.spln_row)

            print("self.spln_cln_am: ", self.spln_cln_am)

            # check alignment files
            if os.path.exists(self.spln_cln_am):
                lanes_st_acc += r"""%s """ % self.spln_cln_am
                lanes_st_aci += r"""%s """ % self.spln_cln_ai
            else:
                accepted_err_arr.append(self.spln_align_lib_dir_glo)

        if accepted_err_arr.__len__() != 0:
            accepted_err_df = pd.DataFrame.from_items([('run_region', accepted_err_arr)])
            accepted_err_f = os.path.join(self.align_smp_dir, "accepted_err_ml.csv")
            accepted_err_df.to_csv(accepted_err_f, sep=',', header=False, index=False)

            print("accepted_err_arr: ", accepted_err_arr)
            print("accepted_err_f: ", accepted_err_f)

        # redirection is important
        # cmd = r"""samtools merge -c -p - %(lanes_st_acc)s > %(fout)s""" % {"lanes_st_acc": lanes_st_acc, "fout": a1}
        lanes_arr = lanes_st_acc.split()
        lanes_ari = lanes_st_aci.split()

        print("lanes_st_acc: ", lanes_st_acc)
        print("lanes_arr: ", lanes_arr)
        lanes_len = lanes_arr.__len__()

        # temporary file
        for i in [self.smp_mer_am, self.smp_mer_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)


        # different domain visibility scratch -> project, copy, don't link
        # but scratch is visible from everywere, the reverse is not
        # ok with symliking
        if lanes_len == 1:
            # shutil.copy(lanes_arr[0], self.smp_mer_am, follow_symlinks=True)
            os.symlink(lanes_arr[0], self.smp_mer_am)
            os.symlink(lanes_ari[0], self.smp_mer_ai)

            exit_codes.append(0)
        else:
            # cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st_acc, "fout": self.smp_mer_am}
            cmd = r"""samtools merge --threads 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": lanes_st_acc, "fout": self.smp_mer_am}
            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_loc, stdout=subprocess.PIPE)
            p1.wait()
            rc1 = p1.returncode
            print("rc1: ", rc1)
            exit_codes.append(rc1)

        # index locally
        # cmd = r"""sambamba index -t 12 %(in)s %(out)s""" % {"scratch_d": self.conf.scratch_loc_dir, "in": self.smp_mer_am, "out": self.smp_mer_ai}
        cmd = r"""samtools index %(in)s %(out)s""" % {"scratch_d": self.conf.scratch_loc_dir, "in": self.smp_mer_am, "out": self.smp_mer_ai}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_loc, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # add lane readgroup for GATK
        cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=rgid RGLB=rglb RGPL=ILLUMINA RGPU=rgpu RGSM=%(spln)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=%(fin)s \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.smp_mer_am,
       "fout": self.smp_rgr_am,
       "spln": self.smp_name}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]

    def extract_noncan(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        cmd = r"""sambamba view -t %(cor_tsk)s -F "paired and (not proper_pair)" -f bam %(fin)s > %(fout)s""" \
              % {"cor_tsk": self.cor_tsk,
                 "fin": self.spln_cln_am,
                 "fout": self.spln_noncan_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # index locally
        # cmd = r"""sambamba index -t 12 %(in)s %(out)s
# """ % {"scratch_d": self.conf.scratch_loc_dir,
#        "in": self.smp_mer_am,
#        "out": self.smp_mer_ai}
#         print(cmd)
#         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_loc, stdout=subprocess.PIPE)
#         p1.wait()
#         rc1 = p1.returncode
#         print("rc1: ", rc1)
#         exit_codes.append(rc1)

        # extract fastq from bam
        # pipe1 = os.path.join(self.dat_fastq_lib_dir, "pipe1")
        r1 = os.path.join(self.spln_align_lib_dir_glo, "r1")
        r2 = os.path.join(self.spln_align_lib_dir_glo, "r2")
        o3 = os.path.join(self.spln_align_lib_dir_glo, "sam_to_fastq.out")
        # f3 = 0
        read1 = "HI.%s.%s.%s.%s_R1.fastq.gz" % (self.spln_run,
                                                self.spln_region,
                                                self.spln_lib,
                                                self.spln_smp)
        read2 = "HI.%s.%s.%s.%s_R2.fastq.gz" % (self.spln_run,
                                                self.spln_region,
                                                self.spln_lib,
                                                self.spln_smp)

        # if not os.path.exists(pipe1):
        #     os.mkfifo(pipe1)

        if not os.path.exists(r1):
            os.mkfifo(r1)
        #
        if not os.path.exists(r2):
            os.mkfifo(r2)
        #
        # erase output files
        # this ensures secure restart from partial previous output
        for i in [read1, read2, o3]:
            if os.path.exists(i):
                os.remove(i)

        with open(o3, "w") as f3:

            # already paired
            cmd = r"""sambamba sort -n \
    -t %(cor_tsk)s -m %(mem_tsk)sM --tmpdir=%(scratch_d)s \
    -F "paired" %(infile)s \
    -o /dev/stdout""" % {"cor_tsk": self.cor_tsk,
                         "mem_tsk": self.mem_tsk,
                         "scratch_d": self.conf.scratch_loc_dir,
                         "infile": self.spln_noncan_am}
            print(cmd)

            # by using named pipes no need for stdin redirection
            p1 = subprocess.Popen(cmd,
                                  shell=True,
                                  cwd=self.spln_align_lib_dir_glo,
                                  stdout=subprocess.PIPE)

            p2 = subprocess.Popen("java -jar $PICARD_JAR SamToFastq VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1, r2), shell=True,
                                  cwd=self.spln_align_lib_dir_glo,
                                  stdin=p1.stdout,
                                  stderr=f3,
                                  stdout=subprocess.PIPE)

            p3 = subprocess.Popen(r"""pigz < %s > %s""" % (r1, read1), shell=True,
                                  cwd=self.conf.fastq_noncan_dir,
                                  stdout = subprocess.PIPE)

            p4 = subprocess.Popen(r"""pigz < %s > %s""" % (r2, read2), shell=True,
                                   cwd=self.conf.fastq_noncan_dir,
                                   stdout=subprocess.PIPE)

            print(p1.pid, p2.pid, p3.pid, p4.pid)
            exit_codes = [pp.wait() for pp in (p1, p2, p3, p4)]

        # os.unlink(pipe1)
        os.unlink(r1)
        os.unlink(r2)


        return [exit_codes]



    def smp_dedup_spl(self):
        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        exit_codes = []

        # temporary file
        for i in [self.smp_spl_ddp_am, self.smp_spl_ddp_ai, self.smp_spl_ddp_met,
                  # self.smp_spl_ddp_uni_am, self.smp_spl_ddp_uni_ai,
                  # self.smp_spl_ddp_plu_am, self.smp_spl_ddp_plu_ai
                  ]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        if self.conf.rmdup_level == "smp":
            fin = self.smp_rgr_am
        elif self.conf.rmdup_level == "spln":
            fin = self.smp_mer_am
        else:
            raise Exception("wrong rmdup_level")


        if self.conf.smp_spl_markdup:
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=%(rmdup)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(dup_metrics)s \
MAX_RECORDS_IN_RAM=5000000
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "rmdup": self.conf.smp_spl_rmdup,
       "fin": fin,
       "fout": self.smp_spl_ddp_am,
       "dup_metrics": self.smp_spl_ddp_met}
            print(cmd)
            p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
            p4.wait()
            rc4 = p4.returncode
            print("rc4: ", rc4)
            exit_codes.append(rc4)
            # temporary file
            # for i in [f2, f2_bai]:
            #     if os.path.exists(i):
            #         os.remove(i)

        else:
            # just link fin to fout
            os.symlink(self.smp_rgr_am, self.smp_spl_ddp_am)
            # !!!! index could be .bam.bai instead of .bai from merging multiple lanes
            os.symlink(self.smp_rgr_ai, self.smp_spl_ddp_ai)
            exit_codes.append(0)
            # f2 is not temporary anymore, it is the real one

            # filter duplicates for expression and statistics
            # cmd = r"""sambamba view -F "not duplicate" \
        # -t 12 %(in_bam)s -o %(out_bam)s -f bam
        # """ % {"scratch_d": self.conf.scratch_loc_dir,
        #        "in_bam": self.smp_spl_ddp_am,
        #        "out_bam": self.smp_spl_ddp_uni_am}
        #         print(cmd)
        #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        #         p1.wait()
        #         rc1 = p1.returncode
        #         print("rc1: ", rc1)
        #         exit_codes.append(rc1)

        # and index
        # cmd = r"""sambamba index -t 12 %(in)s %(out)s
        # """ % {"scratch_d": self.conf.scratch_loc_dir,
        #        "in": self.smp_spl_ddp_uni_am,
        #        "out": self.smp_spl_ddp_uni_ai}
        #         print(cmd)
        #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        #         p1.wait()
        #         rc1 = p1.returncode
        #         print("rc1: ", rc1)
        #         exit_codes.append(rc1)


        # filter duplicates for expression and statistics
        # cmd = r"""sambamba view -F "duplicate" \
        # -t 12 %(in_bam)s -o %(out_bam)s -f bam
        # """ % {"scratch_d": self.conf.scratch_loc_dir,
        #        "in_bam": self.smp_spl_ddp_am,
        #        "out_bam": self.smp_spl_ddp_plu_am}
        #         print(cmd)
        #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        #         p1.wait()
        #         rc1 = p1.returncode
        #         print("rc1: ", rc1)
        #         exit_codes.append(rc1)

        # and index
        # cmd = r"""sambamba index -t 12 %(in)s %(out)s
        # """ % {"scratch_d": self.conf.scratch_loc_dir,
        #        "in": self.smp_spl_ddp_plu_am,
        #        "out": self.smp_spl_ddp_plu_ai}
        #         print(cmd)
        #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        #         p1.wait()
        #         rc1 = p1.returncode
        #         print("rc1: ", rc1)
        #         exit_codes.append(rc1)

        return [exit_codes]

    def smp_dedup_clp(self):

        exit_codes = []

        # temporary file
        for i in [self.smp_clp_ddp_am, self.smp_clp_ddp_ai, self.smp_clp_ddp_met]:
            print("i: ", i)
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        if self.conf.smp_clp_markdup:
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} MarkDuplicates REMOVE_DUPLICATES=%(rmdup)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
ASSUME_SORTED=true \
CREATE_INDEX=true \
INPUT=%(fin)s \
OUTPUT=%(fout)s \
METRICS_FILE=%(dup_metrics)s \
MAX_RECORDS_IN_RAM=5000000
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "rmdup": self.conf.smp_clp_rmdup,
       "fin": self.smp_rgr_am,
       "fout": self.smp_clp_ddp_am,
       "dup_metrics": self.smp_clp_ddp_met}
            print(cmd)
            p4 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
            p4.wait()
            rc4 = p4.returncode
            print("rc4: ", rc4)
            exit_codes.append(rc4)
            # temporary file
            # for i in [f2, f2_bai]:
            #     if os.path.exists(i):
            #         os.remove(i)

        else:
            # just link fin to fout
            os.symlink(self.smp_rgr_am, self.smp_clp_ddp_am)
            # !!!! index could be .bam.bai instead of .bai from merging multiple lanes
            os.symlink(self.smp_rgr_ai, self.smp_clp_ddp_ai)
            exit_codes.append(0)
            # f2 is not temporary anymore, it is the real one

        return [exit_codes]

    def splitN_gatk_link(self):

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_clp_non_gtk_am, self.smp_clp_non_gtk_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        # for gatk
        os.symlink(self.smp_clp_ddp_am, self.smp_clp_non_gtk_am)
        os.symlink(self.smp_clp_ddp_ai, self.smp_clp_non_gtk_ai)

        return [0]

    def splitN_oth_link(self):

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_clp_non_oth_am, self.smp_clp_non_oth_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        # for the other callers
        os.symlink(self.smp_clp_ddp_am, self.smp_clp_non_oth_am)
        os.symlink(self.smp_clp_ddp_ai, self.smp_clp_non_oth_ai)

        return [0]

    def splitN_gatk(self):

        exit_codes = []

        # | samtools sort - -o %(fout)s

        # split N reads using gatk own tool that adds some information into the cigar
        # for its own use later ...
        # --analysis_type SplitNCigarReads -U ALLOW_N_CIGAR_READS
        # --disable-read-filter
        cmd = r"""module unload %(jdk7_mod)s; \
module load %(jdk8_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=%(cor_tsk)s \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${GATK_JAR} SplitNCigarReads -fixNDN \
-R %(ref_genome)s \
-I %(fin)s \
-O %(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "jdk7_mod": self.conf.jdk7_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "fin": self.smp_clp_ddp_am,
       "fout": self.smp_clp_non_gtk_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]

    def splitN_oth(self):

        exit_codes = []

        # for all other callers except gatk
        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
samtools view -h %(fin)s \
| splitNreads.py \
| sed 's/RG:A:/RG:Z:/g' \
| samtools calmd -Sb - %(ref_genome)s 2>/dev/null \
| sambamba sort /dev/stdin -o %(fout)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "scratch_d": self.conf.scratch_loc_dir,
       "fin": self.smp_clp_ddp_am,
       "fout": self.smp_clp_non_oth_am,
       "ref_genome": self.conf.ref_gen_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # and index
        cmd = r"""sambamba index -t 12 %(in)s %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.smp_clp_non_oth_am,
       "out": self.smp_clp_non_oth_ai}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        return [exit_codes]

    def ral_abra(self):

        exit_codes = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.smp_clp_non_oth_ral_am,
                  self.smp_oth_am,
                  self.smp_oth_ai]:
            if os.path.exists(i):
                os.remove(i)

        # remove working folder
        if os.path.exists(self.abra_temp_dir):
            shutil.rmtree(self.abra_temp_dir, ignore_errors=False, onerror=None)
            # print("self.smp_clp_ral_oth_dir: ", self.smp_clp_ral_oth_dir)

        # local and global ralignement using ABRA
        # -Dsamjdk.intel_deflater_so_path=$ABRA_HOME/lib/libIntelDeflater.so \
        # --ib
        cmd = r"""java -XX:ParallelGCThreads=4 -XX:PermSize=256M -XX:MaxPermSize=512M \
-Xms1000M -Xmx12G \
-jar $ABRA_JAR \
--in %(fin)s \
--out %(fout)s \
--ref %(ref_bwa_gen)s \
--targets %(karyo_bed)s \
--threads 12 \
--working %(abra_temp_dir)s \
--no-debug
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "fin": self.smp_clp_non_oth_am,
       "fout": self.smp_clp_non_oth_ral_am,
       "ref_bwa_gen": self.conf.ref_gen_bwa_ix_genome_fa,
       "karyo_bed": self.conf.target_karyo_bed,
       "abra_temp_dir": self.abra_temp_dir}

        # cmd = "./abra3.sh"

        # res = subprocess.call(shlex.split(cmd), shell=False)

        # res = subprocess.call(["./abra3.sh", ""], shell=False, cwd= self.smp_clp_ral_oth_dir)
        # print(res)

        # p1.wait()
        # rc1 = p1.returncode
        # print("rc1: ", rc1)

        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.smp_clp_ral_oth_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # sort
        cmd = r"""sambamba sort --tmpdir=%(scratch_d)s -t 12 -m 12G %(in)s -o %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.smp_clp_non_oth_ral_am,
       "out": self.smp_oth_am}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.smp_clp_ral_oth_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # and index
        cmd = r"""sambamba index -t 12 %(in)s %(out)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in": self.smp_oth_am,
       "out": self.smp_oth_ai}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.smp_clp_ral_oth_dir_loc)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        # should also recalculate NM and MD fields
        # this applied BAQ
        # samtools calmd -bAr aln.bam > aln.baq.bam

        # this only recomputes NM MD BQ
        # samtools calmd -br aln.bam > aln.baq.bam

        return [exit_codes]

    def grind_lanes_rna(self):

        exit_codes = []

        if self.conf.seq_exp == "exp":
            exit_codes += self.smp_dedup_spl()

        elif self.conf.seq_exp == "rnvar":
            exit_codes += self.smp_dedup_spl()
            # create dedup file for variation and split gatk/oth
            exit_codes += self.smp_dedup_clp()

            if "gtk" in self.conf.grind_callers:
                exit_codes += self.splitN_gatk()

            if "oth" in self.conf.grind_callers:
                exit_codes += self.splitN_oth()

        return exit_codes

    def grind_lanes_dna(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        # args = {"smp_idx": self.smp_idx}
        # print("parameters: \n", args)

        exit_codes = []

        if self.conf.seq_exp == "var":
            exit_codes += self.smp_dedup_clp()
            # do not split, just link
            if "gtk" in self.conf.grind_callers:
                exit_codes += self.splitN_gatk_link()

            if "oth" in self.conf.grind_callers:
                exit_codes += self.splitN_oth_link()

        return exit_codes

        # self.mark_exit_args(exit_codes, args)

    # def grind_lanes_dna2(self):
    #
    #     print("------------------------ Entering %s ....  ----------------" % thiscallersname())
    #     args = {"smp_idx": self.smp_idx}
    #     print("parameters: \n", args)
    #
    #     exit_codes = []
    #
    #     exit_codes += self.merge_lanes()
    #     exit_codes += self.smp_dedup_clp()
    #     exit_codes += self.ral_abra()
    #
    #     self.mark_exit_args(exit_codes, args)

    def grind_lanes(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())

        args = {"smp_idx": self.smp_idx}
        print("parameters: \n", args)

        exit_codes = []
        exit_codes += self.merge_lanes()
        # here we have self.smp_rgr_am

        if self.conf.seq_type == "dna":
            exit_codes += self.grind_lanes_dna()
        elif self.conf.seq_type == "rna":
            exit_codes += self.grind_lanes_rna()

        self.mark_exit_args(exit_codes, args)

    def stats_spln_blast(self):
        args = {"spln_idx": self.spln_idx}
        print("parameters: \n", args)

        exit_codes = []

        # delete output files
        for i in [self.one_spln_blast_stats_subsmp_fa, self.one_spln_blast_stats_out, self.one_spln_blast_species_csv]:
            if os.path.exists(i):
                os.remove(i)

        print("self.spln_runreg: ", self.spln_runreg)

        cmd = r"""cat %(p1)s \
| pigz -d \
| seqtk sample -s11 - %(subsmp_reads)s \
| paste - - - - | cut -f1-2 | sed 's/^@/>/g' | tr '\t' '\n' > %(subsmp_fa)s
""" % {"p1": self.spln_p1,
       "subsmp_reads": self.blast_subsmp_reads_nb,
       "subsmp_fa": self.one_spln_blast_stats_subsmp_fa}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print("rc1: ", rc1)
        exit_codes.append(rc1)

        cmd = r"""blastn -num_threads 12 \
-max_target_seqs 1 -evalue 1e-5 -perc_identity 0.5 \
-best_hit_overhang 0.1 -best_hit_score_edge 0.1 \
-seqidlist %(subjects_csv)s \
-query %(subsmp_fa)s \
-db %(contam_fa)s \
-outfmt "6 qseqid sseqid bitscore evalue" \
-out %(stats_out)s
""" % {"subjects_csv": self.conf.blast_contam_subjects_csv,
       "subsmp_fa": self.one_spln_blast_stats_subsmp_fa,
       "contam_fa": self.conf.blast_contam_fa,
       "stats_out": self.one_spln_blast_stats_out}
        print(cmd)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p2.wait()
        rc2 = p2.returncode
        print("rc2: ", rc2)
        exit_codes.append(rc2)

        cmd = r"""cat %(stats_out)s \
| sort -t, -k1,1 -k2,2nr -k3,3n | sort -u -t, -k1,1 --merge \
| gawk 'match($0, /gnl\|(.+)(\|)/, a) {print a[1],"\t",$1}' \
| sort -k1,1 -k2,2 | bedtools groupby -g 1 -c 1 -o count \
> %(species_csv)s

""" % {"stats_out": self.one_spln_blast_stats_out,
       "species_csv": self.one_spln_blast_species_csv}
        print(cmd)
        p3 = subprocess.Popen(cmd, shell=True, cwd=self.spln_align_lib_dir, stdout=subprocess.PIPE)
        p3.wait()
        rc3 = p3.returncode
        print("rc3: ", rc3)
        exit_codes.append(rc3)

        # test if procedure success
        non_null_codes = [exit_code for exit_code in exit_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0
        print("exit_codes, non_null_codes, success: ", exit_codes, non_null_codes, success)
        # mark as done
        self.mark_done_args(args, success)

    def stats_blast(self):

        columns = ['sample', 'run_reg', 'library',
                   'human',
                   'mouse']
        df_ = pd.DataFrame(columns=columns)
        # print("df_: \n", df_, df_.dtypes)


        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'run_reg': 'object', 'library': 'object',
                  'human': 'float64',
                  'mouse': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        # print("df_: \n", df_, df_.dtypes)


        for spln_idx in range(self.nb_splns):
            # for spln_idx in range(2):
            # print("spln_idx: %s" % spln_idx)

            # set index in order to make all path work
            self.spln_idx = spln_idx
            print("self.spln_idx: ", self.spln_idx)
            print("self.one_spln_blast_species_csv", self.one_spln_blast_species_csv)

            try:
                # positional reindexing
                cur_df = pd.read_csv(self.one_spln_blast_species_csv, sep='\t', header=None,
                                     comment='#',
                                     index_col=[0], usecols=[0, 1], names=['species', 'reads']

                                     )

                human_df = cur_df[cur_df.index == 'human ']
                print("human_df.shape: ", human_df.shape[0])
                human = human_df.iloc[0]["reads"] if human_df.shape[0] == 1 else 0
                mouse_df = cur_df[cur_df.index == 'mouse ']
                print("mouse_df.shape: ", mouse_df.shape[0])
                mouse = mouse_df.iloc[0]["reads"] if mouse_df.shape[0] == 1 else 0

                # human_df = cur_df[cur_df['stridx'].str.contains("human|bal")]


                # Setting With Enlargement
                df_.loc[len(df_) + 1] = [self.spln_smp, self.spln_runreg, self.spln_lib,
                                         human,
                                         mouse]


            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

                # print("cur_df: ", cur_df)
                # print("index: ", cur_df.index)

                # cur_df['stridx']= cur_df.index


                # human_df = cur_df[cur_df["species"] == 'mouse ']

        df_.sort(['sample'], ascending=[1], inplace=True)
        df_ = df_.reset_index(drop=True)

        print("df_: \n", df_)

        df_.to_csv(self.all_spln_blast_stats_csv,
                   sep=',', header=True
                   , index=False
                   # , index_label = "idx"
                   )

    def stats_insert(self):

        columns = ['sample', 'run_reg', 'library',
                   'median_insert_size',
                   'mean_insert_size',
                   'median_absolute_deviation',
                   'standard_deviation',
                   'min_insert_size',
                   'max_insert_size',
                   'read_pairs']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)

        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'run_reg': 'object', 'library': 'object',
                  'median_insert_size': 'float64',
                  'mean_insert_size': 'float64',
                  'median_absolute_deviation': 'float64',
                  'standard_deviation': 'float64',
                  'min_insert_size': 'float64',
                  'max_insert_size': 'float64',
                  'read_pairs': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        # print("df_: \n", df_, df_.dtypes)


        for spln_idx in range(self.nb_splns):
            # for spln_idx in range(1):
            # print("spln_idx: %s" % spln_idx)

            # set index in order to make all path work
            self.spln_idx = spln_idx
            print("self.spln_idx: ", self.spln_idx)

            try:
                # positional reindexing
                # cur_df = pd.read_csv(self.one_spln_insert_stats_csv, sep='\t', header=1,
                #                      comment='#',
                #                      index_col=None,
                #                      index_col=False
                # usecols=[0, 1, 2, 3, 4, 5, 6],
                # nrows=2
                # ,
                # names=["library", "perc_dup", "library_size"]
                # )
                print("self.one_spln_insert_stats_csv: ", self.one_spln_insert_stats_csv)

                cur_df = pd.read_csv(self.one_spln_insert_stats_csv, sep='\t',
                                     header=0,
                                     comment='#',
                                     index_col=None,
                                     # index_col=False
                                     usecols=[0, 1, 2, 3, 4, 5, 6],
                                     nrows=1
                                     # names=["library", "perc_dup", "library_size"]
                                     )
            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            print("cur_df: ", cur_df)
            cur_df = cur_df.iloc[0]
            print("cur_df: ", cur_df)

            median_insert_size = cur_df["MEDIAN_INSERT_SIZE"]
            median_absolute_deviation = cur_df["MEDIAN_ABSOLUTE_DEVIATION"]

            min_insert_size = cur_df["MIN_INSERT_SIZE"]
            max_insert_size = cur_df["MAX_INSERT_SIZE"]

            mean_insert_size = cur_df["MEAN_INSERT_SIZE"]
            standard_deviation = cur_df["STANDARD_DEVIATION"]
            read_pairs = cur_df["READ_PAIRS"]

            # Setting With Enlargement
            df_.loc[len(df_) + 1] = [self.spln_smp, self.spln_runreg, self.spln_lib,
                                     median_insert_size,
                                     mean_insert_size,
                                     median_absolute_deviation,
                                     standard_deviation,
                                     min_insert_size,
                                     max_insert_size,
                                     read_pairs]

        print("df_: \n", df_)

        df_.to_csv(self.all_spln_insert_stats_csv,
                   sep=',', header=True
                   , index=False
                   # , index_label = "idx"
                   )

    def stats_spln_dupl(self):

        print("in %s ...." % thiscallersname())
        func_impl = thiscallersname() + "_" + self.conf.rmdup_type

        print("func_impl: ", func_impl)
        self.__getattribute__(func_impl)()

    def stats_spln_dupl_picard(self):
        pass

    def stats_spln_dupl_hplexhs(self):

        columns = ['sample', 'run_reg', 'library',
                   'proper', 'fq_read_cnt', 'idx_cnt', 'dedup_cnt',
                   'min', 'max', 'median', 'mad', 'q25', 'q75', 'mean', 'std']
        df_ = pd.DataFrame(columns=columns)
        print("df_: \n", df_, df_.dtypes)

        # dtypes = ['object', 'int64', 'int64', 'int64', 'int64', 'int64']
        # dtype = {'sample': np.object, 'on_unique': np.int64, 'on_multiple': np.int64,
        #        'off_unique': np.int64, 'off_multiple': np.int64,
        #        'unmapped': np.int64}

        dtypes = {'sample': 'object', 'run_reg': 'object', 'library': 'object',
                  'proper': 'float64', 'fq_read_cnt': 'float64', 'idx_cnt': 'float64', 'dedup_cnt': 'float64',
                  'min': 'float64', 'max': 'float64', 'median': 'float64', 'mad': 'float64',
                  'q25': 'float64', 'q75': 'float64', 'mean': 'float64', 'std': 'float64'}

        for c in df_.columns:
            df_[c] = df_[c].astype(dtypes[c])
            # print(mydata['y'].dtype)   #=> int64
        # print("df_: \n", df_, df_.dtypes)


        for spln_idx in range(self.nb_splns):
            # for spln_idx in range(1):
            # print("spln_idx: %s" % spln_idx)

            # set index in order to make all path work
            self.spln_idx = spln_idx
            print("self.spln_idx: ", self.spln_idx)

            try:
                # positional reindexing
                # cur_df = pd.read_csv(self.one_spln_insert_stats_csv, sep='\t', header=1,
                #                      comment='#',
                #                      index_col=None,
                #                      index_col=False
                # usecols=[0, 1, 2, 3, 4, 5, 6],
                # nrows=2
                # ,
                # names=["library", "perc_dup", "library_size"]
                # )

                cur_df = pd.read_csv(self.one_hplex_qc_stats_csv, sep=',')

                # ,
                # header=True,
                # comment='#',
                # index_col=None,
                # index_col=False,
                #
                # usecols=[0, 1, 2],
                # nrows=1,
                # names=['idx', 'proper','fq_read_cnt']
                # )

            except Exception as inst:
                print(type(inst))  # the exception instance
                print(inst.args)  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                next

            print("cur_df: ", cur_df)
            cur_df = cur_df.iloc[0]
            print("cur_df: ", cur_df)

            # Setting With Enlargement
            df_.loc[len(df_) + 1] = [self.spln_smp, self.spln_runreg, self.spln_lib,
                                     cur_df['proper'],
                                     cur_df['fq_read_cnt'],
                                     cur_df['idx_cnt'],
                                     cur_df['dedup_cnt'],
                                     cur_df['min'],
                                     cur_df['max'],
                                     cur_df['median'],
                                     cur_df['mad'],
                                     cur_df['q25'],
                                     cur_df['q75'],
                                     cur_df['mean'],
                                     cur_df['std']]

        print("df_: \n", df_)

        df_.to_csv(self.all_hplex_qc_stats_csv,
                   sep=',', header=True
                   , index=False
                   # , index_label = "idx"
                   )

    # --------------------------  RNA specific variables ----------------------------------------
    # ---------------------------  readcount genes
    @property
    def genes_readcnt_tracking_f(self):
        return os.path.join(self.align_smpgrp_dir, "%s.readcounts.%s.csv" % (self.smpgrp_name, self.readcnt_prefix))

    @property
    def all_genes_readcnt_tracking_ids_df(self):
        return self.__all_genes_readcnt_tracking_ids_df

    @all_genes_readcnt_tracking_ids_df.setter
    def all_genes_readcnt_tracking_ids_df(self, val):
        self.__all_genes_readcnt_tracking_ids_df = val

    @property
    def all_genes_readcnt_tracking_ids_f(self):
        return os.path.join(self.conf.matr_dir, "%s.genes_readcnt_tracking_ids.csv" % self.readcnt_prefix)

    @property
    def readcnt_known_genes_matrix_df(self):
        return self.__all_genes_readcnt_tracking_df

    @readcnt_known_genes_matrix_df.setter
    def readcnt_known_genes_matrix_df(self, val):
        self.__all_genes_readcnt_tracking_df = val

    @property
    def readcnt_known_genes_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "%s.%s.readcnt.known.genes.csv" % (self.conf.proj_abbrev, self.readcnt_prefix))



    @property
    def qnsorted_bam(self):
        return os.path.join(self.align_smpgrp_dir_glo, "%s.queryNameSorted.bam" % self.smpgrp_name)

    @property
    def htseq_out_sam(self):
        return os.path.join(self.align_smp_dir, "%s.htseq_out.sam" % self.smp_name)

    # ############################### fpkm properties  ******************************
    @property
    def fpkm_known_dir(self):
        this_dir = os.path.join(self.align_smpgrp_dir, "fpkm_known")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            print(("created: ", this_dir))
        return this_dir

    @property
    def fpkm_rabt_dir(self):
        this_dir = os.path.join(self.align_smpgrp_dir, "fpkm_rabt")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            print(("created: ", this_dir))
        return this_dir

    @property
    def fpkm_denovo_dir(self):
        this_dir = os.path.join(self.align_smpgrp_dir, "fpkm_denovo")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            print(("created: ", this_dir))
        return this_dir

    #  ---------------------------  fpkm genes
    @property
    def genes_fpkm_tracking_f(self):
        return os.path.join(self.fpkm_known_dir, "genes.fpkm_tracking")

    @property
    def all_genes_fpkm_tracking_ids_df(self):
        return self.__all_genes_fpkm_tracking_ids_df

    @all_genes_fpkm_tracking_ids_df.setter
    def all_genes_fpkm_tracking_ids_df(self, val):
        self.__all_genes_fpkm_tracking_ids_df = val

    @property
    def all_genes_fpkm_tracking_ids_f(self):
        return os.path.join(self.conf.matr_dir, "all_genes_fpkm_tracking_ids.csv")

    @property
    def fpkm_known_genes_matrix_df(self):
        return self.__all_genes_fpkm_tracking_df

    @fpkm_known_genes_matrix_df.setter
    def fpkm_known_genes_matrix_df(self, val):
        self.__all_genes_fpkm_tracking_df = val

    @property
    def fpkm_known_genes_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "all.fpkm.known.genes.csv")

    # --------------------------- fpkm transcripts

    @property
    def isofs_fpkm_tracking_f(self):
        return os.path.join(self.fpkm_known_dir, "isoforms.fpkm_tracking")

    @property
    def fpkm_known_isofs_matrix_df(self):
        return self.__fpkm_known_isofs_matrix_df

    @fpkm_known_isofs_matrix_df.setter
    def fpkm_known_isofs_matrix_df(self, val):
        self.__fpkm_known_isofs_matrix_df = val

    @property
    def fpkm_known_isofs_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "all.fpkm.known.isofs.csv")

    # ------------------------   kallisto transcripts

    @property
    def kallisto_smp_dir(self):
        this_dir = os.path.join(self.align_smp_dir, "kallisto_output")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
            print(("created: ", this_dir))
        return this_dir

    @property
    def isofs_kallisto_tracking_f(self):
        return os.path.join(self.kallisto_smp_dir, "abundance.tsv")

    @property
    def kallisto_isofs_matrix_f(self):
        return os.path.join(self.conf.matr_dir, "kallisto.isofs.csv")

    # -------------------------   c3p related

    @property
    def c3p_size(self):
        return self.__c3p_size

    @c3p_size.setter
    def c3p_size(self, val):
        self.__c3p_size = val

    @property
    def c3p_smp_idx(self):
        return int(self.__c3p_smp_idx)

    @c3p_smp_idx.setter
    def c3p_smp_idx(self, val):
        self.__c3p_smp_idx = val

    @property
    def c3p_smp_name(self):
        return self.conf.sample_names.iloc[self.c3p_smp_idx, 0]

    @property
    def c3p_contig_name(self):
        return self.__c3p_contig_name

    @c3p_contig_name.setter
    def c3p_contig_name(self, val):
        self.__c3p_contig_name = val

    @property
    def c3p_smp_size_dir(self):
        loc_dir = os.path.join(self.conf.align2_dir, self.c3p_smp_name, "c3p_smp_%s" % self.c3p_size)
        if not os.path.exists(loc_dir):
            os.makedirs(loc_dir)
        return loc_dir

    @property
    def c3p_smp_size_contig_gtf(self):
        return os.path.join(self.c3p_smp_size_dir, "c3p_%s.gtf" % self.c3p_contig_name)

    @property
    def c3p_smp_size_gtf(self):
        return os.path.join(self.c3p_smp_size_dir, "c3p.gtf")

    # ---------------------------- RNA specific -----------------------------------------------


    # only takes the uniques for rnaseq expression
    def merge_smpgrps_exp(self):

        print("------------------------ Entering %s ....  ----------------" % thiscallersname())
        exit_codes = []
        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)


        smpgrps_smps_df = self.conf.sample_grps_smps
        # print("smpgrps_smps_df: \n", smpgrps_smps_df)

        # lanes_df = lanes_df[lanes_df['Name'] == self.smp_name].reset_index(drop=True)

        # filter
        smpgrps_smps_df = smpgrps_smps_df[smpgrps_smps_df.index == self.smpgrp_idx]

        print("-------------")
        print(smpgrps_smps_df)

        smps_spl_acc_st = ""
        smps_spl_err_ar = []

        # erase output files
        # this ensures secure restart from partial previous output
        for i in [self.grp_exp_am, self.grp_exp_ai]:
            if os.path.islink(i):
                os.unlink(i)
            if os.path.exists(i):
                os.remove(i)

        # for f in glob.glob(self.mer_smp_glob):
        #     os.remove(f)

        for idx, row in smpgrps_smps_df.iterrows():
            print("row: ", row[0])
            self.smp_idx = row[0]

            # print("self.smp_idx: ", self.smp_idx, "self.smp_row: ", self.smp_row)
            print("self.smp_idx: ", self.smp_idx)
            print("self.smp_spl_ddp_am: ", self.smp_spl_ddp_am)

            # check alignment files and add them to the list to be merged
            # there is no realignment like grind_smps
            if os.path.exists(self.smp_spl_ddp_am):
                smps_spl_acc_st += r"""%s """ % self.smp_spl_ddp_am

        # f1 = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam" % self.smpgrp_name)
        # f1_bai = os.path.join(self.align_smpgrp_dir_glo, "%s.smp.f1.bam.bai" % self.smpgrp_name)


        # clean input files
        # for i in [f1, f1_bai]:
        #     if os.path.exists(i):
        #         os.remove(i)

        smps_len = smps_spl_acc_st.split().__len__()

        # print("smps_rca_acc_st: ", smps_rca_acc_st)
        # print("smps_len: ", smps_len)

        # one sample does not need renaming
        # link directly to final output
        if smps_len == 0:

            # if merging is not specified, take the same sample name and link later
            # merging has to be specified only for supplementary smpgrp samples

            self.smp_idx = self.smpgrp_idx
            os.symlink(self.smp_spl_ddp_am, self.grp_exp_am)
            os.symlink(self.smp_spl_ddp_ai, self.grp_exp_ai)

            # shutil.copy(self.smp_spl_ddp_ai, self.grp_exp_ai)

            exit_codes.append([0])
        elif smps_len == 1:
            # to implement doing copies
            exit(0)

        elif smps_len > 1:

            # cmd = r"""sambamba merge -t 12 %(fout)s %(lanes_st_acc)s""" % {"lanes_st_acc": smps_spl_acc_st,
            #                                                                "fout": f1}
            cmd = r"""sambamba merge -t 12 /dev/stdout %(lanes_st_acc)s""" % {"lanes_st_acc": smps_spl_acc_st}

            print(cmd)
            p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
            # p1.wait()
            # rc1 = p1.returncode
            # print("rc1: ", rc1)
            # exit_codes.append([rc1])

            # rename sample to new smpgrp name
            cmd = r"""java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=8 \
-Xms8G -Xmx12G \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} AddOrReplaceReadGroups \
RGID=raglab_id RGLB=ILLUMINA RGPL=illumina_pl RGPU=raglab_pu RGSM=%(smp)s \
TMP_DIR=%(scratch_d)s \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
I=/dev/stdin \
O=%(fout)s
""" % {"scratch_d": self.conf.scratch_loc_dir,
       # "fin": f1,
       "fout": self.grp_exp_am,
       "smp": self.smpgrp_name}
            print(cmd)
            p2 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdin=p1.stdout,
                                  stdout=subprocess.PIPE)

            exit_codes_stage = [pp.wait() for pp in [p1, p2]]
            print("exit_codes stage: ", exit_codes_stage)
            exit_codes.extend(exit_codes_stage)

            # temporary file
            # for i in [f1, f1_bai]:
            #     if os.path.exists(i):
            #         os.remove(i)

        self.mark_exit_args(exit_codes, args)


    def qnsort(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []
        args = {"smpgrp_idx": self.smpgrp_idx}
        print("parameters: \n", args)

        for i in [self.qnsorted_bam]:
            if os.path.exists(i):
                os.remove(i)

        cmd = r"""cat %(in_bam)s %(filter)s | java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx1700M \
-Dsamjdk.use_async_io=true \
-jar ${PICARD_JAR} SortSam \
VALIDATION_STRINGENCY=SILENT \
CREATE_INDEX=true \
INPUT=/dev/stdin \
OUTPUT=%(out_bam)s \
SORT_ORDER=queryname \
MAX_RECORDS_IN_RAM=500000
""" % {"scratch_d": self.conf.scratch_loc_dir,
       "in_bam": self.grp_exp_am,
       "filter": self.sambamba_filter_dup(self.conf.spl_use),
       "out_bam": self.qnsorted_bam}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        exit_codes.append([rc1])

        self.mark_exit_args(exit_codes, args)

    def raw_counts_prefix(self):

        # cmd = r"""samtools view %(qnsorted_bam)s | \
        # htseq-count - %(tra_gtf)s  \
        # -r name -t exon -i gene_id \
        # -a 0 -f sam \
        # -s no -m intersection-nonempty \
        # > %(readcounts_csv)s
        # -o %(htseq_out_sam)s
        # """ % {"tra_gtf": self.readcnt_gtf,
        #        "qnsorted_bam": self.qnsorted_bam,
        #        "htseq_out_sam": self.htseq_out_sam,
        #        "readcounts_csv": self.genes_readcnt_tracking_f}

        # r pos/ name

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
samtools view %(in_bam)s | htseq-count - %(tra_gtf)s -f sam %(extra)s \
> %(readcounts_csv)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "tra_gtf": self.readcnt_gtf,
       "in_bam": self.qnsorted_bam,
       "extra": self.conf.raw_counts_extra,
       "readcounts_csv": self.genes_readcnt_tracking_f}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir_glo, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))

        return [rc1]

        # generate bulk gtf
        # self.c3p_gtf_gen()

    def raw_counts(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print(("parameters: \n", args))

        # normal run using the transcriptome gtf
        self.readcnt_prefix = "all"
        self.readcnt_gtf = self.conf.ref_gen_transcr_gtf
        rc = self.raw_counts_prefix()
        exit_codes.append(rc)

        # corrected run using the bulk gtf
        # self.readcnt_prefix = "bulk_%s" % self.c3p_size
        # self.readcnt_gtf = self.c3p_smp_size_gtf
        # rc = self.raw_counts_prefix()
        # exit_codes.append(rc)

        self.mark_exit_args(exit_codes, args)

    def fpkm_known(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print(("parameters: \n", args))

        # remove output folder
        if os.path.exists(self.fpkm_known_dir):
            shutil.rmtree(self.fpkm_known_dir, ignore_errors=False, onerror=None)

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
cat %(in_bam)s %(filter)s | cufflinks -G %(tra_gtf)s -o %(fpkm_known_dir)s -p %(cor_tsk)s \
%(extra)s -
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "cor_tsk": self.cor_tsk,
       "mem_tsk": self.mem_tsk,
       "in_bam": self.grp_exp_am,
       "filter": self.sambamba_filter_dup(self.conf.spl_use),
       "tra_gtf": self.tra_gtf,
       "fpkm_known_dir": self.fpkm_known_dir,
       "extra": self.conf.fpkm_known_extra}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))

        self.mark_exit_args(exit_codes, args)

    def fpkm_rabt(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print(("parameters: \n", args))

        # remove output folder
        if os.path.exists(self.fpkm_rabt_dir):
            shutil.rmtree(self.fpkm_rabt_dir, ignore_errors=False, onerror=None)

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
cufflinks -q -g %(tra_gtf)s \
--max-bundle-frags 1000000 --library-type fr-unstranded -p 8 \
-o %(fpkm_rabt_dir)s \
%(rmdup_bam)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "tra_gtf": self.tra_gtf,
       "rmdup_bam": self.grp_exp_am,
       "fpkm_rabt_dir": self.fpkm_rabt_dir}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))

        self.mark_exit_args(exit_codes, args)

    def fpkm_denovo(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print(("parameters: \n", args))

        # remove output folder
        if os.path.exists(self.fpkm_denovo_dir):
            shutil.rmtree(self.fpkm_denovo_dir, ignore_errors=False, onerror=None)

        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
cufflinks -q \
--max-bundle-frags 1000000 --library-type fr-unstranded -p 8 \
-o %(fpkm_denovo_dir)s \
%(rmdup_bam)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "rmdup_bam": self.grp_exp_am,
       "fpkm_denovo_dir": self.fpkm_denovo_dir}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))

        self.mark_exit_args(exit_codes, args)

    def wiggle(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        args = {"smpgrp_idx": self.smpgrp_idx}
        print(("parameters: \n", args))

        print("self.conf.qc_grp_track_file: ", self.conf.qc_grp_track_file)
        print("self.conf.qc_grp_track_flts_dup: ", self.conf.qc_grp_track_flts_dup)
        print("self.conf.qc_grp_track_flts_uni: ", self.conf.qc_grp_track_flts_uni)
        print("self.conf.qc_grp_track_flts_stn: ", self.conf.qc_grp_track_flts_stn)

        # clss = list(map(int, np.unique(self.conf.sample_grps['cls_idx'])))
        # print("jobs: 11111", clss, type(clss[0]))

        for fl in self.conf.qc_grp_track_file:
            self.qc_grp_track_file = fl

            if fl == "gatk":
                inbam = self.rca_smpgrp_bam
            elif fl == "oth":
                inbam = self.ral_smpgrp_bam
            elif fl == "exp":
                inbam = self.grp_exp_am

            # for cls in clss:
            #     self.cls_idx = cls

            for flt_dup in self.conf.qc_grp_track_flts_dup:
                self.qc_grp_track_flt_dup = flt_dup

                for flt_uni in self.conf.qc_grp_track_flts_uni:
                    self.qc_grp_track_flt_uni = flt_uni

                    for flt_stn in self.conf.qc_grp_track_flts_stn:
                        self.qc_grp_track_flt_stn = flt_stn

                        # erase output files
                        # this ensures secure restart from partial previous output
                        for i in [self.bed_graph, self.big_wig]:
                            if os.path.exists(i):
                                os.remove(i)

                                # nmblines=$(samtools view -F 256 -f 81 \
                                # %(rmdup_bam)s | wc -l)
                                # $ scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc)

                        cmd = r"""cat %(fin)s %(filter)s | bedtools genomecov -bg -ibam stdin \
                -g %(chrominfo)s \
                -split -scale %(scalefactor)s | LC_COLLATE=c sort -k1,1 -k2,2n > \
                %(bed_graph)s
                """ % {"fin": inbam,
                       "filter": self.sambamba_filter_txt(self.sambamba_filter_dup_uni_stn(flt_dup, flt_uni, flt_stn)),
                       "scalefactor": 1,
                       "chrominfo": self.conf.ref_gen_chrominfo,
                       "bed_graph": self.bed_graph}
                        print(cmd)
                        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
                        p1.wait()
                        rc1 = p1.returncode
                        print(("rc1: ", rc1))
                        exit_codes.append([rc1])

                        cmd = r"""bedGraphToBigWig %(in_bedgraph)s %(chrominfo)s %(out_bw)s
                """ % {"in_bedgraph": self.bed_graph,
                       "chrominfo": self.conf.ref_gen_chrominfo,
                       "out_bw": self.big_wig}
                        print(cmd)
                        p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smpgrp_dir, stdout=subprocess.PIPE)
                        p1.wait()
                        rc1 = p1.returncode
                        print(("rc1: ", rc1))
                        exit_codes.append([rc1])

                        # remove temporary files
                        for i in [self.bed_graph]:
                            if os.path.exists(i):
                                os.remove(i)

                                # crossmap has an own sampy which makes it incompatible with merge_lanes
                                # load_one_module('raglab/crossmap/0.1.8')
                                # cmd = r"""
                            # CrossMap.py wig $CROSSMAP_DIR/data/GRCh37ToHg19.over.chain.gz \
                            # %(bed_graph)s %(big_wig_prefix)s
                            # """ % {"bed_graph": self.bed_graph,
                            #        "big_wig_prefix": self.big_wig_prefix}
                            #         print(cmd)
                            #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
                            #         p1.wait()
                            #         rc1 = p1.returncode
                            #         print(("rc1: ", rc1))
                            #         exit_codes.append([rc1])

        # crossmap finished
        # unload_one_module('raglab/crossmap/0.1.8')

        self.mark_exit_args(exit_codes, args)

    def wiggle_x10(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        exit_codes = []

        # args = {"smpgrp_idx": self.smpgrp_idx}
        # print(("parameters: \n", args))

        # print("self.conf.qc_grp_track_file: ", self.conf.qc_grp_track_file)
        # print("self.conf.qc_grp_track_flts_dup: ", self.conf.qc_grp_track_flts_dup)
        # print("self.conf.qc_grp_track_flts_uni: ", self.conf.qc_grp_track_flts_uni)
        # print("self.conf.qc_grp_track_flts_stn: ", self.conf.qc_grp_track_flts_stn)

        # clss = list(map(int, np.unique(self.conf.sample_grps['cls_idx'])))
        # print("jobs: 11111", clss, type(clss[0]))

        # for fl in self.conf.qc_grp_track_file:
        #     self.qc_grp_track_file = fl
        #
        #     if fl == "gatk":
        #         inbam = self.rca_smpgrp_bam
        #     elif fl == "oth":
        #         inbam = self.ral_smpgrp_bam
        #     elif fl == "exp":
        #         inbam = self.grp_exp_am
        #     elif fl == "x10":


        tenx_dir = os.path.join(self.conf.proj_dir, "tenx")
        pool_smp_name = "SH0_MPS12342879_D11_1"
        inbam = os.path.join(tenx_dir, "%s" % pool_smp_name, "outs", "possorted_genome_bam.bam")
        print("inbam: ", inbam)

        flt_dup = "ded"
        flt_uni = "all"
        flt_stn = "uns"

        cluster = "p"

        big_wig_dir = os.path.join(tenx_dir, "ucsc_hub_x10", "big_wig", "%s-%s-%s" % (flt_dup, flt_uni, flt_stn))
        print("big_wig_dir: ", big_wig_dir)
        # if not os.path.exists(big_wig_dir):
        #     os.makedirs(big_wig_dir)

        bed_graph_f = os.path.join(big_wig_dir, "%s.%s.%s.%s.%s.bedGraph" % (pool_smp_name,flt_dup, flt_uni, flt_stn, cluster))
        big_wig_f = os.path.join(big_wig_dir, "%s.%s.%s.%s.%s.bw" % (pool_smp_name,flt_dup, flt_uni, flt_stn, cluster))

            # for cls in clss:
            #     self.cls_idx = cls

            # for flt_dup in self.conf.qc_grp_track_flts_dup:
            #     self.qc_grp_track_flt_dup = flt_dup

                # for flt_uni in self.conf.qc_grp_track_flts_uni:
                #     self.qc_grp_track_flt_uni = flt_uni

                    # for flt_stn in self.conf.qc_grp_track_flts_stn:
                    #     self.qc_grp_track_flt_stn = flt_stn

                        # erase output files
                        # this ensures secure restart from partial previous output
                        # for i in [self.bed_graph, self.big_wig]:
                        #     if os.path.exists(i):
                        #         os.remove(i)

                                # nmblines=$(samtools view -F 256 -f 81 \
                                # %(rmdup_bam)s | wc -l)
                                # $ scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc)

        cmd = r"""cat %(fin)s %(filter)s | bedtools genomecov -bg -ibam stdin \
-g %(chrominfo)s \
-split -scale %(scalefactor)s | LC_COLLATE=c sort -k1,1 -k2,2n > \
%(bed_graph)s
""" % {"fin": inbam,
       "filter": self.sambamba_filter_txt(self.sambamba_filter_dup_uni_stn(flt_dup, flt_uni, flt_stn)),
       "scalefactor": 1,
       "chrominfo": self.conf.ref_gen_chrominfo,
       "bed_graph": bed_graph_f}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=tenx_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append([rc1])

        cmd = r"""bedGraphToBigWig %(in_bedgraph)s %(chrominfo)s %(out_bw)s
""" % {"in_bedgraph": bed_graph_f,
       "chrominfo": self.conf.ref_gen_chrominfo,
       "out_bw": big_wig_f}
        print(cmd)
        p1 = subprocess.Popen(cmd, shell=True, cwd=tenx_dir, stdout=subprocess.PIPE)
        p1.wait()
        rc1 = p1.returncode
        print(("rc1: ", rc1))
        exit_codes.append([rc1])

                        # remove temporary files
                        # for i in [self.bed_graph]:
                        #     if os.path.exists(i):
                        #         os.remove(i)

                                # crossmap has an own sampy which makes it incompatible with merge_lanes
                                # load_one_module('raglab/crossmap/0.1.8')
                                # cmd = r"""
                            # CrossMap.py wig $CROSSMAP_DIR/data/GRCh37ToHg19.over.chain.gz \
                            # %(bed_graph)s %(big_wig_prefix)s
                            # """ % {"bed_graph": self.bed_graph,
                            #        "big_wig_prefix": self.big_wig_prefix}
                            #         print(cmd)
                            #         p1 = subprocess.Popen(cmd, shell=True, cwd=self.align_smp_dir, stdout=subprocess.PIPE)
                            #         p1.wait()
                            #         rc1 = p1.returncode
                            #         print(("rc1: ", rc1))
                            #         exit_codes.append([rc1])

        # crossmap finished
        # unload_one_module('raglab/crossmap/0.1.8')

        # self.mark_exit_args(exit_codes, args)


    def deploy_web(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        print("Not implemented error codes...\n")

        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        if self.rprt_idx == 0:
            # clean deploy dir
            # for f in glob.glob(self.conf.deploy_dir_glob):
            #     print("f: ", f)
            #     if os.path.islink(f):
            #         os.unlink(f)
            #     else:
            #         shutil.rmtree(f, ignore_errors=False, onerror=None)

            # link rnaseqc reports
            if os.path.islink(self.rnaseqc_depl_link):
                os.remove(self.rnaseqc_depl_link)
            os.symlink(self.rnaseqc_dir, self.rnaseqc_depl_link)

            # link rseqc reports
            if os.path.islink(self.rseqc_depl_link):
                os.unlink(self.rseqc_depl_link)

            os.symlink(self.rseqc_dir, self.rseqc_depl_link)

            # link matrices
            if os.path.islink(self.matr_depl_link):
                os.unlink(self.matr_depl_link)
            os.symlink(self.conf.matr_dir, self.matr_depl_link)

            # link metrics
            if os.path.islink(self.metr_depl_link):
                os.unlink(self.metr_depl_link)
            os.symlink(self.conf.metr_dir, self.metr_depl_link)

            # link vcfs
            if os.path.islink(self.vcfs_depl_link):
                os.unlink(self.vcfs_depl_link)
            os.symlink(self.conf.vcfs_dir, self.vcfs_depl_link)

            # link sc_qc_figs
            if os.path.islink(self.sc_qc_figs_depl_link):
                os.unlink(self.sc_qc_figs_depl_link)
            os.symlink((os.path.join(self.conf.sc_qc_dir, "figs")), self.sc_qc_figs_depl_link)


            # materialize aligned files
            # link matrices

            # for idx in range(self.nb_smpgrps):
            # for idx in range(2):
            # set index in order to make all path work
            #   self.smpgrp_idx = idx
            # print(("self.smpgrp_idx: ", self.smpgrp_idx))
            # print(("self.smpgrp_name: ", self.smpgrp_name))    # read sample matrix
            #  if os.path.exists(self.deploy_spl_smpgrp_bam):
            #      os.remove(self.deploy_spl_smpgrp_bam)
            # os.symlink(self.grp_exp_am, self.deploy_spl_smpgrp_bam)

            # if os.path.exists(self.deploy_spl_smpgrp_bai):
            #   os.remove(self.deploy_spl_smpgrp_bai)
            # os.symlink(self.grp_exp_ai, self.deploy_spl_smpgrp_bai)

            # deploy bigwig links
            clsidxes = np.unique(self.conf.sample_grps['cls_idx'].values)
            # print("clsidxes: ", clsidxes)
            for clsidx in clsidxes:
                self.cls_idx = clsidx

                smpgrps_df = self.conf.sample_grps[self.conf.sample_grps['cls_idx'] == self.cls_idx]
                smpgrps_df = smpgrps_df.query('skip != 1')

                for idx, row in smpgrps_df.iterrows():
                    # for idx in self.conf.sample_grps.query('skip != 1').index.values:
                    # for smp_idx in range(1):
                    # set index in order to make all path work
                    self.smpgrp_idx = idx
                    print("self.smpgrp_idx: ", self.smpgrp_idx)
                    print("self.smpgrp_name: ", self.smpgrp_name)
                    # filter

                    for fl in self.conf.qc_grp_track_file:
                        self.qc_grp_track_file = fl

                        for flt_dup in self.conf.qc_grp_track_flts_dup:
                            self.qc_grp_track_flt_dup = flt_dup

                            for flt_uni in self.conf.qc_grp_track_flts_uni:
                                self.qc_grp_track_flt_uni = flt_uni

                                for flt_stn in self.conf.qc_grp_track_flts_stn:
                                    self.qc_grp_track_flt_stn = flt_stn

                                    # link
                                    if os.path.islink(self.deploy_big_wig_link):
                                        os.unlink(self.deploy_big_wig_link)
                                    os.symlink(self.big_wig, self.deploy_big_wig_link)

            self.deploy_ucsc_hub()

            exit_codes.extend([0])
        else:
            pass

        self.mark_exit_args(exit_codes, args)


    def deploy_ucsc_hub(self):

        # deploy hub.txt
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(self.conf.templ_jinja_dir))
        template = env.get_template(self.ucsc_hub_jinja_name)
        template_vars = {"abbrev": self.conf.proj_abbrev}
        txt_out = template.render(template_vars)

        f = open(self.ucsc_hub_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # genomes
        template = env.get_template(self.ucsc_genomes_jinja_name)
        template_vars = {"ucsc_genome": self.conf.ucsc_genome}
        txt_out = template.render(template_vars)

        f = open(self.ucsc_genomes_depl_file, 'w')
        f.write(txt_out)
        f.close()

        # for idx in range(self.nb_samples):
        #     self.smp_idx = idx
        #     print("smp_idx: ", self.smp_idx, "smp_name: ", self.smp_name)
        # print("samples: ", samples)

        conditions = []
        cond_idx = 0

        clsidxes = np.unique(self.conf.sample_grps['cls_idx'].values)
        # print("clsidxes: ", clsidxes)


        # track_db content
        for fl in self.conf.qc_grp_track_file:
            self.qc_grp_track_file = fl

            for flt_dup in self.conf.qc_grp_track_flts_dup:
                self.qc_grp_track_flt_dup = flt_dup

                for flt_uni in self.conf.qc_grp_track_flts_uni:
                    self.qc_grp_track_flt_uni = flt_uni

                    for clsidx in clsidxes:
                        self.cls_idx = clsidx

                        # here we have conditions

                        grname = "%s-%s-%s-%s" % (self.qc_grp_track_file,
                                                  self.qc_grp_track_flt_dup,
                                                  self.qc_grp_track_flt_uni,
                                                  str(self.cls_idx))

                        # now extract the samples
                        samples = []
                        smpgrps_df = self.conf.sample_grps[self.conf.sample_grps['cls_idx'] == self.cls_idx]
                        smpgrps_df = smpgrps_df.query('skip != 1')

                        for idx, row in smpgrps_df.iterrows():
                            # for idx in self.conf.sample_grps.query('skip != 1').index.values:
                            # for smp_idx in range(1):
                            # set index in order to make all path work
                            self.smpgrp_idx = idx
                            print("self.smpgrp_idx: ", self.smpgrp_idx)
                            print("self.smpgrp_name: ", self.smpgrp_name)
                            # multiply by strands
                            for flt_stn in self.conf.qc_grp_track_flts_stn:
                                self.qc_grp_track_flt_stn = flt_stn

                                fname = "%s.grp.%s.%s.%s.%s.bw" % (self.smpgrp_name,
                                                                   self.qc_grp_track_file,
                                                                   self.qc_grp_track_flt_dup,
                                                                   self.qc_grp_track_flt_uni,
                                                                   self.qc_grp_track_flt_stn)

                                smp_item = dict(grname=grname,
                                                idx=self.smpgrp_idx,
                                                name=fname)
                                samples.append(smp_item)

                        cond_item = dict(idx=cond_idx, name=grname, samples=samples)

                        conditions.append(cond_item)
                        cond_idx += 1

                        print("conditions: ", conditions)

        # template
        template = env.get_template(self.ucsc_track_db_jinja_name)
        template_vars = {"conditions": conditions,
                         "samples": samples}
        txt_out = template.render(template_vars)

        f = open(self.ucsc_track_db_depl_file, 'w')
        f.write(txt_out)
        f.close()

    def load_genes_tracking_ids(self):
        self.genes_tracking_ids_df = pd.read_csv(self.conf.genes_tracking_ids_csv,
                                                 sep=',', header=0,
                                                 index_col=[0], usecols=[0, 1, 2, 3],
                                                 names=["idx", "gene_id", "gene_name", "source"])

        # , dtype={"idx": np.int, "tracking_id": np.object}
        # print track_df
        print(("self.genes_tracking_ids_df: \n", self.genes_tracking_ids_df))

    def readcnt_known_genes_matrix(self):

        # calculate initial matrix
        print("in readcnt_known_genes_matrix...")

        # initially only ids
        self.genes_matrix_df = self.genes_tracking_ids_df.copy(deep=True)
        self.genes_matrix_df.set_index(["gene_id"], inplace=True)

        print(("self.genes_matrix_df: ", self.genes_matrix_df))
        cur_df = pd.DataFrame()

        for smpgrp_idx in range(self.nb_smpgrps):
            # for smp_idx in range(2):
            # set index in order to make all path work
            self.smpgrp_idx = smpgrp_idx
            print(("self.smpgrp_idx: ", self.smpgrp_idx))
            print(("self.smpgrp_name: ", self.smpgrp_name))  # read sample matrix
            try:
                cur_df = pd.read_csv(self.genes_readcnt_tracking_f, sep='\t', header=None,
                                     # index_col=[0],
                                     usecols=[0, 1],
                                     names=["gene_id", "readcnt"])
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                continue

            cur_agg = cur_df.groupby(["gene_id"])
            cur_cnt = cur_agg["readcnt"].agg({'cnt_avg': np.average})

            print(("cur_cnt.shape: ", cur_cnt.shape))

            self.genes_matrix_df = pd.merge(self.genes_matrix_df, cur_cnt,
                                            how='inner', left_index=True, right_index=True)
            self.genes_matrix_df.rename(columns={'cnt_avg': self.smpgrp_name}, inplace=True)
            print(("smpgrp_name: %s, rows: %s " % (self.smpgrp_name, self.genes_matrix_df.shape[0])))


    def save_readcnt_known_genes_matrix(self):
        self.genes_matrix_df.to_csv(self.readcnt_known_genes_matrix_f,
                                    sep=',', header=True, index=True, index_label="gene_id")



    def readcnt_rprt(self):

        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        print("Not implemented error codes...\n")

        args = {"rprt_idx": self.rprt_idx}
        print("parameters: \n", args)

        exit_codes = []

        spark, sc = self.servers_obj.get_new_spark_session(1, 10, "readcnt_rprt")
        # self.spark_session = spark

        if self.rprt_idx == 0:


            # normal run using the transcriptome gtf
            # ------------------------------------------------------------------
            self.readcnt_prefix = "all"

            # readcnt genes_matrix
            # self.readcnt_find_reference()
            # self.save_genes_readcnt_tracking_ids()
            # self.load_genes_readcnt_tracking_ids()

            # spark join
            # self.servers_obj.set_spark_session()

            self.sc_qc_obj.spark_parse_genes_tracking_ids()
            self.sc_qc_obj.spark_parse_sc_grp()
            self.sc_qc_obj.spark_parse_readcnt_known_genes_matrix()

            self.readcnt_prefix = "all"

            self.sc_qc_obj.spark_create_gene_filter(gene_filter="gene_trk_ids")

    # nsp.sc_qc_obj.gene_flt_smp_cnt_unpiv(include_meta_smps=False)

            self.sc_qc_obj.gene_flt_smp_cnt_unpiv(include_meta_smps=False)

            # self.servers_obj.close_spark_session()

            # pandas join
            # self.load_genes_tracking_ids()
            # self.readcnt_known_genes_matrix()
            # self.save_readcnt_known_genes_matrix()

            exit_codes.extend([0])
        else:
            pass

            # corrected run using the bulk gtf
            # ------------------------------------------------------------------
            # self.readcnt_prefix = "bulk_%s" % self.c3p_size

            # readcnt genes_matrix
            # self.readcnt_find_reference()
            # self.save_genes_readcnt_tracking_ids()
            # self.load_genes_readcnt_tracking_ids()

            # self.readcnt_known_genes_matrix()
            # self.save_readcnt_known_genes_matrix()


        spark.stop()

        self.mark_exit_args(exit_codes, args)


    # ---------------------  FPKM

    def load_isofs_tracking_ids(self):

        self.isofs_tracking_ids_df = pd.read_csv(self.conf.isofs_tracking_ids_csv,
                                                 sep=',', header=0,
                                                 index_col=[0], usecols=[0, 1, 2, 3, 4, 5],
                                                 names=["idx", "transcript_id", "transcript_name", "gene_id",
                                                        "gene_name", "source"])

        # , dtype={"idx": np.int, "tracking_id": np.object}
        # print track_df
        print(("self.isofs_tracking_ids_df: \n", self.isofs_tracking_ids_df))

    def fpkm_known_genes_matrix(self):

        # calculate initial matrix
        print("in fpkm_known_genes_matrix:")

        # initially only ids

        self.fpkm_known_genes_matrix_df = self.genes_tracking_ids_df.copy(deep=True)
        self.fpkm_known_genes_matrix_df.set_index(["gene_id"], inplace=True)

        # print("self.fpkm_known_genes_matrix_df: ===============================  \n", self.fpkm_known_genes_matrix_df)
        cur_df = pd.DataFrame()

        for idx in range(self.nb_smpgrps):
            # for smp_idx in range(2):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            # print(("self.smp_idx: ", self.smp_idx))
            # print(("self.smp_name: ", self.smp_name))    # read sample matrix
            try:
                cur_df = pd.read_csv(self.genes_fpkm_tracking_f,
                                     sep='\t', header=0
                                     # index_col=[0]
                                     , usecols=[0, 9]
                                     , names=["tracking_id", "fpkm"]
                                     # cur_df = pd.read_csv(os.path.join(self.conf.align2_dir,smp,"fpkm_known","genes.fpkm_tracking"),
                                     #              sep='\t', header = False,index_col=[0],usecols=[0,4,11],
                                     #              names=["track_id", "gene_short_name", "FPKM_conf_hi"]
                                     )
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                continue

                # print("cur_df : \n", cur_df)
                # if smp_idx == 0:
                # prim_df.drop('FPKM_conf_hi', axis=1, inplace=True)
                # print "prim_df: ",prim_df
                # print "res_df: ", res_df
                # cur_df.drop('gene_short_name', axis=1, inplace=True)

            cur_agg_fpkm = cur_df.groupby(["tracking_id"])
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum' : np.sum,'fpkm_mean' : np.mean})
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum': np.sum})
            cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_avg': np.average})

            # print("cur_fpkm --------------------------: \n", cur_fpkm.shape)
            print("cur_fpkm -++++++: \n", cur_fpkm)

            # print "cur_fpkm: ",cur_fpkm
            # gr5 = pd.DataFrame(,
            #                  cur_fpkm)
            # time.sleep(5)
            # gr7 = pd.DataFrame.from_items([('track_id', cur_agg_fpkm.groups.keys())])
            # print "cur_agg.shape: ",cur_agg.shape

            # print "gr7:  %s \n %s" % (gr7.shape,gr7)
            # print "gr7: ", gr7.shape
            # print "cur_fpkm: ",cur_fpkm.shape
            # gr7['fpkm']=cur_fpkm['fpkm']
            # print "gr7: ", gr7.shape
            # print gr7

            self.fpkm_known_genes_matrix_df = pd.merge(self.fpkm_known_genes_matrix_df, cur_fpkm,
                                                       how='inner', left_index=True, right_index=True)
            # ,suffixes=('', ''))
            # res_df.rename(columns={'fpkm_sum': smp}, inplace=True)
            self.fpkm_known_genes_matrix_df.rename(columns={'fpkm_avg': self.smpgrp_name}, inplace=True)
            print(("smp: %s, rows: %s " % (self.smpgrp_name, self.fpkm_known_genes_matrix_df.shape[0])))
            # print "res_df : ", res_df
            # time.sleep(10)
            # pdres.append(local["FPKM_conf_hi"])
            # res_df=pd.concat([res_df, local["FPKM_conf_hi"]], axis=1)
            #

            # print "gr8: ", gr8

    def fpkm_known_isofs_matrix(self):

        # calculate initial matrix
        print("in fpkm_known_isofs_matrix:")

        # initially only ids
        self.fpkm_known_isofs_matrix_df = self.isofs_tracking_ids_df.copy(deep=True)
        print(("self.fpkm_known_isofs_matrix_df: ", self.fpkm_known_isofs_matrix_df))
        self.fpkm_known_isofs_matrix_df.set_index(["transcript_id"], inplace=True)

        cur_df = pd.DataFrame()

        for idx in range(self.nb_smpgrps):
            # for smp_idx in range(2):
            # set index in order to make all path work
            self.smpgrp_idx = idx
            # print(("self.smp_idx: ", self.smp_idx))
            # print(("self.smp_name: ", self.smp_name))    # read sample matrix
            try:
                cur_df = pd.read_csv(self.isofs_fpkm_tracking_f,
                                     sep='\t', header=0
                                     # index_col=[0]
                                     , usecols=[0, 9]
                                     , names=["tracking_id", "fpkm"]
                                     # cur_df = pd.read_csv(os.path.join(self.conf.align2_dir,smp,"fpkm_known","genes.fpkm_tracking"),
                                     #              sep='\t', header = False,index_col=[0],usecols=[0,4,11],
                                     #              names=["track_id", "gene_short_name", "FPKM_conf_hi"]
                                     )
            except Exception as inst:
                print((type(inst)))  # the exception instance
                print((inst.args))  # arguments stored in .args
                print(inst)  # __str__ allows args to be printed directly
                print("smp: ERRORR----------------------------------")
                continue

                # print "cur_df : \n", cur_df
                # if smp_idx == 0:
                # prim_df.drop('FPKM_conf_hi', axis=1, inplace=True)
                # print "prim_df: ",prim_df
                # print "res_df: ", res_df
                # cur_df.drop('gene_short_name', axis=1, inplace=True)

            cur_agg_fpkm = cur_df.groupby(["tracking_id"])
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum' : np.sum,'fpkm_mean' : np.mean})
            # cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_sum': np.sum})
            cur_fpkm = cur_agg_fpkm["fpkm"].agg({'fpkm_avg': np.average})

            print(("cur_fpkm.shape: ", cur_fpkm.shape))

            # print "cur_fpkm: ",cur_fpkm
            # gr5 = pd.DataFrame(,
            #                  cur_fpkm)
            # time.sleep(5)
            # gr7 = pd.DataFrame.from_items([('track_id', cur_agg_fpkm.groups.keys())])
            # print "cur_agg.shape: ",cur_agg.shape

            # print "gr7:  %s \n %s" % (gr7.shape,gr7)
            # print "gr7: ", gr7.shape
            # print "cur_fpkm: ",cur_fpkm.shape
            # gr7['fpkm']=cur_fpkm['fpkm']
            # print "gr7: ", gr7.shape
            # print gr7

            self.fpkm_known_isofs_matrix_df = pd.merge(self.fpkm_known_isofs_matrix_df, cur_fpkm,
                                                       how='inner', left_index=True, right_index=True)
            # ,suffixes=('', ''))
            # res_df.rename(columns={'fpkm_sum': smp}, inplace=True)
            self.fpkm_known_isofs_matrix_df.rename(columns={'fpkm_avg': self.smpgrp_name}, inplace=True)
            print(("smp: %s, rows: %s " % (self.smpgrp_name, self.fpkm_known_isofs_matrix_df.shape[0])))
            # print "res_df : ", res_df
            # time.sleep(10)
            # pdres.append(local["FPKM_conf_hi"])
            # res_df=pd.concat([res_df, local["FPKM_conf_hi"]], axis=1)
            #

            # print "gr8: ", gr8

    def save_fpkm_known_genes_matrix(self):
        print("fpkm_known_genes_matrix_df: ", self.fpkm_known_genes_matrix_df)
        self.fpkm_known_genes_matrix_df.to_csv(self.fpkm_known_genes_matrix_f,
                                               sep=',', header=True, index=True, index_label="tracking_id")

    def save_fpkm_known_isofs_matrix(self):
        self.fpkm_known_isofs_matrix_df.to_csv(self.fpkm_known_isofs_matrix_f,
                                               sep=',', header=True, index=True, index_label="tracking_id")

    def fpkm_genes_rprt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        # fpkm_known_genes_matrix
        # self.fpkm_find_genes_reference()
        # self.save_genes_fpkm_tracking_ids()
        self.load_genes_tracking_ids()
        self.fpkm_known_genes_matrix()
        self.save_fpkm_known_genes_matrix()

        return [0]

    def fpkm_isofs_rprt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))
        # fpkm_known_isofs_matrix
        # self.fpkm_find_isofs_reference()
        # self.save_isofs_fpkm_tracking_ids()
        # self.load_isofs_fpkm_tracking_ids()
        self.load_isofs_tracking_ids()
        self.fpkm_known_isofs_matrix()
        self.save_fpkm_known_isofs_matrix()

        return [0]

    # this is at sample level
    def rnaseqc_data_ini(self):

        self.stage_abrev = "ini"

        smps_df = self.conf.sample_names[self.conf.sample_names['cls_idx'] == self.cls_idx]
        # smps_df = self.conf.sample_grps[self.conf.sample_grps['cls_idx'] == self.cls_idx]
        smps_df = smps_df.query('skip != 1')

        samples_df = pd.DataFrame(columns=('sample', 'bam', 'note'))

        for idx, row in smps_df.iterrows():
            self.smp_idx = idx
            #     print(("smp_idx: ", self.smp_idx))
            #     print(("smp_name: ", self.smp_name))

            # to have library deduplication levels smp_spl_ddp_am
            # otherwise

            if os.path.exists(self.smp_spl_ddp_am):
                samples_df.loc[idx] = [row["vshort_name"], self.smp_spl_ddp_am, self.smp_name]

        # save samples
        samples_df.to_csv(self.rnaseqc_samples_csv, sep='\t', header=True, index=False)

        return [0]

    def rnaseqc_data_fin(self):

        self.stage_abrev = "fin"

        samples_df = pd.DataFrame(columns=('sample', 'bam', 'note'))

        for idx in range(self.nb_smpgrps):
            self.smpgrp_idx = idx
            #     print(("smp_idx: ", self.smp_idx))
            #     print(("smp_name: ", self.smp_name))

            if os.path.exists(self.grp_exp_am):
                samples_df.loc[idx] = [self.smpgrp_name, self.grp_exp_am, "RNAseq"]

        # save samples
        samples_df.to_csv(self.rnaseqc_samples_csv, sep='\t', header=True, index=False)

        return [0]

    def rnaseqc_run(self):

        exit_codes = []

        # f3 = open(self.conf.rnaseqc_log, "w")

        # run report
        cmd = r"""module unload %(python3)s; \
module load %(python2)s; \
module unload %(jdk8_mod)s; \
module load %(jdk7_mod)s; \
java -Djava.io.tmpdir=%(scratch_d)s \
-XX:ParallelGCThreads=4 \
-Xms1000M -Xmx%(mem_tsk)sM \
-jar ${RNASEQC_JAR} -n 1000 -s %(samples_csv)s \
-BWArRNA  %(r_rna_fa)s \
-t %(tra_gtf)s \
-r %(ref_genome)s \
-o %(output_dir)s
""" % {"python3": self.conf.this_python_mod,
       "python2": self.conf.alt_python_mod,
       "jdk8_mod": self.conf.jdk8_mod,
       "jdk7_mod": self.conf.jdk7_mod,
       "scratch_d": self.conf.scratch_loc_dir,
       "mem_tsk": self.mem_tsk,
       "samples_csv": self.rnaseqc_samples_csv,
       "r_rna_fa": self.conf.ref_rrna_fa,
       "tra_gtf": self.conf.ref_transcr_exn_gtf,
       "ref_genome": self.conf.ref_gen_fa,
       "output_dir": self.rnaseqc_output_dir}
        print(cmd)
        # p2 = subprocess.Popen(cmd, shell=True, cwd=self.conf.rnaseqc_dir, stderr=f3, stdout=f3)
        p2 = subprocess.Popen(cmd, shell=True, cwd=self.rnaseqc_dir)
        p2.wait()
        rc2 = p2.returncode
        print(("rc2: ", rc2))
        exit_codes.append(rc2)

        # f3.close()

        return [exit_codes]

    def rnaseqc_ini_rprt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        args = {"cls_idx": self.cls_idx}
        print("parameters: \n", args)

        exit_codes = []

        exit_codes += self.rnaseqc_data_ini()

        exit_codes += self.rnaseqc_run()

        self.mark_exit_args(exit_codes, args)

    def rnaseqc_fin_rprt(self):
        print(("------------------------ Entering %s ....  ----------------" % thiscallersname()))

        exit_codes = []

        exit_codes += self.rnaseqc_data_fin()

        exit_codes += self.rnaseqc_run()

        return exit_codes

    def rna_rprts(self):

        args = {"rprt_idx": self.rprt_idx}
        print(("parameters: \n", args))

        exit_codes = []

        reports = {
            # 0: self.fpkm_genes_rprt,
            # 1: self.fpkm_isofs_rprt,
            2: self.readcnt_rprt
            # 3: self.kallisto_rprt,
            # 4: self.rnaseqc_ini_rprt,
            # 5: self.rnaseqc_fin_rprt,
            # 6: self.rpkmsat_rprt,
            # 7: self.deploy_web
        }

        exit_codes = reports[self.rprt_idx]()
        # print("exit_codes: ", exit_codes)

        self.mark_exit_args(exit_codes, args)

    # --------------------- JOB MANAGEMENT

    def push_qjobs_spln_idx(self):
        jobs_args = []

        splns = self.conf.sample_lanes.query('skip != 1').index.values

        # for spln_idx in range(self.nb_splns):
        for spln_idx in splns:
            # print("spln_idx: %s" % spln_idx)
            args = {"spln_idx": int(spln_idx)}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_smp_idx(self):
        jobs_args = []

        smps = self.conf.sample_names.query('skip != 1').index.values

        # for smp_idx in range(self.nb_samples):
        for smp_idx in smps:
            # print("sample: %s, %s" % smp_idx
            args = {"smp_idx": int(smp_idx)}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_smpgrp_idx(self):
        jobs_args = []

        smpgrps = self.conf.sample_grps.query('skip != 1').index.values
        # print("smpgrps: ", smpgrps)
        # for smpgrp_idx in range(self.nb_smpgrps):
        for smpgrp_idx in smpgrps:
            # print("smpgrp_idx: %s" % smpgrp_idx)
            args = {"smpgrp_idx": int(smpgrp_idx)}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_cls_idx_arr(self, ids):
        jobs_args = []
        for id in ids:
            print("id: %s" % id)
            args = {"cls_idx": id}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)


    def push_qjobs_smp_idx_contig_idx(self):
        jobs_args = []

        smps = self.conf.sample_names.query('skip != 1').index.values

        # for smp_idx in range(self.nb_samples):
        for smp_idx in smps:
            for contig_idx in range(self.nb_contigs):
                # print "sample,contig: %s, %s" % (smp_idx, contig_idx)

                # ordered
                args = OrderedDict([("smp_idx", int(smp_idx)), ("contig_idx", int(contig_idx))])

                # args = {"smp_idx": smp_idx, "contig_idx": contig_idx}
                jobs_args.append(args)

        self.redis_obj.qpush_jobs_args(jobs_args)

    def push_qjobs_smpgrp_idx_contig_idx(self):
        jobs_args = []

        smpgrps = self.conf.sample_grps.query('skip != 1').index.values
        # for smpgrp_idx in range(self.nb_smpgrps):
        for smpgrp_idx in smpgrps:
            for contig_idx in range(self.nb_contigs):
                # print "sample,contig: %s, %s" % (smpgrp_idx, contig_idx)

                args = OrderedDict([("smpgrp_idx", int(smpgrp_idx)), ("contig_idx", int(contig_idx))])
                # args = {"smpgrp_idx": smpgrp_idx, "contig_idx": contig_idx}

                jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_contig_idx(self):
        jobs_args = []
        for contig_idx in range(self.nb_contigs):
            # print("contig: %s" % (contig_idx)
            args = {"contig_idx": int(contig_idx)}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_noarg_idx(self):
        jobs_args = []
        for noarg_idx in range(self.nb_noargs):
            # print("contig: %s" % (contig_idx)
            args = {"noarg_idx": int(noarg_idx)}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_rprt_idx(self):
        jobs_args = []
        for rprt_idx in range(self.nb_rprts):
            # print("rprt_idx: %s" % (rprt_idx)
            args = {"rprt_idx": rprt_idx}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def push_qjobs_rprt_idx_arr(self, ids):
        jobs_args = []
        for id in ids:
            print("id: %s" % id)
            args = {"rprt_idx": id}
            jobs_args.append(args)

        self.redis_obj.push_qjobs_args(jobs_args)

    def stats_lane(self):

        # self.stats_blast()

        self.stats_insert()

        # this for haloplex
        # self.stats_spln_dupl()

        return [0]

    def mark_done_args(self, fname, args, success=False):

        # fname_done = "%s_done" % fname
        # fname_fail = "%s_fail" % fname

        print("qwork: ", self.redis_obj.qwork, "args: ", args, "done: ", self.redis_obj.qwork_done, "success: ", success)

        args_json = json.dumps(args)
        # remove duplicates (one job is done only once)
        self.redis_obj.redis_client.lrem(self.redis_obj.qwork_done, 0, args_json)
        self.redis_obj.redis_client.lrem(self.redis_obj.qwork_fail, 0, args_json)
        # mark caller queue
        if success:
            self.redis_obj.redis_client.rpush(self.redis_obj.qwork_done, args_json)

        else:
            self.redis_obj.redis_client.rpush(self.redis_obj.qwork_fail, args_json)

        # no more active anyway
        # remove active flag
        if self.redis_obj.redis_client is not None:
            print("self.redis_obj.redis_client is not None")

        if self.redis_obj.gid is not None:
            print("self.redis_obj.gid is not None")
            print("removing gid: ", self.redis_obj.gid)

            self.redis_obj.redis_client.lrem("active_gids", 0, self.redis_obj.gid)
            self.redis_obj.redis_client.hdel("client:%s" % self.redis_obj.gid, "exec_queue")
            self.redis_obj.redis_client.hdel("client:%s" % self.redis_obj.gid, "args_json")

    @staticmethod
    def flatten(lst):
        for x in lst:
            if isinstance(x, list):
                for x in NaSeqPip.flatten(x):
                    yield x
            else:
                yield x

    def mark_exit_args(self, exit_codes, args):

        # list2d = [[1,2,3],[4,5,6], [7], [8,9]]
        # merged = list(itertools.chain(*list2d))

        # list2d = [[1,2,3],[4,5,6], [7], [8,9]]
        # merged = list(itertools.chain.from_iterable(list2d))

        # print("exit_codes: ", exit_codes)
        exit_codes = [exit_codes]

        # http://rosettacode.org/wiki/Flatten_a_list#Generative
        merged_codes = list(NaSeqPip.flatten(exit_codes))
        # print("exit_codes: ", exit_codes, "b: ", b)


        non_null_codes = [exit_code for exit_code in merged_codes
                          if exit_code != 0]
        success = non_null_codes.__len__() == 0
        print("exit_codes: %s, merged_codes: %s, non_null_codes: %s, success: %s" %
              (exit_codes, merged_codes, non_null_codes, success))

        fname = callersname()
        # mark as done
        self.mark_done_args(fname, args, success)

    def queue_advance(self):
        # all queues end in another
        # some end to dead end - wait

        # default for non existent
        # self.redis_obj.queue_active = self.conf.next_queue_dict.get(self.redis_obj.queue_active, "nonext")
        self.redis_obj.queue_active = self.redis_obj.redis_client.hget("next_queue", self.redis_obj.queue_active)

    # send to function on queue_active
    def queue_job_execute(self, queue, args_json):
        print("args_json: ", args_json)
        print("queue: ", queue)
        kwargs = json.loads(args_json)
        # take parameters and set corresponding attributes
        for key, value in kwargs.items():
            setattr(self, key, value)

        # now call the function corresponding to the queue
        # where they were taken from
        self.__getattribute__(queue)()
        # in case we need arguments
        # self.__getattribute__(self.queue_active)(**kwargs)

    def unpack_execute_qwork(self):

        # try to find a job (parameters of the active queue)
        # decide queue name
        qwork_todo = self.redis_obj.qwork_todo
        print("qwork_todo: ", qwork_todo)
        args_json = self.redis_obj.redis_client.lpop(qwork_todo)

        # subtle differences: if args_json is not None: will
        # if not args_json: will execute False or empty [] ""
        # if not args_json:
        #     check if all done
        #     if self.redis_obj.redis_client.llen(self.redis_obj.queue_active_done) == \
        #         self.redis_obj.redis_client.llen(self.redis_obj.queue_active_all):
        #     self.queue_advance()
        if args_json:
            # mark clients

            self.redis_obj.redis_client.lrem("active_gids", 0, self.redis_obj.gid)
            self.redis_obj.redis_client.rpush("active_gids", self.redis_obj.gid)

            self.redis_obj.redis_client.hset("client:%s" % self.redis_obj.gid, "exec_queue", self.redis_obj.qwork)
            self.redis_obj.redis_client.hset("client:%s" % self.redis_obj.gid, "args_json", args_json)

            self.queue_job_execute(self.redis_obj.qwork, args_json)


    def unpack_execute_active_queue(self):

        # try to find a job (parameters of the active queue)
        # decide queue name
        queue = self.redis_obj.queue_active
        args_json = self.redis_obj.redis_client.lpop(queue)

        # subtle differences: if args_json is not None: will
        # if not args_json: will execute False or empty [] ""
        # if not args_json:
        #     check if all done
        #     if self.redis_obj.redis_client.llen(self.redis_obj.queue_active_done) == \
        #         self.redis_obj.redis_client.llen(self.redis_obj.queue_active_all):
        #     self.queue_advance()
        if args_json:
            # mark clients

            self.redis_obj.redis_client.lrem("active_gids", 0, self.redis_obj.gid)
            self.redis_obj.redis_client.rpush("active_gids", self.redis_obj.gid)

            self.redis_obj.redis_client.hset("client:%s" % self.redis_obj.gid, "exec_queue", queue)
            self.redis_obj.redis_client.hset("client:%s" % self.redis_obj.gid, "args_json", args_json)

            self.queue_job_execute(queue, args_json)
