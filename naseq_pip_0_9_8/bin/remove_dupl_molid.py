#!/usr/bin/env python


import gc
import os
import subprocess
import numexpr

try:
    import pysam
except ImportError:
    print('Cannot import the pysam package; please make sure it is installed.\n')
    sys.exit(1)


# import GTF                      # https://gist.github.com/slowkow/8101481
# import GTF_ENCODE                      # https://gist.github.com/slowkow/8101481

from docopt import docopt
import pandas as pd
import numpy as np

import gzip
import time
import sys
from contextlib import contextmanager

import tables

def take_best_pair(df):


    # print("take_best  .. df: ", df)

    ex_columns = ['rname']
    ex_ = pd.DataFrame(columns=ex_columns)


    rname = df.iloc[0]['rname']

    ex_.loc[len(ex_)+1] = [rname]

    # add to output

    # print("ex_: ", ex_)

    return ex_

def join_indexes():

    pd.set_option('io.hdf.default_format', 'table')

    store = pd.HDFStore('pairs_1.h5')

    print(store)

    file = pysam.Fastqfile("1.idx.fastq")
    ln = len( [ x for x in file ])
    print("ln: ", ln)
    file.close()


    columns = ['rname', 'idx_seq']
    idx_df = pd.DataFrame(index=np.arange(0, ln), columns=columns)
    # print("df_: \n", idx_df, idx_df.dtypes)

    dtypes = {'rname': 'S100', 'idx_seq': 'S20'}

    for c in idx_df.columns:
        idx_df[c] = idx_df[c].astype(dtypes[c])
        # print(mydata['y'].dtype)   #=> int64
    # print("idx_df: \n", idx_df, idx_df.dtypes)

    rname_arr = np.empty([ln, 2], dtype="S100")



    file = pysam.Fastqfile("1.idx.fastq")
    i = 0
    for s in file:

        # Setting With Enlargement
        rname_arr[i, 0] = s.name
        rname_arr[i, 1] = s.sequence

        # idx_df.ix[i,0] = s.name
        # idx_df.ix[i,1] = s.sequence
        i += 1
        # print("s: ", s)
        # print("s.sequence: ", s.sequence)
        # print("s.quality: ", s.quality)
        # print("s.name: ", s.name)
        # print("comment: ", s.comment)

        if i % 100000 == 0:
            print("i: ", i)
        #     print("idx_df: \n", idx_df)
        #     time.sleep(10)
    df = pd.DataFrame({'rname': rname_arr[:, 0], 'idx_seq': rname_arr[:, 1]})
    df.set_index("rname", inplace=True)

    print("df: \n", df, df.dtypes)

 # --------------------

    file = "1.bedpe"

    bpe = pd.read_table(file,
                            # compression="gzip",
                            header=None,
                            index_col=[6],
                            names=['r1_chr', 'r1_s', 'r1_e',
                                   'r2_chr', 'r2_s', 'r2_e',
                                   'rname', 'score', 'r1_st', 'r2_st'])

    print("bpe: \n", bpe)

    # read_gtf()

    # samtools view -bf 0x2 1.spln.bam | bedtools bamtobed -i stdin > 2.bedpe


    merged_df = pd.merge(df, bpe, how='inner', left_index=True, right_index=True)
    merged_df = merged_df.reset_index('rname')

    print("merged_df: ", merged_df)
    # self.readcnt_known_genes_matrix_df.rename(columns={'readcnt_avg': self.smp_name}, inplace=True)

    store.put('all/compr', merged_df, format='table', complib='blosc', chunksize=2000000,
              data_columns=['idx_seq', 'rname', 'r1_chr', 'r1_s', 'r1_e',
                            'r2_chr', 'r2_s', 'r2_e',
                            'score', 'r1_st', 'r2_st'])
    print(store)
    store.close()




def select_best():

    pd.set_option('io.hdf.default_format', 'table')

    store = pd.HDFStore('pairs_1.h5')

    print(store)

    sel = store.select('all/compr')
    print("sel: \n", sel)

    # dict(list( df.groupby(['A'])['C'] ))

    # first row with maximum score value
    gb = sel.sort('score', ascending=False).groupby(['idx_seq', 'r1_chr', 'r1_s', 'r2_e'], as_index=False).first()
    print("gb: ", gb)

    store.put('all/dedup', gb, format='table', complib='blosc', chunksize=2000000,
              data_columns=True)
    print(store)
    store.close()



    # second solution --slower
    # gb = sel.iloc[sel.groupby(['idx_seq', 'r1_chr', 'r1_s', 'r2_e']).apply(lambda x: x['score'].idxmax())]
    # print("gb: ", gb)



    exit(0)

    i = 0
    gb = sel.groupby(['idx_seq', 'r1_chr', 'r1_s', 'r2_e'])
    print("group ready ..............")
    # ks = gb.groups.keys()
    # print("ks ready .............")
    # print("keys: ", ks.__len__())
    # for i in range(ks.__len__()):
    #     grkey = ks[i]
    #     print("i: ", i , "ks: ", grkey)
    #     slic = gb.get_group(grkey)
    # tst = dict(iter(groups))
    # print("tst: ", tst)
    # exit(0)


    dedup = open('dedup.csv', 'w')

    for k, gp in gb:

        if i % 100000 == 0:
            print("i: ", i, "k: ", k)
            # print('key=' + str(k))
            print(gp)

            gp.select

        max_score = gp.score.max()
        # print("max_score: ", max_score)
        res = []

        for i in range(gp.shape[0]):
            row = gp.iloc[i]
            # print("row: ", row)
            sc = row['score']
            # print("sc: ", sc)
            if sc == max_score:
                res = [row['rname'], sc, row['idx_seq']]

        # print("res: ", res)
        print(res, file=dedup)
        i+=1

    dedup.close()


    # print("groups: \n", groups)

    store.close()
    exit(0)

    gr_res = groups.apply(take_best_pair)
    #
    # gr_res = gr_res.reset_index(level=0)
    gr_res = gr_res.reset_index()

    print("gr_res: \n", gr_res)

    store.put('all/best_pairs', gr_res, format='table', complib='blosc', chunksize=2000000,
              data_columns=['rname'])
    print(store)
    store.close()

def print_best():

    pd.set_option('io.hdf.default_format', 'table')
    store = pd.HDFStore('pairs_1.h5')
    print(store)

    sel = store.select('all/dedup', columns=['rname', 'idx_seq'])
    print("sel.shape: \n", sel.shape)
    # dtypes = {'rname': '|S100', 'idx_seq': '|S10'}
    # for c in sel.columns:
    #     sel[c] = sel[c].astype(dtypes[c])

    sel2 = sel.copy()
    print("sel2.shape before drop: \n", sel2.shape)
    sel2.drop_duplicates(inplace=True)
    print("sel2.shape after drop: \n", sel2.shape)
    sel2.set_index(['rname'], drop=True, inplace=True)

    sel2.sort_index(inplace=True)


    print("sel2: \n", sel2, sel2.dtypes)

    val = 'E'

    if val in sel2.index:
        pdrow = sel2.at[val, 'idx_seq']
        print("pdrow: ", pdrow)
    else:
        print("not found")



    # print ids for Picard
    #

    # sel.to_csv("rnames.csv", sep=',', header=False, index=False)

    rnames = sel.values.astype('S100')
    # print("rnames: ", rnames)
    rnames = np.sort(rnames, axis=None)
    # rnames = np.append(rnames, '')
    # rnames = rnames.astype('S100')

    print("rnames: ", rnames, rnames.dtype)
    # print('--111111111111111111111111111')





    # test = np.array(['D93Y6NS1:551:HBGBCADXX:2:1113:12842:47306',
    #                  'D93Y6NS1:551:HBGBCADXX:2:2106:1376:38978','groshon'])

    val = np.array(['adfs'], dtype='S100')
    # print('-2222222222222222222222222222222')
    # print("val: ", val, val.dtype)
    # print('--33333333333333333333333333')

    # found = np.in1d(val, rnames)[0]
    # print(val.shape, rnames.shape)
    index = np.searchsorted(rnames, val)
    np.minimum(index, rnames.shape[0] -1, out=index)
    print("index: ", index)

    a = rnames[index]

    # rnames[index], val
    found =np.all(numexpr.evaluate('(a==val)|((a!=a)&(val!=val))'))
    # print("index: ", index)
    # print("rnames: ", rnames)
    print("found: ", found)
    # lst = set(rnames.tolist())

    # find using python set
    rnames2 = set(rnames.flat)
    val2 = val[0]
    # print("rnames2: ", rnames2)


    # this one is numpy array
    # y = numpy.unique(x)
    if val2 in rnames2:
        found = True
    else:
        found = False
    print("found: ", found)

    # exit(0)
    # ------------------ Picard style filtering
    # java -jar $PICARD_JAR
    # FilterSamReads
    # I=1.proper.bam
    # O=filtered.bam
    # FILTER=includeReadList
    # RLF="rnames.csv"
    infile = pysam.AlignmentFile("1.proper.bam", "rb")
    out_acc = pysam.AlignmentFile("acc.bam", "wb", template=infile)
    out_rej = pysam.AlignmentFile("rej.bam", "wb", template=infile)

    i =0
    for s in infile:

        if i % 100000 == 0:
            print("i: ", i)



        # if s.query_name in sel2.index:
        if s.query_name in rnames2:
            # xmtag = sel2.at[s.query_name, 'idx_seq']
            # print("xmtag: ", xmtag)

            # s.tags["MQ"] = 0
            # s.setTag("XM", xmtag)
            # print("s.tags: ", s.tags)

            out_acc.write(s)


        # if s.query_name in rnames2:
        #     out_acc.write(s)



        i+=1

    out_acc.close()
    out_rej.close()






"""
        qname = s.query_name
        # print("rname: ", qname)
        val = np.array([qname], dtype='S100')

        index = np.searchsorted(rnames, val)
        np.minimum(index, rnames.shape[0] -1, out=index)
        # print("index: ", index)

        a = rnames[index]
        found =np.all(numexpr.evaluate('(a==val)|((a!=a)&(val!=val))'))
        # print("found: ", found)
        # found = np.in1d(val, rnames)[0]


        if found:
            out_acc.write(s)



        # else:
        #     out_rej.write(s)
"""


        #     s.mapq = 0
            #print(z.tags)
            #z.tags["MQ"] = 0
            # s.setTag("MQ", 0)

    #print "----------------"
    #print "s: ", s, s.opt('MQ')

    #time.sleep(5)

    # skip unpaired reads
    # if s.is_paired:



def main():

    # join_indexes()
    # select_best()
    print_best()



def count_bp(df):
    """Given a DataFrame with the exon coordinates from Gencode for a single
    gene, return the total number of coding bases in that gene.
    Example:
        >>> import numpy as np
        >>> n = 3
        >>> r = lambda x: np.random.sample(x) * 10
        >>> d = pd.DataFrame([np.sort([a,b]) for a,b in zip(r(n), r(n))], columns=['start','end']).astype(int)
        >>> d
           start  end
        0      6    9
        1      3    4
        2      4    9
        >>> count_bp(d)
        7
    """
    start = df.start.min()
    end = df.end.max()
    bp = [False] * (end - start + 1)
    for i in range(df.shape[0]):
        s = df.iloc[i]['start'] - start
        e = df.iloc[i]['end'] - start + 1
        bp[s:e] = [True] * (e - s)
    return sum(bp)


@contextmanager
def log(message):
    """Log a timestamp, a message, and the elapsed time to stderr."""
    start = time.time()
    sys.stderr.write("{} # {}\n".format(time.asctime(), message))
    yield
    elapsed = int(time.time() - start + 0.5)
    sys.stderr.write("{} # done in {} s\n".format(time.asctime(), elapsed))
    sys.stderr.flush()


if __name__ == '__main__':
    # args = docopt(__doc__)
    main()

