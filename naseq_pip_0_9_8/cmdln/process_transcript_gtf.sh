#!/bin/sh

module load raglab/bedops/2.4.7

#python gtf_to_gff.py /gs/project/wst-164-aa/share/raglab_prod/igenomes/enGRCh37sp/annotation/genes_gtf/enGRCh37v75sp_genes.gtf > enGRCh37v75sp_genes.gff3
#python gff_to_bed.py enGRCh37v75sp_genes.gff3 > enGRCh37v75sp_genes.bed
awk -F "\t" '$12 != "0," { print $0 }' enGRCh37v75sp_genes.bed > enGRCh37v75sp_genes_nonzero.bed


