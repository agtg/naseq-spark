#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l nodes=1:ppn=12
#PBS -m bae
#PBS -M revil.t@gmail.com
#PBS -A wst-164-ab
#PBS -q metaq

module load raglab/snpEff/4_0_e
module load gcc/4.8.2
module load raglab/naseq_pip/0.9.2
module load raglab/jdk/1.7.0_71
module load raglab/vt/15.05.14
module load automake/1.12.6
module load raglab/libffi/3.2.0
module load raglab/htslib/1.2.1
module load autoconf/2.69
module load raglab/libyaml/0.1.5
module load raglab/samtools/1.2
module load raglab/gemini/0.14.0
module load OpenBLAS+LAPACK/0.2.12-openmp-gcc
module load raglab/python/2.7.9
module load raglab/bcftools/1.2
module load HDF5/1.8.7-gcc
module load raglab/redis/2.8.18
module load raglab/snpEff/3.6

cd /home/trevil/tim/all-combined-gemini


# setup
db=all-combined-gemini.snpid.snpeff.dbnsfp.db
VCF=all-combined-gemini.vcf
SNPID_CF=all-combined-gemini.snpid.vcf
SNPEFF_CF=all-combined-gemini.snpid.snpeff.vcf
DBNSFP_CF=all-combined-gemini.snpid.snpeff.dbnsfp.vcf

FINAL_CF=all-combined-gemini.snpid.snpeff.dbnsfp.vcf.gz

REF=/gs/project/wst-164-ab/share/raglab_prod/igenomes/enGRCh37/sequence/genome_fa/enGRCh37.fa
SNPEFFJAR=/gs/project/wst-164-ab/share/raglab_prod/software/snpEff/snpEff_3_6/snpEff.jar

# decompose,alize and annotate VCF with snpEff.
# NOTE: can also swap snpEff with VEP
#NOTE: -classic and -formatEff flags needed with snpEff >= v4.1
#zless $VCF \
#| sed 's/ID=AD,Number=./ID=AD,Number=R/' \
#| vt decompose -s - \
#| vtalize -r $REF - > $NORMVCF


#snpID
java \
-XX:ParallelGCThreads=12 \
-XX:PermSize=512M -XX:MaxPermSize=1G -Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/SnpSift.jar annotate \
$RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh37/annotations/Homo_sapiens.GRCh37.dbSNP142.vcf \
$VCF > $SNPID_CF

#snpEFF
java \
-XX:ParallelGCThreads=12 \
-Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/snpEff.jar \
eff GRCh37.75 $SNPID_CF > $SNPEFF_CF 

#dbnsfp
java \
-XX:ParallelGCThreads=12 \
-Xms2G -Xmx16G \
-jar ${SNPEFF_HOME}/SnpSift.jar dbnsfp \
-v $RAGLAB_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh37/annotations/dbNSFP/dbNSFP2.9.txt.gz \
$SNPEFF_CF > $DBNSFP_CF


#gbzip
cat $DBNSFP_CF | bgzip -c > $FINAL_CF
tabix $FINAL_CF

# load the pre-processed VCF into GEMINI
gemini load --cores 12 -t snpEff -v $FINAL_CF $db

# query away
#gemini query -q "select chrom, start, end, ref, alt, (gts).(*) from variants" \
#             --gt-filter "gt_types.mom == HET and \
#                          gt_types.dad == HET and \
#                          gt_types.kid == HOM_ALT" \
#             $db
