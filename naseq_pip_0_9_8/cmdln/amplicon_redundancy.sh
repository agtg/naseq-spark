#!/usr/bin/env bash

cat hs_cov.txt | grep ^all |awk 'BEGIN {sum=0} {sum= sum+$5; print $2,sum}' | awk 'BEGIN {sum=0} {sum= 1-$2; print $1+1,$2,sum}' > hs_red.txt
