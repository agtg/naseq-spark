 
Do you mean the sum of the lengths of features in a single bed file?
If so, try:
awk '{SUM += $3-$2} END {print SUM}' file.bed

Get average length too by:
awk 'OFS="\t" {SUM += $3-$2} END {print SUM, SUM/NR}' file.bed

