#!/usr/bin/env python
from __future__ import print_function

import os
import shutil
import re
import csv
import subprocess
import pandas as pd

class DatSeqPip(NaSeqPip, object):

    @property
    def bam_idx(self):
        return int(self.__bam_idx)

    @bam_idx.setter
    def bam_idx(self, val):
        self.__bam_idx = val

    @property
    def bam_name(self):
        return self.bams_df.iloc[self.bam_idx, 0]

    @property
    def nb_bams(self):
        return self.bams_df.shape[0]

    @property
    def bam_dir(self):
        this_dir = os.path.join(self.proj_dir, "bam")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def bam_f(self):
        return os.path.join(self.bam_dir, self.bam_name)

    @property
    def sample_path(self):
        this_dir = os.path.join(self.fastq_dir, self.smp_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def run_path(self):
        this_dir = os.path.join(self.sample_path, self.rpu)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir


    @property
    def fastq_dir(self):
        this_dir = os.path.join(self.proj_dir, "fastq")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir

    @property
    def conf_dir(self):
        this_dir = os.path.join(self.proj_dir, "conf")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir


    @property
    def pysrc_dir(self):
        this_dir = os.path.join(self.proj_dir, "pysrc")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        return this_dir



    @property
    def bam_names_csv(self):
        return os.path.join(self.conf_dir, 'bam_names.csv')

    def __init__(self):
        #read home
        #self.home_dir = os.getenv('HOME', ".")
        #print "$HOME: ", self.home_dir

        #"""
        #Read all names CSV
        #"""
        #self.sample_names = "%s/exome/du-gq-dav-v.0.2/paired_reads/dav-sample-names.csv" % self.home_dir

        self.proj_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        print("self.proj_dir: ", self.proj_dir)

        self.load_modules()



        #local project
        #self.depl_dir = os.path.join(self.proj_dir, "paired_reads")


    def load_modules(self):

        cmd = os.popen('modulecmd python load raglab/zlib/1.2.8'); exec(cmd);
        cmd = os.popen('modulecmd python load raglab/samtools/1.1.32'); exec(cmd);
        cmd = os.popen('modulecmd python load raglab/jdk'); exec(cmd)
        cmd = os.popen('modulecmd python load raglab/picard/1.128'); exec(cmd)
        cmd = os.popen('modulecmd python load raglab/sambamba'); exec(cmd)


    def extract_bams_paral(self, nbproc):
        pass

        # pool = Pool(processes=nbproc)
        # fool[1] = pool.map(unwrap_self_f, zip([self]*len(names), names))
        # pool.close()
        # pool.join()
        # print fool


    def link_bam(self, bam_name):
        sym_src_file = os.path.join(self.bam_dir, bam_name)
        sym_dst_file = os.path.join(self.fastq_dir, bam_name)
        if os.path.exists(sym_dst_file):
            os.remove(sym_dst_file)
            os.symlink(sym_rel_path, sym_dst_file)

    def extract_one_read_group(self):

        # bam_f = os.path.join(self.fastq_dir,bam_name)

        print("bam_f: ", self.bam_f)

        p1 = subprocess.Popen(r"""samtools view -H %s | grep '^@RG'""" % self.bam_f, shell=True,
                              cwd=self.fastq_dir,
                              stdout=subprocess.PIPE)
        res = p1.communicate()[0]

        p = re.compile('@RG\\tID:(.*)\\tPL:(.*)\\tPU:(.*)\\tLB:(.*)\\tSM:(.*)\\tCN:(.*)$')


        m = p.match(res)
        rid = ''
        rpl = ''
        rpu = ''
        rlb = ''
        rsm = ''

        if m:
            self.rid = m.group(1)
            self.rpl = m.group(2)
            self.rpu = m.group(3)
            self.rlb = m.group(4)
            self.smp_name = m.group(5)

        else:
            print("error")

        print(self.rid, self.rpl, self.rpu, self.rlb, self.smp_name)

        #
        read1_name = "%s.%s.33.pair1.fastq.gz" % (self.smp_name, self.rlb)
        read2_name = "%s.%s.33.pair2.fastq.gz" % (self.smp_name, self.rlb)

        print("self.sample_path: ", self.sample_path)

        # #######create run

        print("self.run_path: ", self.run_path)

        # extract fastq from bam
        pipe1 = os.path.join(self.run_path, "pipe1")
        r1 = os.path.join(self.run_path, "r1")
        r2 = os.path.join(self.run_path, "r2")
        o3 = os.path.join(self.run_path, "sam_to_fastq.out")
        f3 = 0

        if not os.path.exists(pipe1):
            os.mkfifo(pipe1)

        if not os.path.exists(r1):
            os.mkfifo(r1)

        if not os.path.exists(r2):
            os.mkfifo(r2)

        if os.path.exists(o3):
            os.remove(o3)

        f3 = open(o3, "w")


        p1 = subprocess.Popen("sambamba sort -n -m 1700MB -F proper_pair %s --tmpdir=$SCRATCH -o /dev/stdout" % self.bam_f,
                              shell=True,
                              cwd=self.run_path,
                              stdout=subprocess.PIPE)

        # p2 = subprocess.Popen("java -jar $PICARD_HOME/SamToFastq.jar VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1,r2) , shell=True,
        p2 = subprocess.Popen("java -jar $PICARD_JAR SamToFastq VALIDATION_STRINGENCY=LENIENT INPUT=/dev/stdin FASTQ=%s SECOND_END_FASTQ=%s" % (r1, r2), shell=True,
                              cwd=self.run_path,
                              stdin=p1.stdout,
                              stderr=f3,
                              stdout=subprocess.PIPE)

        p3 = subprocess.Popen(r"""pigz < %s > %s""" % (r1, read1_name), shell=True,
                              cwd=self.run_path,
                              #stdin = r1,
                              stdout = subprocess.PIPE)

        p4 = subprocess.Popen(r"""pigz < %s > %s""" % (r2, read2_name), shell=True,
                               cwd=self.run_path,
                               #stdin = r2,
                               stdout=subprocess.PIPE)

        print(p1.pid, p2.pid, p3.pid, p4.pid)
        exit_codes = [pp.wait() for pp in p1, p2, p3, p4]

        print(" exit_codes: ", exit_codes)

        os.unlink(pipe1)
        os.unlink(r1)
        os.unlink(r2)

        f3.close()


    def extract_read_groups(self, bam_name):

        m = re.match("^(.*)\.bam", bam_name)

        bam_prefix = m.group(1)
        print("bam_prefix: ", bam_prefix)

        #extract reads
        #bam_f = os.path.join(self.bam_dir,bam_name)
        #print "bam_f: ",bam_f
        #p0 = subprocess.Popen(r"""samtools split %(bam_f)s""" % {"bam_f": bam_f}, shell=True, cwd=self.fastq_dir);
        #res = p0.communicate()[0]
        #print "res: ", res
        #enumerate the extracted read group bams
        #rg_bam_files = [f for f in os.listdir(self.fastq_dir) if re.match("^%s\_\d+\.bam" % bam_prefix , f)]
        #print "rg_bam_files: ",rg_bam_files
        #for rg_bam_name in rg_bam_files:
        #    self.extract_one_read_group(rg_bam_name)


    def load_sample_names(self):

        gr8 = pd.read_csv(os.path.join(self.conf_dir, 'one-sample-info.csv'),
               sep=',',
               header = False,
               index_col=[0]
               #usecols=[0,1]
               )

        #print "gr8: ", gr8
        self.sample_names = gr8

        return self

    def scan_bam_dir(self):

        self.bam_files = [f for f in os.listdir(self.bam_dir) if re.match('(.+)\.bam$', f)]
        print("files: ", self.bam_files)

    def save_bam_names(self):
        bams_df = pd.DataFrame.from_items([('bam_name', self.bam_files)])
        bams_df.reset_index()

        bams_df.to_csv(self.bam_names_csv,
                           sep=',', header=True
                           , index=True
                           , index_label="idx"
                           )

    def load_bam_names(self):
        self.bams_df = pd.read_csv(self.bam_names_csv,
                  sep=',', header=False
                  ,index_col=[0]
                  ,usecols=[0,1]
                  ,names=["idx","bam_name"]
                  )

        # print "self.bams_df: \n", self.bams_df

    def work(self):
        for bam_idx in range(self.nb_bams):
        # for bam_idx in range(2,self.nb_bams):
            self.bam_idx=bam_idx
            print("bam: %s, %s" % (self.bam_idx, self.bam_name))
            self.extract_one_read_group()


if __name__ == '__main__':

    mprf = ManagePairedReadFiles()
    # mprf.scan_bam_dir()
    # mprf.save_bam_names()
    mprf.load_bam_names()
    mprf.work()

    # mprf.extract_bams()

    # mprf.extract_bams_paral(12)




