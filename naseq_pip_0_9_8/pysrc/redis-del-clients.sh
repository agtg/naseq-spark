module load raglab/redis

redis-cli EVAL "for i, name in ipairs(redis.call('KEYS', ARGV[1])) do redis.call('DEL', name); end" 0 client:*
redis-cli set last_client 0
redis-cli set active_queue wait
redis-cli set shutdown 0
# redis-cli del merge_lanes_rnaseqc_done

redis-cli EVAL "for i, name in ipairs(redis.call('KEYS', ARGV[1])) do redis.call('DEL', name); end" 0 *_done
redis-cli EVAL "for i, name in ipairs(redis.call('KEYS', ARGV[1])) do redis.call('DEL', name); end" 0 *_fail
redis-cli EVAL "for i, name in ipairs(redis.call('KEYS', ARGV[1])) do redis.call('DEL', name); end" 0 *_all

rm -fr job_output/*
rm -fr ../redis/wk-*.log


