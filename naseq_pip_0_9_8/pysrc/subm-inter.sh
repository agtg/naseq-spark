#!/bin/sh

# qsub -I -A wst-164-aa -d /gs/project/wst-164-aa/exome/du-gq-tim-v.0.1 -l walltime=24:00:0 -q sw -l nodes=1:ppn=4
qsub -I -A wst-164-ab -d `pwd` -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12,pmem=1700M
