#!/usr/bin/env python 

__author__ = 'root'

import os, shutil
import re
import csv

import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
pd.set_option('max_columns', 50)

# from treedict import TreeDict

#http://recursive-labs.com/blog/2012/05/31/one-line-python-tree-explained/
#from collections import defaultdict
#def tree(): return defaultdict(tree):
#a = tree()

class ScanConfig:

    # conf = TreeDict()
      
    @classmethod
    def scan_local(cls):
        
        cls.conf.proj_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        
        #conf.orig_dir = os.path.join(conf.proj_dir, "orig")
        cls.conf.conf_dir = os.path.join(cls.conf.proj_dir, "conf")
        cls.conf.bam_dir = os.path.join(cls.conf.proj_dir, "bam")
        
        #local project
        #self.depl_dir = os.path.join(self.proj_dir, "paired_reads") 
        
        return cls.conf
     
    @classmethod
    def scan_sample_tags(cls):
        #create hash for lookup tags --> names
        cls.conf.names_for_tags = {}
        ifile = open(cls.conf.sample_name_tags, "rb")
        reader = csv.DictReader(ifile, delimiter=',')
        for line in reader:
            cls.conf.names_for_tags[line["Sample"]]=line["Name"]
        ifile.close()        
        
       
    @classmethod
    def scan_gq00(cls):
        
        cls.scan_local()
        #add specific configs
        cls.conf.fastq_dir = os.path.join(cls.conf.proj_dir, "fastq-gq00")
        
        #self.depl_dir = os.path.join(self.proj_dir, "/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/wg-gq-ma-v.0.6/raw_reads")        
        
        #deploy locally for testing        
        #as default
        cls.conf.depl_dir = os.path.join(cls.conf.proj_dir, "paired_reads")
        
        return cls.conf 
        
    @classmethod
    def scan_fl01(cls):
        
        cls.scan_local()
        #add specific configs
        cls.conf.fastq_dir = os.path.join(cls.conf.proj_dir, "fastq-fl01")
        #scan name tags
        cls.conf.sample_name_tags = os.path.join(cls.conf.conf_dir, "sample-name-tag.csv") 
        cls.scan_sample_tags()
        
        #self.depl_dir = os.path.join(self.proj_dir, "/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/wg-gq-ma-v.0.6/raw_reads")        
        
        #deploy locally for testing        
        #as default
        cls.conf.depl_dir = os.path.join(cls.conf.proj_dir, "paired_reads")
        
        
        return cls.conf
        
    @classmethod
    def scan_fl02(cls):
        
        cls.scan_local()
        #add specific configs
        cls.conf.fastq_dir = os.path.join(cls.conf.proj_dir, "fastq-fl02")
        #scan name tags
        cls.conf.sample_name_tags = os.path.join(cls.conf.conf_dir, "sample-name-tag.csv") 
        cls.scan_sample_tags()
        
        #self.depl_dir = os.path.join(self.proj_dir, "/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/wg-gq-ma-v.0.6/raw_reads")        
        
        #deploy locally for testing        
        #as default
        cls.conf.depl_dir = os.path.join(cls.conf.proj_dir, "paired_reads")
        
        
        return cls.conf    
        
    @classmethod
    def scan_fl03(cls):
        
        cls.scan_local()
        #add specific configs
        cls.conf.fastq_dir = os.path.join(cls.conf.proj_dir, "fastq-fl03")
        
        #self.depl_dir = os.path.join(self.proj_dir, "/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/wg-gq-ma-v.0.6/raw_reads")        
        
        #deploy locally for testing        
        #as default
        cls.conf.depl_dir = os.path.join(cls.conf.proj_dir, "paired_reads")
        
        return cls.conf         
    
class ScanPairedReadFiles:

    def __init__(self):
        pass
        #read home
        #self.home_dir = os.getenv('HOME', ".")
        #print "$HOME: ", self.home_dir

        #"""
        #Read all names CSV
        #"""
        #self.sample_names = "%s/exome/du-gq-dav-v.0.2/paired_reads/dav-sample-names.csv" % self.home_dir

    def init_gq00(self):
        self.conf = ScanConfig().scan_gq00()
        print self.conf.makeReport() #self.conf.items()
        
        
    def init_fl01(self):
        self.conf = ScanConfig().scan_fl01()
        print self.conf.makeReport() #self.conf.items()
        
    def init_fl02(self):
        self.conf = ScanConfig().scan_fl02()
        print self.conf.makeReport() #self.conf.items()

    def init_fl03(self):
        self.conf = ScanConfig().scan_fl03()
        print self.conf.makeReport() #self.conf.items()

                
    #update deployment dir
    def set_depl_dir(self, depl_dir):
        self.conf.depl_dir = depl_dir

    def fastq_fl01_deploy(self, pref, suff):
        
        #regular expression to find tag and direction
        p = re.compile('(\w*)(_R)(\d)')


        #default values for all samples
        lib_barcode = "lib001"
        lib_run = "1540"
        lib_region = "1"
        lib_quality = "33"
        lib_status = "Data is valid"

        ############### cycle labfiles
        for sample_f in os.listdir(self.conf.fastq_dir):
            print "sample_f: ", sample_f
            m = p.match(sample_f)
            if m:
                sample = m.group(1)
                direction = m.group(3)
                name = self.conf.names_for_tags[sample].strip()
                print 'Match found: ', sample, name
                
                #create sample folder
                sample_f_new = "%s%s%s" % (pref, name, suff) 
                sample_dir_new = os.path.join(self.conf.depl_dir,sample_f_new)
                #print "sample_dir_new: ", sample_dir_new
                if not os.path.exists(sample_dir_new):
                    os.makedirs(sample_dir_new)                
                ########create run
                run_path = os.path.join(sample_dir_new, "run%s_%s") % (lib_run, lib_region)
                print run_path
                if not os.path.exists(run_path):
                    os.makedirs(run_path)
                ########create symlink
                print "direction: %s" % direction

                ########create symlink specific regular expresions
                #sym_src_name = "%s_R%s.fastq.gz"  % (sample, direction)
                sym_src_name = sample_f
                sym_dst_name = "%s.%s.33.pair%s.fastq.gz" % (sample_f_new, lib_barcode, direction)

                print "sym_src_name: ", sym_src_name
                print "sym_dst_name: ", sym_dst_name
                #source is specific fluidigm/labfiles
                sym_src_f = os.path.join(self.conf.fastq_dir, sym_src_name)
                sym_dst_f = os.path.join(run_path, sym_dst_name)
                
                print "sym_dst_f: ", sym_dst_f
                if os.path.exists(sym_dst_f):
                    print "sym_dst_f %s exists " % sym_dst_f
                    os.unlink(sym_dst_f)
                        
                os.symlink(sym_src_f, sym_dst_f)
                
            else:
                print 'No match'

        
    def fastq_fl02_deploy(self,pref,suff):
        
        #regular expression to find tag and direction
        p = re.compile('^L(\d+)_(\w*)_(L\d+)_R(\d)_(\d+)')


        ############ output config file open and header
        #default values for all samples
        lib_quality = "33"
        lib_status = "Data is valid"

        ############### cycle labfiles
        for sample_f in os.listdir(self.conf.fastq_dir):
            print "sample_f: ", sample_f
            m = p.match(sample_f)
            if m:
                region = m.group(1)
                sample = m.group(2)
                #barcode = library
                barcode = m.group(3)
                direction = m.group(4)
                run = m.group(5)
               
                name = self.conf.names_for_tags[sample].strip()
                print 'Match found: ', sample, name
                
                #create sample folder
                sample_f_new = "%s%s%s" % (pref, name, suff) 
                sample_dir_new = os.path.join(self.conf.depl_dir,sample_f_new)
                #print "sample_dir_new: ", sample_dir_new
                if not os.path.exists(sample_dir_new):
                    os.makedirs(sample_dir_new)                
                ########create run
                run_path = os.path.join(sample_dir_new, "run%s_%s") % (run, region)
                print run_path
                if not os.path.exists(run_path):
                    os.makedirs(run_path)
                ########create symlink
                print "direction: %s" % direction

                ########create symlink specific regular expresions
                #sym_src_name = "%s_R%s.fastq.gz"  % (sample, direction)
                sym_src_name = sample_f
                sym_dst_name = "%s.%s.33.pair%s.fastq.gz" % (sample_f_new, barcode, direction)

                print "sym_src_name: ", sym_src_name
                print "sym_dst_name: ", sym_dst_name
                #source is specific fluidigm/labfiles
                sym_src_f = os.path.join(self.conf.fastq_dir, sym_src_name)
                sym_dst_f = os.path.join(run_path, sym_dst_name)
                
                print "sym_dst_f: ",sym_dst_f 
                if os.path.exists(sym_dst_f):
                    print "sym_dst_f %s exists " % sym_dst_f
                    os.unlink(sym_dst_f)
                        
                os.symlink(sym_src_f, sym_dst_f)
                
            else:
                print 'No match'

    def fastq_fl03_deploy(self,pref,suff,rel_pref):
        
        #regular expression to find sample name
        p0 = re.compile('^('+ rel_pref + ')(\w*)(_fastq)')
        
        pd_names = pd.Series([],dtype="|S35")
        pd_fnames = pd.Series([],dtype="|S35")
        
        files_names = os.listdir(self.conf.fastq_dir)
        for fname in files_names:
            #add filename
            pd_fnames = pd.Series(np.concatenate([pd_fnames,pd.Series([fname],dtype="|S35")]))
            m = p0.match(fname)
            if m:
                #print "m: ", m.group(0), m.group(1), m.group(2), m.group(3)
                #lane = region
                sname = m.group(2)
                print 'Match found: ', sname
                s = pd.Series([sname],dtype="|S35")
                pd_names = pd.Series(np.concatenate([pd_names,s]))
                print "fname: ", fname
            else:
                print 'No match'

        #regular expression to find tag and direction
        p1 = re.compile('^L(\d+)_(\w*)_(L\d+)_R(\d)_(\d+)')
        ############### cycle fluidig sample files
        for i in range(pd_names.__len__()):
            sample_name = pd_names[i]
            sample_fname = pd_fnames[i]
            print "sample_name: %s, fname: %s" % (sample_name,fname)
            #scan sample folder
            sample_dir = os.path.join(self.conf.fastq_dir, sample_fname)
            files_names = os.listdir(sample_dir)
            for sample_run_name in files_names:
        
                #default values for all samples
                lib_quality = "33"
                lib_status = "Data is valid"                
                m = p1.match(sample_run_name)
                print "m: ", m.group(0), m.group(1), m.group(2), m.group(3), m.group(4), m.group(5)
                if m:
                    #lane = region
                    region = m.group(1)
                    sample = m.group(2)
                    #barcode = library
                    barcode = m.group(3)
                    direction = m.group(4)
                    run = m.group(5)
                    
                    #create sample folder
                    sample_f_new = "%s%s%s" % (pref, sample_name, suff) 
                    sample_dir_new = os.path.join(self.conf.depl_dir,sample_f_new)
                    #print "sample_dir_new: ", sample_dir_new
                    if not os.path.exists(sample_dir_new):
                        os.makedirs(sample_dir_new)    
                    ########create run
                    run_path = os.path.join(sample_dir_new, "run%s_%s") % (run, region)
                    print run_path
                    if not os.path.exists(run_path):
                        os.makedirs(run_path)
                    ########create symlink specific regular expresions
                    #sym_src_name = "%s_R%s.fastq.gz"  % (sample, direction)
                    sym_src_name = os.path.join(sample_dir,sample_run_name)
                    sym_dst_name = "%s.%s.33.pair%s.fastq.gz" % (sample_f_new, barcode, direction)
    
                    print "sym_src_name: ", sym_src_name
                    print "sym_dst_name: ", sym_dst_name
                    #source is specific fluidigm/labfiles
                    sym_src_f = os.path.join(self.conf.fastq_dir, sym_src_name)
                    sym_dst_f = os.path.join(run_path, sym_dst_name)
                    
                    print "sym_dst_f: ",sym_dst_f 
                    if os.path.exists(sym_dst_f):
                        print "sym_dst_f %s exists " % sym_dst_f
                        os.unlink(sym_dst_f)
                            
                    os.symlink(sym_src_f, sym_dst_f)
                else:
                    print 'No match'           
   
        

    def fastq_gq00_deploy(self, pref, suff):
    
        #regular expression to find tag and direction
        #p = re.compile('(\w*)\.(\w*).33.pair(\d*).fastq.gz')
        p = re.compile('^(\S*)\.(\S*)\.33\.pair(\d).fastq.gz$')


        for sample_f in [f for f in os.listdir(self.conf.fastq_dir)]:
            print "sample_f: ", sample_f
            # create sample dir
            sample_f_new = "%s%s%s" % (pref, sample_f, suff)
            sample_dir_new = os.path.join(self.conf.depl_dir, sample_f_new)
            # print "sample_dir_new: ", sample_dir_new
            if not os.path.exists(sample_dir_new):
                os.makedirs(sample_dir_new)
            for lane_run_f in os.listdir(os.path.join(self.conf.fastq_dir, sample_f)):
                #
                lane_run_dir_new = os.path.join(sample_dir_new, lane_run_f)
                if not os.path.exists(lane_run_dir_new):
                    os.makedirs(lane_run_dir_new)
                for read_f in [f for f in os.listdir(os.path.join(self.conf.fastq_dir, sample_f, lane_run_f)) if re.match('(.+)\.fastq.gz$', f)]:
                    print read_f
                    #
                    m = p.match(read_f)
                    if m:
                        sample = m.group(1)
                        library = m.group(2)
                        direction = m.group(3)
                        
                    else:
                        print 'No match'

                    print("-----------------------read_f: \n", read_f)
                    sym_src_f = os.path.join(self.conf.fastq_dir,sample_f,lane_run_f,read_f)
                    # also replace sample name in file name and reconstitute read name
                    sym_dst_name = "%s.%s.33.pair%s.fastq.gz" % (sample_f_new, library, direction)
                    sym_dst_f = os.path.join(lane_run_dir_new, sym_dst_name)
                    print("sym_src_f: ", sym_src_f)
                    print("sym_dst_f: ", sym_dst_f)
                    os.symlink(sym_src_f, sym_dst_f)                    
                    # print sample_f,lane_run_f,read_f
            
    def scan_wg_d2(self):
        pass
        
       
        
        

    
        
slf = ScanPairedReadFiles()
slf.init_fl01()
slf.set_depl_dir("/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/ex-gq-ma-v.0.6/raw_reads")
slf.fastq_fl01_deploy("", "-03")
#
slf.init_fl02()
slf.set_depl_dir("/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/ex-gq-ma-v.0.6/raw_reads")
slf.fastq_fl02_deploy("","-04")
#
slf.init_fl03()
slf.set_depl_dir("/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/ex-gq-ma-v.0.6/raw_reads")
slf.fastq_fl03_deploy("","-05","Ext6_Test5B_")
#
slf.init_gq00()
slf.set_depl_dir("/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/ex-gq-ma-v.0.6/raw_reads")
slf.fastq_gq00_deploy("","-06")
#
slf.init_fl03()
slf.set_depl_dir("/mnt/parallel_scratch_mp2_wipe_on_april_2015/ioannisr/badescud/ex-gq-ma-v.0.6/raw_reads")
slf.fastq_fl03_deploy("","-07","Ext6A2_Test_")
#
#
# slf.init_gq00()
slf.set_depl_dir("/gs/project/wst-164-ab/expr/eaa-014/raw_reads")
# slf.set_depl_dir("/gs/project/wst-164-ab/expr/eba-015/raw_reads")

slf.fastq_gq00_deploy("", "-eba")




#slf.scan_fluidig2_files()
#slf.scan_wg_d1()
#slf.scan_wg_d2()


#slf.scan_lab_files()
#slf.scan_fluidig_files()

print "ok 444"





