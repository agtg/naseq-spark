#!/usr/bin/env bash

export PYSPARK_PYTHON=python3

export SPARK_MASTER_IP={{redis_host}}
export PROJ_DIR={{proj_dir}}
export SPARK_LOC_CONF_DIR=$PROJ_DIR/servers/spark/{{local_host}}
export SPARK_CONF_DIR=$SPARK_LOC_CONF_DIR/conf
export SPARK_LOG_DIR=$SPARK_LOC_CONF_DIR/log
export SPARK_PID_DIR=$SPARK_LOC_CONF_DIR/pid

#export CLASSPATH="$CLASSPATH:/gs/project/wst-164-ab/share/raglab_prod/software/derby/db-derby-10.10.2.0-bin/lib/derbyclient.jar"
#export SPARK_CLASSPATH=$CLASSPATH
#export SPARK_SUBMIT_CLASSPATH=$CLASSPATH

