#!/bin/bash

#disables cache
#mkdir -m 500 ~/.lmod.d/.cache
export LMOD_SHORT_TIME=86400

export BASE=/sb/scratch/dbadescu/base
export DNLD=$BASE/softbase/download
export SFTW=$BASE/software
export MODU=$BASE/modulefiles/raglab
export HERE=$BASE/gmloc

export TSFT=$BASE/softbase/templ/software
export TMOD=$BASE/softbase/templ/modules/raglab

module purge
#
#rm -rf ~/.lmod.d/.cache
#export LMOD_SHORT_TIME=86400
#

#mkdir $SFTW
#mkdir $SFTW/autoconf

#mkdir $SFTW/autoconf/autoconf-2.69-src

# autoconf
#tar -xJvf $DNLD/autoconf-2.69.tar.xz -C $SFTW/autoconf/
#cd $SFTW/autoconf/autoconf-2.69
#./configure --prefix=$SFTW/autoconf/autoconf-2.69-inst
#make
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/autoconf/ $MODU/autoconf/
#rm -fr $SFTW/autoconf/autoconf-2.69
module load raglab/autoconf/2.69

#automake
#mkdir $SFTW/automake/
#tar -xJvf $DNLD/automake-1.15.tar.xz -C $SFTW/automake/
#cd $SFTW/automake/automake-1.15
#./configure --prefix=$SFTW/automake/automake-1.15-inst
#make
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/automake/ $MODU/automake/
#rm -fr $SFTW/automake/automake-1.15
module load raglab/automake/1.15

#gmp
#mkdir $SFTW/libgmp/
#tar -xJvf $DNLD/gmp-6.1.0.tar.xz -C $SFTW/libgmp/
#cd $SFTW/libgmp/gmp-6.1.0
#./configure --prefix=$SFTW/libgmp/gmp-6.1.0-inst
#make
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/libgmp/ $MODU/libgmp/
#rm -fr $SFTW/libgmp/gmp-6.1.0
module load raglab/libgmp/6.1.0


#mpfr
#rm -fr $SFTW/libmpfr/
#mkdir $SFTW/libmpfr/
#tar -xJvf $DNLD/mpfr-3.1.4.tar.xz -C $SFTW/libmpfr/
#cd $SFTW/libmpfr/mpfr-3.1.4
#./configure --prefix=$SFTW/libmpfr/mpfr-3.1.4-inst --with-gmp=$SFTW/libgmp/gmp-6.1.0-inst
#make
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/libmpfr/ $MODU/libmpfr/
#rm -fr $SFTW/libmpfr/mpfr-3.1.4
module load raglab/libmpfr/3.1.4

#mpc
#rm -fr $SFTW/libmpc/
#mkdir $SFTW/libmpc/
#tar -xzvf $DNLD/mpc-1.0.3.tar.gz -C $SFTW/libmpc/
#cd $SFTW/libmpc/mpc-1.0.3
#./configure --prefix=$SFTW/libmpc/mpc-1.0.3-inst --with-gmp=$SFTW/libgmp/gmp-6.1.0-inst --with-mpfr=$SFTW/libmpfr/mpfr-3.1.4-inst
#make
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/libmpc/ $MODU/libmpc/
#rm -fr $SFTW/libmpc/mpc-1.0.3
module load raglab/libmpc/1.0.3

#gcc
#rm -fr $SFTW/gcc/
#mkdir $SFTW/gcc/
#tar -xjvf $DNLD/gcc-5.3.0.tar.bz2 -C $SFTW/gcc/
#mkdir $SFTW/gcc/gcc-5.3.0/build
#cd $SFTW/gcc/gcc-5.3.0/build
#../configure --prefix=$SFTW/gcc/gcc-5.3.0-inst --disable-multilib --with-system-zlib --enable-languages=c,c++,fortran --with-default-libstdcxx-abi=gcc4-compatible --disable-bootstrap -with-gmp=$SFTW/libgmp/gmp-6.1.0-inst --with-mpfr=$SFTW/libmpfr/mpfr-3.1.4-inst --with-mpc=$SFTW/libmpc/mpc-1.0.3-inst
#make -j 12
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/gcc/ $MODU/gcc/
#rm -fr $SFTW/gcc/gcc-5.3.0
module load raglab/gcc/5.3.0

#openblast
#this is architecture specific so login to guillimin4 or guillimin5.clumeq.ca or ask a 12 core node
#rm -fr $SFTW/linalglibs/
#mkdir $SFTW/linalglibs/
#unzip $DNLD/openblas_0.2.18.zip -d $SFTW/linalglibs/
#cd $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a
# make detects the cpu
#make -j 12
#make install PREFIX=$SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/linalglibs/ $MODU/linalglibs/
#rm -fr $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a
module load raglab/linalglibs/ob0.2.18

#python2
#rm -fr $SFTW/python/
#mkdir $SFTW/python/

#tar -xJvf $DNLD/Python-2.7.11.tar.xz -C $SFTW/python/
#cd $SFTW/python/Python-2.7.11
#./configure --prefix=$SFTW/python/Python-2.7.11-inst --with-threads --enable-shared
#make -j 12
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/python/ $MODU/python/
#rm -fr $SFTW/python/Python-2.7.11
#module load raglab/python/2.7.11

#pip2 will not be installed by default
#python -m ensurepip --upgrade
# update pip and modules comming from source
#pip install --upgrade pip
#pip install --upgrade setuptools
# also install wheel
#pip install wheel

#numpy
#rm -fr $SFTW/linalglibs/
#mkdir $SFTW/linalglibs/

#unzip $DNLD/numpy-1.11.0.zip -d $SFTW/linalglibs/
#cd $SFTW/linalglibs/numpy-1.11.0
#echo "[openblas]" > site.cfg
#echo "libraries = openblas" >> site.cfg
#echo library_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/lib >> site.cfg
#echo include_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/include >> site.cfg
#echo runtime_library_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/lib >> site.cfg

#cp site.cfg ~/.site.cfg
#unset CPPFLAGS
#unset LDFLAGS
#python setup.py build --fcompiler=gnu95
#pip install .
#python ../softbase/tests/test-python2-numpy2.py
#pip install scipy
#python ../softbase/tests/test-python2-scipy.py

#other python2 modules

#pip install cython
#pip install pysam
#pip install pysamstats
#cgat need pg_config in the path
# and comes with huge list of dependencies
#module load raglab/postgres/9.4.1
#pip install cgat

#module unload raglab/python/2.7.11
#rm -fr $SFTW/linalglibs/numpy-1.11.0

#python3
#tar -xJvf $DNLD/Python-3.5.1.tar.xz -C $SFTW/python/
#cd $SFTW/python/Python-3.5.1
#./configure --prefix=$SFTW/python/Python-3.5.1-opt --with-threads --enable-shared
#make -j 12
#make install
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/python/ $MODU/python/
#rm -fr $SFTW/python/Python-3.5.1
#

module load raglab/python/3.5.1
#python --version

# update pip and modules comming from source
#pip install --upgrade pip
#pip install --upgrade setuptools
# also install wheel
#pip install wheel

#numpy
#unzip $DNLD/numpy-1.11.0.zip -d $SFTW/linalglibs/
#cd $SFTW/linalglibs/numpy-1.11.0
#echo "[openblas]" > site.cfg
#echo "libraries = openblas" >> site.cfg
#echo library_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/lib >> site.cfg
#echo include_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/include >> site.cfg
#echo runtime_library_dirs = $SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst/lib >> site.cfg

#cp site.cfg ~/.site.cfg
#unset CPPFLAGS
#unset LDFLAGS
#python setup.py build --fcompiler=gnu95
#pip install .

#python ../softbase/tests/test-numpy2.py
#pip install scipy
#python ../softbase/tests/test-python3-scipy.py
#rm -fr $SFTW/linalglibs/numpy-1.11.0

#pip install psutil
#pip install cython
#pip install jinja2
#pip install mixin
#pip install requests
#pip install matplotlib

export GMINDIR=gmin:/sb/scratch/dbadescu
export GMINRAG=$GMINDIR/base
export ABADIR=/lb/project/ioannisr/share
export ABARAG=$ABADIR/raglab_prod

export S=
export DSTRAG=$GMINRAG

rsync -av --del $SRCRAG/softbase/ $DSTRAG/softbase/
rsync -av --del $SRCRAG/igenomes/ucHg19sp3p92/ $DSTRAG/igenomes/ucHg19sp3p92/
#rsync -av --del $SRCRAG/igenomes/ucHg19sp3p92/ $DSTRAG/igenomes/ucHg19sp3p92/                                                                              

rsync -av --del $ABADIR/expr/a00ht/ $GMINDIR/expr/a00ht/

#rsync -av --del $SRCRAG/softbase/ $DSTRAG/softbase/                                                                                                        

rsync -av --del $SFTW/system/system-0.2/ $STRAG/software/system/system-0.2/                          
#rsync -av --del $SRCRAG/modulefiles/raglab/system/0.2 $DSTRAG/modulefiles/raglab/system/0.2                                                                


#pip install pysam (needs libcurl)
# download jpype 0.6.1 master, rename it then
#pip install .
#           jaydebeapi
#           tables
#            flask
#            flask_restful






# make detects the cpu
#make -j 12
#make install PREFIX=$SFTW/linalglibs/xianyi-OpenBLAS-3f6398a-inst
#rsync -av --del --no-perms --no-o --no-g $BASE/softbase/templ/modules/raglab/linalglibs/ $MODU/linalglibs/
