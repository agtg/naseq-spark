#!/bin/bash

export BASE=/sb/scratch/dbadescu/base
export DNLD=$BASE/softbase/download
export SFTW=$BASE/software
export MODU=$BASE/modulefiles/raglab
export HERE=$BASE/gmloc

#
rm -rf ~/.lmod.d/.cache
export LMOD_SHORT_TIME=86400
#
module load raglab/autoconf/2.69
module load raglab/automake/1.15
module load raglab/libgmp/6.1.0
module load raglab/libmpfr/3.1.4
module load raglab/libmpc/1.0.3
module load raglab/gcc/5.3.0
module load raglab/linalglibs/ob0.2.18
#module load raglab/python/2.7.11

