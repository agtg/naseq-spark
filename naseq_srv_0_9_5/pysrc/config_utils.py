#!/usr/bin/env python3

import os
import configparser

def get_with_default(cfg_parser,section,name,default):
        return cfg_parser.get(section,name) if configparser.has_section(section) and configparser.has_option(section, name) else default

def get_boolean_with_default(cfg_parser,section,name,default):
        return cfg_parser.getboolean(section,name) if cfg_parser.has_section(section) and cfg_parser.has_option(section, name) else default


# load pipeline at runtime
def proj_dir():
    curr_path = os.environ['PWD']
    absdir = os.path.abspath(os.path.join(curr_path, ".."))
    proj_dir = os.path.realpath(absdir)
    return proj_dir

# these are global parameters
cfg_parser = configparser.ConfigParser()
cfg_parser.read(os.path.join(proj_dir(), "conf", "naseq_srv.ini"))

naseq_pip_module = cfg_parser.get("General", "naseq_pip_module")
# global modulecmd_loader_binary
modulecmd_loader_binary = cfg_parser.get("General", "modulecmd_loader_binary")


do_spawn_ctrl = get_boolean_with_default(cfg_parser, "General", "spawn_ctrl", True)
do_shut_clients = get_boolean_with_default(cfg_parser, "General", "shut_clients", False)
do_shut_server = get_boolean_with_default(cfg_parser, "General", "shut_server", False)

do_spawn_nodectrl = get_boolean_with_default(cfg_parser, "General", "spawn_nodectrl", True)

do_start_jupyter_server = get_boolean_with_default(cfg_parser, "General", "start_jupyter_server", True)
do_start_zk_server = get_boolean_with_default(cfg_parser, "General", "start_zk_server", True)
do_start_drillbit = get_boolean_with_default(cfg_parser, "General", "start_drillbit", True)
do_start_derby_server = get_boolean_with_default(cfg_parser, "General", "start_derby_server", True)
do_start_spark_master = get_boolean_with_default(cfg_parser, "General", "start_spark_master", True)
do_start_spark_locslv = get_boolean_with_default(cfg_parser, "General", "start_spark_locslv", False)
do_start_thrift_server = get_boolean_with_default(cfg_parser, "General", "start_thrift_server", True)

do_create_ssh_config = get_boolean_with_default(cfg_parser, "General", "create_ssh_config", True)

