
from __future__ import print_function

import os
import subprocess
import sys

import time

from contextlib import contextmanager
import re

import config_utils

@contextmanager
def log(message):
    """Log a timestamp, a message, and the elapsed time to stderr."""
    start = time.time()
    sys.stderr.write("{} # {}\n".format(time.asctime(), message))
    yield
    elapsed = int(time.time() - start + 0.5)
    sys.stderr.write("{} # done in {} s\n".format(time.asctime(), elapsed))
    sys.stderr.flush()

# import os, sys, string
#
# if not os.environ.has_key('MODULEPATH'):
#         os.environ['MODULEPATH'] = os.popen("""sed -n 's/[      #].*$//; /./H; $ { x; s/^\\n//; s/\\n/:/g; p; }' ${MODULESHOME}/init/.modulespath""").readline()
#
# if not os.environ.has_key('LOADEDMODULES'):
#         os.environ['LOADEDMODULES'] = '';
#
# def module(command, *arguments):
#         commands = os.popen('/usr/bin/modulecmd python %s %s' % (command, string.join(arguments))).read()
#         exec commands
#
        ##catch possible changes to PYTHONPATH environment variable
        # pp = ['']
        # pythonpath = os.environ['PYTHONPATH'].split(":")
        # pp.extend(pythonpath)
        # for p in sys.path:
        #   if (p not in pp) and (p):
        #     pp.append(p)
        #
        # sys.path = pp


def load_indirect_one_module(module_name):

    cmd = ["modulecmd", "python", "load", module_name]
    print("cmd: ", cmd)

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, shell=False,
                     stdout=subprocess.PIPE)
    res1, err = p.communicate()
    print("res1: ", res1)

    res_arr = res1.split(" ")
    print("res_arr: ", res_arr)
    a = res_arr[1]
    b = a.strip('\n').strip("'")
    print("b: ", b)
    execfile(b)

def load_direct_one_module(module_name):

    cmd = ["modulecmd", "python", "load", module_name]
    print("cmd: ", cmd)

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, shell=False,
                     stdout=subprocess.PIPE)
    res1, err = p.communicate()
    print("res1: ", res1)

    res_arr = res1.split(" ")
    print("res_arr: ", res_arr)
    a = res_arr[1]
    b = a.strip('\n').strip("'")
    print("b: ", b)
    execfile(b)


def load_one_module(module_name):

    print("in load one module...")

    # on guillimin: modulecmd.bin
    # on mp2: modulecmd


    cmd = [config_utils.modulecmd_loader_binary, "python", "load", module_name]
    # cmd = ["modulecmd.bin", "python", "load", module_name]
    # cmd = ["modulecmd", "python", "load", module_name]
    # print("cmd: ", cmd)

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, shell=False,
                     stdout=subprocess.PIPE)
    res1, err = p.communicate()
    # print("res1: ", res1)

    res1 = res1.decode("utf-8")

    pat = r"""^exec \'"""
    patc = re.compile(pat)

    m = patc.match(res1)
    if m:
        # old_pref = m.group(1)
        print("exec indirect detected")
        res_arr = res1.split(" ")
        print("res_arr: ", res_arr)
        a = res_arr[1]
        b = a.strip('\n').strip("'")
        print("b: ", b)
        execfile(b)

    else:
        # print("exec direct")
        exec(res1)


def unload_one_module(module_name):

    cmd = ["modulecmd.bin", "python", "unload", module_name]
    # print("cmd: ", cmd)

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, shell=False,
                     stdout=subprocess.PIPE)
    res1, err = p.communicate()
    # print("res1: ", res1)

    res1 = res1.decode("utf-8")

    pat = r"""^exec \'"""
    patc = re.compile(pat)

    m = patc.match(res1)
    if m:
        # old_pref = m.group(1)
        print("exec indirect detected")
        res_arr = res1.split(" ")
        print("res_arr: ", res_arr)
        a = res_arr[1]
        b = a.strip('\n').strip("'")
        print("b: ", b)
        execfile(b)

    else:
        # print("exec direct")
        exec(res1)



def load_one_module_old(module_name):

    # execfile("/usr/share/Modules/init/python")

    # module('load',module_name)
    # module('list')


    # cmd = r"""modulecmd python load %s""" % module_name
    cmd = ["modulecmd","python","load", module_name]
    print("cmd: ", cmd)

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, shell=False,
                     stdout=subprocess.PIPE)
    res1, err = p.communicate()
    # print("res1: ", res1)

    res_arr = res1.split(" ")
    # print("res_arr: ", res_arr)

    a = res_arr[1]

    b = a.strip('\n').strip("'")

    # print("b: ", b)
    execfile(b)

    # p = subprocess.Popen(res_arr, stdin=subprocess.PIPE, shell=False,
    #                  stdout=subprocess.PIPE)
    # res2 = p.communicate()


    # print("res2: ", res_arr)

    # res1 = subprocess.check_call(cmd, shell=False)
    # res2 = subprocess.check_call(res1, shell=False)



    # print(cmd)
    # p1 = subprocess.call(cmd, shell=False)


    # cmd_proc = os.popen(cmd)

    # print("cmd_proc: ", cmd_proc)
    # exec(cmd_proc)


def unload_one_module_old(module_name):

    cmd = r"""modulecmd python unload %s""" % module_name
    cmd_proc = os.popen(cmd)
    # exec(cmd_proc)


def runBash(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    p.wait()
    out = p.stdout.read().strip()
    return out  #This is the stdout from the shell command


VERBOSE = False

def report(output, cmdtype="UNIX COMMAND:"):
   #Notice the global statement allows input from outside of function
   if VERBOSE:
       print("%s: %s" % (cmdtype, output))
   else:
       print(output)

# this uses argument 1, because the call to whoami is now frame 0.
# and similarly:
def callersname():
    return sys._getframe(2).f_code.co_name

# this uses argument 1, because the call to whoami is now frame 0.
# and similarly:
def thiscallersname():
    return sys._getframe(1).f_code.co_name


# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    pass