#!/usr/bin/env python3

__author__ = 'root'

import os
import glob
import subprocess
import optparse

import re

import csv
import sys, random, itertools
# import HTSeq
import shutil

import psutil
import sys

# from sh import ssh


import numpy as np
import pandas as pd


# from treedict import TreeDict
import redis
import time

import socket
import json

import sys

import imp

# this loads the global server configurations used for loading modules and classes
import config_utils

from naseq_utils import *
load_one_module(config_utils.naseq_pip_module)

pypack = os.environ['NASEQ_PYPACK']

print("pypack", pypack)
# add package path to system path
sys.path.append("%s/naseqpack" % (pypack))


ppath = os.environ['PYTHONPATH']
spath = sys.path
print("spath: ", ppath)
print("ppath: ", ppath)
sys.path.append(ppath)
print("spath: ", ppath)



# cmd = r"""modulecmd python load %s""" % "raglab/naseq_pip/0.9.4"
# cmd_proc = os.popen(cmd)
# print("cmd_proc: ", cmd_proc)
# exec(cmd_proc)

# cmd = r"""modulecmd python load %s""" % "raglab/naseq_pip/0.9.4"
# print(cmd)
# p1 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
# p1.wait()
# rc1 = p1.returncode
# print("rc1: ", rc1)
# moduleNames = ['sys', 'os', 're', 'unittest']
# >> moduleNames
# ['sys', 'os', 're', 'unittest']
# >>> modules = map(__import__, moduleNames)

# from naseqpack.naseq import NaSeqPip

# print(imp.find_module('naseq', ['/gs/project/wst-164-ab/share/raglab_prod/software/naseq_pip/naseq_pip_0_9_4/pypack/naseqpack']))
# print(imp.load_module('naseq', ['/gs/project/wst-164-ab/share/raglab_prod/software/naseq_pip/naseq_pip_0_9_4/pypack/naseqpack']))

# http://stackoverflow.com/questions/19009932/import-abitrary-python-source-file-python-3-3
# >> import importlib.machinery
# >>> loader = importlib.machinery.SourceFileLoader('a_b', '/tmp/a-b.txt')
# >>> mod = loader.load_module()
# >>> mod


# naseq = imp.load_source('naseq', "%s/naseqpack/naseq.py" % (pypack) )

# NaSeqPip = naseq.NaSeqPip

# dnaseq = imp.load_source('dnaseq', "%s/naseqpack/dnaseq.py" % (pypack) )

# rnaseq = imp.load_source('rnaseq', "%s/naseqpack/rnaseq.py" % (pypack) )
# datseq = imp.load_source('datseq', "%s/naseqpack/datseq.py" % (pypack) )

# from dnaseq import DnaSeqPip
# DnaSeqPip = dnaseq.DnaSeqPip

#
from naseq import *
print(":----------------------")

from dnaseq import DnaSeqPip
from rnaseq import RnaSeqPip
from datseq import DatSeqPip

from naseq_server_type import NaSeqServerType
from scan_config_naseq import ScanConfig
from redis_controller import RedisController


from flask import Flask
from flask_restful import Resource, Api

import sys

class ClientCtrl(Resource):

    def get(self, gid):

        res = ""

        self.gid = gid
        print("------------------------self.gid: ", self.gid)


        pid_str = self.nsp.redis_obj.redis_client.hget("client:%s" % self.gid, "pid")

        pid = int(pid_str)
        print("pid: ", pid)

        try:
            p = psutil.Process(pid)
        except psutil.NoSuchProcess:
            print("no process found")

            self.recycle_gid()
            res = "gid %s recycled " % self.gid

        else:
            childs = p.children(recursive=True)
            for child_idx in range(childs.__len__()):
                childp = childs[child_idx]
                print("child_idx, child: ", child_idx, childp)
        finally:
            print("in finally")


        return res

    def recycle_gid(self):

        exec_queue = self.nsp.redis_obj.redis_client.hget("client:%s" % self.gid, "exec_queue")
        args_json = self.nsp.redis_obj.redis_client.hget("client:%s" % self.gid, "args_json")
        # remove active status
        self.nsp.redis_obj.redis_client.lrem("active_gids", 0, self.gid)
        self.nsp.redis_obj.redis_client.hdel("client:%s" % self.gid, ["exec_queue", "args_json"])
        # repost work
        self.nsp.redis_obj.qwork = exec_queue
        self.nsp.redis_obj.redis_client.rpush(self.nsp.redis_obj.qwork_todo, args_json)


    @classmethod
    def set_nsp(cls, nsp):
        cls.nsp = nsp
        return cls

class Task(Resource):
    def get(self, task_id):

        res = ""
        if task_id == "task4":
            res = self.nsp.redis_obj.queue_active
        else:
            res = self.tasks[task_id]

        print("nsp", self.nsp, "task_id: ", task_id, " res: ", res)

        return res

    @classmethod
    def set_tasks(cls, tasks):
        cls.tasks = tasks
        return cls

    @classmethod
    def set_nsp(cls, nsp):
        cls.nsp = nsp
        return cls


class NaSeqServer(NaSeqPip, object):


    def __init__(self):

        super(NaSeqServer, self).__init__()

        self.__inst = None

        self.init_proj()

    def load_modules(self):
        pass

        # load_one_module('gcc/4.8.2')
        # load_one_module('OpenBLAS+LAPACK/0.2.12-openmp-gcc')
        # load_one_module('raglab/naseq_pip/0.9.4')

        # load_one_module('raglab/zlib/1.2.8')
        # load_one_module('gcc/4.8.2')
        # self.load_one_module('gcc/4.9.1')

        # load_one_module('raglab/jdk/1.7.0_71')
        # load_one_module('raglab/picard/1.128')
        # load_one_module('raglab/redis/2.8.18')

        # load_one_module('raglab/R/3.1.2')
        # load_one_module('raglab/samtools/1.2')
        # load_one_module('raglab/bcftools/1.2')
        # load_one_module('raglab/htslib/1.2.1')
        # load_one_module('raglab/jvarkit/14.11.28')
        # load_one_module('raglab/snpEff/4_0_e')
        # load_one_module('raglab/gatk/3.3.0')

        # load_one_module('raglab/sambamba/0.5.1')
        # load_one_module('raglab/bedtools/2.22.1')
        # load_one_module('raglab/ucscbio/4.0.0')
        #
        # load_one_module('raglab/trimmomatic/0.33')
        # load_one_module('raglab/bwa/0.7.12')



    def init_proj(self):

        self.load_modules()

        cnf = ScanConfig()
        # composition
        # project configuration
        self.conf = cnf


        # redis object
        self.redis_obj = RedisController(cnf)


    @property
    def inst(self):
        return self.__inst

    @inst.setter
    def inst(self, val):
        self.__inst = val

    # adds an instance of the configuration class to the NaseqServer object
    def factory(self):
        print("in naseq_factory ....")

        if self.conf.server_type == NaSeqServerType.dna_seq:
            self.inst = DnaSeqPip()
        elif self.conf.server_type == NaSeqServerType.rna_seq:
            self.inst = RnaSeqPip()
        elif self.conf.server_type == NaSeqServerType.dat_seq:
            self.inst = DatSeqPip()

        return self

    @property
    def client_naseq_factory(self):
        print("in client_naseq_factory ....")

        self.factory()

        self.inst.init_proj()
        self.inst.redis_obj.conf_redis_client()
        self.inst.redis_obj.connect_redis()

        return self

    @property
    def server_naseq_factory(self):
        print("in server_naseq_factory ....")

        # adds a NaseqServer subclass instance call inst
        self.factory()

        self.inst.init_proj()
        self.inst.redis_obj.conf_redis_server()
        self.inst.redis_obj.start_redis()
        self.inst.redis_obj.connect_redis_localhost()

        return self

    # ------------------------------------------------------------------------------------------------

def submit_qjobs_dat():

    print("in submit_qjobs_dat...")

    nsp = NaSeqServer().client_naseq_factory.inst

    print("self.dat_stages_arr :", nsp.conf.dat_stages_arr)

    if 'htchip_demul' in nsp.conf.dat_stages_arr:

        nsp.redis_obj.qwork = "htchip_demul_col"
        nsp.push_qjobs_htchipcol_idx()

    if 'extract_bams' in nsp.conf.dat_stages_arr:

        nsp.scan_bam_dir()
        nsp.save_bam_names()
        nsp.load_bam_names()

        nsp.redis_obj.qwork = "extract_one_read_group"
        nsp.push_qjobs_bam_idx()


def submit_rnvar_jobs_rna():

    print("in submin_rnvar_jobs_rna...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "trim_lane_trimmomatic"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "align_lane_tophat"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "merge_lanes"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "wiggle"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "merge_lanes_rnaseqc"
    nsp.push_jobs_smp_idx()

def submit_rnvar_jobs_dna():

    print("in submit_rnvar_jobs_dna...")

    nsp = NaSeqServer().client_naseq_factory.inst


    nsp.redis_obj.queue_work = "stats_smp_targ"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "stats_smpgrp_depth"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "indel_realigner"
    nsp.push_jobs_smp_idx_contig_idx()

    nsp.redis_obj.queue_work = "cat_realigned"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "sort_realigned"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "base_recalibrator"
    nsp.push_jobs_smp_idx()
    #
    nsp.redis_obj.queue_work = "call_hc"
    nsp.push_jobs_smp_idx_contig_idx()
    #
    nsp.redis_obj.queue_work = "cat_hc"
    nsp.push_jobs_smp_idx()
    #
    nsp.redis_obj.queue_work = "cat_hc_bam"
    nsp.push_jobs_contig_idx()

    nsp.redis_obj.queue_work = "genotype_gvcf_contig"
    nsp.push_jobs_contig_idx()

    nsp.redis_obj.queue_work = "cat_hc_contigs"
    nsp.push_jobs_noarg_idx()

    nsp.redis_obj.queue_work = "call_mp"
    nsp.push_jobs_region_idx()
    #
    nsp.redis_obj.queue_work = "cat_mp"
    nsp.push_jobs_noarg_idx()
    #
    nsp.redis_obj.queue_work = "call_ug"
    nsp.push_jobs_region_idx()
    #
    nsp.redis_obj.queue_work = "cat_ug"
    nsp.push_jobs_noarg_idx()

    nsp.redis_obj.queue_work = "rprts"
    nsp.push_jobs_rprt_idx()


def submit_rowcounts_rna():

    nsp = NaSeqServer().client_naseq_factory.inst


    nsp.redis_obj.queue_work = "raw_counts"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "rprts"
    jobs = [1]
    nsp.push_jobs_rprt_idx_arr(jobs)



def submit_all_jobs_rna():
    print("in submit_all_jobs_rna...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "trim_lane_trimmomatic"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "align_lane_tophat"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "merge_lanes"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "wiggle"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "merge_lanes_rnaseqc"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "fpkm_known"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "fpkm_rabt"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "fpkm_denovo"
    nsp.push_jobs_smp_idx()

    # only for one report
    nsp.redis_obj.queue_work = "c3p_gtf_gen"
    jobs = [1]
    nsp.push_jobs_rprt_idx_arr(jobs)

    nsp.redis_obj.queue_work = "qnsort"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "raw_counts"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "extract_merged_bam_to_fastqs"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "kallisto_smp_transcr"
    nsp.push_jobs_smp_idx()


    nsp.redis_obj.queue_work = "rseqc"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "rprts"
    # jobs = [1]
    # nsp.push_jobs_rprt_idx_arr(jobs)
    nsp.push_jobs_rprt_idx()


def submit_reports_dna():

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "stats_qc"
    jobs = [0, 1]
    nsp.push_jobs_rprt_idx_arr(jobs)


def submit_all_jobs_rna_only():

    # only dual indexing need ordered dicts

    print("in submit_all_jobs_rna_only...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "spln_calc"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "grind_lanes"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "merge_smpgrps"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "wiggle"
    nsp.push_jobs_smpgrp_idx()
    #
    nsp.redis_obj.queue_work = "qnsort"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "raw_counts"
    nsp.push_jobs_smpgrp_idx()

    # nsp.redis_obj.queue_work = "fpkm_known"
    # nsp.push_jobs_smpgrp_idx()
    #
    # nsp.redis_obj.queue_work = "fpkm_rabt"
    # nsp.push_jobs_smpgrp_idx()
    #
    # nsp.redis_obj.queue_work = "fpkm_denovo"
    # nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "rnaseqc_ini_rprt"
    jobs = [13,16,17,18,19,24]
    #jobs = range(25)
    nsp.push_jobs_cls_idx_arr(jobs)


    # nsp.redis_obj.queue_work = "rna_rprts"
    # jobs = [0,1,2,4,5]
    # nsp.push_jobs_rprt_idx_arr(jobs)


















    # nsp.redis_obj.queue_work = "indel_realigner"
    # nsp.push_jobs_smp_idx_contig_idx()
    #
    # nsp.redis_obj.queue_work = "grind_smps_gtk"
    # nsp.push_jobs_smp_idx()
    #
    # nsp.redis_obj.queue_work = "merge_smpgrps"
    # nsp.push_jobs_smpgrp_idx()
    #
    # nsp.redis_obj.queue_work = "call_hc"
    # nsp.push_jobs_smpgrp_idx_contig_idx()
    #
    # nsp.redis_obj.queue_work = "cat_hc"
    # nsp.push_jobs_smpgrp_idx()
    #
    # nsp.redis_obj.queue_work = "cat_hc_bam"
    # nsp.push_jobs_contig_idx()

    # nsp.redis_obj.queue_work = "genotype_gvcf_contig"
    # nsp.push_jobs_contig_idx()

    # nsp.redis_obj.queue_work = "cat_hc_contigs"
    # nsp.push_jobs_noarg_idx()
    #
    # nsp.redis_obj.queue_work = "call_mp"
    # nsp.push_jobs_region_idx()
    #
    # nsp.redis_obj.queue_work = "cat_mp"
    # nsp.push_jobs_noarg_idx()
    #
    # nsp.redis_obj.queue_work = "rprts"
    # nsp.push_jobs_rprt_idx()

def submit_qjobs_na_modseq():


    # only dual indexing need ordered dicts

    print("in submit_qjobs_na_modseq...")

    nsp = NaSeqServer().client_naseq_factory.inst

    # make sure active gids won't superpose when recycled
    nsp.redis_obj.redis_client.delete("active_gids")
    # nsp.redis_obj.redis_client.set("shutdown", 0)

    nsp.redis_obj.qwork = "pipe_ini"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)

    nsp.redis_obj.qwork = "fq2unal_parq_ini"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)
    #
    nsp.redis_obj.qwork = "fq2unal_parq"
    nsp.push_qjobs_spln_idx()

    nsp.redis_obj.qwork = "fq2unal_parq_fin"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)

    nsp.redis_obj.qwork = "clasif_seqs"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)

    nsp.redis_obj.qwork = "clasifseqs2fq"
    nsp.push_qjobs_spln_idx()

    nsp.redis_obj.qwork = "align_regal"
    nsp.push_qjobs_spln_idx()

    nsp.redis_obj.qwork = "segs2cnts"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)

    nsp.redis_obj.qwork = "gncnts_unpiv"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)




def submit_qjobs_na_comb2():


    # only dual indexing need ordered dicts

    print("in submit_qjobs_na_comb2...")

    nsp = NaSeqServer().client_naseq_factory.inst

    # make sure active gids won't superpose when recycled
    nsp.redis_obj.redis_client.delete("active_gids")
    # nsp.redis_obj.redis_client.set("shutdown", 0)


    nsp.redis_obj.qwork = "spln_calc"
    nsp.push_qjobs_spln_idx()

    nsp.redis_obj.qwork = "grind_lanes"
    nsp.push_qjobs_smp_idx()

    # variation preparation
    if nsp.conf.seq_exp in ["var", "rnvar"]:

        # gatk specific
        if "gtk" in nsp.conf.grind_callers:
            # base recalibration
            nsp.redis_obj.qwork = "grind_smps_gtk"
            nsp.push_qjobs_smp_idx()
            nsp.redis_obj.qwork = "merge_smpgrps_gtk"
            nsp.push_qjobs_smpgrp_idx()
        # all other callers
        if "oth" in nsp.conf.grind_callers:
            # abra realignment
            nsp.redis_obj.qwork = "grind_smps_oth"
            nsp.push_qjobs_smp_idx()
            nsp.redis_obj.qwork = "merge_smpgrps_oth"
            nsp.push_qjobs_smpgrp_idx()


    #
    if nsp.conf.seq_exp in ["var", "rnvar"]:

        if "gatk" in nsp.conf.grind_callers:
            nsp.redis_obj.qwork = "merge_smpgrps_gtk"
            nsp.push_qjobs_smpgrp_idx()

        if "oth" in nsp.conf.grind_callers:
            nsp.redis_obj.qwork = "merge_smpgrps_oth"
            nsp.push_qjobs_smpgrp_idx()


    if nsp.conf.seq_exp in ["exp", "rnvar"]:
        nsp.redis_obj.qwork = "merge_smpgrps_exp"
        nsp.push_qjobs_smpgrp_idx()


    # nsp.redis_obj.queue_work = "smpgrp_calc"
    # nsp.push_jobs_smpgrp_idx()

    # nsp.redis_obj.queue_work = "stats_qc"
    # 0: self.stats_lane,
    # 1: self.stats_smp,
    # 2: self.stats_smpgrp

    # jobs = [2]
    # nsp.push_jobs_rprt_idx_arr(jobs)


    # expression
    if nsp.conf.seq_exp in ["exp", "rnvar"]:

        nsp.redis_obj.qwork = "wiggle"
        nsp.push_qjobs_smpgrp_idx()
        #
        nsp.redis_obj.qwork = "qnsort"
        nsp.push_qjobs_smpgrp_idx()

        nsp.redis_obj.qwork = "raw_counts"
        nsp.push_qjobs_smpgrp_idx()

        nsp.redis_obj.qwork = "readcnt_rprt"
        jobs = [0]
        nsp.push_qjobs_rprt_idx_arr(jobs)



        # nsp.redis_obj.qwork = "fpkm_known"
        # nsp.push_qjobs_smpgrp_idx()
        #
        # nsp.redis_obj.queue_work = "fpkm_rabt"
        # nsp.push_jobs_smpgrp_idx()
        #
        # nsp.redis_obj.queue_work = "fpkm_denovo"
        # nsp.push_jobs_smpgrp_idx()

        # expression qc report
        # this is at sample level
        nsp.redis_obj.qwork = "rnaseqc_ini_rprt"
        # jobs = list(map(int, np.unique(nsp.conf.sample_grps['cls_idx'])))
        jobs = list(map(int, np.unique(nsp.conf.sample_names['cls_idx'])))
        print("jobs: 11111", jobs, type(jobs[0]))
        # jobs = [0]
        #jobs = range(25)
        nsp.push_qjobs_cls_idx_arr(jobs)

    # nsp.redis_obj.queue_work = "rna_rprts"
    # jobs = [0,1,2,4,5]
    # nsp.push_jobs_rprt_idx_arr(jobs)

    # variation
    if nsp.conf.seq_exp in ["var", "rnvar"]:

        for cler in nsp.conf.use_callers.split('-'):
            print("cler: ", cler)

            if "HC" in cler:
                nsp.redis_obj.qwork = "call_hc"
                nsp.push_qjobs_smpgrp_idx_contig_idx()

                nsp.redis_obj.qwork = "cat_hc"
                nsp.push_qjobs_smpgrp_idx()

                nsp.redis_obj.qwork = "cat_hc_bam"
                nsp.push_qjobs_contig_idx()
                #
                nsp.redis_obj.qwork = "genotype_gvcf_contig"
                nsp.push_qjobs_contig_idx()
                #
                nsp.redis_obj.qwork = "cat_hc_contigs"
                nsp.push_qjobs_noarg_idx()

            if "MP" in cler:
                nsp.redis_obj.qwork = "call_mp"
                nsp.push_qjobs_region_idx()

                nsp.redis_obj.qwork = "cat_mp"
                nsp.push_qjobs_noarg_idx()

            if "UG" in cler:
                nsp.redis_obj.qwork = "call_ug"
                nsp.push_qjobs_region_idx()

                nsp.redis_obj.qwork = "cat_ug"
                nsp.push_qjobs_noarg_idx()
                nsp.redis_obj.qwork = "readcnt_rprt"

        # combine results
        nsp.redis_obj.qwork = "comb_vars_rprt"
        jobs = [0]
        nsp.push_qjobs_rprt_idx_arr(jobs)

    # jobs = []
    # variation preparation
    # if nsp.conf.seq_exp in ["var", "rnvar"]:
    #     jobs.extend([0])
    # if nsp.conf.seq_exp in ["exp", "rnvar"]:
    #     jobs.extend([3, 8])

    # all reports combined exp/var/rnvar
    # nsp.redis_obj.queue_work = "rprts"
    # nsp.push_jobs_rprt_idx_arr(jobs)

    # nsp.push_jobs_rprt_idx()

    #  deploy everything
    nsp.redis_obj.qwork = "deploy_web"
    jobs = [0]
    nsp.push_qjobs_rprt_idx_arr(jobs)





def submit_all_jobs_na_comb2():

    # only dual indexing need ordered dicts

    print("in submit_all_jobs_na_comb2...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "spln_calc"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "grind_lanes"
    nsp.push_jobs_smp_idx()

    # variation preparation
    if nsp.conf.seq_exp in ["var", "rnvar"]:

        # gatk specific
        # if nsp.conf.realign_gatk:
        #     nsp.redis_obj.queue_work = "indel_realigner"
        #     nsp.push_jobs_smp_idx_contig_idx()

            # this is basically indel realigner cat, and base recalibration
            nsp.redis_obj.queue_work = "grind_smps_gtk"
            nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "merge_smpgrps"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "smpgrp_calc"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "stats_qc"
    # 0: self.stats_lane,
    # 1: self.stats_smp,
    # 2: self.stats_smpgrp

    jobs = [2]
    nsp.push_jobs_rprt_idx_arr(jobs)


    # expression
    if nsp.conf.seq_exp in ["exp", "rnvar"]:

        nsp.redis_obj.queue_work = "wiggle"
        nsp.push_jobs_smpgrp_idx()
        #
        nsp.redis_obj.queue_work = "qnsort"
        nsp.push_jobs_smpgrp_idx()

        nsp.redis_obj.queue_work = "raw_counts"
        nsp.push_jobs_smpgrp_idx()

        nsp.redis_obj.queue_work = "fpkm_known"
        nsp.push_jobs_smpgrp_idx()
        #
        # nsp.redis_obj.queue_work = "fpkm_rabt"
        # nsp.push_jobs_smpgrp_idx()
        #
        # nsp.redis_obj.queue_work = "fpkm_denovo"
        # nsp.push_jobs_smpgrp_idx()

        # expression qc report
        nsp.redis_obj.queue_work = "rnaseqc_ini_rprt"
        jobs = list(map(int, np.unique(nsp.conf.sample_grps['cls_idx'])))
        print("jobs: 11111", jobs, type(jobs[0]))
        # jobs = [0]
        #jobs = range(25)
        nsp.push_jobs_cls_idx_arr(jobs)

    # nsp.redis_obj.queue_work = "rna_rprts"
    # jobs = [0,1,2,4,5]
    # nsp.push_jobs_rprt_idx_arr(jobs)

    # variation
    if nsp.conf.seq_exp in ["var", "rnvar"]:

        for cler in nsp.conf.use_callers.split('-'):
            print("cler: ", cler)

            if "HC" in cler:
                nsp.redis_obj.queue_work = "call_hc"
                nsp.push_jobs_smpgrp_idx_contig_idx()

                nsp.redis_obj.queue_work = "cat_hc"
                nsp.push_jobs_smpgrp_idx()

                nsp.redis_obj.queue_work = "cat_hc_bam"
                nsp.push_jobs_contig_idx()
                #
                nsp.redis_obj.queue_work = "genotype_gvcf_contig"
                nsp.push_jobs_contig_idx()
                #
                nsp.redis_obj.queue_work = "cat_hc_contigs"
                nsp.push_jobs_noarg_idx()

            if "MP" in cler:
                nsp.redis_obj.queue_work = "call_mp"
                nsp.push_jobs_region_idx()

                nsp.redis_obj.queue_work = "cat_mp"
                nsp.push_jobs_noarg_idx()

    jobs = []
    # variation preparation
    if nsp.conf.seq_exp in ["var", "rnvar"]:
        jobs.extend([0])
    if nsp.conf.seq_exp in ["exp", "rnvar"]:
        jobs.extend([3, 8])

    # all reports combined exp/var/rnvar
    nsp.redis_obj.queue_work = "rprts"
    nsp.push_jobs_rprt_idx_arr(jobs)

    # nsp.push_jobs_rprt_idx()


def submit_all_jobs_rna2():

    # only dual indexing need ordered dicts

    print("in submit_all_jobs_rna2...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "spln_calc_rna"
    nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "grind_lanes_spl"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "indel_realigner"
    nsp.push_jobs_smp_idx_contig_idx()

    nsp.redis_obj.queue_work = "grind_smps_gtk"
    nsp.push_jobs_smp_idx()

    nsp.redis_obj.queue_work = "merge_smpgrps"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "call_hc"
    nsp.push_jobs_smpgrp_idx_contig_idx()

    nsp.redis_obj.queue_work = "cat_hc"
    nsp.push_jobs_smpgrp_idx()

    nsp.redis_obj.queue_work = "cat_hc_bam"
    nsp.push_jobs_contig_idx()
    #
    nsp.redis_obj.queue_work = "genotype_gvcf_contig"
    nsp.push_jobs_contig_idx()
    #
    nsp.redis_obj.queue_work = "cat_hc_contigs"
    nsp.push_jobs_noarg_idx()

    nsp.redis_obj.queue_work = "call_mp"
    nsp.push_jobs_region_idx()

    nsp.redis_obj.queue_work = "cat_mp"
    nsp.push_jobs_noarg_idx()

    nsp.redis_obj.queue_work = "rprts"
    nsp.push_jobs_rprt_idx()





def submit_all_jobs_dna():

    # only dual indexing need ordered dicts

    print("in submit_all_jobs_dna...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.redis_obj.queue_work = "spln_calc_rna"
    nsp.push_jobs_spln_idx()

    # nsp.redis_obj.queue_work = "trim_lane_trimmomatic"
    # nsp.push_jobs_spln_idx()

    nsp.redis_obj.queue_work = "stats_spln_blast"
    nsp.push_jobs_spln_idx()

    # nsp.redis_obj.queue_work = "align_lane_bwa"
    # nsp.push_jobs_spln_idx()

    # nsp.redis_obj.queue_work = "sort_lane"
    # nsp.push_jobs_spln_idx()

    # nsp.redis_obj.queue_work = "stats_spln_insert"
    # nsp.push_jobs_spln_idx()
    #
    # nsp.redis_obj.queue_work = "merge_lanes"
    # nsp.push_jobs_smp_idx()

    # nsp.redis_obj.queue_work = "indel_realigner"
    # nsp.push_jobs_smp_idx_contig_idx()
    #
    # nsp.redis_obj.queue_work = "cat_realigned"
    # nsp.push_jobs_smp_idx()
    #
    # nsp.redis_obj.queue_work = "sort_realigned"
    # nsp.push_jobs_smp_idx()
    #
    # nsp.redis_obj.queue_work = "base_recalibrator"
    # nsp.push_jobs_smp_idx()
    #
    # nsp.redis_obj.queue_work = "stats_smp_targ"
    # nsp.push_jobs_smp_idx()
    #
    # nsp.redis_obj.queue_work = "merge_smpgrps"
    # nsp.push_jobs_smpgrp_idx()
    #
    # nsp.redis_obj.queue_work = "stats_smpgrp_depth"
    # nsp.push_jobs_smpgrp_idx()

    # nsp.redis_obj.queue_work = "stats_qc"
    # 0: self.stats_lane,
    # 1: self.stats_sample

    # jobs = [0, 1]

    # haloplex hs does not have lanes
    # jobs = [1]

    # nsp.push_jobs_rprt_idx_arr(jobs)


    # nsp.redis_obj.queue_work = "call_hc"
    # nsp.push_jobs_smpgrp_idx_contig_idx()

    # nsp.redis_obj.queue_work = "cat_hc"
    # nsp.push_jobs_smpgrp_idx()


    # nsp.redis_obj.queue_work = "cat_hc_bam"
    # nsp.push_jobs_contig_idx()
    #
    # nsp.redis_obj.queue_work = "genotype_gvcf_contig"
    # nsp.push_jobs_contig_idx()
    #
    # nsp.redis_obj.queue_work = "cat_hc_contigs"
    # nsp.push_jobs_noarg_idx()
    #
    # nsp.redis_obj.queue_work = "call_mp"
    # nsp.push_jobs_region_idx()
    #
    # nsp.redis_obj.queue_work = "cat_mp"
    # nsp.push_jobs_noarg_idx()
    #

    # nsp.redis_obj.queue_work = "call_ug"
    # nsp.push_jobs_region_idx()
    #

    # nsp.redis_obj.queue_work = "cat_ug"
    # nsp.push_jobs_noarg_idx()
    #
    # nsp.redis_obj.queue_work = "rprts"
    # nsp.push_jobs_rprt_idx()



    # nsp.comb_vars()

def work_dna():

    nsp = NaSeqServer().client_naseq_factory.inst

    print("ok")

    # nsp.servers_obj.start_spark_master()
    # nsp.servers_obj.start_spark_slave()
    # nsp.servers_obj.drill_config_rest()
    # nsp.servers_obj.drill_rest_sql()

    nsp.cor_tsk = 12
    nsp.mem_tsk = 20000
    # nsp.cls_idx=2
    # nsp.redis_obj.qwork = "rnaseqc_ini_rprt"
    # nsp.rnaseqc_ini_rprt()

    # nsp.redis_obj.qwork = "deploy_web"
    # nsp.rprt_idx = 0
    # nsp.deploy_web()


    # nsp.redis_obj.qwork = "readcnt_rprt"
    #
    # nsp.rprt_idx = 0
    # nsp.readcnt_rprt()

    # nsp.fail2skip_spln_calc()
    # nsp.fail2skip_wiggle()

    # everything is dna now
    # nsp.rnaseqc_run()


    # for idx in range(nsp.nb_splns):
    #     nsp.spln_idx = idx
    #     nsp.stats_spln_insert()

    # nsp.spln_idx = 825
    # nsp.spln_idx = 50
    # nsp.spln_calc()

                # nsp.spln_calc_rna()

    # nsp.smp_idx = 50
    # nsp.smp_idx = 0
    # nsp.grind_lanes()
    # nsp.contig_idx = 0

    # nsp.indel_realigner()



    # nsp.smpgrp_idx = 0
    # nsp.merge_smpgrps()

                    # nsp.readcnt_rprt()
    # nsp.deploy_web()

                    # nsp.rnaseqc_rprt()
    # nsp.wiggle()
    # nsp.wiggle_x10()

    # nsp.deploy_ucsc_hub()
    # nsp.rprt_idx = 0
    # nsp.deploy_web()
    # nsp.qnsort()
    # nsp.raw_counts()


    # t= nsp.conf.proj_dir_loc

    # print("ok t: ", t)


    nsp.trim_lane_trimmomatic()
    # nsp.align_lane_tophat()
    # nsp.pst_lane_tophat()

    # x = nsp.conf.scratch_loc_dir
    # y = nsp.conf.scratch_glo_dir
    # z = nsp.conf.lab_dir
    # print("xyz: ", x, y , z)


    # nsp.align_lane_bwa()

    # nsp.stats_spln_insert()

    # nsp.stats_spln_blast()

    # nsp.stats_blast()


    # nsp.stats_insert()


    # nsp.redis_obj.queue_work = "cat_realigned"
    # nsp.push_jobs_smp_idx()


    # nsp.smp_idx = 0
    # nsp.merge_lanes()
    # nsp.smp_dedup_spl()
    # nsp.splitN_gatk()
    # nsp.ral_abra()

    # nsp.grind_lanes_spl()

    #
    # nsp.merge_lanes_ded_spl()

    # nsp.stats_smp_targ()

    # nsp.smpgrp_idx = 50
    # nsp.smpgrp_calc()

    # for smp_idx in range(nsp.nb_samples):
    #     nsp.smp_idx = smp_idx
    #     nsp.stats_smpgrp_depth()

    # nsp.stats_depth_calc()
    # nsp.gen_stats_depth_graph_files()

    # nsp.rprt_idx = 1
    # nsp.stats_qc()

    # nsp.metr_align_stats()

    # for contig_idx in range(nsp.nb_contigs):
    #     nsp.contig_idx = contig_idx
    # nsp.sort_lane()

    # for smp_idx in range(nsp.nb_samples):
    #     nsp.smp_idx = smp_idx
    #
    #     and index
    #     cmd = r"""sambamba index -t 12 %(in)s %(out)s
# """ % {"in": nsp.smp_gtk_am,
#        "out": nsp.smp_gatk_ai}
#         print(cmd)
#         p1 = subprocess.Popen(cmd, shell=True, cwd=nsp.align_smp_dir_glo)
#         p1.wait()
#         rc1 = p1.returncode
#         print("rc1: ", rc1)

    # nsp.smpgrp_idx = 18
    # nsp.merge_smpgrps()


    # nsp.stats_smpgrp_depth()

    # nsp.stats_lane()

    # nsp.stats_sample()

    # target
    # nsp.stats_targ()
    # nsp.stats_targ_graph()

    # nsp.rprt_idx = 1
    # nsp.stats_qc()


    # nsp.smp_idx = 0
    # nsp.contig_idx = 0

    # nsp.indel_realigner()

    # nsp.cat_realigned()
    # nsp.base_recalibrator()

    # nsp.smpgrp_idx = 41
    # nsp.contig_idx = 1
    # nsp.call_hc()

    # nsp.smpgrp_idx = 0
    # nsp.contig_idx = 0
    # nsp.call_hc()

    # nsp.smp_idx = 3
    # nsp.cat_hc()


    # nsp.contig_idx=0
    # nsp.cat_hc_bam()


    # nsp.region_idx = 5
    # nsp.call_mp()

    # nsp.noarg_idx = 0
    # nsp.cat_mp()


    # nsp.region_idx = 1
    # nsp.call_ug()


    # nsp.noarg_idx = 0
    # nsp.cat_ug()

    # nsp.comb_vars()


    # rsp.smp_idx = 0

    # nsp.merge_lanes()

    # nsp.comb_vars()
    # nsp.snpid()
    # nsp.snpeff()

    # nsp.smp_idx = 93
    # nsp.metr_align_stats()

    # nsp.rprt_idx = 2
    # nsp.stats_qc()

    # nsp.rprt_idx = 8
    # nsp.rprts()


# run this in 3 steps, not only one
def gen_targ():

    print("in naseq gen_targ....")

    # common trunk
    nsp = NaSeqServer().client_naseq_factory.inst


    # prepare target in karyotipic order
    nsp.conf.karyoorder_target_orig_bed()

    # spread for better parallelization
    # nsp.conf.spread_karyo_bed()

    # 1-st run
    nsp.conf.load_target_karyo_bed()

    nsp.conf.load_target_orig_bed()
    # this contigs are only the used ones
    nsp.conf.gen_target_contigs()

    nsp.conf.load_contig_names()


    # 3-rd run (needs contigs, to generate contig bed files)
    # bed files will be constructed from original bed
    nsp.conf.gen_target_beds(100)

        # 10 for wafergen
        # 10 for hla
        # 10 for haloplex
        # 100 for nextera
        # 100 for seq_cap
        # 100 for genomics_1k

    # 4-th run
    # this one needs the final karyotipic bed to chunk it in the final order
    # 1 for small amplicon projects
    # this is now included in previous step
    # nsp.conf.gen_region_beds_old(10)
    print("configuration ok....")

def init_next_queue():

    nsp = NaSeqServer().client_naseq_factory.inst

    print("nsp.conf.next_queue_dict_name: ", nsp.conf.next_queue_dict_name)
    for key, value in nsp.conf.next_queue_dict.items():
        print("key: ", key, " value: ", value)
        nsp.redis_obj.redis_client.hset("next_queue", key, value)

    # nsp.conf.next_queue_dict.get(self.redis_obj.queue_active, "nonext")
    # self.redis_client.hset("client:%s" % gid, "gid", gid)


def work_rna():

    print("in naseq work_rna....")

    nsp = NaSeqServer().client_naseq_factory.inst

    # generate bulk gtf

    # nsp.rprt_idx = 1
    # nsp.c3p_gtf_gen()

    # nsp.spln_idx=0
    # nsp.trim_lane_trimmomatic()

    # nsp.align_lane_tophat()



    # for idx in range(nsp.nb_samples):
    #     nsp.smp_idx = idx

    # nsp.smp_idx = 25

    # nsp.merge_lanes_rnaseqc()
    # nsp.raw_counts()

    # nsp.extract_merged_bam_to_fastqs()
    # nsp.kallisto_smp_transcr()

    # nsp.readcnt_rprt()




        # nsp.wiggle()

    # nsp.deploy_web()

    # nsp.rprt_idx = 1
    # nsp.rna_rprts()




    # nsp.deploy_ucsc_hub()
    # nsp.deploy_web()


    # nsp.raw_counts()

    # nsp.merge_lanes()
    # nsp.rseqc()


    # nsp.rprt_idx = 2
    # nsp.rprts()

#    nsp.c3p_gtf_gen()

    # nsp.smp_idx=0
    # nsp.raw_counts()


def work_dat():
    print("in naseq work_dat....")

    # under DAT

    # resume_server()


    # nsp.bam_idx = 0
    # nsp.extract_one_read_group()

    # queues = []
    # queues.extend(["htchip_demul_col", "extract_one_read_group"])

    # queues.extend(["spln_calc", "grind_lanes", "merge_smpgrps_exp", "wiggle", "qnsort"]
    # queues.extend(["grind_lanes"])
    # queues.extend(["wiggle"])
    # queues.extend(["fpkm_known"])
    # queues.extend(["rnaseqc_ini_rprt"])
    # queues.extend(["raw_counts", "readcnt_rprt"])
    # queues.extend(["grind_smps_gtk", "merge_smpgrps_gtk", "call_hc", "cat_hc",
    #                "cat_hc_bam", "genotype_gvcf_contig"])
    # queues.extend(["cat_hc_contigs"])
    # queues.extend(["grind_smps_oth", "merge_smpgrps_oth", "call_mp", "cat_mp", "call_ug", "cat_ug"])
    # queues.extend(["comb_vars_rprt"])
    # queues.extend(["deploy_web"])

    # queues = []
    # queues.extend(["extract_one_read_group"])
    # restart_queues(queues)

    nsp = NaSeqServer().client_naseq_factory.inst
    ro = nsp.redis_obj
    rc = ro.redis_client

    nsp.cor_tsk = 8
    nsp.mem_tsk = 20000


    # nsp.scan_bam_dir()
    # nsp.save_bam_names()
    # nsp.load_bam_names()
    #


    # nsp.red_shuf_fastq_files(90000, 50000)


    # from fasta
    # ---- regular
    # nsp.ref_gen_orig_genome_faindex()
    # nsp.pyfasta_split_orig_genome()

    #prepare and dump ucsc_pri_chroms and supl_chroms from orig into sequence/chrom
# need ordered contigs in orig/refgen.contigs.csv
#     gets them from sequence/chrom/ dir
#     nsp.chroms_to_genome_fa()
#     nsp.reference_dict()
#     nsp.reference_faindex()
#     nsp.reference_chrominfo()

    # work the target inside the igenomes space
    # we need the genomic target first to run spark (needs any target for initialization)
    # nsp.gen_genomic_target()

    # move file (ex: h2Mm10_gen_10k.karyo.bed) to transcriptome (genome based transcriptome one)
    # from target as .mer.bed in orig folder
    # ex: cp ../all_targ/h4Mm10_geM25.mer/h4Mm10_geM25.mer.karyo.bed ../orig/
    # ex: mv ../orig/h4Mm10_geM25.mer.karyo.bed ../orig/h4Mm10_geM25.mer.bed
    # ex2: cp ../all_targ/h4Hg38_geM36.mer/h4Hg38_geM36.mer.karyo.bed ../orig/
    # ex2: mv ../orig/h4Hg38_geM36.mer.karyo.bed ../orig/h4Hg38_geM36.mer.bed

    # generate target files
    # gen_targ()

    # swith transcriptome to work the GTF with DNA (spark)
    # .work() instead of .work-dat()
    # come back with a gtf: cp gencode-exons.csv ../../orig/hyMm10_geM16.gtf
    #                       cp ../srvf/filef/gencode-exons.csv ../orig/h2Mm10_geM20.gtf
    #                       cp ../srvf/filef/gencode-exons.csv ../orig/h4Mm10_geM25.gtf
    #  ex3: cp ../srvf/filef/gencode-exons.csv ../orig/h4Hg38_geM36.gtf

    # for DNA PIPELINE
    # nsp.bwa_index()

    # KALISTO
    # extract the transcriptome
    # nsp.gffread_transcripts()
    # nsp.kallisto_index()

    # these are transcriptome indexes, should be under transcriptome name

    # also need a link to gtf
    # this is needed for rnaseqc
    # nsp.transcr_to_nz_bed()
    #  generate a target bed3 file from merging transcript positions
    # nsp.transcr_to_target_bed()


    # nsp.bt2_index()


    # nsp.bt2_transcr_link()



    # this one is to be run under DNA
    # no need for h5 anymore
    # spark, sc = nsp.servers_obj.get_new_spark_session(1, 10, "work_gtf")
    # nsp.servers_obj.set_spark_session()

    # need this also under DNA - for expression matrix -readcnt_rprt
    # nsp.sc_qc_obj.spark_save_genes_tracking_ids()
    # spark.stop()


    # nsp.gtf_to_h5()
    # nsp.gtf_parse()
    # nsp.chrnames_parse_mm10()
    # This can add tss_id and p_id / nice to follow
    # https://github.com/davek44/utility/blob/master/
    # cuffify_gtf.py
    # nsp.chrnames_parse()
    # nsp.gtf_ren_chrnames_rn6()
    # nsp.gtf_ren_chrnames_mm10()

    # nsp.gtf_to_csv()
    # nsp.h5_to_tracking_ids()

    # nsp.query_h5()

    # nsp.bt2_transcr_index()
    # nsp.star_index()




    # DEPLOY ON INTERACTIVE DAT

    # nsp.deploy_haa()


    # nsp.deploy_hba()

    # nsp.deploy_hca()

    # nsp.deploy_hcb()

    # nsp.deploy_hda()

    # nsp.deploy_eda()

    # nsp.deploy_iaa()


    # nsp.deploy_naa()
    # nsp.deploy_gaa()


    # nsp.deploy_taa()

    # nsp.deploy_aaa()

    # nsp.deploy_aba()

    # nsp.deploy_a003()

    # nsp.deploy_a004()

    # nsp.deploy_a005()
    # nsp.deploy_a006()

    # nsp.deploy_a008()
    # nsp.deploy_a009()
    # nsp.deploy_a010()

    # nsp.deploy_a011()

    # nsp.deploy_a012()

    # nsp.deploy_a013()
    # nsp.deploy_a014()

    # nsp.deploy_a015()
    # nsp.deploy_a016()

    # nsp.deploy_a00ht()

    # nsp.deploy_a018()
    # nsp.deploy_a019()
    # nsp.deploy_a021()

    # nsp.ren_shuf_fastq_files()

    # nsp.deploy_a024()
    # nsp.deploy_a025()

    # nsp.deploy_a026()
    # nsp.deploy_a027()
    # nsp.deploy_a028()
    # nsp.deploy_a029()
    # nsp.deploy_a001()
    # nsp.deploy_a030()
    # nsp.deploy_a031()
    # nsp.deploy_a032()
    # nsp.deploy_a035()
    # nsp.deploy_a036()
    # this is gene fusions
    # nsp.deploy_a034()
    # nsp.deploy_a037()
    # nsp.deploy_a034_2()
    # nsp.deploy_a038()
    # nsp.deploy_a038_ctl()

    # nsp.deploy_a040()
    # nsp.deploy_a041()
    # nsp.deploy_a041_redo()

    # nsp.deploy_a042()
    # nsp.deploy_a043()
    # nsp.deploy_a044()
    # nsp.deploy_a045()
    # nsp.deploy_a050()
    # nsp.deploy_a051()
    # nsp.deploy_a083()
    # nsp.deploy_a084()

    # nsp.deploy_a085()
    # nsp.deploy_a086()
    # nsp.deploy_a087()
    # nsp.deploy_a088()
    # nsp.deploy_a090()
    # nsp.deploy_a092()
    # nsp.deploy_a093()
    # nsp.deploy_a094()
    # nsp.deploy_a095()
    # nsp.deploy_a094_raw()
    # nsp.deploy_a097()
    # nsp.deploy_a098()
    #  nsp.deploy_a099()
    # nsp.deploy_a101()
    # nsp.deploy_a104()



    # nsp.ren_shuf_fastq_files()
    # nsp.deploy_t001()

    # scan_paired_reads is now integrated to the data pipeline
    # this is regular

    # need real deconvoluted path
    # project_regex = "(\/project\/6004729\/share\/data\/)"
    # project_regex = "(\/home\/dbadescu\/share\/data\/)"
    # project_regex = "\/lb\/scratch\/dbadescu\/data\/"

    # regular location in data folder

    project_regex ="\/lb\/project\/ioannisr\/share\/data\/"

    # for afl, this only checks the number of levels to the project, align the sublevels up to the sample
    # project_regex = "^\/lb\/project\/ioannisr\/NOBACKUP\/dbadescu\/rnvar\/[^\/]+\/align2\/"

    # project_regex = "(\/scratch\/badescud\/data\/)"
    # project_regex = "(\/home\/badescud\/projects\/rrg-ioannisr\/share\/data\/)"
    # project_regex = "(\/gs\/project\/wst-164-aa\/share\/data\/)"
    # project_regex = "(\/gs\/project\/wst-164-aa\/share\/rnvar\/)"

            # project_regex = "(\/gs\/project\/wst-164-aa\/data\/)"
    # this is for htchip
    # project_regex = "(\/lb\/project\/ioannisr\/share\/expr\/)"

    nsp.reads_dir = nsp.conf.raw_afl_dir
    # nsp.reads_dir = nsp.conf.raw_reads_dir

    # nsp.raw_reads_to_all_sample_info_pe(project_regex)
          #### nsp.raw_reads_to_all_sample_info_se(project_regex,False)

    # nsp.all_sample_to_one_sample_info()
    # nsp.one_sample_to_grp_sample_info()
    # nsp.grp_sample_to_sc_grp_sample_info()


    # nsp.htchipcol_idx = 0
    # nsp.htchip_demul_col()

    # nsp.htchipcol_idx = 16
    # nsp.htchip_demul_col()

    # nsp.htchip_stats_fastq()
    # nsp.htchip_debug()

    # for bam_idx in range(nsp.nb_bams):
    #     nsp.bam_idx=bam_idx
    #     nsp.extract_one_read_group()

    # for bam_idx in range(2):
    #     nsp.bam_idx=bam_idx
    #
    # nsp.extract_bams()



def deploy_eba():
    print("in deploy_dat ...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.fastq_dir_name = "fastq"
    # nsp.depl_dir = "/gs/project/wst-164-ab/expr/eaa-014/raw_reads"
    nsp.depl_dir = "/gs/project/wst-164-ab/expr/eba-015/raw_reads"

    nsp.smp_ren_dict = dict(TC_Nextera_NTC='Paul3_TC_Nextera_NTC',
                            TC_Smarter_NTC='Paul3_TC_Smarter_NTC')
    nsp.fastq_gq00_deploy("", "-eba", "", "")

def deploy_eca():
    print("in deploy_dat ...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.fastq_dir_name = "fastq"
    nsp.depl_dir = "/gs/project/wst-164-ab/expr/eca-016/raw_reads"

    # nsp.smp_ren_dict = dict(TC_Nextera_NTC='Paul3_TC_Nextera_NTC',
    #                         TC_Smarter_NTC='Paul3_TC_Smarter_NTC')
    nsp.fastq_gq00_deploy("", "-eca", "", "")



def deploy_dac():
    deploy_dat_gq00_901()



def deploy_dat_gq00_901():

    print("in deploy_dat_gq00_901 ...")

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.fastq_dir_name = "/gs/project/wst-164-ab/data/901-morag/fastq-gq00"
    nsp.depl_dir = "/gs/project/wst-164-ab/exome/dac-901/raw_reads"

    nsp.fastq_gq00_deploy("", "-dac", "", "")


def depl_dat():


    nsp = NaSeqServer().client_naseq_factory.inst
    nsp.deploy_dat_gq00_008()

def submit_jobs_dat():

    init_next_queue()
    submit_all_jobs_dat()


def submit_jobs_dna():

    init_next_queue()
    submit_all_jobs_dna()

def submit_jobs_rna():

    init_next_queue()
    submit_all_jobs_rna()

def submit_jobs_rna2():

    init_next_queue()
    submit_all_jobs_rna2()

def submit_jobs_na_comb2():

    init_next_queue()
    submit_all_jobs_na_comb2()

def submit_jobs_rna_only():

    init_next_queue()
    submit_all_jobs_rna_only()

def prob():

    nsp = NaSeqServer().client_naseq_factory.inst
    ro = nsp.redis_obj
    rc = ro.redis_client

    ro.qgraph_job_status()


def work():


    # resume_server()
    # queues = ["genotype_gvcf_contig"]
    # queues = []
    # queues.extend(["spln_calc"])
    # queues.extend(["grind_lanes"])
    # queues.extend(["merge_smpgrps_exp"])
    # queues.extend(["wiggle"])
    # queues.extend(["qnsort"])
    # queues.extend(["raw_counts"])
    # queues.extend(["readcnt_rprt"])
    # queues.extend(["fpkm_known"])
    # queues.extend(["rnaseqc_ini_rprt"])
    # queues.extend(["grind_smps_gtk"])
    # queues.extend(["merge_smpgrps_gtk"])
    # queues.extend(["call_hc"])
    # queues.extend(["cat_hc"])
    # queues.extend(["cat_hc_bam"])
    # queues.extend(["genotype_gvcf_contig"])
    # queues.extend(["cat_hc_contigs"])
    # queues.extend(["grind_smps_oth"])
    # queues.extend(["merge_smpgrps_oth"])
    # queues.extend(["call_mp"])
    # queues.extend(["cat_mp"])
    # queues.extend(["call_ug"])
    # queues.extend(["cat_ug"])
    # queues.extend(["comb_vars_rprt"])
    # queues.extend(["deploy_web"])

    # queues = []
    # queues.extend(["pipe_ini"])
    # queues.extend(["fq2unal_parq_ini","fq2unal_parq","fq2unal_parq_fin"])
    # queues.extend(["fq2unal_parq_fin"])
    # queues.extend(["clasif_seqs"])
    # queues.extend(["clasifseqs2fq"])
    # queues.extend(["align_regal"])
    # queues.extend(["segs2cnts"])
    # queues.extend(["gncnts_unpiv"])
    #
    # restart_queues(queues)

    # submit_qjobs_na_modseq() # afl
    # submit_qjobs_na_comb2() # raw



    nsp = NaSeqServer().client_naseq_factory.inst
    ro = nsp.redis_obj
    rc = ro.redis_client
    # nsp.redis_obj.spawn_ctrl()


    # nsp.redis_obj.qwork = "align_regal"
    # nsp.push_qjobs_spln_idx()


    # nsp.sc_qc_obj.spark_work_reference()
    # this is only once
    # nsp.sc_qc_obj.spark_valid_bins()



    nsp.cor_tsk = 8
    nsp.mem_tsk = 20000

    # spark, sc = nsp.servers_obj.get_new_spark_session(2,10,"create_tables")
    # nsp.sc_qc_obj.spark_create_tables_meta_seg()
    # spark.stop()

    # nsp.sc_qc_obj.spark_create_tables_meta2()
    # nsp.sc_qc_obj.spark_export_gncnts_tag()
    # spark.stop()

    # feat_mdl_a=["STE"]
    # cnt_typ_a=["UDHP"]
    # tag_a=["a083","a085","a087","a088"]
    # nsp.import_gncnts_tags(feat_mdl_a,cnt_typ_a,tag_a)
    #
    # nsp.gncnts_unpiv_tags()
    # nsp.prep_gene_model()

    # nsp.servers_obj.start_derby_server()
    # nsp.servers_obj.start_thrift_server()

    # time.sleep(20)



    # nsp.redis_obj.qwork = "pipe_ini"
    # nsp.rprt_idx = 0

    # spark, sc = nsp.servers_obj.get_new_spark_session(1, 10, "additional")
    # nsp.sc_qc_obj.spark_create_tables_meta_test()
    # spark.stop()

    # nsp.pipe_ini()
    #
    # nsp.redis_obj.qwork = "fq2unal_parq_ini"
    # nsp.rprt_idx = 0
    # nsp.fq2unal_parq_ini()
    #

    nsp.reads_dir = nsp.conf.raw_afl_dir

    #
    # nsp.redis_obj.qwork = "fq2unal_parq"
    # for i in range(0,1):
    #     nsp.spln_idx = i
    #     nsp.fq2unal_parq()

    # nsp.redis_obj.qwork = "fq2unal_parq_fin"
    # nsp.rprt_idx = 0
    # nsp.fq2unal_parq_fin()
    #
    # nsp.redis_obj.qwork = "clasif_seqs"
    # nsp.rprt_idx = 0
    # nsp.clasif_seqs()
    #
    # nsp.redis_obj.qwork = "clasifseqs2fq"
    # for i in range(0,1):
    #     nsp.spln_idx = i
    #     nsp.clasifseqs2fq()
    #
    # nsp.redis_obj.qwork = "align_regal"
    # for i in range(0,9):
    #     nsp.spln_idx = i
    #     nsp.align_regal()

    #segs table
    # nsp.sc_qc_obj.spark_create_tables_meta6()
    #


    # nsp.redis_obj.qwork = "segs2cnts"
    # nsp.rprt_idx = 0
    # nsp.segs2cnts()
    #
    nsp.redis_obj.qwork = "gncnts_unpiv"
    nsp.rprt_idx = 0
    nsp.gncnts_unpiv()



    #
    # 10x project
    # nsp.bam10x_cb()
    # proj_name="opk389b"
    # seq_typ = "rna10x"
    # nsp.bam10x_snps_vaf(proj_name,seq_typ)
    # must run .jl script on res/opk389b/reg-cov.bed
    # nsp.bam10x_snps_vaf2(proj_name,seq_typ)

    # nsp.gen_tr_cov_prop()
    # nsp.test88()
    #
    # nsp.sc_qc_obj.spark_create_tables_meta5()

    # for i in range(0,3):
    #     print ("I:", i)
    #     nsp.spln_idx = i
    #     for regal in ["TP", "MP", "HP"]:
    #         nsp.spln_regal = regal
    #         nsp.iter_tr_cov_prop2()
            # nsp.export_tr_cov_prop()


    #
    # exit(1)




    # nsp.smpgrp_idx = 2
    # nsp.wiggle()

    # nsp.redis_obj.qwork = "deploy_web"
    # nsp.rprt_idx = 0
    # nsp.deploy_web()
    #



    # exit(1)

    # nsp.servers_obj.set_spark_session()

    # nsp.smp_idx = 2
    # nsp.sc_qc_obj.spark_deconvol_umis_reg_al("H","HP")
    # nsp.sc_qc_obj.spark_gncnts_unpiv("UDHP")



    # nsp.sc_qc_obj.spark_valid_bins()





        # nsp.servers_obj.set_spark_session()




    # nsp.spln_calc()
    # nsp.extract_noncan()

    # CLASSIC PIPELINE

    # nsp.redis_obj.qwork = "spln_calc"
    # nsp.spln_idx = 1
    # nsp.spln_calc()

    # nsp.redis_obj.qwork = "grind_lanes"
    # nsp.smp_idx = 0
    # nsp.grind_lanes()

    # nsp.redis_obj.qwork = "grind_smps_gtk"
    # nsp.smp_idx = 0
    # nsp.grind_smps_gtk()

    # nsp.redis_obj.qwork = "grind_smps_oth"
    # nsp.smp_idx = 0
    # nsp.grind_smps_oth()

    # nsp.redis_obj.qwork = "call_hc"
    # nsp.smpgrp_idx = 0
    # nsp.contig_idx = 0
    # nsp.call_hc()


    # nsp.redis_obj.qwork = "raw_counts"
    # nsp.smpgrp_idx = 4
    # nsp.raw_counts()
    #

    # nsp.redis_obj.qwork = "readcnt_rprt"
    # nsp.rprt_idx = 0
    # nsp.readcnt_rprt()
    #
    # nsp.dbnsfp()

    # nsp.redis_obj.qwork = "deploy_web"
    # nsp.rprt_idx = 0
    # nsp.deploy_web()



    # nsp.redis_obj.qwork = "readcnt_rprt"
    # jobs = [0]
    # nsp.push_qjobs_rprt_idx_arr(jobs)
    # nsp.rprt_idx = 0
    # nsp.readcnt_rprt()
    # this is part of the regular pipeline
    # nsp.readcnt_prefix = "all"
    # nsp.sc_qc_obj.spark_parse_readcnt_known_genes_matrix()



    # nsp.sc_qc_obj.spark_parse_config()
    # nsp.sc_qc_obj.spark_parse_csv("flt_genes")
    # nsp.sc_qc_obj.spark_export_parquet("flt_gene_ids")


    # nsp.sc_qc_obj.spark_import_parquet("flt_gene_ids")
    # nsp.sc_qc_obj.spark_export_simple_csv("flt_gene_ids")

    # nsp.sc_qc_obj.spark_import_json_file("meta_smps")

    # nsp.readcnt_prefix = "ercc"
    # nsp.sc_qc_obj.spark_import_simple_csv("flt_ercc_ids")
    # nsp.sc_qc_obj.gmean_exmp(gene_filter="flt_ercc_ids")
    # nsp.sc_qc_obj.gene_flt_smp_cnt_unpiv(include_meta_smps=True)


    # nsp.readcnt_prefix = "flt"
    # nsp.sc_qc_obj.spark_import_simple_csv("flt_gene_ids")
    # nsp.sc_qc_obj.gmean_exmp(gene_filter="flt_gene_ids")
    # nsp.sc_qc_obj.gene_flt_smp_cnt_unpiv(include_meta_smps=True)


    # nsp.readcnt_prefix = "all"
    # nsp.sc_qc_obj.gmean_exmp(gene_filter="gene_trk_ids")
    # nsp.sc_qc_obj.gene_flt_smp_cnt_unpiv(include_meta_smps=False)
    # nsp.sc_qc_obj.deploy_figs2a_b_c()

    # Expression matrix
    # nsp.sc_qc_obj.spark_work_expr()


    # nsp.sc_qc_obj.spark_parse_csv("flt_ercc")

    # "pdx1876vdt","pdx1735vdt" ,"pdxbcm2665vdt", "pdxhci001vdt"
    # nsp.sc_qc_obj.spark_parse_dna_pdx_ex_vdt()

    # spark work
    # nsp.sc_qc_obj.spark_work_esnv(dna_filter=False)




    # GTF part
    # IMPORT GTF
    # from srvf/filef/gencode.gtf
    # ex1: cp ../orig/gencode/gencode.v36.primary_assembly.annotation.gtf ../srvf/filef/gencode.gtf

    # nsp.sc_qc_obj.gtf_to_csv()


    spark, sc = nsp.servers_obj.get_new_spark_session(1, 10, "work_gtf")

    # nsp.sc_qc_obj.spark_parse_gtf_csv()

    # extract seqnames used in gtf exons
    # nsp.sc_qc_obj.spark_save_gtf_embl_seqnames()

    # manualy match gtf, and fa names from ensembl and ucsc
    # add supplementry contigs to contig names
    # nsp.sc_qc_obj.spark_import_simple_csv("contig_names")
    # nsp.sc_qc_obj.spark_import_simple_csv("contig_names_supl")
    #
    # nsp.sc_qc_obj.spark_import_simple_csv("genes_supl")
    # nsp.sc_qc_obj.spark_import_simple_csv("transcripts_supl")
    # nsp.sc_qc_obj.spark_import_simple_csv("exons_supl")
    # nsp.sc_qc_obj.spark_import_simple_csv("transcript_exons_supl")

    # Extract genes,transcripts,exons, transcript_exons from gtf table and merge with suplementary
    # nsp.sc_qc_obj.spark_extract_gtf_normalize()

    # EXPORT to parquet files for import in analysis databases
    # templ/hive
    # nsp.sc_qc_obj.spark_export_reference()


    # EXPORT to GTF
    # nsp.sc_qc_obj.spark_prepare_gtf_exons()
    # nsp.sc_qc_obj.spark_save_gtf_csv()

    # nsp.sc_qc_obj.spark_prepare_gtf_exons_ucsc()
    # nsp.sc_qc_obj.spark_save_gtf_ucsc_csv()

    # END EXPORT to GTF, return to work-dat
    spark.stop()


    spark, sc = nsp.servers_obj.get_new_spark_session(1, 10, "work2")

    # nsp.sc_qc_obj.spark_unpart_clasif_seqs()
    # nsp.sc_qc_obj.spark_unpart_clasif_seqs_r2()
    # nsp.sc_qc_obj.spark_unpart_umis()




    # nsp.sc_qc_obj.spark_create_tables_meta3()

    # nsp.sc_qc_obj.spark_create_tables_meta()

    # smp_name="20181201PP-9-OOCYTES_A088"
    # rnln="1001_1002"
    # for reg_al in ["TP","HP"]:
    #     in_d = "/home/dbadescu/scratch/rnvar/a088-x088-oocytes-e4/align2/%s/%s/%s/genomecov" % (smp_name,rnln,reg_al)
        # nsp.sc_qc_obj.spark_loadcover_orc(smp_name,reg_al,in_d)


    # smp_name="OOCYTECDNA-157PG_A084"
    # reg_al = "HP"
    # seqname = "chr18"
    # in_d = "/home/dbadescu/share/over/duny/trex2percov/%s_%s_%s.orc" % (smp_name, reg_al, seqname)
    # print("in_d: ", in_d)

    # nsp.sc_qc_obj.spark_loadcover_redis(smp_name,reg_al,seqname,in_d)





    # smp_name="SRR7695400"
    # reg_al="MP"
    # in_d="/home/dbadescu/share/raglab_prod/software/sra/genomecov"
    # nsp.sc_qc_obj.spark_loadcover_orc(smp_name, reg_al, in_d)

    # nsp.sc_qc_obj.spark_import_orc("contig_names")
    # nsp.sc_qc_obj.spark_create_table_utrs()
    # nsp.sc_qc_obj.spark_valid_utrs_bins()


    # smp_name="20181201PP-9-OOCYTES_A088"
    # fastq_bases =   606647800
    # fastq_bases = 10108695400

    #=sambamba flagstat =35550138*150 length =5332520700
    # smp_name = "SRR7695400"
    # fastq_bases = 5332520700
    # nsp.sc_qc_obj.spark_cov_per_base(smp_name,fastq_bases)
    # nsp.sc_qc_obj.spark_export_utr_cov_base(smp_name)


    # nsp.sc_qc_obj.spark_save_ref_gen_orig_contigs()
    # nsp.sc_qc_obj.spark_test_pyarrow()

    # nsp.sc_qc_obj.spark_load1xbam()
    # nsp.sc_qc_obj.spark_savestats1xbam()
    # nsp.sc_qc_obj.spark_savestats2xbam()

    # nsp.sc_qc_obj.spark_homol_mm()

    # nsp.sc_qc_obj.spark_import_parquet("parq_arrow")
    # nsp.sc_qc_obj.spark_import_parquet("ali_bam_segs")
    # nsp.sc_qc_obj.spark_import_parquet("unal_parq")




    ### nsp.sc_qc_obj.spark_work_umis_schemas_unal_5pfrag0()
    ### nsp.sc_qc_obj.spark_deconvol_umis_reg5p_lane("wt", "run4815_1")
    ### nsp.sc_qc_obj.spark_deconvol_umis_reg5p_lane("Oocytes_1of4_2", "run001_2")

    spark.stop()

    exit(1)

    # fq2unal_parq - here
    # nsp.sc_qc_obj.spark_fq2unal_parq()
    for smp in ["wt", "mut"]:
        for rnln in ["run4815_1", "run4815_2"]:
        # for rnln in ["run4815_1"]:
            nsp.sc_qc_obj.spark_parse_clasif_r1(smp, rnln)
            # for reg_typ in ["T", "H", "M"]:
            for reg_typ in ["H"]:
                nsp.sc_qc_obj.spark_parse_r2_join_r1(smp, rnln, reg_typ)
                nsp.sc_qc_obj.spark_clasif_seqs_to_fq(smp, rnln, reg_typ)
                pass

    # hisat_align_reg_al - here
    for smp in ["wt","mut"]:
        for rnln in ["run4815_1", "run4815_2"]:
        # for rnln in ["run4815_2"]:
            for reg_al in ["HP"]:
                # nsp.sc_qc_obj.spark_alipark2bin_segs(smp, rnln, reg_al)
                pass

    for smp in ["wt","mut"]:
        # nsp.sc_qc_obj.spark_deconvol_umis_reg_al(smp,"H","HP")
        pass

    nsp.sc_qc_obj.spark_gncnts_unpiv("UDHP")





    # nsp.sc_qc_obj.spark_work_umi_schemas_segs()

    # segs4 for efficiency
    # nsp.sc_qc_obj.spark_work_umi_schemas_segs2()

    # nsp.sc_qc_obj.spark_deconvol_umis5p()
    # nsp.sc_qc_obj.spark_save_umis5p()

    # nsp.sc_qc_obj.spark_deconvol_umis3()
    # nsp.sc_qc_obj.spark_save_umis3()






    # nsp.sc_qc_obj.spark_save_umis0()
    # old
    #     nsp.sc_qc_obj.spark_deconvol_umis("a1","run001_1")
    #     nsp.sc_qc_obj.spark_deconvol_umis2()
    #     nsp.sc_qc_obj.spark_deconvol_umis("Oocytes_1of4_2")

    # nsp.sc_qc_obj.spark_work_segs()

    # nsp.sc_qc_obj.spark_export_reference()
    #     nsp.sc_qc_obj.spark_work_unal()








    # nsp.sc_qc_obj.spark_export_gene_mut()

    # nsp.servers_obj.drill_config_init()

    # nsp.rename_variants()
    # nsp.cor_tsk = 12
    # nsp.mem_tsk = 20000
    # nsp.comb_vars()
    # ro.qgraph_job_status()

    # nsp = NaSeqServer().client_naseq_factory.inst
    # rc = nsp.redis_obj
    # nsp.redis_obj.spawn_ctrl()

    #

    # nsp.redis_obj.spawn_ctrl()

    # done = rc.qgraph_all_done
    # print("done: ", done)

    # nsp.redis_obj.spawn_nodectrl()
    # nsp.redis_obj.wait_for_shutdown_server()

    # create and configure graph
    # nsp.redis_obj.qgraph_config(nsp.conf.qgraph_dict)

    # check queues
    # rc.qgraph_ckeck_mark_done()
    # rc.qgraph_ckeck_mark_open()
    # ctrl()

    # print("open_queues: ", nsp.redis_obj.qgraph_prop_open)


    # submit_qjobs_na_comb2()
    # print("all done: ", nsp.redis_obj.qgraph_all_done)




    # depl_dat()

    # submit_jobs_dna()
    #submit_jobs_rna_only()

    # submit_jobs_na_comb2()



    # submit_reports_dna()

    # work_dna()
    # print("sasdf")


    # submit_jobs_dat()
    # work_dat()




    # deploy_eba()
    # deploy_baa()
    # deploy_dac()
    # deploy_eca()

    # deploy_dat_fl1()
    # deploy_dat_fl2()

    # init_next_queue()
    # submit_all_jobs_rna()

    # submit_rowcounts_rna()

    # work_rna()

    # set project type to RNA
    # init_next_queue()
    # submit_rnvar_jobs_rna()

    # set project type to DNA
    # submit_rnvar_jobs_dna()


def matrix_metrics_rna():
    #load initial matrix
    print("in global matrix_metrics:")
    rsp = RnaSeqPip()
    #dsp.gen_sample_subm()

    rsp.init_proj()
    rsp.set_proj_dirs()
    rsp.set_proj_files_path_names()
    rsp.matrix_metrics()
    rsp.fpkm_known_genes_matrix()

def work_sample_rna(idx):
    smp_idx = int(idx)
    print("smp_idx: ", smp_idx)
    rsp = RnaSeqPip()
    rsp.init_proj()
    rsp.set_proj_dirs()

    rsp.set_sample(smp_idx)
    rsp.set_sample_dirs()
    rsp.set_sample_files_path_names()

    rsp.merge_lanes()
    # rsp.copy_reordered()
    rsp.smp_reorder()
    rsp.smp_rmdup()

    rsp.wiggle()
    rsp.qnsort()
    rsp.raw_counts()
    rsp.fpkm_known()
    rsp.fpkm_denovo()


    #dsp.set_sample_lane(idx)
    #dsp.set_sample_dirs()
    #dsp.set_sample_files_path_names()
    #do the work on the sample
    #dsp.sort_and_index_bam()

def work_lane_rna(idx):

    print("idx: ", idx)

    dsp = DnaSeqPip()
    dsp.set_sample_lane(idx)

    dsp.sort_and_index_bam()

    # if False:
    #     rsp.subsample(0.2)
    #     rsp.subsample_compress()
    #     rsp.fastqc_subsample()
    #     rsp.save_subsample()
    #
    # if False:
    #     rsp.restore_subsample()
    #     rsp.trim()
    #     rsp.trim_to_subsample()
    #     rsp.fastqc_subsample()

    #rsp.tophat()
    #rsp.tophat_unmapped_repair_pe()

    #rsp.tophat_unmapped_flt_to_subsample()

    #GQ style
    #rsp.tophat_unmapped_to_subsample()

    #rsp.fastq_subsample()

    #rsp.bwa()
    #rsp.extract_softclips()


    #rsp.test()

    #rsp.process_accepted_hits()

def work3_rna():
    print("in work3 ....")

    rsp = RnaSeqPip()
    # dsp.gen_sample_subm()

    rsp.init_proj()
    rsp.conf_redis_client()
    rsp.connect_redis()

    # rsp.queue_name = "trim_lane_trimmomatic"
    # print "nb jobs submitted: ", rsp.push_jobs_smp_lane_idx()

    # rsp.queue_name = "align_lane_tophat"
    # print "nb jobs submitted: ", rsp.push_jobs_smp_lane_idx()

    # rsp.queue_work = "merge_lanes"
    # print("nb jobs submitted: ", rsp.push_jobs_smp_idx())

    # rsp.smp_idx = 0
    # rsp.merge_lanes()




    #rsp.queue_work = "qnsort"
    #print("nb jobs submitted: ", rsp.push_jobs_smp_idx())

    # rsp.queue_work = "merge_lanes_rnaseqc"
    # print("nb jobs submitted: ", rsp.push_jobs_smp_idx())
    # rsp.smp_idx = 0
    # rsp.merge_lanes_rnaseqc()

    # rsp.queue_work = "rseqc"
    # print("nb jobs submitted: ", rsp.push_jobs_smp_idx())
    # rsp.smp_idx = 2
    # rsp.rseqc()



    #rsp.queue_work = "rprts"
    #print("nb jobs submitted: ", rsp.push_jobs_rprt_idx())

    #rsp.rprt_idx = 2
    #rsp.rprts()







    # rsp.trim_lane_trimmomatic(0)
    # rsp.align_lane_tophat(141)

    # all these functions have the same parameters
    #queues_smp_idx = ["merge_lanes",  "wiggle",
    #                  "fpkm_known", "fpkm_rabt", "fpkm_denovo",
    #                  "qnsort", "raw_counts"]

    #for queue in queues_smp_idx:
    #    rsp.queue_work = queue
    #    rsp.push_jobs_smp_idx()
    # rsp.smp_idx = 32
    # rsp.smp_idx = 1
    # rsp.smp_idx = 16
    #
    # rsp.merge_lanes()

    # rsp.smp_idx = 8
    # rsp.wiggle()

    # rsp.smp_idx = 0
    # rsp.merge_lanes()

    #rsp.qnsort()
    #rsp.raw_counts()


    # rsp.fpkm_known()
    # rsp.fpkm_rabt()
    # rsp.fpkm_denovo()

    # migrated here as an emergency
    # self.wiggle()
    #    self.qnsort()
    #    self.raw_counts()
    #    self.fpkm_known()
    #    self.fpkm_rabt()
    #    self.fpkm_denovo()


    # for smp_idx in range(rsp.conf.sample_names.shape[0]):
    #    rsp.merge_lanes(smp_idx)

    #rsp.rnaseqc_run()

    #rsp.tophat_align_lane(0)

    #rsp.merge_lanes_rnaseqc


    # (3)

    #rsp.smp_name = "111"

    #matrix_metrics()

    #process_nodefile()


def resume_server():
    print("in resume_server...")

    nsp = NaSeqServer().client_naseq_factory.inst
    rc = nsp.redis_obj.redis_client

    # set all waits to 0
    nsp.redis_obj.redis_client.set("shut_server", 0)
    nsp.redis_obj.redis_client.set("shut_ctrl", 0)
    nsp.redis_obj.redis_client.set("shut_clients", 0)
    nsp.redis_obj.redis_client.set("shut_queues", 0)

    # clean all active_gids and resubmit all jobs
    nsp.redis_obj.redis_client.set("wait_ctrl", 1)
    # submit jobs to queue
    func_impl = "submit_qjobs_%s" % nsp.conf.qgraph_dict_name
    print("func_impl: ", func_impl)
    # POLYMORPHIC CALL
    globals()[func_impl]()

    # submit_qjobs_na_comb2()
    nsp.redis_obj.redis_client.set("wait_ctrl", 0)

def restart_queues(queues):
    print("in restart_queues...")

    nsp = NaSeqServer().client_naseq_factory.inst
    ro = nsp.redis_obj
    rc = ro.redis_client

    # reopen queues
    ro.qgraph_config(nsp.conf.qgraph_dict)

    # set all waits to 0
    nsp.redis_obj.redis_client.set("shut_server", 0)
    nsp.redis_obj.redis_client.set("shut_ctrl", 0)
    nsp.redis_obj.redis_client.set("shut_clients", 0)
    nsp.redis_obj.redis_client.set("shut_queues", 0)

    # clean all active_gids and resubmit all jobs
    nsp.redis_obj.redis_client.set("wait_ctrl", 1)
    # delete some queues
    # queues = ["cat_hc_contigs", "comb_vars_rprt"]
    # queues = ["grind_lanes", "deploy_web"]
    # queues = ["grind_lanes", "deploy_web"]
    #
    for q in queues:
        nsp.redis_obj.qwork = q
        nsp.redis_obj.del_qwork_jobs()
    # resubmit
    # submit_qjobs_na_comb2()
    # submit jobs to queue
    func_impl = "submit_qjobs_%s" % nsp.conf.qgraph_dict_name
    print("func_impl: ", func_impl)
    # POLYMORPHIC CALL
    globals()[func_impl]()

    nsp.redis_obj.redis_client.set("wait_ctrl", 0)


def spawn_server(ppn, pmem):

    print("in spawn_server...")

    nsp = NaSeqServer().server_naseq_factory.inst
    rc = nsp.redis_obj.redis_client

    # delete all runtime information
    for key in rc.scan_iter("client:*"):
        rc.delete(key)
    for key in rc.scan_iter("node:*"):
        rc.delete(key)

    shutil.rmtree(os.path.join(nsp.conf.redis_dir, "wk-*.log"), ignore_errors=True, onerror=None)
    # files = glob.glob(os.path.join(nsp.conf.redis_dir, "wk-*.log"))
    # for f in files:
    #     os.remove(f)

    # save submitted limits for server in redis
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "ppn", ppn)
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "pmem", pmem)
    # make a place for the debugging manual client on the server
    rc.set("last_client", 10)

    rc.hset("client:1", "gid", 1)
    rc.hset("client:1", "hst", "localhost")
    rc.hset("client:1", "lid", 0)

    # define the graphic to control
    nsp.redis_obj.qgraph_config(nsp.conf.qgraph_dict)

    #  control queues
    nsp.redis_obj.spawn_ctrl()
    nsp.redis_obj.spawn_nodectrl()
    # nsp.servers_obj.start_zk_server()
    # time.sleep(2)
    # nsp.servers_obj.start_drillbit()
    # time.sleep(2)
    # nsp.servers_obj.start_derby_server()
    # time.sleep(2)
    # nsp.servers_obj.start_spark_master()

    resume_server()
    # set all waits to 0
    # nsp.redis_obj.redis_client.set("shut_server", 0)
    # nsp.redis_obj.redis_client.set("shut_ctrl", 0)
    # nsp.redis_obj.redis_client.set("shut_clients", 0)
    # nsp.redis_obj.redis_client.set("shut_queues", 0)
    # clean all active_gids and resubmit all jobs
    # submit_qjobs_na_comb2()


# spawn a simple server
def simple_server(ppn=12, pmem=1700, blocking=True):
    print("in spawn_server_blocking...")

    nsp = NaSeqServer().server_naseq_factory.inst
    rc = nsp.redis_obj.redis_client

    # delete all runtime information
    # for key in rc.scan_iter("client:*"):
    #     rc.delete(key)
    # for key in rc.scan_iter("node:*"):
    #     rc.delete(key)

    shutil.rmtree(os.path.join(nsp.conf.redis_dir, "wk-*.log"), ignore_errors=True, onerror=None)
    shutil.rmtree(os.path.join(nsp.conf.redis_dir, "ctrl.log"), ignore_errors=True, onerror=None)
    # files = glob.glob(os.path.join(nsp.conf.redis_dir, "wk-*.log"))
    # for f in files:
    #     os.remove(f)



    # save submitted limits for server in redis
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "ppn", ppn)
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "pmem", pmem)
    # make a place for the debugging manual client on the server
    rc.set("last_client", 10)

    rc.hset("client:1", "gid", 1)
    rc.hset("client:1", "hst", "localhost")
    rc.hset("client:1", "lid", 0)


    # define the graphic to control
    # nsp.redis_obj.qgraph_config(nsp.conf.qgraph_dict)
    # submit undone remaining jobs
    # resume_server()

    #  control queues
    # nsp.redis_obj.spawn_ctrl()
    # nsp.redis_obj.spawn_nodectrl()

    # data analysis servers

    # nsp.servers_obj.start_zk_server()
    # time.sleep(2)
    # nsp.servers_obj.start_drillbit()
    # time.sleep(2)
    # nsp.servers_obj.start_derby_server()
    # time.sleep(2)
    # nsp.servers_obj.start_spark_master()

    if blocking:
        nsp.redis_obj.wait_for_shutdown_server()


# spawn server
# spawn also ctrl
def spawn_server_blocking(ppn, pmem):
    print("in spawn_server_blocking...")

    nsp = NaSeqServer().server_naseq_factory.inst
    rc = nsp.redis_obj.redis_client

    # delete all runtime information
    for key in rc.scan_iter("client:*"):
        rc.delete(key)
    for key in rc.scan_iter("node:*"):
        rc.delete(key)

    shutil.rmtree(os.path.join(nsp.conf.redis_dir, "wk-*.log"), ignore_errors=True, onerror=None)
    shutil.rmtree(os.path.join(nsp.conf.redis_dir, "ctrl.log"), ignore_errors=True, onerror=None)
    # files = glob.glob(os.path.join(nsp.conf.redis_dir, "wk-*.log"))
    # for f in files:
    #     os.remove(f)



    # save submitted limits for server in redis
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "ppn", ppn)
    rc.hset("node:%s" % nsp.redis_obj.local_host,  "pmem", pmem)
    # make a place for the debugging manual client on the server
    rc.set("last_client", 10)

    rc.hset("client:1", "gid", 1)
    rc.hset("client:1", "hst", "localhost")
    rc.hset("client:1", "lid", 0)


    # define the graphic to control
    nsp.redis_obj.qgraph_config(nsp.conf.qgraph_dict)
    # submit undone remaining jobs
    resume_server()

    #  control queues
    if config_utils.do_spawn_ctrl:
        nsp.redis_obj.spawn_ctrl()
    if config_utils.do_spawn_nodectrl:
        nsp.redis_obj.spawn_nodectrl()

    # data analysis servers

    if config_utils.do_start_jupyter_server:
        nsp.servers_obj.start_jupyter_server()

    if config_utils.do_start_zk_server:
        nsp.servers_obj.start_zk_server()
        time.sleep(2)
    # clients do not keep their configurations
    shutil.rmtree(os.path.join(nsp.servers_obj.servers_dir, "drill"), ignore_errors=True, onerror=None)
    if config_utils.do_start_drillbit:
        nsp.servers_obj.start_drillbit()
        time.sleep(2)
    if config_utils.do_start_derby_server:
        nsp.servers_obj.start_derby_server()
        time.sleep(2)

    shutil.rmtree(os.path.join(nsp.servers_obj.servers_dir, "spark"), ignore_errors=True, onerror=None)
    if config_utils.do_start_spark_master:
        nsp.servers_obj.start_spark_master()
        # need derby to activate hive
        print("in spawn_server_blocking.....22432")
        time.sleep(2)

    if config_utils.do_start_spark_locslv:
        nsp.servers_obj.start_spark_locslv(nb_workers=3, cores="4", mem="8G")
        # need derby to activate hive
        print("in spawn_server_blocking.....22432")
        time.sleep(2)

    if config_utils.do_start_thrift_server:
        nsp.servers_obj.start_thrift_server()
        # need derby to activate hive
        time.sleep(2)

    if config_utils.do_start_drillbit:
        pass
        # nsp.servers_obj.drill_config_init()

    # easy port forwarding
    if config_utils.do_create_ssh_config:
        nsp.servers_obj.create_ssh_config()

    nsp.redis_obj.wait_for_shutdown_server()


def spawn_clients(ppn, pmem):
    print("in spawn_clients...", ppn, pmem)

    nsp = NaSeqServer().client_naseq_factory.inst
    nsp.redis_obj.spawn_nodectrl()
    nsp.redis_obj.spawn_clients(int(ppn), int(pmem))

    # nsp.servers_obj.start_drillbit()
    # time.sleep(2)
    nsp.servers_obj.start_spark_slave()
    # time.sleep(2)

def spawn_clients_blocking(ppn, pmem):

    print("in spawn_clients...", ppn, pmem)

    nsp = NaSeqServer().client_naseq_factory.inst

    if config_utils.do_spawn_nodectrl:
        nsp.redis_obj.spawn_nodectrl()

    nsp.redis_obj.spawn_clients(int(ppn), int(pmem))

    # important here for qsub
    if config_utils.do_start_drillbit:
        nsp.servers_obj.start_drillbit()
        time.sleep(2)
    # spark master works for slaves too for now
    if config_utils.do_start_spark_master:
        nsp.servers_obj.start_spark_slave()

    nsp.redis_obj.wait_for_shutdown_clients()

# def load_one_module(module_name):
#
#     cmd = r"""/home/dbadescu/local/bin/modulecmd python load %s""" % module_name
#     cmd_proc = os.popen(cmd)
#     exec(cmd_proc)
#
# def load_modules():
#         load_one_module('gcc/4.8.2')
#         load_one_module('OpenBLAS+LAPACK/0.2.12-openmp-gcc')
#         load_one_module('raglab/naseq_pip/0.9.2')

def spawn_ctrl():

    print("in spawn_ctrl...")

    # nsp = NaSeqServer().server_naseq_factory.inst
    nsp = NaSeqServer().client_naseq_factory.inst
    nsp.redis_obj.spawn_ctrl()


def spawn_nodectrl():

    print("in spawn_nodectrl...")

    # nsp = NaSeqServer().server_naseq_factory.inst
    nsp = NaSeqServer().client_naseq_factory.inst
    nsp.redis_obj.spawn_nodectrl()

def ctrl():
    print("ok on stderr", file=sys.stderr, flush=True)

    print("in ctrl... on stderr", file=sys.stderr, flush=True)
    nsp = NaSeqServer().client_naseq_factory.inst
    rc = nsp.redis_obj.redis_client

    # print("111111111", file=sys.stderr)


    while rc.get("shut_ctrl") == "0":
        time.sleep(10)
        # skip if controler in wait mode (external update)
        if rc.get("wait_ctrl") == "1":
            continue

        queues_verif = rc.get("queues_verif")

        # print("queues_verif on stderr:", queues_verif, file=sys.stderr, flush=True)

        if not queues_verif:
            print("we should verify...", file=sys.stderr, flush=True)

            active_gids = rc.lrange("active_gids", 0, -1)
            print("active_gids: ", active_gids, file=sys.stderr, flush=True)

            for gid in active_gids:
                print("gid: ", gid, file=sys.stderr, flush=True)
                hst = rc.hget("client:%s" % gid, "hst")
                print("hst: ", hst, file=sys.stderr, flush=True)

                # cmd = curl http://cp2351:5000/client_ctrl/11

                # cmd = r""" /usr/bin/ssh %s \
                # "source /home/dbadescu/.bash_profile; \
                # module load raglab/naseq_pip/0.9.3; \
                # cd %s; \
                # naseq_server.py --ctrlcl %s" """ % (hst, nsp.conf.pysrc_dir, gid)
                # cmd = r""" /usr/bin/ssh %s \
                # "source /home/dbadescu/.bash_profile; \
                # module load raglab/naseq_pip/0.9.3; \
                # cd %s; \
                # naseq_server.py --ctrlcl %s" """ % (hst, nsp.conf.pysrc_dir, gid)

                cmd = r"""curl -s -S http://%s:5000/client_ctrl/%s""" % (hst, gid)
                print(cmd, file=sys.stderr, flush=True)
                p3 = subprocess.Popen(cmd, shell=True,
                                      cwd=nsp.conf.pysrc_dir,
                                      stdout=subprocess.PIPE)
                res = p3.communicate()[0].rstrip()
                rc3 = p3.returncode
                print("rc3: ", rc3, " res: ", res, file=sys.stderr, flush=True)

                # delete not communicating clients
                #if rc3 == 255:
                #    recycle_gid(nsp, gid)


            #
            rc.set("queues_verif", 0)
            rc.expire("queues_verif", 600)

        else:
            # print("verified !", file=sys.stderr, flush=True)
            ttl = rc.ttl("queues_verif")
            print("ttl: ", ttl, file=sys.stderr, flush=True)


        # print("nsp.redis_obj.redis_client.llen(nsp.redis_obj.queue_active_all): ", nsp.redis_obj.redis_client.llen(nsp.redis_obj.queue_active_all) )
        # advance queues
        # if nsp.redis_obj.redis_client.llen(nsp.redis_obj.queue_active_done) == \
        #         nsp.redis_obj.redis_client.llen(nsp.redis_obj.queue_active_all) or \
        #         nsp.redis_obj.redis_client.llen(nsp.redis_obj.queue_active_all) == 0:
        #     nsp.queue_advance()

        # check queues, update status
        # print("qgraph_ckeck_mark_done(): ", file=sys.stderr, flush=True)
        nsp.redis_obj.qgraph_ckeck_mark_done()
        # print("qgraph_ckeck_mark_open(): ", file=sys.stderr, flush=True)
        nsp.redis_obj.qgraph_ckeck_mark_open()

        # if all jobs are finished shutdown clients
        if nsp.redis_obj.qgraph_all_done:
            print("All done, shutting down clients: ", file=sys.stderr, flush=True)
            rc.set("shut_clients", 1)
            time.sleep(60)
            # print("All done, shutting down server: ", file=sys.stderr, flush=True)
            # rc.set("shut_server", 1)

    print("ctrl finished ok..", file=sys.stderr, flush=True)

def nodectrl():

    print("in nodectrl...", file=sys.stderr)
    nsp = NaSeqServer().client_naseq_factory.inst

    print("aaaaaaa", file=sys.stderr)

    todos = {
        'todo1': {'task': 'build an API'},
        'todo2': {'task': nsp.redis_obj.queue_active},
        'todo3': {'task': nsp.redis_obj.redis_client.get("shut_ctrl")},
    }

    app = Flask(__name__)
    api = Api(app)
    CustomTask = Task.set_tasks(todos).set_nsp(nsp)


    api.add_resource(CustomTask, "/api/<task_id>")

    # api.add_resource(HelloWorld, '/')

    CustomClientCtrl = ClientCtrl.set_nsp(nsp)
    api.add_resource(CustomClientCtrl, "/client_ctrl/<gid>")

    app.run(host='0.0.0.0', debug=False)


    # while nsp.redis_obj.redis_client.get("shutdown_ctrl") == "0":
    # time.sleep(60)


    print("nodectrl finished ok..", file=sys.stderr)


def ctrl_to_do():
    pass


    # myserver = ssh.bake("hb-2r06-n42", p=1393)
    # myserver = ssh.bake("hb-2r06-n42")
    # print(myserver) # "/usr/bin/ssh myserver.com -p 1393"

    # resolves to "/usr/bin/ssh myserver.com -p 1393 whoami"
    # iam2 = myserver.whoami()
    # print("iam2: ", iam2)

def recycle_gid(nsp, gid):

    exec_queue = nsp.redis_obj.redis_client.hget("client:%s" % gid, "exec_queue")
    args_json = nsp.redis_obj.redis_client.hget("client:%s" % gid, "args_json")
    # remove active status
    nsp.redis_obj.redis_client.lrem("active_gids", 0, gid)
    nsp.redis_obj.redis_client.hdel("client:%s" % gid, ["exec_queue", "args_json"])
    # repost work
    nsp.redis_obj.qwork = exec_queue
    nsp.redis_obj.redis_client.rpush(nsp.redis_obj.qwork_todo, args_json)


def ctrlcl(gid):

    print("------------------------gid: ", gid)

    nsp = NaSeqServer().client_naseq_factory.inst

    pid_str = nsp.redis_obj.redis_client.hget("client:%s" % gid, "pid")

    pid = int(pid_str)
    print("pid: ", pid)

    try:
        p = psutil.Process(pid)
    except psutil.NoSuchProcess:
        print("no process found")
        recycle_gid(nsp, gid)

    else:
        childs = p.children(recursive=True)
        for child_idx in range(childs.__len__()):
            childp = childs[child_idx]
            print("child_idx, child: ", child_idx, childp)
    finally:
        print("in finally")

def loop(gid):

    print("------------------------gid: ", gid)

    nsp = NaSeqServer().client_naseq_factory.inst

    nsp.loop(gid)


def controller():
    global VERBOSE
    #Create instance of OptionParser Module, included in Standard Library
    p = optparse.OptionParser(description='A unix toolbox',
                                            prog='dnaseq',
                                            version='py4sa 0.1',
                                            usage= '%prog [option]')
    p.add_option('--ip', '-i', action="store_true", help='gets current IP Address')
    p.add_option('--usage', '-u', action="store_true", help='gets disk usage of homedir')
    p.add_option('--verbose', '-v',
                action = 'store_true',
                help='prints verbosely',
                default=False)

    p.add_option('--prob', help='prob', dest='prob', action='store_true')
    p.add_option('--work', help='work', dest='work', action='store_true')


    p.add_option('--work-dna', help='work-dna', dest='work_dna', action="store_true")
    p.add_option('--work-dat', help='work-dat', dest='work_dat', action="store_true")

    p.add_option('--ctrl', action="store_true", help='main ctrl')

    p.add_option('--nodectrl', action="store_true", help='nodectrl')
    p.add_option('--ctrlcl', action='store', help='ctrlcl', dest='ctrlcl_arg')

    p.add_option('--work-lane', help='work-lane', dest='work_lane_arg', action='store')
    p.add_option('--work-sample', help='work-sample', dest='work_sample_arg', action='store')
    p.add_option('--loop', action='store', help='loop', dest='loop_arg')


    p.add_option('--spawn-ctrl', action='store_true', help='spawn-ctrl')
    p.add_option('--spawn-nodectrl', action='store_true', help='spawn-nodectrl')

    # p.add_option('--spawn-server', action='store_true', help='spawn-server')
    # p.add_option('--spawn-server-blocking', action='store_true', help='spawn-server-blocking')
    p.add_option('--simple-server', help='simple-server', dest='smpl_srv_arg', action='store', nargs=3)

    p.add_option('--spawn-server', help='spawn-server', dest='spawn_sv_arg', action='store', nargs=2)
    p.add_option('--spawn-server-blocking', help='spawn-server-blocking', dest='spawn_sv_bl_arg', action='store', nargs=2)

    p.add_option('--spawn-clients', help='spawn-clients', dest='spawn_cl_arg', action='store', nargs=2)
    p.add_option('--spawn-clients-blocking', help='spawn-clients', dest='spawn_cl_bl_arg', action='store', nargs=2)


    # Option Handling passes correct parameter to runBash
    options, arguments = p.parse_args()



    if options.verbose:
        VERBOSE=True

    # if options.ip:
    #     value = runBash(IPADDR)
    #     report(value, "IPADDR")
    # elif options.usage:
    #     value = runBash(HOMEDIR_USAGE)
    #     report(value, "HOMEDIR_USAGE")
#    elif options.work_lane_arg:
#        print "work-lane: ",options.work_lane_arg
#        work_lane(options.work_lane_arg)
    if options.work_sample_arg:
        print("work-sample: ", options.work_sample_arg)
        # work_sample(options.work_sample_arg)

    elif options.prob:
        print("prob option: ")
        prob()
    elif options.work:
        print("work option: ")
        work()
    elif options.work_dna:
        print("work_dna option: ")
        work_dna()
    elif options.work_dat:
        print("work_dat option: ")
        work_dat()

    elif options.ctrl:
        print("ctrl option: ")
        ctrl()
    elif options.nodectrl:
        print("nodectrl option: ")
        nodectrl()


    elif options.loop_arg:
        print("loop: ", options.loop_arg)
        loop(options.loop_arg)
    elif options.ctrlcl_arg:
        print("ctrlcl: ", options.ctrlcl_arg)
        ctrlcl(options.ctrlcl_arg)

    elif options.spawn_ctrl:
        print("spawn_ctrl... ")
        spawn_ctrl()

    # elif options.spawn_server:
    #     print("spawn_server... ")
    #     spawn_server()
    #
    # elif options.spawn_server_blocking:
    #     print("spawn_server_blocking... ")
    #     spawn_server_blocking()


    elif options.smpl_srv_arg:
        print("simple_server... ", options.smpl_srv_arg)
        simple_server(*options.smpl_srv_arg)

    elif options.spawn_sv_arg:
        print("spawn_server... ", options.spawn_sv_arg)
        spawn_server(*options.spawn_sv_arg)

    elif options.spawn_sv_bl_arg:
        print("spawn_server_blocking... ", options.spawn_sv_bl_arg)
        spawn_server_blocking(*options.spawn_sv_bl_arg)

    elif options.spawn_cl_arg:
        print("spawn_clients... ", options.spawn_cl_arg)
        spawn_clients(*options.spawn_cl_arg)

    elif options.spawn_cl_bl_arg:
        print("spawn_clients_blocking... ", options.spawn_cl_bl_arg)
        spawn_clients_blocking(*options.spawn_cl_bl_arg)

    elif options.spawn_nodectrl:
        print("spawn_nodectrl... ")
        spawn_nodectrl()

    else:
        p.print_help()

# Runs all the functions
def main():
    controller()

# This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    main()

