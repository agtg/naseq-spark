#!/usr/bin/env python 

__author__ = 'root'

import os, shutil
import re
import csv

import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
pd.set_option('max_columns', 50)

# from treedict import TreeDict

from scan_config_datseq import ScanConfigDatSeq

class ScanPairedReadFiles:

    def __init__(self):
        pass 
    
    def init_proj(self):
        self.conf = ScanConfigDatSeq().scan_local()
        # print self.conf.makeReport() #self.conf.items()
        
        
    def raw_reads_to_all_sample_info(self, project_regex):
        
        # ########### output config file open and header

        ofile = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "wb")
        smp_report_csv = open(os.path.join(self.conf.conf_dir, 'smp_report.csv'), "wb")

        writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer_smp_report = csv.writer(smp_report_csv, delimiter=',', quotechar='"')

        ofile_header = ["Name", "Library Barcode", "Run Type", "Run", "Quality Offset", "Region", "Status", "FASTQ1", "FASTQ2", "BAM", "Library Source"]

        writer.writerow(ofile_header)
        ###############################################

        sample_names = [f for f in os.listdir(self.conf.raw_reads_dir) ]
        #print "files: ", files

        sample = ''
        run = ''
        lane = ''
        library = ''
        fname1 = ''
        fname2 = ''
        #defaults
        quality = "33"
        status = "Data is valid"
        
        for sample in sample_names:
            print(sample)
            sample_path = os.path.join(self.conf.raw_reads_dir, sample)
            for run_lane in os.listdir(sample_path):
                print(("run_lane: ", run_lane))
                # extract run and lane
                m = re.match("^run(\S*)_(\S*)$", run_lane)
                run = m.group(1)
                lane = m.group(2)
                print(("runlane: ", run_lane , " run: ", run, " lane: ", lane))
                run_lane_path = os.path.join(sample_path, run_lane)

                # take only first pair
                # for each library
                for fname in [f for f in os.listdir(run_lane_path) if re.match('(.+)\.pair1\.fastq\.gz$', f)]:

                    # print("fname: ", fname)
                    m2 = re.match("^(\S+)\.(\S+)\.33\.pair1\.fastq\.gz$", fname)
                    smp_name = m2.group(1)
                    library = m2.group(2)
                    # reconstruct both pairs
                    # in case we miss one we miss all
                    fname1 = r"""%s.%s.33.pair1.fastq.gz""" % (smp_name, library)
                    fname2 = r"""%s.%s.33.pair2.fastq.gz""" % (smp_name, library)

                    # now we know everything
                    row = [sample, library, "PAIRED_END", run, quality, lane, status, fname1, fname2, "", "Library"]
                    print(("row: ", row))
                    writer.writerow(row)

                    # analyse link only for pair1
                    fname_f = os.path.join(run_lane_path, fname1)

                    # do not report links if files are hardcoded
                    if os.path.islink(fname_f):
                        fname_lnk = os.path.realpath(fname_f)
                        # extract project name

                        patlnk = "^" + project_regex + "([^\/]+\/[^\/]+)\/(\S+)$"
                        print(("patlnk: ", patlnk))
                        print(("fname_lnk: ", fname_lnk))


                        motlnk = re.match(patlnk, fname_lnk)
                        src_folder = motlnk.group(2)
                        src_pair = motlnk.group(3)

                        row2 = [sample, run_lane, library, src_folder, src_pair]
                        writer_smp_report.writerow(row2)


                
        #
        ofile.close()
        smp_report_csv.close()
        

    def all_sample_to_one_sample_info(self):

        # with pandas structures
        ifile  = open(os.path.join(self.conf.conf_dir, 'all-sample-info.csv'), "rb")

        self.conf.sample_lanes = pd.read_csv(ifile, names=["name", "library_barcode", "run_type", "run", "quality_offset", "region", "status", "fastq1", "fastq2", "bam", "library_source" ], header=0)
        ifile.close()

        sm = self.conf.sample_lanes
        #print sm
        #print sm.index
        #print sm.columns
        #grouped = sm.groupby('name')
        
        gr2 = sm.groupby(['name'], sort=True)
         
        gr3 = gr2['name']
        #print gr3.groups.keys()
        
        gr4 = gr3.count()
        
        
        gr5= pd.DataFrame(list(gr3.groups.keys()))

        gr5.columns = ['smp_name']
        #print gr5


        gr5.to_csv(os.path.join(self.conf.conf_dir, 'one-sample-info.csv'),
                           sep=',', header=True
                           , index=True
                           , index_label="smp_idx"
                           )

        #print grouped.groups
        #for name, group in gr3:
        #    print(name)
        #    print(group)
        
        #self.conf.sample_names = gr3.groups.keys()
        

if __name__ == "__main__":
    slf = ScanPairedReadFiles()
    slf.init_proj()

    # project_regex = "(\/gs\/project\/wst-164-aa\/share\/data\/)"
    project_regex = "(\/gs\/project\/wst-164-aa\/data\/)"

    slf.raw_reads_to_all_sample_info(project_regex)
    slf.all_sample_to_one_sample_info()



    # project_regex = "(\/gs\/project\/wst-164-ab\/rnvar\/)"

print("ok...")

# slf.paired_reads_to_all_sample_info()



