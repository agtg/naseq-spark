### RnaSeq pipeline, Smartseq full length and Smartseq3 umi variant.

Source code used internally by our group on Compute Canada clusters, where we dynamically spawn an Apache Spark Cluster.

It is provided as is under a permissive BSD 3 clause license. See LICENSE file.

Copyright 2021 (c) The authors of agtg/naseq-spark.
Authors list can be retrieved from git commits.

